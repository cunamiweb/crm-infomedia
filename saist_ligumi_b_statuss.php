<?php
/**
 * Skripts, kas automātiski piešķir visiem beigušamies saistītajiem līgumiem statusu
 * 'Termiņš' ar pamatlīguma datumu un komentāru.
 *
 * Atlasa līgumus kas ir saistītie un nav beigušos līgumu tabulā. Skatās līguma statusu,
 * un skatās vai tas ir "Beidzies" (6). Tādā gadījumā liek iekšā beigušos tabulā.
 *
 * @since 2012-07-02
 * @author Dāvis Frīdenvalds davis@datateks.lv
 */

ini_set('max_execution_time', 3600);
include('inc/init.php');

// Atrod visus saistītos līgumus bez beigu statusa
$contracts = db_get_assoc_all(db_query('
  SELECT
    g.id,
    COUNT(b.id) as count,
    g.saistits_pie
  FROM ' . DB_PREF . 'ligumi g
  LEFT JOIN ' . DB_PREF . 'ligumi_beigusies b ON b.liguma_id = g.id
  WHERE
    g.saistits_pie IS NOT NULL
  GROUP BY g.id
  HAVING
    count = 0
'));

if(!empty($contracts)) {
  foreach($contracts as $contract) {
    if(get_liguma_status_by_id($contract['id']) == 6) {

      //  Pamatlīgums
      $base_contract = db_get_assoc(db_query('SELECT * FROM ' . DB_PREF . 'ligumi WHERE id = ' . $contract['saistits_pie']));

      if(!empty($base_contract)) {
        db_query("
          INSERT INTO `".DB_PREF."ligumi_beigusies`
          SET
            liguma_id = ".$contract['id'].",
            beigu_statuss = 'termins',
            parslegts_lig_nr = ".$contract['saistits_pie'].",
            atlikt_lidz = '" . $base_contract['ligumadat'] . "',
            komentars = 'Šis ir papildinājums līgumam ar Nr. ".$base_contract['ligumanr']."',
            laboja_pardeveja_id = 0,
            laboja_laiks = NOW(),
            pirmoreiz_apstradats = NOW()
        ");
      }
    }
  }
}

die();
?>