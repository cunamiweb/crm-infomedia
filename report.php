<?php
//alter table new_ligumi_google_reports add column fullreport tinyint unsigned default 1;
//alter table new_ligumi add column last_rentabil14 double default 0;
//header('Content-Type: text/html; charset=utf-8');
include('inc/init.php');
require($models_path.'/adwordsReports.class.php');

set_time_limit(0);

$dir=opendir('tmp/google_reports');
while ($f=readdir($dir)){
	if ($f!='.' and $f!='..' and $f!='.gitignore'){
		unlink('tmp/google_reports/'.$f);
	}
}
closedir($dir);

$yearago=date('Y-m-d',mktime(0,0,0,(intval(date('n'))-3),date('j'),date('Y')));

$res=db_get_assoc_all(db_query("select k.liguma_id,k.konta_numurs,l.reklama_no,l.reklama_lidz from ".DB_PREF."pielikumi_kontu_linki k join ".DB_PREF."ligumi l on k.liguma_id=l.id where l.reklama_lidz>'".$yearago."' and k.konta_numurs is not null and k.konta_numurs<>'' order by k.id desc"));

$fields=array(
	'avgCPC'=>'AverageCpc',
	'avgCPM'=>'AverageCpm',
	'avgPosition'=>'AveragePosition',
	'clicks'=>'Clicks',
	'cost'=>'Cost',
	#'costConv'=>'CostPerConversionManyPerClick',
	#'costEstTotalConv'=>'CostPerEstimatedTotalConversion',
	'ctr'=>'Ctr',
	#'estTotalConvRate'=>'EstimatedTotalConversionRate',
	#'estTotalConvValue'=>'EstimatedTotalConversionValue',
	#'estTotalConvValueClick'=>'EstimatedTotalConversionValuePerClick',
	#'estTotalConvValueCost'=>'EstimatedTotalConversionValuePerCost',
	#'estTotalConv'=>'EstimatedTotalConversions',
	'impressions'=>'Impressions',
	'searchLostISBudget'=>'SearchBudgetLostImpressionShare',
	'searchExactMatchIS'=>'SearchExactMatchImpressionShare',
	'searchImprShare'=>'SearchImpressionShare',
	'searchLostISRank'=>'SearchRankLostImpressionShare',
	'viewThroughConv'=>'ViewThroughConversions'
);

//Full reports
$yesterday=date('Y-m-d',mktime(0,0,0,intval(date('n')),(intval(date('j'))-1),intval(date('Y'))));
$yesterday=date('Y-m-d',mktime(0,0,0,intval(date('n')),(intval(date('j'))-1),intval(date('Y'))));
foreach ($res as $r){
	if (trim($r['konta_numurs']) and strlen($r['konta_numurs'])<30){
		try{
			$adwreport=new AdwordsReports(ADWORDS_VERSION,$r['konta_numurs']);
			$reklama_lidz=$r['reklama_lidz'];
			if ($r['reklama_lidz']>$yesterday){
				$reklama_lidz=$yesterday;
			}
			$xml=$adwreport->getReport($fields,$r['konta_numurs'],$r['reklama_no'],$r['reklama_lidz'],$r['reklama_no'],$r['reklama_lidz'],true);
			echo "Ir: ".$r['konta_numurs']."\n";
		}
		catch (Exception $e){
			#var_dump($e);
			echo "Nav: ".$r['konta_numurs']."\n";
		}
	}
	echo '. ';
	sleep(2);
}
#die();

//Reports for last 14 days
$minus14=date('Y-m-d',mktime(0,0,0,intval(substr($yesterday,5,2)),intval(substr($yesterday,8,2))-6,intval(substr($yesterday,0,4))));
foreach ($res as $r){
	if (trim($r['konta_numurs']) and strlen($r['konta_numurs'])<30){
		try{
			$adwreport=new AdwordsReports(ADWORDS_VERSION,$r['konta_numurs']);
			if ($r['reklama_lidz']<$yesterday){
				$lidz=$r['reklama_lidz'];
				$no=date('Y-m-d',mktime(0,0,0,intval(substr($lidz,5,2)),intval(substr($lidz,8,2))-6,intval(substr($lidz,0,4))));
				if ($r['reklama_no']>$no){
					$no=$r['reklama_no'];
				}
			}
			else{
				$no=$r['reklama_no'];
				if ($r['reklama_no']<$minus14){
					$no=$minus14;
				}
				$lidz=$yesterday;
			}
			$xml=$adwreport->getReport($fields,$r['konta_numurs'],$r['reklama_no'],$r['reklama_lidz'],$no,$lidz,false);
		}
		catch (Exception $e){
            #echo 'Get report error';
            #var_dump($e);
			#die();
		}
	}
	echo ".\n";
	sleep(2);
}


$yesterday2=new DateTime($yesterday);
db_query("delete from ".DB_PREF."ligumi_google_reports");
$res=db_get_assoc_all(db_query("select l.nosaukums,k.liguma_id,k.konta_numurs,l.reklama_no,l.reklama_lidz,(SELECT SUM(summa) FROM new_rekini WHERE liguma_id=l.id AND atkapsanas_rekins=0) as liguma_summa_bez_pvn from ".DB_PREF."pielikumi_kontu_linki k join ".DB_PREF."ligumi l on k.liguma_id=l.id where k.konta_numurs is not null and k.konta_numurs<>'' order by k.id desc"));
foreach ($res as $r){
	if (trim($r['konta_numurs']) and file_exists('tmp/google_reports/report.'.date('Ymd').'.'.$r['konta_numurs'].'.'.date('Ymd',strtotime($r['reklama_no'])).'.'.date('Ymd',strtotime($r['reklama_lidz'])).'.f.xml')){
		$xml=simplexml_load_string(file_get_contents('tmp/google_reports/report.'.date('Ymd').'.'.$r['konta_numurs'].'.'.date('Ymd',strtotime($r['reklama_no'])).'.'.date('Ymd',strtotime($r['reklama_lidz'])).'.f.xml'));
		$test=db_get_val(db_query("select id from ".DB_PREF."ligumi_google_reports where liguma_id=".$r['liguma_id']." and konta_numurs='".$r['konta_numurs']."' and cast(laiks as date)=cast(now() as date) and datums_no='".date('Y-m-d',strtotime($r['reklama_no']))."' and datums_lidz='".date('Y-m-d',strtotime($r['reklama_lidz']))."' and fullreport=1"));
		$from=new DateTime($r['reklama_no']);
		$to=new DateTime($r['reklama_lidz']);
		savereport($from,$to,$test,$xml,$r,1);
		$xml=simplexml_load_string(file_get_contents('tmp/google_reports/report.'.date('Ymd').'.'.$r['konta_numurs'].'.'.date('Ymd',strtotime($r['reklama_no'])).'.'.date('Ymd',strtotime($r['reklama_lidz'])).'.p.xml'));
		$test=db_get_val(db_query("select id from ".DB_PREF."ligumi_google_reports where liguma_id=".$r['liguma_id']." and konta_numurs='".$r['konta_numurs']."' and cast(laiks as date)=cast(now() as date) and datums_no='".date('Y-m-d',strtotime($r['reklama_no']))."' and datums_lidz='".date('Y-m-d',strtotime($r['reklama_lidz']))."' and fullreport=0"));
		if ($r['reklama_lidz']<$yesterday){
			$to=new DateTime($r['reklama_lidz']);
			$from=new DateTime(date('Y-m-d',mktime(0,0,0,$to->format('n'),intval($to->format('j'))-6,$to->format('Y'))));
			if ($r['reklama_no']>$from->format('Y-m-d')){
				$from=new DateTime($r['reklama_no']);
			}
		}
		else{
			$from=new DateTime($r['reklama_no']);
			if ($r['reklama_no']<$minus14){
				$from=new DateTime($minus14);
			}
			$to=$yesterday2;
		}
		savereport($from,$to,$test,$xml,$r,0);
	}
}

db_query('update new_ligumi set last_rentabil2=ifnull((select rentabil from new_ligumi_google_reports where new_ligumi_google_reports.liguma_id=new_ligumi.id and fullreport=1 order by new_ligumi_google_reports.datums_lidz desc limit 1),0)');
db_query('update new_ligumi set last_rentabil14=ifnull((select rentabil from new_ligumi_google_reports where new_ligumi_google_reports.liguma_id=new_ligumi.id and fullreport=0 order by new_ligumi_google_reports.datums_lidz desc limit 1),0)');
db_query('
	update new_ligumi
	set total_rentabil=ifnull(
		(
			select sum(rentabil)
			from new_ligumi_google_reports
			where
				new_ligumi_google_reports.liguma_id=new_ligumi.id
			group by new_ligumi_google_reports.liguma_id
		),
		0
	)
');


function savereport($from,$to,$test,$xml,$r,$full){
	global $yesterday2,$fields;
	$from2=new DateTime($r['reklama_no']);
	$to2=new DateTime($r['reklama_lidz']);
	$total=intval($to2->diff($from2)->format('%a'))+1;
	$spent=intval($to->diff($from)->format('%a'))+1;
	if ($yesterday2>$to){
		if ($full){
			$spent=$total;
		}
	}
	else{
		$spent=intval($yesterday2->diff($from)->format('%a'))+1;
	}
	if (intval($spent)==0 or intval($total)==0 or floatval($r['liguma_summa_bez_pvn'])==0){
		$rentabil=0;
	}
	else{
		$rentabil=(round((intval($xml->table->row['cost'])/$spent*$total/floatval($r['liguma_summa_bez_pvn']))/1000000,3)*100);
	}
	if($r['liguma_id'] == 4682){
		echo $from2->format('Y-m-d').' '.$to2->format('Y-m-d').' '.$from->format('Y-m-d').' '.$to->format('Y-m-d').' '.$spent.' '.$total.' '.$rentabil.' '.$r['liguma_id'].' '.$r['nosaukums']."\n";
  }
	if ($test){
		$sql="update ".DB_PREF."ligumi_google_reports set laiks=now()";
		foreach ($fields as $k=>$v){
			$sql.=','.$v."='".$xml->table->row[$k]."'";
		}
		$sql.=",rentabil=".$rentabil.",fullreport=".$full.",datums_no='".$from->format('Y-m-d')."',datums_lidz='".$to->format('Y-m-d')."' where id=".$test;
	}
	else{
		$sqlstart="insert into ".DB_PREF."ligumi_google_reports(liguma_id,konta_numurs,laiks,datums_no,datums_lidz,fullreport";
		$sqlend="values(".$r['liguma_id'].",'".$r['konta_numurs']."',now(),'".$from->format('Y-m-d')."','".$to->format('Y-m-d')."',".$full;
		foreach ($fields as $k=>$v){
			$sqlstart.=','.$v;
			$sqlend.=",'".$xml->table->row[$k]."'";
		}
		$sql=$sqlstart.',rentabil) '.$sqlend.','.$rentabil.')';
	}
	$result = db_query($sql);

  if(!$result){
    echo "Failed to execute query: ".$sql."\n";
		echo "Error: ".mysqli_error($db)."\n";
		die();
  }

}

?>