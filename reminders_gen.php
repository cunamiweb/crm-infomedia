<?php
include("inc/init.php");

// 1 - klienti
// 2 - norēķini
// 3 - darbiniei

file_put_contents('reminders_get.txt', date("d.m.Y H:i:s").' Started'.PHP_EOL, FILE_APPEND);

$query = db_query("
  SELECT *
  FROM `".DB_PREF."rekini`
  ORDER BY id ASC
");

$rekini = array();

while($row = db_get_assoc($query)) {
  $rekini[$row['liguma_id']][] = $row;
}

$query = db_query("
  SELECT *
  FROM `".DB_PREF."atgadinajumi_veidi`
");

$atg_veidi = array();

while($row = db_get_assoc($query)) {
  $atg_veidi[$row['id']] = $row;
}

$query = db_query("
  SELECT *
  FROM `".DB_PREF."ligumi`
  ORDER BY
    `ligumadat` DESC,
    `ligumanr` DESC
");

$curdate = date('Y-m-d');
$yesterday = date('Y-m-d', strtotime('-1 day'));

while($row = db_get_assoc($query)) {

  $beigu_statuss_data = db_get_assoc(db_query("SELECT * FROM `".DB_PREF."ligumi_beigusies` WHERE `liguma_id` = " . $row['id']));
  $parslegs_data = db_get_assoc(db_query("SELECT * FROM `".DB_PREF."ligumi_parslegsanu_rinda` WHERE `liguma_id` = " . $row['id']));

  // brīdinājums, ka klientam drīz beigsies līgums (2ned pirms beigām)

  if (!empty($row['reklama_lidz']) && $curdate == date('Y-m-d', strtotime('-2 weeks', strtotime($row['reklama_lidz'])))) {

    db_query("
      INSERT INTO `".DB_PREF."atgadinajumi` (
        liguma_id,
        atgadinajuma_veida_id,
        datums,
        tema,
        saturs,
        redzamiba
      ) VALUES (
        ".$row['id'].",
        12,
        NOW(),
        1,
        'Drīz beigsies līgums',
        'pardevejs'
      )
    ");

  }

  // Atgādinājums pēc 6 mēnešiem piezvanīt klientam, apjautāties kā iet.

  if ($row['reklama_lidz'] >= $curdate && $row['reklama_no'] == date('Y-m-d', strtotime('-6 months'))) {

    db_query("
      INSERT INTO `".DB_PREF."atgadinajumi` (
        liguma_id,
        atgadinajuma_veida_id,
        datums,
        tema,
        saturs,
        redzamiba
      ) VALUES (
        ".$row['id'].",
        13,
        NOW(),
        1,
        'Pagājis pusgads kopš reklāmas sākuma - jāapjautājas, kā iet',
        'pardevejs'
      )
    ");

  }

  // Ja līguma saņemšanas forma nav Oriģināls, tad 2 ned pēc reklāmas sākuma jābūt atgādinājumam, ka jāpieprasa līguma oriģināls.

  if ($row['sanemsanas_forma'] != 1 && $row['sanemsanas_datums'] == date('Y-m-d', strtotime('-2 weeks'))) {

    db_query("
      INSERT INTO `".DB_PREF."atgadinajumi` (
        liguma_id,
        atgadinajuma_veida_id,
        datums,
        tema,
        saturs,
        redzamiba,
        pardevejs_var_labot
      ) VALUES (
        ".$row['id'].",
        14,
        NOW(),
        1,
        'Nav saņemts līguma oriģināls',
        'kopejs',
        0
      )
    ");

  }

  // Ja nav visi infomedia reklāmas materiāli, tad 1 dienu pirms reklāmas sākuma atgādinājums zvanīt klientam

  if ($row['infomedia_reklamas_mat'] != 1 && $row['reklama_no'] == date('Y-m-d', strtotime('+1 day'))) {

    $pak_skaits = db_get_val(db_query("
      SELECT COUNT(*)
      FROM `".DB_PREF."pakalpojumi` p
      LEFT JOIN `".DB_PREF."pakalpojumi_veidi` v ON (p.pakalpojuma_veida_id = v.id)
      WHERE
        p.liguma_id = ".$row['id']." AND
        v.parent_id = 25 -- Infomedia
    "));

    if (!empty($pak_skaits)) {

      db_query("
        INSERT INTO `".DB_PREF."atgadinajumi` (
          liguma_id,
          atgadinajuma_veida_id,
          datums,
          tema,
          saturs,
          redzamiba,
          pardevejs_var_labot
        ) VALUES (
          ".$row['id'].",
          15,
          NOW(),
          1,
          'Nav visi Infomedia reklāmas materiāli - jāsazinās ar klientu',
          'kopejs',
          0
        )
      ");

    }

  }

  // Ja nav visi Google reklāmas materiāli, tad 1 dienu pirms reklāmas sākuma atgādinājums zvanīt klientam

  if ($row['tiessaite_reklamas_mat'] != 1 && $row['reklama_no'] == date('Y-m-d', strtotime('+1 day'))) {

    $pak_skaits = db_get_val(db_query("
      SELECT COUNT(*)
      FROM `".DB_PREF."pakalpojumi` p
      LEFT JOIN `".DB_PREF."pakalpojumi_veidi` v ON (p.pakalpojuma_veida_id = v.id)
      WHERE
        p.liguma_id = ".$row['id']." AND
        v.parent_id = 26 -- Google
    "));

    if (!empty($pak_skaits)) {

      db_query("
        INSERT INTO `".DB_PREF."atgadinajumi` (
          liguma_id,
          datums,
          tema,
          saturs,
          redzamiba,
          pardevejs_var_labot
        ) VALUES (
          ".$row['id'].",
          NOW(),
          1,
          'Nav visi Google reklāmas materiāli - jāsazinās ar klientu',
          'kopejs',
          0
        )
      ");

    }

  }

  // Reklāmas sākuma datums

  if ($row['reklama_no'] == $curdate) {

    db_query("
      INSERT INTO `".DB_PREF."atgadinajumi` (
        liguma_id,
        datums,
        tema,
        saturs,
        redzamiba
      ) VALUES (
        ".$row['id'].",
        NOW(),
        1,
        'Šodien sākas reklāma',
        'admin'
      )
    ");

  }

  // Reklāmas beigas - nākamajā dienā

  if ($row['reklama_lidz'] == date('Y-m-d', strtotime('-1 day'))) {

    db_query("
      INSERT INTO `".DB_PREF."atgadinajumi` (
        liguma_id,
        datums,
        tema,
        saturs,
        redzamiba,
        pardevejs_var_labot
      ) VALUES (
        ".$row['id'].",
        NOW(),
        1,
        'Vakar beidzās reklāma',
        'kopejs',
        0
      )
    ");

  }

  // Rēķina izrakstīšanas atgādinājums tai pašā dienā

  if (!empty($rekini[$row['id']])) {

    foreach($rekini[$row['id']] as $v) {

      if ($v['anulets'] == 0 && !empty($v['izr_datums']) && $v['izr_datums'] == $curdate) {

        db_query("
          INSERT INTO `".DB_PREF."atgadinajumi` (
            liguma_id,
            atgadinajuma_veida_id,
            datums,
            tema,
            saturs,
            redzamiba
          ) VALUES (
            ".$row['id'].",
            10,
            NOW(),
            2,
            'Šodien jāizraksta rēķins',
            'admin'
          )
        ");

      }

    }

  }

  // Šo vajadzēs vairāk kā vienā vietā
  $sodien_apm_statuss = false;

  if (!empty($rekini[$row['id']])) {
    $sodien_apm_statuss = get_apmaksas_statuss($rekini[$row['id']]);
  }

  // Šo arī vairākās vietās vajadzēs
  $nokavetas_dienas = 0;

  if (!empty($rekini[$row['id']]) && $sodien_apm_statuss == 4) { // ja ir parāds
    $nokavetas_dienas = get_max_nokavetas_dienas($rekini[$row['id']]);
  }

  // Iegūst apmaksas statusu šodienai un vakardienai. Ja atšķiras, un
  // šodien ir parāds, tad palaiž auto atgādinājuma skriptu.

  if (!empty($rekini[$row['id']])) {

    $vakard_apm_statuss = get_apmaksas_statuss($rekini[$row['id']], date("Y-m-d", strtotime('-4 days')));

    if ($vakard_apm_statuss != $sodien_apm_statuss && $sodien_apm_statuss == 4) {

      generate_auto_reminders($row, $beigu_statuss_data, $rekini[$row['id']]);

    }

  }

  // Ja ir pagājušas 10 (iepriekš 17) dienas, un ja klientam vēl aizvien ir parāds šobrīd,
  // tad pievienojam jaunu atgādinājumu, piem., "Parāds! 2. atgādinājums!".
  // Atlasa parāda atgādinājumus tikai no pēdējām 10 dienām, neatkarīgi no
  // statusa.
  // Iepriekšējo atgādinājumu izdzēšs.

  if ($nokavetas_dienas == 10) {

    $atg_query = db_query("
      SELECT
        a.*,
        v.nokl_teksts,
        v.nokl_pardevejs_var_labot
      FROM `".DB_PREF."atgadinajumi` a
      LEFT JOIN `".DB_PREF."atgadinajumi_veidi` v ON (v.id = a.atgadinajuma_veida_id)
      WHERE
        a.liguma_id = ".$row['id']." AND
        a.atgadinajuma_veida_id = 1 AND
        DATEDIFF(CURDATE(), a.created) <= 10
      ORDER BY a.id ASC
    ");

    while($atg_row = db_get_assoc($atg_query)) {

      $paradijuma_reize = $atg_row['paradijuma_reize'] + 1;

      $result = db_query("
        INSERT INTO `".DB_PREF."atgadinajumi` (
          liguma_id,
          atgadinajuma_veida_id,
          datums,
          saturs,
          paradijuma_reize,
          redzamiba,
          pardevejs_var_labot
        ) VALUES (
          ".$row['id'].",
          1,
          NOW(),
          '".esc($atg_row['nokl_teksts'] . ' (' . $paradijuma_reize .') Izsūtīt brīdinājumu par atslēgšanu!')."',
          ".$paradijuma_reize.",
          'kopejs',
          ".$atg_row['nokl_pardevejs_var_labot']."
        )
      ");

      if ($result) {

        db_query("
          DELETE FROM `".DB_PREF."atgadinajumi`
          WHERE id = ".$atg_row['id']."
          LIMIT 1
        ");

      }

    }
  }

  if ($nokavetas_dienas == 16) {

    db_query("
      INSERT INTO `".DB_PREF."atgadinajumi` (
        liguma_id,
        atgadinajuma_veida_id,
        datums,
        saturs,
        redzamiba,
        pardevejs_var_labot
      ) VALUES (
        ".$row['id'].",
        4,
        NOW(),
        '".esc($atg_veidi[4]['nokl_teksts'])."',
        'admin',
        ".$atg_veidi[4]['nokl_pardevejs_var_labot']."
      )
    ");

    db_query("
      INSERT INTO `".DB_PREF."atgadinajumi` (
        liguma_id,
        datums,
        saturs,
        redzamiba
      ) VALUES (
        ".$row['id'].",
        NOW(),
        'Izsūtīt paziņojumu par atslēgšanu no Google!',
        'admin'
      )
    ");

  }

  // Ja pagājušas 32 dienas, tad atg. par brīdinājuma vēstuli

  if ($nokavetas_dienas == 32) {

    db_query("
      INSERT INTO `".DB_PREF."atgadinajumi` (
        liguma_id,
        atgadinajuma_veida_id,
        datums,
        saturs,
        redzamiba
      ) VALUES (
        ".$row['id'].",
        8,
        NOW(),
        'Izsūtīt ierakstītu brīdinājuma vēstuli!',
        'admin'
      )
    ");

  }

  // Ja pagājušas 46 dienas, tad nodot parādu piedziņai

  if ($nokavetas_dienas == 46) {

    db_query("
      INSERT INTO `".DB_PREF."atgadinajumi` (
        liguma_id,
        atgadinajuma_veida_id,
        datums,
        saturs,
        redzamiba
      ) VALUES (
        ".$row['id'].",
        9,
        NOW(),
        'Nodot parādu piedziņai!',
        'admin'
      )
    ");

  }

  // Šis nav atgādinājums

  if (!empty($row['reklama_lidz']) && $row['reklama_lidz'] == $yesterday) {

    if (!empty($parslegs_data)) {

      // Ja līgums vakar beidzas un tam rindā ir ieraksts par pārslēgšanu,
      // tad uzstāda to kā pārslēgtu un norāda jaunā līguma nr

      $stat_data = db_get_assoc(db_query("SELECT * FROM `".DB_PREF."ligumi_beigusies` WHERE `liguma_id` = " . (int)$row['id']));

      if (!empty($stat_data)) {
        db_query("DELETE FROM `".DB_PREF."ligumi_beigusies` WHERE id = ".(int) $stat_data['id']);
      }

      $jaunais_ligums = db_get_assoc(db_query("SELECT * FROM `".DB_PREF."ligumi` WHERE `id` = " . (int)$parslegs_data['jaunais_liguma_id']));

      db_query("
        INSERT INTO `".DB_PREF."ligumi_beigusies`
        SET
          liguma_id = ".$row['id'].",
          beigu_statuss = 'parslegts',
          parslegts_lig_nr = '".$jaunais_ligums['ligumanr']."',
          auto_parslegts = 1,
          laboja_laiks = NOW(),
          pirmoreiz_apstradats = NOW()
      ");

      db_query("DELETE FROM `".DB_PREF."ligumi_parslegsanu_rinda` WHERE id = ".(int) $parslegs_data['id']);

    } else {

      // Ja nav pārslēgšanas rindas ierakstu

      // Ja līgums vakar beidzies, ir atslēgts no googles, un pārtraukts (to
      // nosaka rēķini statusi), tad pievieno attiecīgu beigu statusu 'atteikums'

      if ($row['google_statuss'] == 4) {

        if (in_array($vakard_apm_statuss, array(1, 2, 3, 4, 6))) {

          if (is_rekini_visi_apm_un_annul($rekini[$row['id']])) {

            $stat_data = db_get_assoc(db_query("SELECT * FROM `".DB_PREF."ligumi_beigusies` WHERE `liguma_id` = " . $row['id']));

            if (!empty($stat_data)) {
              db_query("DELETE FROM `".DB_PREF."ligumi_beigusies` WHERE id = ".(int) $stat_data['id']);
            }

            db_query("
              INSERT INTO `".DB_PREF."ligumi_beigusies`
              SET
                liguma_id = ".$row['id'].",
                beigu_statuss = 'atteikums',
                parslegts_lig_nr = null,
                atlikt_lidz = null,
                komentars = 'Līgums pirms reklāmas beigām tika pārtraukts',
                auto_atteikums = 1,
                laboja_laiks = NOW(),
                pirmoreiz_apstradats = NOW()
            ");

          }

        }

      }

    }

  }

  // Google statusa nomaiņa uz "atslēgts" un attiecīgo atgādinājumu pievienošana

  // Skatamies vai nav beidzies reklāmas termiņs (beigu diena vakar), ja ir,
  // tad palaiž auto atgādinājuma ģenerēšanas funkciju. Tad skatās, vai google
  // statuss ir "nav ievadīts", "ievadīts" vai "aktīvs". Ja ir, un apmaksas
  // statuss ir jebkuršs izņemot "anulēts", tad nomaina google statusu uz
  // "atslēgts". Kad tas izdarīts, palaiž vēlreiz auto atgādinājuma ģenerēšanu.

  if ($row['auto_atslegts_disable'] == 0) {

    if (!empty($row['reklama_lidz']) && $row['reklama_lidz'] == $yesterday) {

      generate_auto_reminders($row, $beigu_statuss_data, !empty($rekini[$row['id']]) ? $rekini[$row['id']] : null);

      if (in_array($row['google_statuss'], array(1, 2, 3)) && $sodien_apm_statuss != 5) {

        $sql = "
          UPDATE `".DB_PREF."ligumi`
          SET
            google_statuss = 4,
            auto_atslegts = 1
          WHERE id = ".$row['id']."
          LIMIT 1
        ";

        $result = db_query($sql);

        if ($result) {

          $row['google_statuss'] = 4;
          $row['auto_atslegts'] = 1;

          generate_auto_reminders($row, $beigu_statuss_data, !empty($rekini[$row['id']]) ? $rekini[$row['id']] : null);

        }

      }

    }

  }

}

file_put_contents('reminders_get.txt', date("d.m.Y H:i:s").' Ended'.PHP_EOL, FILE_APPEND);

?>