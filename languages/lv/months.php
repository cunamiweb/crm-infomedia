<?php
return array(
  '01' => 'janvāris',
  '02' => 'februāris',
  '03' => 'marts',
  '04' => 'aprīlis',
  '05' => 'maijs',
  '06' => 'jūnijs',
  '07' => 'jūlijs',
  '08' => 'augusts',
  '09' => 'septembris',
  '10' => 'oktobris',
  '11' => 'novembris',
  '12' => 'decembris',
);
?>