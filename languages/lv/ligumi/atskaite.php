<?php
return array(
  'klients' => 'Klients',
  'liguma_numurs' => 'Līguma numurs',
  'datumu_diapozons' => 'Datumu diapazons',
  'file_name' => 'Atskaite',
  'talrunis' => 'Tālrunis',
  'fakss' => 'Fakss',
  'epasts' => 'E-pasts',
  'web' => 'Web',
  'reklamu_kopa' => 'Reklāmu kopa',
  'seansi' => 'Seansi',
  'klikski' => 'Klikšķi',
  'vid_pozicija' => 'Vid. pozīcija',
  'google_adwords_konta_atskaite' => 'Google AdWords konta atskaite',
  'nosaukums' => 'Nosaukums',
);
?>