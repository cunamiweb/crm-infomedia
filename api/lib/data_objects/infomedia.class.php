<?php
/**
  * Infomedia web service
  */
class infomedia
{
  /**
   * Get information about adwords accounts for a specific contract
   * Returns only accounts which have adwords account number specified
   * @param string Contract number
   * @param string The date of the contract
   * @return adwordsAccount[]
   */
  public function getAdwords($number, $date)
  {
    check_auth();

    $sql = 'SELECT * FROM ' . DB_PREF . 'ligumi WHERE ligumanr = "' . esc($number) . '"';
    $contract = db_get_assoc(db_query($sql));

    if(empty($contract)) {
      throw new Exception('No contract found by this number');
    }

    if($contract['ligumadat'] != $date) {
      throw new Exception('No contract found by this date');
    }

    //  Get the keywords

    $list = array();

    $konti = array();

    $pielikumi_tiessaite = db_get_assoc_all(db_query("
      SELECT google_konts as konts
      FROM `".DB_PREF."pielikumi_tiessaite` t
    	WHERE t.liguma_id = ".$contract['id']."
    "));

    foreach($pielikumi_tiessaite as $piel) {
      if($piel['konts']) {
        $konti[] = $piel['konts'];
      }
    }

    $pielikumi_infomedia = db_get_assoc_all(db_query("
      SELECT google_konts as konts
      FROM `".DB_PREF."pielikumi_infomedia` t
    	WHERE t.liguma_id = ".$contract['id']."
    "));

    foreach($pielikumi_infomedia as $piel) {
      if($piel['konts']) {
        $konti[] = $piel['konts'];
      }
    }

    $sql = '
      SELECT *
      FROM ' . DB_PREF . 'pielikumi_kontu_linki l
      WHERE
        l.liguma_id = ' . $contract['id'] . ' AND
        l.konts IN ("' . implode('", "', $konti) . '") AND
        l.konta_numurs IS NOT NULL
    ';

    $result = db_get_assoc_all(db_query($sql));

    if(!empty($result)) {
      foreach($result as $row) {
        $account = new adwordsAccount();
        $account->account_number = $row['konta_numurs'];

        $list[] = $account;
      }
    }

    //var_dump($list);

    return $list;
  }

  /**
   * Login
   *
   * @param string Username
   * @param string Password
   * @return boolean
   */
  public function login($username, $password)
  {
    $sql = "
      SELECT *
      FROM `".DB_PREF."pardeveji`
      WHERE
				lietotajvards = '".esc($username)."' AND
				aktivs = 1
      LIMIT 1
    ";

    $user_data = db_get_assoc(db_query($sql));

    if(!empty($user_data['parole']) && $user_data['parole'] === md5($password)){

      $_SESSION['login'] = true;

      unset($user_data['parole']);

      $_SESSION['user'] = $user_data;

      log_add("atvera", "Webservisa lietotājs ielogojās");

      return true;
    } else {
      throw new Exception('Incorrect username or password');
    }
  }
}
?>