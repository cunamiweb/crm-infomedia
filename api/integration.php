<?php
session_start();
include '../inc/config.php';
include("../inc/functions.php");
include("../inc/functions_db_mysqli.php");
include("../inc/functions_validate.php");
include("../inc/functions_ajax.php");

set_include_path(get_include_path() . PATH_SEPARATOR . dirname($_SERVER['SCRIPT_FILENAME']) . '/../libs/zend/library');

set_include_path(get_include_path() . PATH_SEPARATOR . dirname($_SERVER['SCRIPT_FILENAME']) . '/../libs/PEAR');

require_once('Zend/Json.php');

//require_once dirname($_SERVER['SCRIPT_FILENAME']) . '/../libs/swiftmailer/swift_required.php';

$GLOBALS['executed_queries'] = array();
$GLOBALS['query_total_time'] = 0;

mb_internal_encoding('UTF-8');

$db = db_connect(DB_HOST, DB_USER, DB_PASS) or die("Neizdevas pieslegties datubazei!");

db_select(DB_NAME) or die("Neatradu datubaazi!");

db_query("SET NAMES utf8");

include("../inc/variables.php");

function check_auth()
{
  if(!isset($_SESSION['login']) || !$_SESSION['login']) {
    throw new Exception('Authorization error.');
  }
}
?>