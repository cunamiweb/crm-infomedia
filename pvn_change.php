<?php
die();
include 'inc/init.php';

//  Client bills
$sql = '
  SELECT * FROM ' . DB_PREF . 'rekini
  WHERE
    pvn = 22.00 AND
    izr_datums >= "2012-07-01"
';

$result = db_get_assoc_all(db_query($sql));

foreach($result as $row) {
  $new_total = round($row['summa'] * 1.21, 2);
  db_query('
    UPDATE ' . DB_PREF . 'rekini
    SET
      pvn = 21.00,
      kopsumma = ' . $new_total . '
    WHERE
      id = ' . $row['id'] . '
  ');
}
?>