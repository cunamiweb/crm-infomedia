<?
if (empty($_GET['id'])) {
  die();
}

$topic_id = (int)$_GET['id'];

require('view_process.php');

$main_data = db_get_assoc(db_query("
  SELECT
    f.*,
    p.vards as pardeveja_vards
  FROM `".DB_PREF."forums` f
  LEFT JOIN `".DB_PREF."pardeveji` p ON (p.id = f.pardeveja_id)
  WHERE f.id = ".$topic_id."
  LIMIT 1
"));

if (empty($main_data) || !empty($main_data['parent_id'])) {
  die();
}

$query = db_query("
  SELECT
    f.*,
    p.vards as pardeveja_vards
  FROM `".DB_PREF."forums` f
  LEFT JOIN `".DB_PREF."pardeveji` p ON (p.id = f.pardeveja_id)
  WHERE f.parent_id = ".$topic_id."
  ORDER BY f.created ASC
");

?>

<iframe src="" style="display:none;" id="downloadframe" name="downloadframe"></iframe>

<button style="float: right; margin-top: 5px;" onclick="$('#fullformplace').show(); window.location.hash='#fullformplace';" class="ui-state-default ui-corner-all">Pievienot atbildi</button>
<button style="float: right; margin-top: 5px; margin-right: 10px;" onclick="document.location='?c=forums'" class="ui-state-default ui-corner-all">Uz tēmu sarakstu</button>

<h1 style="font-size:20px; margin-bottom: 0; text-align: left; margin-left: 5px;" ><?= $main_data['tema'] ?></h1>

<table width="100%" cellpadding="3" cellspacing="0" id="forums_skatit" class="data">

  <thead>

    <tr>
      <th width="50">Autors</th>
      <th>Teksts</th>
      <th width="130">Pievienots</th>
    </tr>

  </thead>

  <tbody>

    <tr class="start">
      <td style="white-space: nowrap;"><?= $main_data['pardeveja_vards'] ?></td>
      <td><?= nl2br($main_data['saturs']) ?></td>
      <td><?= date('d.m.Y H:i', strtotime($main_data['created'])) ?></td>
    </tr>

  </tbody>

  <tbody class="main">

    <? while($row = db_get_assoc($query)) { ?>

      <tr id="reply-<?= $row['id'] ?>">
        <td style="white-space: nowrap;"><?= $row['pardeveja_vards'] ?></td>
        <td><?= nl2br($row['saturs']) ?></td>
        <td><?= date('d.m.Y H:i', strtotime($row['created'])) ?></td>
      </tr>

    <? } ?>

  </tbody>

</table>

<script>
stripeTable($('#forums_skatit'));
</script>

<button style="float: right;" onclick="$('#fullformplace').show(); window.location.hash='#fullformplace';" class="ui-state-default ui-corner-all">Pievienot atbildi</button>

<form action="?c=forums&a=skatit&id=<?= $topic_id ?>" method="post" id="fullformplace" style="display: none; clear: both;">

  <table width="500">

    <tr>
      <td>Atbilde:</td>
      <td><textarea rows="3" cols="40" name="saturs" id="saturs"></textarea></td>
    </tr>

    <tr>
      <td colspan="2" style="text-align: right;">
        <div style="text-align: center;" id="loader_placeholder"></div>
        <input type="hidden" name="add_reply" value="1" />
        <input style="width:150px;" type="button" onclick="return cancelSaves();" class="ui-state-default ui-corner-all" value="Atcelt">
        <input style="width:150px;" class="ui-state-default ui-corner-all" type="submit" onClick="return saveAll(this, 'forums_atbilde')" value="Pievienot">
      </td>
    </tr>

  </table>

</form>

<form action="?c=forums&a=skatit&id=<?= $topic_id ?>" method="post" style="clear: both;" enctype="multipart/form-data">

  <input type="hidden" name="add_file" value="1" />

  <? show_files_block('forums_tema', $topic_id, true) ?>

</form>