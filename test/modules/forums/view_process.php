<?php
if (!empty($_POST['add_reply'])) {

  $errors = validate_forums_atbilde_form($_POST);

  if (empty($errors)) {

    $_POST = trim_array($_POST);

    $sql = "
      INSERT INTO `".DB_PREF."forums`
      SET
        parent_id = ".$topic_id.",
        saturs = '".esc($_POST['saturs'])."',
        pardeveja_id = ".(int)$_SESSION['user']['id'].",
        last_update = NOW()
    ";

    db_query($sql);

    $reply_id = db_last_id();

    $sql = "
      UPDATE `".DB_PREF."forums`
      SET
        last_update_pardeveja_id = ".(int)$_SESSION['user']['id'].",
        last_update = NOW()
      WHERE id = ".$topic_id."
      LIMIT 1
    ";

    db_query($sql);

    header('Location: ?c=forums&a=skatit&id=' . $topic_id . '#reply-' . $reply_id);
    die();

  }

}

if (!empty($_POST['add_file'])) {

  $form_names = array(
    'forums_tema'
  );

  process_files($form_names, $topic_id);

  header('Location: ?c=forums&a=skatit&id=' . $topic_id);
  die();

}
?>