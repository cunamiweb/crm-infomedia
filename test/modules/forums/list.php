<?
require('list_process.php');

$query = db_query("
  SELECT
    f.*,
    p.vards as pardeveja_vards,
    (
      SELECT COUNT(*)
      FROM `".DB_PREF."forums`
      WHERE parent_id = f.id
    ) as atbilzu_skaits,
    p2.vards as pedejais_atbildetajs
  FROM `".DB_PREF."forums` f
  LEFT JOIN `".DB_PREF."pardeveji` p ON (p.id = f.pardeveja_id)
  LEFT JOIN `".DB_PREF."pardeveji` p2 ON (p2.id = f.last_update_pardeveja_id)
  WHERE f.parent_id IS NULL OR f.parent_id = 0
  ORDER BY f.created DESC
");

?>

<button style="float: right; margin-top: 5px;" onclick="$('#fullformplace').show(); window.location.hash='#fullformplace';" class="ui-state-default ui-corner-all">Jauna tēma</button>

<h1 style="font-size:20px; margin-bottom: 0; text-align: left; margin-left: 5px;">Forums</h1>

<table width="100%" cellpadding="3" cellspacing="0" id="forums" class="data">

  <thead>

    <tr>
      <th width="100">Pievienots</th>
      <th>Tēma</th>
      <th width="60">Atbildes</th>
      <th width="50">Autors</th>
      <th width="50">Pēdējais atbildētājs</th>
    </tr>

  </thead>

  <tbody class="main">

    <? while($row = db_get_assoc($query)) { ?>

      <tr>
        <td style="white-space: nowrap;"><?= date('d.m.Y H:i', strtotime($row['created'])) ?></td>
        <td><a style="font-size: 1.35em;" href="?c=forums&a=skatit&id=<?= $row['id'] ?>"><?= $row['tema'] ?></a></td>
        <td><?= $row['atbilzu_skaits'] ?></td>
        <td style="white-space: nowrap;"><?= $row['pardeveja_vards'] ?></td>
        <td style="white-space: nowrap;">
          <? if (!empty($row['pedejais_atbildetajs'])) { ?>
            <?= $row['pedejais_atbildetajs'] ?>, <?= date('d.m.Y H:i', strtotime($row['last_update'])) ?>
          <? } ?>
        </td>
      </tr>

    <? } ?>

  </tbody>

</table>

<script>
stripeTable($('#forums'));
</script>

<form action="?c=forums" method="post" id="fullformplace" style="display: none; clear: both;">

  <table width="500">

    <tr>
      <td>Tēma:</td>
      <td><input type="text" name="tema" id="tema" /></td>
    </tr>

    <tr>
      <td>Teksts:</td>
      <td><textarea rows="3" cols="40" name="saturs" id="saturs"></textarea></td>
    </tr>

    <tr>
      <td colspan="2" style="text-align: right;">
        <div style="text-align: center;" id="loader_placeholder"></div>
        <input style="width:150px;" type="button" onclick="return cancelSaves();" class="ui-state-default ui-corner-all" value="Atcelt">
        <input style="width:150px;" class="ui-state-default ui-corner-all" type="submit" onClick="return saveAll(this, 'forums_tema')" value="Pievienot">
      </td>
    </tr>

  </table>

</form>