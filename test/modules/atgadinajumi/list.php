<?
require('list_process.php');
?>

<form action="" method="get" id="atgadinajumi_main_filter">
  <input type="hidden" name="c" value="atgadinajumi" />
  <input type="hidden" name="main_filter" value="1" />
  <div style="float: left; text-align: left; margin-left: 10px; margin-top: 5px;">
    Rādīt tikai aktīvos:
    <span><input id="only_active" name="only_active" type="checkbox" value="1" <?= !empty($_GET['main_filter']) ? (!empty($_GET['only_active']) ? 'checked="checked"' : '') : 'checked="checked"' ?> style="margin: 0 20px 0 0;" /></span>
    Rādīt privātos:
    <span><input id="show_private" name="show_private" type="checkbox" value="1" <?= !empty($_GET['main_filter']) ? (!empty($_GET['show_private']) ? 'checked="checked"' : '') : '' ?> style="margin: 0 20px 0 0;" /></span>
  </div>
</form>

<div style="float: right; position: relative;">
  <button id="labotdzestbutton" onClick="return editoradd(this)">Labot</button>
</div>

<?

$where = array();

$show_private = false;
if (!empty($_GET['main_filter'])) {
  if (!empty($_GET['show_private'])) {
    $show_private = true;
  }
}

if (!$show_private) {
  $where[] = "a.tema != 4";
}

$sql = "
  SELECT
    a.*,
    g.nosaukums as klienta_nosaukums,
    p.vards as pardeveja_nosaukums
  FROM `".DB_PREF."atgadinajumi` a
  LEFT JOIN `".DB_PREF."ligumi` g ON (a.liguma_id = g.id)
  LEFT JOIN `".DB_PREF."pardeveji` p ON (a.pardeveja_id = p.id)
";

if (!empty($where)) {
  $sql .= "WHERE " . implode(' AND ', $where);
}

$query = db_query($sql);

$atgadinajumi = array();

while($row = db_get_assoc($query)){
  $atgadinajumi[$row['id']] = $row;
}

$skeys = array(
  0 => array(), // - aktīvie
  1 => array(), // - idle
  2 => array(), // - izpildītie
  3 => array() // - dzēstie
);

if (!empty($atgadinajumi)) {

  foreach($atgadinajumi as $atgadinajuma_id => $v) {

    $tmp_d = date("d");
    $tmp_m = date("m");
    $tmp_Y = date("Y");
    $tmp_today = $tmp_d + $tmp_m * 1000 + $tmp_Y * 100000;

    $tmp_date = explode(".", date('d.m.Y', strtotime($v['datums'])));
    $tmp_date = $tmp_date[0] + $tmp_date[1] * 1000 + $tmp_date[2] * 100000;

    if($v['statuss'] == 2) {
      $skeys[2][$atgadinajuma_id] = $tmp_date;
    } elseif ($v['statuss'] == 3) {
      $skeys[3][$atgadinajuma_id] = $tmp_date;
    } else {

      if ($tmp_today >= $tmp_date) { // aktuāli (vecs)
        $skeys[0][$atgadinajuma_id] = $tmp_date;
      } else { // nākotnes
        $skeys[1][$atgadinajuma_id] = $tmp_date;
      }

    }

  }

  $sss = array();

  foreach($skeys as $k => $v){

    arsort($v);

    $skeys[$k] = $v;

  }

  $only_active = true;

  if (!empty($_GET['main_filter'])) {
    if (!empty($_GET['only_active'])) {
      $only_active = true;
    } else {
      $only_active = false;
    }
  }

  if ($only_active) {
    $sss = $skeys[0];
  } else {
    $sss = $skeys[0] + $skeys[1] + $skeys[2] + $skeys[3];
  }

}


?>

<script>
var searchlist = [];

var sp = create_filter_field_array([
  's_datums', 's_statuss', 's_tema', 's_saturs', 's_ligums', 's_pardevejs',
  's_izveidots', 's_izveidoja', 's_izpild_dat', 's_izpildija'
]);
</script>

<? // Prototypes // ?>
<div style="display:none;">

  <div id="komentarsprot__atgadinajumi">
    <table class="comment_form">
      <tr>
        <td width="70">Komentārs:</td>
        <td>
          <textarea name="komentari_new[_atgadinajumi][komentars][]" rows="2" cols="30"></textarea>
        </td>
      </tr>
    </table>
  </div>

</div>

<table style="display:none;">

  <tr id="atgadinajumu_tabula_filter">

    <th>
      <span><input class="search_field kalendari" meth="from" searchclass="s_datums" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span><br>
      <span><input class="search_field kalendari" meth="to" searchclass="s_datums" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span>
    </th>

    <th>
      <select class="search_field advonchange" searchclass="s_statuss" meth="int">
        <option value=""></option>
        <option value="1">Neapstrādāts</option>
        <option value="2">Izpildīts</option>
        <option value="3">Atcelts</option>
      </select>
    </th>

    <th>
      <select class="search_field advonchange" searchclass="s_tema" meth="int">
        <option value=""></option>
        <?php foreach($_vars['atgadinajumu_temas'] as $k=>$p){ ?>
          <option value="<?=$k;?>"><?=$p;?></option>
        <?php } ?>
      </select>
    </th>

    <th><span><input class="search_field" meth="standart" searchclass="s_saturs" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <th><span><input class="search_field" meth="standart" searchclass="s_ligums" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <th><span><input class="search_field" meth="standart" searchclass="s_pardevejs" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>

    <th>
      <span><input class="search_field kalendari" meth="from" searchclass="s_izveidots" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span><br>
      <span><input class="search_field kalendari" meth="to" searchclass="s_izveidots" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span>
    </th>

    <th><span><input class="search_field" meth="standart" searchclass="s_izveidoja" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>

    <th>
      <span><input class="search_field kalendari" meth="from" searchclass="s_izpild_dat" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span><br>
      <span><input class="search_field kalendari" meth="to" searchclass="s_izpild_dat" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span>
    </th>

    <th><span><input class="search_field" meth="standart" searchclass="s_izpildija" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>

  </tr>

</table>

<form action="?c=atgadinajumi" id="fullformplace" method="post" enctype="multipart/form-data" class="clear">

<table width="99%" id="atgadinajumu_tabula" style="margin-top: 10px;" class="ckey_atgadinajumi_list">

  <thead>

    <tr>
      <th width="85">Datums</th>
      <th width="85">Statuss</th>
      <th width="80">Tēma</th>
      <th>Saturs</th>
      <th width="140">Līgums</th>
      <th width="140">Darbinieks</th>
      <th width="70">Izveidots</th>
      <th width="100">Izveidotājs</th>
      <th width="70">Izpildīts</th>
      <th width="100">Izpildīja</th>
    </tr>

  </thead>

  <tbody class="main">

  <? $inid = -1 ?>

  <? if(!empty($sss)) foreach($sss as $atgadinajuma_id => $v ) { ?>

    <?
    $inid ++;
    $v = $atgadinajumi[$atgadinajuma_id];
    ?>

    <tr id="tr<?=$inid;?>" class="atg_id_<?= $atgadinajuma_id ?>">

      <td><input type="input" class="showcalendar calendarcalc atgadinajumi_termimi" value="<?=!empty($v['datums'])?date('d.m.Y', strtotime($v['datums'])):date('d.m.Y');?>" name="atgadinajumi[<?= $atgadinajuma_id ?>][datums]"></td>
      <td class="status">
        <span class="link_wrap">
          <? if ($v['statuss'] == 1) { ?>
            <a onclick="return changeAtgStatus(<?= $atgadinajuma_id ?>, 2);" href="#">Izpildīts</a><br />
            <a onclick="return changeAtgStatus(<?= $atgadinajuma_id ?>, 3);" href="#">Atcelts</a>
          <? } else { ?>
            <? if ($v['statuss'] == 2) { ?>
              Izpildīts
            <? } elseif ($v['statuss'] == 3) { ?>
              Atcelts
            <? } ?>
            <a onclick="return changeAtgStatus(<?= $atgadinajuma_id ?>, 1);" href="#">X</a>
          <? } ?>
        </span>
        <input type="hidden" name="atgadinajumi[<?= $atgadinajuma_id ?>][statuss]" value="<?= $v['statuss'] ?>" />
      </td>
      <td>
        <select name="atgadinajumi[<?= $atgadinajuma_id ?>][tema]">
          <? foreach($_vars['atgadinajumu_temas'] as $temas_id => $temas_nosauk) { ?>
            <option value="<?= $temas_id ?>" <?=(!empty($v['tema']) && $v['tema'] == $temas_id)?"selected":"";?>><?= $temas_nosauk ?></option>
          <? } ?>
        </select>
      </td>
      <td>
        <textarea name="atgadinajumi[<?= $atgadinajuma_id ?>][saturs]" rows="2" cols="30"><?=!empty($v['saturs'])?$v['saturs']:"";?></textarea>
      </td>

      <? if (!empty($v['liguma_id'])) { ?>
        <td><a href="?c=ligumi&a=labot&id=<?= $v['liguma_id'] ?>"><?= $v['klienta_nosaukums'] ?></a></td>
      <? } else { ?>
        <td></td>
      <? } ?>

      <? if (!empty($v['pardeveja_id'])) { ?>
        <td><a href="?c=darbinieki&a=labot&id=<?= $v['pardeveja_id'] ?>"><?= $v['pardeveja_nosaukums'] ?></a></td>
      <? } else { ?>
        <td></td>
      <? } ?>

      <td><?=!empty($v['created'])?date('d.m.Y', strtotime($v['created'])):'';?></td>

      <? if (!empty($v['created_pardeveja_id'])) { ?>
        <td><a href="?c=darbinieki&a=labot&id=<?= $v['created_pardeveja_id'] ?>"><?=$_vars['sys_pardeveji'][$v['created_pardeveja_id']];?></a></td>
      <? } else { ?>
        <td><span class="atg_auto">auto</span></td>
      <? } ?>

      <td><?=!empty($v['apstr_laiks'])?date('d.m.Y', strtotime($v['apstr_laiks'])):'';?></td>

      <? if (!empty($v['apstr_pardeveja_id'])) { ?>
        <td><a href="?c=darbinieki&a=labot&id=<?= $v['apstr_pardeveja_id'] ?>"><?=$_vars['sys_pardeveji'][$v['apstr_pardeveja_id']];?></a></td>
      <? } else { ?>
        <td></td>
      <? } ?>

      <td style="display: none;">
      <script>
        searchlist[<?=$inid;?>] = '';
        sp['s_datums'][<?=$inid;?>] = <?= !empty($v['datums']) ? date('Ymd', strtotime($v['datums'])) : 0 ?>;
        sp['s_statuss'][<?=$inid;?>] = <?= $v['statuss'] ?>;
        sp['s_tema'][<?=$inid;?>] = <?= $v['tema'] ?>;
        sp['s_saturs'][<?=$inid;?>] = '<?=addslashes(mb_strtoupper($v['saturs']));?>';
        sp['s_ligums'][<?=$inid;?>] = '<?=addslashes(mb_strtoupper($v['klienta_nosaukums']));?>';
        sp['s_pardevejs'][<?=$inid;?>] = '<?=addslashes(mb_strtoupper($v['pardeveja_nosaukums']));?>';
        sp['s_izveidots'][<?=$inid;?>] = <?= !empty($v['created']) ? date('Ymd', strtotime($v['created'])) : 0 ?>;

        <? if (!empty($v['created_pardeveja_id'])) { ?>
          sp['s_izveidoja'][<?=$inid;?>] = '<?=addslashes(mb_strtoupper($_vars['sys_pardeveji'][$v['created_pardeveja_id']]));?>';
        <? } else { ?>
          sp['s_izveidoja'][<?=$inid;?>] = 'auto';
        <? } ?>

        sp['s_izpild_dat'][<?=$inid;?>] = <?= !empty($v['apstr_laiks']) ? date('Ymd', strtotime($v['apstr_laiks'])) : 0 ?>;

        <? if (!empty($v['apstr_pardeveja_id'])) { ?>
          sp['s_izpildija'][<?=$inid;?>] = '<?=addslashes(mb_strtoupper($_vars['sys_pardeveji'][$v['apstr_pardeveja_id']]));?>';
        <? } else { ?>
          sp['s_izpildija'][<?=$inid;?>] = '';
        <? } ?>

      </script>
      </td>

    </tr>

  <? } ?>

  </tbody>

</table>

<div style="width:400px; margin:10px;">
  <div style="background-color:#EDFBC6; width:100px; float:left;">AKTĪVS</div>
  <div style="background-color:#FBF8C6; width:100px; float:left;">NAV TERMIŅŠ</div>
  <div style="background-color:#87D97F; width:100px; float:left;">IZPILDĪTS</div>
  <div style="background-color:#D4D4D4; width:100px; float:left;">ATCELTS</div>
  <div style="clear:both;"></div>
</div>

<? show_comments_block('_atgadinajumi') ?>

<table width="600" class="hidethistoo">
  <tr><td colspan="2" align="center"><br><input class="ui-state-default ui-corner-all submit" type="submit" onClick="return saveAll(this, 'atgadinajumi')" value="Saglabāt!"></td></tr>
  <tr><td colspan="2" align="center"><br><input style="width:200px;" type="button" onclick="return cancelSaves();" class="ui-state-default ui-corner-all" value="Atcelt"></td></tr>
</table>

</form>

<script>
$(window).load(function() {

  var table = $("#atgadinajumu_tabula");
  var head = $("#atgadinajumu_tabula_filter");

  var cache_key = getCacheKeyFromClass(table);

  var sortListFromCache = cache_key ? $.jStorage.get(cache_key + '_sort', []) : [];

  table.tablesorter({
    debug: false,
    highlightClass: 'highlight',
    sortList: sortListFromCache,
    headers: {
      0: { sorter:'latviandate' },
      3: { sorter:'latviantext' },
      4: { sorter:'latviantext' },
      5: { sorter:'latviantext' },
      6: { sorter:'latviandate' },
      7: { sorter:'latviantext' },
      8: { sorter:'latviandate' },
      9: { sorter:'latviantext' }
    },
    textExtraction: function(node) {

      var node = $(node);
      var input = node.find(':input');

      if (input.length > 0) {
        return input.val();
      } else {
        return $(node).text();
      }

    }
  }).bind('sortStart', function() {
    table.trigger("update");
  }).bind('sortEnd', function() {

    if (cache_key) $.jStorage.set(cache_key + '_sort', table.get(0).config.sortList);

    stripeTable(table);

  });

  initTableFilter(table, head, processTable, '', searchlist, sp);

});

function processTable() {
}

$('.calendarcalc').change(refreshAtgStatuses);

refreshAtgStatuses();

$(".showcalendar").datepicker({dateFormat:'dd.mm.yy'});

$("form#atgadinajumi_main_filter").each(function() {

  var form = $(this);

  $('input#only_active, input#show_private', form).change(function() {
    form.submit();
  });

});

editordisable();
</script>