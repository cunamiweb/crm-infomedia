<?php
if (!empty($_POST)) {

  $errors = validate_atgadinajumi_form($_POST);

  if (empty($errors)) {

    $_POST = trim_array($_POST);

    // updating reminders

    if (!empty($_POST['atgadinajumi'])) {

      foreach($_POST['atgadinajumi'] as $atgadinajuma_id => $a) {

        $sql = "
          UPDATE `".DB_PREF."atgadinajumi`
          SET
            datums = '".esc(date('Y-m-d', strtotime($a['datums'])))."',
            tema = ".(int)$a['tema'].",
            saturs = '".esc($a['saturs'])."'
          WHERE id = ".$atgadinajuma_id."
          LIMIT 1
        ";

        db_query($sql);

      }

      // inserting comments

      process_comments(array('_atgadinajumi'));

    }

    header('Location: ?c=atgadinajumi');
    die();

  }

}
?>