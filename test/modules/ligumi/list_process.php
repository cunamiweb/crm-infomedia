<?php
if (!empty($_POST)) {

  $errors = validate_lig_kopsavilkums_form($_POST);

  if (empty($errors)) {

    $_POST = trim_array($_POST);

    if (is_admin()) {

      // inserting comments

      process_comments(array('_lig_kopsavilkums'));

    }

    header('Location: ?c=ligumi');
    die();

  }

}
?>