<?php
$subtab = !empty($_GET['subtab']) ? (int)$_GET['subtab'] : 0;

if (is_admin()) {
  require('new_process.php');
}

if (!empty($_GET['id'])){

  $query = db_query("SELECT * FROM `".DB_PREF."ligumi` WHERE `id` = ".(int)$_GET['id']);
  $d = db_get_assoc($query);

  if (!is_admin() && $d['pardeveja_id'] != $_SESSION['user']['id']) {
    die('Nav tiesību apskatīt līgumu!');
  }

  $query = db_query("
    SELECT
      *,
      (
        SELECT datums
        FROM `".DB_PREF."rekini_maksajumi`
        WHERE rekina_id = r.id
        ORDER BY datums DESC
        LIMIT 1
      ) as pedejais_apm_datums
    FROM `".DB_PREF."rekini` r
    WHERE r.`liguma_id` = ".(int)$_GET['id'] . "
    ORDER BY r.id ASC");
  $r = array();

  while($row = db_get_assoc($query)) {
    $r[] = $row;
  }

  $query = db_query("SELECT * FROM `".DB_PREF."ligumi_rekinu_piegades` WHERE `liguma_id` = ".(int)$_GET['id']);
  $piegades = array();

  while($row = db_get_assoc($query)) {
    $piegades[$row['piegades_veids']] = $row;
  }

  $query = db_query("SELECT * FROM `".DB_PREF."kontakti` WHERE `liguma_id` = ".(int)$_GET['id']);
  $kontakti = array();

  while($row = db_get_assoc($query)){
    $kontakti[] = $row;
  }

  $query = db_query("SELECT * FROM `".DB_PREF."pakalpojumi` WHERE `liguma_id` = ".(int)$_GET['id']);
  $pakalpojumi = array();

  while($row = db_get_assoc($query)){
    $pakalpojumi[] = $row;
  }

  $query = db_query("SELECT * FROM `".DB_PREF."pielikumi_infomedia` WHERE `liguma_id` = ".(int)$_GET['id'] . " ORDER BY id ASC");
  $pielikumi_infomedia = array();

  while($row = db_get_assoc($query)){
    $pielikumi_infomedia[] = $row;
  }

  $query = db_query("SELECT * FROM `".DB_PREF."pielikumi_tiessaite` WHERE `liguma_id` = ".(int)$_GET['id'] . " ORDER BY id ASC");
  $pielikumi_tiessaite = array();

  while($row = db_get_assoc($query)){
    $pielikumi_tiessaite[] = $row;
  }

  $query = db_query("SELECT * FROM `".DB_PREF."atgadinajumi` WHERE `liguma_id` = ".(int)$_GET['id']);
  $atgadinajumi = array();

  while($row = db_get_assoc($query)){
    $atgadinajumi[$row['id']] = $row;
  }

} else {

  $d = array();
  $d['pvn'] = "21";
  $d['soda_proc'] = "0.5";

  $r = array();
  $piegades = array();
  $kontakti = array();
  $pakalpojumi = array();
  $pielikumi_infomedia = array();
  $pielikumi_tiessaite = array();
  $atgadinajumi = array();

}
?>
<iframe src="" style="display:none;" id="downloadframe" name="downloadframe"></iframe>

<? show_autodownload_iframes() ?>

<? // Prototypes // ?>
<div style="display:none;">

  <div id="kontprot">
    <div>
      <table width="100%">
        <tr><td>Vārds:</td><td><input type="input" name="kontakti_new[vards][]" value=""></td></tr>
        <tr><td>Tālrunis:</td><td><input type="input" name="kontakti_new[telefons][]" value=""></td></tr>
        <tr><td>E-pasts:</td><td><input class="readthismail" type="input" name="kontakti_new[epasts][]" value=""></td></tr>
        <tr><td>Līguma slēdzējs:</td><td><input class="ligmasledzejcheck" type="checkbox" name="kontakti_new[ligumsledzejs][]"></td></tr>
      </table>
      <button onClick="$(this).parent().remove(); return false;">Dzēst šo kontaktu</button>
    </div>
  </div>

  <div id="piel_infomedia_prot">
    <div style="padding-top: 5px; clear: both;">
      <input type="input" name="piel_infomedia_new[atslegvards][]" style="width:90%; float: left; margin-right: 5px;">
      <a href="javascript:;" onClick="$(this).parent().remove();" style="float: left;">[x]</a>
    </div>
  </div>

  <div id="piel_tiessaite_prot">
    <div style="padding-top: 5px; clear: both;">
      <input type="input" name="piel_tiessaite_new[atslegvards][]" style="width:65%; float: left; margin-right: 5px;">
      <input type="input" name="piel_tiessaite_new[domeni][]" style="width:25%; float: left; margin-right: 5px;">
      <a href="javascript:;" onClick="$(this).parent().remove();" style="float: left;">[x]</a>
    </div>
  </div>

  <div id="pakalpojums_prot">
    <div>
      <select name="pakalpojumi_new[pakalpojuma_veida_id][]">
        <?php foreach($_vars['pakalpojumi'] as $k=>$p){ ?>
          <option value="<?=$k;?>"><?=$p;?></option>
        <?php } ?>
      </select>
      <input type="input" name="pakalpojumi_new[pakalpojuma_apr][]" style="width:100px;">
      <button onclick="$(this).parent().remove();">Dzēst</button>
    </div>
  </div>

  <table id="atgadinajumsprot">

    <tbody>

      <tr>
        <td><input type="input" class="showcalendarnew atgadinajumi_termimi" value="<?=date('d.m.Y');?>" name="atgadinajumi_new[datums][]"></td>
        <td class="status">Jauns</td>
        <td>
          <select name="atgadinajumi_new[tema][]">
            <? foreach($_vars['atgadinajumu_temas'] as $temas_id => $temas_nosauk) { ?>
              <option value="<?= $temas_id ?>"><?= $temas_nosauk ?></option>
            <? } ?>
          </select>
        </td>
        <td><textarea name="atgadinajumi_new[saturs][]" rows="2" cols="30"></textarea></td>
        <td><?=date('d.m.Y');?></td>
        <td><?=$_vars['sys_pardeveji'][$_SESSION['user']['id']];?></td>
        <td></td>
        <td></td>
      </tr>

    </tbody>

  </table>

  <? $form_names = array('lig_klients', 'lig_pakalpojums', 'lig_rekini', 'lig_atgadinajumi') ?>

  <? foreach($form_names as $form_name) { ?>

    <?
    $fields_query = db_query("SELECT * FROM `".DB_PREF."formu_lauki` WHERE forma = '".esc($form_name)."' ORDER BY id ASC");
    $fields = array();

    while($row = db_get_assoc($fields_query)) {
      $fields[] = $row;
    }
    ?>

    <div id="komentarsprot_<?= $form_name ?>">
      <table class="comment_form">
        <tr>
          <td width="70">Komentārs:</td>
          <td>
            <textarea name="komentari_new[<?= $form_name ?>][komentars][]" rows="2" cols="30"></textarea>
          </td>
        </tr>
        <? if (!empty($fields)) { ?>
        <tr>
          <td>Lauks:</td>
          <td>
            <select name="komentari_new[<?= $form_name ?>][formas_lauka_id][]">
              <option value="">Nepiesaistīts laukam</option>
              <? foreach($fields as $row) { ?>
                <option value="<?= $row['id'] ?>"><?= $row['nosaukums'] ?></option>
              <? } ?>
            </select>
          </td>
        </tr>
        <? } ?>
      </table>
    </div>

  <? } ?>

</div>

<? // Header // ?>
<a href="javascript:;" class="tabulation" active="0" open="form_klients">Līgums</a> |
<a href="javascript:;" class="tabulation" active="1" open="form_pakalpojums">Pakalpojums</a> |
<a href="javascript:;" class="tabulation" active="2" open="form_rekini">Rēķini</a> |
<a href="javascript:;" class="tabulation" active="3" open="form_atgadinajumi">Atgādinājumi</a>

<? if (is_admin()) { ?>

  <div style="padding:10px;">
    <center><button id="labotdzestbutton" onClick="return editoradd(this)">Labot/papildināt</button></center>
  </div>

<? } ?>

<h1 style="font-size:20px; height: 22px;" id="uznemuma_nosaukums"><?=!empty($d['nosaukums'])?$d['nosaukums']:""?></h1>

<form action="?c=ligumi&a=labot&id=<?= !empty($_GET['id']) ? '&a=labot&id=' . (int) $_GET['id'] : '&a=jauns' ;?>" id="fullformplace" method="post" enctype="multipart/form-data">

  <div style="display:<?=($subtab==2?"block":"none");?>;" id="form_rekini">
    <? include("forms/rekini.php"); ?>
  </div>

  <div style="display:<?=($subtab==1?"block":"none");?>;" id="form_pakalpojums">
    <? include("forms/pakalpojums.php"); ?>
  </div>

  <div style="display:<?=($subtab==0?"block":"none");?>;" id="form_klients">
    <? include("forms/klients.php"); ?>
  </div>

  <div style="display:<?=($subtab==3?"block":"none");?>;" id="form_atgadinajumi">
    <? include("forms/atgadinajumi.php"); ?>
  </div>

  
  <table width="600" class="hidethistoo">
    <tr><td colspan="2" align="center"><br><input class="ui-state-default ui-corner-all submit" type="submit" onClick="return saveAll(this, 'ligums')" value="Saglabāt!"></td></tr>
    <tr><td colspan="2" align="center"><br><input style="width:200px;" type="button" onclick="return cancelSaves();" class="ui-state-default ui-corner-all" value="Atcelt"></td></tr>
    <? if(!empty($_GET['id'])){ ?>
      <tr><td colspan="2" align="center"><br><input style="width:200px;" type="button" onclick="return deleteClient()" class="ui-state-default ui-corner-all" value="Dzēst šo klientu"></td></tr>
    <? } ?>
  </table>

</form>

<script>

var activeone = <?= $subtab ?>;

$(function() {

  $('.tabulation').click(function(){

    $('#form_klients').hide();
    $('#form_pakalpojums').hide();
    $('#form_rekini').hide();
    $('#form_atgadinajumi').hide();
    $('#'+$(this).attr('open')).show();

    recreateRekiniNr();

    activeone = $(this).attr('active');

  });

  $('#nosaukumsplace').keyup(function(){
    $('#uznemuma_nosaukums').html($(this).val());
  });

});

function copyFaktToJur() {

  $('#juradd1').val($('#add1').val());
  $('#juradd2').val($('#add2').val());
  $('#juradd3').val($('#add3').val());

  return false;

}

function copyJurToFakt() {

  $('#add1').val($('#juradd1').val());
  $('#add2').val($('#juradd2').val());
  $('#add3').val($('#juradd3').val());

  return false;

}

function deleteClient(){

  if (confirm('Vai Jūs tiešām vēlaties dzēst šo klientu?')) {
    document.location = './?deleteClient=<?=!empty($_GET['id'])?(int)$_GET['id']:0;?>';
  }

  return false;

}

function activateTab(tab_name) {

  $('#form_klients').hide();
  $('#form_pakalpojums').hide();
  $('#form_rekini').hide();
  $('#form_atgadinajumi').hide();

  $('#form_' + tab_name).show();

  activeone = $('a[open=form_' + tab_name + ']').attr('active');

}

$("#ligumadat").datepicker({dateFormat:'dd.mm.yy'});
$("#rekno").datepicker({dateFormat:'dd.mm.yy'});
$("#reklidz").datepicker({dateFormat:'dd.mm.yy'});
$("#sanemsanas_datums").datepicker({dateFormat:'dd.mm.yy'});

editordisable();

<? if(empty($_GET['id'])) { ?>
  editoradd($('#labotdzestbutton'));
<? } ?>

<? if(empty($_GET['id'])) { ?>

$('input[name=regnr]').keyup(function() {
  $('input[name=pvnnr]').val('LV' + $(this).val());
});

<? } ?>

</script>