<?
require('list_process.php');

$active_page = !empty($_GET['page']) ? (int)$_GET['page'] : 1;

if (!isset($_GET['show_in_page'])) {
  $show_in_page = 30;
} elseif ($_GET['show_in_page'] > 0) {
  $show_in_page = (int)$_GET['show_in_page'];
} else {
  $show_in_page = '';
}

$order_bys = array(
  'nosaukums' => 'g.nosaukums',
  'indekss' => 'g.faktaddr_index',
  'darbinieks' => 'p.vards',
  'lig_statuss' => '',
  'kopapm_statuss' => '',
  'google_statuss' => 'g.google_statuss',
  'lig_dat' => 'g.ligumadat',
  'rekl_dat_no' => 'g.reklama_no',
  'rekl_dat_lidz' => 'g.reklama_lidz',
  'sanems_forma' => 'g.sanemsanas_forma',
  'pakalpojums' => '',
  'infomedia_statuss' => 'g.infomedia_statuss',
  'google_konts' => 'g.pak_google_konts',
  'google_atslegv' => '',
  'lig_summa' => 'liguma_summa_bez_pvn',
  'rek_izr_dat' => 'last_rek_izr_datums',
  'akt_atgad' => 'aktivi_atgadinajumi'
);

$order = '';
$direction = 'asc';

if (!empty($_GET['order']) && isset($order_bys[$_GET['order']])) {
  $order = $order_bys[$_GET['order']];
}

if (!empty($_GET['direction']) && in_array(strtolower($_GET['direction']), array('asc', 'desc'))) {
  $direction = strtolower($_GET['direction']);
}

$query = db_query("
  SELECT
    *,
    IF (izr_datums IS NOT NULL,
      IF(anulets = 1,
        6, -- anulets
        IF (nodots_piedz = 1,
          7, -- nodots piedzinai
          IF(apm_termins IS NOT NULL,
            IF(kopsumma <= apm_summa,
              2, -- apmaksats
              IF (apm_summa IS NULL OR apm_summa = 0,
                IF(CURDATE() > apm_termins,
                  3, -- parads
                  4  -- nav termins
                ),
                IF(CURDATE() > apm_termins,
                  3, -- parads
                  5  -- daleji apmaksats
                )
              )
            ),
            0
          )
        )
      ),
      0
    ) AS apm_statuss
  FROM `".DB_PREF."rekini`
  ORDER BY izr_datums DESC
");

$rekini = array();

while($row = db_get_assoc($query)){
  $rekini[$row['liguma_id']][] = $row;
}


$query = db_query("
  SELECT
    p.*,
    v.nosaukums
  FROM `".DB_PREF."pakalpojumi` p
  LEFT JOIN `".DB_PREF."pakalpojumi_veidi` v ON (p.pakalpojuma_veida_id = v.id)
");

$pakalpojumi = array();

while($row = db_get_assoc($query)){
  $pakalpojumi[$row['liguma_id']][] = $row;
}

$query = db_query("
  SELECT *
  FROM `".DB_PREF."pielikumi_tiessaite` t
");

$pielikumi_tiessaite = array();

while($row = db_get_assoc($query)){
  $pielikumi_tiessaite[$row['liguma_id']][] = $row;
}

$query = db_query("
  SELECT *
  FROM `".DB_PREF."pielikumi_infomedia`
");

$pielikumi_infomedia = array();

while($row = db_get_assoc($query)){
  $pielikumi_infomedia[$row['liguma_id']][] = $row;
}

$where = array();
$having = array();

if (getGet('fsearch') !== '') $where[] = '
  g.nosaukums LIKE "%'.getGet('fsearch').'%" OR
  g.ligumanr LIKE "%'.getGet('fsearch').'%" OR
  g.regnr LIKE "%'.getGet('fsearch').'%"
';

if (getGet('fnosaukums') !== '') $where[] = 'g.nosaukums LIKE "%'.getGet('fnosaukums').'%"';
if (getGet('ffaktaddr_index') !== '') $where[] = 'g.faktaddr_index LIKE "%'.getGet('ffaktaddr_index').'%"';
if (getGet('fpardevejs') !== '') $where[] = 'p.vards LIKE "%'.getGet('fpardevejs').'%"';
if (getGet('fgoogle_statuss') !== '') $where[] = 'g.google_statuss = '.(int)getGet('fgoogle_statuss');
if (getGet('flig_no') !== '') $where[] = 'g.ligumadat >= "'.to_db_date(getGet('flig_no')).'"';
if (getGet('flig_lidz') !== '') $where[] = 'g.ligumadat <= "'.to_db_date(getGet('flig_lidz')).'"';
if (getGet('frekl_sak_no') !== '') $where[] = 'g.reklama_no >= "'.to_db_date(getGet('frekl_sak_no')).'"';
if (getGet('frekl_sak_lidz') !== '') $where[] = 'g.reklama_no <= "'.to_db_date(getGet('frekl_sak_lidz')).'"';
if (getGet('frekl_beig_no') !== '') $where[] = 'g.reklama_lidz >= "'.to_db_date(getGet('frekl_beig_no')).'"';
if (getGet('frekl_beig_lidz') !== '') $where[] = 'g.reklama_lidz <= "'.to_db_date(getGet('frekl_beig_lidz')).'"';
if (getGet('fsanems_forma') !== '') $where[] = 'g.sanemsanas_forma = '.(int)getGet('fsanems_forma');
if (getGet('fpakalpojums') !== '') $where[] = '
  g.id IN (SELECT liguma_id FROM `'.DB_PREF.'pakalpojumi` WHERE pakalpojuma_veida_id = '.(int)getGet('fpakalpojums').')
';
if (getGet('finfomedia_statuss') !== '') $where[] = 'g.infomedia_statuss = '.(int)getGet('finfomedia_statuss');
if (getGet('fgoogle_konts') !== '') $where[] = 'g.pak_google_konts LIKE "%'.getGet('fgoogle_konts').'%"';
if (getGet('fgoogle_atslegv') !== '') $where[] = '
  g.id IN (SELECT liguma_id FROM `'.DB_PREF.'pielikumi_tiessaite` WHERE atslegvards LIKE "%'.getGet('fgoogle_atslegv').'%") OR
  g.id IN (SELECT liguma_id FROM `'.DB_PREF.'pielikumi_infomedia` WHERE atslegvards LIKE "%'.getGet('fgoogle_atslegv').'%")
';
if (getGet('flig_summa') !== '') $having[] = 'FLOOR(liguma_summa_bez_pvn) = '.(int)getGet('flig_summa');

if (getGet('fakt_atgad') == 1) $having[] = 'aktivi_atgadinajumi > 0';
if (getGet('fakt_atgad') == 2) $having[] = 'aktivi_atgadinajumi = 0';

$sql = "
  SELECT
    g.*,
    b.beigu_statuss,
    b.atlikt_lidz as beigu_statuss_atlikt_lidz,
    (
      SELECT SUM(summa)
      FROM `".DB_PREF."rekini`
      WHERE liguma_id = g.id
    ) as liguma_summa_bez_pvn,
    (
      SELECT COUNT(*)
      FROM `".DB_PREF."atgadinajumi`
      WHERE
        liguma_id = g.id AND
        statuss = 1 AND
        datums <= CURDATE()
    ) as aktivi_atgadinajumi,
    (
      SELECT izr_datums
      FROM `".DB_PREF."rekini`
      WHERE liguma_id = g.id
      ORDER BY izr_datums DESC
      LIMIT 1
    ) as last_rek_izr_datums
  FROM `".DB_PREF."ligumi` g
  LEFT JOIN `".DB_PREF."pardeveji` p ON (g.pardeveja_id = p.id)
  LEFT JOIN `".DB_PREF."ligumi_beigusies` b ON (g.id = b.liguma_id)
";

if (!empty($where)) {
  $sql .= " WHERE " . implode(' AND ', $where);
}

if (!empty($having)) {
  $sql .= " HAVING " . implode(' AND ', $having);
}


$sql .= " ORDER BY " . (!empty($order) ? $order . " " . $direction : 'g.`ligumadat` DESC, g.`ligumanr` DESC');

$query = db_query($sql);

$alldata = array();

// apply filtering to complex columns

while($row = db_get_assoc($query)){

  $row['liguma_statuss'] = get_liguma_statuss($row, !empty($rekini[$row['id']]) ? $rekini[$row['id']] : null);

  if (getGet('flig_statuss') !== '') {

    if (getGet('flig_statuss') == '6_termins') {
      if ($row['liguma_statuss'] != 6 || (!empty($row['beigu_statuss']) && $row['beigu_statuss'] != 'termins')) continue;
    } else if (getGet('flig_statuss') == '6_parslegts') {
      if ($row['liguma_statuss'] != 6 || $row['beigu_statuss'] != 'parslegts') continue;
    } else if (getGet('flig_statuss') == '6_atteikums') {
      if ($row['liguma_statuss'] != 6 || $row['beigu_statuss'] != 'atteikums') continue;
    } else {
      if ($row['liguma_statuss'] != getGet('flig_statuss')) continue;
    }

  }


  $row['norekinu_statuss'] = get_apmaksas_statuss(!empty($rekini[$row['id']]) ? $rekini[$row['id']] : null);

  if (getGet('fkopapm_statuss') !== '') {

    if (getGet('fkopapm_statuss') == '2_barteris') {
      if ($row['norekinu_statuss'] != 2 || !is_barteris(!empty($rekini[$row['id']]) ? $rekini[$row['id']] : null)) continue;
    } else {
      if ($row['norekinu_statuss'] != getGet('fkopapm_statuss')) continue;
    }

  }


  $rekini_to_show = array();

  if (!empty($rekini[$row['id']])) {

    foreach($rekini[$row['id']] as $rekins) {

      if (getGet('frek_izr_no') !== '' && $rekins['izr_datums'] < to_db_date(getGet('frek_izr_no'))) continue;
      if (getGet('frek_izr_lidz') !== '' && $rekins['izr_datums'] > to_db_date(getGet('frek_izr_lidz'))) continue;

      if (getGet('frek_summa') !== '' && floor($rekins['kopsumma']) != (int)getGet('frek_summa')) continue;

      if (getGet('frek_statuss') !== '' && $rekins['statuss'] != getGet('frek_statuss')) continue;

      if (getGet('frek_apm_statuss') !== '') {

        if (getGet('frek_apm_statuss') == '2_barteris') {
          if ($rekins['apm_statuss'] != 2 || empty($rekins['barteris'])) continue;
        } else {
          if ($rekins['apm_statuss'] != getGet('frek_apm_statuss')) continue;
        }

      }

      $rekini_to_show[] = $rekins;

    }

  }

  // if contract has no bills to show, but bill filters are in efect, then dont show contract
  if (empty($rekini_to_show) && (getGet('frek_izr_no') !== '' || getGet('frek_izr_lidz') !== '' || getGet('frek_summa') !== '' || getGet('frek_statuss') !== '' || getGet('frek_apm_statuss') !== '')) continue;

  $row['rekini'] = $rekini_to_show;

  $row['pakalpojumu_saraksts'] = '';

  if (!empty($pakalpojumi[$row['id']])) {

    $pak_saraksts = array();
    foreach($pakalpojumi[$row['id']] as $v) {
      $pak_saraksts[] = $v['nosaukums'] . (!empty($v['pakalpojuma_apr']) ? "(".$v['pakalpojuma_apr'].")" : "");
    }
    $row['pakalpojumu_saraksts'] = implode(', ', $pak_saraksts);

  }

  $row['google_atslegv_saraksts'] = '';

  if (!empty($pielikumi_tiessaite[$row['id']]) || !empty($pielikumi_infomedia[$row['id']])) {

    $atslv = array();

    if (!empty($pielikumi_infomedia[$row['id']])) {
      foreach($pielikumi_infomedia[$row['id']] as $v) {
        $atslv[] = $v['atslegvards'];
      }
    }

    if (!empty($pielikumi_tiessaite[$row['id']])) {
      foreach($pielikumi_tiessaite[$row['id']] as $v) {
        $atslv[] = $v['atslegvards'] . (!empty($v['domeni']) ? ' ('.$v['domeni'].')' : '');
      }
    }

    $row['google_atslegv_saraksts'] = implode(', ', $atslv);

  }

  $alldata[] = $row;

}

$total_rows = count($alldata);


// apply sorting to complex columns

if (getGet('order') == 'lig_statuss') {

  function sort_by_lig_statuss_asc($a, $b) {
    return $a['liguma_statuss'] == $b['liguma_statuss'] ? 0 : (($a['liguma_statuss'] < $b['liguma_statuss']) ? -1 : 1);
  }

  function sort_by_lig_statuss_desc($a, $b) {
    return $a['liguma_statuss'] == $b['liguma_statuss'] ? 0 : (($a['liguma_statuss'] > $b['liguma_statuss']) ? -1 : 1);
  }

  usort($alldata, 'sort_by_lig_statuss_' . $direction);

}

if (getGet('order') == 'kopapm_statuss') {

  function sort_by_kopapm_statuss_asc($a, $b) {
    return $a['norekinu_statuss'] == $b['norekinu_statuss'] ? 0 : (($a['norekinu_statuss'] < $b['norekinu_statuss']) ? -1 : 1);
  }

  function sort_by_kopapm_statuss_desc($a, $b) {
    return $a['norekinu_statuss'] == $b['norekinu_statuss'] ? 0 : (($a['norekinu_statuss'] > $b['norekinu_statuss']) ? -1 : 1);
  }

  usort($alldata, 'sort_by_kopapm_statuss_' . $direction);

}

if (getGet('order') == 'pakalpojums') {

  function sort_by_pakalpojums_asc($a, $b) {
    return strncasecmp($a['pakalpojumu_saraksts'], $b['pakalpojumu_saraksts'], 20);
  }

  function sort_by_pakalpojums_desc($a, $b) {
    return strncasecmp($b['pakalpojumu_saraksts'], $a['pakalpojumu_saraksts'], 20);
  }

  usort($alldata, 'sort_by_pakalpojums_' . $direction);

}

if (getGet('order') == 'google_atslegv') {

  function sort_by_google_atslegv_asc($a, $b) {
    return strncasecmp($a['google_atslegv_saraksts'], $b['google_atslegv_saraksts'], 20);
  }

  function sort_by_google_atslegv_desc($a, $b) {
    return strncasecmp($b['google_atslegv_saraksts'], $a['google_atslegv_saraksts'], 20);
  }

  usort($alldata, 'sort_by_google_atslegv_' . $direction);

}

// limit rows to show

if ($show_in_page > 0) {
  $alldata = array_slice($alldata, ($active_page - 1) * $show_in_page, $show_in_page);
}

$visible_rows = count($alldata);

?>

<form action="" id="fullformplace" method="GET" style="clear: both;">

<input type="hidden" name="c" value="ligumi" />

<input type="hidden" name="order" value="<?= getGet('order') ?>" />
<input type="hidden" name="direction" value="<?= getGet('direction') ?>" />

<input type="hidden" name="page" value="<?= $active_page ?>" />
<input type="hidden" name="show_in_page" value="<?= $show_in_page ?>" />

<input name="fsearch" type="hidden" style="width: 150px;" value="<?= getGet('fsearch') ?>">

<div style="float: left; width: 300px; text-indent: -9999px;"> <input class="ui-state-default ui-corner-all" style="width: 5em;" type="submit" value="Atlasīt" /> </div>

<div style="float: right; width: 300px;">
  <button style="float: right;" onclick="document.location='?c=ligumi&a=jauns'; return false;" class="ui-state-default ui-corner-all">Pievienot jaunu līgumu</button>
</div>

<?php print_paginator($total_rows, $active_page, $show_in_page) ?>

<script>
var searchlist = [];

var sp = create_filter_field_array([
  's_googlestatuss', 's_ilgums', 's_summa'
]);
</script>

<table width="100%" cellpadding="3" cellspacing="0" class="data" id="ligumu_kopsavilkums">

  <thead>

    <tr class="header">

      <th width="30">Npk</th>
      <? print_th('Nosaukums', 'nosaukums', array('width' => 50)) ?>
      <? print_th('Indekss', 'indekss', array('width' => 75)) ?>
      <? print_th('Darbinieks', 'darbinieks') ?>
      <? print_th('Līguma statuss', 'lig_statuss') ?>
      <? print_th('Norēķinu statuss', 'kopapm_statuss') ?>
      <? print_th('Google statuss', 'google_statuss') ?>
      <? print_th('Līguma datums', 'lig_dat') ?>
      <? print_th('Līgums no', 'rekl_dat_no') ?>
      <? print_th('Līgums līdz', 'rekl_dat_lidz') ?>
      <? print_th('Saņemšanas forma', 'sanems_forma') ?>
      <? print_th('Pakalpojums', 'pakalpojums') ?>
      <? print_th('Infomedia statuss', 'infomedia_statuss') ?>
      <? print_th('Google konts', 'google_konts') ?>
      <? print_th('Google atslēgv.', 'google_atslegv') ?>
      <? print_th('Līguma summa (bez PVN)', 'lig_summa') ?>
      <? print_th('Rēķina izr.datums', 'rek_izr_dat') ?>
      <? print_th('Rēķina summa') ?>
      <? print_th('Rēķina statuss') ?>
      <? print_th('Apmaksas statuss') ?>
      <? print_th('Aktīvie atgādinājumi', 'akt_atgad') ?>

    </tr>

    <tr class="filter">

      <th></th>
      <th><span><input name="fnosaukums" class="sfield" type="input" value="<?= getGet('fnosaukums') ?>" style="width:80%;"><a class="sfield_clear" href="#">x</a></span></th>
      <th><span><input name="ffaktaddr_index" class="sfield" type="input" value="<?= getGet('ffaktaddr_index') ?>" style="width:80%;"><a class="sfield_clear" href="#">x</a></span></th>
      <th><span><input name="fpardevejs" class="sfield" type="input" value="<?= getGet('fpardevejs') ?>" style="width:80%;"><a class="sfield_clear" href="#">x</a></span></th>

      <th>
        <select name="flig_statuss" class="sfield">
          <option value=""></option>
          <?php foreach($_vars['liguma_statuss'] as $k=>$p){ ?>
            <option <?= (getGet('flig_statuss') == (string)$k) ? 'selected="selected"' : '' ?> value="<?=$k;?>"><?=$p;?></option>
            <? if ($k == 6) { ?>
              <option <?= (getGet('flig_statuss') == '6_termins') ? 'selected="selected"' : '' ?> value="6_termins">&nbsp;- <?= $_vars['liguma_beigu_statuss']['termins'] ?></option>
              <option <?= (getGet('flig_statuss') == '6_parslegts') ? 'selected="selected"' : '' ?> value="6_parslegts">&nbsp;- <?= $_vars['liguma_beigu_statuss']['parslegts'] ?></option>
              <option <?= (getGet('flig_statuss') == '6_atteikums') ? 'selected="selected"' : '' ?> value="6_atteikums">&nbsp;- <?= $_vars['liguma_beigu_statuss']['atteikums'] ?></option>
            <? } ?>
          <?php } ?>
        </select>
      </th>

      <th>
        <select name="fkopapm_statuss" class="sfield">
          <option value=""></option>
          <?php foreach($_vars['kopapmaksas_statuss'] as $k=>$p){ ?>
            <option <?= (getGet('fkopapm_statuss') == (string)$k) ? 'selected="selected"' : '' ?> value="<?=$k;?>"><?=$p;?></option>
            <? if ($k == 2) { ?>
              <option <?= (getGet('fkopapm_statuss') == '2_barteris') ? 'selected="selected"' : '' ?> value="2_barteris">&nbsp;- Barteris</option>
            <? } ?>
          <?php } ?>
        </select>
      </th>

      <th>
        <select name="fgoogle_statuss" class="sfield">
          <option value=""></option>
          <?php foreach($_vars['googlestatuss'] as $k=>$p){ ?>
            <option <?= (getGet('fgoogle_statuss') == $k) ? 'selected="selected"' : '' ?> value="<?=$k;?>"><?=$p;?></option>
          <?php } ?>
        </select>
      </th>

      <th>
        <span><input name="flig_no" class="sfield kalendari" type="input" value="<?= getGet('flig_no') ?>" style="width:80%;"><a class="sfield_clear" href="#">x</a></span><br>
        <span><input name="flig_lidz" class="sfield kalendari" type="input" value="<?= getGet('flig_lidz') ?>" style="width:80%;"><a class="sfield_clear" href="#">x</a></span>
      </th>

      <th>
        <span><input name="frekl_sak_no" class="sfield kalendari" type="input" value="<?= getGet('frekl_sak_no') ?>" style="width:80%;"><a class="sfield_clear" href="#">x</a></span><br>
        <span><input name="frekl_sak_lidz" class="sfield kalendari" type="input" value="<?= getGet('frekl_sak_lidz') ?>" style="width:80%;"><a class="sfield_clear" href="#">x</a></span>
      </th>

      <th>
        <span><input name="frekl_beig_no" class="sfield kalendari" type="input" value="<?= getGet('frekl_beig_no') ?>" style="width:80%;"><a class="sfield_clear" href="#">x</a></span><br>
        <span><input name="frekl_beig_lidz" class="sfield kalendari" type="input" value="<?= getGet('frekl_beig_lidz') ?>" style="width:80%;"><a class="sfield_clear" href="#">x</a></span>
      </th>

      <th>
        <select name="fsanems_forma" class="sfield">
          <option value=""></option>
          <?php foreach($_vars['sanemsanas_forma'] as $k=>$p){ ?>
            <option <?= (getGet('fsanems_forma') == $k) ? 'selected="selected"' : '' ?> value="<?=$k;?>"><?=$p;?></option>
          <?php } ?>
        </select>
      </th>

      <th>
        <select name="fpakalpojums" class="sfield">
          <option value=""></option>
          <?php foreach($_vars['pakalpojumi'] as $k=>$p){ ?>
            <option <?= (getGet('fpakalpojums') == $k) ? 'selected="selected"' : '' ?> value="<?=$k;?>"><?=$p;?></option>
          <?php } ?>
        </select>
      </th>

      <th>
        <select name="finfomedia_statuss" class="sfield" >
          <option value=""></option>
          <?php foreach($_vars['infomediastatuss'] as $k=>$p){ ?>
            <option <?= (getGet('finfomedia_statuss') == $k) ? 'selected="selected"' : '' ?> value="<?=$k;?>"><?=$p;?></option>
          <?php } ?>
        </select>
      </th>

      <th><span><input name="fgoogle_konts" class="sfield" type="input" value="<?= getGet('fgoogle_konts') ?>" style="width:80%;"><a class="sfield_clear" href="#">x</a></span></th>

      <th><span><input name="fgoogle_atslegv" class="sfield" type="input" value="<?= getGet('fgoogle_atslegv') ?>" style="width:80%;"><a class="sfield_clear" href="#">x</a></span></th>

      <th><span><input name="flig_summa" class="sfield" type="input" value="<?= getGet('flig_summa') ?>" style="width:80%;"><a class="sfield_clear" href="#">x</a></span></th>

      <th>
        <span><input name="frek_izr_no" class="sfield kalendari" type="input" value="<?= getGet('frek_izr_no') ?>" style="width:80%;"><a class="sfield_clear" href="#">x</a></span><br>
        <span><input name="frek_izr_lidz" class="sfield kalendari" type="input" value="<?= getGet('frek_izr_lidz') ?>" style="width:80%;"><a class="sfield_clear" href="#">x</a></span>
      </th>

      <th><span><input name="frek_summa" class="sfield" type="input" value="<?= getGet('frek_summa') ?>" style="width:80%;"><a class="sfield_clear" href="#">x</a></span></th>

      <th>
        <select name="frek_statuss" class="sfield">
          <option value=""></option>
          <?php foreach($_vars['rekina_statuss'] as $k=>$p){ ?>
            <option <?= (getGet('frek_statuss') == $k) ? 'selected="selected"' : '' ?> value="<?=$k;?>"><?=$p;?></option>
          <?php } ?>
        </select>
      </th>

      <th>
        <select name="frek_apm_statuss" class="sfield">
          <option value=""></option>
          <?php foreach($_vars['apmaksas_statuss'] as $k=>$p){ ?>
            <?php if ($k == 1) continue; // do not show "Nav sācies" ?>
            <option <?= (getGet('frek_apm_statuss') == $k) ? 'selected="selected"' : '' ?> value="<?=$k;?>"><?=$p;?></option>
            <? if ($k == 2) { ?>
              <option <?= (getGet('frek_apm_statuss') == '2_barteris') ? 'selected="selected"' : '' ?> value="2_barteris">&nbsp;- Barteris</option>
            <? } ?>
          <?php } ?>
        </select>
      </th>

      <th>
        <select name="fakt_atgad" class="sfield">
          <option value=""></option>
          <option <?= (getGet('fakt_atgad') == 1) ? 'selected="selected"' : '' ?> value="1">Ir</option>
          <option <?= (getGet('fakt_atgad') == 2) ? 'selected="selected"' : '' ?> value="2">Nav</option>
        </select>
      </th>

    </tr>

  </thead>

  <tbody class="main">

    <?php

    $npk = $total_rows - (($active_page - 1) * $show_in_page);
    $inid = -1;

    $totals = array(
      'rek_summa' => 0,
      'ligumu_summa' => 0,
      'lig_vid_summa' => 0,
      'lig_vid_day_summa' => 0
    );

    foreach($alldata as $data){

      $inid ++;

      $totals['ligumu_summa'] += $data['liguma_summa_bez_pvn'];

      $s_ilgums = 0;
      if(!empty($data['reklama_no']) && !empty($data['reklama_lidz'])) {
        $s_ilgums = round((strtotime($data['reklama_lidz']) - strtotime($data['reklama_no'])) / 86400) + 1;
      }

      $vsm = 0;

      if ($s_ilgums > 0 && $data['liguma_summa_bez_pvn'] > 0) {

        $totals['lig_vid_summa'] += $data['liguma_summa_bez_pvn'] / $s_ilgums * 365;

        if($data['google_statuss'] == 3) {
          $totals['lig_vid_day_summa'] += $data['liguma_summa_bez_pvn'] / $s_ilgums;
        }

      }

      ?>

      <tr id="tr<?=$inid;?>" class="lig_row">

        <td align="right" class="s_id" id="npk_id<?=$inid;?>"><?=$npk;?></td>
        <td class="s_nosaukums"><a href="?c=ligumi&a=labot&id=<?=$data['id'];?>"><b><?=$data['nosaukums'];?></b></a></td>
        <td class="s_index"><?=empty($data['faktaddr_index'])?"":$data['faktaddr_index'];?></td>
        <td class="s_pardevejs"><a href="?c=darbinieki&a=labot&id=<?= $data['pardeveja_id'] ?>"><?=$_vars['sys_pardeveji'][$data['pardeveja_id']];?></a></td>

        <? if ($data['liguma_statuss'] == 6) { ?>
          <? $active = (empty($data['beigu_statuss']) || ($data['beigu_statuss'] == 'termins' && $data['beigu_statuss_atlikt_lidz'] <= date('Y-m-d'))) ? true : false; ?>
          <td>
            <a href="#" class="lig_statuss_6 <?= !empty($data['beigu_statuss']) ? 'lig_beigu_statuss_' . $data['beigu_statuss'] : 'lig_beigu_statuss_termins' ?> <?= $active ? 'lig_beigu_statuss_active' : '' ?>" onclick="showLigumaBeiguStatussDialog(<?= $data['id'] ?>); return false;">
              <span><?= $_vars['liguma_statuss'][6] ?></span>
              (<?= !empty($data['beigu_statuss']) ? $_vars['liguma_beigu_statuss'][$data['beigu_statuss']] : $_vars['liguma_beigu_statuss']['termins'] ?>)
            </a>
          </td>
        <? } else { ?>
          <td><?= !empty($_vars['liguma_statuss'][$data['liguma_statuss']]) ? '<span class="lig_statuss_'.$data['liguma_statuss'].'">' . $_vars['liguma_statuss'][$data['liguma_statuss']] . '</span>' : '&nbsp;'?></td>
        <? } ?>

        <? if ($data['norekinu_statuss'] == 2 && is_barteris(!empty($rekini[$data['id']]) ? $rekini[$data['id']] : null)) { ?>
          <td><span class="norek_statuss_2">Barteris</span></td>
        <? } else { ?>
          <td><?= !empty($_vars['kopapmaksas_statuss'][$data['norekinu_statuss']]) ? '<span class="norek_statuss_'.$data['norekinu_statuss'].'">' . $_vars['kopapmaksas_statuss'][$data['norekinu_statuss']] . '</span>' : "&nbsp;" ?></td>
        <? } ?>

        <td class="s_googlestatuss"><?= !empty($_vars['googlestatuss'][$data['google_statuss']]) ? '<span class="google_statuss_'.$data['google_statuss'].'">' . $_vars['googlestatuss'][$data['google_statuss']] . '</span>' : '&nbsp;' ?></td>
        <td class="s_ligdat"><?=!empty($data['ligumadat'])?date("d.m.Y",strtotime($data['ligumadat'])):""?></td>
        <td class="s_rekno"><?=!empty($data['reklama_no'])?date("d.m.Y",strtotime($data['reklama_no'])):""?></td>
        <td class="s_reklidz"><?=!empty($data['reklama_lidz'])?date("d.m.Y",strtotime($data['reklama_lidz'])):""?></td>
        <td class="s_sanforma"><?=empty($_vars['sanemsanas_forma'][empty($data['sanemsanas_forma'])?"&nbsp;":$data['sanemsanas_forma']])?"&nbsp;":$_vars['sanemsanas_forma'][empty($data['sanemsanas_forma'])?"&nbsp;":$data['sanemsanas_forma']];?></td>
        <td><?= $data['pakalpojumu_saraksts'] ?></td>
        <td class="s_infostatuss"><?=empty($_vars['infomediastatuss'][empty($data['infomedia_statuss'])?"&nbsp;":$data['infomedia_statuss']])?"&nbsp;":$_vars['infomediastatuss'][empty($data['infomedia_statuss'])?"&nbsp;":$data['infomedia_statuss']];?></td>
        <td class="s_googlekonts"><?=empty($data['pak_google_konts'])?"":$data['pak_google_konts'];?></td>
        <td><?= $data['google_atslegv_saraksts'] ?></td>
        <td align="center" class="fullsummwithoutpvn"><?= !empty($data['liguma_summa_bez_pvn']) ? number_format($data['liguma_summa_bez_pvn'], 2, '.', '')."&nbsp;Ls" : "&nbsp;"; ?></td>
        <?
          if(empty($data['rekini'])){

            echo "<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>";

          } else {

            $col = array('', '', '', '');

            foreach($data['rekini'] as $rekins) {

              $totals['rek_summa'] += $rekins['kopsumma'];

              $col[0] .= '<span '.($rekins['apm_statuss'] == 3 ? 'style="background: red; color: white;"' : '').'>' . date("d.m.Y", strtotime($rekins['izr_datums'])) . '<br></span>';
              $col[1] .= '<span>'.(!empty($rekins['kopsumma']) ? number_format($rekins['kopsumma'], 2, '.', '')."&nbsp;Ls" : "").'<br></span>';
              $col[2] .= '<span>'.$_vars['rekina_statuss'][$rekins['statuss']].'<br></span>';

              if ($rekins['apm_statuss'] == 2 && !empty($rekins['barteris']))
                $col[3] .= '<span>Barteris<br /></span>';
              else
                $col[3] .= '<span>'.(isset($_vars['apmaksas_statuss'][$rekins['apm_statuss']]) ? $_vars['apmaksas_statuss'][$rekins['apm_statuss']] : '').'<br /></span>';

            }

            echo '
              <td class="rek_datumi">'.$col[0].'</td>
              <td class="rek_summas">'.$col[1].'</td>
              <td class="rek_statusi">'.$col[2].'</td>
              <td class="rek_apm_statusi">'.$col[3].'</td>
            ';

          }
        ?>
        <td>
          <?= (!empty($data['aktivi_atgadinajumi'])) ? 'Ir ('.$data['aktivi_atgadinajumi'].')' : 'Nav' ?>

          <script>

            searchlist[<?=$inid;?>] = ' <?= $data['id'] ?>';

            sp['s_googlestatuss'][<?=$inid;?>] = <?= !empty($data['google_statuss']) ? (int)$data['google_statuss'] : 0 ?>;
            sp['s_summa'][<?=$inid;?>] = '<?=!empty($data['liguma_summa_bez_pvn']) ? $data['liguma_summa_bez_pvn'] : "" ?>';
            sp['s_ilgums'][<?=$inid;?>] = <?= $s_ilgums ?>;

          </script>
        </td>
      </tr>

      <? $npk --; ?>

    <?php } ?>

  </tbody>

  <tr>
    <td valign="top"  align="right" colspan="15">
      <b>Līgumu summa (bez PVN):</b><br>
      <b>Līgumu skaits:</b><br>
      Vidējā līguma summa faktiskā:<br>
      Vidējā līguma summa gada griezumā:<br>
      Dienas budžets:<br>
      Dienas budžeta procenta likme:
    </td>
    <td valign="top"  align="center">
      <span id="ligumusumma"><?= number_format($totals['ligumu_summa'], 2, '.', '') ?>&nbsp;Ls</span><br>
      <span id="ligumuskaits"><?=$visible_rows;?></span><br>
      <span id="ligumavidejais"><?= number_format(!empty($visible_rows) ? $totals['ligumu_summa'] / $visible_rows : 0, 2, '.', '') ?>&nbsp;Ls</span><br>
      <span id="ligvidgad"><?= number_format(!empty($visible_rows) ? $totals['lig_vid_summa'] / $visible_rows : 0, 2, '.', '') ?>&nbsp;Ls</span><br>
      <span id="ligvidday"><?= number_format($totals['lig_vid_day_summa'], 2, '.', '') ?>&nbsp;Ls</span><br>
      <input type="input" id="dienbudzproc" value="100">
    </td>
    <td valign="top" align="right">
      <b>Rēķinu<br />summa:</b>
    </td>
    <td valign="top" align="center">
      <br /><?= number_format($totals['rek_summa'], 2, '.', '') ?>&nbsp;Ls
    </td>
    <td colspan="100"></td>
  </tr>

</table>

<?php print_paginator($total_rows, $active_page, $show_in_page) ?>

</form>

<? // Prototypes // ?>
<div style="display:none;">

  <div id="komentarsprot__lig_kopsavilkums">
    <table class="comment_form">
      <tr>
        <td width="70">Komentārs:</td>
        <td>
          <textarea name="komentari_new[_lig_kopsavilkums][komentars][]" rows="2" cols="30"></textarea>
        </td>
      </tr>
    </table>
  </div>

</div>

<form action="?c=ligumi" method="post" enctype="multipart/form-data" style="clear: both;">

<? show_comments_block('_lig_kopsavilkums') ?>

<table width="600">
  <tr><td colspan="2" align="center"><br><input class="ui-state-default ui-corner-all submit" type="submit" onClick="return saveAll(this, 'lig_kopsavilkums')" value="Saglabāt!"></td></tr>
  <tr><td colspan="2" align="center"><br><input style="width:200px;" type="button" onclick="return cancelSaves();" class="ui-state-default ui-corner-all" value="Atcelt"></td></tr>
</table>

</form>

<script type="text/javascript">
$(window).load(function() {

  processTable();

  $('#dienbudzproc').keyup(function(){

    if($('#dienbudzproc').val() > 0){
      processTable();
    }

  });

  initPlainSortableTable($('#fullformplace'));

});

function processTable() {

  var table = $(this);

  // update footer

  var ligviddaysum = 0;

  var koif = $('#dienbudzproc').val();

  jQuery.each(searchlist, function(k,v){

    if (sp['s_ilgums'][k] > 0 && sp['s_summa'][k] > 0) {

      if(sp['s_googlestatuss'][k] == 3 )
        ligviddaysum += parseFloat(sp['s_summa'][k].replace(',','.')) / sp['s_ilgums'][k];

    }

  });

  $('#ligvidday').html( parseFloat(ligviddaysum * (koif/100)).toFixed(2)+'&nbsp;Ls' );

}
</script>
