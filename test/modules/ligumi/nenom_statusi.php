<?
if (is_admin()) {
  die();
}

$ligumi = get_nenomainitie_statusi(array('pardeveja_id' => $_SESSION['user']['id']));
?>

<script>
var searchlist = [];

var sp = create_filter_field_array([
  's_nosaukums', 's_ligumanr', 's_lig_stat', 's_reklama_lidz'
]);
</script>

<table style="display:none;">

  <tr id="nenomainitie_statusi_filter">

    <th><span><input class="search_field" meth="standart" searchclass="s_nosaukums" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <th><span><input class="search_field" meth="standart" searchclass="s_ligumanr" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>

    <th>
      <span><input class="search_field kalendari" meth="from" searchclass="s_reklama_lidz" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span><br>
      <span><input class="search_field kalendari" meth="to" searchclass="s_reklama_lidz" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span>
    </th>

    <th></th>

  </tr>

</table>

<table style="width: 70%;" cellpadding="3" cellspacing="0" id="nenomainitie_statusi" class="data">

  <thead>

    <tr>
      <th>Klients</th>
      <th>Līguma Nr.</th>
      <th>Līgums beidzas</th>
      <th>Statuss</th>
    </tr>

  </thead>

  <tbody class="main">

    <?php
    $i = count($ligumi);
    $inid = -1;

    foreach($ligumi as $row) {

      $inid ++;

      ?>

      <tr id="tr<?=$inid;?>">

        <td><a href="?c=ligumi&a=labot&id=<?=$row['id'];?>"><b><?= $row['nosaukums'] ?></b></a></td>
        <td><?= $row['ligumanr'] ?></td>
        <td><span style="display:none;"><?=date("Y.m.d",strtotime($row['reklama_lidz']));?>|</span><?= !empty($row['reklama_lidz'])?date("d.m.Y",strtotime($row['reklama_lidz'])):"" ?></td>
        <td style="text-align: center;"><a href="#" class="lig_statuss_6 lig_beigu_statuss_termins" onclick="showLigumaBeiguStatussDialog(<?= $row['id'] ?>); return false;"><strong><?= $_vars['liguma_statuss'][6] ?></strong> (termiņš) <img src="css/edit.png" alt="Mainīt" style="vertical-align: middle;" /></a></td>

        <td style="display: none;">
        <script>
          searchlist[<?=$inid;?>] = '<?=addslashes(mb_strtoupper($row['nosaukums']));?>';
          sp['s_nosaukums'][<?=$inid;?>] = '<?=addslashes(mb_strtoupper($row['nosaukums']));?>';
          sp['s_ligumanr'][<?=$inid;?>] = '<?=addslashes(mb_strtoupper($row['ligumanr']));?>';
          sp['s_lig_stat'][<?=$inid;?>] = 6;
          sp['s_reklama_lidz'][<?=$inid;?>] = <?= !empty($row['reklama_lidz']) ? date('Ymd', strtotime($row['reklama_lidz'])) : 0 ?>;
        </script>
        </td>

      </tr>

      <? $i -- ?>

    <?php } ?>

  </tbody>

</table>

<script type="text/javascript">
$(window).load(function() {

  var table = $("#nenomainitie_statusi");
  var head = $("#nenomainitie_statusi_filter");

  table.tablesorter({
    debug: false,
    highlightClass: 'highlight',
    headers: {
      0: { sorter:'latviantext' },
      1: { sorter:'latviantext' }
    }
  }).bind('sortEnd', function() {

    stripeTable(table);

  });

  initTableFilter(table, head, function() {}, '', searchlist, sp);

});
</script>