<?
if (!is_admin()) {
  die();
}

if (empty($_GET['rek_id'])) {
  die();
}

$rek_id = (int) $_GET['rek_id'];

$rek_data = db_get_assoc(db_query("
  SELECT *
  FROM `".DB_PREF."rekini`
  WHERE id = ".$rek_id."
"));

if (empty($rek_data)) {
  die();
}

$lig_data = db_get_assoc(db_query("SELECT * FROM `".DB_PREF."ligumi` WHERE `id` = " . $rek_data['liguma_id']));
$beigu_statuss_data = db_get_assoc(db_query("SELECT * FROM `".DB_PREF."ligumi_beigusies` WHERE `liguma_id` = " . $rek_data['liguma_id']));

if (!empty($_GET['del_rek_apm_id'])) {

  $old_rek_apm_data = db_get_assoc(db_query("
    SELECT m.*
    FROM `".DB_PREF."rekini_maksajumi` m
    LEFT JOIN `".DB_PREF."pardeveji_rekini_detalas` d ON (m.id = d.rekina_maksajuma_id)
    WHERE
      m.id = ".(int)$_GET['del_rek_apm_id']." AND
      d.id IS NULL
  "));

  if (empty($old_rek_apm_data)) {
    die();
  }

  $old_rekini_data = db_get_assoc_all(db_query("SELECT * FROM `".DB_PREF."rekini` WHERE `liguma_id` = " . $rek_data['liguma_id'] . " ORDER BY id ASC"));

  $result = db_query("
    DELETE FROM `".DB_PREF."rekini_maksajumi`
    WHERE
      `rekina_id` = ".$rek_id." AND
      `id` = ".(int)$_GET['del_rek_apm_id']."
    LIMIT 1
  ");

  if ($result) {

    db_query("
      UPDATE `".DB_PREF."rekini`
      SET apm_summa = IFNULL(apm_summa, 0) - ".$old_rek_apm_data['summa']."
      WHERE id = ".$rek_id."
      LIMIT 1
    ");

    $new_rekini_data = db_get_assoc_all(db_query("SELECT * FROM `".DB_PREF."rekini` WHERE `liguma_id` = " . $rek_data['liguma_id'] . " ORDER BY id ASC"));

    if (get_apmaksas_statuss($old_rekini_data) != get_apmaksas_statuss($new_rekini_data)) {

      generate_auto_reminders($lig_data, $beigu_statuss_data, $new_rekini_data);

    }

  }

  $_SESSION['rek_apm_deleted'] = true;
  header('Location: ?c=ligumi&a=rek_apmaksas&rek_id='.$rek_id.'&without_nav=1');
  die();

}

if (!empty($_POST)) {

  $_POST = trim_array($_POST);

  $errors = array();

  $_POST['summa'] = str_replace(',', '.', $_POST['summa']);
  $datums = !empty($_POST['datums']) ? date('Y-m-d', strtotime($_POST['datums'])) : '';

  if (empty($_POST['summa']) || !is_numeric($_POST['summa'])) {
    $errors[] = 'Nav norādīta pareiza summa!';
  } else {

    $kopsumma_sant = (int) ($rek_data['kopsumma'] * 100);
    $kopapm_sant = (int) (($rek_data['apm_summa'] + $_POST['summa']) * 100);

    if ($kopsumma_sant < $kopapm_sant) {
      $errors[] = 'Ievadīta pārāk liela summa!';
    }

  }

  if (empty($datums)) {
    $errors[] = 'Nav norādīts datums!';
  } elseif ($datums < $rek_data['izr_datums']) {
    $errors[] = 'Apmaksas datums nevar būt pirms rēķina izrakstīšanas datuma!';
  }

  if (empty($errors)) {

    $old_rekini_data = db_get_assoc_all(db_query("SELECT * FROM `".DB_PREF."rekini` WHERE `liguma_id` = " . $rek_data['liguma_id'] . " ORDER BY id ASC"));

    $result = db_query("
      INSERT INTO `".DB_PREF."rekini_maksajumi` (
        rekina_id,
        datums,
        summa,
        created_pardeveja_id
      ) VALUES (
        ".$rek_id.",
        '".esc($datums)."',
        ".(float)$_POST['summa'].",
        ".$_SESSION['user']['id']."
      )
    ");

    if ($result) {

      db_query("
        UPDATE `".DB_PREF."rekini`
        SET apm_summa = IFNULL(apm_summa, 0) + ".(float)$_POST['summa']."
        WHERE id = ".$rek_id."
        LIMIT 1
      ");

      $new_rekini_data = db_get_assoc_all(db_query("SELECT * FROM `".DB_PREF."rekini` WHERE `liguma_id` = " . $rek_data['liguma_id'] . " ORDER BY id ASC"));

      if (get_apmaksas_statuss($old_rekini_data) != get_apmaksas_statuss($new_rekini_data)) {

        generate_auto_reminders($lig_data, $beigu_statuss_data, $new_rekini_data);

      }

    }

    $_SESSION['rek_apm_added'] = true;
    header('Location: ?c=ligumi&a=rek_apmaksas&rek_id='.$rek_id.'&without_nav=1');
    die();

  }

}

$query = db_query("
  SELECT
    m.*,
    d.id as pardeveja_rekina_detalas_id
  FROM `".DB_PREF."rekini_maksajumi` m
  LEFT JOIN `".DB_PREF."pardeveji_rekini_detalas` d ON (m.id = d.rekina_maksajuma_id)
  WHERE m.rekina_id = ".$rek_id."
  ORDER BY m.datums ASC, m.id ASC
")
?>

<? if (!empty($errors)) { ?>
  <ul style="margin: 10px 0 5px 0;" class="errors">
    <? foreach($errors as $error) { ?>
      <li><?= $error ?></li>
    <? } ?>
  </ul>
<? } ?>

<? if (!empty($_SESSION['rek_apm_deleted'])) { ?>
  <p style="color: green; margin: 10px 0 5px 0;">Rēķina apmaksa izdzēsta</p>
  <? unset($_SESSION['rek_apm_deleted']) ?>
<? } ?>

<? if (!empty($_SESSION['rek_apm_added'])) { ?>
  <p style="color: green; margin: 10px 0 5px 0;">Rēķina apmaksa pievienota</p>
  <? unset($_SESSION['rek_apm_added']) ?>
<? } ?>

<table id="rek_apmaksas" class="data" style="width: 99%;">

  <thead>

    <tr class="last">
      <th>Summa</th>
      <th>Apmaksas datums</th>
      <th>Pievienoja</th>
      <th>Pievienots</th>
      <th></th>
    </tr>

  </thead>

  <tbody class="main">

    <? while($row = db_get_assoc($query)) { ?>

      <tr>
        <td><?= format_currency($row['summa']) ?></td>
        <td><?= !empty($row['datums']) ? date('d.m.Y', strtotime($row['datums'])) : ""; ?></td>
        <td><?= !empty($row['created_pardeveja_id']) ? $_vars['sys_pardeveji'][$row['created_pardeveja_id']] : ''; ?></td>
        <td><?= !empty($row['created']) ? date('d.m.Y', strtotime($row['created'])) : ""; ?></td>
        <td class="last" style="text-align: center;">
          <? if (empty($row['pardeveja_rekina_detalas_id'])) { ?>
            <a href="?c=ligumi&a=rek_apmaksas&rek_id=<?= $rek_id ?>&del_rek_apm_id=<?= $row['id'] ?>&without_nav=1" onclick="return confirm('Vai tiešām dzēst apmaksu?');" title="Dzēst"><img src="css/del.png" alt="Dzēst" /></a>
          <? } else { ?>
            <span class="info">Nevar dzēst, jo ir izmaksāta provīzija darbiniekam</span>
          <? } ?>
        </td>
      </tr>

    <? } ?>

  </tbody>

</table>

<form id="rek_apm_form" action="" method="post">

  Summa: <input type="text" name="summa" value="<?= isset($_POST['summa']) ? $_POST['summa'] : '' ?>" style="width: 80px; margin-right: 10px;" />
  Apmaksas Datums: <input id="apmaksas_datums" type="text" name="datums" value="<?= isset($_POST['datums']) ? $_POST['datums'] : date('d.m.Y') ?>" style="width: 90px; margin-right: 10px;" />

  <button class="ui-state-default ui-corner-all" style="">Pievienot apmaksu</button>

  <p class="info">Rēķina kopsumma: <strong><?= format_currency($rek_data['kopsumma']) ?></strong>, neapmaksāta summa: <strong><?= format_currency($rek_data['kopsumma'] - $rek_data['apm_summa']) ?></strong></p>

</form>

<script type="text/javascript">
stripeTable($('#rek_apmaksas'));
$("#apmaksas_datums").datepicker({dateFormat:'dd.mm.yy'});
</script>