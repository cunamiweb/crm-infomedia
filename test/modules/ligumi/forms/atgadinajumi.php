<?
$skeys = array(
  0 => array(), // - aktīvie
  1 => array(), // - idle
  2 => array(), // - izpildītie
  3 => array() // - dzēstie
);

if (!empty($atgadinajumi)) {

  foreach($atgadinajumi as $atgadinajuma_id => $v) {

    $tmp_d = date("d");
    $tmp_m = date("m");
    $tmp_Y = date("Y");
    $tmp_today = $tmp_d + $tmp_m * 1000 + $tmp_Y * 100000;

    $tmp_date = explode(".", date('d.m.Y', strtotime($v['datums'])));
    $tmp_date = $tmp_date[0] + $tmp_date[1] * 1000 + $tmp_date[2] * 100000;
    //$tmp_date = 1+2*1000+2000*100000;

    if($v['statuss'] == 2) {
      $skeys[2][$atgadinajuma_id] = $tmp_date;
    } elseif ($v['statuss'] == 3) {
      $skeys[3][$atgadinajuma_id] = $tmp_date;
    } else {

      if ($tmp_today >= $tmp_date) { // aktuāli (vecs)
        $skeys[0][$atgadinajuma_id] = $tmp_date;
      } else { // nākotnes
        $skeys[1][$atgadinajuma_id] = $tmp_date;
      }

    }

  }

  $sss = array();

  foreach($skeys as $k => $v){

    asort($v);

    $skeys[$k] = $v;

  }

  $sss = $skeys[0] + $skeys[1] + $skeys[2] + $skeys[3];

}


?>

<button class="hidethistoo" onclick="return addAtgRow();">Pievienot jaunu atgādinājumu</button><br /><br />

<table width="99%" id="atgadinajumu_tabula">

  <tr>
    <th width="85">Datums</th>
    <th width="85">Statuss</th>
    <th width="80">Tēma</th>
    <th>Saturs</th>
    <th width="70">Izveidots</th>
    <th width="100">Izveidotājs</th>
    <th width="70">Izpildīts</th>
    <th width="100">Izpildīja</th>
  </tr>

  <? if(!empty($sss)) foreach($sss as $atgadinajuma_id => $v ) { ?>

    <? $v = $atgadinajumi[$atgadinajuma_id] ?>

    <tr class="atg_id_<?= $atgadinajuma_id ?>">

      <td><input type="input" class="showcalendar atgadinajumi_termimi" value="<?=!empty($v['datums'])?date('d.m.Y', strtotime($v['datums'])):date('d.m.Y');?>" name="atgadinajumi[<?= $atgadinajuma_id ?>][datums]"></td>
      <td class="status">
        <span class="link_wrap">
          <? if ($v['statuss'] == 1) { ?>
            <a onclick="return changeAtgStatus(<?= $atgadinajuma_id ?>, 2);" href="#">Izpildīts</a><br />
            <a onclick="return changeAtgStatus(<?= $atgadinajuma_id ?>, 3);" href="#">Atcelts</a>
          <? } else { ?>
            <? if ($v['statuss'] == 2) { ?>
              Izpildīts
            <? } elseif ($v['statuss'] == 3) { ?>
              Atcelts
            <? } ?>
            <a onclick="return changeAtgStatus(<?= $atgadinajuma_id ?>, 1);" href="#">X</a>
          <? } ?>
        </span>
        <input type="hidden" name="atgadinajumi[<?= $atgadinajuma_id ?>][statuss]" value="<?= $v['statuss'] ?>" />
      </td>
      <td>
        <select name="atgadinajumi[<?= $atgadinajuma_id ?>][tema]">
          <? foreach($_vars['atgadinajumu_temas'] as $temas_id => $temas_nosauk) { ?>
            <option value="<?= $temas_id ?>" <?=(!empty($v['tema']) && $v['tema'] == $temas_id)?"selected":"";?>><?= $temas_nosauk ?></option>
          <? } ?>
        </select>
      </td>
      <td><textarea name="atgadinajumi[<?= $atgadinajuma_id ?>][saturs]" rows="2" cols="30"><?=!empty($v['saturs'])?$v['saturs']:"";?></textarea></td>
      <td><?=!empty($v['created'])?date('d.m.Y', strtotime($v['created'])):'';?></td>
      <? if (!empty($v['created_pardeveja_id'])) { ?>
        <td><a href="?c=darbinieki&a=labot&id=<?= $v['created_pardeveja_id'] ?>"><?=$_vars['sys_pardeveji'][$v['created_pardeveja_id']];?></a></td>
      <? } else { ?>
        <td><span class="atg_auto">auto</span></td>
      <? } ?>
      <td><?=!empty($v['apstr_laiks'])?date('d.m.Y', strtotime($v['apstr_laiks'])):'';?></td>
      <? if (!empty($v['apstr_pardeveja_id'])) { ?>
        <td><a href="?c=darbinieki&a=labot&id=<?= $v['apstr_pardeveja_id'] ?>"><?=$_vars['sys_pardeveji'][$v['apstr_pardeveja_id']];?></a></td>
      <? } else { ?>
        <td></td>
      <? } ?>

    </tr>

  <? } ?>

</table>

<br>

<div style="width:400px; margin:10px;">
  <div style="background-color:#EDFBC6; width:100px; float:left;">AKTĪVS</div>
  <div style="background-color:#FBF8C6; width:100px; float:left;">NAV TERMIŅŠ</div>
  <div style="background-color:#87D97F; width:100px; float:left;">IZPILDĪTS</div>
  <div style="background-color:#D4D4D4; width:100px; float:left;">ATCELTS</div>
  <div style="clear:both;"></div>
</div>


<script>
//atgadinajumi_tabula_visa
$('.atgadinajumi_termimi').change(refreshAtgStatuses);

refreshAtgStatuses();
  
$(".showcalendar").datepicker({dateFormat:'dd.mm.yy'});

</script>

<? show_files_block('lig_atgadinajumi', isset($_GET['id']) ? $_GET['id'] : null) ?>

<? show_comments_block('lig_atgadinajumi', isset($_GET['id']) ? $_GET['id'] : null) ?>