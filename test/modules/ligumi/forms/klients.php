<?php if(!empty($_GET['id'])){ ?>
  <input type="hidden" name="liguma_id" value="<?=(int)$_GET['id']?>">
<? } ?>

<table width="600">

  <tr><td colspan="2"><h2>Līgums</h2></td></tr>
  <tr><td>Līguma numurs:</td><td><input type="input" id="orgligumanr" name="ligumanr" value="<?=!empty($d['ligumanr'])?$d['ligumanr']:""?>"><br><div class="info">AA000000</div></td></tr>
  <tr><td>Līguma datums:</td><td><input type="input" name="ligumadat" id="ligumadat" value="<?=!empty($d['ligumadat'])?date('d.m.Y', strtotime($d['ligumadat'])):''?>"></td></tr>
  <tr><td>Nosaukums:</td><td><input type="input" id="nosaukumsplace" name="nosaukums" value="<?=!empty($d['nosaukums'])?$d['nosaukums']:""?>"></td></tr>
  <tr><td>Reģ.nr/pers.kods:</td><td><input type="input" name="regnr" value="<?=!empty($d['regnr'])?$d['regnr']:""?>"></td></tr>
  <tr><td>PVN nr.:</td><td><input type="input" name="pvnnr" value="<?=!empty($d['pvnnr'])?$d['pvnnr']:""?>"></td></tr>
  <tr>
    <td>Faktiskā adrese:<br /><button class="ui-state-default ui-corner-all hidethistoo" onClick="return copyFaktToJur();">Kopēt uz jur.</button></td>
    <td>
      <div><input style="width: 78%; margin-bottom: 1px;" type="input" name="faktaddr_street" id="add2" value="<?=!empty($d['faktaddr_street'])?$d['faktaddr_street']:""?>"> <span class="info">Iela</span></div>
      <div><input style="width: 78%; margin-bottom: 1px;" type="input" name="faktaddr_city" id="add1" value="<?=!empty($d['faktaddr_city'])?$d['faktaddr_city']:""?>"> <span class="info">Pilsēta</span></div>
      <div><input style="width: 78%; margin-bottom: 1px;" type="input" name="faktaddr_index" id="add3" value="<?=!empty($d['faktaddr_index'])?$d['faktaddr_index']:""?>"> <span class="info">Pasta indekss</span></div>
    </td>
  </tr>
  <tr>
    <td>Juridiskā adrese:<br /><button class="ui-state-default ui-corner-all hidethistoo" onClick="return copyJurToFakt();">Kopēt uz fakt.</button></td>
    <td>
      <div><input style="width: 78%; margin-bottom: 1px;" type="input" name="juridaddr_street" id="juradd2" value="<?=!empty($d['juridaddr_street'])?$d['juridaddr_street']:""?>"> <span class="info">Iela</span></div>
      <div><input style="width: 78%; margin-bottom: 1px;" type="input" name="juridaddr_city" id="juradd1" value="<?=!empty($d['juridaddr_city'])?$d['juridaddr_city']:""?>"> <span class="info">Pilsēta</span></div>
      <div><input style="width: 78%; margin-bottom: 1px;" type="input" name="juridaddr_index" id="juradd3" value="<?=!empty($d['juridaddr_index'])?$d['juridaddr_index']:""?>"> <span class="info">Pasta indekss</span></div>
    </td>
  </tr>
  <tr>
    <td>Kontaktpersona:</td>
    <td>
      <div id="kontplace">

        <?php foreach($kontakti as $k) { ?>

          <div>
            <table width="100%">
              <tr><td>Vārds:</td><td><input type="input" name="kontakti[<?= $k['id'] ?>][vards]" value="<?=!empty($k['vards'])?$k['vards']:""?>"></td></tr>
              <tr><td>Tālrunis:</td><td><input type="input" name="kontakti[<?= $k['id'] ?>][telefons]" value="<?=!empty($k['telefons'])?$k['telefons']:""?>"></td></tr>
              <tr><td>E-pasts:</td><td><input type="input" class="readthismail" name="kontakti[<?= $k['id'] ?>][epasts]" value="<?=!empty($k['epasts'])?$k['epasts']:""?>"></td></tr>
              <tr><td>Līguma slēdzējs:</td><td><input type="checkbox" class="ligmasledzejcheck" name="kontakti[<?= $k['id'] ?>][ligumsledzejs]" <?=!empty($k['ligumsledzejs'])?"checked":""?>></td></tr>
            </table>
            <button class="hidethistoo" onClick="$(this).parent().html(''); return false;">Dzēst šo kontaktu</button>
          </div>

        <?php } ?>

      </div>

      <script>
      function init_ligmasledzejcheck() {

        var cprocess = function() {
          $('.ligmasledzejcheck').not(this).attr('checked', false);
        }

        $('.ligmasledzejcheck').unbind('click');

        $('.ligmasledzejcheck').click(cprocess);


      }
      init_ligmasledzejcheck();
      </script>

      <br>
      <button onClick="$('#kontplace').append($('#kontprot').html()); init_ligmasledzejcheck(); return false;" class="ui-state-default ui-corner-all hidethistoo">Pievienot kontaktu</button>

    </td>
  </tr>
  
  <tr>
    <td colspan="2">

      <table width="100%">

        <tr>
          <td valign="top" width="50%">
            <h2>Pārdevējs</h2>
            <select name="pardeveja_id">
              <?php foreach($_vars['sys_pardeveji'] as $k => $p){ ?>
                <option value="<?=$k;?>" <?=($k==(!empty($d['pardeveja_id'])?$d['pardeveja_id']:$_SESSION['user']['id'])?"selected":"")?>><?=$p;?></option>
              <?php } ?>
            </select><br/  />
            <? if (!empty($d['pardeveja_id'])) { ?>
              <a style="margin-left: 5px;" href="?c=darbinieki&a=labot&id=<?= $d['pardeveja_id'] ?>">Atvērt darbinieka līgumus</a>
            <? } ?>
          </td>
          <td valign="top">
            <h2>Saņemšanas forma</h2>
            <select name="sanemsanas_forma">
              <?php foreach($_vars['sanemsanas_forma'] as $k => $p){ ?>
                <option value="<?=$k;?>" <?=($k==(!empty($d['sanemsanas_forma'])?$d['sanemsanas_forma']:1)?"selected":"")?>><?=$p;?></option>
              <?php } ?>
            </select>
            <h2>Saņemšanas datums:</h2>
            <input type="input" name="sanemsanas_datums" id="sanemsanas_datums" value="<?=!empty($d['sanemsanas_datums'])?date('d.m.Y', strtotime($d['sanemsanas_datums'])):(!isset($_GET['id']) ? date('d.m.Y') : '')?>">
          </td>
        </tr>

      </table>

    </td>
  </tr>

</table>

<? show_files_block('lig_klients', isset($_GET['id']) ? $_GET['id'] : null) ?>

<? show_comments_block('lig_klients', isset($_GET['id']) ? $_GET['id'] : null) ?>