<input type="hidden" name="makeedit" value="true">

<div style="background-color:#f0f0f0; padding:5px; margin:5px;">
  Soda procenti: <b><input type="input" value="<?=(float)$d['soda_proc'];?>" name="soda_proc" style="width:30px;" id="pvnset">%</b>,
  PVN likme: <b><input type="input" value="<?=(float)$d['pvn'];?>" name="pvn" style="width:30px;" id="pvnset">%</b>, rēķinu skaits
  <button onclick="return false;" class="ui-state-default ui-corner-all ijustpressplus hidethistoo">+</button>
  <b><span id="text_rekinu_skaits"><?=count($r);?></span></b>
  <button  onclick="return false;" class="ui-state-default ui-corner-all ijustpressminus hidethistoo">-</button>
</div>

<input type="hidden" name="maksajumi" id="maksajumi" value="<?=$r;?>">

<table width="98%" id="rekinu_tabula" class="data">

  <thead>

    <tr class="last">
      <th>Rēķina<br />numurs</th>
      <th>Izrakstīšanas datums</th>
      <th>Rēķina statuss</th>
      <th>Summa</th>
      <th>PVN&nbsp;<?=(float)$d['pvn'];?>%</th>
      <th>Kopsumma</th>
      <th>Apmaksas termiņš</th>
      <th width="90">Apmaksas statuss</th>
      <th>Kav. dienas/<br />Soda nauda</th>
      <th>Apmaksātā summa</th>
      <th>Apmaksas datums</th>
      <th></th>
      <th></th>
      <th></th>
    </tr>

  </thead>

  <tbody class="main">

  <? foreach($r as $rekins) { ?>

    <tr class="tr_rek">
      <td class="rekinanrclass"><span style="font-size: 0.9em;"></span><input type="hidden" name="rekini[<?=$rekins['id'];?>][rek_nr]" value="" /></td>
      <td><input style="width: 6em;" type="input" name="rekini[<?=$rekins['id'];?>][izr_datums]" value="<?=!empty($rekins['izr_datums'])?date('d.m.Y', strtotime($rekins['izr_datums'])):"";?>" class="showcalendar"></td>
      <td>
        <select name="rekini[<?=$rekins['id'];?>][statuss]">
          <option value="1" <?=(!empty($rekins['statuss']) && $rekins['statuss'] == 1)?"selected":"";?>>Nav</option>
          <option value="2" <?=(!empty($rekins['statuss']) && $rekins['statuss'] == 2)?"selected":"";?>>Izsūtīts</option>
          <option value="3" <?=(!empty($rekins['statuss']) && $rekins['statuss'] == 3)?"selected":"";?>>Apstipr.</option>
        </select>
      </td>
      <td><input style="width: 4em;" type="input" aidii="field1" class="autocalc field1" value="<?=!empty($rekins['summa'])?$rekins['summa']:"";?>" name="rekini[<?=$rekins['id'];?>][summa]"></td>
      <td style="white-space: nowrap;"><div class="pvnplace" style="text-align:center;">0 Ls</div></td>
      <td><input type="input" aidii="field2" class="autocalc field2" value="<?=!empty($rekins['kopsumma'])?$rekins['kopsumma']:"";?>" name="rekini[<?=$rekins['id'];?>][kopsumma]"></td>
      <td><input style="width: 6em;" type="input" name="rekini[<?=$rekins['id'];?>][apm_termins]" class="showcalendar calendarcalc apm_termins" value="<?=!empty($rekins['apm_termins'])?date('d.m.Y', strtotime($rekins['apm_termins'])):"";?>"></td>
      <td align="center" style="text-align: center;"><span class="date_status" style="font-size: 0.8em;"></span></td>
      <td align="center">
        <span class="soda_nauda" style="display: block; height: 1.6em;"></span>
        <span class="<?= (empty($rekins['apm_soda_summa'])) ? 'hidethistoo' : ''?>" style="font-size: 0.8em; white-space: nowrap;">Samaksāts: <input style="width: 3em;" type="input" value="<?=!empty($rekins['apm_soda_summa'])?$rekins['apm_soda_summa']:"";?>" name="rekini[<?=$rekins['id'];?>][apm_soda_summa]"></span>
      </td>
      <td align="center">
        <span class="apm_summa" style="vertical-align: middle;"><?=!empty($rekins['apm_summa']) && $rekins['apm_summa'] != '0.00'?$rekins['apm_summa']:"";?></span>
        <? if (is_admin()) { ?>
          <a style="vertical-align: middle;" class="onlyinview" href="#" onclick="showRekiniApmaksas(<?= $rekins['id'] ?>); return false;" title="Atvērt rēķina apmaksas"><img src="css/plus.gif" alt="Apmaksas" style="vertical-align: middle;" /></a>
        <? } ?>
      </td>
      <td><?=!empty($rekins['pedejais_apm_datums'])?date('d.m.Y', strtotime($rekins['pedejais_apm_datums'])):"";?></td>
      <td valign="top" align="left" style="white-space: nowrap;">

        <div style="font-size: 0.8em; height: 22px;">
          <input type="checkbox" name="rekini[<?=$rekins['id'];?>][anulets]" class="anulets calendarcalc" <?=!empty($rekins['anulets'])?"checked":"";?>> Anulēts
          <input style="width: 6em;" type="input" name="rekini[<?=$rekins['id'];?>][anul_datums]" class="showcalendar" value="<?=!empty($rekins['anul_datums'])?date('d.m.Y', strtotime($rekins['anul_datums'])):"";?>">
        </div>

        <div style="font-size: 0.8em;">
          <input type="checkbox" name="rekini[<?=$rekins['id'];?>][nodots_piedz]" class="nodots_piedz calendarcalc" <?=!empty($rekins['nodots_piedz'])?"checked":"";?>> Nodots piedz.
          <input style="width: 6em;" type="input" name="rekini[<?=$rekins['id'];?>][nodots_piedz_datums]" class="showcalendar" value="<?=!empty($rekins['nodots_piedz_datums'])?date('d.m.Y', strtotime($rekins['nodots_piedz_datums'])):"";?>">
        </div>

      </td>
      <td valign="top" style="white-space: nowrap;">

        <div style="font-size: 0.8em; height: 22px;">
          <input type="checkbox" name="rekini[<?=$rekins['id'];?>][barteris]" class="barteris calendarcalc" <?=!empty($rekins['barteris'])?"checked":"";?>> Barteris
        </div>

        <div style="font-size: 0.8em;">
          <input type="checkbox" name="rekini[<?=$rekins['id'];?>][atkapsanas_rekins]" class="atkapsanas_rekins calendarcalc" <?=!empty($rekins['atkapsanas_rekins'])?"checked":"";?>> Atkāpš. rēķ.
        </div>

      </td>
      <td align="left" style="white-space: nowrap;">
        <? if (is_admin()) { ?>

          <button onclick="showGenPdfDialog(<?= $rekins['id'] ?>, '<?=!empty($rekins['izr_datums'])?date('d.m.Y', strtotime($rekins['izr_datums'])):"";?>'); return false;" class="onlyinview" style="font-size: 0.75em;">Ģen.&nbsp;PDF</button>

          <? if (!empty($rekins['fails_avansa_ep']) || !empty($rekins['fails_avansa_pp']) || !empty($rekins['fails_gala_ep']) || !empty($rekins['fails_avansa_pp'])) { ?>

            <a class="onlyinview" href="#" onclick="showPdfDialog(<?= $rekins['id'] ?>); return false;" title="Lejupielādēt PDF"><img src="css/pdf.gif" alt="PDF" style="vertical-align: middle;" /></a>

            <div class="pdflinks" id="pdflinks<?= $rekins['id'] ?>">

              <? if (!empty($rekins['fails_avansa_ep']) && is_file("faili/" . $rekins['fails_avansa_ep'])) { ?>
                <div class="i">
                  <a href="?getfile=<?=$rekins['fails_avansa_ep'];?>" target="downloadframe" title="Lejupielādēt PDF">Avansa rēķins (pa e-pastu)</a>
                  <? if (!empty($piegades[1]['adrese'])) { ?>
                    <a onclick="sendRekToEmail(this, <?= $rekins['id'] ?>, 'fails_avansa_ep', '<?= addslashes($piegades[1]['adrese']) ?>'); return false;" href="#" title="Nosūtīt pa e-pastu"><img src="css/email.png" alt="Nosūtīt pa e-pastu" /></a>
                  <? } ?>
                </div>
              <? } ?>

              <? if (!empty($rekins['fails_avansa_pp']) && is_file("faili/" . $rekins['fails_avansa_pp'])) { ?>
                <div class="i">
                  <a href="?getfile=<?=$rekins['fails_avansa_pp'];?>" target="downloadframe" title="Lejupielādēt PDF">Avansa rēķins (pa pastu)</a>
                </div>
              <? } ?>

              <? if (!empty($rekins['fails_gala_ep']) && is_file("faili/" . $rekins['fails_gala_ep'])) { ?>
                <div class="i">
                  <a href="?getfile=<?=$rekins['fails_gala_ep'];?>" target="downloadframe" title="Lejupielādēt PDF">Gala rēķins (pa e-pastu)</a>
                  <? if (!empty($piegades[1]['adrese'])) { ?>
                    <a onclick="sendRekToEmail(this, <?= $rekins['id'] ?>, 'fails_gala_ep', '<?= addslashes($piegades[1]['adrese']) ?>'); return false;" href="#" title="Nosūtīt pa e-pastu"><img src="css/email.png" alt="Nosūtīt pa e-pastu" /></a>
                  <? } ?>
                </div>
              <? } ?>

              <? if (!empty($rekins['fails_gala_pp']) && is_file("faili/" . $rekins['fails_gala_pp'])) { ?>
                <div class="i">
                  <a href="?getfile=<?=$rekins['fails_gala_pp'];?>" target="downloadframe" title="Lejupielādēt PDF">Gala rēķins (pa pastu)</a>
                </div>
              <? } ?>

            </div>

            <script>
            $('#pdflinks<?= $rekins['id'] ?>').dialog({
              modal: true,
              autoOpen: false
            })
            </script>

          <? } ?>

        <? } ?>
      </td>
    </tr>

  <? } ?>

  </tbody>

  <tbody>

    <tr>
      <td colspan="5" align="right" valign="top"><strong>Summa:</strong></td>
      <td valign="top"><input type="input" id="fullsumm" name="rekini_fullsumm" value="0" style="width:70px; border:0px;" readonly="true"></td>
      <td align="right" colspan="3"><strong>Kopā Ls:<br>Kopā %:</strong></td>
      <td align="center"><span id="atmsumm"></span>&nbsp;Ls<br><span id="atmproc"></span>%<br></td>
      <td colspan="100"></td>
    </tr>

  </tbody>

</table>

<input type="hidden" id="fullsummwithoutpvn" name="rekini_fullsummwithoutpvn" value="0">

<h2>Rēķina piegāde</h2>

<table width="600">
  <?php foreach($_vars['rekina_piegade'] as $k => $p) { ?>
    <tr>
      <td style="width:150px;"><input class="radiobtnlistener" val="<?=$k;?>" type="checkbox" name="piegades[<?=$k;?>][check]" <?=(!empty($piegades[$k])) ? "checked" : "";?>>&nbsp;<?=$p;?></td>
      <td><input id="adressfieldme<?=$k;?>" value="<?=!empty($piegades[$k])?$piegades[$k]['adrese']:""?>" name="piegades[<?=$k;?>][adrese]" type="input" style="width:400px;"></td>
    </tr>
  <?php } ?>
</table>

<script>
$('.radiobtnlistener').click(function() {

  if($(this).attr('val') == 1 && $(this).attr('checked')){

    $('.ligmasledzejcheck:checked').each(function() {
      var email = $(this).parents('table').filter(':first').find('.readthismail').val();
      $('#adressfieldme1').attr('value', email);
    })

  }

  if($(this).attr('val') == 2 && $(this).attr('checked')){
    $('#adressfieldme2').attr('value', $('#add1').val() + ", " + $('#add2').val() + ", " + 'LV-' + $('#add3').val() );
  }

});

function showGenPdfDialog(rek_id, default_izr_datums) {

  var form = $(
    '<form class="liggenpdfform" method="get" action="">' +
      '<div class="i"><button class="ui-state-default ui-corner-all" onclick="window.location.href=\'?c=ligumi&a=gen_rek&tips=avansa&rek_id='+rek_id+'\'; return false;">Izveidot avansa rēķinu</button></div>' +
      '<div class="i"><button class="ui-state-default ui-corner-all" onclick="$(this).parent().next().toggle(); return false;">Izveidot gala rēķinu</button></div>' +
      '<div class="gala_block">' +
        '<input type="hidden" name="c" value="ligumi" />' +
        '<input type="hidden" name="a" value="gen_rek" />' +
        '<input type="hidden" name="tips" value="gala" />' +
        '<input type="hidden" name="rek_id" value="'+rek_id+'" />' +
        'Izr. datums: <input class="showcalendar" type="text" name="izr_datums" value="'+default_izr_datums+'" /><br />' +
        '<input class="subm" type="submit" value="Izveidot" />' +
      '</div>' +
    '</form>'
  );

  $(form).dialog({
    modal: true
  });

  $(".showcalendar", form).datepicker({dateFormat:'dd.mm.yy'});

}

function showPdfDialog(rek_id) {

  $('#pdflinks' + rek_id).dialog('open');

}

function showRekiniApmaksas(rek_id) {

  $('<iframe src="?c=ligumi&a=rek_apmaksas&rek_id='+rek_id+'&without_nav=1" />').dialog({
      title: 'Rēķina apmaksas',
      autoOpen: true,
      width: 800,
      height: 500,
      modal: true,
      resizable: false,
      draggable: false,
      closeOnEscape: false,
      close: function() {
        document.location = $('#fullformplace').attr('action') +  '&subtab=2';
      }
  }).width(780);

}

function sendRekToEmail(a, rek_id, tips, epasts) {

  if (!confirm('Vai nosūtīt rēķinu uz klienta e-pastu "'+epasts+'"?')) {
    return false;
  }

  var loading_html = '<img style="margin-left: 5px;" alt="Notiek ielāde..." src="css/loading.gif" />';

  $(a).after(loading_html);

  var params = {
    rek_id: rek_id,
    tips: tips
  }

  $.get('ajax.php?action=send_rek_to_email', params, function(result) {

    if (result['success']) {
      alert('Rēķins veiksmīgi nosūtīts!');
    } else {
      alert(result['error']);
    }

    $(a).next().remove();

  }, 'json');

}

function recreateRekiniNr() {

  var i = 0;
  var ligumnr = $('#orgligumanr').val();

  if($('.rekinanrclass').length == 1) {

    var row = $('.rekinanrclass').parents('tr.tr_rek');

    var nr = ligumnr;
    if (row.find('.barteris').is(':checked')) {
      nr += '/B';
    } else if (row.find('.atkapsanas_rekins').is(':checked')) {
      nr += '/A';
    }

    $('.rekinanrclass span').html(nr);
    $('.rekinanrclass input').val(nr);

  } else {

    jQuery.each($('.rekinanrclass'), function(){

      i++;

      var row = $(this).parents('tr.tr_rek');

      var nr = ligumnr;

      if (row.find('.barteris').is(':checked')) {
        nr += '/B';
      } else if (row.find('.atkapsanas_rekins').is(':checked')) {
        nr += '/A';
      } else {
        nr += '/' + i;
      }

      $('span', this).html(nr);
      $('input', this).val(nr);

    });

  }

}

$(function() {

  recreateRekiniNr();

  stripeTable($("table#rekinu_tabula"));

  $('.ijustpressminus').click(function(){

    $('#rekinu_tabula tbody.main tr:last').remove();

    recalc();
    recalcPVN();
    recreateRekiniNr();

    stripeTable($("table#rekinu_tabula"));
    
    c = $('.rekinanrclass').length;

    $('#text_rekinu_skaits').html( c );
    $('#maksajumi').val(c);

  });

  $('.ijustpressplus').click(function(){

    c = $('.rekinanrclass').length+1;

    $('#maksajumi').val(c);

    v =  '<tr class="tr_rek"><td class="rekinanrclass"><span style="font-size: 0.9em;"></span><input type="hidden" name="rekini_new[rek_nr][]" value="" /></td>'+
       '<td><input style="width: 6em;" type="input" name="rekini_new[izr_datums][]" value="" class="showcalendar"></td>'+
       '<td><select name="rekini_new[statuss][]"><option value="1">Nav</option><option value="2">Izsūtīts</option><option value="3">Apstipr.</option>'+
       '</select></td>'+
       '<td><input type="input" aidii="field1" class="autocalc field1" value="" name="rekini_new[summa][]"></td>'+
       '<td><div class="pvnplace" style="text-align:center;">0 Ls</div></td>'+
       '<td><input type="input" aidii="field2" class="autocalc field2" value="" name="rekini_new[kopsumma][]"></td>'+
       '<td><input style="width: 6em;" type="input" name="rekini_new[apm_termins][]" class="showcalendar calendarcalc" value=""></td>'+
       '<td align="center" style="text-align:center;"><span class="date_status"  style="font-size: 0.8em;"></span></td>'+
       '<td></td>'+
       '<td></td>'+
       '<td></td>'+
       '<td valign="top" align="left" style="white-space: nowrap;">' +
         '<div style="font-size: 0.8em; height: 22px;">' +
           '<input type="checkbox" name="rekini_new[anulets][]" class="anulets calendarcalc"> Anulēts' +
           '<input style="width: 6em;" type="input" name="rekini_new[anul_datums][]" class="showcalendar" value=""><br />' +
         '</div>' +
         '<div style="font-size: 0.8em;">' +
           '<input type="checkbox" name="rekini_new[nodots_piedz][]" class="nodots_piedz calendarcalc"> Nodots piedz.' +
           '<input style="width: 6em;" type="input" name="rekini_new[nodots_piedz_datums][]" class="showcalendar" value="">' +
         '</div>' +
       '</td>' +
       '<td valign="top" style="white-space: nowrap;">'+
         '<div style="font-size: 0.8em; height: 22px;">' +
           '<input type="checkbox" name="rekini_new[barteris][]" class="barteris calendarcalc"> Barteris' +
         '</div>' +
         '<div style="font-size: 0.8em;">' +
           '<input type="checkbox" name="rekini_new[atkapsanas_rekins][]" class="atkapsanas_rekins calendarcalc"> Atkāpš. rēķ.' +
         '</div>' +
       '</td>' +
       '<td></td>'+
       '</tr>';

    $('#rekinu_tabula tbody.main').append( v );

    var new_row = $('#rekinu_tabula tbody.main tr:last');

    new_row.find('.autocalc').keyup(recalc); // recalcCal
    new_row.find('.calendarcalc').change(recalcCal);
    new_row.find(".showcalendar").datepicker({dateFormat:'dd.mm.yy'});

    new_row.find('input.barteris').click(processBarterisClick);
    new_row.find('input.atkapsanas_rekins').click(processAtkapsanaClick);

    new_row.find('input.anulets').click(processAnuletsClick);
    new_row.find('input.nodots_piedz').click(processNodotsPiedzClick);

    recalc();
    recalcPVN();
    recreateRekiniNr();

    stripeTable($("table#rekinu_tabula"));

    $('#text_rekinu_skaits').html( c );

  });

  $('.autocalc').keyup(recalc);

  function recalc(){

    var PVN = parseInt(ctop($('#pvnset').val()));

    if($(this).attr('aidii') == 'field1'){

      $(this).removeClass('input_has_error');

      if(isNumber($(this).parent().parent().find('.field1').val())){

        sum = parseFloat(ctop($(this).parent().parent().find('.field1').val()));
        pvn = (sum*PVN)/100;

        $(this).parent().parent().find('.pvnplace').html( pvn.toFixed(2) + ' Ls' );
        $(this).parent().parent().find('.field2').val( parseFloat(sum + pvn).toFixed(2) );

      } else if ($(this).parent().parent().find('.field1').val() != '') {

        $(this).addClass('input_has_error');

      }
    }

    if($(this).attr('aidii') == 'field2'){

      $(this).removeClass('input_has_error');

      if(isNumber($(this).parent().parent().find('.field2').val())){

        sum = parseFloat(ctop($(this).parent().parent().find('.field2').val()));
        pvn = sum * (PVN/100) / (1+PVN/100);

        $(this).parent().parent().find('.pvnplace').html( pvn.toFixed(2) + ' Ls' );
        $(this).parent().parent().find('.field1').val( parseFloat(sum-pvn).toFixed(2) );

      }else if ($(this).parent().parent().find('.field2').val() != ''){

        $(this).addClass('input_has_error');

      }

    }

    globalsumm = 0;

    jQuery.each( $('.field2') , function(){
      if(isNumber($(this).val())) globalsumm += parseFloat(ctop($(this).val()));
    });

    $('#fullsumm').val(globalsumm.toFixed(2) + ' Ls');

    withoutpvnsumm = 0;

    jQuery.each( $('.field1') , function(){
      if(isNumber($(this).val())) withoutpvnsumm += parseFloat(ctop($(this).val()));
    });

    $('#fullsummwithoutpvn').val(withoutpvnsumm.toFixed(2));

    atmsumm = 0;

    jQuery.each( $('.apm_summa') , function(){

      if(isNumber($(this).html())) {
        atmsumm += parseFloat(ctop($(this).html()));
      }

    });

    $('#atmsumm').html(atmsumm.toFixed(2));

    if(isNumber(atmsumm) && isNumber(globalsumm)){
      $('#atmproc').html( (atmsumm/globalsumm*100).toFixed(1) );
    }else{
      $('#atmproc').html( "-" );
    }

    recalcCal();

  }
  
  $('.calendarcalc').change(recalcCal);

  function recalcCal(){

    jQuery.each($('.calendarcalc'), function() {

      if($(this).parent().parent().find('.anulets').is(':checked')){

        $(this).parent().parent().find('.date_status').html('Anulēts');

      } else if($(this).parent().parent().find('.nodots_piedz').is(':checked')){

        $(this).parent().parent().find('.date_status').html('Nodots piedz.');

      } else if ($(this).is(':text')) {

        var today = new Date();

        var apm_term = new Date();

        var parts = $(this).val().split('.');

        apm_term.setFullYear(parseInt(parts[2], 10), parseInt(parts[1], 10) - 1, parseInt(parts[0], 10),0,0,0);

        needSum = parseFloat(ctop($(this).parent().parent().find('.field2').val()));
        apmSum = parseFloat(ctop($(this).parent().parent().find('.apm_summa').html()));

        if(apmSum >= needSum) {

          if ($(this).parent().parent().find('.barteris').is(':checked')) {
            $(this).parent().parent().find('.date_status').html('Barteris');
          } else {
            $(this).parent().parent().find('.date_status').html('Apmaksāts');
          }

        } else if (apmSum == 0 || isNaN(apmSum)){

          if(today > apm_term){
            $(this).parent().parent().find('.date_status').html('Parāds!');
            calcSodaNauda($(this).parent().parent());
          }else{
            $(this).parent().parent().find('.date_status').html('Nav apm. termiņš');
          }

        } else { // Nav apmaksāts pilnā apmērā... skatamies kalendāru

          if(today > apm_term){
            $(this).parent().parent().find('.date_status').html('Parāds!');
            calcSodaNauda($(this).parent().parent());
          }else{
            $(this).parent().parent().find('.date_status').html('Daļēji apmaksāts');
          }

        }

      }

    });

  }

  function calcSodaNauda(rekins_row) {

    var apm_term = rekins_row.find('.apm_termins').val();

    var day_diff = days_between(apm_term, new Date());

    if (day_diff > 0) {

      var rek_sum = parseFloat(ctop(rekins_row.find('.field2').val()));
      var apm_sum = parseFloat(ctop(rekins_row.find('.apm_summa').html()));

      if (isNaN(rek_sum)) rek_sum = 0;
      if (isNaN(apm_sum)) apm_sum = 0;

      var diff_sum = rek_sum - apm_sum;

      var day_sum = diff_sum * <?= (isset($d['soda_proc']) ? $d['soda_proc'] : 0)  / 100 ?>;

      var soda_nauda = day_sum * day_diff;

      rekins_row.find('.soda_nauda').html('(' + day_diff + ')&nbsp;' + soda_nauda.toFixed(2) + '&nbsp;Ls');

    }

  }

  function recalcPVN(){

    var PVN = parseInt(ctop($('#pvnset').val()));

    jQuery.each( $('.field1') , function(){

      pvn = ( parseFloat(ctop($(this).val())) * PVN )/100;

      if(isNumber(pvn)) $(this).parent().parent().find('.pvnplace').html( pvn.toFixed(2) + ' Ls' );

    });

  };

  recalc();
  recalcPVN();

  $(".showcalendar").datepicker({dateFormat:'dd.mm.yy'});

  recalcCal();

  $('#rekinu_tabula input.anulets').click(processAnuletsClick);

  $('#rekinu_tabula input.nodots_piedz').click(processNodotsPiedzClick);

  $('#rekinu_tabula input.barteris').click(processBarterisClick);

  $('#rekinu_tabula input.atkapsanas_rekins').click(processAtkapsanaClick);

  function processAnuletsClick() {

    if (this.checked) {
      $(this).parents('.tr_rek').find('input.nodots_piedz').attr('checked', false);
    }

  }

  function processNodotsPiedzClick() {

    if (this.checked) {
      $(this).parents('.tr_rek').find('input.anulets').attr('checked', false);
    }

  }

  function processBarterisClick() {

    if (this.checked) {
      $(this).parents('.tr_rek').find('input.atkapsanas_rekins').attr('checked', false);
    }

    recreateRekiniNr();

  }

  function processAtkapsanaClick() {

    if (this.checked) {
      $(this).parents('.tr_rek').find('input.barteris').attr('checked', false);
    }

    recreateRekiniNr()

  }

});
</script>

<? show_files_block('lig_rekini', isset($_GET['id']) ? $_GET['id'] : null) ?>

<? show_comments_block('lig_rekini', isset($_GET['id']) ? $_GET['id'] : null) ?>