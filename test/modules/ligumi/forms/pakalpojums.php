<table width="600">

  <tr><td colspan="2"><h2>Pakalpojums</h2></td></tr>
  <tr><td width="100">Reklāma no:</td><td><input type="input" name="reklama_no" id="rekno" value="<?=!empty($d['reklama_no'])?date('d.m.Y', strtotime($d['reklama_no'])):''?>"></td></tr>
  <tr><td>Reklāma līdz:</td><td><input type="input" name="reklama_lidz" id="reklidz" value="<?=!empty($d['reklama_lidz'])?date('d.m.Y', strtotime($d['reklama_lidz'])):''?>"></td></tr>
  <tr><td>Nosaukums datubāzē:</td><td><input type="input" name="pak_nosaukums_db" value="<?=!empty($d['pak_nosaukums_db'])?$d['pak_nosaukums_db']:""?>"></td></tr>
  <tr><td colspan="2"><h2>Pakalpojumu saraksts</h2></td></tr>
  <tr>
    <td colspan="2">

      <button class="hidethistoo" onclick="return make_new_pakalp();">Pievienot jaunu</button>

      <div id="pakalpojuma_place">

        <? if(!empty($pakalpojumi)) { ?>

          <? foreach($pakalpojumi as $k => $v) { ?>

            <div>
              <select name="pakalpojumi[<?= $v['id'] ;?>][pakalpojuma_veida_id]">
                <?php foreach($_vars['pakalpojumi'] as $key=>$pva){ ?>
                  <option value="<?=$key;?>" <?=(($v['pakalpojuma_veida_id']==$key)?"selected":"");?>><?=$pva;?></option>
                <?php } ?>
              </select>
              <input type="input" name="pakalpojumi[<?= $v['id'] ;?>][pakalpojuma_apr]" value="<?=$v['pakalpojuma_apr'];?>" style="width:100px;">
              <button class="hidethistoo" onclick="$(this).parent().remove();">Dzēst</button>
            </div>

          <? } ?>

        <? } ?>

      </div>

    </td>
  </tr>

  <script>
  function make_new_pakalp() {
    $('#pakalpojuma_place').append( $('#pakalpojums_prot').html() );
    return false;
  }
  </script>


  <tr><td colspan="2"><h2>Reklāmas statuss</h2></td></tr>

  <tr>
    <td>Infomedia</td>
    <td>
      <select name="infomedia_statuss">
        <?php foreach($_vars['infomediastatuss'] as $k=>$p){ ?>
          <option value="<?=$k;?>" <?=($k==(!empty($d['infomedia_statuss'])?$d['infomedia_statuss']:1)?"selected":"")?>><?=$p;?></option>
        <?php } ?>
      </select>
    </td>
  </tr>
  <tr>
    <td>Google</td>
    <td>
      <select name="google_statuss">
        <?php foreach($_vars['googlestatuss'] as $k=>$p){ ?>
          <option value="<?=$k;?>" <?=($k==(!empty($d['google_statuss'])?$d['google_statuss']:1)?"selected":"")?>><?=$p;?></option>
        <?php } ?>
      </select>
    </td>
  </tr>
  <tr><td colspan="2"></td></tr>
  <tr><td><b>Google konts:</b></td><td><input type="input" name="pak_google_konts" value="<?=!empty($d['pak_google_konts'])?$d['pak_google_konts']:""?>"></td></tr>

  <tr><td colspan="2" style="padding-top:30px;"><h2>Pielikumi</h2></td></tr>

  <tr>
    <td width="50%"><b>Infomedia</b> (Atslēgvārdi Google)</td>
    <td><b>Tiešsaite</b> (Atslēgvārdi Google)</td>
  </tr>
  <tr class="hidethistoo">
    <td>Skaits: <input type="input" id="how_many_info" style="width:50px;"><button onclick="return add_info();" class="add_field_pielikums">Pievienot laukus</button></td>
    <td>Skaits: <input type="input" id="how_many_live" style="width:50px;"><button onclick="return add_live();" onClick="">Pievienot laukus</button></td>
  </tr>
  <tr>

    <td id="piel_info_place">
      <? if(!empty($pielikumi_infomedia)) { ?>
        <? foreach($pielikumi_infomedia as $med) { ?>
          <div style="padding-top: 5px; clear: both;">
            <input type="input" name="piel_infomedia[<?= $med['id'] ?>][atslegvards]" value="<?=$med['atslegvards'];?>" style="width:90%; float: left; margin-right: 5px;">
            <a href="javascript:;" class="hidethistoo" style="float:left;" onClick="$(this).parent().remove();">[x]</a>
          </div>
        <? } ?>
      <? } ?>
    </td>

    <td id="piel_ties_place">
      <? if(!empty($pielikumi_tiessaite)) { ?>
        <? foreach($pielikumi_tiessaite as $med){ ?>
          <div style="padding-top: 5px; clear: both;">
            <input type="input" name="piel_tiessaite[<?= $med['id'] ?>][atslegvards]" value="<?=$med['atslegvards'];?>" style="width:65%; float: left; margin-right: 5px;">
            <input type="input" name="piel_tiessaite[<?= $med['id'] ?>][domeni]" value="<?=$med['domeni'];?>" style="width:25%; float: left; margin-right: 5px;">
            <a href="javascript:;" onClick="$(this).parent().remove();" class="hidethistoo" style="float:left;">[x]</a>
          </div>
        <? } ?>
      <? } ?>
    </td>

  </tr>
  <tr>
    <td>
      Reklāmas materiāli:
      <select name="infomedia_reklamas_mat">
        <? $yes_no = array(1=>"Ir",2=>"Nav",3=>"Daļēji"); ?>
        <? foreach($yes_no as $k=>$p) { ?>
          <option value="<?=$k;?>" <?=($k==(!empty($d['infomedia_reklamas_mat'])?$d['infomedia_reklamas_mat']:2)?"selected":"")?>><?=$p;?></option>
        <? } ?>
      </select>
    </td>
    <td>
      Reklāmas materiāli:
      <select name="tiessaite_reklamas_mat">
        <? $yes_no = array(1=>"Ir",2=>"Nav",3=>"Daļēji"); ?>
        <? foreach($yes_no as $k=>$p) { ?>
          <option value="<?=$k;?>" <?=($k==(!empty($d['tiessaite_reklamas_mat'])?$d['tiessaite_reklamas_mat']:2)?"selected":"")?>><?=$p;?></option>
        <? } ?>
      </select>
    </td>
  </tr>

  <script>
  function add_info(){

    for(var i=1;i<=$('#how_many_info').val();i++){
      $('#piel_info_place').append( $('#piel_infomedia_prot').html() );
    }

    return false;

  };

  function add_live(){

    for(var i=1;i<=$('#how_many_live').val();i++){
      $('#piel_ties_place').append( $('#piel_tiessaite_prot').html() );
    }

    return false;

  };
  </script>

</table>

<? show_files_block('lig_pakalpojums', isset($_GET['id']) ? $_GET['id'] : null) ?>

<? show_comments_block('lig_pakalpojums', isset($_GET['id']) ? $_GET['id'] : null) ?>