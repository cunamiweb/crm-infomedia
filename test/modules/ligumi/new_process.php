<?php
if (!empty($_POST)) {

  $errors = validate_ligums_form($_POST);

  if (empty($errors)) {

    $_POST = trim_array($_POST);

    $ligumadat = !empty($_POST['ligumadat']) ? "'".esc(date('Y-m-d', strtotime($_POST['ligumadat'])))."'" : 'null';
    $reklama_no = !empty($_POST['reklama_no']) ? "'".esc(date('Y-m-d', strtotime($_POST['reklama_no'])))."'" : 'null';
    $reklama_lidz = !empty($_POST['reklama_lidz']) ? "'".esc(date('Y-m-d', strtotime($_POST['reklama_lidz'])))."'" : 'null';
    $sanemsanas_datums = !empty($_POST['sanemsanas_datums']) ? "'".esc(date('Y-m-d', strtotime($_POST['sanemsanas_datums'])))."'" : 'null';

    if (!empty($_POST['liguma_id'])) {

      $liguma_id = (int)$_POST['liguma_id'];

      $old_data = db_get_assoc(db_query("SELECT * FROM `".DB_PREF."ligumi` WHERE `id` = " . $liguma_id));

      $query = db_query("SELECT * FROM `".DB_PREF."rekini` WHERE `liguma_id` = " . $liguma_id . " ORDER BY id ASC");
      $old_rekini_data = array();

      while($row = db_get_assoc($query)) {
        $old_rekini_data[] = $row;
      }

      $auto_atslegts_disable = 'auto_atslegts_disable'; // do not change value
      if ($old_data['google_statuss'] == 4 && $_POST['google_statuss'] != 4 && $old_data['auto_atslegts'] == true) {
        $auto_atslegts_disable = 1;
      }

      $sql = "
        UPDATE `".DB_PREF."ligumi`
        SET
          ligumanr = '".esc($_POST['ligumanr'])."',
          ligumadat = ".$ligumadat.",
          nosaukums = '".esc($_POST['nosaukums'])."',
          regnr = '".esc($_POST['regnr'])."',
          pvnnr = '".esc($_POST['pvnnr'])."',
          faktaddr_city = '".esc($_POST['faktaddr_city'])."',
          faktaddr_street = '".esc($_POST['faktaddr_street'])."',
          faktaddr_index = '".esc($_POST['faktaddr_index'])."',
          juridaddr_city = '".esc($_POST['juridaddr_city'])."',
          juridaddr_street = '".esc($_POST['juridaddr_street'])."',
          juridaddr_index = '".esc($_POST['juridaddr_index'])."',
          pardeveja_id = ".(int)$_POST['pardeveja_id'].",
          reklama_no = ".$reklama_no.",
          reklama_lidz = ".$reklama_lidz.",
          pak_nosaukums_db = '".esc($_POST['pak_nosaukums_db'])."',
          sanemsanas_forma = ".(int)$_POST['sanemsanas_forma'].",
          sanemsanas_datums = ".$sanemsanas_datums.",
          pvn = ".(float)str_replace(',', '.', $_POST['pvn']).",
          infomedia_statuss = ".(int)$_POST['infomedia_statuss'].",
          google_statuss = ".(int)$_POST['google_statuss'].",
          pak_google_konts = '".esc($_POST['pak_google_konts'])."',
          infomedia_reklamas_mat = ".(int)$_POST['infomedia_reklamas_mat'].",
          tiessaite_reklamas_mat = ".(int)$_POST['tiessaite_reklamas_mat'].",
          auto_atslegts_disable = ".$auto_atslegts_disable.",
          soda_proc = ".(float)str_replace(',', '.', $_POST['soda_proc'])."
        WHERE id = ".$liguma_id."
        LIMIT 1
      ";

      db_query($sql);

    } else {

      $beig_ligumi = db_get_assoc_all(db_query("
        SELECT
          g.*,
          b.beigu_statuss
        FROM `".DB_PREF."ligumi` g
        LEFT JOIN `".DB_PREF."ligumi_beigusies` b ON (g.id = b.liguma_id)
        WHERE
          g.regnr LIKE '".esc($_POST['regnr'])."'
      "));

      $sql = "
        INSERT INTO `".DB_PREF."ligumi`
        SET
          ligumanr = '".esc($_POST['ligumanr'])."',
          ligumadat = ".$ligumadat.",
          nosaukums = '".esc($_POST['nosaukums'])."',
          regnr = '".esc($_POST['regnr'])."',
          pvnnr = '".esc($_POST['pvnnr'])."',
          faktaddr_city = '".esc($_POST['faktaddr_city'])."',
          faktaddr_street = '".esc($_POST['faktaddr_street'])."',
          faktaddr_index = '".esc($_POST['faktaddr_index'])."',
          juridaddr_city = '".esc($_POST['juridaddr_city'])."',
          juridaddr_street = '".esc($_POST['juridaddr_street'])."',
          juridaddr_index = '".esc($_POST['juridaddr_index'])."',
          pardeveja_id = ".(int)$_POST['pardeveja_id'].",
          sanemsanas_forma = ".(int)$_POST['sanemsanas_forma'].",
          sanemsanas_datums = ".$sanemsanas_datums.",
          pvn = ".(float)str_replace(',', '.', $_POST['pvn']).",
          reklama_no = ".$reklama_no.",
          reklama_lidz = ".$reklama_lidz.",
          pak_nosaukums_db = '".esc($_POST['pak_nosaukums_db'])."',
          infomedia_statuss = ".(int)$_POST['infomedia_statuss'].",
          google_statuss = ".(int)$_POST['google_statuss'].",
          pak_google_konts = '".esc($_POST['pak_google_konts'])."',
          infomedia_reklamas_mat = ".(int)$_POST['infomedia_reklamas_mat'].",
          tiessaite_reklamas_mat = ".(int)$_POST['tiessaite_reklamas_mat'].",
          soda_proc = ".(float)str_replace(',', '.', $_POST['soda_proc'])."
      ";

      db_query($sql);

      $liguma_id = db_last_id();

      foreach($beig_ligumi as $beig_ligums) {

        if (empty($beig_ligums['beigu_statuss'])) {

          $beig_lig_rekini = db_get_assoc_all(db_query("
            SELECT * FROM `".DB_PREF."rekini` WHERE liguma_id = " . $beig_ligums['id'] . "
          "));

          if (get_liguma_statuss($beig_ligums, $beig_lig_rekini) == 6) {

            $sql = "
              INSERT INTO `".DB_PREF."ligumi_beigusies`
              SET
                liguma_id = ".$beig_ligums['id'].",
                beigu_statuss = 'parslegts',
                parslegts_lig_nr = '".esc($_POST['ligumanr'])."',
                laboja_pardeveja_id = ".$_SESSION['user']['id'].",
                laboja_laiks = NOW()
            ";

            db_query($sql);

          }

        } else {

          if (in_array($beig_ligums['beigu_statuss'], array('termins', 'atteikums'))) {

            $sql = "
              UPDATE `".DB_PREF."ligumi_beigusies`
              SET
                beigu_statuss = 'parslegts',
                parslegts_lig_nr = '".esc($_POST['ligumanr'])."',
                atlikt_lidz = NULL,
                komentars = NULL,
                laboja_pardeveja_id = ".$_SESSION['user']['id'].",
                laboja_laiks = NOW()
              WHERE
                liguma_id = ".$beig_ligums['id']."
              LIMIT 1
            ";

            db_query($sql);

          }

        }

      }

    }

    if (!empty($_POST['liguma_id'])) {

      // deleting contacts

      $sql = "
        DELETE FROM `".DB_PREF."kontakti`
        WHERE
          liguma_id = ".$liguma_id."
          " . (!empty($_POST['kontakti']) ? "AND id NOT IN (".implode(',', array_keys($_POST['kontakti'])).") " : '')."
      ";

      db_query($sql);

      // updating contacts

      if (!empty($_POST['kontakti'])) {

        foreach($_POST['kontakti'] as $kontakta_id => $c) {

          $sql = "
            UPDATE `".DB_PREF."kontakti`
            SET
              vards = '".esc($c['vards'])."',
              epasts = '".esc($c['epasts'])."',
              telefons = '".esc($c['telefons'])."',
              ligumsledzejs = ".(!empty($c['ligumsledzejs']) ? 1 : 0)."
            WHERE id = ".$kontakta_id."
            LIMIT 1
          ";

          db_query($sql);

        }

      }

    }

    // adding contacts

    if (!empty($_POST['kontakti_new']['vards'])) {

      foreach($_POST['kontakti_new']['vards'] as $i => $vards) {

        $telefons = $_POST['kontakti_new']['telefons'][$i];
        $epasts = $_POST['kontakti_new']['epasts'][$i];
        $ligumsledzejs = !empty($_POST['kontakti_new']['ligumsledzejs'][$i]) ? 1 : 0;

        $sql = "
          INSERT INTO `".DB_PREF."kontakti` (
            liguma_id,
            vards,
            epasts,
            telefons,
            ligumsledzejs
          ) VALUES (
            ".$liguma_id.",
            '".esc($vards)."',
            '".esc($epasts)."',
            '".esc($telefons)."',
            ".$ligumsledzejs."
          )
        ";

        db_query($sql);

      }

    }

    if (!empty($_POST['liguma_id'])) {

      // deleting old services

      $sql = "
        DELETE FROM `".DB_PREF."pakalpojumi`
        WHERE
          liguma_id = ".$liguma_id."
          " . (!empty($_POST['pakalpojumi']) ? "AND id NOT IN (".implode(',', array_keys($_POST['pakalpojumi'])).") " : '')."
      ";

      db_query($sql);

      // updating services

      if (!empty($_POST['pakalpojumi'])) {

        foreach($_POST['pakalpojumi'] as $pakalpojuma_id => $p) {

          $sql = "
            UPDATE `".DB_PREF."pakalpojumi`
            SET
              pakalpojuma_veida_id = ".(int)$p['pakalpojuma_veida_id'].",
              pakalpojuma_apr = '".esc($p['pakalpojuma_apr'])."'
            WHERE id = ".$pakalpojuma_id."
            LIMIT 1
          ";

          db_query($sql);

        }

      }

    }

    // adding services

    if (!empty($_POST['pakalpojumi_new']['pakalpojuma_veida_id'])) {

      foreach($_POST['pakalpojumi_new']['pakalpojuma_veida_id'] as $i => $pakalpojuma_veida_id) {

        $pakalpojuma_apr = $_POST['pakalpojumi_new']['pakalpojuma_apr'][$i];

        $sql = "
          INSERT INTO `".DB_PREF."pakalpojumi` (
            liguma_id,
            pakalpojuma_veida_id,
            pakalpojuma_apr
          ) VALUES (
            ".$liguma_id.",
            ".(int)$pakalpojuma_veida_id.",
            '".esc($pakalpojuma_apr)."'
          )
        ";

        db_query($sql);

      }

    }

    if (!empty($_POST['liguma_id'])) {

      // deleting old pielikumi_infomedia

      $sql = "
        DELETE FROM `".DB_PREF."pielikumi_infomedia`
        WHERE
          liguma_id = ".$liguma_id."
          " . (!empty($_POST['piel_infomedia']) ? "AND id NOT IN (".implode(',', array_keys($_POST['piel_infomedia'])).") " : '')."
      ";

      db_query($sql);

      // updating pielikumi_infomedia

      if (!empty($_POST['piel_infomedia'])) {

        foreach($_POST['piel_infomedia'] as $piel_infomedia_id => $p) {

          $sql = "
            UPDATE `".DB_PREF."pielikumi_infomedia`
            SET
              atslegvards = '".esc($p['atslegvards'])."'
            WHERE id = ".$piel_infomedia_id."
            LIMIT 1
          ";

          db_query($sql);

        }

      }

    }

    // adding pielikumi_infomedia

    if (!empty($_POST['piel_infomedia_new']['atslegvards'])) {

      foreach($_POST['piel_infomedia_new']['atslegvards'] as $i => $atslegvards) {

        $sql = "
          INSERT INTO `".DB_PREF."pielikumi_infomedia` (
            liguma_id,
            atslegvards
          ) VALUES (
            ".$liguma_id.",
            '".esc($atslegvards)."'
          )
        ";

        db_query($sql);

      }

    }


    if (!empty($_POST['liguma_id'])) {

      // deleting old pielikumi_tiessaite

      $sql = "
        DELETE FROM `".DB_PREF."pielikumi_tiessaite`
        WHERE
          liguma_id = ".$liguma_id."
          " . (!empty($_POST['piel_tiessaite']) ? "AND id NOT IN (".implode(',', array_keys($_POST['piel_tiessaite'])).") " : '')."
      ";

      db_query($sql);

      // updating pielikumi_tiessaite

      if (!empty($_POST['piel_tiessaite'])) {

        foreach($_POST['piel_tiessaite'] as $piel_tiessaite_id => $p) {

          $sql = "
            UPDATE `".DB_PREF."pielikumi_tiessaite`
            SET
              atslegvards = '".esc($p['atslegvards'])."',
              domeni = '".esc($p['domeni'])."'
            WHERE id = ".$piel_tiessaite_id."
            LIMIT 1
          ";

          db_query($sql);

        }

      }

    }

    // adding pielikumi_tiessaite

    if (!empty($_POST['piel_tiessaite_new']['atslegvards'])) {

      foreach($_POST['piel_tiessaite_new']['atslegvards'] as $i => $atslegvards) {

        $domeni = $_POST['piel_tiessaite_new']['domeni'][$i];

        $sql = "
          INSERT INTO `".DB_PREF."pielikumi_tiessaite` (
            liguma_id,
            atslegvards,
            domeni
          ) VALUES (
            ".$liguma_id.",
            '".esc($atslegvards)."',
            '".esc($domeni)."'
          )
        ";

        db_query($sql);

      }

    }

    if (!empty($_POST['liguma_id'])) {

      // updating reminders

      if (!empty($_POST['atgadinajumi'])) {

        foreach($_POST['atgadinajumi'] as $atgadinajuma_id => $a) {

          $sql = "
            UPDATE `".DB_PREF."atgadinajumi`
            SET
              datums = '".esc(date('Y-m-d', strtotime($a['datums'])))."',
              tema = ".(int)$a['tema'].",
              saturs = '".esc($a['saturs'])."'
            WHERE id = ".$atgadinajuma_id."
            LIMIT 1
          ";

          db_query($sql);

        }

      }

    }

    // adding reminders

    if (!empty($_POST['atgadinajumi_new']['datums'])) {

      foreach($_POST['atgadinajumi_new']['datums'] as $i => $datums) {

        $tema = $_POST['atgadinajumi_new']['tema'][$i];
        $saturs = $_POST['atgadinajumi_new']['saturs'][$i];

        $sql = "
          INSERT INTO `".DB_PREF."atgadinajumi` (
            liguma_id,
            datums,
            tema,
            saturs,
            created_pardeveja_id
          ) VALUES (
            ".$liguma_id.",
            '".esc(date('Y-m-d', strtotime($datums)))."',
            ".(int)$tema.",
            '".esc($saturs)."',
            ".$_SESSION['user']['id']."
          )
        ";

        db_query($sql);

      }

    }


    if (!empty($_POST['liguma_id'])) {

      // deleting old invoices

      $sql = "
        DELETE FROM `".DB_PREF."rekini`
        WHERE
          liguma_id = ".$liguma_id."
          " . (!empty($_POST['rekini']) ? "AND id NOT IN (".implode(',', array_keys($_POST['rekini'])).") " : '')."
      ";

      db_query($sql);

      // updating invoices

      if (!empty($_POST['rekini'])) {

        foreach($_POST['rekini'] as $rekina_id => $r) {

          $rek_nr = !empty($r['rek_nr']) ? "'" . esc($r['rek_nr']) . "'" : null;
          $izr_datums = !empty($r['izr_datums']) ? "'".date('Y-m-d', strtotime($r['izr_datums']))."'" : 'null';
          $summa = (strlen($r['summa']) != 0) ? (float) str_replace(',', '.', $r['summa']) : 'null';
          $kopsumma = (strlen($r['kopsumma']) != 0) ? (float) str_replace(',', '.', $r['kopsumma']) : 'null';
          $apm_soda_summa = (strlen($r['apm_soda_summa']) != 0) ? (float) str_replace(',', '.', $r['apm_soda_summa']) : 'null';
          $apm_termins = !empty($r['apm_termins']) ? "'".date('Y-m-d', strtotime($r['apm_termins']))."'" : 'null';
          $anul_datums = !empty($r['anul_datums']) ? "'".date('Y-m-d', strtotime($r['anul_datums']))."'" : 'null';
          $nodots_piedz_datums = !empty($r['nodots_piedz_datums']) ? "'".date('Y-m-d', strtotime($r['nodots_piedz_datums']))."'" : 'null';

          $sql = "
            UPDATE `".DB_PREF."rekini`
            SET
              rek_nr = ".$rek_nr.",
              izr_datums = ".$izr_datums.",
              statuss = ".(int)$r['statuss'].",
              summa = ".$summa.",
              kopsumma = ".$kopsumma.",
              apm_soda_summa = ".$apm_soda_summa.",
              apm_termins = ".$apm_termins.",
              anulets = ".(!empty($r['anulets']) ? 1 : 0).",
              anul_datums = ".$anul_datums.",
              nodots_piedz = ".(!empty($r['nodots_piedz']) ? 1 : 0).",
              nodots_piedz_datums = ".$nodots_piedz_datums.",
              barteris = ".(!empty($r['barteris']) ? 1 : 0).",
              atkapsanas_rekins = ".(!empty($r['atkapsanas_rekins']) ? 1 : 0)."
            WHERE id = ".$rekina_id."
            LIMIT 1
          ";

          db_query($sql);

        }

      }

    }

    // adding invoices

    if (!empty($_POST['rekini_new']['izr_datums'])) {

      foreach($_POST['rekini_new']['izr_datums'] as $i => $izr_datums) {

        $rek_nr = !empty($_POST['rekini_new']['rek_nr'][$i]) ? "'".$_POST['rekini_new']['rek_nr'][$i]."'" : 'null';
        $statuss = $_POST['rekini_new']['statuss'][$i];
        $summa = (strlen($_POST['rekini_new']['summa'][$i]) != 0) ? (float) str_replace(',', '.', $_POST['rekini_new']['summa'][$i]) : 'null';
        $kopsumma = (strlen($_POST['rekini_new']['kopsumma'][$i]) != 0) ? (float) str_replace(',', '.', $_POST['rekini_new']['kopsumma'][$i]) : 'null';
        $anulets = !empty($_POST['rekini_new']['anulets'][$i]) ? 1 : 0;
        $anul_datums = !empty($_POST['rekini_new']['anul_datums'][$i]) ? "'".date('Y-m-d', strtotime($_POST['rekini_new']['anul_datums'][$i]))."'" : 'null';
        $nodots_piedz = !empty($_POST['rekini_new']['nodots_piedz'][$i]) ? 1 : 0;
        $nodots_piedz_datums = !empty($_POST['rekini_new']['nodots_piedz_datums'][$i]) ? "'".date('Y-m-d', strtotime($_POST['rekini_new']['nodots_piedz_datums'][$i]))."'" : 'null';
        $barteris = !empty($_POST['rekini_new']['barteris'][$i]) ? 1 : 0;
        $atkapsanas_rekins = !empty($_POST['rekini_new']['atkapsanas_rekins'][$i]) ? 1 : 0;

        $izr_datums = !empty($izr_datums) ? "'".date('Y-m-d', strtotime($izr_datums))."'" : 'null';
        $apm_termins = !empty($_POST['rekini_new']['apm_termins'][$i]) ? "'".date('Y-m-d', strtotime($_POST['rekini_new']['apm_termins'][$i]))."'" : 'null';

        if ($barteris) {
          if ($izr_datums == 'null') $izr_datums = 'NOW()';
          if ($apm_termins == 'null') $apm_termins = 'NOW()';
        }

        $sql = "
          INSERT INTO `".DB_PREF."rekini`
          SET
            rek_nr = ".$rek_nr.",
            liguma_id = ".$liguma_id.",
            izr_datums = ".$izr_datums.",
            statuss = ".(int)$statuss.",
            summa = ".$summa.",
            kopsumma = ".$kopsumma.",
            apm_termins = ".$apm_termins.",
            anulets = ".$anulets.",
            anul_datums = ".$anul_datums.",
            nodots_piedz = ".$nodots_piedz.",
            nodots_piedz_datums = ".$nodots_piedz_datums.",
            barteris = ".$barteris.",
            atkapsanas_rekins = ".$atkapsanas_rekins."
        ";

        if (db_query($sql) && $barteris) {

          $new_rek_id = db_last_id();

          $result = db_query("
            INSERT INTO `".DB_PREF."rekini_maksajumi` (
              rekina_id,
              datums,
              summa,
              created_pardeveja_id
            ) VALUES (
              ".$new_rek_id.",
              NOW(),
              ".(float)$kopsumma.",
              ".$_SESSION['user']['id']."
            )
          ");

          if ($result) {

            db_query("
              UPDATE `".DB_PREF."rekini`
              SET
                apm_summa = IFNULL(apm_summa, 0) + ".(float)$kopsumma."
              WHERE id = ".$new_rek_id."
              LIMIT 1
            ");

          }

        }

      }

    }

    // updating invoice deliveries

    $sql = "
      DELETE FROM `".DB_PREF."ligumi_rekinu_piegades`
      WHERE liguma_id = ".$liguma_id."
    ";

    db_query($sql);

    if (!empty($_POST['piegades'])) {

      foreach($_POST['piegades'] as $k => $p) {

        if (!empty($p['check'])) {

          $sql = "
            INSERT INTO `".DB_PREF."ligumi_rekinu_piegades`
            SET
              liguma_id = ".$liguma_id.",
              piegades_veids = ".(int)$k.",
              adrese = '".esc($p['adrese'])."'
          ";

          db_query($sql);

        }

      }

    }

    $form_names = array(
      'lig_klients',
      'lig_pakalpojums',
      'lig_rekini',
      'lig_atgadinajumi',
    );

    // inserting files

    process_files($form_names, $liguma_id);

    // inserting comments

    process_comments($form_names, $liguma_id);


    // after everything is done, add reminders if needed

    $new_data = db_get_assoc(db_query("SELECT * FROM `".DB_PREF."ligumi` WHERE `id` = " . $liguma_id));

    $beigu_statuss_data = db_get_assoc(db_query("SELECT * FROM `".DB_PREF."ligumi_beigusies` WHERE `liguma_id` = " . $liguma_id));

    $query = db_query("SELECT * FROM `".DB_PREF."rekini` WHERE `liguma_id` = " . $liguma_id . " ORDER BY id ASC");
    $new_rekini_data = array();

    while($row = db_get_assoc($query)) {
      $new_rekini_data[] = $row;
    }

    if (empty($old_data)) {

      generate_auto_reminders($new_data, $beigu_statuss_data, $new_rekini_data);

    } else {

      $old_rekl_statuss = get_reklamas_statuss($old_data);
      $new_rekl_statuss = get_reklamas_statuss($new_data);

      $old_apm_statuss = get_apmaksas_statuss($old_rekini_data);
      $new_apm_statuss = get_apmaksas_statuss($new_rekini_data);

      if ($old_rekl_statuss != $new_rekl_statuss || $old_data['google_statuss'] != $new_data['google_statuss'] || $old_apm_statuss != $new_apm_statuss) {

        generate_auto_reminders($new_data, $beigu_statuss_data, $new_rekini_data);

      }

    }

    header('Location: ?c=ligumi&a=labot&id=' . $liguma_id . '&subtab=' . $subtab);
    die();

  }

}
?>