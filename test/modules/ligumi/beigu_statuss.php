<?
if (empty($_GET['liguma_id'])) {
  die();
}

$liguma_id = (int) $_GET['liguma_id'];

$lig_data = db_get_assoc(db_query("SELECT * FROM `".DB_PREF."ligumi` WHERE `id` = " . $liguma_id));

if (empty($lig_data)) {
  die();
}

if (!is_admin() && $lig_data['pardeveja_id'] != $_SESSION['user']['id']) {
  die('Nav tiesību apskatīt līgumu!');
}

$stat_data = db_get_assoc(db_query("SELECT * FROM `".DB_PREF."ligumi_beigusies` WHERE `liguma_id` = " . $liguma_id));

if (!empty($_POST)) {

  $_POST = trim_array($_POST);

  $errors = validate_beigu_statuss_form($_POST);

  if (empty($errors)) {

    $beigu_statuss = "'" . $_POST['beigu_statuss'] . "'";
    $parslegts_lig_nr = 'null';
    $atlikt_lidz = 'null';
    $komentars = 'null';

    if ($_POST['beigu_statuss'] == 'parslegts') {
      $parslegts_lig_nr = "'".$_POST['parslegts_lig_nr']."'";
    } elseif ($_POST['beigu_statuss'] == 'termins') {
      $atlikt_lidz = "'" . date('Y-m-d', strtotime($_POST['atlikt_lidz'])) . "'";
      $komentars = "'".$_POST['komentars1']."'";
    } elseif ($_POST['beigu_statuss'] == 'atteikums') {
      $komentars = "'".$_POST['komentars2']."'";
    }

    if (!empty($stat_data)) {

      db_query("
        UPDATE `".DB_PREF."ligumi_beigusies`
        SET
          beigu_statuss = ".$beigu_statuss.",
          parslegts_lig_nr = ".$parslegts_lig_nr.",
          atlikt_lidz = ".$atlikt_lidz.",
          komentars = ".$komentars.",
          laboja_pardeveja_id = ".$_SESSION['user']['id'].",
          laboja_laiks = NOW()
        WHERE id = ".$stat_data['id']."
        LIMIT 1
      ");

    } else {

      db_query("
        INSERT INTO `".DB_PREF."ligumi_beigusies`
        SET
          liguma_id = ".$liguma_id.",
          beigu_statuss = ".$beigu_statuss.",
          parslegts_lig_nr = ".$parslegts_lig_nr.",
          atlikt_lidz = ".$atlikt_lidz.",
          komentars = ".$komentars.",
          laboja_pardeveja_id = ".$_SESSION['user']['id'].",
          laboja_laiks = NOW()
      ");

    }

    ?>
    <script type="text/javascript">
    parent.document.location.reload();
    </script>
    <?
    die();

  }

}

?>

<div class="beigu_statuss_dialog">

  <div style="text-align: right; height: 20px;">

    <div>
      <button onClick="return editoradd(this)">Labot</button>
    </div>

    <div id="loader_placeholder"></div>

  </div>



  <form style="clear: both;" id="fullformplace" action="?c=ligumi&a=beigu_statuss&liguma_id=<?= $liguma_id ?>&without_nav=1" method="post">

    <div class="row">
      <div class="col radio">
        <input type="radio" name="beigu_statuss" value="parslegts" <?= ($stat_data['beigu_statuss'] == 'parslegts') ? 'checked="checked"' : '' ?> /> Pārslēgts
      </div>
      <div class="col <?= ($stat_data['beigu_statuss'] != 'parslegts') ? 'hidethistoo' : '' ?>">
        Jaunā līguma nr.:
        <input style="width: 90%;" type="text" name="parslegts_lig_nr" value="<?= $stat_data['parslegts_lig_nr'] ?>" />
      </div>
    </div>

    <div class="row">
      <div class="col radio">
        <input type="radio" name="beigu_statuss" value="termins" <?= ($stat_data['beigu_statuss'] == 'termins') ? 'checked="checked"' : '' ?> /> Termiņš
      </div>
      <div class="col <?= ($stat_data['beigu_statuss'] != 'termins') ? 'hidethistoo' : '' ?>" >
        Atlikt līdz:<br />
        <input style="width: 90%;" type="text" name="atlikt_lidz" value="<?= isset($stat_data['atlikt_lidz']) ? date('d.m.Y', strtotime($stat_data['atlikt_lidz'])) : '' ?>" />
      </div>
      <div class="col <?= ($stat_data['beigu_statuss'] != 'termins') ? 'hidethistoo' : '' ?>" style="width: 50%;">
        Kāpēc:<br />
        <textarea name="komentars1" cols="30" rows="2"><?= ($stat_data['beigu_statuss'] == 'termins') ? $stat_data['komentars'] : '' ?></textarea>
      </div>
    </div>

    <div class="row" style="margin-bottom: 2em;">
      <div class="col radio">
        <input type="radio" name="beigu_statuss" value="atteikums" <?= ($stat_data['beigu_statuss'] == 'atteikums') ? 'checked="checked"' : '' ?> /> Atteikums
      </div>
      <div class="col <?= ($stat_data['beigu_statuss'] != 'atteikums') ? 'hidethistoo' : '' ?>" style="width: 75%;">
        Kāpēc:<br />
        <textarea name="komentars2" cols="60" rows="2"><?= ($stat_data['beigu_statuss'] == 'atteikums') ? $stat_data['komentars'] : '' ?></textarea>
      </div>
    </div>

    <div style="text-align: center; clear: both;" class="hidethistoo">
      <button class="ui-state-default ui-corner-all" style="" onClick="return saveAll(this, 'beigu_statuss')">Saglabāt</button>
    </div>

  </form>

</div>

<script type="text/javascript">

editordisable();
$("input[name=atlikt_lidz]").datepicker({dateFormat:'dd.mm.yy'});
</script>