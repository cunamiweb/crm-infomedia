<?php

$a = empty($_GET['a']) ? "" : $_GET['a'];

if (!is_admin() && !in_array($a, array('labot', 'nenom_statusi', 'beigu_statuss'))) {
  die();
}

switch($a){

  case "jauns": $include = "new.php";  break;
  case "labot": $include = "new.php";  break;
  case "gen_rek": $include = "gen_rek.php";  break;
  case "rek_apmaksas": $include = "rek_apmaksas.php";  break;
  case "nenom_statusi": $include = "nenom_statusi.php";  break;
  case "beigu_statuss": $include = "beigu_statuss.php";  break;  

  default: $include = "list.php";

}

include($include);

?>