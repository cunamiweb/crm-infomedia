<?php
if (!empty($_GET['c'])) {
  $c = strtolower($_GET['c']);
} else {
  $c = is_admin() ? 'ligumi' : 'darbinieki';
}
?>

<?php if (empty($_GET['without_nav'])) { ?>

  <center>
  <div style="width:100%;" id="page_top">

    <div class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all" style="padding: 6px 9px 5px 7px; margin-bottom:13px;">

      <form class="top-quick-search" action="" method="get">
        <? if (is_admin()) { ?>
          <input type="hidden" name="c" value="ligumi" />
          <input type="text" name="fsearch" value="<?= ($c == 'ligumi') ? getGet('fsearch') : '' ?>" />
          <input class="ui-state-default ui-corner-all" style="width: 8em;" type="submit" value="Meklēt klientu" />
        <? } ?>
      </form>

      <div style="float:right; width: 300px; text-align: right;">
        <span style="margin-right: 20px;">Sveiks, <?=$_vars['sys_pardeveji'][$_SESSION['user']['id']];?>!</span>
        <a href="?logout=true">Izlogoties</a>
      </div>

      <div class="main-menu">

        <? if (is_admin()) { ?>
          <a <?= ($c == 'ligumi' && (!isset($_GET['a']) || $_GET['a'] != 'nenom_statusi')) ? 'class="active"' : '' ?> href="?c=ligumi">Klienti</a> |
        <? } ?>

        <a <?= ($c == 'darbinieki') ? 'class="active"' : '' ?> href="?c=darbinieki"><?= is_admin() ? 'Darbinieki' : 'Kopsavilkums' ?></a>

        <? if (is_admin()) { ?>
          | <a <?= ($c == 'atgadinajumi') ? 'class="active"' : '' ?> href="?c=atgadinajumi">Atgādinājumi</a>
        <? } ?>

        | <a <?= ($c == 'forums') ? 'class="active"' : '' ?> href="?c=forums">Forums</a>

        <? $nenom_statusi = get_nenomainitie_statusi(array('pardeveja_id' => $_SESSION['user']['id'])) ?>

        <? if (!is_admin() && !empty($nenom_statusi)) { ?>
          | <a <?= ($c == 'ligumi' && isset($_GET['a']) && $_GET['a'] == 'nenom_statusi') ? 'class="active"' : '' ?> style="color: red;" href="?c=ligumi&amp;a=nenom_statusi">Nenomainītie statusi (<?= count($nenom_statusi) ?>)</a>
        <? } ?>

      </div>

    </div>

    <?php } ?>

    <?php

    $default = is_admin() ? 'modules/ligumi/ligumi.php' : 'modules/darbinieki/darbinieki.php';

    switch($c){

      case "atgadinajumi": include("modules/atgadinajumi/atgadinajumi.php"); break;
      case "darbinieki": include("modules/darbinieki/darbinieki.php"); break;
      case "forums": include("modules/forums/forums.php"); break;
      case "ligumi": include("modules/ligumi/ligumi.php"); break;
      default: include($default); break;

    }

    ?>

    <?php if (empty($_GET['without_nav'])) { ?>

  </div>
  </center>

<?php } ?>