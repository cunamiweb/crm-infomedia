<?php
if (!empty($_POST)) {

  $_POST = trim_array($_POST);

  if(!empty($_POST['login']) && !empty($_POST['user']) && !empty($_POST['pass'])) {

    $login_fail = true;

    $sql = "
      SELECT *
      FROM `".DB_PREF."pardeveji`
      WHERE lietotajvards = '".esc($_POST['user'])."'
      LIMIT 1
    ";

    $user_data = db_get_assoc(db_query($sql));

    if(!empty($user_data['parole']) && $user_data['parole'] === md5($_POST['pass'])){

      $_SESSION['login'] = true;

      unset($user_data['parole']);

      $_SESSION['user'] = $user_data;

      if (!is_admin() && get_nenomainitie_statusi(array('pardeveja_id' => $_SESSION['user']['id']))) {
        header("Location: ?c=ligumi&a=nenom_statusi");
        die();
      }

      header("Location: ./");
      die();

    }

  }

}
?>

<script  type="text/javascript">
$.jStorage.flush();
</script>

<center style="margin-top: 150px;">

  <? if (!empty($login_fail)) { ?>
    <p style="color: red;">Nepareizs lietotājvārds un/vai parole!</p>
  <? } ?>

  <? if (!empty($_SESSION['auto_logout'])) { ?>
    <p style="color: red;">Jūs tikāt automātiski izlogots, jo ilgāku laiku neko nedarījāt sistēmā!</p>
    <? unset($_SESSION['auto_logout']); ?>
  <? } ?>

  <div style="width:200px;">
    <form method="post" action="">
      <input type="hidden" name="login" value="true"><br>
      Lietotājvārds:<br>
      <input type="input" name="user"><br>
      Parole:<br>
      <input type="password" name="pass"><br><br>
      <input type="submit" value="Ielogoties">
    </form>
  </div>

</center>