<?
require('list_process.php');
?>

<form action="" method="get" id="period_filter">
  <input type="hidden" name="c" value="darbinieki" />
  <div style="float: left; text-align: left;">
    Periods:
    <span><input id="date_from" name="date_from" type="input" style="width:80px;" value="<?= !empty($_GET['date_from']) ? $_GET['date_from'] : '' ?>" />
    -
    <input id="date_to" name="date_to" type="input" style="width:80px;" value="<?= !empty($_GET['date_to']) ? $_GET['date_to'] : '' ?>">
    <a class="clear_form" href="#">x</a></span>
  </div>
</form>


<? if (is_admin()) { ?>
  <button style="float: right;" onclick="document.location='?c=darbinieki&a=jauns'" class="ui-state-default ui-corner-all">Pievienot jaunu darbinieku</button>
<? } ?>

<script>
var searchlist = [];

var sp = create_filter_field_array([
  's_vards', 's_ligumu_sk', 's_lig_kopsumma', 's_lig_vid_summa',
  's_apm_lig_kopsumma', 's_vid_prov_proc', 's_apr_prov', 's_vid_apr_prov',
  's_apmaksajama_prov', 's_apmaksajama_prov_percent', 's_izm_prov',
  's_rek_apm_percent', 's_dept_cred', 's_akt_atg'
]);
</script>

<? // Prototypes // ?>
<div style="display:none;">

  <div id="komentarsprot__darb_kopsavilkums">
    <table class="comment_form">
      <tr>
        <td width="70">Komentārs:</td>
        <td>
          <textarea name="komentari_new[_darb_kopsavilkums][komentars][]" rows="2" cols="30"></textarea>
        </td>
      </tr>
    </table>
  </div>

</div>

<table style="display:none;">

  <tr id="darbinieki_kopsavilkums_filter">

    <th><span><input class="search_field" meth="standart" searchclass="s_vards" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <th><span><input class="search_field" meth="int" searchclass="s_ligumu_sk" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <th><span><input class="search_field" meth="coin" searchclass="s_lig_kopsumma" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <th><span><input class="search_field" meth="coin" searchclass="s_lig_vid_summa" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <th><span><input class="search_field" meth="coin" searchclass="s_apm_lig_kopsumma" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <th><span><input class="search_field" meth="coin" searchclass="s_vid_prov_proc" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <th><span><input class="search_field" meth="coin" searchclass="s_apr_prov" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <th><span><input class="search_field" meth="coin" searchclass="s_vid_apr_prov" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <th><span><input class="search_field" meth="coin" searchclass="s_apmaksajama_prov" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <th><span><input class="search_field" meth="coin" searchclass="s_apmaksajama_prov_percent" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <th><span><input class="search_field" meth="coin" searchclass="s_izm_prov" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <th><span><input class="search_field" meth="coin" searchclass="s_rek_apm_percent" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <th><span><input class="search_field" meth="coin" searchclass="s_dept_cred" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>

    <th>
      <select class="search_field advonchange" searchclass="s_akt_atg" meth="int">
        <option value=""></option>
        <option value="1">Ir</option>
        <option value="2">Nav</option>
      </select>
    </th>

  </tr>

</table>


<table width="100%" cellpadding="3" cellspacing="0" id="darbinieki_kopsavilkums" class="data ckey_darbinieki_list">

  <thead>

    <tr>
      <th>Darbinieks</th>
      <th>Līgumu sk.</th>
      <th>Līg. kopsumma</th>
      <th>Līg. vid. summa</th>
      <th>Apmaksātā līg. kopsumma</th>
      <th>Provīzija % (vid.)</th>
      <th>Apr. provīzija Ls</th>
      <th>Vid. apr. provīzija Ls</th>
      <th>Apmaksātā provīzija Ls</th>
      <th>Apmaksātā provīzija&nbsp;%</th>
      <th>Izmaksātā provīz. Ls</th>
      <th>Rēķinu apmaksa&nbsp;%</th>
      <th>Debepts/Kredīts</th>
      <th>Aktīvie atgādinājumi</th>
    </tr>

  </thead>

  <tbody class="main">

    <?php

    $period_where = array();

    if (!empty($_GET['date_from'])) {
      $period_where[] = "g.ligumadat >= '".esc(date('Y-m-d', strtotime($_GET['date_from'])))."'";
    }

    if (!empty($_GET['date_to'])) {
      $period_where[] = "g.ligumadat <= '".esc(date('Y-m-d', strtotime($_GET['date_to'])))."'";
    }

    $ligumu_summa = array();
    $katra_liguma_summa = array();
    $katra_liguma_summa_bez_pvn = array();
    $katra_liguma_apm_summa = array();
    $katra_liguma_apm_summa_bez_pvn = array();
    $ligumu_apm_summa = array();
    $prov_proc_summa = array();
    $apr_prov = array();
    $apmaksajama_prov = array();
    $izmaks_prov = array();

    $sql = "
      SELECT p.*
      FROM `".DB_PREF."pardeveji` p
    ";

    if (!is_admin()) {
      $sql .= "WHERE p.id = " . (int)$_SESSION['user']['id'];
    }

    $sql .= "
      ORDER BY p.`vards` ASC
    ";

    $query = db_query($sql);

    while($row = db_get_assoc($query)) {

      $data[] = $row;

      $ligumu_summa[$row['id']] = 0;
      $ligumu_apm_summa[$row['id']] = 0;
      $prov_proc_summa[$row['id']] = 0;
      $izmaks_prov[$row['id']] = 0;

    }

    // -----

    $sql = "
      SELECT g.*
      FROM `".DB_PREF."ligumi` g
    ";

    $lig_where = $period_where;
    if (!is_admin()) {
      $lig_where[] = "pardeveja_id = " . (int)$_SESSION['user']['id'];
    }

    if (!empty($lig_where)) {
      $sql .= "WHERE " . implode(" AND ", $lig_where);
    }

    $query = db_query($sql);
    $ligumi = array();

    while($row = db_get_assoc($query)){

      $ligumi[$row['pardeveja_id']][$row['id']] = $row;

      $prov_proc_summa[$row['pardeveja_id']] += $row['provizija_pardevejam'];

      $katra_liguma_summa[$row['pardeveja_id']][$row['id']] = 0;
      $katra_liguma_summa_bez_pvn[$row['pardeveja_id']][$row['id']] = 0;
      $katra_liguma_apm_summa[$row['pardeveja_id']][$row['id']] = 0;
      $katra_liguma_apm_summa_bez_pvn[$row['pardeveja_id']][$row['id']] = 0;

    }

    // -----

    $sql = "
      SELECT
        r.*,
        g.pardeveja_id
      FROM `".DB_PREF."rekini` r
      LEFT JOIN `".DB_PREF."ligumi` g ON (r.liguma_id = g.id)
    ";

    $rek_where = $period_where;

    $rek_where[] = 'r.atkapsanas_rekins = 0';

    if (!is_admin()) {
      $rek_where[] = "g.pardeveja_id = " . (int)$_SESSION['user']['id'];
    }

    if (!empty($rek_where)) {
      $sql .= "WHERE " . implode(" AND ", $rek_where);
    }

    $query = db_query($sql);
    $lig_rekini = array();

    while($row = db_get_assoc($query)){

      $lig_rekini[$row['pardeveja_id']][] = $row;

      $ligumu_summa[$row['pardeveja_id']] += $row['kopsumma'];
      $ligumu_apm_summa[$row['pardeveja_id']] += $row['apm_summa'];

      $katra_liguma_summa[$row['pardeveja_id']][$row['liguma_id']] += $row['kopsumma'];
      $katra_liguma_summa_bez_pvn[$row['pardeveja_id']][$row['liguma_id']] += $row['kopsumma'] / (1 + ($ligumi[$row['pardeveja_id']][$row['liguma_id']]['pvn'] / 100));
      $katra_liguma_apm_summa[$row['pardeveja_id']][$row['liguma_id']] += $row['apm_summa'];
      $katra_liguma_apm_summa_bez_pvn[$row['pardeveja_id']][$row['liguma_id']] += $row['apm_summa'] / (1 + ($ligumi[$row['pardeveja_id']][$row['liguma_id']]['pvn'] / 100));

    }

    foreach($katra_liguma_summa_bez_pvn as $pard_id => $lig) {

      $apr_prov[$pard_id] = 0;

      foreach($lig as $lig_id => $lig_kopsumma) {
        $apr_prov[$pard_id] += $lig_kopsumma * ($ligumi[$pard_id][$lig_id]['provizija_pardevejam'] / 100);
      }

    }

    foreach($katra_liguma_apm_summa_bez_pvn as $pard_id => $lig) {

      $apmaksajama_prov[$pard_id] = 0;

      foreach($lig as $lig_id => $apm_summa) {
        $apmaksajama_prov[$pard_id] += $apm_summa * ($ligumi[$pard_id][$lig_id]['provizija_pardevejam'] / 100);
      }

    }

    // -----

    $sql = "
      SELECT r.*
      FROM `".DB_PREF."pardeveji_rekini` r
    ";

    if (!is_admin()) {
      $pard_rek_where[] = "r.pardeveja_id = " . (int)$_SESSION['user']['id'];
    }

    if (!empty($pard_rek_where)) {
      $sql .= "WHERE " . implode(" AND ", $pard_rek_where);
    }

    $query = db_query($sql);

    while($row = db_get_assoc($query)){

      if ($row['statuss'] == 2) { // apmaksāts
        $izmaks_prov[$row['pardeveja_id']] += $row['summa']; // rādam bez pvn
      }

    }

    // -----

    $query = db_query("
      SELECT
        a.*,
        g.pardeveja_id as liguma_pardeveja_id
      FROM `".DB_PREF."atgadinajumi` a
      LEFT JOIN `".DB_PREF."ligumi` g ON (a.liguma_id = g.id)
    ");
    $atgadinajumi = array();

    while($row = db_get_assoc($query)){

      if (!empty($row['pardeveja_id'])) {
        $atgadinajumi[$row['pardeveja_id']][] = $row;
      } else {
        $atgadinajumi[$row['liguma_pardeveja_id']][] = $row;
      }

    }

    // -----

    $now = date("Y")*10000+date("m")*100+date("d");
    $inid = -1;
    ?>

    <?php foreach($data as $row) { ?>

      <?
      $inid ++;

      $iratg = 0;

      if(!empty($atgadinajumi[$row['id']])) {

        foreach($atgadinajumi[$row['id']] as $d) {

          if ($d['statuss'] == 1) {

            $thisnow = explode(".", date("d.m.Y", strtotime($d['datums'])));
            $thisnow = $thisnow[2]*10000+$thisnow[1]*100+$thisnow[0];

            if($now >= $thisnow){
              $iratg++;
            }

          }

        }

        if($iratg != 0){
          $akt_atg = 1;
        } else{
          $akt_atg = 2;
        }

      } else {

        $akt_atg = 2;

      }

      ?>

      <tr id="tr<?=$inid;?>">

        <td><a href="?c=darbinieki&a=labot&id=<?=$row['id'];?>"><b><?=$row['vards'];?></b></a></td>

        <? if (!empty($ligumi[$row['id']])) { ?>

          <? $lig_skaits = count($ligumi[$row['id']]) ?>

          <td><?= $lig_skaits ?></td>
          <td><?= format_currency($ligumu_summa[$row['id']]) ?></td>
          <td><?= format_currency($ligumu_summa[$row['id']] / $lig_skaits) ?></td>
          <td><?= format_currency($ligumu_apm_summa[$row['id']]) ?></td>
          <td><?= round($prov_proc_summa[$row['id']] / $lig_skaits, 1) . '%' ?></td>
          <td><?= format_currency($apr_prov[$row['id']]) ?></td>
          <td><?= format_currency($apr_prov[$row['id']] / $lig_skaits) ?></td>
          <td><?= format_currency($apmaksajama_prov[$row['id']]) ?></td>
          <td><?= !empty($apr_prov[$row['id']]) ? round(round($apmaksajama_prov[$row['id']], 2) * 100 / round($apr_prov[$row['id']], 2), 2) : 0 ?>%</td>
          <td><?= format_currency($izmaks_prov[$row['id']]) ?></td>
          <td><?= !empty($apmaksajama_prov[$row['id']]) ? round(round($izmaks_prov[$row['id']], 2) * 100 / round($apmaksajama_prov[$row['id']], 2), 2) : 0 ?>%</td>
          <td><?= format_currency(round($apmaksajama_prov[$row['id']] - $izmaks_prov[$row['id']], 2)) ?></td>
          <td><?= $akt_atg == 1 ? 'Ir ('.$iratg.')' : 'Nav' ?></td>

          <td style="display: none">
          <script>
            searchlist[<?=$inid;?>] = '<?=addslashes(mb_strtoupper($row['vards']));?>';
            sp['s_vards'][<?=$inid;?>] = '<?=addslashes(mb_strtoupper($row['vards']));?>';
            sp['s_ligumu_sk'][<?=$inid;?>] = <?= $lig_skaits ?>;
            sp['s_lig_kopsumma'][<?=$inid;?>] = <?= round($ligumu_summa[$row['id']], 2) ?>;
            sp['s_lig_vid_summa'][<?=$inid;?>] = <?= round($ligumu_summa[$row['id']] / $lig_skaits, 2) ?>;
            sp['s_apm_lig_kopsumma'][<?=$inid;?>] = <?= round($ligumu_apm_summa[$row['id']], 2) ?>;
            sp['s_vid_prov_proc'][<?=$inid;?>] = <?= round($prov_proc_summa[$row['id']] / $lig_skaits, 1) ?>;
            sp['s_apr_prov'][<?=$inid;?>] = <?= round($apr_prov[$row['id']], 2) ?>;
            sp['s_vid_apr_prov'][<?=$inid;?>] = <?= round($apr_prov[$row['id']] / $lig_skaits, 2) ?>;
            sp['s_apmaksajama_prov'][<?=$inid;?>] = <?= round($apmaksajama_prov[$row['id']], 2) ?>;
            sp['s_apmaksajama_prov_percent'][<?=$inid;?>] = <?= !empty($apr_prov[$row['id']]) ? round($apmaksajama_prov[$row['id']] * 100 / $apr_prov[$row['id']], 2) : 0 ?>;
            sp['s_izm_prov'][<?=$inid;?>] = <?= round($izmaks_prov[$row['id']], 2) ?>;
            sp['s_rek_apm_percent'][<?=$inid;?>] = <?= !empty($apmaksajama_prov[$row['id']]) ? round($izmaks_prov[$row['id']] * 100 / $apmaksajama_prov[$row['id']], 2) : 0 ?>;
            sp['s_dept_cred'][<?=$inid;?>] = <?= round($apmaksajama_prov[$row['id']] - $izmaks_prov[$row['id']], 2) ?>;
            sp['s_akt_atg'][<?=$inid;?>] = <?=$akt_atg;?>;
          </script>
          </td>

        <? } else { ?>

          <td>0</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td><?= $akt_atg == 1 ? 'Ir ('.$iratg.')' : 'Nav' ?></td>

          <td style="display: none;">
          <script>
            searchlist[<?=$inid;?>] = '<?=addslashes(mb_strtoupper($row['vards']));?>';
            sp['s_vards'][<?=$inid;?>] = '<?=addslashes(mb_strtoupper($row['vards']));?>';
            sp['s_ligumu_sk'][<?=$inid;?>] = 0;
            sp['s_lig_kopsumma'][<?=$inid;?>] = 0;
            sp['s_lig_vid_summa'][<?=$inid;?>] = 0;
            sp['s_apm_lig_kopsumma'][<?=$inid;?>] = 0;
            sp['s_vid_prov_proc'][<?=$inid;?>] = 0;
            sp['s_apr_prov'][<?=$inid;?>] = 0;
            sp['s_vid_apr_prov'][<?=$inid;?>] = 0;
            sp['s_apmaksajama_prov'][<?=$inid;?>] = 0;
            sp['s_apmaksajama_prov_percent'][<?=$inid;?>] = 0;
            sp['s_izm_prov'][<?=$inid;?>] = 0;
            sp['s_rek_apm_percent'][<?=$inid;?>] = 0;
            sp['s_dept_cred'][<?=$inid;?>] = 0;
            sp['s_akt_atg'][<?=$inid;?>] = <?=$akt_atg;?>;
          </script>
          </td>

        <? } ?>

      </tr>

    <?php } ?>


  </tbody>

  <? if (is_admin()) { ?>

  <tr>

    <td align="right"><strong>Kopā:</strong></td>
    <td id="ligskaits">0</td>
    <td id="ligkopsumma">0</td>
    <td></td>
    <td id="apmligsumma">0</td>
    <td></td>
    <td id="aprprovsumma">0</td>
    <td></td>
    <td id="apmaksajamaprovsumma">0</td>
    <td></td>
    <td id="izmprovsumma">0</td>
    <td></td>
    <td id="deptkredsumma">0</td>
    <td></td>

  </tr>

  <tr>

    <td align="right"><strong>Vidēji:</strong></td>
    <td id="ligskaitsvid">0</td>
    <td></td>
    <td id="ligvidsummavid">0</td>
    <td></td>
    <td id="provpercentvid">0</td>
    <td></td>
    <td id="vidaprprovvid">0</td>
    <td></td>
    <td id="apmprovpercentvid">0</td>
    <td id="izmprovvid">0</td>
    <td id="rekapmpercentvid">0</td>
    <td></td>
    <td></td>

  </tr>

  <? } ?>

</table>

<script type="text/javascript">
$(window).load(function() {

  var table = $("#darbinieki_kopsavilkums");
  var head = $("#darbinieki_kopsavilkums_filter");

  <? if (is_admin()) { ?>

    var cache_key = getCacheKeyFromClass(table);

    var sortListFromCache = cache_key ? $.jStorage.get(cache_key + '_sort', []) : [];

    table.tablesorter({
      debug: false,
      highlightClass: 'highlight',
      sortList: sortListFromCache,
      headers: {
        0: { sorter:'latviantext' },
        1: { sorter:'digit' },
        2: { sorter:'currency' },
        3: { sorter:'currency' },
        4: { sorter:'currency' },
        5: { sorter:'digit' },
        6: { sorter:'currency' },
        7: { sorter:'currency' },
        8: { sorter:'currency' },
        9: { sorter:'digit' },
        10: { sorter:'currency' },
        11: { sorter:'digit' },
        12: { sorter:'currency' }
      }
    }).bind('sortEnd', function() {

      if (cache_key) $.jStorage.set(cache_key + '_sort', table.get(0).config.sortList);

      stripeTable(table);

    });

    initTableFilter(table, head, processTable, '', searchlist, sp);

  <? } else { ?>

    stripeTable(table);

  <? } ?>

});

function processTable() {

  var pardskaits = 0;
  var ligumuskaits = 0;
  var ligkopsumma = 0;
  var ligvidsumma = 0;
  var apmligsumma = 0;
  var provpercentsumma = 0;
  var aprprovsumma = 0;
  var vidaprprovsumma = 0;
  var apmaksajamaprovsumma = 0;
  var apmprovpercentsumma = 0;
  var izmprovsumma = 0;
  var rekapmpercentsumma = 0;
  var deptkredsumma = 0;

  jQuery.each(searchlist, function(k,v){

    var row = $('#tr' + k);

    if (row.is(':visible') && sp['s_ligumu_sk'][k] > 0) {

      pardskaits ++;

      if(sp['s_ligumu_sk'][k] > 0) ligumuskaits += sp['s_ligumu_sk'][k];
      if(sp['s_lig_kopsumma'][k] > 0) ligkopsumma += sp['s_lig_kopsumma'][k];
      if(sp['s_lig_vid_summa'][k] > 0) ligvidsumma += sp['s_lig_vid_summa'][k];
      if(sp['s_apm_lig_kopsumma'][k] > 0) apmligsumma += sp['s_apm_lig_kopsumma'][k];
      if(sp['s_vid_prov_proc'][k] > 0) provpercentsumma += sp['s_vid_prov_proc'][k];
      if(sp['s_apr_prov'][k] > 0) aprprovsumma += sp['s_apr_prov'][k];
      if(sp['s_vid_apr_prov'][k] > 0) vidaprprovsumma += sp['s_vid_apr_prov'][k];
      if(sp['s_apmaksajama_prov'][k] > 0) apmaksajamaprovsumma += sp['s_apmaksajama_prov'][k];
      if(sp['s_apmaksajama_prov_percent'][k] > 0) apmprovpercentsumma += sp['s_apmaksajama_prov_percent'][k];
      if(sp['s_izm_prov'][k] > 0) izmprovsumma += sp['s_izm_prov'][k];
      if(sp['s_rek_apm_percent'][k] > 0) rekapmpercentsumma += sp['s_rek_apm_percent'][k];

      deptkredsumma += sp['s_dept_cred'][k];

    }

  });

  $('#ligskaits').html(ligumuskaits);
  $('#ligskaitsvid').html(Math.round(ligumuskaits / pardskaits * 10) / 10);
  $('#ligkopsumma').html(ligkopsumma.toFixed(2) + '&nbsp;Ls');
  $('#ligvidsummavid').html((ligvidsumma / pardskaits).toFixed(2) + '&nbsp;Ls');
  $('#apmligsumma').html(apmligsumma.toFixed(2) + '&nbsp;Ls');
  $('#provpercentvid').html((Math.round(provpercentsumma / pardskaits * 20) / 20) + '%');
  $('#aprprovsumma').html(aprprovsumma.toFixed(2) + '&nbsp;Ls');
  $('#vidaprprovvid').html((vidaprprovsumma / pardskaits).toFixed(2) + '&nbsp;Ls');
  $('#apmaksajamaprovsumma').html(apmaksajamaprovsumma.toFixed(2) + '&nbsp;Ls');
  $('#apmprovpercentvid').html((Math.round(apmprovpercentsumma / pardskaits * 20) / 20) + '%');
  $('#izmprovsumma').html(izmprovsumma.toFixed(2) + '&nbsp;Ls');
  $('#izmprovvid').html((izmprovsumma / pardskaits).toFixed(2) + '&nbsp;Ls');
  $('#rekapmpercentvid').html((Math.round(rekapmpercentsumma / pardskaits * 20) / 20) + '%');
  $('#deptkredsumma').html(deptkredsumma.toFixed(2) + '&nbsp;Ls');

}

$("#date_from").datepicker({dateFormat:'dd.mm.yy'});
$("#date_to").datepicker({dateFormat:'dd.mm.yy'});

$("form#period_filter").each(function() {

  var form = $(this);

  $('input#date_from, input#date_to', form).change(function() {
    form.submit();
  });

  $('a.clear_form', form).click(function() {
    $('input#date_from, input#date_to', form).val('');
    form.submit();
  });

});

</script>

<? if (is_admin()) { ?>

  <form action="?c=darbinieki" id="fullformplace" method="post" enctype="multipart/form-data" style="clear: both;">

    <? show_comments_block('_darb_kopsavilkums', null, true) ?>

  </form>

<? } ?>

<? if (!is_admin()) { ?>

  <form id="liguma_check_form" method="" action="">

    Meklēt klientu: <input class="nosaukums" type="text" />

    <input type="submit" value="Meklēt" />

    <div class="result">


    </div>

  </form>

  <script>
  $('form#liguma_check_form').submit(function() {

     var loading_html = '<div class="loading" style="text-align: center;"><img alt="Notiek ielāde..." src="css/loading.gif" /></div>';

     var holder = $('div.result', this);

     holder.html(loading_html);

     $.get('ajax.php?action=check_client', {nosauk: $('.nosaukums', this).val()}, function(result) {

       holder.html(result);

     })

     return false;

  });
  </script>

<? } ?>