<?php
if (!empty($_POST)) {

  $errors = validate_darbinieks_form($_POST);

  if (empty($errors)) {

    $_POST = trim_array($_POST);

    $agenta_lig_datums = !empty($_POST['agenta_lig_datums']) ? "'".esc(date('Y-m-d', strtotime($_POST['agenta_lig_datums'])))."'" : 'null';
    $pvn_likme = (float) str_replace(',', '.', $_POST['pvn_likme']);

    $password = '';

    if (!empty($_POST['password1'])) {
      $password = md5($_POST['password1']);
    }

    if(!empty($_GET['id'])) {

      $darbinieka_id = (int)$_GET['id'];

      if (is_admin()) {

        $sql = "
          UPDATE `".DB_PREF."pardeveji`
          SET
            vards = '".esc($_POST['vards'])."',
            jurpers_nosauk = '".esc($_POST['jurpers_nosauk'])."',
            jurpers_adrese = '".esc($_POST['jurpers_adrese'])."',
            reg_nr = '".esc($_POST['reg_nr'])."',
            pvn_maksatajs = ".(!empty($_POST['pvn_maksatajs']) ? 1 : 0).",
            pvn_nr = '".esc($_POST['pvn_nr'])."',
            konta_nr = '".esc($_POST['konta_nr'])."',
            norek_rekviziti = '".esc($_POST['norek_rekviziti'])."',
            agenta_lig_nr = '".esc($_POST['agenta_lig_nr'])."',
            agenta_lig_datums = ".$agenta_lig_datums.",
            pvn_likme = ".$pvn_likme.",
            parole = ".(!empty($password) ? "'".esc($password)."'" : 'parole')."
          WHERE id = ".$darbinieka_id."
          LIMIT 1
        ";

      } else {

        $sql = "
          UPDATE `".DB_PREF."pardeveji`
          SET
            vards = '".esc($_POST['vards'])."',
            parole = ".(!empty($password) ? "'".esc($password)."'" : 'parole')."
          WHERE id = ".$darbinieka_id."
          LIMIT 1
        ";

      }

      db_query($sql);

    } else {

      $sql = "
        INSERT INTO `".DB_PREF."pardeveji`
        SET
          vards = '".esc($_POST['vards'])."',
          lietotajvards = '".esc($_POST['lietotajvards'])."',
          tips = 'sales',
          jurpers_nosauk = '".esc($_POST['jurpers_nosauk'])."',
          jurpers_adrese = '".esc($_POST['jurpers_adrese'])."',
          reg_nr = '".esc($_POST['reg_nr'])."',
          pvn_maksatajs = ".(!empty($_POST['pvn_maksatajs']) ? 1 : 0).",
          pvn_nr = '".esc($_POST['pvn_nr'])."',
          konta_nr = '".esc($_POST['konta_nr'])."',
          norek_rekviziti = '".esc($_POST['norek_rekviziti'])."',          
          agenta_lig_nr = '".esc($_POST['agenta_lig_nr'])."',
          agenta_lig_datums = ".$agenta_lig_datums.",
          pvn_likme = ".$pvn_likme.",
          parole = ".(!empty($password) ? "'".esc($password)."'" : 'parole')."
      ";

      db_query($sql);

      $darbinieka_id = db_last_id();

    }


    if (!empty($_GET['id'])) {

      // deleting old email addresses

      $sql = "
        DELETE FROM `".DB_PREF."pardeveji_epasti`
        WHERE
          pardeveja_id = ".$darbinieka_id."
          " . (!empty($_POST['epasti']) ? "AND id NOT IN (".implode(',', array_keys($_POST['epasti'])).") " : '')."
      ";

      db_query($sql);

      // updating email addresses

      if (!empty($_POST['epasti'])) {

        foreach($_POST['epasti'] as $epasta_id => $epasts) {

          $sql = "
            UPDATE `".DB_PREF."pardeveji_epasti`
            SET
              epasts = '".esc($epasts)."'
            WHERE id = ".$epasta_id."
            LIMIT 1
          ";

          db_query($sql);

        }

      }

    }

    // adding email addresses

    if (!empty($_POST['epasti_new'])) {

      foreach($_POST['epasti_new'] as $epasts) {

        if (!empty($epasts)) {

          $sql = "
            INSERT INTO `".DB_PREF."pardeveji_epasti` (
              pardeveja_id,
              epasts
            ) VALUES (
              ".$darbinieka_id.",
              '".esc($epasts)."'
            )
          ";

          db_query($sql);

        }

      }

    }


    if (!empty($_GET['id'])) {

      // deleting old tel numbers

      $sql = "
        DELETE FROM `".DB_PREF."pardeveji_telefoni`
        WHERE
          pardeveja_id = ".$darbinieka_id."
          " . (!empty($_POST['telefoni']) ? "AND id NOT IN (".implode(',', array_keys($_POST['telefoni'])).") " : '')."
      ";

      db_query($sql);

      // updating tel numbers

      if (!empty($_POST['telefoni'])) {

        foreach($_POST['telefoni'] as $telefona_id => $tel) {

          $sql = "
            UPDATE `".DB_PREF."pardeveji_telefoni`
            SET
              telefons = '".esc($tel)."'
            WHERE id = ".$telefona_id."
            LIMIT 1
          ";

          db_query($sql);

        }

      }

    }

    // adding tel numbers

    if (!empty($_POST['telefoni_new'])) {

      foreach($_POST['telefoni_new'] as $tel) {

        if (!empty($tel)) {

          $sql = "
            INSERT INTO `".DB_PREF."pardeveji_telefoni` (
              pardeveja_id,
              telefons
            ) VALUES (
              ".$darbinieka_id.",
              '".esc($tel)."'
            )
          ";

          db_query($sql);

        }

      }

    }

    if (!empty($_GET['id'])) {

      // updating reminders

      if (!empty($_POST['atgadinajumi'])) {

        foreach($_POST['atgadinajumi'] as $atgadinajuma_id => $a) {

          $sql = "
            UPDATE `".DB_PREF."atgadinajumi`
            SET
              datums = '".esc(date('Y-m-d', strtotime($a['datums'])))."',
              tema = ".(int)$a['tema'].",
              saturs = '".esc($a['saturs'])."'
            WHERE id = ".$atgadinajuma_id."
            LIMIT 1
          ";

          db_query($sql);

        }

      }

    }

    // adding reminders

    if (!empty($_POST['atgadinajumi_new']['datums'])) {

      foreach($_POST['atgadinajumi_new']['datums'] as $i => $datums) {

        $tema = $_POST['atgadinajumi_new']['tema'][$i];
        $saturs = $_POST['atgadinajumi_new']['saturs'][$i];

        $liguma_id = $_POST['atgadinajumi_new']['ligums'][$i];
        $pardeveja_id = 'null';

        if (empty($liguma_id)) {
          $liguma_id = 'null';
          $pardeveja_id = $darbinieka_id;
        }

        $sql = "
          INSERT INTO `".DB_PREF."atgadinajumi` (
            liguma_id,
            pardeveja_id,
            datums,
            tema,
            saturs,
            created_pardeveja_id
          ) VALUES (
            ".$liguma_id.",
            ".$pardeveja_id.",
            '".esc(date('Y-m-d', strtotime($datums)))."',
            ".(int)$tema.",
            '".esc($saturs)."',
            ".$_SESSION['user']['id']."
          )
        ";

        db_query($sql);

      }

    }

    if (is_admin()) {

      if (!empty($_GET['id'])) {

        // updating provisions

        if (!empty($_POST['lig_provizija'])) {

          foreach($_POST['lig_provizija'] as $lig_id => $provizija) {

            $provizija = (float)str_replace(',', '.', $provizija);

            $sql = "
              UPDATE `".DB_PREF."ligumi`
              SET
                provizija_pardevejam = ".$provizija."
              WHERE id = ".$lig_id."
              LIMIT 1
            ";

            db_query($sql);

          }

        }

      }


      if (!empty($_GET['id'])) {

        // updating invoices

        if (!empty($_POST['rekini'])) {

          foreach($_POST['rekini'] as $rekina_id => $r) {

            $old_data = db_get_assoc(db_query("
              SELECT *
              FROM `".DB_PREF."pardeveji_rekini`
              WHERE id = ".$rekina_id."
            "));

            $rek_nr = !empty($r['rek_nr']) ? "'" . esc($r['rek_nr']) . "'" : "''";
            $izr_datums = !empty($r['izr_datums']) ? "'".date('Y-m-d', strtotime($r['izr_datums']))."'" : 'null';
            $apm_datums = !empty($r['apm_datums']) ? "'".date('Y-m-d', strtotime($r['apm_datums']))."'" : 'null';

            $statuss = 1;
            if (!empty($r['apmaksats'])) {
              $statuss = 2;
            }

            if ($statuss == 1) {
              $apm_datums = 'NULL';
            }

            $sql = "
              UPDATE `".DB_PREF."pardeveji_rekini`
              SET
                rek_nr = ".$rek_nr.",
                izr_datums = ".$izr_datums.",
                statuss = ".$statuss.",
                apm_datums = ".$apm_datums."
              WHERE id = ".$rekina_id."
              LIMIT 1
            ";

            db_query($sql);

          }

        }

      }

      $form_names = array(
        'darb_rekviziti',
        'darb_ligumi',
        'darb_norekini',
        'darb_atgadinajumi',
      );

      // inserting files

      process_files($form_names, $darbinieka_id);

      // inserting comments

      process_comments($form_names, $darbinieka_id);

    }


    header('Location: ?c=darbinieki&a=labot&id=' . $darbinieka_id . '&subtab=' . $subtab);
    die();

  }

}
?>