<?
if (empty($_GET['darbinieka_rek_id'])) {
  die();
}

$darbinieka_rek_id = (int) $_GET['darbinieka_rek_id'];

$rekins = db_get_assoc(db_query("
  SELECT *
  FROM `".DB_PREF."pardeveji_rekini`
  WHERE id = ".$darbinieka_rek_id."
"));

if (empty($rekins)) {
  die();
}

if (!is_admin() && $_SESSION['user']['id'] != $rekins['pardeveja_id']) {
  die();
}

$query = db_query("
  SELECT
    d.*,
    p.uz_rokas,
    g.nosaukums as klienta_nosaukums,
    r.rek_nr as liguma_rek_nr,
    r.kopsumma as rekina_kopsumma,
    m.summa as maksajuma_summa,
    m.datums as maksajuma_datums
  FROM `".DB_PREF."pardeveji_rekini_detalas` d
  LEFT JOIN `".DB_PREF."pardeveji_rekini` p ON (p.id = d.pardeveja_rekina_id)
  LEFT JOIN `".DB_PREF."rekini` r ON (r.id = d.rekina_id)
  LEFT JOIN `".DB_PREF."ligumi` g ON (g.id = r.liguma_id)
  LEFT JOIN `".DB_PREF."rekini_maksajumi` m ON (m.id = d.rekina_maksajuma_id)
  WHERE d.pardeveja_rekina_id = ".$darbinieka_rek_id."
");

$apmaksas = array();
$avansa = array();

while($row = db_get_assoc($query)) {

  if (!empty($row['rekina_maksajuma_id'])) {
    $apmaksas[] = $row;
  } else {
    $avansa[] = $row;
  }

}
?>

<? if (!empty($apmaksas)) { ?>

  <h2 style="margin-bottom: 0;">Saņemtās samaksas no klientiem</h2>

  <table id="maksajumi" class="data" style="width: 99%;">

    <thead>

      <tr class="last">
        <th align="center">Uz<br />rokas</th>
        <th>Klients</th>
        <th>Rēķina nr.</th>
        <th>Rēķina<br />summa&nbsp;Ls</th>
        <th>Saņemtā<br />samaksa&nbsp;Ls</th>
        <th>Apmaksas<br />Datums</th>
        <th>Provīzija&nbsp;%</th>
        <th>Provīzijas<br />summa&nbsp;Ls</th>
        <th>PVN<br />(provīzija)&nbsp;Ls</th>
        <th>Summa<br />kopā&nbsp;Ls</th>
      </tr>

    </thead>

    <tbody class="main">

      <?
        $total = array();
        $total['rekina_summa'] = 0;
        $total['sanemta_samaksa'] = 0;
        $total['provizijas_summa'] = 0;
        $total['provizijas_pvn'] = 0;
        $total['summa_kopa'] = 0;
      ?>

      <? foreach($apmaksas as $apmaksa) { ?>

        <?
          $total['rekina_summa'] += $apmaksa['rekina_kopsumma'];
          $total['sanemta_samaksa'] += $apmaksa['maksajuma_summa'];
          $total['provizijas_summa'] += $apmaksa['summa'];
          $total['provizijas_pvn'] += round($apmaksa['kopsumma'] - $apmaksa['summa'], 2);
          $total['summa_kopa'] += $apmaksa['kopsumma'];
        ?>

        <tr>
          <td><?= !empty($apmaksa['uz_rokas']) ? 'Jā' : 'Nē' ?></td>
          <td><?= $apmaksa['klienta_nosaukums'] ?></td>
          <td><?= $apmaksa['liguma_rek_nr'] ?></td>
          <td><?= $apmaksa['rekina_kopsumma'] ?></td>
          <td><?= $apmaksa['maksajuma_summa'] ?></td>
          <td><?= date('d.m.Y', strtotime($apmaksa['maksajuma_datums'])) ?></td>
          <td><?= round($apmaksa['prov_likme'], 2) ?>%</td>
          <td><?= $apmaksa['summa'] ?></td>
          <td><?= format_currency($apmaksa['kopsumma'] - $apmaksa['summa']) ?></td>
          <td><?= $apmaksa['kopsumma'] ?></td>
        </tr>

      <? } ?>

    </tbody>

    <tr>

      <td align="right" colspan="3"><strong>Kopā:</strong></td>
      <td><?= format_currency($total['rekina_summa']) ?></td>
      <td><?= format_currency($total['sanemta_samaksa']) ?></td>
      <td></td>
      <td></td>
      <td><?= format_currency($total['provizijas_summa']) ?></td>
      <td><?= format_currency($total['provizijas_pvn']) ?></td>
      <td><?= format_currency($total['summa_kopa']) ?></td>

    </tr>

  </table>

<? } ?>

<? if (!empty($avansa)) { ?>

  <h2 style="margin-bottom: 0;">Avansa rēķini</h2>

  <table id="avansa_summas" class="data" style="width: 99%;">

    <thead>

      <tr class="last">
        <th align="center">Uz<br />rokas</th>
        <th>Klients</th>
        <th>Rēķina<br />nr.</th>
        <th>Rēķina<br />summa&nbsp;Ls</th>
        <th>Neapmaksātā<br />summa&nbsp;Ls</th>
        <th>Provīzija&nbsp;%</th>
        <th>Provīzijas<br />summa&nbsp;Ls</th>
        <th>PVN<br />(provīzija)&nbsp;Ls</th>
        <th>Summa<br />kopā&nbsp;Ls</th>
      </tr>

    </thead>

    <tbody class="main">

      <?
        $total = array();
        $total['rekina_kopsumma'] = 0;
        $total['neapm_summa'] = 0;
        $total['provizijas_summa'] = 0;
        $total['provizijas_pvn'] = 0;
        $total['summa_kopa'] = 0;
      ?>

      <? foreach($avansa as $apmaksa) { ?>

        <?
          $total['rekina_kopsumma'] += $apmaksa['rekina_kopsumma'];
          $total['neapm_summa'] += $apmaksa['no_summas'];
          $total['provizijas_summa'] += $apmaksa['summa'];
          $total['provizijas_pvn'] += round($apmaksa['kopsumma'] - $apmaksa['summa'], 2);
          $total['summa_kopa'] += $apmaksa['kopsumma'];
        ?>

        <tr>
          <td><?= !empty($apmaksa['uz_rokas']) ? 'Jā' : 'Nē' ?></td>
          <td><?= $apmaksa['klienta_nosaukums'] ?></td>
          <td><?= $apmaksa['liguma_rek_nr'] ?></td>
          <td><?= $apmaksa['rekina_kopsumma'] ?></td>
          <td><?= $apmaksa['no_summas'] ?></td>
          <td><?= round($apmaksa['prov_likme'], 2) ?>%</td>
          <td><?= $apmaksa['summa'] ?></td>
          <td><?= format_currency($apmaksa['kopsumma'] - $apmaksa['summa']) ?></td>
          <td><?= $apmaksa['kopsumma'] ?></td>
        </tr>

      <? } ?>

    </tbody>

    <tr>

      <td align="right" colspan="3"><strong>Kopā:</strong></td>
      <td><?= format_currency($total['rekina_kopsumma']) ?></td>
      <td><?= format_currency($total['neapm_summa']) ?></td>
      <td></td>
      <td><?= format_currency($total['provizijas_summa']) ?></td>
      <td><?= format_currency($total['provizijas_pvn']) ?></td>
      <td><?= format_currency($total['summa_kopa']) ?></td>

    </tr>

  </table>

<? } ?>

<script>
stripeTable($('#maksajumi'));
stripeTable($('#avansa_summas'));
</script>
