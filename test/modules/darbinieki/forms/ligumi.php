<script>
var searchlist = [];

var sp = create_filter_field_array([
  's_lig_nr', 's_nosaukums', 's_lig_stat', 's_norek_stat', 's_ligdat', 's_sanems_dat',
  's_sanforma', 's_rekl_no', 's_rekl_lidz', 's_summa',
  's_pvn', 's_kopsumma', 's_apm', 's_kopsumma_bez_pvn', 's_prov_percent', 's_prov',
  's_apm_prov', 's_apm_prov_percent'
]);
</script>

<table style="display:none;">

  <tr id="darbinieka_ligumi_filter">

    <th></th>
    <th><span><input class="search_field" meth="standart" searchclass="s_lig_nr" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <th><span><input class="search_field" meth="standart" searchclass="s_nosaukums" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>

    <th>
      <select class="search_field advonchange" searchclass="s_lig_stat" meth="int">
        <option value=""></option>
        <? foreach($_vars['liguma_statuss'] as $k=>$p){ ?>
          <option value="<?=$k;?>"><?=$p;?></option>
        <? } ?>
      </select>
    </th>

    <th>
      <select class="search_field advonchange" searchclass="s_norek_stat" meth="int">
        <option value=""></option>
        <? foreach($_vars['kopapmaksas_statuss'] as $k=>$p){ ?>
          <option value="<?=$k;?>"><?=$p;?></option>
        <? } ?>
      </select>
    </th>

    <th>
      <span><input class="search_field kalendari" meth="from" searchclass="s_ligdat" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span><br>
      <span><input class="search_field kalendari" meth="to" searchclass="s_ligdat" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span>
    </th>

    <th>
      <span><input class="search_field kalendari" meth="from" searchclass="s_sanems_dat" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span><br>
      <span><input class="search_field kalendari" meth="to" searchclass="s_sanems_dat" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span>
    </th>

    <th>
      <select class="search_field advonchange" searchclass="s_sanforma" meth="int">
        <option value=""></option>
        <?php foreach($_vars['sanemsanas_forma'] as $k=>$p){ ?>
          <option value="<?=$k;?>"><?=$p;?></option>
        <?php } ?>
      </select>
    </th>

    <th>
      <span><input class="search_field kalendari" meth="from" searchclass="s_rekl_no" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span><br>
      <span><input class="search_field kalendari" meth="to" searchclass="s_rekl_no" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span>
    </th>

    <th>
      <span><input class="search_field kalendari" meth="from" searchclass="s_rekl_lidz" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span><br>
      <span><input class="search_field kalendari" meth="to" searchclass="s_rekl_lidz" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span>
    </th>

    <th><span><input class="search_field" meth="coin" searchclass="s_summa" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <th><span><input class="search_field" meth="coin" searchclass="s_pvn" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <th><span><input class="search_field" meth="coin" searchclass="s_kopsumma" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <th><span><input class="search_field" meth="coin" searchclass="s_apm" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>

    <th><span><input class="search_field" meth="coin" searchclass="s_prov_percent" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <th><span><input class="search_field" meth="coin" searchclass="s_prov" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <th><span><input class="search_field" meth="coin" searchclass="s_apm_prov" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <th><span><input class="search_field" meth="coin" searchclass="s_apm_prov_percent" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>

  </tr>

</table>

<table width="100%" cellpadding="3" cellspacing="0" class="data <?= isset($_GET['id']) ? 'ckey_darbinieka_ligumi_' . $_GET['id'] : '' ?>" id="darbinieka_ligumi">

  <thead class="head">

    <tr>
      <th>Npk.</th>
      <th>Līg. nr.</th>
      <th>Nosaukums</th>
      <th>Līg. statuss</th>
      <th>Norēķinu statuss</th>
      <th>Līg. datums</th>
      <th>Saņemš. datums</th>
      <th>Saņemš. forma</th>
      <th>Līgums no</th>
      <th>Līgums līdz</th>
      <th>Summa</th>
      <th>PVN</th>
      <th>Kopsumma</th>
      <th>Apmaksāts Ls</th>
      <th>Provīzija %</th>
      <th>Provīzija Ls (bez PVN)</th>
      <th>Izmaksātā provīzija Ls (bez PVN)</th>
      <th>Izmaksātā provīzija %</th>
    </tr>

  </thead>

  <tbody class="main">

    <?php
    $i = count($ligumi);
    $inid = -1;

    foreach($ligumi as $row) {

      $row['liguma_statuss'] = get_liguma_statuss($row, !empty($lig_rekini[$row['id']]) ? $lig_rekini[$row['id']] : null);
      $row['norekinu_statuss'] = get_apmaksas_statuss(!empty($lig_rekini[$row['id']]) ? $lig_rekini[$row['id']] : null);

      $inid ++;

      ?>

      <tr id="lig_tr<?=$inid;?>" class="lig_row">
        <td align="right" class="s_id"><?=$i;?></td>
        <td><?= $row['ligumanr'] ?></td>
        <td><a href="?c=ligumi&a=labot&id=<?=$row['id'];?>"><b><?=$row['nosaukums'] ?></b></a></td>

        <? if ($row['liguma_statuss'] == 6) { ?>
          <? $active = (empty($row['beigu_statuss']) || ($row['beigu_statuss'] == 'termins' && $row['beigu_statuss_atlikt_lidz'] <= date('Y-m-d'))) ? true : false; ?>
          <td>
            <a href="#" class="lig_statuss_6 <?= !empty($row['beigu_statuss']) ? 'lig_beigu_statuss_' . $row['beigu_statuss'] : 'lig_beigu_statuss_termins' ?> <?= $active ? 'lig_beigu_statuss_active' : '' ?>" onclick="showLigumaBeiguStatussDialog(<?= $row['id'] ?>); return false;">
              <span><?= $_vars['liguma_statuss'][6] ?></span>
              (<?= !empty($row['beigu_statuss']) ? $_vars['liguma_beigu_statuss'][$row['beigu_statuss']] : $_vars['liguma_beigu_statuss']['termins'] ?>)
            </a>
          </td>
        <? } else { ?>
          <td><?= !empty($_vars['liguma_statuss'][$row['liguma_statuss']]) ? '<span class="lig_statuss_'.$row['liguma_statuss'].'">' . $_vars['liguma_statuss'][$row['liguma_statuss']] . '</span>' : ''?></td>
        <? } ?>

        <? if ($row['norekinu_statuss'] == 2 && is_barteris(!empty($lig_rekini[$row['id']]) ? $lig_rekini[$row['id']] : null)) { ?>
          <td><span class="norek_statuss_2">Barteris</span></td>
        <? } else { ?>
          <td><?= !empty($_vars['kopapmaksas_statuss'][$row['norekinu_statuss']]) ? '<span class="norek_statuss_'.$row['norekinu_statuss'].'">' . $_vars['kopapmaksas_statuss'][$row['norekinu_statuss']] . '</span>' : "" ?></td>
        <? } ?>

        <td><span style="display:none;"><?=date("Y.m.d",strtotime($row['ligumadat']));?>|</span><?= !empty($row['ligumadat'])?date("d.m.Y",strtotime($row['ligumadat'])):"" ?></td>
        <td><span style="display:none;"><?=date("Y.m.d",strtotime($row['sanemsanas_datums']));?>|</span><?= !empty($row['sanemsanas_datums'])?date("d.m.Y",strtotime($row['sanemsanas_datums'])):"" ?></td>
        <td><?= !empty($row['sanemsanas_forma']) ? $_vars['sanemsanas_forma'][$row['sanemsanas_forma']] : '' ?></td>
        <td><span style="display:none;"><?=date("Y.m.d",strtotime($row['reklama_no']));?>|</span><?= !empty($row['reklama_no'])?date("d.m.Y",strtotime($row['reklama_no'])):"" ?></td>
        <td><span style="display:none;"><?=date("Y.m.d",strtotime($row['reklama_lidz']));?>|</span><?= !empty($row['reklama_lidz'])?date("d.m.Y",strtotime($row['reklama_lidz'])):"" ?></td>
        <td><?= format_currency($row['rekinu_summa']) ?></td>
        <td><?= format_currency($row['rekinu_kopsumma'] - $row['rekinu_summa']) ?></td>
        <td><?= format_currency($row['rekinu_kopsumma']) ?></td>
        <td><?= format_currency($row['apm_kopsumma']) ?></td>
        <td class="prov_percent" style="white-space: nowrap;"><span style="display:none;"><?= round($row['provizija_pardevejam'], 2) ?></span><input style="text-align: center; width:2em;" type="text" name="lig_provizija[<?= $row['id'] ?>]" value="<?= round($row['provizija_pardevejam'], 2) ?>" />%</td>
        <td class="prov"><span>0</span></td>
        <td class="prov_apm">
          <?
          $apm_prov = 0;

          if (!empty($pard_rekini_det[$row['id']])) {
            foreach($pard_rekini_det[$row['id']] as $pr) {
              if ($pr['pard_rekina_statuss'] == 2) { // apmaksāts
                $apm_prov += $pr['summa'];
              }
            }
          }
          ?>
          <span><?= format_currency($apm_prov) ?></span>
        </td>
        <td class="prov_apm_percent"><span>0</span></td>
        <td style="display: none;">
          <script>
            searchlist[<?=$inid;?>] = '<?=addslashes(mb_strtoupper($row['nosaukums']));?>';
            sp['s_lig_nr'][<?=$inid;?>] = '<?=addslashes(mb_strtoupper($row['ligumanr']));?>';
            sp['s_nosaukums'][<?=$inid;?>] = '<?=addslashes(mb_strtoupper($row['nosaukums']));?>';
            sp['s_lig_stat'][<?=$inid;?>] = <?= $row['liguma_statuss'] ?>;
            sp['s_norek_stat'][<?=$inid;?>] = <?= $row['norekinu_statuss'] ?>;
            sp['s_ligdat'][<?=$inid;?>] = <?= !empty($row['ligumadat']) ? date('Ymd', strtotime($row['ligumadat'])) : 0 ?>;
            sp['s_sanems_dat'][<?=$inid;?>] = <?= !empty($row['sanemsanas_datums']) ? date('Ymd', strtotime($row['sanemsanas_datums'])) : 0 ?>;
            sp['s_sanforma'][<?=$inid;?>] = <?= !empty($row['sanemsanas_forma']) ? (int)$row['sanemsanas_forma'] : 0 ?>;
            sp['s_rekl_no'][<?=$inid;?>] = <?= !empty($row['reklama_no']) ? date('Ymd', strtotime($row['reklama_no'])) : 0 ?>;
            sp['s_rekl_lidz'][<?=$inid;?>] = <?= !empty($row['reklama_lidz']) ? date('Ymd', strtotime($row['reklama_lidz'])) : 0 ?>;
            sp['s_summa'][<?=$inid;?>] = '<?=!empty($row['rekinu_summa']) ? $row['rekinu_summa'] : "" ?>';
            sp['s_pvn'][<?=$inid;?>] = '<?= round($row['rekinu_kopsumma'] - $row['rekinu_summa'], 2) ?>';
            sp['s_kopsumma'][<?=$inid;?>] = '<?=!empty($row['rekinu_kopsumma']) ? $row['rekinu_kopsumma'] : "" ?>';
            sp['s_apm'][<?=$inid;?>] = '<?=!empty($row['apm_kopsumma']) ? $row['apm_kopsumma'] : "" ?>';

            sp['s_prov_percent'][<?=$inid;?>] = '<?= round($row['provizija_pardevejam'], 2) ?>';
            sp['s_prov'][<?=$inid;?>] = 0;
            sp['s_apm_prov'][<?=$inid;?>] = '<?= $apm_prov ?>';
            sp['s_apm_prov_percent'][<?=$inid;?>] = 0;

            // this is for calculations only
            sp['s_kopsumma_bez_pvn'][<?=$inid;?>] = '<?=!empty($row['rekinu_summa']) ? $row['rekinu_summa'] : "" ?>';

          </script>
        </td>
      </tr>

      <? $i -- ?>

    <?php } ?>


  </tbody>

  <tr>

    <td align="right" colspan="10"><strong>Kopā:</strong></td>
    <td id="ligumusumma">0</td>
    <td id="pvnsumma">0</td>
    <td id="ligumukopsumma">0</td>
    <td id="apmsumma">0</td>
    <td></td>
    <td id="provsumma">0</td>
    <td id="apmprovsumma">0</td>
    <td></td>

  </tr>

  <tr>

    <td align="right" colspan="10"><strong>Vidēji:</strong></td>
    <td id="ligumusummavid">0</td>
    <td></td>
    <td id="ligumukopsummavid">0</td>
    <td></td>
    <td id="provpercentvid">0</td>
    <td id="provsummavid">0</td>
    <td id="apmprovsummavid">0</td>
    <td id="apmprovpercentvid">0</td>

  </tr>

</table>

<script>

$(window).load(function() {

  var table = $("#darbinieka_ligumi");
  var head = $("#darbinieka_ligumi_filter");

  var cache_key = getCacheKeyFromClass(table);

  var sortListFromCache = cache_key ? $.jStorage.get(cache_key + '_sort', []) : [];

  table.tablesorter({
    debug: false,
    highlightClass: 'highlight',
    sortList: sortListFromCache,
    headers: {
      0: { sorter: false },
      2: { sorter:'latviantext' },
      10: { sorter:'currency' },
      11: { sorter:'currency' },
      12: { sorter:'currency' },
      13: { sorter:'currency' },
      14: { sorter:'digit' },
      15: { sorter:'currency' },
      16: { sorter:'currency' },
      17: { sorter:'digit' }
    }
  }).bind('sortEnd', function() {

    if (cache_key) $.jStorage.set(cache_key + '_sort', table.get(0).config.sortList);

    updateNpk(table);
    stripeTable(table);

  });

  initTableFilter(table, head, processTable, 'lig', searchlist, sp);
  table.trigger("update");

  $('.prov_percent input').keyup(function() {
    processTable();
    table.trigger("update");
  });

});

function processTable(table) {

  var table = $(this);

  var ligumuskaits = 0;
  var ligumusumma = 0;
  var pvnsumma = 0;
  var ligumukopsumma = 0;
  var apmsumma = 0;
  var provsumma = 0;
  var apmprovsumma = 0;

  var vid_prov_percent = 0;

  jQuery.each(searchlist, function(k,v){

    var row = $('#lig_tr' + k);

    var prov_percent = $(".prov_percent input", row).val();

    var prov = sp['s_kopsumma_bez_pvn'][k] * (prov_percent / 100);
    prov = prov.toFixed(2);

    $(".prov span", row).html(prov);
    sp['s_prov'][k] = prov;

    $(".prov_percent span", row).html(prov_percent);

    var prov_apm_percent = 0;
    if (prov > 0) {
      prov_apm_percent = sp['s_apm_prov'][k] * 100 / prov;
    }
    prov_apm_percent = Math.round(prov_apm_percent * 100) / 100;

    $(".prov_apm_percent span", row).html(prov_apm_percent + '%');
    sp['s_apm_prov_percent'][k] = prov_apm_percent;

    if (row.is(':visible')) {

      ligumuskaits ++;

      vid_prov_percent += parseFloat(prov_percent);

      if(sp['s_summa'][k] > 0) ligumusumma += parseFloat(sp['s_summa'][k].replace(',','.'));
      if(sp['s_pvn'][k] > 0) pvnsumma += parseFloat(sp['s_pvn'][k].replace(',','.'));
      if(sp['s_kopsumma'][k] > 0) ligumukopsumma += parseFloat(sp['s_kopsumma'][k].replace(',','.'));
      if(sp['s_apm'][k] > 0) apmsumma += parseFloat(sp['s_apm'][k].replace(',','.'));
      if(sp['s_prov'][k] > 0) provsumma += parseFloat(sp['s_prov'][k].replace(',','.'));
      if(sp['s_apm_prov'][k] > 0) apmprovsumma += parseFloat(sp['s_apm_prov'][k].replace(',','.'));

    }

  });

  $('#ligumusumma').html(parseFloat(ligumusumma).toFixed(2)+'&nbsp;Ls');
  $('#ligumusummavid').html( parseFloat(ligumusumma / ligumuskaits).toFixed(2) + '&nbsp;Ls' );

  $('#pvnsumma').html(parseFloat(pvnsumma).toFixed(2)+'&nbsp;Ls');

  $('#ligumukopsumma').html(parseFloat(ligumukopsumma).toFixed(2)+'&nbsp;Ls');
  $('#ligumukopsummavid').html(parseFloat(ligumukopsumma / ligumuskaits).toFixed(2)+'&nbsp;Ls');

  $('#apmsumma').html(parseFloat(apmsumma).toFixed(2)+'&nbsp;Ls');

  $('#provpercentvid').html((Math.round(parseFloat(vid_prov_percent / ligumuskaits) * 100) / 100) + '%')

  $('#provsumma').html(parseFloat(provsumma).toFixed(2)+'&nbsp;Ls');
  $('#provsummavid').html(parseFloat(provsumma / ligumuskaits).toFixed(2)+'&nbsp;Ls');

  $('#apmprovsumma').html(parseFloat(apmprovsumma).toFixed(2)+'&nbsp;Ls');
  $('#apmprovsummavid').html(parseFloat(apmprovsumma / ligumuskaits).toFixed(2)+'&nbsp;Ls');

  $('#apmprovpercentvid').html((Math.round(parseFloat(((apmprovsumma / ligumuskaits) * 100) / (provsumma / ligumuskaits)) * 100) / 100) + '%');

  updateNpk(table);

}

function updateNpk(table) {

  // updating npk

  var visible_rows = $('tr.lig_row:visible', table);

  var npk = visible_rows.length;

  visible_rows.each(function() {

    $('.s_id', this).html(npk);

    npk --;

  });

}
</script>

<? show_files_block('darb_ligumi', isset($_GET['id']) ? $_GET['id'] : null) ?>

<? show_comments_block('darb_ligumi', isset($_GET['id']) ? $_GET['id'] : null) ?>