<script>
var searchlist_rek = [];

var sp_rek = create_filter_field_array([
  's_izr_dat', 's_num', 's_summa', 's_pvn_percent', 's_pvn',
  's_kopsumma', 's_statuss', 's_apm_dat'
]);
</script>

<table style="display:none;">

  <tr id="darbinieku_norekini_filter">

    <th>
      <span><input class="search_field kalendari" meth="from" searchclass="s_izr_dat" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span><br>
      <span><input class="search_field kalendari" meth="to" searchclass="s_izr_dat" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span>
    </th>

    <th><span><input class="search_field" meth="standart" searchclass="s_num" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <th><span><input class="search_field" meth="coin" searchclass="s_summa" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <th><span><input class="search_field" meth="coin" searchclass="s_pvn_percent" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <th><span><input class="search_field" meth="coin" searchclass="s_pvn" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <th><span><input class="search_field" meth="coin" searchclass="s_kopsumma" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>

    <th>
      <select class="search_field advonchange" searchclass="s_statuss" meth="int">
        <option value=""></option>
        <option value="2">Jā</option>
        <option value="1">Nē</option>
      </select>
    </th>

    <th>
      <span><input class="search_field kalendari" meth="from" searchclass="s_apm_dat" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span><br>
      <span><input class="search_field kalendari" meth="to" searchclass="s_apm_dat" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span>
    </th>

    <th></th>

  </tr>

</table>

<h3>Izrakstītie rēķini</h3>

<? if (is_admin() && isset($_GET['id'])) { ?>
  <p>
    <button class="ui-state-default ui-corner-all onlyinview" onclick="showRekinaIzveide(<?= $_GET['id'] ?>); return false;">Pievienot jaunu rēķinu</button>
  </p>
<?php } ?>

<table style="width: 970px;" id="darbinieku_norekini" class="data">

  <thead>

    <tr>
      <th width="100">Izrakst.&nbsp;datums</th>
      <th>Numurs</th>
      <th>Summa&nbsp;Ls</th>
      <th>PVN&nbsp;%</th>
      <th>PVN&nbsp;Ls</th>
      <th>Kopsumma&nbsp;Ls</th>
      <th>Apmaksāts</th>
      <th>Apm.&nbsp;datums</th>
      <th></th>
    </tr>

  </thead>

  <tbody class="main">

    <? $inid = -1; ?>

    <? foreach($pard_rekini as $rekins) { ?>

      <? $inid ++;  ?>

      <tr id="rek_tr<?=$inid;?>" class="tr_rek">
        <td><input type="text" name="rekini[<?=$rekins['id'];?>][izr_datums]" value="<?=!empty($rekins['izr_datums'])?date('d.m.Y', strtotime($rekins['izr_datums'])):"";?>" class="showcalendar"></td>
        <td class="rekinanrclass"><input type="text" name="rekini[<?=$rekins['id'];?>][rek_nr]" value="<?=$rekins['rek_nr'];?>" /></td>
        <td class="summa"><?=!empty($rekins['summa'])?$rekins['summa']:"";?></td>
        <td class="pvn_percent"><?=!empty($rekins['pvn'])?round($rekins['pvn'], 2):0;?>%</td>
        <td><span class="pvn"><?= format_currency($rekins['kopsumma'] - $rekins['summa']) ?></span></td>
        <td class="kopsumma"><?=!empty($rekins['kopsumma'])?$rekins['kopsumma']:"";?></td>
        <td align="center"><input class="apmaksats" type="checkbox" value="1" name="rekini[<?=$rekins['id'];?>][apmaksats]" <?=($rekins['statuss'] == 2)?"checked":"";?>></td>
        <td><input type="text" name="rekini[<?=$rekins['id'];?>][apm_datums]" value="<?=!empty($rekins['apm_datums'])?date('d.m.Y', strtotime($rekins['apm_datums'])):"";?>" class="showcalendar"></td>
        <td class="last" style="white-space: nowrap;">
          <button onclick="showRekinaDetalas(<?=$rekins['id'];?>); return false;" class="onlyinview" style="font-size: 0.75em;">Detalizācija</button>
          <? if (empty($rekins['uz_rokas'])) { ?>
            <button onclick="window.location.href='?c=darbinieki&a=gen_rek&rek_id=<?= $rekins['id'] ?>'; return false;" class="onlyinview" style="font-size: 0.75em;">Pārģen. PDF</button>
          <? } ?>
          <? if (!empty($rekins['fails_ep'])) { ?>
            <a class="onlyinview" href="?getfile=<?=$rekins['fails_ep'];?>" target="downloadframe" title="Lejupielādēt PDF"><img src="css/pdf.gif" alt="PDF" style="vertical-align: middle;" /></a>
          <? } ?>
          <a href="#" class="hidethistoo" onclick="deletePardRek(<?= $rekins['id'] ?>); return false;" title="Dzēst"><img src="css/del.png" alt="Dzēst" /></a>
        </td>

        <td style="display: none;">
        <script>

          searchlist_rek[<?=$inid;?>] = '<?=addslashes(mb_strtoupper($rekins['rek_nr']));?>';

          sp_rek['s_izr_dat'][<?=$inid;?>] = <?= !empty($rekins['izr_datums']) ? date('Ymd', strtotime($rekins['izr_datums'])) : 0 ?>;
          sp_rek['s_num'][<?=$inid;?>] = '<?=addslashes(mb_strtoupper($rekins['rek_nr']));?>';
          sp_rek['s_summa'][<?=$inid;?>] = '<?=!empty($rekins['summa']) ? $rekins['summa'] : "" ?>';
          sp_rek['s_pvn_percent'][<?=$inid;?>] = '<?=!empty($rekins['pvn']) ? $rekins['pvn'] : "" ?>';
          sp_rek['s_pvn'][<?=$inid;?>] = '<?= round($rekins['kopsumma'] - $rekins['summa'], 2) ?>';
          sp_rek['s_kopsumma'][<?=$inid;?>] = '<?=!empty($rekins['kopsumma']) ? $rekins['kopsumma'] : "" ?>';
          sp_rek['s_statuss'][<?=$inid;?>] = <?= $rekins['statuss'] ?>;
          sp_rek['s_apm_dat'][<?=$inid;?>] = <?= !empty($rekins['apm_datums']) ? date('Ymd', strtotime($rekins['apm_datums'])) : 0 ?>;

        </script>
        </td>

      </tr>

    <? } ?>

  </tbody>

  <tr>
    <td colspan="2" align="right"><strong>Kopā:</strong></td>
    <td id="summakopa">0</td>
    <td></td>
    <td id="pvnkopa">0</td>
    <td id="kopsummakopa">0</td>
    <td id="apmsummakopa" align="center">0</td>
    <td></td>
    <td></td>
  </tr>

</table>

<script>

function deletePardRek(pardeveja_rekina_id) {

  if (confirm('Vai tiešām izdzēst pārdevēja rēķinu?')) {

    $.post(
      "?c=darbinieki&a=del_rek",
      {pardeveja_rekina_id: pardeveja_rekina_id},
      function() {
        window.location.href = $('#fullformplace').attr('action') + '&subtab=1';
      }
    );

  }

  return false;

}

function showRekinaIzveide(darbinieka_id) {

  $('<iframe src="?c=darbinieki&a=rek_izveide&darbinieka_id='+darbinieka_id+'&without_nav=1" />').dialog({
      title: 'Jauns rēķins',
      autoOpen: true,
      width: 970,
      height: $(window).height() - 50,
      modal: true,
      resizable: false,
      draggable: false,
      closeOnEscape: false
  }).width(950);

}

function showRekinaDetalas(darbinieka_rek_id) {

  $('<iframe src="?c=darbinieki&a=rek_detalas&darbinieka_rek_id='+darbinieka_rek_id+'&without_nav=1" />').dialog({
      title: 'Rēķina detalizācija',
      autoOpen: true,
      width: 970,
      height: $(window).height() - 50,
      modal: true,
      resizable: false,
      draggable: false,
      closeOnEscape: false
  }).width(950);

}

$(window).load(function() {

  var table = $("#darbinieku_norekini");
  var head = $("#darbinieku_norekini_filter");

  table.tablesorter({
    debug: false,
    highlightClass: 'highlight',
    headers: {
      0: { sorter:'latviandate' },
      2: { sorter:'currency' },
      3: { sorter:'digit' },
      4: { sorter:'digit' },
      5: { sorter:'currency' }
    },
    textExtraction: function(node) {

      var node = $(node);
      var input = node.find(':input');

      if (input.length > 0) {
        if (input.is('select')) {
          return $(':selected', input).text();
        } else {
          return input.val();
        }
      } else {
        return $(node).text();
      }

    }
  }).bind('sortStart', function() {
    table.trigger("update");
  }).bind('sortEnd', function() {
    stripeTable(table);
  });

  initTableFilter(table, head, processRekTable, 'rek', searchlist_rek, sp_rek);

  $('.apmaksats', table).click(processRekTable);

  $(".showcalendar").datepicker({dateFormat:'dd.mm.yy'});

});

function processRekTable() {

  var summakopa = 0;
  var pvnkopa = 0;
  var kopsummakopa = 0;
  var apmsummakopa = 0;

  $('#darbinieku_norekini tr.tr_rek:visible').each(function() {

    if ($('.summa', this).html()) summakopa += parseFloat($('.summa', this).html());
    if ($('.pvn', this).html()) pvnkopa += parseFloat($('.pvn', this).html());
    if ($('.kopsumma', this).html()) kopsummakopa += parseFloat($('.kopsumma', this).html());

    if ($('.apmaksats', this).is(':checked')) {
      apmsummakopa += parseFloat($('.kopsumma', this).html());
    }

  });

  $('#summakopa').html(summakopa.toFixed(2) + '&nbsp;Ls');
  $('#pvnkopa').html(pvnkopa.toFixed(2) + '&nbsp;Ls');
  $('#kopsummakopa').html(kopsummakopa.toFixed(2) + '&nbsp;Ls');
  $('#apmsummakopa').html(apmsummakopa.toFixed(2) + '&nbsp;Ls');

}
</script>

<? if (isset($_GET['id'])) { ?>

  <h3>Kontā uz izmaksu</h3>

  <?
  $query = db_query("
    SELECT
      m.*,
      g.id as liguma_id,
      g.nosaukums as klienta_nosaukums,
      g.pvn as liguma_pvn,
      g.provizija_pardevejam,
      r.rek_nr,
      r.izr_datums as rekina_izr_datums,
      r.kopsumma as rekina_kopsumma,
      (
        SELECT SUM(no_summas)
        FROM `".DB_PREF."pardeveji_rekini_detalas`
        WHERE rekina_id = m.rekina_id
      ) as prov_izmaksu_summa,
      p.pvn_likme
    FROM `".DB_PREF."rekini_maksajumi` m
    LEFT JOIN `".DB_PREF."rekini` r ON (m.rekina_id = r.id)
    LEFT JOIN `".DB_PREF."ligumi` g ON (r.liguma_id = g.id)
    LEFT JOIN `".DB_PREF."pardeveji_rekini_detalas` d ON (m.id = d.rekina_maksajuma_id)
    LEFT JOIN `".DB_PREF."pardeveji` p ON (p.id = g.pardeveja_id)
    WHERE
      g.pardeveja_id = ".$_GET['id']." AND
      d.id IS NULL AND
      r.atkapsanas_rekins = 0 AND
      r.nodots_piedz = 0
    HAVING
      m.summa <= IFNULL(r.kopsumma, 0) - IFNULL(prov_izmaksu_summa, 0)
    ORDER BY m.datums DESC, m.id DESC
  ");

  $apmaksas = array();
  while($row = db_get_assoc($query)) {
    $apmaksas[$row['id']] = $row;
  }
  ?>

  <script>
  var searchlist_uzizm = [];

  var sp_uzizm = create_filter_field_array([
    's_klienta_nosaukums', 's_rek_nr', 's_rek_izr_datums', 's_summa', 's_datums',
    's_prov_percent', 's_prov_summa', 's_prov_pvn', 's_prov_kopsumma'
  ]);
  </script>

  <table style="display:none;">

    <tr id="uz_izmaksu_filter">

      <th><span><input class="search_field" meth="standart" searchclass="s_klienta_nosaukums" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
      <th><span><input class="search_field" meth="standart" searchclass="s_rek_nr" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>

      <th>
        <span><input class="search_field kalendari" meth="from" searchclass="s_rek_izr_datums" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span><br>
        <span><input class="search_field kalendari" meth="to" searchclass="s_rek_izr_datums" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span>
      </th>

      <th><span><input class="search_field" meth="coin" searchclass="s_summa" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>

      <th>
        <span><input class="search_field kalendari" meth="from" searchclass="s_datums" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span><br>
        <span><input class="search_field kalendari" meth="to" searchclass="s_datums" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span>
      </th>

      <th><span><input class="search_field" meth="coin" searchclass="s_prov_percent" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
      <th><span><input class="search_field" meth="coin" searchclass="s_prov_summa" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
      <th><span><input class="search_field" meth="coin" searchclass="s_prov_pvn" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
      <th><span><input class="search_field" meth="coin" searchclass="s_prov_kopsumma" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>

    </tr>

  </table>

  <table id="uz_izmaksu" class="data" style="width: 970px;">

    <thead>

      <tr class="last">
        <th>Klients</th>
        <th>Rēķina nr.</th>
        <th>Rēķins izstādīts</th>
        <th>Saņemtā summa Ls</th>
        <th>Apmaksas datums</th>
        <th>Provīzija %</th>
        <th>Bez PVN Ls</th>
        <th>PVN Ls</th>
        <th>Kopā Ls</th>
      </tr>

    </thead>

    <tbody class="main">

      <? $inid = -1; ?>

      <? foreach($apmaksas as $apmaksa) { ?>

        <? $inid ++;  ?>

        <?
        $apm_summa_bez_pvn = $apmaksa['summa'] / (1 + ($apmaksa['liguma_pvn'] / 100));
        $prov_summa = $apm_summa_bez_pvn * ($apmaksa['provizija_pardevejam'] / 100);

        $prov_pvn = $prov_summa * ($apmaksa['pvn_likme'] / 100);
        ?>

        <tr id="uzizm_tr<?=$inid;?>" class="tr_uzizm">
          <td><a href="?c=ligumi&a=labot&id=&a=labot&id=<?= $apmaksa['liguma_id'] ?>&subtab=2"><?= $apmaksa['klienta_nosaukums'] ?></a></td>
          <td><?= $apmaksa['rek_nr'] ?></td>
          <td><?= date('d.m.Y', strtotime($apmaksa['rekina_izr_datums'])) ?></td>
          <td><?= $apmaksa['summa'] ?></td>
          <td><?= date('d.m.Y', strtotime($apmaksa['datums'])) ?></td>
          <td><?= round($apmaksa['provizija_pardevejam'], 2) ?>%</td>
          <td><?= format_currency($prov_summa) ?></td>
          <td><?= format_currency($prov_pvn) ?></td>
          <td class="last"><?= format_currency($prov_summa + $prov_pvn) ?></td>
          <td style="display: none;">
            <script>

              searchlist_uzizm[<?=$inid;?>] = '<?=addslashes(mb_strtoupper($apmaksa['rek_nr']));?>';

              sp_uzizm['s_klienta_nosaukums'][<?=$inid;?>] = '<?=addslashes(mb_strtoupper($apmaksa['klienta_nosaukums']));?>';
              sp_uzizm['s_rek_nr'][<?=$inid;?>] = '<?=addslashes(mb_strtoupper($apmaksa['rek_nr']));?>';
              sp_uzizm['s_rek_izr_datums'][<?=$inid;?>] = <?= !empty($apmaksa['rekina_izr_datums']) ? date('Ymd', strtotime($apmaksa['rekina_izr_datums'])) : 0 ?>;
              sp_uzizm['s_summa'][<?=$inid;?>] = '<?=!empty($apmaksa['summa']) ? $apmaksa['summa'] : "" ?>';
              sp_uzizm['s_datums'][<?=$inid;?>] = <?= !empty($apmaksa['datums']) ? date('Ymd', strtotime($apmaksa['datums'])) : 0 ?>;
              sp_uzizm['s_prov_percent'][<?=$inid;?>] = '<?=!empty($apmaksa['provizija_pardevejam']) ? $apmaksa['provizija_pardevejam'] : "" ?>';
              sp_uzizm['s_prov_summa'][<?=$inid;?>] = '<?=!empty($prov_summa) ? $prov_summa : "" ?>';
              sp_uzizm['s_prov_pvn'][<?=$inid;?>] = '<?=!empty($prov_pvn) ? $prov_pvn : "" ?>';
              sp_uzizm['s_prov_kopsumma'][<?=$inid;?>] = '<?=$prov_summa + $prov_pvn ?>';

            </script>
          </td>
        </tr>

      <? } ?>

    </tbody>

    <tr>

      <td align="right" colspan="3"><strong>Kopā:</strong></td>
      <td id="uzizm_sanemta_summa">0</td>
      <td></td>
      <td></td>
      <td id="uzizm_prov_summa">0</td>
      <td id="uzizm_prov_pvn">0</td>
      <td id="uzizm_prov_kopsumma">0</td>

    </tr>

  </table>

  <script>
  $(window).load(function() {

    var table = $("#uz_izmaksu");
    var head = $("#uz_izmaksu_filter");

    table.tablesorter({
      debug: false,
      highlightClass: 'highlight',
      headers: {
        0: {sorter: 'latviantext'},
        2: {sorter: 'latviandate'},
        4: {sorter: 'latviandate'}
      }
    }).bind('sortStart', function() {
      table.trigger("update");
    }).bind('sortEnd', function() {
      stripeTable(table);
    });

    initTableFilter(table, head, processUzIzmTable, 'uzizm', searchlist_uzizm, sp_uzizm);

  });

  function processUzIzmTable() {

    var uzizm_sanemta_summa = 0;
    var uzizm_prov_summa = 0;
    var uzizm_prov_pvn = 0;
    var uzizm_prov_kopsumma = 0;

    jQuery.each(searchlist_uzizm, function(k,v){

      var row = $('#uzizm_tr' + k);

      if (row.is(':visible')) {

        if (sp_uzizm['s_summa'][k] > 0) uzizm_sanemta_summa += parseFloat(sp_uzizm['s_summa'][k]);
        if (sp_uzizm['s_prov_summa'][k] > 0) uzizm_prov_summa += parseFloat(sp_uzizm['s_prov_summa'][k]);
        if (sp_uzizm['s_prov_pvn'][k] > 0) uzizm_prov_pvn += parseFloat(sp_uzizm['s_prov_pvn'][k]);
        if (sp_uzizm['s_prov_kopsumma'][k] > 0) uzizm_prov_kopsumma += parseFloat(sp_uzizm['s_prov_kopsumma'][k]);

      }

    });

    $('#uzizm_sanemta_summa').html(uzizm_sanemta_summa.toFixed(2));
    $('#uzizm_prov_summa').html(uzizm_prov_summa.toFixed(2));
    $('#uzizm_prov_pvn').html(uzizm_prov_pvn.toFixed(2));
    $('#uzizm_prov_kopsumma').html(uzizm_prov_kopsumma.toFixed(2));

  }
  </script>

  <script>
  var searchlist_nakmaks = [];

  var sp_nakmaks = create_filter_field_array([
    's_klienta_nosaukums', 's_rek_nr', 's_rek_izr_datums', 's_apm_termins',
    's_rekina_statuss', 's_summa', 's_neapm_summa', 's_prov_percent', 's_prov_summa',
    's_prov_pvn', 's_prov_kopsumma'
  ]);
  </script>

  <?
  $query = db_query("
    SELECT
      r.*,
      g.nosaukums as klienta_nosaukums,
      g.pvn as liguma_pvn,
      g.provizija_pardevejam,
      (IFNULL(r.kopsumma, 0) - SUM(IFNULL(m.summa, 0))) as neapm_summa,
      (
        SELECT SUM(no_summas)
        FROM `".DB_PREF."pardeveji_rekini_detalas`
        WHERE rekina_id = r.id
      ) as prov_izmaksu_summa,
      p.pvn_likme
    FROM `".DB_PREF."rekini` r
    LEFT JOIN `".DB_PREF."ligumi` g ON (r.liguma_id = g.id)
    LEFT JOIN `".DB_PREF."rekini_maksajumi` m ON (m.rekina_id = r.id)
    LEFT JOIN `".DB_PREF."pardeveji` p ON (p.id = g.pardeveja_id)
    WHERE
      g.pardeveja_id = ".$_GET['id']." AND
      r.anulets = 0 AND
      r.nodots_piedz = 0 AND
      r.atkapsanas_rekins = 0 AND
      r.nodots_piedz = 0
    GROUP BY r.id
    HAVING
      neapm_summa > 0 AND
      neapm_summa <= IFNULL(r.kopsumma, 0) - IFNULL(prov_izmaksu_summa, 0)
  ");

  $avansa_rekini = array();
  while($row = db_get_assoc($query)) {
    $avansa_rekini[$row['id']] = $row;
  }
  ?>

  <h3>Nākotnes maksājumi</h3>

  <table style="display:none;">

    <tr id="nakotnes_maksajumi_filter">

      <th><span><input class="search_field" meth="standart" searchclass="s_klienta_nosaukums" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
      <th><span><input class="search_field" meth="standart" searchclass="s_rek_nr" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>

      <th>
        <span><input class="search_field kalendari" meth="from" searchclass="s_rek_izr_datums" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span><br>
        <span><input class="search_field kalendari" meth="to" searchclass="s_rek_izr_datums" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span>
      </th>

      <th>
        <span><input class="search_field kalendari" meth="from" searchclass="s_apm_termins" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span><br>
        <span><input class="search_field kalendari" meth="to" searchclass="s_apm_termins" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span>
      </th>

    <th>
      <select class="search_field advonchange" searchclass="s_rekina_statuss" meth="standart">
        <option value=""></option>
        <?php foreach($_vars['apmaksas_statuss'] as $k=>$p){ ?>
          <?php if ($k == 1) continue; // do not show "Nav sācies" ?>
          <option value="<?=$k;?>"><?=$p;?></option>
        <?php } ?>
      </select>
    </th>

      <th><span><input class="search_field" meth="coin" searchclass="s_summa" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
      <th><span><input class="search_field" meth="coin" searchclass="s_neapm_summa" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
      <th><span><input class="search_field" meth="coin" searchclass="s_prov_percent" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
      <th><span><input class="search_field" meth="coin" searchclass="s_prov_summa" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
      <th><span><input class="search_field" meth="coin" searchclass="s_prov_pvn" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
      <th><span><input class="search_field" meth="coin" searchclass="s_prov_kopsumma" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>

    </tr>

  </table>

  <table id="nakotnes_maksajumi" class="data" style="width: 970px;">

    <thead>

      <tr class="last">
        <th>Klients</th>
        <th>Rēķina nr.</th>
        <th>Rēķins izstādīts</th>
        <th>Apmaksas termiņš</th>
        <th>Apmaksas statuss</th>
        <th>Rēķina summa Ls</th>
        <th>Rēķina neapm. summa Ls</th>
        <th>Provīzija %</th>
        <th>Bez PVN Ls</th>
        <th>PVN Ls</th>
        <th>Kopā Ls</th>
      </tr>

    </thead>

    <tbody class="main">

      <? $inid = -1; ?>

      <? foreach($avansa_rekini as $avansa) { ?>

        <? $inid ++;  ?>

        <?
        $apm_summa_bez_pvn = $avansa['neapm_summa'] / (1 + ($avansa['liguma_pvn'] / 100));
        $prov_summa = $apm_summa_bez_pvn * ($avansa['provizija_pardevejam'] / 100);

        $prov_pvn = $prov_summa * ($avansa['pvn_likme'] / 100);
        ?>

        <tr id="nakmaks_tr<?=$inid;?>" class="tr_nakmaks">
          <td><a href="?c=ligumi&a=labot&id=&a=labot&id=<?= $avansa['liguma_id'] ?>&subtab=2"><?= $avansa['klienta_nosaukums'] ?></a></td>
          <td><?= $avansa['rek_nr'] ?></td>
          <td><?= !empty($avansa['izr_datums']) ? date('d.m.Y', strtotime($avansa['izr_datums'])) : '' ?></td>
          <td><?= !empty($avansa['apm_termins']) ? date('d.m.Y', strtotime($avansa['apm_termins'])) : '' ?></td>
          <td>
            <?
            $status = get_rekina_statuss($avansa);
            if (!empty($status)) {
              echo $_vars['apmaksas_statuss'][$status];
            }
            ?>
          </td>
          <td><?= $avansa['kopsumma'] ?></td>
          <td><?= $avansa['neapm_summa'] ?></td>
          <td><?= round($avansa['provizija_pardevejam'], 2) ?>%</td>
          <td><?= format_currency($prov_summa) ?></td>
          <td><?= format_currency($prov_pvn) ?></td>
          <td class="last"><?= format_currency($prov_summa + $prov_pvn) ?></td>
          <td style="display: none;">
            <script>

              searchlist_nakmaks[<?=$inid;?>] = '<?=addslashes(mb_strtoupper($avansa['rek_nr']));?>';

              sp_nakmaks['s_klienta_nosaukums'][<?=$inid;?>] = '<?=addslashes(mb_strtoupper($avansa['klienta_nosaukums']));?>';
              sp_nakmaks['s_rek_nr'][<?=$inid;?>] = '<?=addslashes(mb_strtoupper($avansa['rek_nr']));?>';
              sp_nakmaks['s_rek_izr_datums'][<?=$inid;?>] = <?= !empty($avansa['izr_datums']) ? date('Ymd', strtotime($avansa['izr_datums'])) : 0 ?>;
              sp_nakmaks['s_apm_termins'][<?=$inid;?>] = <?= !empty($avansa['apm_termins']) ? date('Ymd', strtotime($avansa['apm_termins'])) : 0 ?>;
              sp_nakmaks['s_rekina_statuss'][<?=$inid;?>] = '<?=$status?>';
              sp_nakmaks['s_summa'][<?=$inid;?>] = '<?=!empty($avansa['kopsumma']) ? $avansa['kopsumma'] : "" ?>';
              sp_nakmaks['s_neapm_summa'][<?=$inid;?>] = '<?=!empty($avansa['neapm_summa']) ? $avansa['neapm_summa'] : "" ?>';
              sp_nakmaks['s_prov_percent'][<?=$inid;?>] = '<?=!empty($avansa['provizija_pardevejam']) ? $avansa['provizija_pardevejam'] : "" ?>';
              sp_nakmaks['s_prov_summa'][<?=$inid;?>] = '<?=!empty($prov_summa) ? $prov_summa : "" ?>';
              sp_nakmaks['s_prov_pvn'][<?=$inid;?>] = '<?=!empty($prov_pvn) ? $prov_pvn : "" ?>';
              sp_nakmaks['s_prov_kopsumma'][<?=$inid;?>] = '<?=$prov_summa + $prov_pvn ?>';

            </script>
          </td>
        </tr>

      <? } ?>

    </tbody>

    <tr>

      <td align="right" colspan="5"><strong>Kopā:</strong></td>
      <td id="nakmaks_rekinu_summa">0</td>
      <td id="nakmaks_neapm_summa">0</td>
      <td></td>
      <td id="nakmaks_prov_summa">0</td>
      <td id="nakmaks_prov_pvn">0</td>
      <td id="nakmaks_prov_kopsumma">0</td>

    </tr>

  </table>

  <script>
  $(window).load(function() {

    var table = $("#nakotnes_maksajumi");
    var head = $("#nakotnes_maksajumi_filter");

    table.tablesorter({
      debug: false,
      highlightClass: 'highlight',
      headers: {
        0: {sorter: 'latviantext'},
        2: {sorter: 'latviandate'},
        3: {sorter: 'latviandate'}
      }
    }).bind('sortStart', function() {
      table.trigger("update");
    }).bind('sortEnd', function() {
      stripeTable(table);
    });

    initTableFilter(table, head, processNakMaksTable, 'nakmaks', searchlist_nakmaks, sp_nakmaks);

  });

  function processNakMaksTable() {

    var nakmaks_rekinu_summa = 0;
    var nakmaks_neapm_summa = 0;
    var nakmaks_prov_summa = 0;
    var nakmaks_prov_pvn = 0;
    var nakmaks_prov_kopsumma = 0;

    jQuery.each(searchlist_nakmaks, function(k,v){

      var row = $('#nakmaks_tr' + k);

      if (row.is(':visible')) {

        if (sp_nakmaks['s_summa'][k] > 0) nakmaks_rekinu_summa += parseFloat(sp_nakmaks['s_summa'][k]);
        if (sp_nakmaks['s_neapm_summa'][k] > 0) nakmaks_neapm_summa += parseFloat(sp_nakmaks['s_neapm_summa'][k]);
        if (sp_nakmaks['s_prov_summa'][k] > 0) nakmaks_prov_summa += parseFloat(sp_nakmaks['s_prov_summa'][k]);
        if (sp_nakmaks['s_prov_pvn'][k] > 0) nakmaks_prov_pvn += parseFloat(sp_nakmaks['s_prov_pvn'][k]);
        if (sp_nakmaks['s_prov_kopsumma'][k] > 0) nakmaks_prov_kopsumma += parseFloat(sp_nakmaks['s_prov_kopsumma'][k]);

      }

    });

    $('#nakmaks_rekinu_summa').html(nakmaks_rekinu_summa.toFixed(2));
    $('#nakmaks_neapm_summa').html(nakmaks_neapm_summa.toFixed(2));
    $('#nakmaks_prov_summa').html(nakmaks_prov_summa.toFixed(2));
    $('#nakmaks_prov_pvn').html(nakmaks_prov_pvn.toFixed(2));
    $('#nakmaks_prov_kopsumma').html(nakmaks_prov_kopsumma.toFixed(2));

  }
  </script>

<? } ?>

<? show_files_block('darb_norekini', isset($_GET['id']) ? $_GET['id'] : null) ?>

<? show_comments_block('darb_norekini', isset($_GET['id']) ? $_GET['id'] : null) ?>