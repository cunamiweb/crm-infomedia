<table width="600">

  <tr><td colspan="2"><h2>Kontakti</h2></td></tr>
  <tr><td>Vārds, uzvārds:</td><td><input class="non_admin_edit" type="input" id="vardsplace" name="vards" value="<?=!empty($d['vards'])?$d['vards']:""?>"></td></tr>

  <?php foreach($telefoni as $v) { ?>
    <tr><td>Telefons:</td><td><input class="non_admin_edit" style="width: 95%;" type="input" name="telefoni[<?= $v['id'] ?>]" value="<?= $v['telefons'] ?>"> <a style="float: right; display: block;" class="hidethistoo non_admin_edit" onclick="$(this).parent().parent().remove();" href="javascript:;">[x]</a></td></tr>
  <?php } ?>

  <tr><td colspan="2"><button onClick="$(this).parent().parent().before($('#telefonsprot tbody').html()); return false;" class="ui-state-default ui-corner-all hidethistoo non_admin_edit">Pievienot telefonu</button></td></tr>

  <?php foreach($epasti as $v) { ?>
    <tr><td>Epasts:</td><td><input class="non_admin_edit" style="width: 95%;" type="input" name="epasti[<?= $v['id'] ?>]" value="<?= $v['epasts'] ?>"> <a style="float: right; display: block;" class="hidethistoo non_admin_edit" onclick="$(this).parent().parent().remove();" href="javascript:;">[x]</a></td></tr>
  <?php } ?>

  <tr ><td colspan="2"><button onClick="$(this).parent().parent().before($('#epastsprot tbody').html()); return false;" class="ui-state-default ui-corner-all hidethistoo non_admin_edit">Pievienot epastu</button></td></tr>

  <tr><td colspan="2"><h2>Pieejas dati</h2></td></tr>

  <?php if (empty($_GET['id'])) { ?>
    <tr><td>Lietotājvārds:</td><td><input type="input" name="lietotajvards" value="<?=!empty($d['lietotajvards'])?$d['lietotajvards']:""?>"></td></tr>
  <?php } else { ?>
    <tr><td>Lietotājvārds:</td><td><?=!empty($d['lietotajvards'])?$d['lietotajvards']:""?></td></tr>
  <?php } ?>

  <tr><td>Parole:</td><td><input class="non_admin_edit" id="password1" type="password" name="password1" value=""></td></tr>
  <tr><td>Parole atkārtoti:</td><td><input class="non_admin_edit" id="password2" type="password" name="password2" value=""></td></tr>

  <tr><td colspan="2"><h2>Norēķinu rekvizīti</h2></td></tr>

  <tr><td>Jur. pers. nosaukums:</td><td><input type="input" id="jurpers_nosauk" name="jurpers_nosauk" value="<?=!empty($d['jurpers_nosauk'])?$d['jurpers_nosauk']:""?>"></td></tr>
  <tr><td>Jur. pers. adrese:</td><td><input type="input" id="jurpers_adrese" name="jurpers_adrese" value="<?=!empty($d['jurpers_adrese'])?$d['jurpers_adrese']:""?>"></td></tr>
  <tr><td>Reģ. nr./ Pers. kods:</td><td><input type="input" id="reg_nr" name="reg_nr" value="<?=!empty($d['reg_nr'])?$d['reg_nr']:""?>"></td></tr>
  <tr><td>PVN maksātājs:</td><td><input type="checkbox" name="pvn_maksatajs" <?=!empty($d['pvn_maksatajs'])?"checked":""?> /></td></tr>
  <tr><td>PVN nr.:</td><td><input type="input" id="pvn_nr" name="pvn_nr" value="<?=!empty($d['pvn_nr'])?$d['pvn_nr']:""?>"></td></tr>
  <tr><td>PVN likme:</td><td><input type="input" id="pvn_likme" name="pvn_likme" value="<?=!empty($d['pvn_likme'])?$d['pvn_likme']:""?>"></td></tr>
  <tr><td>Konta numurs:</td><td><input type="input" id="konta_nr" name="konta_nr" value="<?=!empty($d['konta_nr'])?$d['konta_nr']:""?>"></td></tr>
  <tr><td>Norēķinu rekvizīti:</td><td><input type="input" id="norek_rekviziti" name="norek_rekviziti" value="<?=!empty($d['norek_rekviziti'])?$d['norek_rekviziti']:""?>"></td></tr>
  <tr><td>Aģenta līg. nr.:</td><td><input type="input" id="agenta_lig_nr" name="agenta_lig_nr" value="<?=!empty($d['agenta_lig_nr'])?$d['agenta_lig_nr']:""?>"></td></tr>
  <tr><td>Aģenta līg. datums:</td><td><input type="input" name="agenta_lig_datums" id="agenta_lig_datums" value="<?=!empty($d['agenta_lig_datums'])?date('d.m.Y', strtotime($d['agenta_lig_datums'])):''?>"></td></tr>

</table>

<? show_files_block('darb_rekviziti', isset($_GET['id']) ? $_GET['id'] : null) ?>

<? show_comments_block('darb_rekviziti', isset($_GET['id']) ? $_GET['id'] : null) ?>