<?
if (!is_admin()) {
  die();
}

if (empty($_GET['darbinieka_id'])) {
  die();
}

$darbinieka_id = (int) $_GET['darbinieka_id'];

$darbinieks = db_get_assoc(db_query("
  SELECT *
  FROM `".DB_PREF."pardeveji`
  WHERE id = ".$darbinieka_id."
"));

if (empty($darbinieks)) {
  die();
}

$query = db_query("
  SELECT
    m.*,
    g.nosaukums as klienta_nosaukums,
    g.pvn as liguma_pvn,
    g.provizija_pardevejam,
    r.rek_nr,
    r.kopsumma as rekina_kopsumma,
    (
      SELECT SUM(no_summas)
      FROM `".DB_PREF."pardeveji_rekini_detalas`
      WHERE rekina_id = m.rekina_id
    ) as prov_izmaksu_summa
  FROM `".DB_PREF."rekini_maksajumi` m
  LEFT JOIN `".DB_PREF."rekini` r ON (m.rekina_id = r.id)
  LEFT JOIN `".DB_PREF."ligumi` g ON (r.liguma_id = g.id)
  LEFT JOIN `".DB_PREF."pardeveji_rekini_detalas` d ON (m.id = d.rekina_maksajuma_id)
  WHERE
    g.pardeveja_id = ".$darbinieka_id." AND
    d.id IS NULL AND
    r.atkapsanas_rekins = 0 AND
    r.nodots_piedz = 0
  HAVING
    m.summa <= IFNULL(r.kopsumma, 0) - IFNULL(prov_izmaksu_summa, 0)
");

$apmaksas = array();
while($row = db_get_assoc($query)) {
  $apmaksas[$row['id']] = $row;
}


$query = db_query("
  SELECT
    r.*,
    g.nosaukums as klienta_nosaukums,
    g.pvn as liguma_pvn,
    g.provizija_pardevejam,
    (IFNULL(r.kopsumma, 0) - SUM(IFNULL(m.summa, 0))) as neapm_summa,
    (
      SELECT SUM(no_summas)
      FROM `".DB_PREF."pardeveji_rekini_detalas`
      WHERE rekina_id = r.id
    ) as prov_izmaksu_summa
  FROM `".DB_PREF."rekini` r
  LEFT JOIN `".DB_PREF."ligumi` g ON (r.liguma_id = g.id)
  LEFT JOIN `".DB_PREF."rekini_maksajumi` m ON (m.rekina_id = r.id)
  WHERE
    g.pardeveja_id = ".$darbinieka_id." AND
    r.atkapsanas_rekins = 0 AND
    r.anulets = 0 AND
    r.nodots_piedz = 0
  GROUP BY r.id
  HAVING
    neapm_summa > 0 AND
    neapm_summa <= IFNULL(r.kopsumma, 0) - IFNULL(prov_izmaksu_summa, 0)
");

$avansa_rekini = array();
while($row = db_get_assoc($query)) {
  $avansa_rekini[$row['id']] = $row;
}


if (!empty($_POST)) {

  $_POST = trim_array($_POST);

  $errors = array();

  $uz_rokas_skaits = 0;
  $rekinaa_skaits = 0;

  if (!empty($_POST['apm'])) {

    foreach($_POST['apm'] as $apm_id => $v) {

      if ($v['uz_rokas'] === '0') {
        $rekinaa_skaits ++;
      } else if ($v['uz_rokas'] === '1') {
        $uz_rokas_skaits ++;
      }

    }

  }

  if (!empty($_POST['avansa'])) {

    foreach($_POST['avansa'] as $rek_id => $v) {

      if ($v['uz_rokas'] === '0') {
        $rekinaa_skaits ++;
      } else if ($v['uz_rokas'] === '1') {
        $uz_rokas_skaits ++;
      }

    }

  }

  if (empty($_POST['izr_datums'])) {
    $errors[] = 'Nav norādīts izrakstīšanas datums!';
  }

  if (empty($_POST['rek_nr'])) {
    $errors[] = 'Nav norādīts rēķina numurs!';
  }

  if (empty($uz_rokas_skaits) && empty($rekinaa_skaits)) {
    $errors[] = 'Nekas netika atzīmēts!';
  }

  if (empty($errors)) {

    $izr_datums = date('Y-m-d', strtotime($_POST['izr_datums']));

    if (!empty($uz_rokas_skaits)) {

      $rek_nr = get_darb_rek_next_no('C');

      db_query("
        INSERT INTO `".DB_PREF."pardeveji_rekini` SET
          pardeveja_id = ".$darbinieka_id.",
          rek_nr = '".esc($rek_nr)."',
          izr_datums = '".esc($izr_datums)."',
          summa = 0,
          pvn = 0,
          kopsumma = 0,
          uz_rokas = 1
      ");

      $pardeveja_rekina_id = db_last_id();

      $rekina_summa = 0;

      if (!empty($_POST['apm'])) {

        foreach($_POST['apm'] as $apm_id => $v) {

          if ($v['uz_rokas'] === '1') {

            $apm_data = $apmaksas[$apm_id];

            $apm_summa_bez_pvn = $apm_data['summa'] / (1 + ($apm_data['liguma_pvn'] / 100));
            $prov_summa = round($apm_summa_bez_pvn * ($apm_data['provizija_pardevejam'] / 100), 2);

            db_query("
              INSERT INTO `".DB_PREF."pardeveji_rekini_detalas` SET
                pardeveja_rekina_id = ".$pardeveja_rekina_id.",
                rekina_id = ".$apm_data['rekina_id'].",
                rekina_maksajuma_id = ".$apm_id.",
                summa = '".$prov_summa."',
                pvn_likme = 0,
                kopsumma = '".$prov_summa."',
                no_summas = '".$apm_data['summa']."',
                prov_likme = '".$apm_data['provizija_pardevejam']."'
            ");

            $rekina_summa += $prov_summa;

          }

        }

      }

      if (!empty($_POST['avansa'])) {

        foreach($_POST['avansa'] as $rek_id => $v) {

          if ($v['uz_rokas'] === '1') {

            $rek_data = $avansa_rekini[$rek_id];

            $neapm_summa_bez_pvn = $rek_data['neapm_summa'] / (1 + ($rek_data['liguma_pvn'] / 100));
            $prov_summa = round($neapm_summa_bez_pvn * ($rek_data['provizija_pardevejam'] / 100), 2);

            db_query("
              INSERT INTO `".DB_PREF."pardeveji_rekini_detalas` SET
                pardeveja_rekina_id = ".$pardeveja_rekina_id.",
                rekina_id = ".$rek_id.",
                rekina_maksajuma_id = null,
                summa = '".$prov_summa."',
                pvn_likme = 0,
                kopsumma = '".$prov_summa."',
                no_summas = '".$rek_data['neapm_summa']."',
                prov_likme = '".$rek_data['provizija_pardevejam']."'
            ");

            $rekina_summa += $prov_summa;

          }

        }

      }

      db_query("
        UPDATE `".DB_PREF."pardeveji_rekini` SET
          summa = '".$rekina_summa."',
          kopsumma = '".$rekina_summa."'
        WHERE id = ".$pardeveja_rekina_id."
        LIMIT 1
      ");

    }

    if (!empty($rekinaa_skaits)) {

      $pvn_likme = (float) str_replace(',', '.', $_POST['pvn_likme']);

      $rek_nr = $_POST['rek_nr'];

      db_query("
        INSERT INTO `".DB_PREF."pardeveji_rekini` SET
          pardeveja_id = ".$darbinieka_id.",
          rek_nr = '".esc($rek_nr)."',
          izr_datums = '".esc($izr_datums)."',
          summa = 0,
          pvn = '".$pvn_likme."',
          kopsumma = 0,
          uz_rokas = 0
      ");

      $pardeveja_rekina_id = db_last_id();
      $pdf_rekina_id = $pardeveja_rekina_id;

      $rekina_summa = 0;
      $rekina_summa_ar_pvn = 0;

      if (!empty($_POST['apm'])) {

        foreach($_POST['apm'] as $apm_id => $v) {

          if ($v['uz_rokas'] === '0') {

            $apm_data = $apmaksas[$apm_id];

            $apm_summa_bez_pvn = $apm_data['summa'] / (1 + ($apm_data['liguma_pvn'] / 100));
            $prov_summa = round($apm_summa_bez_pvn * ($apm_data['provizija_pardevejam'] / 100), 2);

            $prov_summa_ar_pvn = round($prov_summa + ($prov_summa * ($pvn_likme / 100)), 2);

            db_query("
              INSERT INTO `".DB_PREF."pardeveji_rekini_detalas` SET
                pardeveja_rekina_id = ".$pardeveja_rekina_id.",
                rekina_id = ".$apm_data['rekina_id'].",
                rekina_maksajuma_id = ".$apm_id.",
                summa = '".$prov_summa."',
                pvn_likme = '".$pvn_likme."',
                kopsumma = '".$prov_summa_ar_pvn."',
                no_summas = '".$apm_data['summa']."',
                prov_likme = '".$apm_data['provizija_pardevejam']."'
            ");

            $rekina_summa += $prov_summa;
            $rekina_summa_ar_pvn += $prov_summa_ar_pvn;

          }

        }

      }

      if (!empty($_POST['avansa'])) {

        foreach($_POST['avansa'] as $rek_id => $v) {

          if ($v['uz_rokas'] === '0') {

            $rek_data = $avansa_rekini[$rek_id];

            $neapm_summa_bez_pvn = $rek_data['neapm_summa'] / (1 + ($rek_data['liguma_pvn'] / 100));
            $prov_summa = round($neapm_summa_bez_pvn * ($rek_data['provizija_pardevejam'] / 100), 2);

            $prov_summa_ar_pvn = round($prov_summa + ($prov_summa * ($pvn_likme / 100)), 2);

            db_query("
              INSERT INTO `".DB_PREF."pardeveji_rekini_detalas` SET
                pardeveja_rekina_id = ".$pardeveja_rekina_id.",
                rekina_id = ".$rek_id.",
                rekina_maksajuma_id = null,
                summa = '".$prov_summa."',
                pvn_likme = '".$pvn_likme."',
                kopsumma = '".$prov_summa_ar_pvn."',
                no_summas = '".$rek_data['neapm_summa']."',
                prov_likme = '".$rek_data['provizija_pardevejam']."'
            ");

            $rekina_summa += $prov_summa;
            $rekina_summa_ar_pvn += $prov_summa_ar_pvn;

          }

        }

      }

      db_query("
        UPDATE `".DB_PREF."pardeveji_rekini` SET
          summa = '".$rekina_summa."',
          kopsumma = '".$rekina_summa_ar_pvn."'
        WHERE id = ".$pardeveja_rekina_id."
        LIMIT 1
      ");

    }

    if (!empty($pdf_rekina_id)) {
      $refresh_url = '?c=darbinieki&a=gen_rek&rek_id=' . $pdf_rekina_id;
    } else {
      $refresh_url = '?c=darbinieki&a=labot&id=' . $darbinieka_id . '&subtab=1';
    }

    ?>
    <script type="text/javascript">
    window.parent.location.href = '<?= $refresh_url ?>';
    </script>
    <?
    die();

  }

}

?>

<? if (!empty($errors)) { ?>
  <ul class="errors">
    <? foreach($errors as $error) { ?>
      <li><?= $error ?></li>
    <? } ?>
  </ul>
<? } ?>

<script>
var searchlist_maks = [];

var sp_maks= create_filter_field_array([
  's_klienta_nosaukums', 's_rek_nr', 's_rekina_kopsumma', 's_summa', 's_datums',
  's_prov_percent', 's_prov_summa', 's_prov_pvn', 's_prov_kopsumma'
]);

var searchlist_avansa = [];

var sp_avansa = create_filter_field_array([
  's_klienta_nosaukums', 's_rek_nr', 's_izr_datums', 's_rekina_kopsumma', 's_neapm_summa',
  's_prov_percent', 's_prov_summa', 's_prov_pvn', 's_prov_kopsumma'
]);
</script>

<table style="display:none;">

  <tr id="maksajumi_filter">

    <th style="text-align: center;"><a onclick="selectAllInCol('maksajumi', 'radio_n'); return false;" style="font-size: 1em; " href="#">Visi</a></th>
    <th style="text-align: center;"><a onclick="selectAllInCol('maksajumi', 'uz_rokas'); return false;" style="font-size: 1em; " href="#">Visi</a></th>
    <th style="text-align: center;"><a onclick="selectAllInCol('maksajumi', 'ieklaut_rekina'); return false;" style="font-size: 1em; " href="#">Visi</a></th>

    <th><span><input class="search_field" meth="standart" searchclass="s_klienta_nosaukums" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <th><span><input class="search_field" meth="standart" searchclass="s_rek_nr" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <th><span><input class="search_field" meth="coin" searchclass="s_rekina_kopsumma" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <th><span><input class="search_field" meth="coin" searchclass="s_summa" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>

    <th>
      <span><input class="search_field kalendari" meth="from" searchclass="s_datums" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span><br>
      <span><input class="search_field kalendari" meth="to" searchclass="s_datums" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span>
    </th>

    <th><span><input class="search_field" meth="coin" searchclass="s_prov_percent" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <th><span><input class="search_field" meth="coin" searchclass="s_prov_summa" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <th><span><input class="search_field" meth="coin" searchclass="s_prov_pvn" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <th><span><input class="search_field" meth="coin" searchclass="s_prov_kopsumma" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>

  </tr>

</table>

<table style="display:none;">

  <tr id="avansa_summas_filter">

    <th style="text-align: center;"><a onclick="selectAllInCol('avansa_summas', 'radio_n'); return false;" style="font-size: 1em; " href="#">Visi</a></th>
    <th style="text-align: center;"><a onclick="selectAllInCol('avansa_summas', 'uz_rokas'); return false;" style="font-size: 1em; " href="#">Visi</a></th>
    <th style="text-align: center;"><a onclick="selectAllInCol('avansa_summas', 'ieklaut_rekina'); return false;" style="font-size: 1em; " href="#">Visi</a></th>

    <th><span><input class="search_field" meth="standart" searchclass="s_klienta_nosaukums" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <th><span><input class="search_field" meth="standart" searchclass="s_rek_nr" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>

    <th>
      <span><input class="search_field kalendari" meth="from" searchclass="s_izr_datums" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span><br>
      <span><input class="search_field kalendari" meth="to" searchclass="s_izr_datums" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span>
    </th>

    <th><span><input class="search_field" meth="coin" searchclass="s_rekina_kopsumma" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <th><span><input class="search_field" meth="coin" searchclass="s_neapm_summa" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <th><span><input class="search_field" meth="coin" searchclass="s_prov_percent" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <th><span><input class="search_field" meth="coin" searchclass="s_prov_summa" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <th><span><input class="search_field" meth="coin" searchclass="s_prov_pvn" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <th><span><input class="search_field" meth="coin" searchclass="s_prov_kopsumma" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>

  </tr>


</table>

<form action="" method="post">

  <h2 style="margin-bottom: 0;">Saņemtās samaksas no klientiem</h2>

  <table id="maksajumi" class="data" style="width: 99%;">

    <thead>

      <tr class="last">
        <th align="center">Ne-<br />iekļaut</th>
        <th align="center">Uz<br />rokas</th>
        <th align="center">Iekļaut<br />rēķinā</th>
        <th>Klients</th>
        <th>Rēķina nr.</th>
        <th>Rēķina<br />summa&nbsp;Ls</th>
        <th>Saņemtā<br />samaksa&nbsp;Ls</th>
        <th>Apmaksas<br />datums</th>
        <th>Provīzija %</th>
        <th>Provīzijas<br />summa&nbsp;Ls</th>
        <th>PVN<br />(provīzija)&nbsp;Ls</th>
        <th>Summa<br />kopā&nbsp;Ls</th>
      </tr>

    </thead>

    <tbody class="main">

      <? $inid = -1; ?>

      <? foreach($apmaksas as $apmaksa) { ?>

        <? $inid ++;  ?>

        <?
        $apm_summa_bez_pvn = $apmaksa['summa'] / (1 + ($apmaksa['liguma_pvn'] / 100));
        $prov_summa = $apm_summa_bez_pvn * ($apmaksa['provizija_pardevejam'] / 100);
        ?>

        <tr id="maks_tr<?=$inid;?>" class="tr_maks">
          <td align="center" class="radio radio_n"><input checked="checked" type="radio" name="apm[<?= $apmaksa['id'] ?>][uz_rokas]" value="n" /></td>
          <td align="center" class="radio uz_rokas"><input type="radio" name="apm[<?= $apmaksa['id'] ?>][uz_rokas]" value="1" /></td>
          <td align="center" class="radio ieklaut_rekina"><input type="radio" name="apm[<?= $apmaksa['id'] ?>][uz_rokas]" value="0" /></td>
          <td><?= $apmaksa['klienta_nosaukums'] ?></td>
          <td><?= $apmaksa['rek_nr'] ?></td>
          <td><?= $apmaksa['rekina_kopsumma'] ?></td>
          <td><?= $apmaksa['summa'] ?></td>
          <td><?= date("d.m.Y", strtotime($apmaksa['datums'])) ?></td>
          <td><?= round($apmaksa['provizija_pardevejam'], 2) ?>%</td>
          <td class="prov_summa"><?= format_currency($prov_summa) ?></td>
          <td class="prov_pvn"></td>
          <td class="last prov_summa_ar_pvn"></td>
          <td style="display: none;">
            <input type="hidden" class="row_id" value="<?=$inid;?>" />
            <script>

              searchlist_maks[<?=$inid;?>] = '<?=addslashes(mb_strtoupper($apmaksa['rek_nr']));?>';

              sp_maks['s_klienta_nosaukums'][<?=$inid;?>] = '<?=addslashes(mb_strtoupper($apmaksa['klienta_nosaukums']));?>';
              sp_maks['s_rek_nr'][<?=$inid;?>] = '<?=addslashes(mb_strtoupper($apmaksa['rek_nr']));?>';
              sp_maks['s_rekina_kopsumma'][<?=$inid;?>] = '<?=!empty($apmaksa['rekina_kopsumma']) ? $apmaksa['rekina_kopsumma'] : "" ?>';
              sp_maks['s_summa'][<?=$inid;?>] = '<?=!empty($apmaksa['summa']) ? $apmaksa['summa'] : "" ?>';
              sp_maks['s_datums'][<?=$inid;?>] = <?= !empty($apmaksa['datums']) ? date('Ymd', strtotime($apmaksa['datums'])) : 0 ?>;
              sp_maks['s_prov_percent'][<?=$inid;?>] = '<?=!empty($apmaksa['provizija_pardevejam']) ? $apmaksa['provizija_pardevejam'] : "" ?>';
              sp_maks['s_prov_summa'][<?=$inid;?>] = '<?=!empty($prov_summa) ? $prov_summa : "" ?>';
              sp_maks['s_prov_pvn'][<?=$inid;?>] = 0;
              sp_maks['s_prov_kopsumma'][<?=$inid;?>] = 0;

            </script>
          </td>
        </tr>

      <? } ?>

    </tbody>

    <tr>

      <td align="right" colspan="5"><strong>Kopā:</strong></td>
      <td id="maks_rekinu_summa">0</td>
      <td id="maks_sanemta_summa">0</td>
      <td></td>
      <td></td>
      <td id="maks_prov_summa">0</td>
      <td id="maks_prov_pvn">0</td>
      <td id="maks_prov_kopsumma">0</td>

    </tr>

  </table>

  <h2 style="margin-bottom: 0;">Avansa rēķini</h2>

  <table id="avansa_summas" class="data" style="width: 99%;">

    <thead>

      <tr class="last">
        <th align="center">Ne-<br />iekļaut</th>
        <th align="center">Uz<br />rokas</th>
        <th align="center">Iekļaut<br />rēķinā</th>
        <th>Klients</th>
        <th>Rēķina&nbsp;nr.</th>
        <th>Izrakstīšanas<br />datums</th>
        <th>Rēķina<br />summa&nbsp;Ls</th>
        <th>Neapmaksātā<br />summa&nbsp;Ls</th>
        <th>Provīzija %</th>
        <th>Provīzijas<br />summa&nbsp;Ls</th>
        <th>PVN<br />(provīzija)&nbsp;Ls</th>
        <th>Summa<br />kopā&nbsp;Ls</th>
      </tr>

    </thead>

    <tbody class="main">

      <? $inid = -1; ?>

      <? foreach($avansa_rekini as $avansa) { ?>

        <? $inid ++;  ?>

        <?
        $neapm_summa_bez_pvn = $avansa['neapm_summa'] / (1 + ($avansa['liguma_pvn'] / 100));
        $prov_summa = $neapm_summa_bez_pvn * ($avansa['provizija_pardevejam'] / 100);
        ?>

        <tr id="avansa_tr<?=$inid;?>" class="tr_avansa">
          <td align="center" class="radio radio_n"><input checked="checked" type="radio" name="avansa[<?= $avansa['id'] ?>][uz_rokas]" value="n" /></td>
          <td align="center" class="radio uz_rokas"><input type="radio" name="avansa[<?= $avansa['id'] ?>][uz_rokas]" value="1" /></td>
          <td align="center" class="radio ieklaut_rekina"><input type="radio" name="avansa[<?= $avansa['id'] ?>][uz_rokas]" value="0" /></td>
          <td><?= $avansa['klienta_nosaukums'] ?></td>
          <td><?= $avansa['rek_nr'] ?></td>
          <td><?= date("d.m.Y", strtotime($avansa['izr_datums'])) ?></td>
          <td><?= $avansa['kopsumma'] ?></td>
          <td><?= $avansa['neapm_summa'] ?></td>
          <td><?= round($avansa['provizija_pardevejam'], 2) ?>%</td>
          <td class="prov_summa"><?= format_currency($prov_summa) ?></td>
          <td class="prov_pvn"></td>
          <td class="last prov_summa_ar_pvn"></td>
          <td style="display: none;">
            <input type="hidden" class="row_id" value="<?=$inid;?>" />
            <script>

              searchlist_avansa[<?=$inid;?>] = '<?=addslashes(mb_strtoupper($avansa['rek_nr']));?>';

              sp_avansa['s_klienta_nosaukums'][<?=$inid;?>] = '<?=addslashes(mb_strtoupper($avansa['klienta_nosaukums']));?>';
              sp_avansa['s_rek_nr'][<?=$inid;?>] = '<?=addslashes(mb_strtoupper($avansa['rek_nr']));?>';
              sp_avansa['s_izr_datums'][<?=$inid;?>] = <?= !empty($avansa['izr_datums']) ? date('Ymd', strtotime($avansa['izr_datums'])) : 0 ?>;
              sp_avansa['s_rekina_kopsumma'][<?=$inid;?>] = '<?=!empty($avansa['kopsumma']) ? $avansa['kopsumma'] : "" ?>';
              sp_avansa['s_neapm_summa'][<?=$inid;?>] = '<?=!empty($avansa['neapm_summa']) ? $avansa['neapm_summa'] : "" ?>';
              sp_avansa['s_prov_percent'][<?=$inid;?>] = '<?=!empty($avansa['provizija_pardevejam']) ? $avansa['provizija_pardevejam'] : "" ?>';
              sp_avansa['s_prov_summa'][<?=$inid;?>] = '<?=!empty($prov_summa) ? $prov_summa : "" ?>';
              sp_avansa['s_prov_pvn'][<?=$inid;?>] = 0;
              sp_avansa['s_prov_kopsumma'][<?=$inid;?>] = 0;

            </script>
          </td>
        </tr>

      <? } ?>

    </tbody>

    <tr>

      <td align="right" colspan="6"><strong>Kopā:</strong></td>
      <td id="avansa_rekinu_summa">0</td>
      <td id="avansa_neapm_summa">0</td>
      <td></td>
      <td id="avansa_prov_summa">0</td>
      <td id="avansa_prov_pvn">0</td>
      <td id="avansa_prov_kopsumma">0</td>

    </tr>

  </table>

  <p>PVN likme rēķinam: <input id="pvn_likme" type="text" name="pvn_likme" value="<?= isset($_POST['pvn_likme']) ? $_POST['pvn_likme'] : $darbinieks['pvn_likme'] ?>" style="width: 80px;" /></p>

  <p>
    Kopējā summa uz rokas: <strong id="kopsumma_uz_rokas"></strong><br />
    Kopējā summa rēķinā: <strong id="kopsumma_rek"></strong><br />
    Kopējā summa: <strong id="kopsumma"></strong>
  </p>

  <p>
    <div style="margin-bottom: 5px;">Rēķ. numurs: <input id="rek_nr" type="text" name="rek_nr" value="<?= isset($_POST['rek_nr']) ? $_POST['rek_nr'] : get_darb_rek_next_no('D') ?>" style="width: 80px;" /> <span class="info">Šis numurs netiks izmantots rēķinam, kas tiek izmaksāts uz rokas.</span></div>
    <div>Izrakst. datums: <input id="izr_datums" type="text" name="izr_datums" class="showcalendar" value="<?= isset($_POST['izr_datums']) ? $_POST['izr_datums'] : date('d.m.Y') ?>" style="width: 80px;" /></div>
  </p>

  <button class="ui-state-default ui-corner-all" style="">Ģenerēt rēķinu</button>

</form>

<script type="text/javascript">

$('table td.radio').click(function() {

  $(this).find('input[type=radio]').attr('checked', true).trigger('click');

});

$(window).load(function() {

  var table = $("#maksajumi");
  var head = $("#maksajumi_filter");

  table.tablesorter({
    debug: false,
    highlightClass: 'highlight',
    headers: {
      0: {sorter: false},
      1: {sorter: false},
      2: {sorter: false},
      3: {sorter: 'latviantext'},
      4: {sorter: 'latviantext'},
      7: {sorter: 'latviandate'}
    }
  }).bind('sortStart', function() {
    table.trigger("update");
  }).bind('sortEnd', function() {
    stripeTable(table);
  });

  initTableFilter(table, head, processTables, 'maks', searchlist_maks, sp_maks);

});

$(window).load(function() {

  var table = $("#avansa_summas");
  var head = $("#avansa_summas_filter");

  table.tablesorter({
    debug: false,
    highlightClass: 'highlight',
    headers: {
      0: {sorter: false},
      1: {sorter: false},
      2: {sorter: false},
      3: {sorter: 'latviantext'},
      4: {sorter: 'latviantext'}
    }
  }).bind('sortStart', function() {
    table.trigger("update");
  }).bind('sortEnd', function() {
    stripeTable(table);
  });

  initTableFilter(table, head, processTables, 'avansa', searchlist_avansa, sp_avansa);

});

function processTables() {

  $('tr.tr_maks:hidden, tr.tr_avansa:hidden').each(function() {
    $(this).find('td.radio_n input[type=radio]').attr('checked', true);
  });

  var pvn_likme = parseFloat(ctop($('#pvn_likme').val()));

  var kopsumma_uz_rokas = 0;
  var kopsumma_rek = 0;

  $('tr.tr_maks:visible, tr.tr_avansa:visible').each(function() {

    var prov_summa = parseFloat(ctop($(this).find('.prov_summa').html()));

    var prov_pvn = prov_summa * (pvn_likme / 100);
    var prov_summa_ar_pvn = prov_summa + prov_pvn;

    var r = $(this).find('td.radio input[type=radio]:checked').val();

    if (r == '1') {

      prov_pvn = 0;
      prov_summa_ar_pvn = prov_summa;

      kopsumma_uz_rokas += prov_summa;

    } else if (r == '0') {

      kopsumma_rek += prov_summa_ar_pvn;

    }

    $(this).find('.prov_pvn').html(prov_pvn.toFixed(2));
    $(this).find('.prov_summa_ar_pvn').html(prov_summa_ar_pvn.toFixed(2));

    var id = $(this).find('.row_id').val();

    if ($(this).is('.tr_maks')) {
      sp_maks['s_prov_pvn'][id] = prov_pvn;
      sp_maks['s_prov_kopsumma'][id] = prov_summa_ar_pvn;
    } else {
      sp_avansa['s_prov_pvn'][id] = prov_pvn;
      sp_avansa['s_prov_kopsumma'][id] = prov_summa_ar_pvn;
    }

  });

  $('#kopsumma_uz_rokas').html(kopsumma_uz_rokas.toFixed(2));
  $('#kopsumma_rek').html(kopsumma_rek.toFixed(2));
  $('#kopsumma').html((kopsumma_uz_rokas + kopsumma_rek).toFixed(2));

  var maks_rekinu_summa = 0;
  var maks_sanemta_summa = 0;
  var maks_prov_summa = 0;
  var maks_prov_pvn = 0;
  var maks_prov_kopsumma = 0;

  jQuery.each(searchlist_maks, function(k,v){

    var row = $('#maks_tr' + k);

    if (row.is(':visible')) {

      if (sp_maks['s_rekina_kopsumma'][k] > 0) maks_rekinu_summa += parseFloat(sp_maks['s_rekina_kopsumma'][k]);
      if (sp_maks['s_summa'][k] > 0) maks_sanemta_summa += parseFloat(sp_maks['s_summa'][k]);
      if (sp_maks['s_prov_summa'][k] > 0) maks_prov_summa += parseFloat(sp_maks['s_prov_summa'][k]);
      if (sp_maks['s_prov_pvn'][k] > 0) maks_prov_pvn += parseFloat(sp_maks['s_prov_pvn'][k]);
      if (sp_maks['s_prov_kopsumma'][k] > 0) maks_prov_kopsumma += parseFloat(sp_maks['s_prov_kopsumma'][k]);

    }

  });

  $('#maks_rekinu_summa').html(maks_rekinu_summa.toFixed(2));
  $('#maks_sanemta_summa').html(maks_sanemta_summa.toFixed(2));
  $('#maks_prov_summa').html(maks_prov_summa.toFixed(2));
  $('#maks_prov_pvn').html(maks_prov_pvn.toFixed(2));
  $('#maks_prov_kopsumma').html(maks_prov_kopsumma.toFixed(2));


  var avansa_rekinu_summa = 0;
  var avansa_neapm_summa = 0;
  var avansa_prov_summa = 0;
  var avansa_prov_pvn = 0;
  var avansa_prov_kopsumma = 0;

  jQuery.each(searchlist_avansa, function(k,v){

    var row = $('#avansa_tr' + k);

    if (row.is(':visible')) {

      if (sp_avansa['s_rekina_kopsumma'][k] > 0) avansa_rekinu_summa += parseFloat(sp_avansa['s_rekina_kopsumma'][k]);
      if (sp_avansa['s_neapm_summa'][k] > 0) avansa_neapm_summa += parseFloat(sp_avansa['s_neapm_summa'][k]);
      if (sp_avansa['s_prov_summa'][k] > 0) avansa_prov_summa += parseFloat(sp_avansa['s_prov_summa'][k]);
      if (sp_avansa['s_prov_pvn'][k] > 0) avansa_prov_pvn += parseFloat(sp_avansa['s_prov_pvn'][k]);
      if (sp_avansa['s_prov_kopsumma'][k] > 0) avansa_prov_kopsumma += parseFloat(sp_avansa['s_prov_kopsumma'][k]);

    }

  });

  $('#avansa_rekinu_summa').html(avansa_rekinu_summa.toFixed(2));
  $('#avansa_neapm_summa').html(avansa_neapm_summa.toFixed(2));
  $('#avansa_prov_summa').html(avansa_prov_summa.toFixed(2));
  $('#avansa_prov_pvn').html(avansa_prov_pvn.toFixed(2));
  $('#avansa_prov_kopsumma').html(avansa_prov_kopsumma.toFixed(2));

}

processTables();

$('#pvn_likme').keyup(processTables);

$('td.radio input[type=radio]').click(function(e) {

  e.stopPropagation();

  processTables();

});

$(".showcalendar").datepicker({dateFormat:'dd.mm.yy'});

function selectAllInCol(table_id, class_name) {

  $('#' + table_id + ' td.' + class_name + ' input[type=radio]').attr('checked', true);

  processTables();

}

</script>