<?
if (!is_admin()) {
  die();
}

if (empty($_POST['pardeveja_rekina_id'])) {
  die();
}

$pardeveja_rekina_id = (int) $_POST['pardeveja_rekina_id'];

$rek_data = db_get_assoc(db_query("
  SELECT *
  FROM `".DB_PREF."pardeveji_rekini`
  WHERE id = ".$pardeveja_rekina_id."
"));

if (empty($rek_data)) {
  die();
}

$result = db_query("
  DELETE FROM `".DB_PREF."pardeveji_rekini`
  WHERE `id` = ".$pardeveja_rekina_id."
  LIMIT 1
");

if ($result) {

  db_query("
    DELETE FROM `".DB_PREF."pardeveji_rekini_detalas`
    WHERE `pardeveja_rekina_id` = ".$pardeveja_rekina_id."
  ");

  if (!empty($rek_data['fails_ep'])) {

    db_query("DELETE FROM `".DB_PREF."faili` WHERE `id` = ".(int)$rek_data['fails_ep']);

    if (is_file("faili/".(int)$rek_data['fails_ep'])) {
      unlink("faili/".(int)$rek_data['fails_ep']);
    }

  }

}

header("Location: ?c=darbinieki&a=labot&id=" . $rek_data['pardeveja_id'] . "&subtab=1");
die();

?>