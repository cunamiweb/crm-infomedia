<?php
if (!is_admin() && (empty($_GET['id']) || $_GET['id'] != $_SESSION['user']['id'])) {
  die();
}

$subtab = 0;

if (empty($_GET['id'])) {
  $subtab = 2;
}

$subtab = !empty($_GET['subtab']) ? (int)$_GET['subtab'] : $subtab;

require('new_process.php');

if(!empty($_GET['id'])){

  $query = db_query("SELECT * FROM `".DB_PREF."pardeveji` WHERE `id` = ".(int)$_GET['id']);
  $d = db_get_assoc($query);

  $query = db_query("SELECT * FROM `".DB_PREF."pardeveji_epasti` WHERE `pardeveja_id` = ".(int)$_GET['id'] . " ORDER BY id ASC");
  $epasti = array();

  while($row = db_get_assoc($query)){
    $epasti[] = $row;
  }

  $query = db_query("SELECT * FROM `".DB_PREF."pardeveji_telefoni` WHERE `pardeveja_id` = ".(int)$_GET['id'] . " ORDER BY id ASC");
  $telefoni = array();

  while($row = db_get_assoc($query)){
    $telefoni[] = $row;
  }

  $query = db_query("
    SELECT
      a.*,
      g.nosaukums as klienta_nosaukums
    FROM `".DB_PREF."atgadinajumi` a
    LEFT JOIN `".DB_PREF."ligumi` g ON (a.liguma_id = g.id)
    WHERE
      a.pardeveja_id = ".(int)$_GET['id']." OR
      g.pardeveja_id = ".(int)$_GET['id']."
  ");
  $atgadinajumi = array();

  while($row = db_get_assoc($query)){
    $atgadinajumi[$row['id']] = $row;
  }

  $query = db_query("
    SELECT
      g.*,
      b.beigu_statuss,
      b.atlikt_lidz as beigu_statuss_atlikt_lidz,
      SUM(r.summa) as rekinu_summa,
      SUM(r.kopsumma) as rekinu_kopsumma,
      SUM(r.apm_summa) as apm_kopsumma
    FROM `".DB_PREF."ligumi` g
    LEFT JOIN `".DB_PREF."ligumi_beigusies` b ON (g.id = b.liguma_id)
    LEFT JOIN `".DB_PREF."rekini` r ON (
      r.liguma_id = g.id AND
      r.atkapsanas_rekins = 0
    )
    WHERE g.`pardeveja_id` = ".(int)$_GET['id'] . "
    GROUP BY g.id
    ORDER BY g.ligumadat DESC
  ");
  $ligumi = array();

  while($row = db_get_assoc($query)){
    $ligumi[] = $row;
  }


  $query = db_query("
    SELECT r.*
    FROM `".DB_PREF."rekini` r
    LEFT JOIN `".DB_PREF."ligumi` g ON (g.id = r.liguma_id)
    WHERE
      g.`pardeveja_id` = ".(int)$_GET['id']."
  ");
  $lig_rekini = array();

  while($row = db_get_assoc($query)){
    $lig_rekini[$row['liguma_id']][] = $row;
  }


  $query = db_query("
    SELECT r.*
    FROM `".DB_PREF."pardeveji_rekini` r
    WHERE r.`pardeveja_id` = ".(int)$_GET['id'] . "
    ORDER BY r.izr_datums DESC, r.id DESC
  ");
  $pard_rekini = array();

  while($row = db_get_assoc($query)){
    $pard_rekini[] = $row;
  }


  $query = db_query("
    SELECT
      d.*,
      r.liguma_id,
      p.statuss as pard_rekina_statuss
    FROM `".DB_PREF."pardeveji_rekini_detalas` d
    LEFT JOIN `".DB_PREF."pardeveji_rekini` p ON (p.id = d.pardeveja_rekina_id)
    LEFT JOIN `".DB_PREF."rekini` r ON (r.id = d.rekina_id)
    WHERE p.`pardeveja_id` = ".(int)$_GET['id']."
  ");
  $pard_rekini_det = array();

  while($row = db_get_assoc($query)){
    $pard_rekini_det[$row['liguma_id']][] = $row;
  }

} else {

  $d = array();
  $epasti = array();
  $telefoni = array();
  $atgadinajumi = array();
  $ligumi = array();
  $lig_rekini = array();
  $pard_rekini = array();
  $pard_rekini_det = array();

}

?>
<iframe src="" style="display:none;" id="downloadframe" name="downloadframe"></iframe>

<? show_autodownload_iframes() ?>

<? // Prototypes // ?>
<div style="display:none;">

  <table id="epastsprot">>
    <tbody>
      <tr><td>Epasts:</td><td><input style="width: 95%;" type="input" name="epasti_new[]" value=""> <a style="float: right; display: block;" class="hidethistoo" onclick="$(this).parent().parent().remove();" href="javascript:;">[x]</a></td></tr>
    </tbody>
  </table>

  <table id="telefonsprot">
    <tbody>
      <tr><td>Telefons:</td><td><input style="width: 95%;" type="input" name="telefoni_new[]" value=""> <a style="float: right; display: block;" class="hidethistoo" onclick="$(this).parent().parent().remove();" href="javascript:;">[x]</a></td></tr>
    </tbody>
  </table>

  <table id="atgadinajumsprot">

    <tbody>

      <tr>
        <td><input type="input" class="showcalendarnew calendarcalc atgadinajumi_termimi" value="<?=date('d.m.Y');?>" name="atgadinajumi_new[datums][]"></td>
        <td class="status">Jauns</td>
        <td>
          <select name="atgadinajumi_new[tema][]">
            <? foreach($_vars['atgadinajumu_temas'] as $temas_id => $temas_nosauk) { ?>
              <option value="<?= $temas_id ?>"><?= $temas_nosauk ?></option>
            <? } ?>
          </select>
        </td>
        <td><textarea name="atgadinajumi_new[saturs][]" rows="2" cols="30"></textarea></td>
        <td>
          <select name="atgadinajumi_new[ligums][]">
            <option value="">Nepiesaistīts līgumam</option>
            <? foreach($ligumi as $v) { ?>
              <option value="<?= $v['id'] ?>"><?= $v['nosaukums'] ?></option>
            <? } ?>
          </select>
        </td>
        <td><?=date('d.m.Y');?></td>
        <td><?=$_vars['sys_pardeveji'][$_SESSION['user']['id']];?></td>
        <td></td>
        <td></td>
      </tr>

    </tbody>

  </table>

  <? $form_names = array('darb_rekviziti', 'darb_atgadinajumi', 'darb_ligumi', 'darb_norekini') ?>

  <? foreach($form_names as $form_name) { ?>

    <?
    $fields_query = db_query("SELECT * FROM `".DB_PREF."formu_lauki` WHERE forma = '".esc($form_name)."' ORDER BY id ASC");
    $fields = array();

    while($row = db_get_assoc($fields_query)) {
      $fields[] = $row;
    }
    ?>

    <div id="komentarsprot_<?= $form_name ?>">
      <table class="comment_form">
        <tr>
          <td width="70">Komentārs:</td>
          <td>
            <textarea name="komentari_new[<?= $form_name ?>][komentars][]" rows="2" cols="30"></textarea>
          </td>
        </tr>
        <? if (!empty($fields)) { ?>
        <tr>
          <td>Lauks:</td>
          <td>
            <select name="komentari_new[<?= $form_name ?>][formas_lauka_id][]">
              <option value="">Nepiesaistīts laukam</option>
              <? foreach($fields as $row) { ?>
                <option value="<?= $row['id'] ?>"><?= $row['nosaukums'] ?></option>
              <? } ?>
            </select>
          </td>
        </tr>
        <? } ?>
      </table>
    </div>

  <? } ?>

</div>

<? // Header // ?>
<a href="javascript:;" class="tabulation" active="0" open="form_ligumi">Līgumi</a> |
<a href="javascript:;" class="tabulation" active="1" open="form_norekini">Norēķini</a> |
<a href="javascript:;" class="tabulation" active="2" open="form_rekviziti">Rekvizīti</a> |
<a href="javascript:;" class="tabulation" active="3" open="form_atgadinajumi">Atgādinājumi</a>

<div style="padding:10px;">
  <center><button id="labotdzestbutton" onClick="return editoradd(this, <?= is_admin() ? 1 : 0 ?>)">Labot/papildināt</button></center>
</div>

<h1 style="font-size:20px;" id="darbinieka_nosaukums"><?=!empty($d['vards'])?$d['vards']:""?></h1>

<form action="?c=darbinieki&a=labot&id=<?= !empty($_GET['id']) ? '&a=labot&id=' . (int) $_GET['id'] : '&a=jauns' ;?>" id="fullformplace" method="post" enctype="multipart/form-data">

  <div style="display:<?=($subtab==0?"block":"none");?>;" id="form_ligumi">
    <? include("forms/ligumi.php"); ?>
  </div>

  <div style="display:<?=($subtab==1?"block":"none");?>;" id="form_norekini">
    <? include("forms/norekini.php"); ?>
  </div>

  <div style="display:<?=($subtab==2?"block":"none");?>;" id="form_rekviziti">
    <? include("forms/rekviziti.php"); ?>
  </div>

  <div style="display:<?=($subtab==3?"block":"none");?>;" id="form_atgadinajumi">
    <? include("forms/atgadinajumi.php"); ?>
  </div>


  <table width="600" class="hidethistoo non_admin_edit">
    <tr><td colspan="2" align="center"><br><input class="ui-state-default ui-corner-all non_admin_edit submit" type="submit" onClick="return saveAll(this, 'darbinieks')" value="Saglabāt!"></td></tr>
    <tr><td colspan="2" align="center"><br><input style="width:200px;" type="button" onclick="return cancelSaves();" class="ui-state-default ui-corner-all non_admin_edit" value="Atcelt"></td></tr>
  </table>

</form>

<script>

var activeone = <?= $subtab ?>;

$(function() {

  $('.tabulation').click(function(){

    $('#form_rekviziti').hide();
    $('#form_ligumi').hide();
    $('#form_norekini').hide();
    $('#form_atgadinajumi').hide();
    $('#'+$(this).attr('open')).show();

    activeone = $(this).attr('active');

  });

  $('#vardsplace').keyup(function(){
    $('#darbinieka_nosaukums').html($(this).val());
  });

});

function activateTab(tab_name) {

  $('#form_rekviziti').hide();
  $('#form_ligumi').hide();
  $('#form_norekini').hide();
  $('#form_atgadinajumi').hide();

  $('#form_' + tab_name).show();

  activeone = $('a[open=form_' + tab_name + ']').attr('active');

}

$("#agenta_lig_datums").datepicker({dateFormat:'dd.mm.yy'});

editordisable();

<? if(empty($_GET['id'])) { ?>
  editoradd($('#labotdzestbutton'));
<? } ?>

</script>