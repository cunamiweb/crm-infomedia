$.datepicker.setDefaults({
  firstDay: 1,
  changeMonth: true,
  changeYear: true,
  dayNamesMin: ['Sv', 'Pr', 'Ot', 'Tr', 'Ce', 'Pk', 'Se'],
  monthNamesShort: ['Jan', 'Feb', 'Mar', 'Apr', 'Mai', 'Jūn', 'Jūl', 'Aug', 'Sep', 'Okt', 'Nov', 'Dec']
});

$.tablesorter.addParser({
  id: 'latviantext',
  type: 'text',
  is: function(s) {
    return false;
  },
  format: function (s) {
    var translate = {
      "ē" : "e",
      "ŗ" : "r",
      "ū" : "u",
      "ī" : "i",
      "ō" : "o",
      "ā" : "a",
      "š" : "s",
      "ķ" : "k",
      "ļ" : "l",
      "ž" : "z",
      "č" : "c",
      "ņ" : "n"
    };
    var translate_re = /[ēŗūīōāšķļžčņ]/g;
    return ( s.toLowerCase().replace(translate_re, function(match) {
      return translate[match.toLowerCase()];
    }));
  }
});

$.tablesorter.addParser({
  id: 'latviandate',
  type: 'numeric',
  is: function(s) {
    return false;
  },
  format: function (s) {

    var parts = s.split('.');

    if (parts.length == 3) {
      return (parts[2] * 10000) + (parts[1] * 100) + parts[0];
    } else {
      return 0;
    }

  }
});

$.tablesorter.addParser({
  id: 'multiplelatviandates',
  type: 'numeric',
  is: function(s) {
    return false;
  },
  format: function (s) {

    var parts = s.split('.');

    if (parts.length == 3) {
      return (parts[2] * 10000) + (parts[1] * 100) + parts[0];
    } else {
      return 0;
    }

  }
});


function addmorefiles(e, filetype){

  $(e).after('<div><input type="file" name="'+filetype+'[]"><button onclick="$(this).parent().remove(); return false;">Dzēst</button></div>');

  return false;

}

function deleteFile(e,who){

  if(confirm('Vai tiešām Jūs vēlaties dzēst šo failu?')){

    $(e).parent().slideUp('slow');
    $.get("./", { deletefile: who } );

  }

}

function editoradd(e, user_is_admin) {

  if (typeof(user_is_admin) == 'undefined') user_is_admin = true;

  var filter1 = '';
  var filter2 = '';
  var filter3 = '';


  if (user_is_admin) {
    filter1 = '#fullformplace input:not(.search_field), #fullformplace select:not(.search_field), #fullformplace textarea:not(.search_field)';
    filter2 = '#fullformplace button';
    filter3 = '.hidethistoo';
    } else {
    filter1 = '#fullformplace input.non_admin_edit:not(.search_field), #fullformplace select.non_admin_edit:not(.search_field), #fullformplace textarea.non_admin_edit:not(.search_field)'
    filter2 = '#fullformplace button.non_admin_edit';
    filter3 = '.hidethistoo.non_admin_edit';
  }

  $(filter1)
      .css('border','1px solid gray')
      .css('background-color','white')
      .css('color','black')
      .attr('disabled','');

  $(filter2).attr('disabled','');

  $(filter3).css('display','');

  $('.onlyinview').hide();

  $(e).parent().html('');

  return false;

}

function editordisable() {

  $('#fullformplace input:not(.search_field), #fullformplace select:not(.search_field), #fullformplace textarea:not(.search_field)')
      .css('border','none')
      .css('background-color','white')
      .css('color','black')
      .attr('disabled','true');

  $('#fullformplace button:not(.onlyinview)').attr('disabled','true');

  $('.hidethistoo').css('display','none');

}

function saveAll(save_button, validate_action) {

  $('#fullformplace .input_has_error').removeClass('input_has_error');

  var loading_html = '<div class="loading"><img alt="Notiek ielāde..." src="css/loading.gif" /></div>';

  if ($('#loader_placeholder').length == 1) {
    $('#loader_placeholder').html(loading_html);
  } else {
    $(save_button).before(loading_html);
  }

  var loading = $('div.loading');

  var had_errors = true;
  var error_msgs = '';

  var tab_activated = false;

  $.ajax({
    url: 'validate.php?action=' + validate_action,
    async: false,
    cache: false,
    data: $('#fullformplace').serialize(),
    dataType: 'json',
    type: 'POST',
    success: function(data) {

      if (data['success']) {
        had_errors = false;
      } else {

        had_errors = true;

        $.each(data['errors'], function(tab_name, errors) {

          if (!tab_activated && typeof activateTab != "undefined") {
            activateTab(tab_name);
            tab_activated = true;
          }

          $.each(errors, function(field_name, error_msg) {
            $('#' + field_name).addClass('input_has_error');
            if (error_msg != '') {
              error_msgs += "- " + error_msg + "\n";
            }
          });

        });

      }

    }
  });

  loading.remove();

  if (had_errors) {

    window.location.hash = '#page_top';

    if (error_msgs != '')
      alert(error_msgs);

    return false;

  } else {

    var url = $('#fullformplace').attr('action');

    if (typeof activeone != 'undefined') {
      url = url + '&subtab=' + activeone;
    }

    $('#fullformplace').attr('action',  url);

    return true;

  }

}

function cancelSaves() {

  var url = $('#fullformplace').attr('action');

  if (typeof activeone != 'undefined') {
    url = url + '&subtab=' + activeone;
  }

  document.location = url;

  return false;

}

function initTableFilter(table, head, call_after, prefix, searchlist, sp) {

  table.call_after_function = call_after;
  table.prefix = (prefix != '') ? prefix + '_' : '';
  table.searchlist = searchlist;
  table.sp = sp;

  showAll(table);

  $('thead', table).append('<tr class="last">'+head.html()+'</tr>');

  head.parents('table').remove();

  $('input.search_field, select.search_field', table).keyup(function() {
    advSearch(table);
  });

  $('input.kalendari', table).datepicker({dateFormat:'dd.mm.yy'}).change(function() {
    advSearch(table);
  });

  $('select.advonchange', table).change(function() {
    advSearch(table);
  });

  var clear_field = function() {
    $(this).parent().find('input').val('');
    advSearch(table);
    return false;
  }

  $('a.search_field', table).click(clear_field)

  fillSearchParamsFromCache(table);

}

function showAll(table) {

  table.showall = true;

  $("tbody.main tr", table).show();

  stripeTable(table);

  table.call_after_function();

}

function advSearch(table) {

  var exclude = ',';
  var hasParams = false;

  jQuery.each($('.search_field', table),function(k,v){

    var el = $(v);
    var el_to = null;
    var search_val = el.val();
    var meth = el.attr('meth');

    if (meth == 'to') {
      return; // 'to' field we will access together with 'from'
    }

    if (meth == 'from') {
      el_to = el.parent().parent().find('input[meth=to]');
    }

    if((el.val() > 0 || el.val().length > 1) || (el_to != null && el_to.val().length > 1)){

      hasParams = true;
      table.showall = false;

      if(meth == "int"){
        what = search_val;
      }else if(meth == "standart"){
        what = search_val.toUpperCase();
      }else if(meth == "nouupper"){
        what = search_val;
      }else if(meth == "from"){

        var from_date = null;
        var to_date = null;

        if (search_val.length > 1) {
          from_date = search_val.split('.');
          from_date = parseInt(from_date[2], 10)*10000+parseInt(from_date[1], 10)*100+parseInt(from_date[0], 10);
        }

        if (el_to.val().length > 1) {
          to_date = el_to.val().split('.');
          to_date = parseInt(to_date[2], 10)*10000+parseInt(to_date[1], 10)*100+parseInt(to_date[0], 10);
        }

      }else if(meth == "coin"){
        what = search_val.replace(',','.');
      }

      jQuery.each(table.sp[el.attr('searchclass')],function(c,values){

        if (typeof(values) == 'object') {
          values = values;
        } else {
          values = [values];
        }

        var found = false;

        $.each(values, function(i, d) {

          if($.trim(d.toString()) != '') {

            if (meth == "int") {

              if (d == what) {
                found = true;
              }

            }else if(meth == "standart" || meth == "nouupper"){

              d = d.toString();

              if(d.indexOf(what) >= 0){
                found = true;
              }

            }else if(meth == "from"){

              if (from_date != null && to_date != null) {

                if(d >= from_date && d <= to_date) {
                  found = true;
                }

              } else if (from_date != null) {

                if(d >= from_date) {
                  found = true;
                }

              } else if (to_date != null) {

                if(d <= to_date) {
                  found = true;
                }

              }


            }else if(meth == "to"){

              if(d <= tmpdate) {
                found = true;
              }

            }else if(meth == "coin"){

              if(parseInt(d, 10) == parseInt(what, 10)) {
                found = true;
              }

            }

          }

        });

        if (!found) {

          if(exclude.indexOf(',' + c + ',') < 0) {
            exclude += c + ',';
          }

        }

      });

    }

  });

  if(hasParams){

    table.showall = false;

    jQuery.each(table.searchlist, function(k,v){

      if(v != null && exclude.indexOf(',' + k + ',') < 0){
        $('#' + table.prefix + 'tr' + k).show();
      } else {
        $('#' + table.prefix + 'tr' + k).hide();
      }

    });

    stripeTable(table);

    table.call_after_function();

    cacheSearchParams(table);

  } else if(!table.showall) {

    showAll(table);

    clearSearchParamsCache(table);

  }


}

function stripeTable(table){

  table.each(function() {

    $("tbody.main tr", this).removeClass("odd");

    var i = 0;

    $("tbody.main tr:visible", this).each(function() {

      if (i % 2 == 0) {
        $(this).addClass("odd");
      }

      i ++;

    });

  });

};

function cacheSearchParams(table) {

  var cache_key = getCacheKeyFromClass(table);

  if (cache_key) {

    var data = {};

    $('input.search_field, select.search_field', table).each(function() {

      var key = $(this).attr('searchclass') + '||' + $(this).attr('meth');

      data[key] = $(this).val();

    });

    $.jStorage.set(cache_key, data);

  }

}

function clearSearchParamsCache(table) {

  var cache_key = getCacheKeyFromClass(table);

  if (cache_key) {
    $.jStorage.deleteKey(cache_key);
  }

}

function fillSearchParamsFromCache(table) {

  var cache_key = getCacheKeyFromClass(table);

  if (cache_key) {

    var data = $.jStorage.get(cache_key, false);

    if (data) {

      var has_search_params = false;

      $.each(data, function(k, v) {

        if (v != '') {

          has_search_params = true;

          var parts = k.split('||');

          table.find('.search_field[searchclass='+parts[0]+'][meth='+parts[1]+']').val(v);

        }

      });

      if (has_search_params) {
        advSearch(table);
      }

    }

  }

}

function changeAtgStatus(atg_id, status) {

  var msg = '';

  if (status == 1) {
    msg = 'Vai atstatīt atgādinājuma statusu uz "Jauns"?';
  } else if (status == 2) {
    msg = 'Vai atzīmēt atgādinājumu kā izpildītu?'
  } else if (status == 3) {
    msg = 'Vai atzīmēt atgādinājumu kā atceltu?'
  }


  if (confirm(msg)) {

    $.get('?updateAtgStatus=' + atg_id + '&status=' + status, function(result) {

      if (result == '1') {

        if (status == 1) {

          $('tr.atg_id_' + atg_id + ' td.status span.link_wrap').html(
            '<a onclick="return changeAtgStatus(' + atg_id + ', 2);" href="#">Izpildīts</a><br />' +
            '<a onclick="return changeAtgStatus(' + atg_id + ', 3);" href="#">Atcelts</a>'
          );

        } else if (status == 2) {

          $('tr.atg_id_' + atg_id + ' td.status span.link_wrap').html(
            'Izpildīts <a onclick="return changeAtgStatus(' + atg_id + ', 1);" href="#">X</a>'
          );

        } else if (status == 3) {

          $('tr.atg_id_' + atg_id + ' td.status span.link_wrap').html(
            'Atcelts <a onclick="return changeAtgStatus(' + atg_id + ', 1);" href="#">X</a>'
          );

        }

        $('tr.atg_id_' + atg_id + ' td.status input').val(status);

        refreshAtgStatuses();

      } else {

        alert('Neizdevās nomainīt atgādinājuma statusu!');

      }

    });

  }

  return false;

}

function refreshAtgStatuses(){

  jQuery.each( $('.atgadinajumi_termimi') , function(){

    var today = new Date();
    tmpdate = $(this).val().split('.');
    var myDate=new Date(tmpdate[2],tmpdate[1]-1,tmpdate[0],0,0,0,0);

    if($(this).val() != ''){

      $(this).parent().parent().removeClass('active no_term done canceled');

      if(today > myDate){

        $(this).parent().parent().addClass('active');

      }else{

        $(this).parent().parent().addClass('no_term');

      }

      if( $(this).parent().parent().find('td.status input').val() == 2 ){

        $(this).parent().parent().removeClass('active no_term done canceled');

        $(this).parent().parent().addClass('done');

      }

      if( $(this).parent().parent().find('td.status input').val() == 3 ){

        $(this).parent().parent().removeClass('active no_term done canceled');

        $(this).parent().parent().addClass('canceled');

      }

    }

  });

}

function addAtgRow() {

  $('#atgadinajumu_tabula tr#atg_new_row').removeAttr('id');

  var tpl = $('#atgadinajumsprot tbody').html();

  $('#atgadinajumu_tabula').append(tpl);

  $('#atgadinajumu_tabula tr:last-child').attr('id', 'atg_new_row')

  $(".showcalendarnew").datepicker({dateFormat:'dd.mm.yy'});
  $('.calendarcalc').change(refreshAtgStatuses);

  refreshAtgStatuses();

  window.location.hash = '#atg_new_row';

  return false;

}

function addmorecomments(e, filetype){

  var tpl = $('#komentarsprot_' + filetype).html();

  $(e).parent().after(tpl);

  return false;

}

function create_filter_field_array(names) {

  var sp = new Array();

  $.each(names, function(i, name) {
    sp[name] = new Array();
  });

  return sp;

}

function deleteComment(e,who){

  if(confirm('Vai tiešām Jūs vēlaties dzēst šo komentāru?')){

    $(e).parents('li').filter(':first').slideUp('slow');
    $.get("./", { deletecomment: who } );

  }

}

function ctop(s) {
  s = s+'';
  return s.replace(",", ".");
}

function isNumber(x) {
  x=parseFloat(ctop(x));
  return ((x > 0 || x <= 0));
}

function changeTableOrder(th, col_name) {

  var form = $(th).parents('form');

  var order_input = form.find('input[name=order]');
  var direction_input = form.find('input[name=direction]');

  var direction = 'asc';

  if (order_input.val() == col_name) {

    if (direction_input.val() == 'asc') {
      direction = 'desc';
    } else {
      direction = 'asc';
    }

  }

  order_input.val(col_name);

  direction_input.val(direction);

  form.submit();

  return false;

}

function initPlainSortableTable(form) {

  form.find('a.sfield_clear').click(function() {

    $(this).parent().find('input').val('');
    form.find('input[name=page]').val(1);

    form.submit();

    return false;

  });

  form.find(".kalendari").datepicker({dateFormat:'dd.mm.yy'});

  form.find("tr.filter :input, div.glob_filter :input").change(function() {
    form.find('input[name=page]').val(1);
  });

  form.find("tr.filter .kalendari, div.glob_filter .kalendari").change(function() {
    form.submit();
  });

  form.find("tr.filter select, div.glob_filte.select").change(function() {
    form.submit();
  });

  var order_input = form.find('input[name=order]');
  var direction_input = form.find('input[name=direction]');

  var th = form.find('th[colname="' + order_input.val() + '"]');

  if (th.size()) {

    if (direction_input.val() == 'asc') {
      th.addClass('headerSortUp');
      th.removeClass('headerSortDown');
    } else {
      th.addClass('headerSortDown');
      th.removeClass('headerSortUp');
    }

  }

  stripeTable(form.find('table'));

}

function days_between(date1, date2) {

  if (typeof(date1) == 'object') {
    var date1_obj = date1;
  } else {

    var date1_obj = new Date();

    var date1_obj_parts = date1.split('.');

    date1_obj.setFullYear(
      parseInt(date1_obj_parts[2], 10),
      parseInt(date1_obj_parts[1], 10) - 1,
      parseInt(date1_obj_parts[0], 10),
      0,0,0
    );

  }

  if (typeof(date2) == 'object') {
    var date2_obj = date2;
  } else {

    var date2_obj = new Date();

    var date2_obj_parts = date2.split('.');

    date2_obj.setFullYear(
      parseInt(date2_obj_parts[2], 10),
      parseInt(date2_obj_parts[1], 10) - 1,
      parseInt(date2_obj_parts[0], 10),
      0,0,0
    );

  }

  var one_day = 1000 * 60 * 60 * 24

  var date1_ms = date1_obj.getTime()
  var date2_ms = date2_obj.getTime()

  return Math.round((date2_ms - date1_ms) / one_day)

}

function init_auto_logout() {

  window.setInterval(function() {

    $.get('?do_auto_logout=1', function(do_auto_logout) {

      do_auto_logout = parseInt(do_auto_logout, 10);

      if (do_auto_logout) {
        window.location.href = '?logout=true&auto_logout=1';
      }

    }, 'text');

  }, 120 * 1000) // 2 minutes

}

function getCacheKeyFromClass(element) {

  var cache_key = false;

  if (element.attr('class').match(/\bckey_/)) {

    var ckey_info = /\bckey_([a-z0-9_]+)/i.exec(element.attr('class'));

    cache_key = ckey_info[1];

  }

  return cache_key;

}

function showLigumaBeiguStatussDialog(liguma_id) {

  $('<iframe src="?c=ligumi&a=beigu_statuss&liguma_id='+liguma_id+'&without_nav=1" />').dialog({
      title: 'Līguma beigu statuss',
      autoOpen: true,
      width: 600,
      height: 403,
      modal: true,
      resizable: false,
      draggable: false,
      closeOnEscape: false,
      close: function() {
        document.location.reload();
      }
  }).width(580);

}