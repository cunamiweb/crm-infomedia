<?php
include("inc/init.php");

// 1 - klienti
// 2 - norēķini
// 3 - darbiniei

$query = db_query("
  SELECT *
  FROM `".DB_PREF."rekini`
  ORDER BY id ASC
");
$rekini = array();

while($row = db_get_assoc($query)) {
  $rekini[$row['liguma_id']][] = $row;
}

$query = db_query("
  SELECT *
  FROM `".DB_PREF."ligumi`
  ORDER BY
    `ligumadat` DESC,
    `ligumanr` DESC
");

$curdate = date('Y-m-d');
$yesterday = date('Y-m-d', strtotime('-1 day'));

while($row = db_get_assoc($query)) {

  $beigu_statuss_data = db_get_assoc(db_query("SELECT * FROM `".DB_PREF."ligumi_beigusies` WHERE `liguma_id` = " . $row['id']));

  // brīdinājums, ka klientam drīz beigsies līgums (2ned pirms beigām)

  if (!empty($row['reklama_lidz']) && $curdate == date('Y-m-d', strtotime('-2 weeks', strtotime($row['reklama_lidz'])))) {

    db_query("
      INSERT INTO `".DB_PREF."atgadinajumi` (
        liguma_id,
        datums,
        tema,
        saturs
      ) VALUES (
        ".$row['id'].",
        '".esc(date('Y-m-d', strtotime($row['reklama_lidz'])))."',
        1,
        'Drīz beigsies līgums'
      )
    ");

  }

  // Atgādinājums pēc 6 mēnešiem piezvanīt klientam, apjautāties kā iet.

  if ($row['reklama_lidz'] >= $curdate && $row['reklama_no'] == date('Y-m-d', strtotime('-6 months'))) {

    db_query("
      INSERT INTO `".DB_PREF."atgadinajumi` (
        liguma_id,
        datums,
        tema,
        saturs
      ) VALUES (
        ".$row['id'].",
        NOW(),
        1,
        'Pagājis pusgads kopš reklāmas sākuma - jāapjautājas, kā iet'
      )
    ");

  }

  // Ja līguma saņemšanas forma nav Oriģināls, tad 2 ned pēc reklāmas sākuma jābūt atgādinājumam, ka jāpieprasa līguma oriģināls.

  if ($row['sanemsanas_forma'] != 1 && $row['reklama_no'] == date('Y-m-d', strtotime('-2 weeks'))) {

    db_query("
      INSERT INTO `".DB_PREF."atgadinajumi` (
        liguma_id,
        datums,
        tema,
        saturs
      ) VALUES (
        ".$row['id'].",
        NOW(),
        1,
        'Nav saņemts līguma oriģināls'
      )
    ");

  }

  // Ja nav reklāmas materiāli, tad 1 dienu pirms reklāmas sākuma atgādinājums zvanīt klientam

  if (($row['infomedia_reklamas_mat'] != 1 || $row['tiessaite_reklamas_mat'] != 1) && $row['reklama_no'] == date('Y-m-d', strtotime('+1 day'))) {

    db_query("
      INSERT INTO `".DB_PREF."atgadinajumi` (
        liguma_id,
        datums,
        tema,
        saturs
      ) VALUES (
        ".$row['id'].",
        NOW(),
        1,
        'Nav visi reklāmas materiāli - jāsazinās ar klientu'
      )
    ");

  }

  // Reklāmas sākuma datums

  if ($row['reklama_no'] == $curdate) {

    db_query("
      INSERT INTO `".DB_PREF."atgadinajumi` (
        liguma_id,
        datums,
        tema,
        saturs
      ) VALUES (
        ".$row['id'].",
        NOW(),
        1,
        'Šodien sākas reklāma'
      )
    ");

  }

  // Reklāmas beigas - nākamajā dienā

  if ($row['reklama_lidz'] == date('Y-m-d', strtotime('-1 day'))) {

    db_query("
      INSERT INTO `".DB_PREF."atgadinajumi` (
        liguma_id,
        datums,
        tema,
        saturs
      ) VALUES (
        ".$row['id'].",
        NOW(),
        1,
        'Vakar beidzās reklāma'
      )
    ");

  }

  // Rēķina izrakstīšanas atgādinājums tai pašā dienā

  if (!empty($rekini[$row['id']])) {

    foreach($rekini[$row['id']] as $v) {

      if (!empty($v['izr_datums']) && $v['izr_datums'] == $curdate) {

        db_query("
          INSERT INTO `".DB_PREF."atgadinajumi` (
            liguma_id,
            datums,
            tema,
            saturs
          ) VALUES (
            ".$row['id'].",
            NOW(),
            2,
            'Šodien jāizraksta rēķins'
          )
        ");

      }

    }

  }

  // Šo vajadzēs vairāk kā vienā vietā
  $sodien_apm_statuss = false;
  if (!empty($rekini[$row['id']])) {
    $sodien_apm_statuss = get_apmaksas_statuss($rekini[$row['id']]);
  }

  // Iegūst apmaksas statusu šodienai un vakardienai. Ja atšķiras, un
  // šodien ir parāds, tad palaiž auto atgādinājuma skriptu.

  if (!empty($rekini[$row['id']])) {

    $vakard_apm_statuss = get_apmaksas_statuss($rekini[$row['id']], date("Y-m-d", strtotime('-4 days')));

    if ($vakard_apm_statuss != $sodien_apm_statuss && $sodien_apm_statuss == 4) {

      generate_auto_reminders($row, $beigu_statuss_data, $rekini[$row['id']]);

    }

  }

  // Pārbaudam visus atgādinājumus, kuru tips ir "Parāds!" un kuri ir aktīvi,
  // un kuri ir vecāki par 2 nedēļām. Ja klientam vēl aizvien ir parāds šobrīd,
  // tad pievienojam jaunu atgādinājumu, piem., "Parāds! 2. atgādinājums!".
  // Iepriekšējo atgādinājumu izdzēšs.

  if (!empty($rekini[$row['id']])) {

    if ($sodien_apm_statuss == 4) {

      $atg_query = db_query("
        SELECT
          a.*,
          v.nokl_teksts
        FROM `".DB_PREF."atgadinajumi` a
        LEFT JOIN `".DB_PREF."atgadinajumi_veidi` v ON (v.id = a.atgadinajuma_veida_id)
        WHERE
          a.liguma_id = ".$row['id']." AND
          a.atgadinajuma_veida_id = 1 AND
          a.statuss = 1 AND
          DATEDIFF(CURDATE(), a.created) >= 14
        ORDER BY a.id ASC
      ");

      while($atg_row = db_get_assoc($atg_query)) {

        $paradijuma_reize = $atg_row['paradijuma_reize'] + 1;

        $result = db_query("
          INSERT INTO `".DB_PREF."atgadinajumi` (
            liguma_id,
            atgadinajuma_veida_id,
            datums,
            tema,
            saturs,
            paradijuma_reize
          ) VALUES (
            ".$row['id'].",
            1,
            NOW(),
            1,
            '".esc($atg_row['nokl_teksts'] . ' ' . $paradijuma_reize .'. atgādinājums!')."',
            ".$paradijuma_reize."
          )
        ");

        if ($result) {

          db_query("
            DELETE FROM `".DB_PREF."atgadinajumi`
            WHERE id = ".$atg_row['id']."
            LIMIT 1
          ");

        }

      }

    }

  }

  // Google statusa nomaiņa uz "atslēgts" un attiecīgo atgādinājumu pievienošana

  // Skatamies vai nav beidzies reklāmas termiņs (beigu diena vakar), ja ir,
  // tad palaiž auto atgādinājuma ģenerēšanas funkciju. Tad skatās, vai google
  // statuss ir "nav ievadīts", "ievadīts" vai "aktīvs". Ja ir, un apmaksas
  // statuss ir jebkuršs izņemot "anulēts", tad nomaina google statusu uz
  // "atslēgts". Kad tas izdarīts, palaiž vēlreiz auto atgādinājuma ģenerēšanu.

  if ($row['auto_atslegts_disable'] == 0) {

    if (!empty($row['reklama_lidz']) && $row['reklama_lidz'] == $yesterday) {

      generate_auto_reminders($row, $beigu_statuss_data, !empty($rekini[$row['id']]) ? $rekini[$row['id']] : null);

      if (in_array($row['google_statuss'], array(1, 2, 3)) && $sodien_apm_statuss != 5) {

        $sql = "
          UPDATE `".DB_PREF."ligumi`
          SET
            google_statuss = 4,
            auto_atslegts = 1
          WHERE id = ".$row['id']."
          LIMIT 1
        ";

        $result = db_query($sql);

        if ($result) {

          $row['google_statuss'] = 4;
          $row['auto_atslegts'] = 1;

          generate_auto_reminders($row, $beigu_statuss_data, !empty($rekini[$row['id']]) ? $rekini[$row['id']] : null);

        }

      }

    }

  }


}
?>