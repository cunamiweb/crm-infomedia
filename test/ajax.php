<?php
include("inc/init.php");

if (!$_SESSION['login']) {
  die();
}

$actions = array(
  'check_client',
  'send_rek_to_email'
);

if (!empty($_GET['action']) && in_array($_GET['action'], $actions)) {

  $function_to_call = 'ajax_' . $_GET['action'];

  $function_to_call();

}

?>