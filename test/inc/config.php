<?php
define("DB_PREF", "new_");
define("DB_HOST", "localhost");
define("DB_NAME", "infomedia_serveris_lv_test");
define("DB_USER", "infomedia");
define("DB_PASS", "ki870cewfv32");

// only used when STOP_ON_ERRORS = false
define("ERROR_MAIL", 'ap-junk@datateks.lv');

// this flag shoul be true ONLY on development site
// when error happens, detailed info is shown
// will stop execution even on notices
// when true, error is not sent to ERROR_MAIL
define("STOP_ON_ERRORS", true);

define("DEBUG_QUERIES", false);

define("REKV_NOSAUK", "Infomedia, SIA");
define("REKV_JUR_ADR", "Rītupes iela 7/9-38, Rīga, LV-1019");
define("REKV_FIZ_ADR", "Kr. Barona iela 108-7, Rīga, LV-1001");
define("REKV_BANKA", "Swedbank, AS");
define("REKV_SWIFT", "HABALV22");
define("REKV_KONTS", "LV38HABA0551020711161");
define("REKV_REG_NR", "40103166641");
define("REKV_PVN_NR", "LV40103166641");

define("DARB_REK_PAK_NOSAUK", "Atlīdzība par pakalpojumiem");

define("LIG_REK_PAK_NOSAUK", "Reklāmas pakalpojumi");
define("LIG_REK_ATKAPS_PAMATOJUMS", "Līgumsods par līgumu Nr. %s pamatojoties uz līguma punktu 11.5.");

define("LIG_REK_INFO1", "Būsim pateicīgi par savlaicīgu rēķina apmaksu.");
define("LIG_REK_INFO2", "Ja rēķins netiks apmaksāts līdz norādītajam datumam, reklāmas darbība tiks apturēta saskaņā ar līguma punktu Nr. 8.7.");
define("LIG_REK_INFO3", "Informācijai par rēķiniem un līgumiem lūdzam zvanīt 29970762.");

define("LIG_REK_VALDES_LOCEKLIS", "V. Juzāns");
define("LIG_REK_TALR", "67290200");
define("LIG_REK_FAX", "67291033");
define("LIG_REK_EPASTS", "info@infomedia.lv");
define("LIG_REK_WWW", "www.infomedia.lv");

define("AVANSA_REK_SUBJ", "Avansa rēķins par reklāmas pakalpojumiem");
define("AVANSA_REK_BODY", "Rēķins pielikumā");

define("GALA_REK_SUBJ", "Rēķins par reklāmas pakalpojumiem");
define("GALA_REK_BODY", "Rēķins pielikumā");

define("LOGOUT_AFTER", 60 * 30); // 30 mins   
?>