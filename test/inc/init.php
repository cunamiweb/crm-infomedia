<?php
include("config.php");
include("functions.php");
include("functions_db.php");
include("functions_validate.php");
include("functions_ajax.php");

set_include_path(get_include_path() . PATH_SEPARATOR . dirname($_SERVER['SCRIPT_FILENAME']) . '/libs/zend/library');

require_once('Zend/Json.php');

// these will be used only if 
$GLOBALS['executed_queries'] = array();
$GLOBALS['query_total_time'] = 0;

mb_internal_encoding('UTF-8');

set_error_handler('standard_error_handler', E_ALL);

session_start();

if (!isset($_SESSION['login'])) {
  $_SESSION['login'] = false;
}

if(!empty($_GET['do_auto_logout'])) { // called through ajax

  if ($_SESSION['login']) {

    if (!empty($_SESSION['last_use_time']) && $_SESSION['last_use_time'] < time() - LOGOUT_AFTER) {
      echo 1;
    }

  } else {

    echo 1;

  }

  echo 0;

  die();

}

if ($_SESSION['login'] && empty($_GET['logout'])) {

  if (!empty($_SESSION['last_use_time']) && $_SESSION['last_use_time'] < time() - LOGOUT_AFTER) {

    header('Location: ?logout=true&auto_logout=1');
    die();

  }

  $_SESSION['last_use_time'] = time();

}

$db = db_connect(DB_HOST, DB_USER, DB_PASS) or die("Neizdevas pieslegties datubazei!");

db_select(DB_NAME) or die("Neatradu datubaazi!");

db_query("SET NAMES utf8");

include("variables.php");
?>