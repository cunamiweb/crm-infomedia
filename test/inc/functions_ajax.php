<?php
function ajax_check_client() {

  $str = !empty($_GET['nosauk']) ? trim($_GET['nosauk']) : '';

  $str = trim(str_ireplace(array('sia', '%'), '', $str));

  if (mb_strlen($str) < 3) {
    echo '<p style="margin: 15px 0 0 0; text-align: center;"><strong>Nosaukumā jānorāda vismaz 3 simboli!</strong></p>';
    return;
  }

  $query = db_query("
    SELECT *
    FROM `".DB_PREF."ligumi`
    WHERE
      `nosaukums` LIKE '%".esc($str)."%'
      " . (mb_strlen($str) >= 8 ? "OR `regnr` LIKE '%".esc($str)."%'" : '') . "
  ");

  echo '
    <table id="ligums_check_table">
      <tr>
        <th>Klients</th>
        <th>Līgums no</th>
        <th>Līgums līdz</th>
      </tr>
  ';

  if (db_num_rows($query) > 0) {

    while($row = db_get_assoc($query)) {

      echo '
        <tr>
          <td>'.$row['nosaukums'].'</td>
          <td>'.(!empty($row['reklama_no']) ? date('d.m.Y', strtotime($row['reklama_no'])) : '').'</td>
          <td>'.(!empty($row['reklama_lidz']) ? date('d.m.Y', strtotime($row['reklama_lidz'])) : '').'</td>
        </tr>
      ';

    }

  } else {

    echo '
      <tr>
        <td colspan="3">Netika atrasts neviens klients</td>
      </tr>
    ';

  }

  echo '</table>';


}

function ajax_send_rek_to_email() {

  if (!is_admin()) {
    return;
  }

  require_once("Zend/Mail.php");

  if (empty($_GET['rek_id'])) {
    return;
  }

  $rek_id = (int) $_GET['rek_id'];

  $tipi = array('fails_avansa_ep', 'fails_gala_ep');

  if (empty($_GET['tips']) || !in_array($_GET['tips'], $tipi)) {
    return;
  }

  $tips = $_GET['tips'];

  $rek_data = db_get_assoc(db_query("
    SELECT *
    FROM `".DB_PREF."rekini`
    WHERE id = ".$rek_id."
  "));

  if (empty($rek_data[$tips])) {
    $error = 'Rēķinam nav izveidots PDF fails!';
  } else {

    $faila_data = db_get_assoc(db_query("
      SELECT *
      FROM `".DB_PREF."faili`
      WHERE id = ".$rek_data[$tips]."
    "));

    if (empty($faila_data) || !is_file('faili/'.$faila_data['id'])) {
      $error = 'Rēķina fails netika atrasts!';
    } else {

      $piegade = db_get_assoc(db_query("
        SELECT *
        FROM `".DB_PREF."ligumi_rekinu_piegades`
        WHERE
          liguma_id = ".$rek_data['liguma_id']." AND
          piegades_veids = 1
        LIMIT 1
      "));

      if (empty($piegade['adrese'])) {
        $error = 'Klientam nav norādīts piegādes e-pasts!';
      } else {

        $name = 'faili/'.$faila_data['id'];

        $file_content = file_get_contents($name);

        $mail = new Zend_Mail('UTF-8');

        if ($tips == 'fails_avansa_ep') {

          $mail->setSubject(AVANSA_REK_SUBJ);
          $mail->setBodyText(AVANSA_REK_BODY);

        } else {

          $mail->setSubject(GALA_REK_SUBJ);
          $mail->setBodyText(GALA_REK_BODY);

        }

        $mail->addTo($piegade['adrese']);

        $at = $mail->createAttachment($file_content);

        $at->filename = $faila_data['fails'];

        if (!$mail->send()) {
          $error = 'Neizdevās nosūtīt rēķinu pa e-pastu! Tehniska kļūda';
        }

      }

    }

  }


  if (!empty($error)) {
    echo Zend_Json::encode(array('error' => $error));
  } else {
    echo Zend_Json::encode(array('success' => 1));
  }




}
?>