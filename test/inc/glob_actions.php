<?php
if ($_SESSION['login']) {

  if(!empty($_GET['logout'])){

    $_SESSION = array();

    if (!empty($_GET['auto_logout'])) {
      $_SESSION['auto_logout'] = true;
    }

    header("location: ./");
    die();

  }

  if(!empty($_GET['getfile'])){

    $sql = "
      SELECT f.*
      FROM `".DB_PREF."faili` f
      WHERE f.`id` = ".(int)$_GET['getfile']."
    ";

    if (!is_admin()) {

      $sql .= "
        AND
          (
            (
              objekta_tips IN ('darb_norekini', 'darb_rekviziti', 'darb_atgadinajumi', 'darb_ligumi') AND
              objekta_id = ".$_SESSION['user']['id']."
            ) OR (
              objekta_tips IN ('lig_rekini', 'lig_klients', 'lig_pakalpojums', 'lig_atgadinajumi') AND
              objekta_id IN (
                SELECT id
                FROM `".DB_PREF."ligumi`
                WHERE pardeveja_id = ".$_SESSION['user']['id']."
              )
            ) OR (
              objekta_tips = 'forums_tema'
            )
          )
      ";

    }

    $r = db_query($sql);
    $a = db_get_assoc($r);

    if (empty($a)) {
      die();
    }

    $name = 'faili/'.$a['id'];

    if(file_exists($name)){

      $outname = $a['fails'];

      $fp = fopen($name, 'rb');

      header("Content-type: application/octet-stream");
      header("Content-Disposition:attachment; filename=\"".trim(htmlentities($outname))."\"");
      header("Content-Length: ".filesize($name));

      fpassthru($fp);
      die();

    }

    die();

  }

  if(!empty($_GET['deletefile'])){

    if (!is_admin()) {
      die();
    }

    db_query("DELETE FROM `".DB_PREF."faili` WHERE `id` = ".(int)$_GET['deletefile']);
    unlink("faili/".(int)$_GET['deletefile']);

    die();

  }

  if(!empty($_GET['deleteClient'])){

    if (!is_admin()) {
      die();
    }

    $id = (int)$_GET['deleteClient'];

    db_query("DELETE FROM `".DB_PREF."ligumi` WHERE `id` = ".(int)$id);

    db_query("DELETE FROM `".DB_PREF."atgadinajumi` WHERE `liguma_id` = ".(int)$id);

    db_query("DELETE FROM `".DB_PREF."kontakti` WHERE `liguma_id` = ".(int)$id);

    db_query("DELETE FROM `".DB_PREF."ligumi_rekinu_piegades` WHERE `liguma_id` = ".(int)$id);

    db_query("DELETE FROM `".DB_PREF."pakalpojumi` WHERE `liguma_id` = ".(int)$id);

    db_query("DELETE FROM `".DB_PREF."pielikumi_infomedia` WHERE `liguma_id` = ".(int)$id);

    db_query("DELETE FROM `".DB_PREF."pielikumi_tiessaite` WHERE `liguma_id` = ".(int)$id);

    db_query("DELETE FROM `".DB_PREF."rekini` WHERE `liguma_id` = ".(int)$id);

    // Izdze�am visus klienta failus
    $query = db_query("SELECT * FROM `".DB_PREF."faili` WHERE `liguma_id` = ".(int)$id." AND objekta_tips IN ('lig_klients', 'lig_pakalpojums', 'lig_rekini', 'lig_atgadinajumi')");

    while($row = db_get_assoc($query)) unlink("faili/".$row['id']);

    db_query("DELETE FROM `".DB_PREF."faili` WHERE `objekta_id` = ".(int)$id." AND objekta_tips IN ('lig_klients', 'lig_pakalpojums', 'lig_rekini', 'lig_atgadinajumi')");

  }

  if(!empty($_GET['updateAtgStatus']) && !empty($_GET['status'])){

    // called from ajax

    $old_data = db_get_assoc(db_query("SELECT * FROM `".DB_PREF."atgadinajumi` WHERE id = " . (int)$_GET['updateAtgStatus']));

    $apstr_laiks = 'apstr_laiks';
    $apstr_pardeveja_id = 'apstr_pardeveja_id';

    if ($old_data['statuss'] == 1 && $_GET['status'] != 1) {
      $apstr_laiks = "NOW()";
      $apstr_pardeveja_id = (int)$_SESSION['user']['id'];
    } elseif ($old_data['statuss'] != 1 && $_GET['status'] == 1) {
      $apstr_laiks = 'NULL';
      $apstr_pardeveja_id = 'NULL';
    }

    $result = db_query("
      UPDATE `".DB_PREF."atgadinajumi`
      SET
        statuss = ".(int)$_GET['status'].",
        apstr_laiks = ".$apstr_laiks.",
        apstr_pardeveja_id = ".$apstr_pardeveja_id."
      WHERE `id` = ".(int)$_GET['updateAtgStatus']
    );

    echo $result ? 1 : 0;

    die();

  }

  if(!empty($_GET['deletecomment'])){

    if (!is_admin()) {

      $pardeveja_id = db_get_val(db_query("
        SELECT pardeveja_id
        FROM `".DB_PREF."komentari`
        WHERE `id` = ".(int)$_GET['deletecomment']."
      "));

      if ($pardeveja_id != $_SESSION['user']['id']) {
        die();
      }

    }

    db_query("
      DELETE FROM `".DB_PREF."komentari`
      WHERE `id` = ".(int)$_GET['deletecomment']."
    ");

    die();

  }

}
?>