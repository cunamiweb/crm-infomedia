<?php
function validate_ligums_form($data) {

  $data = trim_array($data);

  $errors = array();

  if (empty($data['ligumanr'])) {
    $errors['klients']['orgligumanr'] = '';
  } else {

    $c = db_get_val(db_query("
      SELECT COUNT(*)
      FROM `".DB_PREF."ligumi`
      WHERE
        ligumanr = '".esc($data['ligumanr'])."'
        ".(!empty($data['liguma_id']) ? " AND id != " . (int)$data['liguma_id'] : '')."
    "));

    if (!empty($c)) {
      $errors['klients']['orgligumanr'] = 'Šāds līguma numurs jau ir aizņemts!';
    }


  }

  if (!empty($data['rekini'])) {

    foreach($data['rekini'] as $rekina_id => $r) {

      if (!empty($r['anulets']) && empty($r['anul_datums'])) {
        $errors['rekini'][] = sprintf('Rēķinam %s nav norādīts anulēšanas datums!', $r['rek_nr']);
      }

      if (!empty($r['nodots_piedz']) && empty($r['nodots_piedz_datums'])) {
        $errors['rekini'][] = sprintf('Rēķinam %s nav norādīts nodots piedziņai datums!', $r['rek_nr']);
      }

      if (!empty($data['ligumadat']) && !empty($r['izr_datums']) && to_db_date($r['izr_datums']) < to_db_date($data['ligumadat'])) {
        $errors['rekini'][] = sprintf('Rēķinam %s izrakstīšanas datums ir norādīts vecāks par līguma datumu!', $r['rek_nr']);
      }

    }

  }

  if (!empty($data['rekini_new']['izr_datums'])) {

    foreach($data['rekini_new']['izr_datums'] as $i => $izr_datums) {

      if (!empty($data['rekini_new']['anulets'][$i]) && empty($data['rekini_new']['anul_datums'][$i])) {
        $errors['rekini'][] = sprintf('Rēķinam %s nav norādīts anulēšanas datums!', $data['rekini_new']['rek_nr'][$i]);
      }

      if (!empty($data['rekini_new']['nodots_piedz'][$i]) && empty($data['rekini_new']['nodots_piedz_datums'][$i])) {
        $errors['rekini'][] = sprintf('Rēķinam %s nav norādīts nodots piedziņai datums!', $data['rekini_new']['rek_nr'][$i]);
      }

      if (!empty($data['ligumadat']) && !empty($data['rekini_new']['izr_datums'][$i]) && to_db_date($data['rekini_new']['izr_datums'][$i]) < to_db_date($data['ligumadat'])) {
        $errors['rekini'][] = sprintf('Rēķinam %s izrakstīšanas datums ir norādīts vecāks par līguma datumu!', $data['rekini_new']['rek_nr'][$i]);
      }

    }

  }

  if (empty($data['nosaukums']))
    $errors['klients']['nosaukumsplace'] = '';

  return $errors;

}

function validate_darbinieks_form($data) {

  $data = trim_array($data);

  $errors = array();

  if (empty($data['vards']))
    $errors['rekviziti']['vardsplace'] = '';

  if (!empty($data['password1']) || !empty($data['password2'])) {

    if ($data['password1'] != $data['password2']) {
      $errors['rekviziti']['password1'] = 'Paroles nesakrīt!';
      $errors['rekviziti']['password2'] = '';
    } elseif (mb_strlen($data['password1']) < 6) {
      $errors['rekviziti']['password1'] = 'Parole ir par īsu!';
      $errors['rekviziti']['password2'] = '';
    }

  }

  if (!empty($data['rekini'])) {

    foreach($data['rekini'] as $rekina_id => $r) {

      if (!empty($r['apmaksats']) && empty($r['apm_datums'])) {
        $errors['norekini'][] = sprintf('Rēķinam %s nav norādīts apmaksas datums!', $r['rek_nr']);
      }

    }

  }

  return $errors;

}

function validate_atgadinajumi_form($data) {

  $data = trim_array($data);

  $errors = array();

  return $errors;

}

function validate_forums_tema_form($data) {

  $data = trim_array($data);

  $errors = array();

  if (empty($data['tema'])) {
    $errors['form']['tema'] = 'Nav norādīta tēma!';
  }

  if (empty($data['saturs'])) {
    $errors['form']['saturs'] = 'Nav norādīts teksts!';
  }

  return $errors;

}

function validate_forums_atbilde_form($data) {

  $data = trim_array($data);

  $errors = array();

  if (empty($data['saturs'])) {
    $errors['form']['saturs'] = 'Nav norādīts teksts!';
  }

  return $errors;

}

function validate_darb_kopsavilkums_form($data) {

  $data = trim_array($data);

  $errors = array();


  return $errors;

}

function validate_lig_kopsavilkums_form($data) {

  $data = trim_array($data);

  $errors = array();


  return $errors;

}

function validate_beigu_statuss_form($data) {

  $data = trim_array($data);

  $errors = array();

  if (empty($data['beigu_statuss'])) {
    $errors['form']['beigu_statuss'] = 'Nav norādīts veids!';
  } else {

    if ($data['beigu_statuss'] == 'parslegts') {

      if (empty($data['parslegts_lig_nr'])) {
        $errors['form']['parslegts_lig_nr'] = 'Nav norādīts jaunā līguma numurs!';
      }


    } else if ($data['beigu_statuss'] == 'termins') {

      if (empty($data['atlikt_lidz'])) {
        $errors['form']['atlikt_lidz'] = 'Nav norādīts datums līdz guram atlikt statusa nomaiņu!';
      }

      if (empty($data['komentars1'])) {
        $errors['form']['komentars1'] = 'Nav norādīts komentārs pie atlikšanas uz laiku!';
      }

    } elseif ($data['beigu_statuss'] == 'atteikums') {

      if (empty($data['komentars2'])) {
        $errors['form']['komentars2'] = 'Nav norādīts komentārs pie atteikuma!';
      }

    }

  }

  return $errors;

}
?>