<?php
define("DB_PREF", "infomedia__");
define("DB_HOST", "");
define("DB_NAME", "");
define("DB_USER", "");
define("DB_PASS", "");

// only used when STOP_ON_ERRORS = false
define("ERROR_MAIL", '');

// this flag shoul be true ONLY on development site
// when error happens, detailed info is shown
// will stop execution even on notices
// when true, error is not sent to ERROR_MAIL
define("STOP_ON_ERRORS", false);

define("DEBUG_QUERIES", false);

define("REKV_NOSAUK", "");
define("REKV_JUR_ADR", "");
define("REKV_FIZ_ADR", "");
define("REKV_BANKA", "");
define("REKV_SWIFT", "");
define("REKV_KONTS", "");
define("REKV_REG_NR", "");
define("REKV_PVN_NR", "");

define("DARB_REK_PAK_NOSAUK", "");

define("LIG_REK_PAK_NOSAUK", "");
define("LIG_REK_ATKAPS_PAMATOJUMS", "");

define("LIG_REK_INFO1", "");
define("LIG_REK_INFO2", "");
define("LIG_REK_INFO3", "");

define("LIG_REK_VALDES_LOCEKLIS", "");
define("LIG_REK_TALR", "");
define("LIG_REK_FAX", "");
define("LIG_REK_EPASTS", "");
define("LIG_REK_WWW", "");

define("AVANSA_REK_SUBJ", "Avansa r��ins par rekl�mas pakalpojumiem");
define("AVANSA_REK_BODY", "R��ins pielikum�");

define("GALA_REK_SUBJ", "Avansa r��ins par rekl�mas pakalpojumiem");
define("GALA_REK_BODY", "R��ins pielikum�");

define("LOGOUT_AFTER", 60 * 30); // 30 mins

?>