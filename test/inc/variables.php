<?
// visadi mainigie
$_vars = array();

$_vars['sanemsanas_forma'] = array(
  1=>"Orģināls",
  2=>"Fakss",
  3=>"E-pasts",
);

$_vars['infomediastatuss'] = array(
  1=>"Nav ievadīts",
  2=>"Ievadīts",
  3=>"Daļēji ievadīts",
  4=>"Googlē",
);

$_vars['googlestatuss'] = array(
  1=>"Nav ievadīts",
  2=>"Ievadīts",
  3=>"Pieslēgts",
  4=>"Atslēgts",
);

$_vars['rekina_piegade'] = array(
  1=>"Uz e-pastu",
  2=>"Pa&nbsp;pastu",
);

$_vars['pakalpojumi'] = array();

$query = db_query("SELECT * FROM `".DB_PREF."pakalpojumi_veidi` ORDER BY `id` ASC");

while($row = db_get_assoc($query)){
  $_vars['pakalpojumi'][$row['id']] = $row['nosaukums'];
}

$_vars['rekina_statuss'] = array(
  1=>"Nav",
  2=>"Izsūtīts",
  3=>"Apstiprināts",
);

$_vars['apmaksas_statuss'] = array(
  4=>"Nav apm. termiņš",
  2=>"Apmaksāts",
  5=>"Daļēji apmaksāts",
  3=>"Parāds!",
  7=>"Nodots piedziņai",
  6=>"Anulēts",
  1=>"Nav sācies",
);

$_vars['kopapmaksas_statuss'] = array(
  1=>"Nav apm. termiņš",
  2=>"Apmaksāts",
  3=>"Daļēji apmaksāts",
  4=>"Parāds",
  6=>"Nodots piedziņai",
  5=>"Anulēts",
);

$_vars['liguma_statuss'] = array(
  1=>"Nav sācies",
  //2=>"Aktīvs, bet ir parāds"
  3=>"Aktīvs",
  4=>"Apturēts",
  5=>"Pārtraukts",
  6=>"Beidzies",
  7=>"Anulēts",
);

$_vars['liguma_beigu_statuss'] = array(
  'termins' => 'termiņš',
  'parslegts' => 'pārslēgts',
  'atteikums' => 'atteikums',
);

$_vars['sys_pardeveji'] = array();

$query = db_query("SELECT * FROM `".DB_PREF."pardeveji` ORDER BY `vards`");

while($row = db_get_assoc($query)){
  $_vars['sys_pardeveji'][$row['id']] = $row['vards'];
}

$_vars['atgadinajumu_temas'] = array(
  1=>"Klienti",
  2=>"Klientu norēķini",
  3=>"Darbinieki",
  4=>"Privāts",
  5=>"Darbinieku algas"
);

$_vars['pard_rekina_statuss'] = array(
  1=>"Neapmaksāts",
  2=>"Apmaksāts"
);

$_vars['menesi'] = array(
  1=>"Janvāris",
  2=>"Februāris",
  3=>"Marts",
  4=>"Aprīlis",
  5=>"Maijs",
  6=>"Jūnijs",
  7=>"Jūlijs",
  8=>"Augusts",
  9=>"Septembris",
  10=>"Oktobris",
  11=>"Novembris",
  12=>"Decembris"
);

?>