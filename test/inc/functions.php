<?php

function resize_bytes($size) {

   if($size==0){ $size = 1; }

   $filesizename = array(" Bytes", " KB", " MB", " GB", " TB", " PB", " EB", " ZB", " YB");

   return round($size/pow(1024, ($i = floor(log($size, 1024)))), 2) . $filesizename[$i];
   
}

function upload($files, $who, $dest, $overwrite=true) { // False - OK, else return error

  if (!is_uploaded_file($files['tmp_name'][$who]) || $files['error'][$who] != 0) return false;

  if ($files['size'][$who] <= 0) return false;

  if(!$overwrite) if (file_exists($dest)) return false;

  if (function_exists("move_uploaded_file")) {
    if (!move_uploaded_file($files['tmp_name'][$who], $dest))  return false;
  } else {
    if (!copy($fails["tmp_name"][$who], $dest))  return false;
  }

  if (!chmod($dest, 0644)) return false;

  return true;

}

function trim_array($array) {

  if (!is_array($array)) {
    return false;
  }

  foreach($array as $k => $v) {

    if (is_array($v)) {
      $array[$k] = trim_array($v);
    } elseif (is_string($v)) {
      $array[$k] = trim($v);
    } else {
      $array[$k] = $v;
    }

  }

  return $array;

}

function debug_var($var) {

  echo '<pre>';
  print_r($var);
  echo '</pre>';

}

function standard_error_handler($errno, $errstr, $errfile, $errline, $errcontext) {

  $msg = '';

  $levels = array(
    1 => 'E_ERROR',
    2 => 'E_WARNING',
    4 => 'E_PARSE',
    8 => 'E_NOTICE',
    16 => 'E_CORE_ERROR',
    32 => 'E_CORE_WARNING',
    64 => 'E_COMPILE_ERROR',
    128 => 'E_COMPILE_WARNING',
    256 => 'E_USER_ERROR',
    512 => 'E_USER_WARNING',
    1024 => 'E_USER_NOTICE',
    2048 => 'E_STRICT',
    4096 => 'E_RECOVERABLE_ERROR',
    8192 => 'E_DEPRECATED',
    16384 => 'E_USER_DEPRECATED'
  );

  $errtype = isset($levels[$errno]) ? $levels[$errno] : $errno;

  $msg .= sprintf("%s: %s in %s on line %s)\n\n", $errtype, $errstr, $errfile, $errline);

  if (php_sapi_name() != 'cli') {

    $msg .= sprintf("Time: %s\n", date('Y-m-d H:i:s'));
    $msg .= sprintf("IP: %s\n\n", $_SERVER['REMOTE_ADDR']);

    $msg .= sprintf("CONTEXT: \n\n%s\n\n", print_r($errcontext, true));

    $msg .= sprintf("BACKTRACE: \n\n%s\n\n", print_r(debug_backtrace(), true));

    $msg .= sprintf("_SERVER variable: \n\n%s\n\n", print_r($_SERVER, true));

  }

  if (STOP_ON_ERRORS) {

    die('<pre style="text-align: left;">' . $msg . '</pre>');

  } else {

    if (ERROR_MAIL) {
      mail(ERROR_MAIL, 'Infomedia System error', $msg);
    }

    return false; // "give" error back to standard PHP error handler

  }

}

function show_files_block($objekta_tips, $objekta_id = null, $show_submit = false) {

  ?>

  <div class="file_block">

    <h2>Saistītie faili</h2>

    <? if(!empty($objekta_id)) { ?>

      <ul>

      <? $query = db_query("
        SELECT *
        FROM `".DB_PREF."faili`
        WHERE
          `objekta_id` = ".(int)$objekta_id." AND
          `objekta_tips` = '".esc($objekta_tips)."'
        ORDER BY `id` DESC
      "); ?>

      <? while($row = db_get_assoc($query)) { ?>
        <li style="margin-bottom:10px;"><a class="download_link" href="?getfile=<?=$row['id'];?>" target="downloadframe"><?=$row['fails'];?></a> <span class="filesize">(<?=resize_bytes($row['izmers']);?>)</span>
        <a class="hidethistoo" href="javascript:;" onClick="deleteFile(this,<?=$row['id'];?>)">Dzēst šo failu!</a></li>
      <? } ?>

      </ul>

    <? } ?>

    <div style="margin:15px;"></div>

    <button class="hidethistoo" onclick="addmorefiles(this, 'faili_<?=$objekta_tips;?>'); $(this).parents('div.file_block').find('div.submit').show(); return false;">Pievienot jaunu failu</button>

    <? if ($show_submit) { ?>
      <div style="display: none; text-align: center; margin-top: 10px;" class="submit">
        <button type="submit">Saglabāt</button>
      </div>
    <? } ?>

  </div>

  <?php

}

function microtime_diff($start, $end) {

  return round($end - $start, 6);

}


function process_files($file_types, $object_id) {

  foreach($file_types as $file_type) {

    $key = 'faili_' . $file_type;

    if (!empty($_FILES[$key])) {

      foreach($_FILES[$key]['name'] as $k => $name){

        db_query("
          INSERT INTO `".DB_PREF."faili` SET
            `objekta_id` = ".$object_id.",
            `objekta_tips` = '".$file_type."',
            `fails` = '".esc($name)."',
            `izmers` = ".$_FILES[$key]['size'][$k]."
        ");

        if(!upload($_FILES[$key], $k, "faili/" . db_last_id(), false)) {

          db_query("DELETE FROM `".DB_PREF."faili` WHERE `id` = ".db_last_id());

        }

      }

    }

  }

}

function is_admin() {

  if (!empty($_SESSION['user']['tips']) && $_SESSION['user']['tips'] == 'admin') {
    return true;
  }

  return false;

}

function format_currency($amount, $coma = false) {

  if ($coma) {
    return number_format($amount, 2, ',', '');
  } else {
    return number_format($amount, 2, '.', '');
  }

}


function show_comments_block($objekta_tips, $objekta_id = null, $show_submit = false) {

  ?>

  <div class="comments_block">

    <h2>Komentāri</h2>

    <div style="padding-bottom:5px; text-align: center;">
      <button class="hidethistoo" onclick="addmorecomments(this, '<?=$objekta_tips;?>'); $(this).parents('div.comments_block').find('div.submit').show(); return false;">Pievienot jaunu komentāru</button>
    </div>

    <? if ($show_submit) { ?>
      <div style="display: none; text-align: center;" class="submit">
        <button type="submit">Saglabāt</button>
      </div>
    <? } ?>

    <ol>

    <? if(!empty($objekta_id) || substr($objekta_tips, 0, 1) == '_') {

      $query = db_query("
        SELECT
          k.*,
          f.nosaukums as lauka_nosaukums,
          p.vards as pardeveja_vards
        FROM `".DB_PREF."komentari` k
        LEFT JOIN `".DB_PREF."formu_lauki` f ON (f.id = k.formas_lauka_id)
        LEFT JOIN `".DB_PREF."pardeveji` p ON (p.id = k.pardeveja_id)
        WHERE
          k.`objekta_tips` = '".esc($objekta_tips)."'
          " . (substr($objekta_tips, 0, 1) != '_' ? " AND k.`objekta_id` = ".(int)$objekta_id : '') . "
        ORDER BY k.created DESC
      ");

      $i = db_num_rows($query);

      while($row = db_get_assoc($query)) {
      ?>

        <li value="<?= $i ?>">
          <div class="text">
            <?= nl2br($row['komentars']) ?>
          </div>
          <div class="meta">
            <strong><?= $row['pardeveja_vards'] ?></strong>
            <? if (!empty($row['lauka_nosaukums'])) { ?>
              par lauku <strong><?= $row['lauka_nosaukums'] ?></strong>
            <? } ?><br />
            <?= date('d.m.Y H:i', strtotime($row['created'])) ?>
            <? if (is_admin() || $row['pardeveja_id'] == $_SESSION['user']['id']) { ?>
              <a class="del_link" href="#" onclick="deleteComment(this, <?= $row['id'] ?>); return false;" title="Dzēst"><img src="css/del.png" alt="Dzēst" /></a>
            <? } ?>
          </div>
          <div class="clear"></div>
        </li>

        <? $i -- ?>

      <?  } ?>

    <? } ?>

    </ol>

  </div>

  <?php

}


function process_comments($object_types, $object_id = null) {

  foreach($object_types as $object_type) {

    $key = $object_type;

    if (!empty($_POST['komentari_new'][$key])) {

      foreach($_POST['komentari_new'][$key]['komentars'] as $k => $komentars){

        if (empty($komentars)) {
          continue;
        }

        $formas_lauka_id = 'null';
        if (!empty($_POST['komentari_new'][$key]['formas_lauka_id'][$k])) {
          $formas_lauka_id = (int) $_POST['komentari_new'][$key]['formas_lauka_id'][$k];
        }

        db_query("
          INSERT INTO `".DB_PREF."komentari` SET
            `objekta_id` = ".(!empty($object_id) ? (int)$object_id : 'null').",
            `objekta_tips` = '".$object_type."',
            `formas_lauka_id` = ".$formas_lauka_id.",
            `pardeveja_id` = ".$_SESSION['user']['id'].",
            `komentars` = '".esc($komentars)."'
        ");

      }

    }

  }

}

function get_apmaksas_statuss($rekini, $checkdate = null) {

  if (empty($checkdate)) {
    $checkdate = date("Y-m-d", strtotime('-3 days'));
  }

  $statuss = 0;

  $kopa = 0;

  $nav_sacies = 0;
  $apmaksats = 0;
  $parads = 0;
  $nav_apmaksats = 0;
  $dal_apmaksats = 0;
  $anulets = 0;
  $nodots_piedz = 0;

  if(!empty($rekini)) {

    foreach($rekini as $v) {

      $kopa ++;

      if(!empty($v['izr_datums'])) {

        if(!empty($v['anulets'])){

          $anulets ++;

        } else if(!empty($v['nodots_piedz'])){

          $nodots_piedz ++;

        } else {

          if(!empty($v['apm_termins'])) {

            $apmSumSant = !empty($v['apm_summa']) ? (int) ($v['apm_summa'] * 100) : 0;
            $needSumSant = !empty($v['kopsumma']) ? (int) ($v['kopsumma'] * 100) : 0;

            if ($needSumSant <= $apmSumSant){

              $apmaksats ++;

            } elseif (empty($apmSumSant)) {

              if($checkdate > $v['apm_termins']){

                $parads ++;

              } else {

                $nav_apmaksats ++;

              }

            } else {

              if($checkdate > $v['apm_termins']) {

                $parads ++;

              } else {

                $dal_apmaksats ++;

              }

            }

          }

        }

      }

    }

  }

  if ($dal_apmaksats == 0 && $apmaksats == 0 && $parads == 0 && $nodots_piedz == 0 && $anulets != $kopa ) {

    // nav apm. termiņs (visi rēķini ir neapmaksāti, bet nav parādu un piedziņas)

    $statuss = 1;

  } else if ($apmaksats > 0 && $dal_apmaksats == 0 && $parads == 0 && $nodots_piedz == 0) {

    // apmaksāts (nav parādu un piedziņas)

    $statuss = 2;

  } else if ($dal_apmaksats > 0 && $parads == 0 && $nodots_piedz == 0) {

    // daļēji apmaksāts (kāds rēķins ir daļēji apmaksāts, bet nav parādu un piedziņas)

    $statuss = 3;

  } else if ($parads > 0) {

    // parāds (kādam no rēķiniem ir parāds ilgāk par 3 dienām)

    $statuss = 4;

  } else if ($anulets == $kopa) {

    // anulēts (visi rēķini anulēti)

    $statuss = 5;

  } else if ($nodots_piedz > 0) {

    // nodots piedziņai (kāds no rēķiniem ir nodots piedziņai)

    $statuss = 6;

  }

  return $statuss;

}

function is_barteris($rekini) {

  if (count($rekini) == 1) {

    if (!empty($rekini[0]['barteris'])) {

      return true;

    }

  }

  return false;

}

function get_liguma_statuss($ligums, $rekini) {

  $rekinu_statuss = get_apmaksas_statuss($rekini);

  if (empty($rekinu_statuss) || empty($ligums['reklama_no']) || empty($ligums['reklama_lidz']) || empty($ligums['google_statuss'])) {
    return 0;
  }

  $c = array();

  $c[1] = array(); // vēl nav
  $c[2] = array(); // aktīvs
  $c[3] = array(); // beidzies

  $c[1][1] = array(); // nav ievadīts
  $c[1][2] = array(); // ievadīts
  $c[1][3] = array(); // aktīvs
  $c[1][4] = array(); // atslēgts

  $c[2][1] = array(); // nav ievadīts
  $c[2][2] = array(); // ievadīts
  $c[2][3] = array(); // aktīvs
  $c[2][4] = array(); // atslēgts

  $c[3][1] = array(); // nav ievadīts
  $c[3][2] = array(); // ievadīts
  $c[3][3] = array(); // aktīvs
  $c[3][4] = array(); // atslēgts


  // Reklāmas termiņš: vēl nav
  // -------------------------

  // Google statuss: nav ievadīts
  $c[1][1][1] = 1;
  $c[1][1][2] = 1;
  $c[1][1][3] = 1;
  $c[1][1][4] = 1;
  $c[1][1][5] = 7;
  $c[1][1][6] = 1;

  // Google statuss: ievadīts
  $c[1][2][1] = 1;
  $c[1][2][2] = 1;
  $c[1][2][3] = 1;
  $c[1][2][4] = 1;
  $c[1][2][5] = 7;
  $c[1][2][6] = 1;

  // Google statuss: aktīvs
  $c[1][3][1] = 1;
  $c[1][3][2] = 1;
  $c[1][3][3] = 1;
  $c[1][3][4] = 1;
  $c[1][3][5] = 7;
  $c[1][3][6] = 1;

  // Google statuss: atslēgts
  $c[1][4][1] = 1;
  $c[1][4][2] = 1;
  $c[1][4][3] = 1;
  $c[1][4][4] = 1;
  $c[1][4][5] = 7;
  $c[1][4][6] = 1;


  // Reklāmas termiņš: aktīvs
  // ------------------------

  // Google statuss: nav ievadīts
  $c[2][1][1] = 3;
  $c[2][1][2] = 3;
  $c[2][1][3] = 3;
  $c[2][1][4] = 3;
  $c[2][1][5] = 7;
  $c[2][1][6] = 3;

  // Google statuss: ievadīts
  $c[2][2][1] = 3;
  $c[2][2][2] = 3;
  $c[2][2][3] = 3;
  $c[2][2][4] = 3;
  $c[2][2][5] = 7;
  $c[2][2][6] = 3;

  // Google statuss: aktīvs
  $c[2][3][1] = 3;
  $c[2][3][2] = 3;
  $c[2][3][3] = 3;
  $c[2][3][4] = 3;
  $c[2][3][5] = 7;
  $c[2][3][6] = 3;

  // Google statuss: atslēgts
  $c[2][4][1] = 4; // vai 5 (sk. tālāk kodā)
  $c[2][4][2] = 4; // vai 5
  $c[2][4][3] = 4; // vai 5
  $c[2][4][4] = 4; // vai 5
  $c[2][4][5] = 7;
  $c[2][4][6] = 4; // vai 5


  // Reklāmas termiņš: beidzies
  // --------------------------

  // Google statuss: nav ievadīts
  $c[3][1][1] = 6;
  $c[3][1][2] = 6;
  $c[3][1][3] = 6;
  $c[3][1][4] = 6;
  $c[3][1][5] = 7;
  $c[3][1][6] = 6;

  // Google statuss: ievadīts
  $c[3][2][1] = 6;
  $c[3][2][2] = 6;
  $c[3][2][3] = 6;
  $c[3][2][4] = 6;
  $c[3][2][5] = 7;
  $c[3][2][6] = 6;

  // Google statuss: aktīvs
  $c[3][3][1] = 6;
  $c[3][3][2] = 6;
  $c[3][3][3] = 6;
  $c[3][3][4] = 6;
  $c[3][3][5] = 7;
  $c[3][3][6] = 6;

  // Google statuss: atslēgts
  $c[3][4][1] = 6;
  $c[3][4][2] = 6;
  $c[3][4][3] = 6;
  $c[3][4][4] = 6;
  $c[3][4][5] = 7;
  $c[3][4][6] = 6;


  $lig_statuss = 0;

  $rekl_statuss = get_reklamas_statuss($ligums);

  foreach($c as $k1 => $v1) {

    if ($rekl_statuss == $k1) {

      foreach($v1 as $k2 => $v2) {

        if ($ligums['google_statuss'] == $k2) {

          foreach($v2 as $k3 => $v3) {

            if ($k3 == $rekinu_statuss) {

              if ($k1 == 2 && $k2 == 4 && in_array($k3, array(1, 2, 3, 4, 6))) {

                // ja reklāma aktīva, bet gūgles statuss pātraukts, tad līguma
                // statuss "apturēts"

                // ja reklāma aktīva, bet gūgles statuss pātraukts, un visi
                // rēķini ir vai nu apmaksāti vai anulēti (bet ne vienādi),
                // tad statuss ir "pārtraukts"


                $kopa = 0;
                $anulets = 0;
                $apmaksats = 0;

                foreach($rekini as $r) {

                  $kopa ++;

                  if (!empty($r['izr_datums'])) {

                    if (!empty($r['anulets'])) {

                      $anulets ++;

                    } else {

                      if(!empty($r['apm_termins'])) {

                        $apmSumSant = !empty($r['apm_summa']) ? (int) ($r['apm_summa'] * 100) : 0;
                        $needSumSant = !empty($r['kopsumma']) ? (int) ($r['kopsumma'] * 100) : 0;

                        if ($needSumSant <= $apmSumSant){

                          $apmaksats ++;

                        }

                      }

                    }

                  }

                }

                if ($anulets > 0 && $apmaksats > 0 && $anulets + $apmaksats == $kopa) {

                  $lig_statuss = 5; // paatraukts
                  break 3;

                } else {

                  $lig_statuss = $v3;
                  break 3;

                }

              } else {

                $lig_statuss = $v3;
                break 3;

              }

            }

          }

        }

      }

    }

  }

  return $lig_statuss;

}

function get_reklamas_statuss($ligums) {

  $rekl_statuss = 0;

  $curdate = date('Y-m-d');

  if ($curdate < $ligums['reklama_no']) {
    $rekl_statuss = 1;
  } else if ($curdate >= $ligums['reklama_no'] && $curdate <= $ligums['reklama_lidz']) {
    $rekl_statuss = 2;
  } else if ($curdate > $ligums['reklama_lidz']) {
    $rekl_statuss = 3;
  }

  return $rekl_statuss;

}

function get_rekina_statuss($rekins) {

  $apm_statuss_id = null;

  if(!empty($rekins['anulets'])){

    $apm_statuss_id = 6;

  } else if(!empty($rekins['nodots_piedz'])){

    $apm_statuss_id = 7;

  } else {

    if(!empty($rekins['apm_termins'])){

      $now = date("Y")*10000+date("m")*100+date("d");

      $thisnow = explode(".", date("d.m.Y", strtotime($rekins['apm_termins'])));
      $thisnow = $thisnow[2]*10000+$thisnow[1]*100+$thisnow[0];

      $apmSumSant = !empty($rekins['apm_summa']) ? (int) ($rekins['apm_summa'] * 100) : 0;
      $needSumSant = !empty($rekins['kopsumma']) ? (int) ($rekins['kopsumma'] * 100) : 0;

      if ($needSumSant == $apmSumSant){

        $apm_statuss_id = 2;

      } elseif (empty($apmSumSant)) {

        if($now > $thisnow){

          $apm_statuss_id = 3;

        } else {

          $apm_statuss_id = 4;

        }

      } else {

        if($now > $thisnow) {

          $apm_statuss_id = 3;

        } else {

          $apm_statuss_id = 5;

        }

      }

    }

  }

  return $apm_statuss_id;

}

function generate_auto_reminders($ligums, $beigu_statuss, $rekini) {

  $rekinu_statuss = get_apmaksas_statuss($rekini);

  if (empty($rekinu_statuss) || empty($ligums['reklama_no']) || empty($ligums['reklama_lidz']) || empty($ligums['google_statuss'])) {
    return 0;
  }

  $c = array();

  $c[1] = array(); // vēl nav
  $c[2] = array(); // aktīvs
  $c[3] = array(); // beidzies

  $c[1][1] = array(); // nav ievadīts
  $c[1][2] = array(); // ievadīts
  $c[1][3] = array(); // aktīvs
  $c[1][4] = array(); // atslēgts

  $c[2][1] = array(); // nav ievadīts
  $c[2][2] = array(); // ievadīts
  $c[2][3] = array(); // aktīvs
  $c[2][4] = array(); // atslēgts

  $c[3][1] = array(); // nav ievadīts
  $c[3][2] = array(); // ievadīts
  $c[3][3] = array(); // aktīvs
  $c[3][4] = array(); // atslēgts


  // Reklāmas termiņš: vēl nav
  // -------------------------

  // Google statuss: nav ievadīts
  $c[1][1][1] = false;
  $c[1][1][2] = false;
  $c[1][1][3] = false;
  $c[1][1][4] = 1;
  $c[1][1][5] = 2;
  $c[1][1][6] = false;

  // Google statuss: ievadīts
  $c[1][2][1] = false;
  $c[1][2][2] = false;
  $c[1][2][3] = false;
  $c[1][2][4] = 1;
  $c[1][2][5] = 2;
  $c[1][2][6] = false;

  // Google statuss: aktīvs
  $c[1][3][1] = 3;
  $c[1][3][2] = 3;
  $c[1][3][3] = 3;
  $c[1][3][4] = array(1, 4);
  $c[1][3][5] = array(4, 2);
  $c[1][3][6] = 4;

  // Google statuss: atslēgts
  $c[1][4][1] = false;
  $c[1][4][2] = false;
  $c[1][4][3] = false;
  $c[1][4][4] = 1;
  $c[1][4][5] = 2;
  $c[1][4][6] = false;


  // Reklāmas termiņš: aktīvs
  // ------------------------

  // Google statuss: nav ievadīts
  $c[2][1][1] = array(5, 6);
  $c[2][1][2] = array(5, 6);
  $c[2][1][3] = array(5, 6);
  $c[2][1][4] = 1;
  $c[2][1][5] = 2;
  $c[2][1][6] = false;

  // Google statuss: ievadīts
  $c[2][2][1] = 6;
  $c[2][2][2] = 6;
  $c[2][2][3] = 6;
  $c[2][2][4] = 1;
  $c[2][2][5] = 2;
  $c[2][2][6] = false;

  // Google statuss: aktīvs
  $c[2][3][1] = false;
  $c[2][3][2] = false;
  $c[2][3][3] = false;
  $c[2][3][4] = array(1, 4);
  $c[2][3][5] = array(4, 2);
  $c[2][3][6] = 4;

  // Google statuss: atslēgts
  $c[2][4][1] = false; // ja līg. statuss "apturēts", tad 6
  $c[2][4][2] = false; // // ja līg. statuss "apturēts", tad 6
  $c[2][4][3] = false; // // ja līg. statuss "apturēts", tad 6
  $c[2][4][4] = 1; //
  $c[2][4][5] = 2;
  $c[2][4][6] = false;


  // Reklāmas termiņš: beidzies
  // --------------------------

  // Google statuss: nav ievadīts
  $c[3][1][1] = false; // 2 vai 7 (sk. tālāk)
  $c[3][1][2] = false; // 2 vai 7 (sk. tālāk)
  $c[3][1][3] = false; // 2 vai 7 (sk. tālāk)
  $c[3][1][4] = 1;  // 2 vai 7 (sk. tālāk)
  $c[3][1][5] = 2;
  $c[3][1][6] = false; // 2 vai 7 (sk. tālāk)

  // Google statuss: ievadīts
  $c[3][2][1] = false; // 2 vai 7 (sk. tālāk)
  $c[3][2][2] = false; // 2 vai 7 (sk. tālāk)
  $c[3][2][3] = false; // 2 vai 7 (sk. tālāk)
  $c[3][2][4] = 1; // 2 vai 7 (sk. tālāk)
  $c[3][2][5] = 2;
  $c[3][2][6] = false; // 2 vai 7 (sk. tālāk)

  // Google statuss: aktīvs
  $c[3][3][1] = 4;
  $c[3][3][2] = 4;
  $c[3][3][3] = 4;
  $c[3][3][4] = array(1, 4);
  $c[3][3][5] = array(4, 2);
  $c[3][3][6] = 4;

  // Google statuss: atslēgts
  $c[3][4][1] = false; // 2 vai 7 (sk. tālāk)
  $c[3][4][2] = false; // 2 vai 7 (sk. tālāk)
  $c[3][4][3] = false;  // 2 vai 7 (sk. tālāk)
  $c[3][4][4] = 1;  // 2 vai 7 (sk. tālāk)
  $c[3][4][5] = false;  // 2 vai 7 (sk. tālāk)
  $c[3][4][6] = false;  // 2 vai 7 (sk. tālāk)

  $rekl_statuss = 0;

  $curdate = date('Y-m-d');

  if ($curdate < $ligums['reklama_no']) {
    $rekl_statuss = 1;
  } else if ($curdate > $ligums['reklama_no'] && $curdate < $ligums['reklama_lidz']) {
    $rekl_statuss = 2;
  } else if ($curdate > $ligums['reklama_lidz']) {
    $rekl_statuss = 3;
  }

  foreach($c as $k1 => $v1) {

    if ($rekl_statuss == $k1) {

      foreach($v1 as $k2 => $v2) {

        if ($ligums['google_statuss'] == $k2) {

          foreach($v2 as $k3 => $atg_veidu_ids) {

            if ($k3 == $rekinu_statuss) {

              if (empty($atg_veidu_ids)) {
                $atg_veidu_ids = array();
              } if (!is_array($atg_veidu_ids)) {
                $atg_veidu_ids = array($atg_veidu_ids);
              }

              if ($k1 == 2 && $k2 == 4 && in_array($k3, array(1, 2, 3))) { // reklāmas termiņs: aktīvs; google statuss: atslēgts

                $kopa = 0;
                $anulets = 0;
                $apmaksats = 0;

                foreach($rekini as $r) {

                  $kopa ++;

                  if (!empty($r['izr_datums'])) {

                    if (!empty($r['anulets'])) {

                      $anulets ++;

                    } else {

                      if(!empty($r['apm_termins'])) {

                        $apmSumSant = !empty($r['apm_summa']) ? (int) ($r['apm_summa'] * 100) : 0;
                        $needSumSant = !empty($r['kopsumma']) ? (int) ($r['kopsumma'] * 100) : 0;

                        if ($needSumSant <= $apmSumSant){

                          $apmaksats ++;

                        }

                      }

                    }

                  }

                }

                if ($anulets > 0 && $apmaksats > 0 && $anulets + $apmaksats == $kopa) {
                  // do nothing
                } else {
                  $atg_veidu_ids[] = 6; // "Pieslēgt pie google"
                }

              }

              if ($k1 == 3 && (($k2 == 1 && in_array($k3, array(1, 2, 3, 4, 6))) || ($k2 == 2 && in_array($k3, array(1, 2, 3, 4, 6))) || ($k2 == 4))) {

                if (empty($beigu_statuss) || ($beigu_statuss['beigu_statuss'] == 'termins')) {
                  $atg_veidu_ids[] = 7;
                } elseif (!empty($beigu_statuss) && $beigu_statuss['beigu_statuss'] == 'atteikums') {
                  $atg_veidu_ids[] = 2;
                }

              }

              if (!empty($atg_veidu_ids)) {

                foreach($atg_veidu_ids as $atgadinajuma_veida_id) {

                  $existing = db_get_assoc(db_query("
                    SELECT id
                    FROM `".DB_PREF."atgadinajumi`
                    WHERE
                      `liguma_id` = ".(int)$ligums['id']." AND
                      `atgadinajuma_veida_id` = ".(int)$atgadinajuma_veida_id." AND
                      `statuss` = 1
                  "));

                  if (empty($existing['id'])) {

                    $veids = db_get_assoc(db_query("
                      SELECT *
                      FROM `".DB_PREF."atgadinajumi_veidi`
                      WHERE id = ".(int)$atgadinajuma_veida_id."
                    "));

                    db_query("
                      INSERT INTO `".DB_PREF."atgadinajumi` SET
                        `liguma_id` = ".(int)$ligums['id'].",
                        `atgadinajuma_veida_id` = ".(int)$atgadinajuma_veida_id.",
                        `datums` = NOW(),
                        `tema` = ".(int)$veids['nokl_tema'].",
                        `saturs` = '".esc($veids['nokl_teksts'])."'
                    ");

                  }

                }

                break 3;

              }

            }

          }

        }

      }

    }

  }


}

function save_file($content, $dest) {

  $h = fopen($dest, 'w');

  if (!$h) return false;

  if (!fwrite($h, $content)) return false;

  fclose($h);

  if (!chmod($dest, 0644)) return false;

  return true;

}

function prepare_filename($str) {

  $str = str_replace(array('/', '\\', '?', '%', '*', ':', '|', '"', '<', '>'), '-', $str);
  $str = preg_replace('/\s{2,}/', ' ', $str);
  $str = preg_replace('/\-{2,}/', '-', $str);

  return $str;

}

function show_autodownload_iframes() {

  if (!empty($_SESSION['autodownload_file_ids'])) {

    foreach($_SESSION['autodownload_file_ids'] as $id) {

      echo '<iframe src="?getfile='.$id.'" style="display:none;"></iframe>';

    }

    unset($_SESSION['autodownload_file_ids']);

  }

}

function get_darb_rek_next_no($prefix) {

  $len = strlen($prefix) + 1;

  $sql = "
    SELECT IFNULL(MAX(CONVERT(SUBSTR(`rek_nr`, ".$len."), SIGNED)), 0) + 1
    FROM `".DB_PREF."pardeveji_rekini`
    WHERE SUBSTR(`rek_nr`, 1, ".($len - 1).") = '".esc($prefix)."'
  ";

  $next_no = db_get_val(db_query($sql));

  return $prefix . $next_no;

}

function print_paginator($total_rows, $active_page, $show_in_page) {

  $total_pages = 1;

  if ($show_in_page > 0) {
    $total_pages = ceil($total_rows / $show_in_page);
  }

  ?>

  <div class="paging">

    <button onclick="$(this).parents('form').find('input[name=page]').val(<?= $active_page - 1 ?>); $(this).parents('form').submit();" class="ui-state-default <?= ($active_page == 1) ? 'ui-state-disabled' : '' ?> ui-corner-all" <?= ($active_page == 1) ? 'disabled' : '' ?>>&lt;&lt;</button>

    <select onchange="$(this).parents('form').find('input[name=page]').val($(this).val()); $(this).parents('form').submit();">
      <? for($i = 1; $i <= $total_pages; $i ++) { ?>
        <option value="<?= $i ?>" <?= ($i == $active_page) ? 'selected="selected"' : '' ?>><?= ($i . '/' . $total_pages) ?></option>
      <? } ?>
    </select>

    <button onclick="$(this).parents('form').find('input[name=page]').val(<?= $active_page + 1 ?>); $(this).parents('form').submit();" class="ui-state-default <?= ($active_page == $total_pages) ? 'ui-state-disabled' : '' ?> ui-corner-all" <?= ($active_page == $total_pages) ? 'disabled' : '' ?>>&gt;&gt;</button>

    &nbsp;&nbsp;Rezultāti lapā:
    <input onchange="$(this).parents('form').find('input[name=show_in_page]').val($(this).val()); $(this).parents('form').find('input[name=page]').val(1); $(this).parents('form').submit();" type="text" style="width: 2em; text-align: center;" value="<?php echo $show_in_page ?>" />

    &nbsp;&nbsp;<a onclick="$(this).parents('form').find('input[name=page]').val(1); $(this).parents('form').find('input[name=show_in_page]').val(''); $(this).parents('form').submit(); return false;" href="#">Visi</a>

  </div>

  <?php

}

function getGet($name) {

  if (isset($_GET[$name])) {
    return $_GET[$name];
  }

  return '';

}

function print_th($title, $col_name = null, $params = array()) {

  $ordering = '';

  if (!empty($col_name)) {
    $ordering = 'class="header" colname="'.$col_name.'" onclick="return changeTableOrder(this, \''.$col_name.'\');"';
  }

  ?>

    <th <?= !empty($params['width']) ? 'width="'.$params['width'].'"' : '' ?> <?= $ordering ?>><?= $title ?></th>

  <?php

}

function to_db_date($date) {
  return date('Y-m-d', strtotime($date));
}

function get_nenomainitie_statusi($filter = array()) {

  static $cache = array();

  $key = serialize($filter);

  if (!empty($cache[$key])) {
    return $cache[$key];
  }

  $cur_date = date('Y-m-d');

  $query = db_query("
    SELECT r.*
    FROM `".DB_PREF."rekini` r
    LEFT JOIN `".DB_PREF."ligumi` g ON (g.id = r.liguma_id)
    WHERE g.reklama_lidz <= '".$cur_date."'
  ");

  $rekini = array();
  while($row = db_get_assoc($query)) {
    $rekini[$row['liguma_id']][] = $row;
  }

  $query = db_query("
    SELECT
      g.*,
      b.beigu_statuss
    FROM `".DB_PREF."ligumi` g
    LEFT JOIN `".DB_PREF."ligumi_beigusies` b ON (g.id = b.liguma_id)
    WHERE
      ".(!empty($filter['pardeveja_id']) ? 'g.pardeveja_id = ' . (int) $filter['pardeveja_id'] . ' AND' : '')."
      g.reklama_lidz <= '".$cur_date."' AND
      (
        b.id IS NULL OR
        (
          b.beigu_statuss = 'termins' AND
          b.atlikt_lidz <= '".$cur_date."'
        )
      )
    ORDER BY
      IF (b.beigu_statuss IS NOT NULL, b.atlikt_lidz, g.reklama_lidz) ASC
  ");

  $cache[$key] = array();

  while($row = db_get_assoc($query)) {

    $lig_statuss = get_liguma_statuss($row, isset($rekini[$row['id']]) ? $rekini[$row['id']] : null);

    if ($lig_statuss == 6) { // beidzies
      $cache[$key][] = $row;
    }

  }

  return $cache[$key];

}
?>