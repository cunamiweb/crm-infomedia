<?php
include("inc/init.php");
include("inc/glob_actions.php");

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Infomedia</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<link rel="StyleSheet" href="css/style.css?2" type="text/css">
<link type="text/css" href="css/ui-lightness/jquery-ui-1.7.2.custom.css" rel="stylesheet" />
<script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.7.2.custom.min.js"></script>
<script type="text/javascript" src="js/jquery.tablesorter.min.js"></script>
<script type="text/javascript" src="js/jstorage.min.js"></script>
<script type="text/javascript" src="js/scripts.js?2"></script>
<? if ($_SESSION['login']) { ?>
<script type="text/javascript">init_auto_logout()</script>
<? } ?>
</head>
<body>

<?php
if (!$_SESSION['login']){
  include("modules/login.php");
} else {
  include("modules/main.php");
}
?>

<?php if (DEBUG_QUERIES) { ?>
  <div>
  <?php foreach($GLOBALS['executed_queries'] as $v) { ?>
    <p>
      <strong>SQL:</strong>: <pre><?php echo $v['sql']; ?></pre>
      <strong>Exec. time:</strong> <?php echo $v['time']; ?><br />
    </p>
  <?php } ?>
  </div>
  <p><strong>Total time:</strong> <?php echo $GLOBALS['query_total_time']; ?></p>
<?php } ?>

</body>
</html>
