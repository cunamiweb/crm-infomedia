<?php
include("inc/config.php");

function esc($str) {
  global $db;
  return mysql_real_escape_string($str, $db);
}

function to_date($date) {

  return date('Y-m-d', strtotime($date));

}

$db = mysql_connect(DB_HOST, DB_USER, DB_PASS) or die("Neizdevas pieslegties datubazei!");

mysql_select_db(DB_NAME, $db) or die("Neatradu datubaazi!");

mysql_query("SET NAMES utf8", $db);

mysql_query("TRUNCATE TABLE new_atgadinajumi;");
mysql_query("TRUNCATE TABLE new_kontakti;");
mysql_query("TRUNCATE TABLE new_ligumi;");
mysql_query("TRUNCATE TABLE new_ligumi_rekinu_piegades;");
mysql_query("TRUNCATE TABLE new_pakalpojumi;");
mysql_query("TRUNCATE TABLE new_pakalpojumi_veidi;");
mysql_query("TRUNCATE TABLE new_pielikumi_infomedia;");
mysql_query("TRUNCATE TABLE new_pielikumi_tiessaite;");
mysql_query("TRUNCATE TABLE new_rekini;");
mysql_query("TRUNCATE TABLE new_rekini_maksajumi;");
mysql_query("TRUNCATE TABLE new_faili;");



// inserting users

mysql_query("TRUNCATE TABLE new_pardeveji;");
mysql_query("INSERT INTO new_pardeveji (id, vards, lietotajvards) SELECT id, vards, id FROM infomedia__pardeveji;");


// inserting files

mysql_query("
INSERT INTO new_faili (
  id, objekta_id, objekta_tips, fails, izmers, created
) SELECT
  id,
  did,
  CASE `type`
    WHEN 1 THEN 'lig_klients'
    WHEN 2 THEN 'lig_pakalpojums'
    WHEN 3 THEN 'lig_rekini'
    WHEN 4 THEN 'lig_atgadinajumi'
  END,
  filename,
  filesize,
  created
FROM
  infomedia__faili
");


// inserting new_pakalpojumi_veidi

$pakalpojumi = array(
  1=>"Komplekts Nr.1",
  2=>"Komplekts Nr.1a",
  3=>"Komplekts Nr.2",
  4=>"Komplekts Nr.3",
  5=>"Komplekts Nr.4",
  6=>"Garantētā pozīcija",
  7=>"Banneris augšā",
  8=>"Banneris sānā",
  9=>"Google atslēgvārdi",
  10=>"Filiāles",
  11=>"Pamata profils",
  12=>"Logotips",
  13=>"Foto galerija",
  14=>"Video klips",
  15=>"Uzpeldošais uzraksts",
  16=>"Īpaša atzīme kartē",
  17=>"Saite ar mājaslapu",
  18=>"Preču zīmes",
  19=>"Papildus kontakti",
  20=>"Atslēgvārdu rindas",
  21=>"Tiešsaite",
  22=>"Tiešsaite ar garantēto pozīciju",
  23=>"Google karte",
  24=>"Cits",
);

foreach($pakalpojumi as $id => $nosaukums) {

  $sql = "
    INSERT INTO new_pakalpojumi_veidi (
      id,
      nosaukums
    ) VALUES (
      ".$id.",
      '".esc($nosaukums)."'
    )
  ";

  mysql_query($sql) or die(mysql_error());

}


$r = mysql_query("SELECT * FROM infomedia__data ORDER BY id ASC");

while($contract = mysql_fetch_assoc($r)) {

  $liguma_id = (int)$contract['id'];

  $data = unserialize($contract['data']);

  if (empty($data)) {
    echo $liguma_id . ' - empty $data' . "\n";
    continue;
  }

  // inserting new_ligumi

  $pvn = !empty($data['pvn']) ? floatval(str_replace(',', '.', $data['pvn'])) : 'null';

  $sql = "
    INSERT INTO new_ligumi (
      id,
      pardeveja_id,
      ligumanr,
      ligumadat,
      nosaukums,
      regnr,
      faktaddr_city,
      faktaddr_street,
      faktaddr_index,
      juridaddr_city,
      juridaddr_street,
      juridaddr_index,
      infomedia_statuss,
      google_statuss,
      pak_google_konts,
      infomedia_reklamas_mat,
      tiessaite_reklamas_mat,
      sanemsanas_forma,
      sanemsanas_datums,
      pvn,
      reklama_no,
      reklama_lidz,
      pak_nosaukums_db,
      created
    ) VALUES (
      ".$liguma_id.",
      ".(int) $data['pardevejs'].",
      '".esc($contract['lignr'])."',
      '".date('Y-m-d', $contract['ligdat'])."',
      '".esc($data['nosaukums'])."',
      '".esc(isset($data['regnr']) ? $data['regnr'] : '')."',
      '".esc(isset($data['faktaddr_city']) ? $data['faktaddr_city'] : '')."',
      '".esc(isset($data['faktaddr_street']) ? $data['faktaddr_street'] : '')."',
      '".esc(isset($data['faktaddr_index']) ? $data['faktaddr_index'] : '')."',
      '".esc(isset($data['juridaddr_city']) ? $data['juridaddr_city'] : '')."',
      '".esc(isset($data['juridaddr_street']) ? $data['juridaddr_street'] : '')."',
      '".esc(isset($data['juridaddr_index']) ? $data['juridaddr_index'] : '')."',
      ".(isset($data['infomedia_statuss']) ? (int) $data['infomedia_statuss'] : 1).",
      ".(isset($data['google_statuss']) ? (int) $data['google_statuss'] : 1).",
      '".esc(isset($data['pak_google_konts']) ? $data['pak_google_konts'] : '')."',
      ".(isset($data['infomedia_reklamas_mat']) ? (int) $data['infomedia_reklamas_mat'] : 1).",
      ".(isset($data['tiessaite_reklamas_mat']) ? (int) $data['tiessaite_reklamas_mat'] : 1).",
      ".(isset($data['sanemsanas_forma']) ? (int) $data['sanemsanas_forma'] : 1).",
      ".(!empty($contract['ligdat']) ? "'" . date('Y-m-d', $contract['ligdat']) . "'" : 'null').",
      ".$pvn.",
      ".(!empty($data['rekno']) ? "'" . esc(to_date($data['rekno'])) . "'" : 'null').",
      ".(!empty($data['reklidz']) ? "'" . esc(to_date($data['reklidz'])) . "'" : 'null').",
      '".esc(isset($data['pak_nosaukums_db']) ? $data['pak_nosaukums_db'] : '')."',
      '".esc($contract['created'])."'
    )
  ";

  mysql_query($sql) or die(mysql_error());

  // inserting new_atgadinajumi

  if (!empty($data['atg_statuss'])) {

    foreach($data['atg_statuss'] as $i => $atg_status) {

      $atg_date = $data['atg_date'][$i];
      $atg_tema = $data['atg_tema'][$i];
      $atg_saturs = $data['atg_saturs'][$i];
      $atg_created = $data['atg_created'][$i];
      $atg_pardevejs = $data['atg_pardevejs'][$i];

      $status = 1;
      $status = isset($data['atgadinajumi_done'][$i]) ? 2 : $status;
      $status = isset($data['atgadinajumi_cancel'][$i]) ? 3 : $status;

      $sql = "
        INSERT INTO new_atgadinajumi (
          liguma_id,
          pardeveja_id,
          statuss,
          datums,
          tema,
          saturs,
          created,
          created_pardeveja_id
        ) VALUES (
          ".$liguma_id.",
          null,
          ".$status.",
          '".to_date($atg_date)."',
          ".(int) $atg_tema.",
          '".esc($atg_saturs)."',
          '".esc(to_date($atg_created))."',
          ".(int) $atg_pardevejs."
        )
      ";

      mysql_query($sql) or die(mysql_error());

    }

  }

  // inserting new_kontakti

  if (!empty($data['kontakti'])) {

    $contacts = array();
    $c = 0;

    foreach($data['kontakti'] as $i => $item) {

      if (isset($item['name'])) $c ++;

      $contacts[$c][key($item)] = $item[key($item)];

    }

    foreach($contacts as $contact) {

      $ligumsledzejs = isset($contact['check']) ? 1 : 0;

      $sql = "
        INSERT INTO new_kontakti (
          liguma_id,
          vards,
          telefons,
          epasts,
          ligumsledzejs
        ) VALUES (
          ".$liguma_id.",
          '".esc($contact['name'])."',
          '".esc($contact['phone'])."',
          '".esc($contact['mail'])."',
          ".$ligumsledzejs."
        )
      ";

      mysql_query($sql) or die(mysql_error());

    }

  }

  // inserting new_pakalpojumi

  if (!empty($data['pakalpojums_chose'])) {

    foreach($data['pakalpojums_chose'] as $i => $service_id) {

      $pakalpojums_apr = $data['pakalpojums_apr'][$i];

      $sql = "
        INSERT INTO new_pakalpojumi (
          liguma_id,
          pakalpojuma_veida_id,
          pakalpojuma_apr
        ) VALUES (
          ".$liguma_id.",
          ".(int) $service_id.",
          '".esc($pakalpojums_apr)."'
        )
      ";

      mysql_query($sql) or die(mysql_error());

    }

  }

  // inserting new_pielikumi_infomedia

  if (!empty($data['piel_infomedia'])) {

    foreach($data['piel_infomedia'] as $atslegvards) {

      $sql = "
        INSERT INTO new_pielikumi_infomedia (
          liguma_id,
          atslegvards
        ) VALUES (
          ".$liguma_id.",
          '".esc($atslegvards)."'
        )
      ";

      mysql_query($sql) or die(mysql_error());

    }

  }

  // inserting new_pielikumi_tiessaite

  if (!empty($data['piel_tiessaite'])) {

    foreach($data['piel_tiessaite'] as $atslegvards) {

      $sql = "
        INSERT INTO new_pielikumi_tiessaite (
          liguma_id,
          atslegvards
        ) VALUES (
          ".$liguma_id.",
          '".esc($atslegvards)."'
        )
      ";

      mysql_query($sql) or die(mysql_error());

    }

  }

  // insert new_ligumi_rekinu_piegades

  if (!empty($data['piegade'])) {

    foreach($data['piegade'] as $piegades_veids => $v) {

      $adrese = $data['piegade_' . $piegades_veids];

      $sql = "
        INSERT INTO new_ligumi_rekinu_piegades (
          liguma_id,
          piegades_veids,
          adrese
        ) VALUES (
          ".$liguma_id.",
          ".(int)$piegades_veids.",
          '".esc($adrese)."'
        )
      ";

      mysql_query($sql) or die(mysql_error());

    }

  }

  // inserting new_rekini

  if (!empty($data['izrdat'])) {

    $nr = 0;

    foreach($data['izrdat'] as $i => $izrdat) {

      $nr ++;

      if (count($data['rekstat']) == 1) {
        $rek_nr = $contract['lignr'];
      } else {
        $rek_nr = $contract['lignr'] . '/' . $nr;
      }

      $rekstat = $data['rekstat'][$i];
      $summ = str_replace(',', '.', $data['summ'][$i]);
      $kpsumm = str_replace(',', '.', $data['kpsumm'][$i]);
      $apmtermins = $data['apmdat'][$i];
      $apmdat = isset($data['apmaksaja'][$i]) ? $data['apmaksaja'][$i] : null;
      $apmsum = str_replace(',', '.', $data['apmsum'][$i]);
      $anuldat = isset($data['anuldat'][$i]) ? $data['anuldat'][$i] : null;

      $anulets = isset($data['anulets'][$i]) ? 1 : 0;

      $sql = "
        INSERT INTO new_rekini (
          liguma_id,
          rek_nr,
          izr_datums,
          statuss,
          summa,
          kopsumma,
          apm_termins,
          apm_summa,
          anulets,
          anul_datums
        ) VALUES (
          ".$liguma_id.",
          '".esc($rek_nr)."',
          ".(!empty($izrdat) ? "'" . esc(to_date($izrdat)) . "'" : 'null').",
          ".(int)$rekstat.",
          ".(is_numeric($summ) ? floatval($summ) : 'null').",
          ".(is_numeric($kpsumm) ? floatval($kpsumm) : 'null').",
          ".(!empty($apmtermins) ? "'" . esc(to_date($apmtermins)) . "'" : 'null').",
          ".(is_numeric($apmsum) ? floatval($apmsum) : 'null').",
          ".$anulets.",
          ".(!empty($anuldat) ? "'" . esc(to_date($anuldat)) . "'" : 'null')."
        )
      ";

      mysql_query($sql) or die(mysql_error());

      $rek_id = mysql_insert_id();

      if ($apmsum > 0) {

        $apmdat = !empty($apmdat) ? $apmdat : $izrdat;

        $sql = "
          INSERT INTO new_rekini_maksajumi (
            rekina_id,
            datums,
            summa
          ) VALUES (
            ".$rek_id.",
            ".(!empty($apmdat) ? "'" . esc(to_date($apmdat)) . "'" : 'null').",
            ".(is_numeric($apmsum) ? floatval($apmsum) : 'null')."
          )
        ";

        mysql_query($sql) or die(mysql_error());

      }

    }

  }



}
?>