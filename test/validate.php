<?php
include("inc/init.php");

if (!$_SESSION['login']) {
  die();
}

$actions = array(
  'ligums' => 'validate_ligums_form',
  'darbinieks' => 'validate_darbinieks_form',
  'atgadinajumi' => 'validate_atgadinajumi_form',
  'forums_tema' => 'validate_forums_tema_form',
  'forums_atbilde' => 'validate_forums_atbilde_form',
  'darb_kopsavilkums' => 'validate_darb_kopsavilkums_form',
  'lig_kopsavilkums' => 'validate_lig_kopsavilkums_form',
  'beigu_statuss' => 'validate_beigu_statuss_form'
);

if (!empty($_GET['action']) && isset($actions[$_GET['action']])) {

  $errors = $actions[$_GET['action']]($_POST);

  if (!empty($errors)) {
    echo Zend_Json::encode(array('errors' => $errors));
  } else {
    echo Zend_Json::encode(array('success' => 1));
  }

}
?>