<?php
/**
 * Skripts, kas atjauno cpc  visiem standarta piedāvājumiem.
 * Datu iegūšanai izmanto Adwords API
 *
 * @since 2012.07.24.
 * @author Dāvis Frīdenvalds davis@datateks.lv
*/

include('inc/init.php');

set_time_limit(36000);

$mekletaji = Mekletajs::model()->findAll();

foreach($mekletaji as $mekletajs) {

  $date = date('Y-m-d H:i:s', mktime(0,0,0, date('m'), date('d') - ADWORDS_REFRESH_RATE, date('Y')));

  $sql = '
    SELECT a.*, MAX(h.datums) AS pedejais_datums, h.statuss
    FROM ' . DB_PREF . 'piedavajumi_av a
    LEFT JOIN ' . DB_PREF . 'piedavajumi_av_cpc_vesture h ON h.atslegvards = a.atslegvards AND h.mekletajs_id = a.mekletajs_id
    WHERE
      a.tips = "std" AND
      a.mekletajs_id = ' . $mekletajs->id . '
    GROUP BY a.atslegvards
    HAVING ((h.statuss != 1 OR h.statuss IS NULL) OR (pedejais_datums IS NULL OR pedejais_datums < "'.$date.'"))
  ';

  $rows = db_get_assoc_all(db_query($sql));

  $models = StandartaPiedavajums::model()->findAll(array('conditions' => array('mekletajs_id = ' . $mekletajs->id)));

  //  Old prices
  $old = array();
  foreach($models as $model) {
      $old[$model->atslegvards] = $model->priceArray();
  }

  $limit = 500; //by agris changed from 2000 to 500

  $batches= array_chunk($rows, $limit);


  //  CPC
  if(!empty($batches)) {

    $adwords = new Adwords;

    $country_codes = array($mekletajs->getValsts()->country_code);
    foreach($batches as $atslegvardi) {

      $avs = array();
      foreach($atslegvardi as $atslegvards) {
        $avs[] = $atslegvards['atslegvards'];
      }
      $avs = array_unique($avs);
      
      try {
        //by agris 27.12.2018 add $country_codes to language codes becauses missing argument

        $result = $adwords->getCostsEstimate($avs, $country_codes,$country_codes);

        foreach($avs as $atslegvards) {
          $status = 0;
          if($result && isset($result[$atslegvards])) {
            $cpc = $result[$atslegvards]['average_cpc']; // In adwords account currency
            //$cpc = ADWORDS_CURRENCY != 'LVL' ? convert_currency($result[$atslegvards]['average_cpc'], ADWORDS_CURRENCY, 'LVL') : $result[$atslegvards]['average_cpc'];
            $clicks = $result[$atslegvards]['clicks'];

            $sql = '
              UPDATE ' . DB_PREF . 'piedavajumi_av
              SET
                google_cpc = ' . $cpc . ',
                google_clicks = ' . $clicks . '
              WHERE
                tips = "std" AND
                atslegvards = "'.$atslegvards.'" AND
                mekletajs_id = ' . $mekletajs->id;

            if(db_query($sql)) {
              $status = 1;

              //  Create model and log the price change
              $model = StandartaPiedavajums::model()->find(array('conditions' => array(
                'atslegvards = "'.$atslegvards.'"',
                'mekletajs_id = ' . $mekletajs->id,
              )));
              $model->logPriceChange('google', isset($old[$atslegvards]) ? $old[$atslegvards] : null);

            }

            $adwords->addCpcLog($atslegvards, $mekletajs->id, $cpc, $clicks, $status);
          } else {
            $adwords->addCpcLog($atslegvards, $mekletajs->id, null, null, $status);
          }
        }
      } catch (Exception $e) {
        foreach($avs as $atslegvards) {
          $adwords->addCpcLog($atslegvards, $mekletajs->id, null, null, -2, $e->getMessage());
        }
      }
    }
  }
}



/*$atslegvardi = StandartaPiedavajums::model()->findAll();

$batches_searches = array();
$batches_cpc = array();

if(!empty($atslegvardi)) {
  $bsi = 0;
  $bci = 0;
  $limit = 2000;
  foreach($atslegvardi as $atslegvards) {

    //  Searches
    if(isOutdated($atslegvards->getLastSearchesUpdate())) {

      $country_key = $atslegvards->getMekletajs()->getValsts()->id;
      if(isset($batches_searches[$country_key][$bsi])) {
        if(count($batches_searches[$country_key][$bsi]) == $limit) {
          $bsi++;
          $batches_searches[$country_key][$bsi] = array($atslegvards);
        } else {
          $batches_earches[$country_key][$bsi][] = $atslegvards;
        }
      } else {
        $batches_searches[$country_key][$bsi] = array($atslegvards);
      }
    }

    //  CPC
    if(isOutdated($atslegvards->getLastCPCUpdate())) {
      //  Liekam updeitojamo sarakstā. Jādala grupās pēc valstīm un to grupām,
      // jo uz adwords vienā pieprasījumā padodot valstu filtru, tas attiecas uz visiem av kuru info tajā tiek prasīta
      $country_key = $atslegvards->getMekletajs()->getValsts()->id;
      if(isset($batches_cpc[$country_key][$bci])) {
        if(count($batches_cpc[$country_key][$bci]) == $limit) {
          $bci++;
          $batches_cpc[$country_key][$bci] = array($atslegvards);
        } else {
          $batches_cpc[$country_key][$bci][] = $atslegvards;
        }
      } else {
        $batches_cpc[$country_key][$bci] = array($atslegvards);
      }
    }
  }
}
die('sss');
//  Meklējumi
if(!empty($batches_searches)) {

  $adwords = new Adwords;

  foreach($batches_searches as $country_id => $atslegvardi_sets) {
    $country_codes = array(Valsts::model()->findByPk($country_id)->country_code);

    foreach($atslegvardi_sets as $atslegvardi) {
      try {
        $status = 0;
        $result = $adwords->getMonthlySearches(modelsToList($atslegvardi, 'atslegvards'), $country_codes);

        foreach($atslegvardi as $atslegvards) {
          if($result && isset($result[$atslegvards->atslegvards])) {
            $atslegvards->meklejumi = $result[$atslegvards->atslegvards];
            if($atslegvards->save()) {
              $status = 1;
            }

            db_query('INSERT INTO ' . DB_PREF . 'piedavajumi_av_meklejumi_vesture (av_id, meklejumi, datums, statuss) VALUES ('.$atslegvards->id.', '.$result[$atslegvards->atslegvards].', "'.date('Y-m-d H:i:s').'", '.$status.')');
          } else {

            db_query('INSERT INTO ' . DB_PREF . 'piedavajumi_av_meklejumi_vesture (av_id, datums, statuss) VALUES ('.$atslegvards->id.', "'.date('Y-m-d H:i:s').'", -1)');
          }
        }
      } catch (Exception $e) {
        foreach($atslegvardi as $atslegvards) {
          db_query('INSERT INTO ' . DB_PREF . 'piedavajumi_av_meklejumi_vesture (av_id, datums, statuss) VALUES ('.$atslegvards->id.', "'.date('Y-m-d H:i:s').'", -2)');
        }
      }
    }
  }
}

//  CPC
if(!empty($batches_cpc)) {

  $adwords = new Adwords;
  foreach($batches_cpc as $country_id => $atslegvardi_sets) {
    $country_codes = array(Valsts::model()->findByPk($country_id)->country_code);

    foreach($atslegvardi_sets as $atslegvardi) {
      $status = 0;
      try {
        $result = $adwords->getCostsEstimate(modelsToList($atslegvardi, 'atslegvards'), $country_codes);

        foreach($atslegvardi as $atslegvards) {
          if($result && isset($result[$atslegvards->atslegvards])) {
            $cpc = $result[$atslegvards->atslegvards]['average_cpc']; // In adwords account currency
            //$cpc_converted = ADWORDS_CURRENCY != 'LVL' ? convert_currency($cpc, ADWORDS_CURRENCY, 'LVL') : $cpc;
            //$atslegvards->google_cpc = $cpc_converted;
            $atslegvards->google_cpc = $cpc;

            $atslegvards->google_clicks = $result[$atslegvards->atslegvards]['clicks'];
            if($atslegvards->save()) {
              $status = 1;
            }

            db_query('INSERT INTO ' . DB_PREF . 'piedavajumi_av_cpc_vesture (av_id, cpc, clicks, datums, statuss) VALUES ('.$atslegvards->id.', '.$cpc_converted.', '.$result[$atslegvards->atslegvards]['clicks'].', "'.date('Y-m-d H:i:s').'", '.$status.')');
          } else {
            db_query('INSERT INTO ' . DB_PREF . 'piedavajumi_av_cpc_vesture (av_id, datums, statuss) VALUES ('.$atslegvards->id.', "'.date('Y-m-d H:i:s').'", -1)');
          }
        }
      } catch (Exception $e) {
        foreach($atslegvardi as $atslegvards) {
          db_query('INSERT INTO ' . DB_PREF . 'piedavajumi_av_cpc_vesture (av_id, datums, statuss) VALUES ('.$atslegvards->id.', "'.date('Y-m-d H:i:s').'", -2)');
        }
      }
    }
  }
}

function isOutdated($last_update)
{
  $needs_update = false;

  if($last_update) {
    if($last_update['statuss'] != 1) {
      //  Iepriekšējais update bijis neveiksmīgs
      $needs_update = true;
    }

    $ts = strtotime($last_update['datums']);
    //  Laiks kurā būtu jābūt nākamajam update
    $next_update = mktime(date('H', $ts) - 1, date('i', $ts), 0, date('m', $ts), date('d', $ts) + ADWORDS_REFRESH_RATE, date('Y', $ts)); // -1h, citādi automatizējot un izpildot šo skriptu vienā pulksteņlaikā, sanāktu ka tikai reizi divos mēģinājumos izpildītos, jo laiks tiek piefiksēts izpildes beigās.


    if($next_update <= time()) {
      $needs_update = true;
    }

  } else {
    //  Vēl nav bijis update
    $needs_update = true;
  }

  return $needs_update;
} */

?>