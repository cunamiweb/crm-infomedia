<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
</head>
<?php
/**
 * Skripts kas veic prēmiju aprēķinus ik ceturksni.
 *
 * Domāts darbināt ar CRON.
 * Rezultāts nav atkarīgs no skripta izpildes datuma, jo tiek fiksēts kam, par kādu
 * laika posmu ticis aprēķināts.
 * Tāpēc CRON perioda uzstādījums nav kritiski svarīgs.
 * Šodienas datums interesē tikai tiktāl, lai apskatītos par kādiem ceturkšņiem
 * vispār jārēķina prēmijas šobrīd.
 *
 * Prēmijas tiek rēķinātas sākot ar 2011. gada 4. ceturksni
 *
 * 17.01.2013. Noteiktos datumu intervālos darbojas nu jau 3 dažādas prēmiju aprēķināšanas metodes.
 *
 * @author Dāvis Frīdenvalds davis@datateks.lv
 * @since 29.02.2012.
 */

ini_set('max_execution_time', 3600);

include("inc/init.php");

//  Uzģenerē ceturkšņu sarakstu
$year = 2011; // Sākuma gads
$month = 10; // Sākuma mēnesis
$i_date = mktime(0,0,0,$month, 1, $year); // Sākuma datuma timestamp

$current_date = mktime(0,0,0,date('m'), 1, date('Y'));
//$current_date = strtotime(date('Y-m')); // Šī mēneša timestamp

$quarters = array();

while($current_date > $i_date) { // Kamēr šis mēnesis ir lielāks par skaitāmo mēnesi

  //  Ja 3 modulis no mēneša numura ir nulle tad tas ir ceturkšņa pēdējais mēnesis
  if(date('m', $i_date) % 3 == 0) {

    //  Liekam ceturkšņu sarakstā
    $quarters[] = array(
      'year' => date('Y', $i_date),
      'quarter' => date('m', $i_date) / 3,
    );
  }

  $i_date = mktime(0,0,0,date('m', $i_date) + 1, 1, date('Y', $i_date));
}

if(empty($quarters)) {
  die('No quarters.');
}

var_dump($quarters);

//  Savāc visu pārdevēju sarakstu. Tikai tie kam tips ir sales un aktīvs
$result = db_query('
  SELECT p.* FROM `' . DB_PREF . 'pardeveji` p
  WHERE p.tips = "sales" AND p.aktivs = 1
');

//  Apgrozījuma nosacījumi [versija] = apgrozījums
$conditions = array(
  '1' => '1.1',
  '2' => '1.05',
  '3' => '1',
);
var_dump(array('nosacījumu matrica' => $conditions));

//  Prēmiju matricas - jaunajām sistēmām
$premium_matrix = array(
  '2' => array('1.05' => 2, '1.1' => 3, '1.15' => 4, '1.2' => 5, '1.3' => 6),
  '22' => array('1.05' => 1, '1.1' => 2, '1.15' => 3, '1.2' => 4, '1.3' => 5), // otrās sistēmas otrais variants
  '3' => array('1' => 1, '1.01' => 2, '1.02' => 3, '1.03' => 5),
);
var_dump(array('prēmiju matrica' => $premium_matrix));


//  Pārbauda katru pārdevēju vai tam ir sarēķinātas prēmijas par visiem ceturkšņiem
while($merchant = db_get_assoc($result)) {
  //  Katrs pārdevējs

  echo '<br />********************************************<br />';

  var_dump(array('id' => $merchant['id'], 'name' => $merchant['vards']));

  foreach($quarters as $quarter) {

    //  Katrs ceturksnis

    //echo '<h1>' . $merchant['vards'] . ' ' . implode('-', $quarter). '</h1>';

    echo '<br />---------------------------------------------<br />';
    var_dump($quarter['year'] . '-' . $quarter['quarter']);

    $query = '
      SELECT 1
      FROM `' . DB_PREF . 'premijas`
      WHERE pardevejs_id = ' . $merchant['id'] . ' AND gads = ' . $quarter['year'] . ' AND ceturksnis = ' . $quarter['quarter'];
    $existing_premium = db_get_assoc(db_query($query));

    //  Šis bloks ir par to ka pēc prēmijas izrēķināšas vēl vienu mēnesi ir "pārbaudes laiks" kad viņš viss var mainīties.
    if(!$existing_premium) {

      var_dump('Vēl prēmija nav bijusi rēķināta.');

      //var_dump('nav vēl prēmija rēķināta');
      $is_premium_calculated = false;

      db_query('
          DELETE FROM `' . DB_PREF . 'izpilde_history`
          WHERE pardevejs_id = '.esc($merchant['id']).' AND gads = ' . $quarter['year'] . ' AND menesis BETWEEN ' . ((int)($quarter['quarter'] * 3) - 2) .' AND ' . $quarter['quarter'] * 3);

    } else {

      var_dump('Ir rēķināta prēmija.');
      //  Aiznākamā mēneša (aiz šī cetukšņa pēdējā mēneša) pirmā diena. tātad ja tas ir 4. ceturksnis tad mums vajag pirmo februāri dabūt.
      //$dynamic_till =  mktime(0, 0, 0, $quarter['quarter'] * 3 + 2, 1, $quarter['year']);  // + 2
      //var_dump('dinamic_till ' . date('Y-m-d', $dynamic_till));

      if(!is_quarter_fixed($quarter['year'], $quarter['quarter'])) {

        //var_dump('vēl nav nogatavojies. dzēšam laukā.');
        db_query('
          DELETE FROM `' . DB_PREF . 'premijas`
          WHERE pardevejs_id = ' . $merchant['id'] . ' AND gads = ' . $quarter['year'] . ' AND ceturksnis = ' . $quarter['quarter']);

       db_query('
          DELETE FROM `' . DB_PREF . 'izpilde_history`
          WHERE pardevejs_id = '.esc($merchant['id']).' AND gads = ' . $quarter['year'] . ' AND menesis BETWEEN ' . ((int)($quarter['quarter'] * 3) - 2) .' AND ' . $quarter['quarter'] * 3);

        $is_premium_calculated = false;

      } else {
        //var_dump('ir nogatavojies. vairs neko nevajag rēķināt.');
        $is_premium_calculated = true;
      }
    }

    //  Ja nav tad sarēķina
    if(!$is_premium_calculated) {

      var_dump('NAV fiksējusies');

      $is_new_sys = is_new_premium_sys(array('year'=>$quarter['year'], 'quarter'=>$quarter['quarter']));
      $is_new2_sys = is_new2_premium_sys(array('year'=>$quarter['year'], 'quarter'=>$quarter['quarter']));

      $sys_version = $is_new2_sys ? 3 : ($is_new_sys ? 2 : 1); // Sistēmas versija. 1 - vecā, 2 - otrā, 3 - trešā (jaunākā)

      var_dump('Sistēmas versija: ' . $sys_version);

      //var_dump('rēķināsim');
      $qData = get_izpildits($merchant['id'], $quarter['year'],  $quarter['quarter']);

      var_dump('Apgrozījuma nosacījums: ' .  $conditions[$sys_version]);

      var_dump(array(
          'skaits' => array('ir' => $qData['kopa']['skaits'], 'vajag' => $qData['atdoti']['skaits']),
          'apgrozījums' => array('ir' => $qData['kopa']['apgr'], 'vajag' => $qData['atdoti']['apgr'], 'attieciba' => get_apgrozijuma_attiecibu($qData)),
        )
      );

      //  Vai izpildās ceturkšņa nosacījums: noslēgti vismaz tik līgumi cik plānoti un sasniegtā summa vismaz x% no plānotās.
      if(is_premium_condition_met($qData, $conditions[$sys_version])) {
        //  Izpildās. Tālāk skatāmies par katru mēnesi, lai iegūtu prēmijas procentus

        var_dump('Izpildās nosacījums (ceturkšņa)');

        $premium = 0;

        $amats = get_amats($merchant['id'], $quarter['year'], $quarter['quarter']);
        //var_dump($merchant);
        //var_dump($amats);
        //var_dump($amats['premija']);
        //var_dump($amats['premija'] === NULL);

        if(($amats && $amats['premija'] === NULL) || $sys_version == 3) {
          //  Ir amats un nav norādīta stingrā prēmija vai 3. versija
          var_dump('(IR amats un NAV norādīta stingrā prēmija) vai 3. versija');

          if($sys_version > 1) { // tikai abām jaunajām sistēmām

            $premium = determine_premium_new_sys($qData, $premium_matrix[$sys_version]);

          } else {

            //  Mēneša nosacījums: mēnesī noslēgti līgumi vismaz tikpat cik plānoti un summa sasniegtā ir vismaz 110% no plānotās.
            foreach(range(0,2) as $mi) {

              $month = (int)$quarter['quarter'] * 3 - $mi; // Mēneša numurus iegūst no ceturksnis * 3 - {2, 1, 0}

              $mData = get_izpildits($merchant['id'], $quarter['year'], null, $month);

              db_query('
                INSERT INTO `' . DB_PREF . 'izpilde_history`
                (pardevejs_id, gads, menesis, kopa_apgr, kopa_skaits, atdoti_apgr, atdoti_skaits, kopa_ligumu_ids, atdoti_ligumu_ids, kopa_ligumu_ids_all)
                VALUES ('.esc($merchant['id']).',
                  '.$quarter['year'].',
                  '.$month.',
                  '.$mData['kopa']['apgr'].',
                  '.$mData['kopa']['skaits'].',
                  '.$mData['atdoti']['apgr'].',
                  '.$mData['atdoti']['skaits'].',
                  "'.implode(',', $mData['kopa']['ligumu_id']).'",
                  "'.implode(',', $mData['atdoti']['ligumu_id']).'",
                  "'.implode(',', $mData['kopa']['ligumu_id_all']).'"
                )');

              if(is_premium_condition_met($mData)) {
                //  Izpildās. Prēmijas procenti + 5%
                $premium += 5;
              }
              //var_dump($premium);
            }
          }
        } elseif(!$amats || empty($amats)) {

          //  Nav amata
          var_dump('NAV amata');

          $premium = 0;

        } else {

          //  Ir amats un ir norādīta  prēmija - šeit varētu nonākt tikai vecās vai pirmās jaunās sistēmas gadījumos
          var_dump('IR amats un IR norādīta prēmija');

          if($sys_version == 2) {

            //  Pirmajā jaunajā sistēmā ja ir amats ar norādītu prēmiju tad skatās prēmiju pēc citas matricas
            $premium = determine_premium_new_sys($qData, $premium_matrix[$sys_version . '2']);

          } else if($sys_version == 1) {

            //  Vecajā sistēmā ņem norādīto stingro prēmiju
            $premium = (int)$amats['premija'];

          } else {

            //  Šeit itkā nevajadzētu nonākt bet katram gadījumam...
            $premium = 0;
          }
        }

        var_dump(array('premija' => $premium));

        db_query('
          INSERT INTO `' . DB_PREF . 'premijas` (gads, ceturksnis, pardevejs_id, ir_premija, premijas_apmers)
          VALUES (' . $quarter['year'] . ', ' . $quarter['quarter'] . ', ' . $merchant['id'] . ', 1, ' . $premium . ')
        ');

        //die('tikai līdz pirmajam kuram izpildās ceturksnis.');
      } else {

        var_dump('Neizpildās nosacījums');

        db_query('
          INSERT INTO `' . DB_PREF . 'premijas` (gads, ceturksnis, pardevejs_id, ir_premija)
          VALUES (' . $quarter['year'] . ', ' . $quarter['quarter'] . ', ' . $merchant['id'] . ', 0)
        ');
      }
      //die('līdz pirmajam kas nav rēķināts');
    } else {
      var_dump('IR fiksējusies');
    }
  }
}


?>
</html>