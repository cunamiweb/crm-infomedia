$.datepicker.setDefaults({
  firstDay: 1,
  changeMonth: true,
  changeYear: true,
  dayNamesMin: ['Sv', 'Pr', 'Ot', 'Tr', 'Ce', 'Pk', 'Se'],
  monthNamesShort: ['Jan', 'Feb', 'Mar', 'Apr', 'Mai', 'Jūn', 'Jūl', 'Aug', 'Sep', 'Okt', 'Nov', 'Dec']
});

$.tablesorter.addParser({
  id: 'latviantext',
  type: 'text',
  is: function(s) {
    return false;
  },
  format: function (s) {
    var translate = {
      "ē" : "e",
      "ŗ" : "r",
      "ū" : "u",
      "ī" : "i",
      "ō" : "o",
      "ā" : "a",
      "š" : "s",
      "ķ" : "k",
      "ļ" : "l",
      "ž" : "z",
      "č" : "c",
      "ņ" : "n"
    };
    var translate_re = /[ēŗūīōāšķļžčņ]/g;

    var result = s.toLowerCase().replace(
      translate_re,
      function(match) {
        return translate[match.toLowerCase()];
      }
    );

    result = $.trim($('<div>' + result + '</div>').text());

    return result;
  }
});

$.tablesorter.addParser({
  id: 'latviandate',
  type: 'numeric',
  is: function(s) {
    return false;
  },
  format: function (s) {

    var parts = s.split('.');

    if (parts.length == 3) {
      return (parts[2] * 10000) + (parts[1] * 100) + parts[0];
    } else {
      return 0;
    }

  }
});

$.tablesorter.addParser({
  id: 'multiplelatviandates',
  type: 'numeric',
  is: function(s) {
    return false;
  },
  format: function (s) {

    var parts = s.split('.');

    if (parts.length == 3) {
      return (parts[2] * 10000) + (parts[1] * 100) + parts[0];
    } else {
      return 0;
    }

  }
});

function addmorefiles(e, filetype){

  $(e).after('<div><input type="file" name="'+filetype+'[]"><button onclick="$(this).parent().remove(); return false;">Dzēst</button></div>');

  return false;

}

function deleteFile(e,who){

  if(confirm('Vai tiešām Jūs vēlaties dzēst šo failu?')){

    $(e).parent().slideUp('slow');
    $.get("./", { deletefile: who } );

  }

}

function editoradd(e, is_admin, is_limited) {
  // console.log(is_admin);

  var filter1 = '';
  var filter2 = '';
  var filter3 = '';
  var filter4 = '';

  var form = $('#fullformplace');

  if (is_admin) {

    //if (is_limited)
    var canedit1 = 'input.canedit, select.canedit, textarea.canedit'
    var canedit2= 'button.canedit'
    var canedit3 = '.hidethistoo.canedit'
    var canedit4 = '.disablethistoo.canedit'

    if (is_limited == 1) { // 06.02.2012.
      filter1 = form.find('input.limited_admin_edit, select.limited_admin_edit, textarea.limited_admin_edit, ' + canedit1);
      filter2 = form.find('button.limited_admin_edit, ' + canedit2);
      filter3 = '.hidethistoo.limited_admin_edit, ' + canedit3;
      filter4 = '.disablethistoo.limited_admin_edit, ' + canedit4;
    } else if (is_limited == 2) {
      filter1 = form.find('input.bookkeeper_admin_edit, select.bookkeeper_admin_edit, textarea.bookkeeper_admin_edit, ' + canedit1);
      filter2 = form.find('button.bookkeeper_admin_edit, ' + canedit2);
      filter3 = '.hidethistoo.bookkeeper_admin_edit, ' + canedit3;
      filter4 = '.disablethistoo.bookkeeper_admin_edit, ' + canedit4;
    } else if (is_limited == 3) {
      filter1 = form.find('input.minimal_admin_edit, select.minimal_admin_edit, textarea.minimal_admin_edit, ' + canedit1);
      filter2 = form.find('button.minimal_admin_edit, ' + canedit2);
      filter3 = '.hidethistoo.minimal_admin_edit, ' + canedit3;
      filter4 = '.disablethistoo.minimal_admin_edit, ' + canedit4;
    } else {
      filter1 = form.find('input, select, textarea');
      filter2 = form.find('button');
      filter3 = '.hidethistoo';
      filter4 = '.disablethistoo';
    }

  } else {
    filter1 = form.find('input.non_admin_edit, select.non_admin_edit, textarea.non_admin_edit');
    filter2 = form.find('button.non_admin_edit');
    filter3 = '.hidethistoo.non_admin_edit';
    filter4 = '.disablethistoo.non_admin_edit';
  }

  $(filter1).each(function() {

    if ($(this).is('.search_field') || $(this).is('.show_in_act_panel')) {
      return;
    }

    if ($(this).is('.always_disabled')) {
      return;
    }

    $(this).prop('disabled', '').removeClass('readonly');

  })

  $(filter2).not('.always_disabled').removeAttr('disabled');

  $(filter3).css('display', '');

  $(filter4).prop('disabled', '').removeClass('readonly');

  $('.onlyinview').hide();

  $(e).parent().html('');

  moveActionsToActPanel();
//not used anymore
  // $('.chosen').chosen();

  return false;

}

function editordisable() {

  $('#fullformplace').find('input, select, textarea').each(function() {

    if ($(this).is('.search_field') || $(this).is('.show_in_act_panel') || $(this).is('.sfield')) {
      return;
    }

    $(this).prop('disabled',true).addClass('readonly');

  });

  $('#fullformplace button:not(.onlyinview, .active_everywhere)').prop('disabled',true);

  $('.hidethistoo').css('display','none');

  $('.disablethistoo').prop('disabled',true).addClass('readonly');

  moveEditToActPanel();

}

function initActPanel() {

  var form = $('form#fullformplace');

  if (form.length != 1) {
    return false;
  }

  if (form.is('.disable_act_panel')) {
    return false;
  }

  if ($('#act_panel').length == 1) {
    return true;
  }

  $('body').css('padding-bottom', '50px');

  form.append('<div id="act_panel"><div id="act_panel_left"></div><div id="act_panel_center"></div><div id="act_panel_right"></div></div>');

  return true;

}

function moveEditToActPanel() {

  if (!initActPanel()) {
    return false;
  }

  var act_panel = $('#act_panel');

  $('#act_panel_left, #act_panel_center, #act_panel_right').empty();

  if ($('#labotdzestbutton').length) {
    $('#act_panel_center').append($('#labotdzestbutton'));
  } else {
    act_panel.hide();
  }

  /*

  act_panel.css('height', '2px');
  act_panel.find('*').hide();

  var timeout_h = false;

  act_panel.hover(function() {

    if (timeout_h) {
      clearTimeout(timeout_h);
    }

    $(this).find('*').show();
    $(this).animate({height: '2.3em'}, 100);

  }, function() {

    timeout_h = window.setTimeout(function() {

      $('#act_panel').find('*').hide();
      $('#act_panel').animate({height: '2px'}, 100);

    }, 500);

  });

  */

}

function moveActionsToActPanel() {

  if (!initActPanel()) {
    return false;
  }

  var act_panel = $('#act_panel');

  var left = $('#act_panel_left');
  var center = $('#act_panel_center');
  var right = $('#act_panel_right');

  left.empty();
  center.empty();
  right.empty();

  $('.show_in_act_panel').each(function() {

    if ($(this).is('.align_left')) {
      left.append($(this));
    } else if ($(this).is('.align_right')) {
      center.append($(this));
    } else {
      right.append($(this));
    }

  })

  act_panel.unbind('mouseenter');
  act_panel.unbind('mouseleave');

}

function saveAll(save_button, validate_action) {

  $('#fullformplace .input_has_error').removeClass('input_has_error');

  var loading_html = '<div class="loading"><img alt="Notiek ielāde..." src="css/loading.gif" /></div>';

  if ($('#loader_placeholder').length == 1) {
    $('#loader_placeholder').html(loading_html);
  } else {
    $(save_button).before(loading_html);
  }

  var loading = $('div.loading');

  var had_errors = true;
  var error_msgs = '';

  var tab_activated = false;

  $.ajax({
    url: 'validate.php?action=' + validate_action,
    async: false,
    cache: false,
    data: $('#fullformplace').serialize(),
    dataType: 'json',
    type: 'POST',
    success: function(data) {

      if (data['success']) {
        had_errors = false;
      } else {

        had_errors = true;

        $.each(data['errors'], function(tab_name, errors) {

          if (!tab_activated && typeof activateTab != "undefined") {
            activateTab(tab_name);
            tab_activated = true;
          }

          $.each(errors, function(field_name, error_msg) {
            $('#' + field_name).addClass('input_has_error');
            if (error_msg != '') {
              error_msgs += "- " + error_msg + "\n";
            }
          });

        });

      }

    }
  });

  loading.remove();

  if (had_errors) {

    window.location.hash = '#page_top';

    if (error_msgs != '')
      alert(error_msgs);

    return false;

  } else {

    var url = $('#fullformplace').attr('action');

    if (typeof activeone != 'undefined') {
      url = url + '&subtab=' + activeone;
    }

    $('#fullformplace').attr('action',  url);

    return true;

  }

}

function cancelSaves() {

  var url = $('#fullformplace').attr('action');

  if (typeof activeone != 'undefined') {
    url = url + '&subtab=' + activeone;
  }

  document.location = url;

  return false;

}

function initTableFilter(table, head, call_after, prefix, searchlist, sp, groups) {

  table.call_after_function = call_after;
  table.prefix = (prefix != '') ? prefix + '_' : '';
  table.searchlist = searchlist;
  table.sp = sp;

  showAll(table);

  $('thead', table).append('<tr class="last filter">'+head.html()+'</tr>');

  if (groups) {
    $('thead', table).prepend('<tr class="groups">'+groups.html()+'</tr>');
  }

  head.parents('table').remove();

  $('input.search_field, select.search_field', table).keyup(function() {
    advSearch(table);
  });

  $('input.kalendari', table).datepicker({dateFormat:'dd.mm.yy'}).change(function() {
    advSearch(table);
  });

  /*
  $('select.advonchange', table).change(function() {
    advSearch(table);
  });
  */

  var clear_field = function() {

    if ($(this).parent().find('.multiSelectOptions').length) {
      $(this).parent().find('.multiSelect').multiSelectOptionsUncheckAll();
    } else {
      $(this).parent().find('input').val('');
    }

    advSearch(table);

    return false;

  }

  $('a.search_field', table).click(clear_field)

  table.find('select.search_field').attr('multiple', true);

  var has_search_params = fillSearchParamsFromCache(table);

  initMultiSelects(table, {showFilterButton: false});

  if (has_search_params) {
    advSearch(table);
  }

  table.find('.multiSelectOptions input').click(function() {
    advSearch(table);
  })

}

function showAll(table) {

  table.showall = true;

  $("tbody.main tr", table).show();

  stripeTable(table);

  table.call_after_function();

}

function advSearch(table) {

  var exclude = ',';
  var hasParams = false;

  jQuery.each($('.search_field', table),function(k,v){

    var el = $(v);

    var el_to = null;
    var search_val = el.val();
    var meth = el.attr('meth');

    var isMultiSelect = el.is('.multiSelectOptions');

    if (meth == 'to') {
      return; // 'to' field we will access together with 'from'
    }

    if (meth == 'from') {
      el_to = el.parent().parent().find('input[meth=to]');
    }

    if((isMultiSelect && el.find('input:checked').length > 0) || (el.val() > 0 || el.val().length > 1) || (el_to != null && el_to.val().length > 1)){

      hasParams = true;
      table.showall = false;

      var what = false;

      if (isMultiSelect) {

        what = [];

        el.find('input:checked').not('.selectAll').each(function() {
          what.push($(this).val());
        });

      }else if(meth == "int"){

        what = search_val;

      }else if(meth == "standart"){

        what = search_val.toUpperCase();

      }else if(meth == "nouupper"){

        what = search_val;

      }else if(meth == "from"){

        var from_date = null;
        var to_date = null;

        if (search_val.length > 1) {
          from_date = search_val.split('.');
          from_date = parseInt(from_date[2], 10)*10000+parseInt(from_date[1], 10)*100+parseInt(from_date[0], 10);
        }

        if (el_to.val().length > 1) {
          to_date = el_to.val().split('.');
          to_date = parseInt(to_date[2], 10)*10000+parseInt(to_date[1], 10)*100+parseInt(to_date[0], 10);
        }

      }else if(meth == "coin"){

        what = search_val.replace(',','.');

      }

      jQuery.each(table.sp[el.attr('searchclass')],function(c,values){

        if (typeof(values) == 'object') {
          values = values;
        } else {
          values = [values];
        }

        var found = false;

        $.each(values, function(i, d) {

          if($.trim(d.toString()) != '') {

            if (isMultiSelect) {

              if ($.inArray(d.toString(), what) > -1) {
                found = true;
              }

            }else if (meth == "int") {

              if (d == what) {
                found = true;
              }

            }else if(meth == "standart" || meth == "nouupper"){

              d = d.toString();

              if(d.indexOf(what) >= 0){
                found = true;
              }

            }else if(meth == "from"){

              if (from_date != null && to_date != null) {

                if(d >= from_date && d <= to_date) {
                  found = true;
                }

              } else if (from_date != null) {

                if(d >= from_date) {
                  found = true;
                }

              } else if (to_date != null) {

                if(d <= to_date) {
                  found = true;
                }

              }


            }else if(meth == "to"){

              if(d <= tmpdate) {
                found = true;
              }

            }else if(meth == "coin"){

              if(parseInt(d, 10) == parseInt(what, 10)) {
                found = true;
              }

            }

          }

        });

        if (!found) {

          if(exclude.indexOf(',' + c + ',') < 0) {
            exclude += c + ',';
          }

        }

      });

    }

  });

  if(hasParams){

    table.showall = false;

    jQuery.each(table.searchlist, function(k,v){

      if(v != null && exclude.indexOf(',' + k + ',') < 0){
        $('#' + table.prefix + 'tr' + k).show();
      } else {
        $('#' + table.prefix + 'tr' + k).hide();
      }

    });

    stripeTable(table);

    table.call_after_function();

    cacheSearchParams(table);

  } else if(!table.showall) {

    showAll(table);

    clearSearchParamsCache(table);

  }


}

function stripeTable(table){

  table.each(function() {

    $("tbody.main tr", this).removeClass("odd");

    var i = 0;

    $("tbody.main tr:visible", this).each(function() {

      if (i % 2 == 0) {
        $(this).addClass("odd");
      }

      i ++;

    });

  });

};

function cacheSearchParams(table) {

  var cache_key = getCacheKeyFromClass(table);

  if (cache_key) {

    var data = {};

    $('input.search_field, select.search_field', table).each(function() {

      var key = $(this).attr('searchclass') + '||' + $(this).attr('meth');

      data[key] = $(this).val();

    });

    $('div.search_field', table).each(function() {

      var key = $(this).attr('searchclass');

      data[key] = [];

      $(this).find('input:checked').not('.selectAll').each(function() {
        data[key].push($(this).val());
      });

    });

    $.jStorage.set(cache_key, data);

  }

}

function clearSearchParamsCache(table) {

  var cache_key = getCacheKeyFromClass(table);

  if (cache_key) {
    $.jStorage.deleteKey(cache_key);
  }

}

function fillSearchParamsFromCache(table) {

  var cache_key = getCacheKeyFromClass(table);

  var has_search_params = false;

  if (cache_key) {

    var data = $.jStorage.get(cache_key, false);

    if (data) {

      $.each(data, function(k, v) {

        if (typeof v == 'object') {

          if (v.length > 0) {

            has_search_params = true;

            table.find('.search_field[searchclass='+k+']').val(v);

          }

        } else {

          if (v != '') {

            has_search_params = true;

            var parts = k.split('||');

            table.find('.search_field[searchclass='+parts[0]+'][meth='+parts[1]+']').val(v);

          }

        }

      });

    }

  }

  return has_search_params;

}

function changeAtgStatus(atg_id, status) {

  var msg = '';

  if (status == 1) {
    msg = 'Vai atstatīt atgādinājuma statusu uz "Jauns"?';
  } else if (status == 2) {
    msg = 'Vai atzīmēt atgādinājumu kā izpildītu?'
  } else if (status == 3) {
    msg = 'Vai atzīmēt atgādinājumu kā atceltu?'
  }


  if (confirm(msg)) {

    $.get('?updateAtgStatus=' + atg_id + '&status=' + status, function(result) {

      if (result == '1') {

        if (status == 1) {

          $('tr.atg_id_' + atg_id + ' td.status span.link_wrap').html(
            '<a onclick="return changeAtgStatus(' + atg_id + ', 2);" href="#">Izpildīts</a><br />' +
            '<a onclick="return changeAtgStatus(' + atg_id + ', 3);" href="#">Atcelts</a>'
          );

        } else if (status == 2) {

          $('tr.atg_id_' + atg_id + ' td.status span.link_wrap').html(
            'Izpildīts <a onclick="return changeAtgStatus(' + atg_id + ', 1);" href="#">X</a>'
          );

        } else if (status == 3) {

          $('tr.atg_id_' + atg_id + ' td.status span.link_wrap').html(
            'Atcelts <a onclick="return changeAtgStatus(' + atg_id + ', 1);" href="#">X</a>'
          );

        }

        $('tr.atg_id_' + atg_id + ' td.status input').val(status);

        refreshAtgStatuses();

      } else {

        alert('Neizdevās nomainīt atgādinājuma statusu!');

      }

    });

  }

  return false;

}

function refreshAtgStatuses(){

  jQuery.each( $('.atgadinajumi_termimi') , function(){

    var today = new Date();
    tmpdate = $(this).val().split('.');
    var myDate=new Date(tmpdate[2],tmpdate[1]-1,tmpdate[0],0,0,0,0);

    if($(this).val() != ''){

      $(this).parent().parent().removeClass('active no_term done canceled');

      if(today > myDate){

        $(this).parent().parent().addClass('active');

      }else{

        $(this).parent().parent().addClass('no_term');

      }

      if( $(this).parent().parent().find('td.status input').val() == 2 ){

        $(this).parent().parent().removeClass('active no_term done canceled');

        $(this).parent().parent().addClass('done');

      }

      if( $(this).parent().parent().find('td.status input').val() == 3 ){

        $(this).parent().parent().removeClass('active no_term done canceled');

        $(this).parent().parent().addClass('canceled');

      }

    }

  });

}

function addAtgRow() {

  $('#atgadinajumu_tabula tr#atg_new_row').removeAttr('id');

  var tpl = $('#atgadinajumsprot tbody').html();

  $('#atgadinajumu_tabula').append(tpl);

  $('#atgadinajumu_tabula tr:last-child').attr('id', 'atg_new_row')

  $(".showcalendarnew").datepicker({dateFormat:'dd.mm.yy'});
  $('.calendarcalc').change(refreshAtgStatuses);

  refreshAtgStatuses();

  window.location.hash = '#atg_new_row';

  return false;

}

function addmorecomments(e, filetype){

  var tpl = $('#komentarsprot_' + filetype).html();

  $(e).parent().after(tpl);

  return false;

}

function create_filter_field_array(names) {

  var sp = new Array();

  $.each(names, function(i, name) {
    sp[name] = new Array();
  });

  return sp;

}

function deleteComment(e,who){

  if(confirm('Vai tiešām Jūs vēlaties dzēst šo komentāru?')){

    $(e).parents('li').filter(':first').slideUp('slow');
    $.get("./", { deletecomment: who } );

  }

}

function ctop(s) {
  s = s+'';
  return s.replace(",", ".");
}

function isNumber(x) {
  x=parseFloat(ctop(x));
  return ((x > 0 || x <= 0));
}

function changeTableOrder(th, col_name) {

  var form = $(th).parents('form');

  var order_input = form.find('input[name=order]');
  var direction_input = form.find('input[name=direction]');

  var direction = 'asc';

  if (order_input.val() == col_name) {

    if (direction_input.val() == 'asc') {
      direction = 'desc';
    } else {
      direction = 'asc';
    }

  }

  order_input.val(col_name);

  direction_input.val(direction);

  form.submit();

  return false;

}

function initPlainSortableTable(form) {

  var table = form.find('table');

  form.find('a.sfield_clear').click(function() {

    if ($(this).parent().find('.multiSelectOptions').length) {
      $(this).parent().find('.multiSelectOptions input').prop('checked', false);
    } else {
      $(this).parent().find('input').val('');
    }

    form.find('input[name=page]').val(1);

    form.submit();

    return false;

  });

  form.find(".kalendari").datepicker({dateFormat:'dd.mm.yy'});

  form.find("tr.filter :input, div.glob_filter :input").change(function() {
    form.find('input[name=page]').val(1);
  });

  form.find("tr.filter .kalendari, div.glob_filter .kalendari").change(function() {
    form.submit();
  });

  form.find("tr.filter select, div.glob_filter.select").change(function() {
    form.submit();
  });

  var order_input = form.find('input[name=order]');
  var direction_input = form.find('input[name=direction]');

  var th = form.find('th[colname="' + order_input.val() + '"]');

  if (th.length) {

    if (direction_input.val() == 'asc') {
      th.addClass('headerSortUp');
      th.removeClass('headerSortDown');
    } else {
      th.addClass('headerSortDown');
      th.removeClass('headerSortUp');
    }

  }

  initMultiSelects(
    table,
    {
      onSubmitClick: function() {
        $(this).parents('form').find('input[name=page]').val(1);
      }
    }
  );

  form.find('tr.filter input.multiSelectSubmit').click();

  stripeTable(table);

}

function days_between(date1, date2) {

  if (typeof(date1) == 'object') {
    var date1_obj = date1;
  } else {

    var date1_obj = new Date();

    var date1_obj_parts = date1.split('.');

    date1_obj.setFullYear(
      parseInt(date1_obj_parts[2], 10),
      parseInt(date1_obj_parts[1], 10) - 1,
      parseInt(date1_obj_parts[0], 10),
      0,0,0
    );

  }

  if (typeof(date2) == 'object') {
    var date2_obj = date2;
  } else {

    var date2_obj = new Date();

    var date2_obj_parts = date2.split('.');

    date2_obj.setFullYear(
      parseInt(date2_obj_parts[2], 10),
      parseInt(date2_obj_parts[1], 10) - 1,
      parseInt(date2_obj_parts[0], 10),
      0,0,0
    );

  }

  var one_day = 1000 * 60 * 60 * 24

  var date1_ms = date1_obj.getTime()
  var date2_ms = date2_obj.getTime()

  return Math.round((date2_ms - date1_ms) / one_day)

}

function init_auto_logout() {

  window.setInterval(function() {

    $.get('?do_auto_logout=1', function(do_auto_logout) {

      do_auto_logout = parseInt(do_auto_logout, 10);

      if (do_auto_logout) {
        window.location.href = '?logout=true&auto_logout=1';
      }

    }, 'text');

  }, 120 * 1000) // 2 minutes

}

function getCacheKeyFromClass(element) {

  var cache_key = false;

  if (element.attr('class') && element.attr('class').match(/\bckey_/)) {

    var ckey_info = /\bckey_([a-z0-9_]+)/i.exec(element.attr('class'));

    cache_key = ckey_info[1];

  }

  return cache_key;

}

function showLigumaBeiguStatussDialog(liguma_id) {

  $('<iframe src="?c=ligumi&a=beigu_statuss&liguma_id='+liguma_id+'&without_nav=1" />').dialog({
      title: 'Līguma beigu statuss',
      autoOpen: true,
      width: 600,
      height: 450,
      modal: true,
      resizable: false,
      draggable: false,
      closeOnEscape: false,
      close: function() {
        document.location.reload();
      }
  }).width(580);

}

function initSelectsWithStatusColors() {

  function update(select) {

    var classprefix = /\bclassprefix-([a-z0-9_]+)/i.exec(select.attr('class'));

    classprefix = classprefix[1];

    var re = new RegExp('\\b' + classprefix + '_\\d+', 'g');

    select.attr('class', select.attr('class').replace(re, ''));

    var selopt = select.find('option:selected');

    var status = /\bstatus-([0-9]+)/i.exec(selopt.attr('class'));

    select.addClass(classprefix + '_' + status[1]);

    select.find('option')
      .css('background-color', 'white')
      .css('color', 'black');

  }

  $('select.with_status_colors').each(function() {

    update($(this));

    $(this).change(function() {
      update($(this));
    })

  });


}

function initMultiSelects(table, options) {

  if (typeof options == 'undefined') options = {};

  options['showFilterButton'] = (typeof options['showFilterButton'] != 'undefined') ? options['showFilterButton'] : true;
  options['onSubmitClick'] = (typeof options['onSubmitClick'] != 'undefined') ? options['onSubmitClick'] : function() {};

  table.find('select.search_field, select.sfield').multiSelect({
    selectAllText: 'Visi',
    noneSelected: '&nbsp;',
    oneOrMoreSelected: '*',
    showFilterButton: options['showFilterButton'],
    onSubmitClick: options['onSubmitClick'],
    optGroupSelectable: true,
    listHeight: 300
  });

}

function initTableCols(table, table_name, col_data) {

  if (typeof col_data == 'undefined') col_data = [];

  var groups = getTableGroups(table);

  var hide_col = function(th) {

    var title = th.text();

    title = title.substr(0, title.length - 1);

    var pos = th.index() + 1;

    var color_class = /\bbgc-([a-z]+)/i.exec(th.attr('class'));

    th.hide().after('<th class="hidden_col"></th>');

    th.next().wTooltip({
      'content' : title
    })

    if (color_class) {
      th.next().addClass(color_class[0]);
    }

    table.find('thead > tr.filter > th:nth-child('+pos+')').each(function() {

      $(this).hide().after('<th class="hidden_col"></th>')

      if (color_class) {
        $(this).next().addClass(color_class[0]);
      }

    });

    table.find('tbody > tr > td:nth-child('+pos+')').each(function() {

      $(this).hide().after('<td class="hidden_col"></td>');

      if (color_class && $(this).parent('tr').is('.odd') == false && $(this).parents('tbody').is('.main')) {
        $(this).next().addClass(color_class[0] + '2');
      }

    });

    th.next().click(function() {

      $.get('?setTableColVisible=' + $(this).prev().attr('colname') + '&tableName=' + table_name);

      var pos = $(this).index();

      var color_class = /\bbgc-([a-z]+)/i.exec($(this).attr('class'));

      var prev_el = $(this).prev();

      prev_el.show();

      if (color_class) {
        prev_el.addClass(color_class[0]);
      }

      $(this).remove();

      table.find('thead > tr.filter > th:nth-child('+pos+')').each(function() {

        $(this).show().next().remove();

        if (color_class) {
          $(this).addClass(color_class[0]);
        }

      });

      table.find('tbody > tr > td:nth-child('+pos+')').each(function() {

        $(this).show().next().remove();

        if (color_class && $(this).parent('tr').is('.odd') == false && $(this).parents('tbody').is('.main')) {
          $(this).addClass(color_class[0] + '2');
        }

      });

      toggle_col_group(prev_el);

    });

    toggle_col_group(th.next());

  }

  var position_hide_button = function(a) {

    var cell = a.parents('th');

    var cell_pos = cell.position();
    console.log(cell_pos);
    var cell_width = cell.outerWidth(false);

    a.css('left', (cell_pos.left + cell_width - 15) + 'px')
    a.css('top', cell_pos.top + 'px');

  }

  var toggle_col_group = function(th) {

    if (groups.length == 0) {
      return false;
    }

    var vis_pos = 0;

    th.parents('tr').find('th:visible').each(function() {

      if (this == th.get(0)) {
        return false;
      }

      vis_pos ++;

    });

    var group_to_toggle;

    $.each(groups, function(group_num, group) {

      $.each(group, function(col_num, col) {

        if (col_num == vis_pos) {
          group_to_toggle = group_num;
        }

      });

    });

    var visible_cols = 0;

    $.each(groups[group_to_toggle], function(col_num, col) {

      if (table.find('thead > tr.filter').children('th:visible').eq(col_num).is('.hidden_col') == false) {
        visible_cols ++;
      }

    });

    var group_th = table.find('thead > tr.groups > th:nth-child(' + (parseInt(group_to_toggle) + 1) + ')');

    if (visible_cols > 0) {
      group_th.css('text-indent', '0');
    } else {
      group_th.css('text-indent', '-9999px');
    }


  }

  table.find('th.header[colname]').append('<a class="hide">x</a>');

  table.find('th.header a.hide').each(function() {

    $(this).parents('th').hover(function() {
      $(this).find('.hide').show();
      $(this).find('.hide')
      // position_hide_button();
    }, function() {
      $(this).find('.hide').hide();
    })

    $(this).click(function(e) {

      e.stopPropagation();

      if (table.find('thead th.header:visible').length <= 1) {
        alert('Pēdējo redzamo kolonu nevar aizvērt!');
        return false;
      }

      var th = $(this).parents('th');

      $.get('?setTableColHidden=' + th.attr('colname') + '&tableName=' + table_name);

      hide_col(th);

    });

  });


  $.each(col_data, function(k, v) {

    if (typeof v['lauks'] != 'undefined' && v['redzams'] == 0) {

      var col = table.find('th.header[colname='+v['lauks']+']');

      if (col.length > 0) {
        hide_col(col);
      }

    }

  })

}

function getTableGroups(table) {

  var cols = {};

  var i = 0;

  var from = 0;
  var to = 0;

  var color_class;

  table.find('tr.groups th').each(function() {

    cols[i] = {};

    to += parseInt($(this).attr('colspan')) ? parseInt($(this).attr('colspan')) : 1;

    color_class = /\bbgc-([a-z]+)/i.exec($(this).attr('class'));

    for(var ii = from; ii < to; ii ++) {

      cols[i][ii] = {};

      cols[i][ii]['color'] = color_class ? color_class[0] : false;

    }

    from = to;

    i ++;

  });

  return cols;

}

function initTableGroupColors(table) {

  var rows = table.find('tr.header, tr.filter, tbody.main > tr:visible:not(.odd)');

  $('tbody.main > tr > td').each(function() {

    color_class = /\bbgc-([a-z]+)/i.exec($(this).attr('class'));

    if (color_class) {
      $(this).removeClass(color_class[0] + '2');
    }

  });

  var cols = getTableGroups(table);

  rows.each(function() {

    var row = $(this);

    $.each(cols, function(group_num, group) {

      $.each(group, function(col_num, col) {

        if (col['color']) {

          if (row.is('.filter') || row.is('.header')) {
            row.children('th:visible').eq(col_num).addClass(col['color']);
          } else {
            row.children('td:visible').eq(col_num).addClass(col['color'] + '2');
          }

        }

      });

    });

  });

}

function showLigumaAutoParslegsanasDialog(liguma_id) {

  $('<iframe src="?c=ligumi&a=parslegsana&liguma_id='+liguma_id+'&without_nav=1" />').dialog({
      title: 'Līguma pārslēgšana',
      autoOpen: true,
      width: 800,
      height: 450,
      modal: true,
      resizable: false,
      draggable: false,
      closeOnEscape: false,
      close: function() {
        document.location.reload();
      }
  }).width(780);

}

function initStatistikaTable() {

  var table = $('table#ceturksnu_statistika');

  table.find('td').each(function() {

    if ($(this).attr('title')) {

      $(this).wTooltip({
        follow: true,
        offsetY: 10,
        offsetX: 10
      });

    }

  });

  var loading_html = '<div class="loading"><img alt="Notiek ielāde..." src="css/loading.gif" /></div>';

  table.find('a.open_months').click(function() {

    var open_months_link = $(this);

    var quarter = /\quarter\-([0-9\-]+)/i.exec($(this).attr('class'))[1];

    var pardeveja_id = /\pardeveja_id\-([0-9]+)/i.exec($(this).attr('class'));

    if (pardeveja_id)
      pardeveja_id = pardeveja_id[1];

    var table = $(this).closest('table');

    var month_rows = table.find('tr.month_row-' + quarter);

    if (month_rows.length) {
      if (month_rows.is(':visible')) {
        table.find('tr.parent_month_row-' + quarter).hide();
        month_rows.hide();
      } else {
        month_rows.show();
      }
      return false;
    }

    open_months_link.hide();
    open_months_link.after(loading_html);

    var bottom_quarter_row = table.find('tr.bottom_quarter-' + quarter);

    $.get(
      'ajax.php?action=statistika_months_rows',
      {
        quarter: quarter,
        pardeveja_id: pardeveja_id ? pardeveja_id : ''
      },
      function(result) {

        open_months_link.closest('td').find('.loading').remove();
        open_months_link.show();

        var html = $(result);

        bottom_quarter_row.after(html.find('tr').addClass('month_row-' + quarter));

        if (!pardeveja_id) {

          table.find('tr.month_row-' + quarter).find('a.open_pardeveji').click(function() {

            var open_pardeveji_link = $(this);

            var month = /\month\-([0-9\-]+)/i.exec($(this).attr('class'))[1];

            var pardeveja_rows = table.find('tr.pardeveja_row-' + month);

            if (pardeveja_rows.length) {
              pardeveja_rows.is(':visible') ? pardeveja_rows.hide() : pardeveja_rows.show();
              return false;
            }

            open_pardeveji_link.hide();
            open_pardeveji_link.after(loading_html);

            var bottom_month_row = table.find('tr.bottom_month-' + month);

            $.get('ajax.php?action=statistika_month_pardeveji_rows', {month: month}, function(result) {

              open_pardeveji_link.closest('td').find('.loading').remove();
              open_pardeveji_link.show();

              var html = $(result);

              html.find('tr').addClass('pardeveja_row-' + month).addClass('parent_month_row-' + quarter);

              bottom_month_row.after(html.find('tr'));

            });

            return false;

          });

        }

      },
      'html'
    );

    return false;

  });

}

function initPlanaIzpildeTable(monthsOpenAtLoad) {

  var table = $('table#plana_izpilde');

  if(!monthsOpenAtLoad) {
    table.find('tr.month_row').hide();
  }
  table.find('tr.pardeveja_row').hide();

  table.find('a.open_months').click(function() {

    var quarter = /\quarter\-([0-9\-]+)/i.exec($(this).attr('class'))[1];

    var pardeveja_id = /\pardeveja_id\-([0-9]+)/i.exec($(this).attr('class'));

    if (pardeveja_id)
      pardeveja_id = pardeveja_id[1];

    var table = $(this).closest('table');

    var month_rows = table.find('tr.month_row-' + quarter);

    if (month_rows.is(':visible')) {
      month_rows.hide();
      table.find('tr.pardeveja_row_quarter-' + quarter).hide();
    } else {
      month_rows.show();
    }

  });

  table.find('a.open_pardeveji').click(function() {

    var month = /\month\-([0-9\-]+)/i.exec($(this).attr('class'))[1];

    var pardeveja_rows = table.find('tr.pardeveja_row-' + month);

    if (pardeveja_rows.is(':visible'))
      pardeveja_rows.hide()
    else
      pardeveja_rows.show();

    return false;

  });

}

function showGoogleKontuLinkiDialog(liguma_id) {

  $('<iframe src="?c=ligumi&a=google_kontu_linki&liguma_id='+liguma_id+'&without_nav=1" />').dialog({
      title: 'Līguma kontu saites',
      autoOpen: true,
      width: 600,
      height: 450,
      modal: true,
      resizable: false,
      draggable: false,
      closeOnEscape: false,
      close: function() {
        document.location.reload();
      }
  }).width(580);

}

/**
 * Hides main menu bar and displays an X with a link instead
 */
function hideMenuBar(close_url) {
  $('.main-menu-wrap').html(' ')
    .append($('<a>Tālāk</a>')
      .attr('href', close_url));
}

function deleteAtg(atgId, obj) {
    var answer = confirm('Tiešām dzēst šo atgādinājumu?');

    if(answer) {
      $.get('ajax.php?action=atg_delete', {id : atgId}, function(data){
          data = $.parseJSON(data);
          if(data) {
              $(obj).parent().parent().hide();
          }
      })
    }
    return false;
}

function changePrices(obj, id, r, s, t)
{
  d = $("#change_prices").clone();
  d.removeAttr('id');

  $.getJSON('ajax.php?action=get_std_prices&id=' + id, {}, function(data){

    /*var cena = $(obj).parent().parent().find('.cena .value').html()
    var cena_s = $(obj).parent().parent().find('.cena_s .value').html()
    var cena_t = $(obj).parent().parent().find('.cena_t .value').html()*/
    var cena = data['cena'];
    var cena_s = data['cena_s'];
    var cena_t = data['cena_t'];

    d.find(".standartapiedavajums_override").val(cena);
    d.find(".standartapiedavajums_override_s").val(cena_s);
    d.find(".standartapiedavajums_override_t").val(cena_t);

    if(r) {d.find(".enable_override").prop('checked', true).trigger('change');}
    if(s) {d.find(".enable_override_s").prop('checked', true).trigger('change');}
    if(t) {d.find(".enable_override_t").prop('checked', true).trigger('change');}

    d.find("form").submit(function(){
      if(!$(this).find('.standartapiedavajums_override').val() || !$(this).find('.standartapiedavajums_override_s').val() || !$(this).find('.standartapiedavajums_override_t').val()) {
        alert('- Cenu jānorāda obligāti');
      } else {
        $.getJSON('ajax.php?action=change_std_prices&id=' + id, d.find('form').serialize(), function(data){
          if(data) {
            window.location.reload()
          } else {
            alert('Cenas neizdevās nomainīt');
          }
        });
      }
      return false;
    })

    d.dialog({title:data['atslegvards'], close: function(event, ui){}});
  });
}