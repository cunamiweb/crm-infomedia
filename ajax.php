<?php
include("inc/init.php");

if (!$_SESSION['login']) {
  die();
}

$actions = array(
  'check_client',
  'send_rek_to_email',
  'toggle_ipasais',
  'ligumu_grupas',
  'statistika_months_rows',
  'statistika_month_pardeveji_rows',
  'ligumi_parslegsanas_rindai',
  'atg_delete',
  'get_std_piedavajumi',
  'change_std_prices',
  'search_std_av',
  'get_std_prices',
);

if (!empty($_GET['action']) && in_array($_GET['action'], $actions)) {

  $function_to_call = 'ajax_' . $_GET['action'];

  $function_to_call();

}

?>