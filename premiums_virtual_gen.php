<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
</head>
<?php
/**
 * Skripts kas veic VIRTUĀLO prēmiju aprēķinus ik ceturksni.
 *
 * Domāts darbināt ar CRON.
 * Rezultāts nav atkarīgs no skripta izpildes datuma, jo tiek fiksēts kam, par kādu
 * laika posmu ticis aprēķināts.
 * Tāpēc CRON perioda uzstādījums nav kritiski svarīgs.
 * Rēķina tikai tekošajam ceturksnim
 * Darbojas tikai trešā prēmiju versija
 *
 * @author Dāvis Frīdenvalds davis@datateks.lv
 * @since 2013.01.25.
 */

ini_set('max_execution_time', 3600);

include("inc/init.php");

$cur_quarter = (int)ceil(date('m')/3);
$year = (int)date('Y');
$quarters = array_slice(array(1, 2, 3, 4), array_search($cur_quarter, array(1, 2, 3, 4)));


//  Savāc visu pārdevēju sarakstu. Tikai tie kam tips ir sales un aktīvs
$merchants = db_query('
  SELECT p.* FROM `' . DB_PREF . 'pardeveji` p
  WHERE p.tips = "sales" AND p.aktivs = 1
');

//  Apgrozījuma nosacījums
$condition = 1; //  minimālais sasniegtais/plānotais

//  Prēmiju matricas - jaunajām sistēmām
$premium_matrix = array('1' => 1, '1.01' => 2, '1.02' => 3, '1.03' => 5);



//  Pārbauda katru pārdevēju vai tam ir sarēķinātas prēmijas par visiem ceturkšņiem
while($merchant = db_get_assoc($merchants)) {
  //  Katrs pārdevējs

  foreach($quarters as $quarter) {

    echo '<br />#########################################<br />';
    var_dump($quarter, $year);

    echo '<br />********************************************<br />';

    var_dump(array('id' => $merchant['id'], 'name' => $merchant['vards']));

    var_dump($year . '-' . $quarter);

    //  Nodzēš veco
    db_query('
      DELETE FROM `' . DB_PREF . 'premijas_virtual`
      WHERE pardevejs_id = ' . $merchant['id'] . ' AND gads = ' . $year . ' AND ceturksnis = ' . $quarter);

    $qData = get_izpildits_virtual($merchant['id'], $year,  $quarter);

    var_dump('Apgrozījuma nosacījums: ' .  $condition);

    var_dump(array(
        'skaits' => array('ir' => $qData['kopa']['skaits'], 'vajag' => $qData['atdoti']['skaits']),
        'apgrozījums' => array('ir' => $qData['kopa']['apgr'], 'vajag' => $qData['atdoti']['apgr'], 'attieciba' => get_apgrozijuma_attiecibu($qData)),
      )
    );

    //  Vai izpildās ceturkšņa nosacījums: noslēgti vismaz tik līgumi cik plānoti un sasniegtā summa vismaz x% no plānotās.
    if(is_premium_condition_met($qData, $condition)) {
      //  Izpildās. Tālāk skatāmies par katru mēnesi, lai iegūtu prēmijas procentus

      var_dump('Izpildās nosacījums (ceturkšņa)');

      $premium = determine_premium_new_sys($qData, $premium_matrix);

      var_dump(array('premija' => $premium));

      db_query('
        INSERT INTO `' . DB_PREF . 'premijas_virtual` (gads, ceturksnis, pardevejs_id, ir_premija, premijas_apmers)
        VALUES (' . $year . ', ' . $quarter . ', ' . $merchant['id'] . ', 1, ' . $premium . ')
      ');

      //die('tikai līdz pirmajam kuram izpildās ceturksnis.');
    } else {

      var_dump('Neizpildās nosacījums');

      db_query('
        INSERT INTO `' . DB_PREF . 'premijas_virtual` (gads, ceturksnis, pardevejs_id, ir_premija)
        VALUES (' . $year . ', ' . $quarter . ', ' . $merchant['id'] . ', 0)
      ');
    }
  }
}


?>
</html>