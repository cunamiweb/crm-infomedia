<?php
class Pakalpojums extends BaseModel
{
  public static function model($classname=__CLASS__)
  {
    return parent::model($classname);
  }

  public function tableName()
  {
    return 'pakalpojumi';
  }

  public function getTitle()
  {
    $result = db_get_assoc(db_query('SELECT nosaukums FROM ' . DB_PREF . ' WHERE id = ' .  $this->pakalpojuma_veida_id));
    if(isset($result['nosaukums'])) {
      return $result['nosaukums'];
    }
  }
}
?>