<?php
class mpkDisplay extends BaseModel
{
  public static function model($classname=__CLASS__)
  {
    return parent::model($classname);
  }

  public function tableName()
  {
    return 'mpk_displays';
  }

  public function getHumanName()
  {
    $names = array(
      'youtube' => 'YouTube',
      'mobile' => 'Mob.aplik.',
      'retargeting' => 'Retargeting',
    );

    return isset($names[$this->code]) ? $names[$this->code] : ucfirst($this->code);
  }
}
?>