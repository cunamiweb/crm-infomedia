<?php
class ManualCenaLog extends BaseModel
{
  public static function model($classname=__CLASS__)
  {
    return parent::model($classname);
  }

  public function tableName()
  {
    return 'piedavajumi_m_cenas_log';
  }

  public function save()
  {
    if(!$this->pardevejs_id) {
      $this->pardevejs_id = $_SESSION['user']['id'];
    }

    if(!$this->datums) {
      $this->datums = date('Y-m-d H:i:s');
    }

    parent::save();
  }
}
?>
