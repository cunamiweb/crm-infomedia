<?php
class BudzetaParskaitijums extends BaseModel
{
  public static function model($classname=__CLASS__)
  {
    return parent::model($classname);
  }

  public function tableName()
  {
    return 'rekini_budzeta_parskaitijumi';
  }

  public function validate($scenario = null)
  {
    if($scenario == 'update') {
      if(!$this->summa) {
        $this->addError('summa', 'Summu jānorāda obligāti!');
      }

      if(!$this->apm_datums) {
        $this->addError('apm_datums', 'Apmaksas datumu jānorāda obligāti!');
      }
    }

    return !$this->hasErrors();
  }
}
?>