<?php
class Mekletajs extends BaseModel
{
  public static function model($classname=__CLASS__)
  {
    return parent::model($classname);
  }

  public function tableName()
  {
    return 'mekletaji';
  }

  public function getValsts()
  {
    return Valsts::model()->findByPk($this->valsts);
  }
}
?>