<?php
class Uzstadijums extends BaseModel
{
  public static function model($classname=__CLASS__)
  {
    return parent::model($classname);
  }

  public function tableName()
  {
    return 'uzstadijumi';
  }

  public function validate()
  {
    if(!$this->value) {
      $this->addError('value', $this->label . ' jānorāda obligāti.');
    }

    return !$this->hasErrors();
  }
}
?>