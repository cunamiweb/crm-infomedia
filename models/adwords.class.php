<?php

use Google\AdsApi\AdWords\v201809\cm\Keyword;
use Google\AdsApi\AdWords\v201809\cm\KeywordMatchType;
use Google\AdsApi\AdWords\v201809\o\KeywordEstimateRequest;
use Google\AdsApi\AdWords\v201809\o\AdGroupEstimateRequest;
use Google\AdsApi\AdWords\v201809\cm\Money;
use Google\AdsApi\AdWords\v201809\o\CampaignEstimateRequest;
use Google\AdsApi\AdWords\v201809\cm\Selector;
use Google\AdsApi\AdWords\v201809\cm\Location;
use Google\AdsApi\AdWords\v201809\cm\Language;
use Google\AdsApi\AdWords\v201809\cm\OrderBy;
use Google\AdsApi\AdWords\v201809\cm\DateRange;
use Google\AdsApi\AdWords\v201809\o\TargetingIdeaService;
use Google\AdsApi\AdWords\v201809\o\TrafficEstimatorService;
use Google\AdsApi\AdWords\v201809\o\TrafficEstimatorSelector;
use Google\AdsApi\AdWords\v201809\o\TargetingIdeaSelector;
use Google\AdsApi\AdWords\v201809\o\RequestType;
use Google\AdsApi\AdWords\v201809\o\IdeaType;
use Google\AdsApi\AdWords\v201809\o\AttributeType;
use Google\AdsApi\AdWords\v201809\o\LanguageSearchParameter;
use Google\AdsApi\AdWords\v201809\cm\Paging;
use Google\AdsApi\AdWords\v201809\cm\ReportDefinitionReportType;
use Google\AdsApi\AdWords\AdWordsSessionBuilder;
use Google\AdsApi\AdWords\AdWordsServices;
use Google\AdsApi\Common\OAuth2TokenBuilder;
use Google\AdsApi\AdWords\v201809\o\RelatedToQuerySearchParameter;
use Google\AdsApi\AdWords\v201809\o\LocationSearchParameter;
use Google\AdsApi\Common\Util\MapEntries;
use Google\AdsApi\AdWords\v201809\cm\ReportDefinitionService;
use Google\AdsApi\AdWords\Reporting\v201809\DownloadFormat;
use Google\AdsApi\AdWords\Reporting\v201809\ReportDefinition;
use Google\AdsApi\AdWords\Reporting\v201809\ReportDownloader;
use Google\AdsApi\AdWords\Reporting\v201809\ReportDefinitionDateRangeType;
use Google\AdsApi\AdWords\v201809\cm\CampaignService;
use Google\AdsApi\AdWords\v201809\cm\SortOrder;
use Google\AdsApi\AdWords\v201809\mcm\ManagedCustomerService;
use Google\AdsApi\AdWords\v201809\cm\NetworkSetting;
use Google\AdsApi\AdWords\v201809\o\NetworkSearchParameter;
use Google\AdsApi\AdWords\v201809\cm\ApiException;
use Google\AdsApi\AdWords\v201809\cm\RateExceededError;

class Adwords {
	private $user;
	public $version;
	const RECOMMENDED_PAGE_SIZE = 500;

	public function __construct( $version = ADWORDS_VERSION, $customer_id = null, $auth_version = 'default' ) {

		if ( $auth_version == 'default' ) {
			$filePath = DIR_ROOT . '/inc/adwords_default.ini';
		} else if ( $auth_version == 'customers' ) {
			$filePath = DIR_ROOT . '/inc/adwords_customers.ini';
		} else {
			throw new Exception( 'ERROR: unknown adwords auth version' );
		}

		// Generate a refreshable OAuth2 credential for authentication.
		$oAuth2Credential = ( new OAuth2TokenBuilder() )->fromFile( $filePath )->build();

		$this->user = ( new AdWordsSessionBuilder() )->fromFile( $filePath );

		if ( $customer_id ) {
			$this->user->withClientCustomerId( $customer_id );
		} else if ( $auth_version == "default" ) {
			$this->user->withClientCustomerId( ADWORDS_DEFAULT_CLIENT_CUSTOMER_ID );
		}

		$this->session = $this->user->withOAuth2Credential( $oAuth2Credential )->build();

		$this->version = $version;
	}

	public function getMonthlySearches( $keywords, $country_codes, $language_codes = array() ) {

		$pageLimit = 500;


		$adWordsServices      = new AdWordsServices();
		$targetingIdeaService = $adWordsServices->get( $this->session, TargetingIdeaService::class );

		// Search param array
		$params = [];

		$k_array = [];
		foreach ( $keywords as $k ) {
			$k_array[] = mb_strtolower( $k );
		}

		// Create selector.
		$selector = new TargetingIdeaSelector();
		$selector->setRequestType( RequestType::STATS );
		$selector->setIdeaType( IdeaType::KEYWORD );

		$selector->setRequestedAttributeTypes( [
			AttributeType::KEYWORD_TEXT,
			AttributeType::SEARCH_VOLUME,
		] );

		// Set selector paging (required for targeting idea service).
		$paging = new Paging();
		$paging->setStartIndex( 0 );
		$paging->setNumberResults( $pageLimit );
		$selector->setPaging( $paging );

		// Create related to keyword search parameter.
		$relatedToKeywordSearchParameter = new RelatedToQuerySearchParameter();
		$relatedToKeywordSearchParameter->setQueries( $k_array );
		$params[] = $relatedToKeywordSearchParameter;

		// Create keyword match type search parameter to ensure unique results.

		if ( ! empty( $country_codes ) ) {
			// Create CountryTargetSearchParameter
			$locations = [];
			foreach ( $country_codes as $country_code ) {
				$location = new Location();
				$location->setId( self::getCountryID( $country_code ) );
				$locations[] = $location;
			}
			$locationParameter = new LocationSearchParameter();
			$locationParameter->setLocations( $locations );
			$params[] = $locationParameter;
		}

		if ( ! empty( $language_codes ) ) {
			// Create CountryTargetSearchParameter
			$languages = [];
			foreach ( $language_codes as $language_code ) {
				$language = new Language();
				$language->setId( self::getLanguageID( $language_code ) );
				$languages[] = $language;
			}
			$languageParameter = new LanguageSearchParameter();
			$languageParameter->setLanguages( $languages );
			$params[] = $languageParameter;
		}

		// Create network search parameter (optional).
		$networkSetting = new NetworkSetting();
		$networkSetting->setTargetGoogleSearch( true );
		$networkSetting->setTargetSearchNetwork( false );
		$networkSetting->setTargetContentNetwork( false );
		$networkSetting->setTargetPartnerSearchNetwork( false );

		$networkSearchParameter = new NetworkSearchParameter();
		$networkSearchParameter->setNetworkSetting( $networkSetting );
		$params[] = $networkSearchParameter;

		$selector->setSearchParameters( $params );
		$totalNumEntries = 0;
		$return          = Array();

		do {
			// Get related keywords.
			$NUM_OF_ATTEMPTS = 5;
			$attempts        = 0;
			$success         = false;


			do {
				try {
					$page    = $targetingIdeaService->get( $selector );
					$success = true;
				} catch ( ApiException $e ) {
					foreach ( $e->getErrors() as $error ) {
						if ( $error instanceof RateExceededError ) {
							if ( $attempts == 4 ) {
								throw $e;
							}
							sleep( pow( 2, $attempts ) );
						} else {
							throw $e;
						}
					}
					$attempts ++;
				}
			} while ( $attempts < $NUM_OF_ATTEMPTS && ! $success );

			if ( ! ( $success ) ) {
				return false;
			}


			// return search count.
			if ( $page->getEntries() != null ) {
				$totalNumEntries = $page->getTotalNumEntries();
				foreach ( $page->getEntries() as $targetingIdea ) {
					$data    = MapEntries::toAssociativeArray( $targetingIdea->getData() );
					$keyword = $data['KEYWORD_TEXT']->getValue();
					$count   = $data['SEARCH_VOLUME']->getValue();

					$return[ $keyword ] = $count ? $count : 0;
				}


			}

			$selector->getPaging()->setStartIndex(
				$selector->getPaging()->getStartIndex() + $pageLimit );
		} while ( $selector->getPaging()->getStartIndex() < $totalNumEntries );

		return $return;
	}

	public function getCostsEstimate( $phrases, $country_codes, $language_codes ) {
		$phrases = array_unique( $phrases );
		try {

			$adWordsServices         = new AdWordsServices();
			$trafficEstimatorService = $adWordsServices->get( $this->session, TrafficEstimatorService::class );

			// Create keywords. Up to 2000 keywords can be passed in a single request.
			$keywords = array();
			foreach ( $phrases as $phrase ) {
				$keyword = new Keyword;
				$keyword->setText( $phrase );
				$keyword->setMatchType( KeywordMatchType::EXACT );
				$keywords[] = $keyword;
			}

			// Create a keyword estimate request for each keyword.
			$keywordEstimateRequests = array();
			foreach ( $keywords as $keyword ) {
				$keywordEstimateRequest = new KeywordEstimateRequest();
				$keywordEstimateRequest->setKeyword( $keyword );
				$keywordEstimateRequests[] = $keywordEstimateRequest;
			}

			// Create ad group estimate requests.
			$adGroupEstimateRequest = new AdGroupEstimateRequest();
			$adGroupEstimateRequest->setKeywordEstimateRequests( $keywordEstimateRequests );
			$money = new Money();
			$money->setMicroAmount( get_config( 'adwords_max_cpc' ) );
			$adGroupEstimateRequest->setMaxCpc( $money );

			// Create campaign estimate requests.
			$campaignEstimateRequest = new CampaignEstimateRequest();
			$campaignEstimateRequest->setAdGroupEstimateRequests( [ $adGroupEstimateRequest ] );

			// Set targeting criteria. Only locations and languages are supported.
			$criteria = [];
			foreach ( $country_codes as $country_code ) {
				$country = new Location();
				$country->setId( self::getCountryID( $country_code ) );
				$criteria[] = $country;
			}

			foreach ( $language_codes as $language_code ) {
				$language = new Language();
				$language->setId( self::getLanguageID( $language_code ) );
				$criteria[] = $language;
			}

			if ( $criteria ) {
				$campaignEstimateRequest->setCriteria( $criteria );
			}

			// Create selector.
			$selector = new TrafficEstimatorSelector();
			$selector->setCampaignEstimateRequests( [ $campaignEstimateRequest ] );

			// Make the get request.
			$result = $trafficEstimatorService->get( $selector );

			// Display results.
			$res              = array();
			$keywordEstimates = $result->getCampaignEstimates()[0]->getAdGroupEstimates()[0]->getKeywordEstimates();
			for ( $i = 0; $i < sizeof( $keywordEstimates ); $i ++ ) {
				$keywordEstimateRequest = $keywordEstimateRequests[ $i ];
				// Skip negative keywords, since they don't return estimates.
				if ( ! $keywordEstimateRequest->getIsNegative() ) {
					$keyword         = $keywordEstimateRequest->getKeyword();
					$keywordEstimate = $keywordEstimates[ $i ];

					// Find the mean of the min and max values.
					$minmicroAmount = 0;
					if ( isset( $keywordEstimate ) and $keywordEstimate->getMin() and $keywordEstimate->getMin()->getAverageCpc() and $keywordEstimate->getMin()->getAverageCpc()->getMicroAmount() ) {
						$minmicroAmount = $keywordEstimate->getMin()->getAverageCpc()->getMicroAmount();
					}
					$maxmicroAmount = 0;
					if ( isset( $keywordEstimate ) and $keywordEstimate->getMax() and $keywordEstimate->getMax()->getAverageCpc() and $keywordEstimate->getMax()->getAverageCpc()->getMicroAmount() ) {
						$maxmicroAmount = $keywordEstimate->getMin()->getAverageCpc()->getMicroAmount();
					}

					$meanAverageCpc      = ( $minmicroAmount + $maxmicroAmount ) / 2;
					$meanAveragePosition = ( $keywordEstimate->getMin()->getAveragePosition() + $keywordEstimate->getMax()->getAveragePosition() ) / 2;
					$meanClicks          = ( $keywordEstimate->getMin()->getClicksPerDay() + $keywordEstimate->getMax()->getClicksPerDay() ) / 2;
					$meanTotalCost       = ( $keywordEstimate->getMin()->getTotalCost()->getMicroAmount() + $keywordEstimate->getMax()->getTotalCost()->getMicroAmount() ) / 2;

					$res[ $keyword->getText() ] = array(
						'average_cpc' => $meanAverageCpc / 1000000,
						'position'    => $meanAveragePosition,
						'clicks'      => $meanClicks,
						'daily_cost'  => $meanTotalCost / 1000000,
					);

				}

			}

			if ( ! empty( $res ) ) {
				return $res;
			} else {
				return false;
			}

		} catch ( Exception $e ) {
			throw $e;
		}

	}

	public static function getCountryID( $country_code ) {
		$country_id = null;
		#$h = fopen(DOCUMENT_ROOT . '/libs/adwords/src/Google/Api/Ads/AdWords/countrycodes.csv', 'r');
		$h = fopen( DIR_ROOT . '/countrycodes.csv', 'r' );
		while ( $country = fgetcsv( $h ) ) {
			if ( $country[1] == strtoupper( $country_code ) ) {
				$country_id = $country[2];
				break;
			}
		}
		fclose( $h );

		return $country_id;
	}

	public static function getLanguageID( $lang_code ) {
		$lang_id = null;

		#$h = fopen(DOCUMENT_ROOT . '/libs/adwords/src/Google/Api/Ads/AdWords/languagecodes.csv', 'r');
		$h = fopen( DIR_ROOT . '/languagecodes.csv', 'r' );
		while ( $lang = fgetcsv( $h ) ) {
			if ( $lang[1] == strtolower( $lang_code ) ) {
				$lang_id = $lang[2];
				break;
			}
		}
		fclose( $h );

		return $lang_id;
	}

	public function addSearchesLog( $atslegvards, $mekletajs, $searches, $statuss, $message = null ) {
		$sql = 'INSERT INTO ' . DB_PREF . 'piedavajumi_av_meklejumi_vesture (atslegvards, mekletajs_id, meklejumi, datums, statuss, message)
        VALUES ("' . $atslegvards . '", ' . $mekletajs . ', ' . ( $searches === null ? 'null' : $searches ) . ', "' . date( 'Y-m-d H:i:s' ) . '", ' . $statuss . ', "' . str_replace( '"', '\'', $message ) . '")';
		db_query( $sql );
	}

	public function addCpcLog( $atslegvards, $mekletajs, $cpc, $clicks, $statuss, $message = null ) {
		db_query( 'INSERT INTO ' . DB_PREF . 'piedavajumi_av_cpc_vesture (atslegvards, mekletajs_id, cpc, clicks, datums, statuss, message)
      VALUES ("' . $atslegvards . '", ' . $mekletajs . ', ' . ( $cpc === null ? 'null' : $cpc ) . ', ' . ( $clicks === null ? 'null' : $clicks ) . ', "' . date( 'Y-m-d H:i:s' ) . '", ' . $statuss . ', "' . str_replace( '"', '\'', $message ) . '")' );
	}

//	public function getAdGroupsStats( $campaigns, $startDate = null, $endDate = null ) {
//
//		// Get the service, which loads the required classes.
//		#$adGroupService = $this->user->GetService('AdGroupService', $this->version);
//		$adWordsServices      = new AdWordsServices();
//		$targetingIdeaService = $adWordsServices->get( $this->session, CampaignService::class );
//
//
//		// Create selector.
//		$selector = new Selector();
//		$selector->setFields( array( 'Name', 'Impressions', 'Clicks', 'AveragePosition', 'CampaignId' ) );
//		$selector->setOrdering( [ new OrderBy( 'Name', SortOrder::ASCENDING ) ] );
//
//		//  Date range
//		if ( $startDate && $endDate ) {
//
//			$dateRange      = new DateRange();
//			$dateRange->min = date( 'Ymd', strtotime( $startDate ) );
//			$dateRange->max = date( 'Ymd', strtotime( $endDate ) );
//
//			$selector->dateRange = $dateRange;
//		}
//
//		// Create predicates.
//		$selector->predicates[] =
//			new Predicate( 'CampaignId', 'IN', $campaigns );
//
//		// Create paging controls.
//		$selector->paging = new Paging( 0, self::RECOMMENDED_PAGE_SIZE );
//
//		$results = array();
//
//		do {
//			// Make the get request.
//			$page = $adGroupService->get( $selector );
//
//			// Display results.
//			if ( isset( $page->entries ) ) {
//				foreach ( $page->entries as $adGroup ) {
//					$results[] = array(
//						'ad_group'    => $adGroup->name,
//						'impressions' => $adGroup->stats->impressions,
//						'clicks'      => $adGroup->stats->clicks,
//						'avg_pos'     => $adGroup->stats->averagePosition,
//						'campaign_id' => $adGroup->campaignId,
//					);
//				}
//			}
//
//			// Advance the paging index.
//			$selector->paging->startIndex += self::RECOMMENDED_PAGE_SIZE;
//		} while ( $page->totalNumEntries > $selector->paging->startIndex );
//
//		return $results;
//	}

	function getCampaigns() {
		// Get the service, which loads the required classes.
		#$campaignService = $this->user->GetService('CampaignService', $this->version);
		$adWordsServices = new AdWordsServices();
		$campaignService = $adWordsServices->get( $this->session, CampaignService::class );

		// Create selector.
		$selector = new Selector();
		$selector->setFields( array( 'Id', 'Name' ) );
		$selector->setOrdering( [ new OrderBy( 'Name', 'ASCENDING' ) ] );

		// Create paging controls.
		$selector->setPaging( new Paging( 0, self::RECOMMENDED_PAGE_SIZE ) );

		$results = array();

		do {
			// Make the get request.
			$page = $campaignService->get( $selector );

			// Display results.
			foreach ( $page->getEntries() as $campaign ) {
				$results[ $campaign->getId() ] = $campaign->getName();
			}


			// Advance the paging index.
			$selector->getPaging()->setStartIndex( $selector->getPaging()->getStartIndex() + self::RECOMMENDED_PAGE_SIZE );
		} while ( $page->getTotalNumEntries() > $selector->getPaging()->getStartIndex() );

		return $results;
	}

	public function getIdeaCPC( $keywords, $country_codes, $language_codes = array()  ) {

		$pageLimit = 500;


		$adWordsServices      = new AdWordsServices();
		$targetingIdeaService = $adWordsServices->get( $this->session, TargetingIdeaService::class );

		// Search param array
		$params = array();

		$k_array = array();
		foreach ( $keywords as $k ) {
			$k_array[] = mb_strtolower( $k );
		}

		// Create selector.
		$selector = new TargetingIdeaSelector();
		$selector->setRequestType( RequestType::STATS );
		$selector->setIdeaType( IdeaType::KEYWORD );
		$selector->setRequestedAttributeTypes( [
			AttributeType::KEYWORD_TEXT,
			AttributeType::AVERAGE_CPC,
		] );


		// Create related to keyword search parameter.
		$relatedToKeywordSearchParameter = new RelatedToQuerySearchParameter();
		$relatedToKeywordSearchParameter->setQueries( $k_array );
		$params[] = $relatedToKeywordSearchParameter;

		// Create keyword match type search parameter to ensure unique results.

		if ( ! empty( $country_codes ) ) {
			// Create CountryTargetSearchParameter
			$locations = array();
			foreach ( $country_codes as $country_code ) {
				$location = new Location();
				$location->setId( self::getCountryID( $country_code ) );
				$locations[] = $location;
			}
			$locationParameter = new LocationSearchParameter();
			$locationParameter->setLocations( $locations );
			$params[] = $locationParameter;
		}
		if ( ! empty( $language_codes ) ) {
			// Create CountryTargetSearchParameter
			$languages = [];
			foreach ( $language_codes as $language_code ) {
				$language = new Language();
				$language->setId( self::getLanguageID( $language_code ) );
				$languages[] = $language;
			}
			$languageParameter = new LanguageSearchParameter();
			$languageParameter->setLanguages( $languages );
			$params[] = $languageParameter;
		}

		// Create network search parameter (optional).
		$networkSetting = new NetworkSetting();
		$networkSetting->setTargetGoogleSearch( true );
		$networkSetting->setTargetSearchNetwork( false );
		$networkSetting->setTargetContentNetwork( false );
		$networkSetting->setTargetPartnerSearchNetwork( false );

		$networkSearchParameter = new NetworkSearchParameter();
		$networkSearchParameter->setNetworkSetting( $networkSetting );
		$params[] = $networkSearchParameter;

		// Set selector paging (required for targeting idea service).
		$paging = new Paging();
		$paging->setStartIndex( 0 );
		$paging->setNumberResults( $pageLimit );
		$selector->setPaging( $paging );

		$selector->setSearchParameters( $params );

		// Get related keywords.


		$return          = Array();
		$totalNumEntries = 0;

		do {
			$NUM_OF_ATTEMPTS = 5;
			$attempts        = 0;
			$success         = false;

			do {
				try {
					$page    = $targetingIdeaService->get( $selector );
					$success = true;
				} catch ( ApiException $e ) {
					foreach ( $e->getErrors() as $error ) {
						if ( $error instanceof RateExceededError ) {
							if ( $attempts == 4 ) {
								throw $e;
							}
							sleep( pow( 2, $attempts ) );
						} else {
							throw $e;
						}
					}
					$attempts ++;
				}
			} while ( $attempts < $NUM_OF_ATTEMPTS && ! $success );

			if ( ! ( $success ) ) {
				return false;
			}


			// return search count.
			if ( $page->getEntries() !== null ) {
				$totalNumEntries = $page->getTotalNumEntries();
				foreach ( $page->getEntries() as $targetingIdea ) {

					$data    = MapEntries::toAssociativeArray( $targetingIdea->getData() );
					$keyword = $data['KEYWORD_TEXT']->getValue();

					$cpc = 0;
					if ( $data['AVERAGE_CPC']->getValue() and $data['AVERAGE_CPC']->getValue()->getMicroAmount() ) {
						$cpc = $data['AVERAGE_CPC']->getValue()->getMicroAmount();
					}

					$return[ $keyword ] = $cpc ? $cpc / 1000000 : 0;
				}

				//return $return;

			} else {
				return false;
			}
			$selector->getPaging()->setStartIndex(
				$selector->getPaging()->getStartIndex() + $pageLimit );

		} while ( $selector->getPaging()->getStartIndex() < $totalNumEntries );


		return ( $return );

	}

	public function downloadReport( $filePath, $params ) {

		$user = $this->user;

		// Load the service, so that the required classes are available.
		$adWordsServices         = new AdWordsServices();
		$reportDefinitionService = $adWordsServices->get( $this->session, ReportDefinitionService::class );

		// Create selector.
		$selector = new Selector();
		$selector->setFields( array( 'CampaignName', 'AdGroupName', 'Impressions', 'Clicks', 'AveragePosition', 'CampaignId', 'SearchImpressionShare' ) );

		// Filter out removed criteria.
		//$selector->predicates[] = new Predicate('Status', 'NOT_IN', array('REMOVED'));


		//  Date range
		$dateRange = new DateRange();

		if ( isset( $params['date']['min'] ) ) {
			$dateRange->setMin( date( 'Ymd', strtotime( $params['date']['min'] ) ) );
		}

		if ( isset( $params['date']['max'] ) ) {
			$dateRange->setMax( date( 'Ymd', strtotime( $params['date']['max'] ) ) );
		}

		$selector->setDateRange( $dateRange );

		// Create report definition.
		$reportDefinition = new ReportDefinition();
		$reportDefinition->setSelector( $selector );
		$reportDefinition->setReportName( 'Criteria performance report #' . uniqid() );
		$reportDefinition->setDateRangeType( ReportDefinitionDateRangeType::CUSTOM_DATE );

		$reportDefinition->setReportType( ReportDefinitionReportType::ADGROUP_PERFORMANCE_REPORT );
		$reportDefinition->setDownloadFormat( DownloadFormat::CSV );

		// Exclude criteria that haven't recieved any impressions over the date range.
		#$reportDefinition->includeZeroImpressions = FALSE;

		// Set additional options.
		#$options = array('version' => $this->version);
		#$options['includeZeroImpressions'] = false;

		//$reportSettings = (new ReportSettingsBuilder())->includeZeroImpressions(false)->build();

		// Download report.
		#$report_utils = new ReportUtils();
		#$report_utils->DownloadReport($reportDefinition, $filePath, $user, $options);

		$reportDownloader     = new ReportDownloader( $this->session );
		$reportDownloadResult = $reportDownloader->downloadReport( $reportDefinition );
		$reportDownloadResult->saveToFile( $filePath );

	}

	function getAccountHierarchy() {
		$user = $this->user;

		// Get the service, which loads the required classes.
		$managedCustomerService =
			$user->GetService( 'ManagedCustomerService', $this->version );

		// Create selector.
		$selector = new Selector();
		// Specify the fields to retrieve.
		$selector->fields = array( 'Login', 'CustomerId', 'Name' );

		// Make the get request.
		$graph = $managedCustomerService->get( $selector );

		// Display serviced account graph.
		if ( isset( $graph->entries ) ) {
			// Create map from customerId to parent and child links.
			$childLinks  = array();
			$parentLinks = array();
			if ( isset( $graph->links ) ) {
				foreach ( $graph->links as $link ) {
					$childLinks[ $link->managerCustomerId ][] = $link;
					$parentLinks[ $link->clientCustomerId ][] = $link;
				}
			}
			// Create map from customerID to account, and find root account.
			$accounts    = array();
			$rootAccount = null;
			foreach ( $graph->entries as $account ) {
				$accounts[ $account->customerId ] = $account;
				if ( ! array_key_exists( $account->customerId, $parentLinks ) ) {
					$rootAccount = $account;
				}
			}
			// The root account may not be returned in the sandbox.
			if ( ! isset( $rootAccount ) ) {
				$rootAccount             = new Account();
				$rootAccount->customerId = 0;
				$rootAccount->login      = $user->GetEmail();
			}
			// Display account tree.
			/*print "(Customer Id, Account Name/Login)\n";
			  DisplayAccountTree($rootAccount, NULL, $accounts, $childLinks, 0);*/

			return $accounts;
		} else {
			print "No serviced accounts were found.\n";
		}
	}

	//add by agris
	function getAccountHierarchyNew() {
		$user            = $this->user;
		$adWordsServices = new AdWordsServices();
		// Get the service, which loads the required classes.
		// $managedCustomerService =
		// $user->GetService('ManagedCustomerService', $this->version);
		$managedCustomerService = $adWordsServices->get( $this->session, ManagedCustomerService::class );

		// Create selector.
		$selector = new Selector();
		// Specify the fields to retrieve.
		$selector->setFields( array( 'CustomerId', 'Name' ) ); //'Login',
		// Create selector.


		// Make the get request.
		$graph = $managedCustomerService->get( $selector );
		var_dump( $graph->getEntries() );

		// Display serviced account graph.
		if ( $graph->getEntries() ) {
			// Create map from customerId to parent and child links.
			$childLinks  = array();
			$parentLinks = array();
			if ( isset( $graph->links ) ) {
				foreach ( $graph->links as $link ) {
					$childLinks[ $link->managerCustomerId ][] = $link;
					$parentLinks[ $link->clientCustomerId ][] = $link;
				}
			}
			// Create map from customerID to account, and find root account.
			$accounts    = array();
			$rootAccount = null;
			foreach ( $graph->getEntries() as $account ) {
				$accounts[ $account->getCustomerId ] = $account;
				if ( ! array_key_exists( $account->getCustomerId, $parentLinks ) ) {
					$rootAccount = $account;
				}
			}
			// The root account may not be returned in the sandbox.
			if ( ! isset( $rootAccount ) ) {
				$rootAccount             = new Account();
				$rootAccount->customerId = 0;
				$rootAccount->login      = $user->GetEmail();
			}
			// Display account tree.
			/*print "(Customer Id, Account Name/Login)\n";
			  DisplayAccountTree($rootAccount, NULL, $accounts, $childLinks, 0);*/
			var_dump( $account );
			var_dump( $rootAccount );

			return $accounts;
		} else {
			print "No serviced accounts were found.\n";
		}
	}
}

?>