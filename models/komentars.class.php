<?php
class Komentars extends BaseModel
{
  public static function model($classname=__CLASS__)
  {
    return parent::model($classname);
  }

  public function tableName()
  {
    return 'komentari';
  }

  public function save()
  {
    if(!$this->created || $this->created == 'CURRENT_TIMESTAMP') {
      $this->created = date('Y-m-d H:i:s');
    }

    parent::save();
  }
}
?>