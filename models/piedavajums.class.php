<?php
class Piedavajums extends BaseModel
{
  public $atslegvardi = array();
  public static function model($classname=__CLASS__)
  {
    return parent::model($classname);
  }

  public function tableName()
  {
    return 'piedavajumi';
  }

  public function unsafeValues()
  {
    return array('statuss');
  }

  public function validate()
  {
    if(!$this->klients) {
      $this->addError('klients', 'Klientu jānorāda obligāti');
    }

    if(strlen(str_replace(' ', '', $this->klients)) < 3) {
      $this->addError('klients', 'Klienta nosaukumā jābūt vismaz 3 simboliem (neskaitot atstarpes)');
    }

    if(is_admin() && !$this->pardevejs_id) {
      $this->addError('pardevejs_id', 'Menedžeri jānorāda obligāti');
    }

    if(!$this->mekletajs_id) {
      $this->addError('mekletajs_id', 'Meklētāju jānorāda obligāti');
    }

    if($this->s_variants && !$this->s_pamatojums) {
      $this->addError('s_pamatojums', 'Pamatojumu jānorāda obligāti');
    }

    if($this->t_variants && !$this->t_pamatojums) {
      $this->addError('t_pamatojums', 'Pamatojumu jānorāda obligāti');
    }

    if(!$this->epasts) {
      $this->addError('epasts', 'Epastu jānorāda obligāti');
    } else if(!filter_var($this->epasts, FILTER_VALIDATE_EMAIL)) {
      $this->addError('epasts', 'Norādīts nekorekts epasts.');
    }

    return !$this->hasErrors();
  }

  public function getAtslegvardi()
  {
    if(empty($this->atslegvardi) && $this->getId()) {
      return Atslegvards::model()->findAll(array(
        'conditions' => array(
          'owner_id = ' . esc($this->getId()),
        ),
        'order' => 'meklejumi desc, google_cpc desc, id'
      ));
    }

    //return $this->avs;
  }

  public function getMekletajs()
  {
    return Mekletajs::model()->findByPk($this->mekletajs_id);
  }

  public function getMeklejumi()
  {
    $total = 0;

    foreach($this->getAtslegvardi() as $av) {
      $total += $av->meklejumi;
    }

    return $total;
  }

  public function getPardevejs()
  {
    return Pardevejs::model()->findByPk($this->pardevejs_id);
  }

  public function cena($forced = false)
  {
    $override = $this->getOverridenCena();

    if($override === false || $override['cena'] == -1 || $forced) {
      $total = 0;
      $atslegvardi = $this->getAtslegvardi();

      foreach($atslegvardi as $atslegvards) {
        $total += $atslegvards->cena();
      }

      return $total;
    } else {
      return $override['cena'];
    }
  }

  public function cena_S($forced = false)
  {
    $override = $this->getOverridenCena();

    if($override === false || $override['cena_s'] == -1 || $forced) {
      $total = 0;
      $atslegvardi = $this->getAtslegvardi();

      foreach($atslegvardi as $atslegvards) {
        $total += $atslegvards->cena_S();
      }

      return $total;
    } else {
      return $override['cena_s'];
    }
  }

  public function cena_T($forced = false)
  {
    $override = $this->getOverridenCena();

    if($override === false || $override['cena_t'] == -1 || $forced) {
      $total = 0;
      $atslegvardi = $this->getAtslegvardi();

      foreach($atslegvardi as $atslegvards) {
        $total += $atslegvards->cena_T();
      }

      return $total;
    } else {
      return $override['cena_t'];
    }
  }

  public function gala_cena($wo_vat = false)
  {
    if($this->t_variants) {
      $total = $this->cena_T();
    } elseif($this->s_variants) {
      $total =  $this->cena_S();
    } else {
      $total =  $this->cena();
    }

    $total = ceil($total);

    if(!$wo_vat) {
      return $total + $total * VAT/100;
    } else {
      return $total;
    }
  }

  public function getOverridenCena()
  {
    $result = db_get_assoc(db_query('
      SELECT *
      FROM ' . DB_PREF . 'piedavajumi_kops_izm
      WHERE piedavajums_id = ' . esc($this->id) . '
      ORDER BY laiks DESC
      LIMIT 1
    '));

    if(empty($result)) {
      return false;
    } else {
      return $result;
    }
  }

  public function delete()
  {
    //  Izdzēš visus piesaistītos av.
    foreach((array)$this->getAtslegvardi() as $av) {
      $av->delete();
    }

    return parent::delete();
  }

  public function isStandard()
  {
    if(!$this->s_variants && !$this->t_variants) {
      return true;
    } else {
      return false;
    }
  }

  public function confirm($forced = false, $wosave = false){
    $can_confirm = true;

    $avs = $this->getAtslegvardi();

    //  3 poz && top3
    foreach($avs as $av) {
      $poz3 = is_3poz($av->atslegvards, $this->getMekletajs()->getValsts()->country_code);

      if($this->t_variants) {
        $top3 = is_top3($av->atslegvards);
      } else {
        $top3 = false;
      }

      if(!empty($poz3) || !empty($top3)) {
        $can_confirm = false;
        break;
      }

    }

    if($can_confirm || $forced) {
      $this->statuss = 1;
      $this->statuss_datums = date('Y-m-d H:i:s');

      if($wosave) {
        return true;
      } else {
        if($this->save()) {
          return true;
        }
      }
    }

  }

  /*public function find($criteria = array(), $debug = false)
  {
    if(!isset($criteria['conditions'])) {
      $criteria['conditions'] = array();
    }

    $criteria['conditions'][] = 'deleted = 0';

    return parent::find($criteria, $debug);
  }

  public function findAll($criteria = array(), $debug = false)
  {
    if(!isset($criteria['conditions'])) {
      $criteria['conditions'] = array();
    }

    $criteria['conditions'][] = 'deleted = 0';

    return parent::findAll($criteria, $debug);
  }

  public function findByPk($id)
  {
    $model = parent::findByPk($id);

    if($model && $model->deleted) {
      throw new Exception('Šis piedāvājums ir ticis dzēsts.');
    }

    return $model;
  }*/
}
?>