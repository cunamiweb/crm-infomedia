<?php
class Nozare extends BaseModel
{
  public $valsts;

  public static function model($classname=__CLASS__)
  {
    return parent::model($classname);
  }

  public function tableName()
  {
    return 'nozares';
  }

  public function getApaksnozares()
  {
    return Nozare::model()->findAll(array(
      'conditions' => array('parent_id = ' . $this->id),
      'order' => 'title_lv'
    ));
  }

  public function getVirsnozare()
  {
    if($this->parent_id) {
      return Nozare::model()->findByPk($this->parent_id);
    }
  }

  // NB! šie ir av no līgumiem nevis standarta av piedāvājumiem
  public function getAtslegvardi()
  {
    $infomedia = PielikumsInfomedia::model()->findAll(array('conditions' => array('nozare_id = ' . $this->id)));
    $tiessaite = PielikumsTiessaite::model()->findAll(array('conditions' => array('nozare_id = ' . $this->id)));
    $yandex    = PielikumsYandex::model()->findAll(array('conditions' => array('nozare_id = ' . $this->id)));
    $citi      = PielikumsCits::model()->findAll(array('conditions' => array('nozare_id = ' . $this->id)));

    return array_merge($infomedia, $tiessaite, $yandex, $citi);
  }

  public function validate()
  {
    if(!$this->title_lv) {
      $this->addError('title_lv', 'Nosaukums jānorāda obligāti');
    }

    if(!$this->country) {
      $this->addError('country', 'Valsti jānorāda obligāti');
    }

    return !$this->hasErrors();
  }

  public function getStandartaPiedavajums($criteria = array())
  {
    $criteria['conditions'][] = 'nozare_id = ' . $this->id;
    return StandartaPiedavajums::model()->findAll($criteria);
  }

  public function findAll($criteria = array(), $debug = false)
  {
    if(isset($criteria['conditions'])) {
      $criteria['conditions'][] = 'disabled = 0';
    } else {
      $criteria['conditions'] = array('disabled = 0');
    }

    return parent::findAll($criteria, $debug);
  }
}
?>