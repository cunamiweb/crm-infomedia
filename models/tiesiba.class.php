<?php
class Tiesiba extends BaseModel
{
  public static function model($classname=__CLASS__)
  {
    return parent::model($classname);
  }

  public function tableName()
  {
    return 'tiesibas';
  }

  public function getGroups()
  {
    $result = db_get_assoc_all(db_query('SELECT * FROM ' . DB_PREF . 'tiesibas_grupas WHERE tiesibas_id = ' . esc($this->id)));

    $data = array();
    if(!empty($result)) {
      foreach($result as $row) {
        $data[$row['grupa']] = $row['piekluve'];
      }
    }

    if(!isset($data['admin'])) {
      $data['admin'] = -1;
    }

    if(!isset($data['sales'])) {
      $data['sales'] = -1;
    }

    if(!isset($data['partner'])) {
      $data['partner'] = -1;
    }

    return $data;
  }

  public function getUser($user_id)
  {
    $result = db_get_assoc(db_query('SELECT * FROM ' . DB_PREF . 'tiesibas_lietotaji WHERE tiesibas_id = ' . esc($this->id) . ' AND lietotajs_id = ' . esc($user_id)));

    if($result) {
      return $result['piekluve'];
    } else {
      return 0;
    }
  }
}
?>