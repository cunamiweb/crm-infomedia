<?php
class ForumCategory extends BaseModel
{
  private $topics;
  private $hasUnread;

  public static function model($classname=__CLASS__)
  {
    return parent::model($classname);
  }

  public function tableName()
  {
    return 'forums_kategorijas';
  }

  public function getTopics()
  {
    if(empty($this->topics)) {
      $sql = "
        SELECT
          f.id,
          (
            SELECT COUNT(*)
            FROM `".DB_PREF."forums_atverts`
            WHERE
              temas_id = f.id AND
              pardeveja_id = ".$_SESSION['user']['id']."
          ) as tema_atverta,
          (
            SELECT COUNT(*)
            FROM `".DB_PREF."forums` sf
            LEFT JOIN `".DB_PREF."forums_atverts` sa ON (
              sa.temas_id = sf.id AND
              sa.pardeveja_id = ".$_SESSION['user']['id']."
            )
            WHERE
              sf.parent_id = f.id AND
              sa.pardeveja_id IS NULL
          ) as jaunas_atbildes
        FROM `".DB_PREF."forums` f
        WHERE
          (f.parent_id IS NULL OR f.parent_id = 0)
          AND f.category_id = " . esc($this->id);


      $query = db_query($sql);

      while($row = db_get_assoc($query)) {
        $topic = ForumTopic::model()->findByPk($row['id']);
        $this->topics[] = $topic;

        if(!$row['tema_atverta'] || $row['jaunas_atbildes']) {
          $this->hasUnread = true;
        }
      }
    }

    return $this->topics;
  }

  public function validate()
  {
    if(!$this->nosaukums) {
      $this->addError('nosaukums', 'Nosaukumu jānorāda obligāti');
    }

    return !$this->hasErrors();
  }

  public function hasUnread()
  {
    $this->getTopics();
    return $this->hasUnread;
  }
}
?>