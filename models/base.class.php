<?php
/**
 * Modeļa pamata klase
 * Definējot jaunu modeli šo vajag paplašināt un kā minimums overraidot metodi tableName,
 * kurai jāatgriež konkrētā modeļa tabulas nosaukumu bez prefiksa.
 *
 * @author Davis Fridenvalds davis@datateks.lv
 * @since 2012-07-13
 */
class BaseModel
{
  public $errors = array();
  public $criteria = array();
  public $init_cols = array();

  public function __construct($init_cols = array())
  {
    if(!empty($init_cols)) {
      $this->init_cols = $init_cols;
    }

    $this->init();
  }

  static public function model($classname=__CLASS__, $init_cols = array())
  {
    $model = new $classname($init_cols);

    return $model;
  }

  public function init()
  {
    if(empty($this->init_cols)) {
      $this->init_cols = $this->loadSchema();
    }

    if(!empty($this->init_cols)) {
      foreach($this->init_cols as $column) {
        $this->{$column['Field']} = $column['Default'];
      }
    }

    $this->setDefaultCriteria();
  }

  public function loadSchema()
  {
    //  Pārbauda vai ir iekešota un vai nav jau veca (10 sec)
    /*$filename = DOCUMENT_ROOT . '/tmp/table_schemas/' . $this->tableName();

    clearstatcache(true,$filename);
    if(file_exists($filename) && (time() - filemtime($filename)) < 10) {
      $schema = unserialize(file_get_contents($filename));
    } else {
      //  Nav iekešota - ielādējam no db un iekešojam
      $schema = db_get_assoc_all(db_query('SHOW COLUMNS FROM ' . DB_PREF . $this->tableName()));
      //  Store schema to file
      $handle = fopen($filename, 'w');
      fwrite($handle, serialize($schema));
      fclose($handle);
    }      */

    if(isset($GLOBALS['schemas'][$this->tableName()])) {
      $schema = $GLOBALS['schemas'][$this->tableName()];
    } else {
      $schema = db_get_assoc_all(db_query('SHOW COLUMNS FROM ' . DB_PREF . $this->tableName()));
      $GLOBALS['schemas'][$this->tableName()] = $schema;
    }

    return $schema;
  }

  public function findByPk($id)
  {
    $data = db_get_assoc(db_query('
      SELECT * FROM ' . DB_PREF . $this->tableName() . ' WHERE ' . $this->idName() . ' = "' . esc($id) . '" LIMIT 1
    '));

    if(!empty($data)) {
      $this->setValues($data, true);

      return $this;
    }
  }

  public function find($criteria = array(), $debug = false)
  {
    $criteria['limit'] = 1;
    $result = $this->findAll($criteria, $debug);

    /*if($result[0]) {
      return $result;
    } else {
      return null;
    } */

    return array_shift($result);
  }

  public function findAll($criteria = array(), $debug = false)
  {
    $sql = 'SELECT * FROM ' . DB_PREF . $this->tableName();

    //  Criteria

    //  Conditions
    $conditions = array();

    if(!empty($criteria['conditions'])) {
      $conditions = array_merge($criteria['conditions'], $conditions);
    }

    $conditions = array_merge($this->criteria['conditions'], $conditions);

    if(!empty($conditions)) {
      $sql .= ' WHERE ' . implode(' AND ', $conditions);
    }

    //  Order
    if(!empty($criteria['order'])) {    //die;
      $sql .= ' ORDER BY ' . $criteria['order'];
    }

    //  Limit
    if(isset($criteria['limit']) ) {
      $sql .= ' LIMIT ' . (isset($criteria['offset']) ? (int)$criteria['offset'] . ', ' : '') . (int)$criteria['limit'];
    }

    if($debug) {
      echo $sql;
    }

    $rows = db_get_assoc_all(db_query($sql));

    $models = array();
    if(!empty($rows)) {
      foreach($rows as $row) {
        $model = $this->model(get_class($this), $this->init_cols)->setValues($row, true);
        $models[$model->getId()] = $model;
      }
    }

    return $models;
  }

  public function setValues($data, $forced = false)
  {
    $unsafeValues = $this->unsafeValues();
    foreach($data as $name => $value) {
      if($forced || (isset($this->$name) || property_exists($this, $name)) && (!in_array($name, $unsafeValues))) {
        $this->{$name} = $value;
      }
    }
    return $this;
  }

  public function unsafeValues()
  {
    return array();
  }

  public function is_initial($name)
  {
    $is = false;
    foreach($this->init_cols as $ic) {
      if($ic['Field'] == $name) {
        $is = true;
        break;
      }
    }
    return $is;
  }

  public function setDefaultCriteria()
  {
    //  Set default criteria
    $this->criteria = array(
      'conditions' => array(),
      'limit' => null,
      'offset' => null,
      'order' => null,
    );
  }

  public function asArray()
  {
    $vars = get_object_vars($this);
    unset($vars['errors']);
    unset($vars['criteria']);
    unset($vars['init_cols']);
    return $vars;
  }

  public function save()
  {
    if($this->validate()) {
      $vals = $this->asArray();

      if($this->id) {
        $sql = 'UPDATE ' . DB_PREF . $this->tableName() . ' ';

        $sets = array();
        foreach($vals as $name => $value) {
          //if($value !== null && $name != $this->idName() && $this->is_initial($name)) {
          if($name != $this->idName() && $this->is_initial($name)) {
            if($value === null) {
              $sets[] = $name . ' = null';
            } else {
              $sets[] = $name . ' = "' . esc($value) . '"';
            }
          }
        }

        if(!empty($sets)) {
          $sql .= ' SET ' . implode(', ', $sets);
          $sql .= ' WHERE ' . $this->idName() . ' = "' . $this->getId() . '"';

          if(db_query($sql)) {
            return true;
          }
        }

      } else {

        $sql = 'INSERT INTO ' . DB_PREF . $this->tableName() . ' ';

        $cols = array();
        foreach($vals as $name => $value) {
          if($value !== null && $this->is_initial($name)) {
            $cols[$name] = esc($value);
          }
        }

        if(!empty($cols)) {
          $sql .= '('.implode(', ', array_keys($cols)).')';
          $sql .= 'VALUES ("'.implode('", "', $cols).'")';

          if(db_query($sql)) {
            $this->id = db_last_id();
            return true;
          }
        }
      }
    }
  }

  public function validate()
  {
    return true;
  }

  public function delete()
  {
    if($this->id) {
      if(db_query('DELETE FROM ' .  DB_PREF . $this->tableName() . ' WHERE ' .  $this->idName() . '=' . $this->getId())) {
        return true;
      } else {
        return false;
      }
    }
  }

  public function addError($attr, $text)
  {
    if(!isset($this->errors[$attr])) {
      $this->errors[$attr] = array();
    }
    $this->errors[$attr][] = $text;
  }

  public function hasErrors()
  {
    return !empty($this->errors);
  }

  public function tableName()
  {
    return '';
  }

  public function idName()
  {
    return 'id';
  }

  public function getId()
  {
    return $this->{$this->idName()};
  }
}

?>