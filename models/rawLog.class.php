<?php
class RawLog extends BaseModel
{
  public static function model($classname=__CLASS__)
  {
    return parent::model($classname);
  }

  public function tableName()
  {
    return 'raw_log';
  }

  public function save()
  {
    //  Serializē visu
    $this->post = serialize($_POST);
    $this->get = serialize($_GET);
    $this->session = serialize($_SESSION);
    $this->sqls = serialize($this->sqls);

    $this->user_id = isset($_SESSION['user']['id']) ?  $_SESSION['user']['id'] : null;
    $this->time = date('Y-m-d H:i:s');
    $this->request_uri = $_SERVER["REQUEST_URI"];

    parent::save();
  }
}
?>