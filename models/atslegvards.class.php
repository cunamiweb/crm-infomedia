<?php
class Atslegvards extends BaseModel
{
  public $number;

  public static function model($classname=__CLASS__)
  {
    return parent::model($classname);
  }

  public function tableName()
  {
    return 'piedavajumi_av';
  }

  public function unsafeValues()
  {
    return array('owner_id', 'tips', 'meklejumi', 'google_cpc');
  }

  public function validate()
  {
    global $_vars;

    //  Av
    if(!$this->atslegvards) {
      $this->addError('atslegvards', 'Atslēgvārdu jānorāda obligāti');
    }

		$piedavajums = $this->getPiedavajums();

    if(!$this->mekletajs_id) $this->mekletajs_id = $piedavajums->mekletajs_id;

    //  Duplikāti
    $existing = $piedavajums->getAtslegvardi();

    if(!$this->meklejumi){
	  	$this->meklejumi = $this->getSearchCount();
	  }

		$option = Uzstadijums::model()->findByPk('min_meklejumi_av');
   	$min_allowed_searches = $option->value;

    if(!$this->meklejumi || $min_allowed_searches > $this->meklejumi){
    	$error = $this->atslegvards.': Minimālajam meklējumu skaitam vajag būt [min_count]. Šim atslēgvārdam meklējumu skaits ir [count]';
			$error = str_replace("[min_count]", $min_allowed_searches, $error);
			$error = str_replace("[count]", number_format($this->meklejumi), $error);
     	$this->addError('atslegvards', $error);
    }

    $no_dupl = true;
    foreach((array)$existing as $av) {
      if($av->atslegvards == $this->atslegvards && $av->id != $this->id) {
        $no_dupl = false;
      }
    }
    if(!$no_dupl) {
      $this->addError('atslegvards', 'Šāds atslēgvārds jau eksistē');
    }

    return !$this->hasErrors();
  }

  public function getPiedavajums()
  {
    return Piedavajums::model()->findByPk($this->owner_id);
  }

  public function getMekletajs()
  {
    return Mekletajs::model()->findByPk($this->mekletajs_id);
  }

  public function getSearchCount(){

  	$adwords = new Adwords;
    $meklejumi = 0;

		try {
      $result = $adwords->getMonthlySearches(array($this->atslegvards), array($this->getMekletajs()->getValsts()->country_code)/*, array($this->getMekletajs()->getValsts()->language_code)*/);
      if($result && isset($result[mb_strtolower($this->atslegvards)])) {
        $meklejumi = $result[mb_strtolower($this->atslegvards)];
			}
		} catch (Exception $e) {
      $adwords->addSearchesLog($this->atslegvards, $this->mekletajs_id, null, -2, $e->getMessage());
      return false;
    }

		return $meklejumi;

  }

  public function updateSearchCount()
  {
    $status = 0;
    $adwords = new Adwords;

    try {
      $result = $adwords->getMonthlySearches(array($this->atslegvards), array($this->getMekletajs()->getValsts()->country_code)/*, array($this->getMekletajs()->getValsts()->language_code)*/);
      if($result && isset($result[$this->atslegvards])) {
        $this->meklejumi = $result[$this->atslegvards];
        if($this->save()) {
          $status = 1;
        }

        $adwords->addSearchesLog($this->atslegvards, $this->mekletajs_id, $this->meklejumi, $status);
        return (boolean)$status;
      } else {
        $adwords->addSearchesLog($this->atslegvards, $this->mekletajs_id, null, -1);
        return false;
      }
    } catch (Exception $e) {
      $adwords->addSearchesLog($this->atslegvards, $this->mekletajs_id, null, -2, $e->getMessage());
      return false;
    }
  }

  public function updateCPC()
  {
    $status = 0;
    try {
      $adwords = new Adwords;

      $result = $adwords->getCostsEstimate(array($this->atslegvards), array($this->getMekletajs()->getValsts()->country_code), array($this->getMekletajs()->getValsts()->language_code));

      $kwt_result = $adwords->getIdeaCPC(array($this->atslegvards), array($this->getMekletajs()->getValsts()->country_code), array($this->getMekletajs()->getValsts()->language_code));

      if($result && isset($result[$this->atslegvards])) {
        //$cpc = $result[$this->atslegvards]['average_cpc']; // In adwords account currency
        $cpc = $kwt_result[$this->atslegvards]; // In adwords account currency
        //$cpc_converted = ADWORDS_CURRENCY != 'EUR' ? convert_currency($cpc, ADWORDS_CURRENCY, 'EUR') : $cpc;
        //$this->google_cpc = ADWORDS_CURRENCY != 'EUR' ? convert_currency($cpc, ADWORDS_CURRENCY, 'EUR') : $cpc;
        $this->google_cpc = $cpc;
        $this->google_clicks = $result[$this->atslegvards]['clicks'];
        if($this->save()) {
          $status = 1;
        }

        $adwords->addCpcLog($this->atslegvards, $this->mekletajs_id, $this->google_cpc, $this->google_clicks, $status);
        return (boolean)$status;
      } else {
        $adwords->addCpcLog($this->atslegvards, $this->mekletajs_id, null, null, $status);
        return false;
      }

    } catch (Exception $e) {
      $adwords->addCpcLog($this->atslegvards, $this->mekletajs_id, null, null, -2, $e->getMessage());
    }
  }

  public function getLastCPCUpdate()
  {
    return db_get_assoc(db_query('SELECT * FROM ' . DB_PREF . 'piedavajumi_av_cpc_vesture WHERE av_id = ' . $this->id . ' ORDER BY datums DESC LIMIT 1'));
  }

  public function getLastSearchesUpdate()
  {
    return db_get_assoc(db_query('SELECT * FROM ' . DB_PREF . 'piedavajumi_av_meklejumi_vesture WHERE av_id = ' . $this->id . ' ORDER BY datums DESC LIMIT 1'));
  }

  public function googleCena($currency = 'EUR')
  {
    $total = $this->google_cpc * $this->google_clicks * 31;

    if($currency != 'EUR') {
      $total = convert_currency($total, 'EUR', $currency);
    }
    return $total;
  }

  public function getConfigSufix()
  {
    /*$country_code = $this->getMekletajs()->getValsts()->country_code;
    if($country_code == 'lv') {
      $config_sufix = '';
    } else {
      $config_sufix = '_'.$country_code;
    }

    return $config_sufix; */   return '';
  }

  public function cena($currency = 'EUR')
  {

    $config_sufix = $this->getConfigSufix();

    if($this->override) {
      //  Override
      $total = $this->override;
    } else {

      $total = $this->meklejumi * $this->google_cpc * get_config('piedavajumi_x'.$config_sufix);
    }

    if($this->tips == 'av') {
      $number = $this->getNumber();

      if($number == 1) {
        $total = $total > get_config('piedavajumi_min1') ? $total : get_config('piedavajumi_min1'.$config_sufix);
      }

      if($number == 2) {
        $total = $total > get_config('piedavajumi_min2') ? $total : get_config('piedavajumi_min2'.$config_sufix);
      }

      if($number == 3) {
        $total = $total > get_config('piedavajumi_min3') ? $total : get_config('piedavajumi_min3'.$config_sufix);
      }

      if($number >= 4 && $number <= 10) {
        $total = $total + get_config('piedavajumi_n49'.$config_sufix);
      }

      if($number > 10) {
        $total = $total + get_config('piedavajumi_n10'.$config_sufix);
      }
    }

    //$total += $total * ($this->izmaina / 100);

    if($currency != 'EUR') {
      $total = convert_currency($total, 'EUR', $currency);
    }
    return round($total, 2);
  }

  public function cena_S($currency = 'EUR')
  {
    $config_sufix = $this->getConfigSufix();

    if(!$this->override_s) {
    	$total =  $this->meklejumi * $this->google_cpc * get_config('piedavajumi_xs'.$config_sufix);
    } else {
      //  Override
      $total = $this->override_s;
    }

    //$total += $total * ($this->izmaina / 100);

    if($currency != 'EUR') {
      $total = convert_currency($total, 'EUR', $currency);
    }
    return round($total, 2);
  }

  public function cena_T($currency = 'EUR')
  {
    $config_sufix = $this->getConfigSufix();

    if(!$this->override_t) {
      $total =  $this->meklejumi * $this->google_cpc * get_config('piedavajumi_xt'.$config_sufix);
    } else {
      //  Override
      $total = $this->override_t;
    }

    //$total += $total * ($this->izmaina / 100);


    if($currency != 'EUR') {
      $total = convert_currency($total, 'EUR', $currency);
    }
    return round($total, 2);
  }

  public function findAll($criteria = array(), $debug = false)
  {
    $criteria['conditions'][] = 'tips = "'.$this->getType().'"';
    return parent::findAll($criteria, $debug);
  }

  public function findByPk($id, $ignore_type = false)
  {
    $model = parent::findByPk($id);
    if($ignore_type || $model->tips == $this->getType()) {
      return $model;
    } else {
      throw new Exception('Wrong object.');
    }
  }

  public function getType()
  {
    return 'av';
  }

  public function getNozare()
  {
    if($this->nozare_id) {
      return Nozare::model()->findByPk($this->nozare_id);
    }
  }

  public function getHistory($offset = false, $limit = false)
  {
    $sql = '
      SELECT *
      FROM ' . DB_PREF . 'piedavajumi_av_cpc_vesture h
      WHERE
        h.statuss = 1 AND
        h.atslegvards = "'.$this->atslegvards.'" AND
        h.mekletajs_id = '.$this->mekletajs_id.'
      ORDER BY h.datums DESC
    ';

    if($offset !== false && $limit !== false) {
      $sql .= ' LIMIT ' . $offset . ', ' .  $limit;
    }
    return db_get_assoc_all(db_query($sql));
  }

  public function save()
  {
    $this->atslegvards = mb_strtolower(trim($this->atslegvards));

    return parent::save();
  }

  public function getNumber()
  {
    if(!$this->number) {
      $all = array_values($this->getPiedavajums()->getAtslegvardi());
      $count = count($all);
      for($i = 1; $i <= $count; $i++) {
        if($all[($i-1)]->id == $this->id) {
          $this->number = $i;
        }
      }
    }

    return $this->number;
  }

  public function getAlikes()
  {
    $class = get_class($this);
    return call_user_func(array($class, 'model'))->findAll(array(
    //return $class::model()->findAll(array(
      'conditions' => array(
        'atslegvards = "' . esc($this->atslegvards) . '"',
        'tips = "' . esc($this->tips) . '"',
        'mekletajs_id = ' . esc($this->mekletajs_id),
      ),
    ));
  }

  /**
   * Sinhronizē datus starp vienādiem a/v
   */
  public function synchronize($params = array())
  {
    $alikes = $this->getAlikes();

    foreach((array)$alikes as $alike) {
      foreach($params as $param) {
        if(property_exists($this, $param)) {
          $alike->$param = $this->$param;
        }
      }
      $alike->save();
    }
  }

  /**
   * Atrod atbilstošu standarta a/v
   */
  public function findMatchingStd()
  {
    return StandartaPiedavajums::model()->find(array('conditions' => array(
      'atslegvards = "' . esc($this->atslegvards) . '"',
      'mekletajs_id = ' . esc($this->mekletajs_id),
    )), false);
  }

  //  Helper function for logging
  public function priceArray()
  {
    return array(
      'cena' => $this->cena(),
      'cena_s' => $this->cena_s(),
      'cena_t' => $this->cena_t(),
    );
  }

  /**
   * Ielog'o cenu izmaiņu ja tāda ir
   */
  public function logPriceChange($reason, $last)
  {
    //  Last log
    /*if(!$last) {
      $last = db_get_assoc(db_query('
        SELECT pec as cena, pec_s as cena_s, pec_t as cena_t FROM '. DB_PREF . 'piedavajumi_cenas_log
        WHERE
          atslegvards = "'.esc($this->atslegvards).'" AND
          mekletajs_id = '.$this->mekletajs_id.'
        ORDER BY datums DESC
        LIMIT 1
      '));
    }   */

    if($last) {
      $cena = $this->cena(); // atrdarbibai
      $cena_s = $this->cena_s(); // atrdarbibai
      $cena_t = $this->cena_t(); // atrdarbibai

      if(bccomp($last['cena'], $cena, 2) || bccomp($last['cena_s'], $cena_s, 2) || bccomp($last['cena_t'], $cena_t, 2)) {
        //  Cena atšķiras. Veidojam jaunu logu.
        /*var_dump($last);
        var_dump($this->asArray());
        var_dump(bccomp($last['cena'], $this->cena(), 2), bccomp($last['cena_s'], $this->cena_s(), 2), bccomp($last['cena_t'], $this->cena_t(), 2));
        var_dump($this->cena_t()); */

        if(db_query('
          INSERT INTO ' . DB_PREF . 'piedavajumi_cenas_log
          (atslegvards, mekletajs_id, izmainas_iemesls, pirms, pirms_s, pirms_t, pec, pec_s, pec_t, datums)
          VALUES
          ("'.esc($this->atslegvards).'", '.esc($this->mekletajs_id).', "'.$reason.'",
            '.(isset($last['cena']) ? $last['cena'] : $cena).',
            '.(isset($last['cena_s']) ? $last['cena_s'] : $cena_s).',
            '.(isset($last['cena_t']) ? $last['cena_t'] : $cena_t).',
            '.$cena.', '.$cena_s.', '.$cena_t.',
            "'.date('Y-m-d H:i:s').'")
        ')) {
          return true;
        }
      }
    }
  }
}
?>