<?php
class Ligums extends BaseModel
{
  public static function model($classname=__CLASS__)
  {
    return parent::model($classname);
  }

  public function tableName()
  {
    return 'ligumi';
  }

  public function isGoogle()
  {
    $sql = '
      SELECT l.id, MAX(a.id), MAX(ca.id), MAX(co.id), MAX(d.id), MAX(k.id), MAX(ci.id), MAX(inf.id), MAX(ti.id)
      FROM new_ligumi l
      LEFT JOIN new_mpk_analytics a ON a.ligums_id = l.id AND a.enabled = 1 AND a.site = "google"
      LEFT JOIN new_mpk_categories ca ON ca.ligums_id = l.id AND ca.status = 1 AND ca.site = "google"
      LEFT JOIN new_mpk_conversions co ON co.ligums_id = l.id AND co.enabled = 1 AND co.site = "google"
      LEFT JOIN new_mpk_displays d ON d.ligums_id = l.id AND d.enabled = 1 AND d.site = "google"
      LEFT JOIN new_mpk_keywords k ON k.ligums_id = l.id AND k.site = "google"
      LEFT JOIN new_pielikumi_citi ci ON ci.liguma_id = l.id
      LEFT JOIN new_pielikumi_infomedia inf ON inf.liguma_id = l.id
      LEFT JOIN new_pielikumi_tiessaite ti ON ti.liguma_id = l.id
      WHERE l.id = '.$this->id.'
      GROUP BY l.id
    ';

    $result = db_get_assoc(db_query($sql));
    $result['id'] = null;

    return array_filter($result) ? true : false;
  }

  public function isYandex()
  {
    $sql = '
      SELECT l.id, MAX(a.id), MAX(ca.id), MAX(co.id), MAX(d.id), MAX(k.id), MAX(ci.id)
      FROM new_ligumi l
      LEFT JOIN new_mpk_analytics a ON a.ligums_id = l.id AND a.enabled = 1 AND a.site = "yandex"
      LEFT JOIN new_mpk_categories ca ON ca.ligums_id = l.id AND ca.status = 1 AND ca.site = "yandex"
      LEFT JOIN new_mpk_conversions co ON co.ligums_id = l.id AND co.enabled = 1 AND co.site = "yandex"
      LEFT JOIN new_mpk_displays d ON d.ligums_id = l.id AND d.enabled = 1 AND d.site = "yandex"
      LEFT JOIN new_mpk_keywords k ON k.ligums_id = l.id AND k.site = "yandex"
      LEFT JOIN new_pielikumi_yandex ci ON ci.liguma_id = l.id
      WHERE l.id = '.$this->id.'
      GROUP BY l.id
    ';

    $result = db_get_assoc(db_query($sql));
    $result['id'] = null;

    return array_filter($result) ? true : false;
  }

  public function getBillText()
  {
    $is_google = $this->isGoogle();
    $is_yandex = $this->isYandex();

    if($is_google && $is_yandex) {
      $text = 'Reklāmas izvietošana Google AdWords un Yandex Direct';
    } elseif($is_google) {
      $text = 'Reklāmas izvietošana Google AdWords';
    } elseif($is_yandex) {
      $text = 'Reklāmas izvietošana Yandex Direct';
    } else {
      $text = 'Reklāmas pakalpojumi';
    }

    return $text;
  }

  public function getPielikumiTiessaite()
  {
    if($this->id) {
      return PielikumsTiessaite::model()->findAll(array('conditions'=>array('liguma_id' => $this->id)));
    } else {
      return array();
    }
  }

  public function getPielikumiCits()
  {
    if($this->id) {
      return PielikumsCits::model()->findAll(array('conditions'=>array('liguma_id' => $this->id)));
    } else {
      return array();
    }
  }

  public function getPielikumiInfomedia()
  {
    if($this->id) {
      return PielikumsInfomedia::model()->findAll(array('conditions'=>array('liguma_id' => $this->id)));
    } else {
      return array();
    }
  }

  public function getPielikumiYandex()
  {
    if($this->id) {
      return PielikumsYandex::model()->findAll(array('conditions'=>array('liguma_id' => $this->id)));
    } else {
      return array();
    }
  }

  public function getMpkAnalytics($site)
  {
    if($this->id) {
      $analytics = mpkAnalytics::model()->find(array('conditions' => array('ligums_id = ' . esc($this->id), 'site = "' . esc($site) . '"')));
    } else {
      $analytics = null;
    }

    if(!$analytics) {
      $analytics = new mpkAnalytics;
      $analytics->ligums_id = $this->id;
      $analytics->site = $site;
    }

    return $analytics;
  }

  public function getMpkDisplayMisc($site)
  {
    if($this->id) {
      $dm = mpkDisplayMisc::model()->find(array('conditions' => array('ligums_id = ' . esc($this->id), 'site = "' . esc($site) . '"')));
    } else {
      $dm = null;
    }

    if(!$dm) {
      $dm = new mpkDisplayMisc;
      $dm->ligums_id = $this->id;
      $dm->site = $site;
    }

    return $dm;
  }

  public function getMpkConversion($site = null)
  {
    if($this->id) {

      $conditions = array('ligums_id = ' . esc($this->id));

      if($site) {
        $conditions[] = 'site = "' . esc($site) . '"';
      }

      $conv = mpkConversion::model()->find(array('conditions' => $conditions));
    } else {
      $conv = null;
    }

    if(!$conv) {
      $conv = new mpkConversion;
      $conv->ligums_id = $this->id;
      $conv->site = $site;
    }

    return $conv;
  }

  public function getMpkDisplay($site, $code = null)
  {
    if($this->id) {

      $conditions = array(
        'ligums_id = ' . esc($this->id),
      );

      if($site) {
        $conditions[] = 'site = "' . esc($site) . '"';
      }

      if($code) {
        $conditions[] = 'code = "' . esc($code) . '"';
      }

      $display = mpkDisplay::model()->find(array('conditions'=>$conditions));
    } else {
      $display = null;
    }

    if(!$display) {
      $display = new mpkDisplay;
      $display->ligums_id = $this->id;
      $display->site = $site;
      $display->code = $code;
    }

    return $display;
  }

  public function getMpkKeywords($site = null)
  {
    if($this->id) {

      $conditions = array('ligums_id = ' . esc($this->id));

      if($site) {
        $conditions[] = 'site = "' . esc($site) . '"';
      }

      return mpkKeyword::model()->findAll(array('conditions' => $conditions));
    } else {
      return array();
    }
  }

  public function getMpkCategories($site = null)
  {
    if($this->id) {

      $conditions = array('ligums_id = ' . esc($this->id));

      if($site) {
        $conditions[] = 'site = "' . esc($site) . '"';
      }

      return mpkCategory::model()->findAll(array('conditions' => $conditions));
    } else {
      return array();
    }
  }

  public function getMpkOther()
  {
    if($this->id) {
      $other = mpkOther::model()->find(array('conditions' => array(
        'ligums_id = ' . esc($this->id),
      )));
    } else {
      $other = null;
    }

    if(!$other) {
      $other = new mpkOther;
      $other->ligums_id = $this->id;
    }

    return $other;
  }

  public function getAdwordsAccounts()
  {
    $rows = db_get_assoc_all(db_query('
        SELECT konta_numurs
        FROM new_pielikumi_kontu_linki
        WHERE
            konta_numurs IS NOT NULL AND
            konta_numurs != "" AND
            liguma_id = ' . (int)$this->id));

    $account_numbers = array();

    foreach((array)$rows as $row) {
        $account_numbers[] = $row['konta_numurs'];
    }

    return $account_numbers;
  }

  public function hasAdwordsAccounts()
  {
    $accounts = $this->getAdwordsAccounts();

    return !empty($accounts);
  }

  public function updateBeiguStatuss($status, $comment)
  {
    $old_status = db_get_assoc(db_query('
      SELECT *
      FROM '.DB_PREF.'ligumi_beigusies
      WHERE liguma_id = ' . (int)$this->id . '
    '));

    if(!empty($old_status)) {

      $comment .= $old_status['komentars'] ? ' | ' . $old_status['komentars'] : ''; // lai saglabātu arī vecos pierakstus

      db_query('UPDATE '.DB_PREF.'ligumi_beigusies SET beigu_statuss = "'.esc($status).'", komentars = "'.esc($comment).'" WHERE liguma_id = ' . $this->id);
    }
  }

}
?>