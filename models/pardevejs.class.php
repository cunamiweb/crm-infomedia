<?php
class Pardevejs extends BaseModel
{
  public static function model($classname=__CLASS__)
  {
    return parent::model($classname);
  }

  public function tableName()
  {
    return 'pardeveji';
  }


  //  Filtrs
  public function sales()
  {
    $this->criteria['conditions'][] = 'tips = "sales"';
    return $this;
  }

  public function partner()
  {
    $this->criteria['conditions'][] = 'tips = "partner"';
    return $this;
  }

  public function manager()
  {
    $this->criteria['conditions'][] = 'tips = "admin"';
    $this->criteria['conditions'][] = 'vaditajs = 1';
    return $this;
  }

  public function active()
  {
    $this->criteria['conditions'][] = 'aktivs = 1';
    return $this;
  }


  public function getEmails()
  {
    $result = db_get_assoc_all(db_query('SELECT epasts FROM ' . DB_PREF . 'pardeveji_epasti WHERE pardeveja_id = '.esc($this->id)));

    $emails = array();
    if(!empty($result)) {
      foreach($result as $row) {
        $emails[] = $row['epasts'];
      }
    }

    return $emails;
  }

  public function getPhones()
  {
    $result = db_get_assoc_all(db_query('SELECT telefons FROM ' . DB_PREF . 'pardeveji_telefoni WHERE pardeveja_id = '.esc($this->id)));

    $phones = array();
    if(!empty($result)) {
      foreach($result as $row) {
        $phones[] = $row['telefons'];
      }
    }

    return $phones;
  }

  public function getPadotie()
  {
    return Pardevejs::model()->findAll(array('conditions' => array('vaditajs_id = ' . esc($this->id))));
  }

  public function isPadotais($id) {
    foreach((array)$this->getPadotie() as $padotais) {
      if($padotais->id == $id) {
        return true;
        break;
      }
    }

    return false;
  }

  public function isKorp()
  {
    if($this->id == KORP_VADITAJA_ID) {
      return true;
    } else {
      return false;
    }
  }

  public function isPardVad()
  {
    if($this->id == PARD_VADITAJA_ID) {
      return true;
    } else {
      return false;
    }
  }

  public function isPartner()
  {
    if($this->tips == 'partner') {
      return true;
    } else {
      return false;
    }
  }

  public function getLigumi()
  {
    return Ligums::model()->findAll(array('conditions' => array('pardeveja_id = ' . $this->id)));
  }

  public function getRekini()
  {
    return PardevejaRekins::model()->findAll(array('conditions' => array('pardeveja_id = ' . $this->id)));
  }

  public function getPiedavajumi()
  {
    return Piedavajums::model()->findAll(array('conditions' => array('pardevejs_id = ' . $this->id)));
  }

  public function getSaistitie()
  {
    $result = db_get_assoc_all(db_query('SELECT liguma_id FROM '.DB_PREF.'ligumi_saistitie_pardeveji WHERE pardeveja_id = ' . $this->id));

    $ids = array();
    foreach((array)$result as $row) {
      $ids[] = $row['liguma_id'];
    }

    if(!empty($ids)) {
      return Ligums::model()->findAll(array('conditions' => array('id IN ('. implode(', ', $ids) . ')')));
    } else {
      return array();
    }
  }

  public function canDelete()
  {

    if(!$this->id) {
      return false;
    }

    if($this->tips == 'admin') {
      return false;
    }

    $ligumi = $this->getLigumi();
    if(!empty($ligumi)) {
      return false;
    }

    $rekini = $this->getRekini();
    if(!empty($rekini)) {
      return false;
    }

    $piedavajumi = $this->getPiedavajumi();
    if(!empty($piedavajumi)) {
      return false;
    }

    $saistitie = $this->getSaistitie();

    if(!empty($saistitie)) {
      return false;
    }

    return true;
  }
}
?>