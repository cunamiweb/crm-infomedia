<?php

use Google\AdsApi\AdWords\Reporting\v201809\ReportDefinition;
use Google\AdsApi\AdWords\Reporting\v201809\ReportDefinitionDateRangeType;
use Google\AdsApi\AdWords\Reporting\v201809\ReportDownloader;
use Google\AdsApi\AdWords\v201809\cm\ReportDefinitionReportType;
use Google\AdsApi\AdWords\Reporting\v201809\DownloadFormat;
use Google\AdsApi\AdWords\AdWordsSessionBuilder;
use Google\AdsApi\AdWords\AdWordsServices;
use Google\AdsApi\AdWords\ReportSettingsBuilder;
use Google\AdsApi\Common\OAuth2TokenBuilder;
use Google\AdsApi\AdWords\v201809\cm\Selector;
use Google\AdsApi\AdWords\v201809\cm\DateRange;

class AdwordsReports{
	private $user;
	public $version;

	public function __construct($version = ADWORDS_VERSION, $customer_id = null){

    $filePath=DIR_ROOT.'/inc/adwords_customers.ini';

    // Generate a refreshable OAuth2 credential for authentication.
    $oAuth2Credential = (new OAuth2TokenBuilder())->fromFile($filePath)->build();

    $this->user = (new AdWordsSessionBuilder())->fromFile($filePath);

    if($customer_id) {
        $this->user->withClientCustomerId($customer_id);
    } else if($auth_version== "default") {
        $this->user->withClientCustomerId(ADWORDS_DEFAULT_CLIENT_CUSTOMER_ID);
    }

    $this->session = $this->user->withOAuth2Credential($oAuth2Credential)->build();


		$this->version=$version;
	}
	
	
	function getReport($fields,$customer_id,$fdate_from,$fdate_to,$date_from,$date_to,$full=true){

		$slct = new Selector();
	  $slct->setFields(array_values($fields));

		$dateRange = new DateRange();
		$dateRange->setMin(date('Ymd',strtotime($date_from)));
		$dateRange->setMax(date('Ymd',strtotime($date_to)));
		$slct->setDateRange($dateRange);

		$reportDefinition=new ReportDefinition();
		$reportDefinition->setSelector($slct);
		$reportDefinition->setReportName('keywordsreport');
		$reportDefinition->setDateRangeType(ReportDefinitionDateRangeType::CUSTOM_DATE);
		$reportDefinition->setReportType(ReportDefinitionReportType::ACCOUNT_PERFORMANCE_REPORT);
		$reportDefinition->setDownloadFormat(DownloadFormat::XML);

    // download
    if ($full){
			$fname='tmp/google_reports/report.'.date('Ymd').'.'.$customer_id.'.'.date('Ymd',strtotime($fdate_from)).'.'.date('Ymd',strtotime($fdate_to)).'.f.xml';
		}
		else{
			$fname='tmp/google_reports/report.'.date('Ymd').'.'.$customer_id.'.'.date('Ymd',strtotime($fdate_from)).'.'.date('Ymd',strtotime($fdate_to)).'.p.xml';
		}

    $reportDownloader = new ReportDownloader($this->session);
    $reportDownloader->downloadReport($reportDefinition)->saveToFile($fname);

		return file_get_contents($fname);

	}

}

?>