<?php
class StandartaPiedavajums extends Atslegvards
{
  public static function model($classname=__CLASS__)
  {
    return parent::model($classname);
  }

  public function getNozare()
  {
    return Nozare::model()->findByPk($this->nozare_id);
  }

  public function validate()
  {
    global $_vars;

    //  Av
    if(!$this->atslegvards) {
      $this->addError('atslegvards', 'Atslēgvārdu jānorāda obligāti');
    }

    //  Duplikāti
    $existing = $this->getNozare()->getStandartaPiedavajums();
    $no_dupl = true;
    foreach((array)$existing as $av) {
      if($av->atslegvards == $this->atslegvards && $av->id != $this->id) {
        $no_dupl = false;
      }
    }
    if(!$no_dupl) {
      $this->addError('atslegvards', 'Šāds atslēgvārds jau eksistē');
    }

    return !$this->hasErrors();
  }

  public function getType()
  {
    return 'std';
  }

  public function hasOverridenPrices()
  {
    if($this->override !== null || $this->override_s !== null || $this->override_t !== null) {
      return true;
    } else {
      return false;
    }
  }
}
?>