<?php
class Rekins extends BaseModel
{
  public static function model($classname=__CLASS__)
  {
    return parent::model($classname);
  }

  public function tableName()
  {
    return 'rekini';
  }

  public function getLigums()
  {
    return Ligums::model()->findByPk($this->liguma_id);
  }

  public function getBudzetaParskaitijumi()
  {
    return BudzetaParskaitijums::model()->findAll(array(
      'conditions'=>array('rekina_id = ' . esc($this->id)),
      'order' => 'apm_datums DESC',
    ));
  }
}
?>