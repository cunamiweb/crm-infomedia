<?php
include("inc/init.php");

$query = db_query("
  SELECT *
  FROM `".DB_PREF."pielikumi_tiessaite` t
");

$pielikumi_tiessaite_skaits = array();

while($row = db_get_assoc($query)){

  if (!isset($pielikumi_tiessaite_skaits[$row['liguma_id']])) {
    $pielikumi_tiessaite_skaits[$row['liguma_id']] = 0;
  }

  $pielikumi_tiessaite_skaits[$row['liguma_id']] ++;

}


$query = db_query("
  SELECT *
  FROM `".DB_PREF."pielikumi_infomedia` i
");

$pielikumi_infomedia_skaits = array();

while($row = db_get_assoc($query)){

  if (!isset($pielikumi_infomedia_skaits[$row['liguma_id']])) {
    $pielikumi_infomedia_skaits[$row['liguma_id']] = 0;
  }

  $pielikumi_infomedia_skaits[$row['liguma_id']] ++;

}


$query = db_query("
  SELECT *
  FROM `".DB_PREF."pielikumi_yandex`
");

$pielikumi_yandex_skaits = array();

while($row = db_get_assoc($query)){

  if (!isset($pielikumi_yandex_skaits[$row['liguma_id']])) {
    $pielikumi_yandex_skaits[$row['liguma_id']] = 0;
  }

  $pielikumi_yandex_skaits[$row['liguma_id']] ++;

}

$query = db_query("
  SELECT
    g.*,
    (
      SELECT SUM(summa)
      FROM `".DB_PREF."rekini`
      WHERE
        liguma_id = g.id AND
        atkapsanas_rekins = 0
    ) as liguma_summa_bez_pvn
  FROM `".DB_PREF."ligumi` g
  ORDER BY
    g.`ligumadat` DESC,
    g.`ligumanr` DESC
");

$piesl_lig_skaits = 0;
$piesl_tiessaites_av_skaits = 0;
$piesl_infomedia_av_skaits = 0;
$piesl_yandex_av_skaits = 0;
$dienas_budzets = 0;

while($row = db_get_assoc($query)) {

  if ($row['google_statuss'] != 3) {
    continue;
  }

  $piesl_lig_skaits ++;

  if (!empty($pielikumi_tiessaite_skaits[$row['id']])) {
    $piesl_tiessaites_av_skaits += $pielikumi_tiessaite_skaits[$row['id']];
  }

  if (!empty($pielikumi_infomedia_skaits[$row['id']])) {
    $piesl_infomedia_av_skaits += $pielikumi_infomedia_skaits[$row['id']];
  }

  if (!empty($pielikumi_yandex_skaits[$row['id']])) {
    $piesl_yandex_av_skaits += $pielikumi_yandex_skaits[$row['id']];
  }

  $ilgums = 0;
  if(!empty($row['reklama_no']) && !empty($row['reklama_lidz'])) {
    $ilgums = round((strtotime($row['reklama_lidz']) - strtotime($row['reklama_no'])) / 86400) + 1;
  }

  if (!empty($ilgums)) {

    $dienas_budzets += $row['liguma_summa_bez_pvn'] / $ilgums;

  }

}

$sql = "
  INSERT INTO `".DB_PREF."ligumi_parskats_log`
  SET
    datums = NOW(),
    piesl_lig_skaits = ".$piesl_lig_skaits.",
    piesl_tiessaites_av_skaits = ".$piesl_tiessaites_av_skaits.",
    piesl_infomedia_av_skaits = ".$piesl_infomedia_av_skaits.",
    piesl_yandex_av_skaits = ".$piesl_yandex_av_skaits.",
    dienas_budzets = ".$dienas_budzets.",
    izveidots = NOW()
";

db_query($sql);

?>