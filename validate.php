<?php
include("inc/init.php");

if (!$_SESSION['login']) {
  die();
}

if (empty($_GET['action']) || !preg_match('/^[a-z_]+$/i', $_GET['action'])) {
  die();
}

$fnc_to_call = 'validate_' . $_GET['action'] . '_form';

if (function_exists($fnc_to_call)) {

  $errors = $fnc_to_call($_POST);

  if (!empty($errors)) {
    echo Zend_Json::encode(array('errors' => $errors));
  } else {
    echo Zend_Json::encode(array('success' => 1));
  }

}
?>