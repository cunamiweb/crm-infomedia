<?php

include("inc/init.php");

$query = db_query("
  SELECT
    g.*, k.epasts
  FROM `".DB_PREF."ligumi` g
  LEFT JOIN `".DB_PREF."kontakti` k ON k.liguma_id = g.id
  WHERE
    ligumadat >= '2017-01-01'
  ORDER BY
    g.`ligumadat` DESC,
    g.`ligumanr` DESC
");

$emaili = array();
while($row = db_get_assoc($query)) {
  $query_rekini = db_query("
    SELECT r.*
    FROM `".DB_PREF."rekini` as r
    WHERE liguma_id = " . $row['id']
  );
  $rekini = array();
  while($rekini_row = db_get_assoc($query_rekini)) {
    $rekini[] = $rekini_row;
  }

  $statuss = get_liguma_statuss($row, $rekini);
  if($statuss == 3 || $statuss == 6) { // 3 - aktivs, 6 - beidzies
    $emaili[] = $row['epasts'];
  }
}

$emaili = array_unique($emaili);
foreach($emaili as $email) {
  if(trim($email)) {
    echo $email . "<br />";
  }
}
?>