<?php
if(!isset($pardeveja_id)) {
  //  Jauns
  if(!check_access('partneri-admin')) {
    die('Nav pieejas tiesību');
  }
} else {

  //  Labot/skatīt
  if(check_access('partneri-admin')) {

    if(!check_access('partneri-visi') && (!check_access('partneri-savi') || !$lietotajs->isPadotais($pardeveja_id))) {
      die('Nav pieejas tiesību');
    }

  //  paša profils
  } elseif($pardeveja_id != $_SESSION['user']['id'] || !check_access('darbinieki-savi-maksajumi')) {
    die('Nav pieejas tiesību');
  }
}
?>