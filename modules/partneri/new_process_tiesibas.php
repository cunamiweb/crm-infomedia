<?php
if(!is_superadmin()) {
  die();
}

if(isset($_POST['Tiesiba'])) {
  $log_data = array();
  $log_data['lietotaja_tiesibas']['title'] = 'Laboja lietotāja ' . $d['vards'] . ' tiesības';

  //  Ielogo un izdzēš visas saglabātās tiesības
  $log_data['lietotaja_tiesibas']['old'] = db_get_assoc_all(db_query('SELECT * FROM ' . DB_PREF . 'tiesibas_lietotaji WHERE lietotajs_id = ' . esc($pardeveja_id)));
  db_query('DELETE FROM ' . DB_PREF . 'tiesibas_lietotaji WHERE lietotajs_id = ' . esc($pardeveja_id));

  foreach($_POST['Tiesiba'] as $tiesiba_id => $perm) {
    $existing = db_query('INSERT INTO ' . DB_PREF . 'tiesibas_lietotaji (lietotajs_id, tiesibas_id, piekluve) VALUES ("'.esc($pardeveja_id).'", '.esc($tiesiba_id).', '.esc($perm).')');
  }

  $log_data['lietotaja_tiesibas']['new'] = db_get_assoc_all(db_query('SELECT * FROM ' . DB_PREF . 'tiesibas_lietotaji WHERE lietotajs_id = ' . esc($pardeveja_id)));

  log_add('laboja', $log_data);

  header('Location: ?c=partneri&a=labot&id=' . $pardeveja_id . '&subtab=' . $subtab);
}
?>