<?php
if(!isset($pardeveja_id)) {
  //  Jauns
  if(!check_access('partneri-admin')) {
    die('Nav pieejas tiesību');
  }
} else {

  //  Labot/skatīt
  if(check_access('partneri-admin')) {

    if(!check_access('partneri-visi') && (!check_access('partneri-savi') || !$lietotajs->isPadotais($pardeveja_id))) {
      die('Nav pieejas tiesību');
    }

  //  paša profils
  } elseif($pardeveja_id != $_SESSION['user']['id'] || !check_access('darbinieki-savi-ligumi')) {
    die('Nav pieejas tiesību');
  }
}

if (!empty($_POST)) {

  $errors = validate_darbinieks_ligumi_form($_POST);

  if (empty($errors)) {

    $_POST = trim_array($_POST);

    if (check_access('partneri-admin')) {
      // updating provisions
      if (!empty($_POST['lig_provizija'])) {

        foreach($_POST['lig_provizija'] as $lig_id => $provizija) {

          $log_data = array();

          $log_data['ligums']['old'] = db_get_assoc(db_query("SELECT * FROM `".DB_PREF."ligumi` WHERE id = ".$lig_id));

          $provizija = (float)str_replace(',', '.', $provizija);

          $sql = "
            UPDATE `".DB_PREF."ligumi`
            SET
              provizija_pardevejam = ".$provizija."
            WHERE id = ".$lig_id."
            LIMIT 1
          ";
          db_query($sql);

          $log_data['ligums']['new'] = db_get_assoc(db_query("SELECT * FROM `".DB_PREF."ligumi` WHERE id = ".$lig_id));
          $log_data['ligums']['title'] = sprintf('Laboja līguma %s datus', $log_data['ligums']['old']['ligumanr']);
          log_add("laboja", $log_data);
        }
      }
    }

    if (check_access('partneri-vaditajs')) {
      // updating provisions
      if (!empty($_POST['atlidziba_vaditajam'])) {

        foreach($_POST['atlidziba_vaditajam'] as $lig_id => $atlidziba) {
          $log_data = array();

          $log_data['ligums']['old'] = db_get_assoc(db_query("SELECT * FROM `".DB_PREF."ligumi` WHERE id = ".$lig_id));
          $atlidziba = (float)str_replace(',', '.', $atlidziba);

          $sql = "
            UPDATE `".DB_PREF."ligumi`
            SET
              atlidziba_vaditajam = ".$atlidziba."
            WHERE id = ".$lig_id."
            LIMIT 1
          ";
          db_query($sql);

          $log_data['ligums']['new'] = db_get_assoc(db_query("SELECT * FROM `".DB_PREF."ligumi` WHERE id = ".$lig_id));
          $log_data['ligums']['title'] = sprintf('Laboja līguma %s datus', $log_data['ligums']['old']['ligumanr']);
          log_add("laboja", $log_data);
        }
      }
    }

    // inserting files
    process_files(array('darb_ligumi'), $pardeveja_id);

    // inserting comments
    process_comments(array('darb_ligumi'), $pardeveja_id);

    header('Location: ?c=partneri&a=labot&id=' . $pardeveja_id . '&subtab=' . $subtab);
    die();

  }

}
?>