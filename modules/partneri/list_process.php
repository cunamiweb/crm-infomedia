<?php
if (!empty($_POST)) {

  $errors = validate_darb_kopsavilkums_form($_POST);

  if (empty($errors)) {

    $_POST = trim_array($_POST);

    if (check_access('darbinieki-kopsavilkums-komentet')) {

      // inserting comments

      process_comments(array('_darb_kopsavilkums'));

    }

    header('Location: ?c=partneri');
    die();

  }

}
?>