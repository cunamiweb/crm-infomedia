<?php
if(!check_access('darbinieki-ligumi') && ($pardeveja_id != $_SESSION['user']['id'] || !check_access('darbinieki-savi-ligumi'))) {
  die('Jums nav pieejas tiesību šai sadaļai');
}

if (!empty($_POST)) {

  $errors = validate_darbinieks_saistitie_ligumi_form($_POST);

  if (empty($errors)) {

    $_POST = trim_array($_POST);

    if (check_access('darbinieki-saist-ligumi-faili')) {

      // inserting files
      process_files(array('darb_saistitie_ligumi'), $pardeveja_id);

    }

    // inserting comments
    process_comments(array('darb_saistitie_ligumi'), $pardeveja_id);

    header('Location: ?c=partneri&a=labot&id=' . $pardeveja_id . '&subtab=' . $subtab);
    die();

  }

}
?>