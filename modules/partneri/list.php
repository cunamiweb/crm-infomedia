<?
$agenturas = true;

require('list_process.php');

log_add("atvera", "Atvēra partneru kopsavilkumu");

if (date('m') <= 3) {
  //$cur_quarter .= 1;
  $cur_quarter_nr = 1;
} elseif (date('m') <= 6) {
  //$cur_quarter .= 2;
  $cur_quarter_nr = 2;
} elseif (date('m') <= 9) {
  //$cur_quarter .= 3;
  $cur_quarter_nr = 3;
} elseif (date('m') <= 12) {
  //$cur_quarter .= 4;
  $cur_quarter_nr = 4;
}

$cur_quarter = '2011-' . $cur_quarter_nr;

// par vecākiem ceturkšņiem dati var nebūt precīzi, tāpēc nemaz nepiedāvājam
$y = '2011';
$q = '1';

$quarters_roman = array(1 => 'I', 2 => 'II', 3 => 'III', 4 => 'IV');

$quarters = array();

//while($y . '-' . $q <= $cur_quarter) {
while($y < date('Y') || ($y == date('Y') && $q <= $cur_quarter_nr)) {
  $quarters[] = array(
    $y . '-' . $q,
    $y . ' - ' . $quarters_roman[$q]
  );

  if ($q < 4) {
    $q ++;
  } else {
    $y++;
    $q = 1;
  }

}

if (!empty($_GET['stats_quarter'])) {
  $stats_quarter = $_GET['stats_quarter'];
} else {
  $stats_quarter = $cur_quarter;
}

?>

<?php if(check_access('darbinieki-saraksts-filtrs')) {?>
<form action="" method="get" id="period_filter">
  <input type="hidden" name="c" value="partneri" />
  <div class="item" style="float: left; text-align: left;">
    Periods:
    <span><input id="date_from" name="date_from" type="input" style="width:80px;" value="<?= !empty($_GET['date_from']) ? $_GET['date_from'] : '' ?>" />
    -
    <input id="date_to" name="date_to" type="input" style="width:80px;" value="<?= !empty($_GET['date_to']) ? $_GET['date_to'] : '' ?>">
    <a class="clear_form" href="#">x</a></span>
  </div>
  <div class="item" style="float: left; text-align: left; margin-left: 30px;">
    Rādīt neaktīvos:
    <span><input id="inactive" name="inactive" type="checkbox" value="1" <?= !empty($_GET['inactive']) ? 'checked="checked"' : '' ?> /></span>
  </div>
</form>
<?php } ?>


<? if (check_access('partneri-jauns')) { ?>
  <button style="float: right;" onclick="document.location='?c=partneri&a=jauns'" class="ui-state-default ui-corner-all">Pievienot jaunu partneri</button>
<? } ?>

<div class="clear"></div>

<script>
var searchlist = [];

var sp = create_filter_field_array([
  's_pardevejs', 's_ligumu_sk', 's_lig_kopsumma', 's_lig_vid_summa',
  's_apm_lig_kopsumma', 's_vid_prov_proc', 's_apr_prov', 's_vid_apr_prov',
  's_apmaksajama_prov', 's_apmaksajama_prov_percent', 's_izm_prov',
  's_apr_piem', 's_izm_piem', 's_rek_apm_percent', 's_dept_cred',
  's_akt_atg'
]);
</script>

<?  // Prototypes // ?>
<div style="display:none;">

  <div id="komentarsprot__darb_kopsavilkums">
    <table class="comment_form">
      <tr>
        <td width="70">Komentārs:</td>
        <td>
          <textarea name="komentari_new[_darb_kopsavilkums][komentars][]" rows="2" cols="30"></textarea>
        </td>
      </tr>
    </table>
  </div>

</div>

<table style="display:none;">

  <tr id="darbinieki_kopsavilkums_groups">
    <th></th>
    <?php if(check_access('darbinieki-k-ligumi')) { ?>
      <th colspan="4" class="bgc-lig">Līgumi</th>
    <?php } ?>

    <?php if(check_access('darbinieki-k-provizijas')) {?>
      <th colspan="6" class="bgc-pro">Komisijas</th>
    <?php } ?>

    <?php if(check_access('darbinieki-k-piemaksas')) {?>
      <th colspan="2" class="bgc-pie">Piemaksas</th>
    <?php } ?>

    <?php if(check_access('darbinieki-k-bilance')) { ?>
      <th colspan="2" class="bgc-bil">Bilance</th>
    <?php } ?>

    <?php if(check_access('darbinieki-k-atgadinajumi')) {?>
      <th>Atg.</th>
    <?php } ?>
  </tr>

  <tr id="darbinieki_kopsavilkums_filter">

    <th>
      <select class="search_field advonchange" searchclass="s_pardevejs" meth="int">
        <option value=""></option>
        <?php foreach(Pardevejs::model()->partner()->findAll() as $p){ ?>
          <option value="<?=$p->id;?>"><?=$p->vards;?></option>
        <?php } ?>
      </select>
      <a class="search_field" href="#">x</a>
    </th>

    <?php if(check_access('darbinieki-k-ligumi')) {?>
      <th><span><input class="search_field" meth="int" searchclass="s_ligumu_sk" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
      <th><span><input class="search_field" meth="coin" searchclass="s_lig_kopsumma" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
      <th><span><input class="search_field" meth="coin" searchclass="s_lig_vid_summa" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
      <th><span><input class="search_field" meth="coin" searchclass="s_apm_lig_kopsumma" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <?php } ?>

    <?php if(check_access('darbinieki-k-provizijas')) {?>
      <th><span><input class="search_field" meth="coin" searchclass="s_vid_prov_proc" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
      <th><span><input class="search_field" meth="coin" searchclass="s_apr_prov" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
      <th><span><input class="search_field" meth="coin" searchclass="s_vid_apr_prov" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
      <th><span><input class="search_field" meth="coin" searchclass="s_apmaksajama_prov" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
      <th><span><input class="search_field" meth="coin" searchclass="s_apmaksajama_prov_percent" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
      <th><span><input class="search_field" meth="coin" searchclass="s_izm_prov" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <?php } ?>

    <?php if(check_access('darbinieki-k-piemaksas')) {?>
      <th><span><input class="search_field" meth="coin" searchclass="s_apr_piem" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
      <th><span><input class="search_field" meth="coin" searchclass="s_izm_piem" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <?php } ?>

    <?php if(check_access('darbinieki-k-bilance')) { ?>
      <th><span><input class="search_field" meth="coin" searchclass="s_rek_apm_percent" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
      <th><span><input class="search_field" meth="coin" searchclass="s_dept_cred" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <?php } ?>

    <?php if(check_access('darbinieki-k-atgadinajumi')) {?>
      <th>
        <select class="search_field advonchange" searchclass="s_akt_atg" meth="int">
          <option value=""></option>
          <option value="1">Ir</option>
          <option value="2">Nav</option>
        </select>
        <a class="search_field" href="#">x</a>
      </th>
    <?php } ?>

  </tr>

</table>

<table cellpadding="3" cellspacing="0" id="darbinieki_kopsavilkums" class="data ckey_darbinieki_list" style="width: auto;">

  <thead>

    <tr class="header">
      <th width="150" class="header" colname="darbinieks">Darbinieks</th>

      <?php if(check_access('darbinieki-k-ligumi')) {?>
        <th width="40" class="header" colname="lig_sk">Līgumu sk.</th>
        <th width="50" class="header" colname="lig_kopsumma">Līg.&nbsp;kopsumma<br />EUR&nbsp;(bez PVN)</th>
        <th width="50" class="header" colname="lig_vid_summa">Līg.&nbsp;vid.<br />summa&nbsp;EUR<br />(bez PVN)</th>
        <th width="50" class="header" colname="lig_apm_kopsumma">Apmaksātā līg.<br />kopsumma<br />(ar PVN)</th>
      <?php } ?>

      <?php if(check_access('darbinieki-k-provizijas')) {?>
        <th width="50" class="header" colname="prov_procent_vid">Partnera komisija&nbsp;%<br />(vid.)</th>
        <th width="50" class="header" colname="apr_prov_summa">Apr.&nbsp;komisija<br />EUR&nbsp;(bez PVN)</th>
        <th width="50" class="header" colname="vid_apr_prov_summa">Vid. apr.<br />komisija&nbsp;EUR<br />(bez PVN)</th>
        <th width="50" class="header" colname="apm_prov_summa">Apmaksātā<br />komisija&nbsp;EUR<br />(bez PVN)</th>
        <th width="50" class="header" colname="apm_prov_procent">Apmaksātā<br />komisija&nbsp;%<br /></th>
        <th width="50" class="header" colname="izm_prov_summa">Izmaksātā<br />komisija&nbsp;EUR<br />(bez PVN)</th>
      <?php } ?>

      <?php if(check_access('darbinieki-k-piemaksas')) {?>
        <th width="50" class="header" colname="apr_piem_summa">Aprēķinātās<br />piemaksas&nbsp;EUR<br />(bez PVN)</th>
        <th width="50" class="header" colname="izm_piem_summa">Izmaksātās<br />piemaksas&nbsp;EUR<br />(bez PVN)</th>
      <?php } ?>

      <?php if(check_access('darbinieki-k-bilance')) { ?>
        <th width="50" class="header" colname="rek_apm_procent">Rēķinu<br />apmaksa&nbsp;%</th>
        <th width="50" class="header" colname="deb_kred_bez_pvn">Debepts/Kredīts<br />EUR&nbsp;(bez PVN)</th>
      <?php } ?>

      <?php if(check_access('darbinieki-k-atgadinajumi')) {?>
        <th width="40" class="header" colname="akt_atgad">Aktīvie<br />atgādinājumi</th>
      <?php } ?>
    </tr>

  </thead>

  <tbody class="main">

    <?php
    $period_where = array();

    if (!empty($_GET['date_from'])) {
      $period_where[] = "g.sanemsanas_datums >= '".esc(date('Y-m-d', strtotime($_GET['date_from'])))."'";
    }

    if (!empty($_GET['date_to'])) {
      $period_where[] = "g.sanemsanas_datums <= '".esc(date('Y-m-d', strtotime($_GET['date_to'])))."'";
    }

    $pardeveju_ids = array();

    $ligumu_summa = array();
    $katra_liguma_summa = array();
    $katra_liguma_summa_bez_pvn = array();
    $katra_liguma_apm_summa = array();
    $katra_liguma_apm_summa_bez_pvn = array();
    $ligumu_apm_summa = array();
    $prov_proc_summa = array();
    $apr_prov = array();
    $apmaksajama_prov = array();
    $izmaks_prov = array();
    $apr_piem = array();
    $izmaks_piem = array();
    $uz_izmaksu_prov = array();

    $sql = "
      SELECT
        p.*,
        (
          SELECT SUM(sd.summa)
          FROM `".DB_PREF."pardeveji_rekini_detalas` sd
          LEFT JOIN `".DB_PREF."pardeveji_rekini` sr ON (sr.id = sd.pardeveja_rekina_id)
          WHERE
            sr.pardeveja_id = p.id AND
            sr.statuss = 2 AND
            sd.rekina_id IS NOT NULL AND
            sd.rekina_maksajuma_id IS NULL
        ) as izmaksats_avansaa_bez_pvn,
        (
          SELECT COUNT(*)
          FROM `".DB_PREF."atgadinajumi` sa
          USE INDEX (pardeveja_id_index)
          LEFT JOIN `".DB_PREF."ligumi` sg ON (sa.liguma_id = sg.id)
          WHERE
            (
              sa.pardeveja_id = p.id OR
              sg.pardeveja_id = p.id OR
              sg.id IN (
                SELECT liguma_id
                FROM `".DB_PREF."ligumi_saistitie_pardeveji`
                WHERE pardeveja_id = p.id
              )
            ) AND
            sa.statuss = 1 AND
            sa.datums <= CURDATE() AND
            sa.redzamiba IN ('kopejs', ".(is_admin() ? "'admin'" : "'pardevejs'").")
        ) as aktivi_atgadinajumi
      FROM `".DB_PREF."pardeveji` p
    ";

		$where = array();

    $where[] = 'p.tips = "partner"';

    //  Ja prasa skatīties konkrētu partneri
		if (getRequest('pardevejs_id')) {
      $where[] = 'p.id = ' . (int)getRequest('pardevejs_id');
		}

    //  Ja nav tiesību skatīties citus partnerus tad tikai sevi redz
    if(!check_access('partneri')) {
			$where[] = 'p.id = ' . (int)$_SESSION['user']['id'];
		}

    if(!check_access('partneri-visi')) {
      //  Ja tikai savus partnerus
      if(check_access('partneri-savi')) {
        $where[] = 'p.vaditajs_id = '. $_SESSION['user']['id'];

      //  Ja ne visi ne savi
      } else {
        $where[] = 'p.id = ' . (int)$_SESSION['user']['id'];
      }
    }

		if (empty($_GET['inactive']))
			$where[] = 'p.aktivs = 1';

		if (!empty($where))
			$sql .= " WHERE " . implode(' AND ', $where);

    $sql .= "
      ORDER BY p.`vards` ASC
    ";

    $query = db_query($sql);

    if(!isset($data)) {
      $data = array();
    }

    while($row = db_get_assoc($query)) {

      $data[] = $row;

      $pardeveju_ids[] = $row['id'];

      $ligumu_summa[$row['id']] = 0;
      $ligumu_apm_summa[$row['id']] = 0;
      $prov_proc_summa[$row['id']] = 0;
      $izmaks_prov[$row['id']] = 0;
      $apr_piem[$row['id']] = 0;
      $izmaks_piem[$row['id']] = 0;
      $uz_izmaksu_prov[$row['id']] = 0;
      $apr_prem[$row['id']] = 0;
      $apm_prem[$row['id']] = 0;
      $izm_prem[$row['id']] = 0;
    }

    // -----
		// Līgumi

    $sql = "
      SELECT g.*
      FROM `".DB_PREF."ligumi` g
    ";

    $lig_where = $period_where;

    if ($pardeveju_ids) {
      $lig_where[] = "pardeveja_id IN (".implode(',', $pardeveju_ids).")";
    }

    if (!empty($lig_where)) {
      $sql .= "WHERE " . implode(" AND ", $lig_where);
    }

    $query = db_query($sql);
    $ligumi = array();

    while($row = db_get_assoc($query)){
      if(in_array($row['pardeveja_id'], $pardeveju_ids)) {
        $ligumi[$row['pardeveja_id']][$row['id']] = $row;

        $prov_proc_summa[$row['pardeveja_id']] += $row['provizija_pardevejam'];

        $katra_liguma_summa[$row['pardeveja_id']][$row['id']] = 0;
        $katra_liguma_summa_bez_pvn[$row['pardeveja_id']][$row['id']] = 0;
        $katra_liguma_apm_summa[$row['pardeveja_id']][$row['id']] = 0;
        $katra_liguma_apm_summa_bez_pvn[$row['pardeveja_id']][$row['id']] = 0;
      }
    }

    // -----
		// Rēķini

    $sql = "
      SELECT
        r.*,
        r.apm_termins as rekina_apm_termins,
        g.pardeveja_id,
        (
          SELECT SUM(m.summa)
          FROM `".DB_PREF."rekini_maksajumi` m
          WHERE
            m.rekina_id = r.id AND
            (
              r.nodots_piedz_datums IS NULL OR
              r.nodots_piedz_datums > m.datums
            )
        ) as apm_summa_no_ka_rekinat_prov
      FROM `".DB_PREF."rekini` r
      LEFT JOIN `".DB_PREF."ligumi` g ON (r.liguma_id = g.id)
    ";

    $rek_where = $period_where;

    $rek_where[] = 'r.atkapsanas_rekins = 0';

    if ($pardeveju_ids) {
      $rek_where[] = "g.pardeveja_id IN (".implode(',', $pardeveju_ids).")";
    }

    if (!empty($rek_where)) {
      $sql .= "WHERE " . implode(" AND ", $rek_where);
    }

    $query = db_query($sql);
    $lig_rekini = array();

    while($row = db_get_assoc($query)){
      if(in_array($row['pardeveja_id'], $pardeveju_ids)) {
        $lig_rekini[$row['pardeveja_id']][] = $row;

        $ligumu_summa[$row['pardeveja_id']] += $row['summa'];

        $ligumu_apm_summa[$row['pardeveja_id']] += $row['apm_summa'];

        $katra_liguma_summa[$row['pardeveja_id']][$row['liguma_id']] += $row['kopsumma'];
        $summa_bez_pvn = $row['kopsumma'] / (1 + ($row['pvn'] / 100));
        $katra_liguma_summa_bez_pvn[$row['pardeveja_id']][$row['liguma_id']] += $summa_bez_pvn;

        $katra_liguma_apm_summa[$row['pardeveja_id']][$row['liguma_id']] += $row['apm_summa'];
        $apm_summa_bez_pvn = $row['apm_summa_no_ka_rekinat_prov'] / (1 + ($row['pvn'] / 100));
        $katra_liguma_apm_summa_bez_pvn[$row['pardeveja_id']][$row['liguma_id']] += $apm_summa_bez_pvn;

        //  Prēmija
        //$prem = getContractPremium($row, $premiums_cache, $row['pardeveja_id']);
        //$prem = 4;

        //$apr_prem[$row['pardeveja_id']] = bcadd($apr_prem[$row['pardeveja_id']], bcmul($summa_bez_pvn, bcdiv($prem, 100, 4), 2), 2);
        //$apm_prem[$row['pardeveja_id']] = bcadd($apm_prem[$row['pardeveja_id']], bcmul($apm_summa_bez_pvn, bcdiv($prem, 100, 4), 2), 2);;
      }
    }

    foreach($lig_rekini as $pardeveja_id => $rekini) {
      $ids = array();

      foreach((array)$rekini as $rekins) {
        $ids[] = $rekins['id'];
      }
    }

    foreach($katra_liguma_summa_bez_pvn as $pard_id => $lig) {

      $apr_prov[$pard_id] = 0;

      foreach($lig as $lig_id => $lig_kopsumma) {
        $apr_prov[$pard_id] += $lig_kopsumma * ($ligumi[$pard_id][$lig_id]['provizija_pardevejam'] / 100);
      }

    }

    foreach($katra_liguma_apm_summa_bez_pvn as $pard_id => $lig) {

      $apmaksajama_prov[$pard_id] = 0;

      foreach($lig as $lig_id => $apm_summa) {
        $apmaksajama_prov[$pard_id] += $apm_summa * ($ligumi[$pard_id][$lig_id]['provizija_pardevejam'] / 100);
      }

    }

    // -----
		// Pārdevēju rēķini

    $sql = "
      SELECT
        r.*,
        (
          SELECT SUM(summa)
          FROM `".DB_PREF."pardeveji_rekini_detalas`
          WHERE
            pardeveja_rekina_id = r.id AND
            rekina_id IS NOT NULL
        ) as prov_summa,
        (
          SELECT SUM(summa)
          FROM `".DB_PREF."pardeveji_rekini_detalas`
          WHERE
            pardeveja_rekina_id = r.id AND
            rekina_id IS NULL AND
            rekina_maksajuma_id IS NULL
        ) as piemaksu_summa
      FROM `".DB_PREF."pardeveji_rekini` r
    ";

    if ($pardeveju_ids) {
      $pard_rek_where[] = "r.pardeveja_id IN (".implode(',', $pardeveju_ids).")";
    }

    if (!empty($pard_rek_where)) {
      $sql .= "WHERE " . implode(" AND ", $pard_rek_where);
    }

    $query = db_query($sql);

    while($row = db_get_assoc($query)){

      if ($row['statuss'] == 2 && isset($izmaks_prov[$row['pardeveja_id']])) { // apmaksāts
        $izmaks_prov[$row['pardeveja_id']] += $row['prov_summa']; // rādam bez pvn
      }

      if(isset($apr_piem[$row['pardeveja_id']])) {
        $apr_piem[$row['pardeveja_id']] += $row['piemaksu_summa']; // rādam bez pvn
      }

      if ($row['statuss'] == 2 && isset($izmaks_piem[$row['pardeveja_id']])) { // apmaksāts
        $izmaks_piem[$row['pardeveja_id']] += $row['piemaksu_summa']; // rādam bez pvn
      }

    }

    // -----

    $sql = "
      SELECT
        m.*,
        r.pvn as rekina_pvn,
        r.kopsumma,
        d.id as detalas_id,
        g.pardeveja_id,
        g.provizija_pardevejam,
        (
          SELECT SUM(d2.no_summas)
          FROM `".DB_PREF."pardeveji_rekini_detalas` d2
          LEFT JOIN `".DB_PREF."pardeveji_rekini` pr2 ON (pr2.id = d2.pardeveja_rekina_id)
          LEFT JOIN `".DB_PREF."rekini` r2 ON (d2.rekina_id = r2.id)
          LEFT JOIN `".DB_PREF."ligumi` g2 ON (r2.liguma_id = g2.id)
          WHERE
            d2.rekina_id = m.rekina_id AND
            d2.rekina_maksajuma_id IS NOT NULL AND
            pr2.statuss = 2 AND
            g2.pardeveja_id = pr2.pardeveja_id
        ) as prov_izmaksu_summa
      FROM `".DB_PREF."rekini_maksajumi` m
      LEFT JOIN `".DB_PREF."rekini` r ON (m.rekina_id = r.id)
      LEFT JOIN `".DB_PREF."ligumi` g ON (r.liguma_id = g.id)
      LEFT JOIN `".DB_PREF."pardeveji` p ON (p.id = g.pardeveja_id)
      LEFT JOIN (
      	SELECT d1.id, d1.rekina_maksajuma_id, d1.pardeveja_rekina_id
      	FROM ".DB_PREF."pardeveji_rekini_detalas d1
      	LEFT JOIN ".DB_PREF."pardeveji_rekini pr1 ON pr1.id = d1.pardeveja_rekina_id
      	WHERE pr1.pardeveja_id IN (".implode(',', $pardeveju_ids).")
      ) d ON (m.id = d.rekina_maksajuma_id)
      LEFT JOIN `".DB_PREF."pardeveji_rekini` pr ON (pr.id = d.pardeveja_rekina_id)
    ";

    $uz_izmaksu_where = $period_where;

    if ($pardeveju_ids) {
      $uz_izmaksu_where[] = "g.pardeveja_id IN (".implode(',', $pardeveju_ids).")";
    }

    $uz_izmaksu_where[] = "
      (
        d.id IS NULL OR
        pr.statuss = 1
      ) AND
      r.atkapsanas_rekins = 0 AND
      (
        r.nodots_piedz_datums IS NULL OR
        r.nodots_piedz_datums > m.datums
      )
    ";

    $uz_izmaksu_where[] = '(pr.pardeveja_id IS NULL OR pr.pardeveja_id = g.pardeveja_id)'; // Lai nepaņemtu klāt korporatīvā vadītāja rēķinus

    if (!empty($uz_izmaksu_where)) {
      $sql .= "WHERE " . implode(" AND ", $uz_izmaksu_where);
    }

    $sql .= "
      HAVING m.summa <= IFNULL(r.kopsumma, 0) - IFNULL(prov_izmaksu_summa, 0)
      ORDER BY m.datums DESC, m.id DESC
    ";

    $query = db_query($sql);

    while($row = db_get_assoc($query)) {
      //var_dump($row);
      $apm_summa_bez_pvn = $row['summa'] / (1 + ($row['rekina_pvn'] / 100));
      $prov_summa = $apm_summa_bez_pvn * ($row['provizija_pardevejam'] / 100);

      if(isset($uz_izmaksu_prov[$row['pardeveja_id']])) {
        $uz_izmaksu_prov[$row['pardeveja_id']] += $prov_summa;
      }

    }

    // -----

    $inid = -1;
    ?>

    <?php foreach($data as $row) { ?>

      <?
      $inid ++;
      ?>

      <tr id="tr<?=$inid;?>" title="<?=$row['vards'];?>">

        <td><a href="?c=partneri&a=labot&id=<?=$row['id'];?>"><b><?=$row['vards'];?></b></a></td>

        <? if (!empty($ligumi[$row['id']])) { ?>

          <? $lig_skaits = count($ligumi[$row['id']]) ?>

          <?
            $rek_apm_proc = 0;

            if (!empty($apmaksajama_prov[$row['id']]) || !empty($apr_piem[$row['id']])) {
              $rek_apm_proc = round(round($izmaks_prov[$row['id']] + $izmaks_piem[$row['id']], 2) * 100 / round($apmaksajama_prov[$row['id']] + $apr_piem[$row['id']], 2), 1);
            }

            //$deb_kred = round(($apmaksajama_prov[$row['id']] + $apr_piem[$row['id']]) - ($izmaks_prov[$row['id']] + $izmaks_piem[$row['id']]), 2);
            $deb_kred = round($uz_izmaksu_prov[$row['id']] + $apr_piem[$row['id']] - $izmaks_piem[$row['id']] - $row['izmaksats_avansaa_bez_pvn'], 2);

          ?>
           <?php if(check_access('darbinieki-k-ligumi')) {?>
            <td class="c"><?= $lig_skaits ?></td>
            <td class="r"><?= format_currency($ligumu_summa[$row['id']]) ?></td>
            <td class="r"><?= format_currency($ligumu_summa[$row['id']] / $lig_skaits) ?></td>
            <td class="r"><?= format_currency($ligumu_apm_summa[$row['id']]) ?></td>
           <?php } ?>

           <?php if(check_access('darbinieki-k-provizijas')) {?>
            <td class="r"><?= round($prov_proc_summa[$row['id']] / $lig_skaits, 1) . '%' ?></td>
            <td class="r"><?= format_currency($apr_prov[$row['id']]) ?></td>
            <td class="r"><?= format_currency($apr_prov[$row['id']] / $lig_skaits) ?></td>
            <td class="r"><?= format_currency($apmaksajama_prov[$row['id']]) ?></td>
            <td class="r"><?= !empty($apr_prov[$row['id']]) ? round(round($apmaksajama_prov[$row['id']], 2) * 100 / round($apr_prov[$row['id']], 2), 1) : 0 ?>%</td>
            <td class="r"><?= format_currency($izmaks_prov[$row['id']]) ?></td>
           <?php } ?>

           <?php if(check_access('darbinieki-k-piemaksas')) {?>
            <td class="r"><?= format_currency($apr_piem[$row['id']]) ?></td>
            <td class="r"><?= format_currency($izmaks_piem[$row['id']]) ?></td>
           <?php } ?>

          <?php if(check_access('darbinieki-k-bilance')) { ?>
            <td class="r"><?= $rek_apm_proc ?>%</td>
            <td class="r"><strong <?= $deb_kred < 0 ? 'style="color: red;"' : '' ?>><?= format_currency($deb_kred) ?></strong></td>
          <?php } ?>

          <?php if(check_access('darbinieki-k-atgadinajumi')) {?>
            <td class="c"><a href="?c=partneri&a=labot&id=<?=$row['id'];?>&subtab=atgadinajumi"><?= !empty($row['aktivi_atgadinajumi']) ? 'Ir ('.$row['aktivi_atgadinajumi'].')' : 'Nav' ?></a></td>
          <?php } ?>

          <td style="display: none">
          <script>
            searchlist[<?=$inid;?>] = '<?=addslashes(mb_strtoupper($row['vards']));?>';
            sp['s_pardevejs'][<?=$inid;?>] = <?= $row['id'] ?>;
            sp['s_ligumu_sk'][<?=$inid;?>] = <?= $lig_skaits ?>;
            sp['s_lig_kopsumma'][<?=$inid;?>] = <?= round($ligumu_summa[$row['id']], 2) ?>;
            sp['s_lig_vid_summa'][<?=$inid;?>] = <?= round($ligumu_summa[$row['id']] / $lig_skaits, 2) ?>;
            sp['s_apm_lig_kopsumma'][<?=$inid;?>] = <?= round($ligumu_apm_summa[$row['id']], 2) ?>;

            sp['s_vid_prov_proc'][<?=$inid;?>] = <?= round($prov_proc_summa[$row['id']] / $lig_skaits, 1) ?>;
            sp['s_apr_prov'][<?=$inid;?>] = <?= round($apr_prov[$row['id']], 2) ?>;
            sp['s_vid_apr_prov'][<?=$inid;?>] = <?= round($apr_prov[$row['id']] / $lig_skaits, 2) ?>;
            sp['s_apmaksajama_prov'][<?=$inid;?>] = <?= round($apmaksajama_prov[$row['id']], 2) ?>;
            sp['s_apmaksajama_prov_percent'][<?=$inid;?>] = <?= !empty($apr_prov[$row['id']]) ? round($apmaksajama_prov[$row['id']] * 100 / $apr_prov[$row['id']], 2) : 0 ?>;
            sp['s_izm_prov'][<?=$inid;?>] = <?= round($izmaks_prov[$row['id']], 2) ?>;

            sp['s_apr_piem'][<?=$inid;?>] = <?= round($apr_piem[$row['id']], 2) ?>;
            sp['s_izm_piem'][<?=$inid;?>] = <?= round($izmaks_piem[$row['id']], 2) ?>;

            sp['s_rek_apm_percent'][<?=$inid;?>] = <?= $rek_apm_proc ?>;
            sp['s_dept_cred'][<?=$inid;?>] = <?= $deb_kred ?>;
            sp['s_akt_atg'][<?=$inid;?>] = '<?=!empty($row['aktivi_atgadinajumi']) ? '1' : '2';?>';
          </script>
          </td>

        <? } else { ?>

          <?php if(check_access('darbinieki-k-ligumi')) {?>
            <td class="c">0</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          <?php } ?>

          <?php if(check_access('darbinieki-k-provizijas')) {?>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          <?php } ?>

          <?php if(check_access('darbinieki-k-piemaksas')) {?>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          <?php } ?>

          <?php if(check_access('darbinieki-k-bilance')) { ?>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          <? } ?>

          <?php if(check_access('darbinieki-k-atgadinajumi')) {?>
            <td class="c"><a href="?c=partneri&a=labot&id=<?=$row['id'];?>&subtab=atgadinajumi"><?= !empty($row['aktivi_atgadinajumi']) ? 'Ir ('.$row['aktivi_atgadinajumi'].')' : 'Nav' ?></a></td>
          <? } ?>


          <td style="display: none;">
          <script>
            searchlist[<?=$inid;?>] = '<?=addslashes(mb_strtoupper($row['vards']));?>';
            sp['s_pardevejs'][<?=$inid;?>] = <?= $row['id'] ?>;
            sp['s_ligumu_sk'][<?=$inid;?>] = 0;
            sp['s_lig_kopsumma'][<?=$inid;?>] = 0;
            sp['s_lig_vid_summa'][<?=$inid;?>] = 0;
            sp['s_apm_lig_kopsumma'][<?=$inid;?>] = 0;

            sp['s_vid_prov_proc'][<?=$inid;?>] = 0;
            sp['s_apr_prov'][<?=$inid;?>] = 0;
            sp['s_vid_apr_prov'][<?=$inid;?>] = 0;
            sp['s_apmaksajama_prov'][<?=$inid;?>] = 0;
            sp['s_apmaksajama_prov_percent'][<?=$inid;?>] = 0;
            sp['s_izm_prov'][<?=$inid;?>] = 0;

            sp['s_apr_piem'][<?=$inid;?>] = 0;
            sp['s_izm_piem'][<?=$inid;?>] = 0;

            sp['s_rek_apm_percent'][<?=$inid;?>] = 0;
            sp['s_dept_cred'][<?=$inid;?>] = 0;
            sp['s_akt_atg'][<?=$inid;?>] = '<?=!empty($row['aktivi_atgadinajumi']) ? '1' : '2';?>';
          </script>
          </td>

        <? } ?>

      </tr>

    <?php } ?>


  </tbody>

  <? if (check_access('partneri-admin')) { ?>

  <tr>

    <?php if(check_access('darbinieki-k-ligumi') || check_access('darbinieki-k-statistika') || check_access('darbinieki-k-provizijas') || check_access('darbinieki-k-premijas') || check_access('darbinieki-k-piemaksas') || check_access('darbinieki-k-bilance')) { ?>
      <td align="right"><strong>Kopā:</strong></td>
    <?php } else { ?>
      <td align="right"></td>
    <?php }?>

    <?php if(check_access('darbinieki-k-ligumi')) {?>
      <td class="c" id="ligskaits">0</td>
      <td class="r" id="ligkopsumma">0</td>
      <td></td>
      <td class="r" id="apmligsumma">0</td>
    <?php } ?>

    <?php if(check_access('darbinieki-k-provizijas')) {?>
      <td></td>
      <td class="r" id="aprprovsumma">0</td>
      <td></td>
      <td class="r" id="apmaksajamaprovsumma">0</td>
      <td></td>
      <td class="r" id="izmprovsumma">0</td>
    <?php } ?>

    <?php if(check_access('darbinieki-k-piemaksas')) {?>
      <td class="r" id="aprpiemsumma">0</td>
      <td class="r" id="izmpiemsumma">0</td>
    <?php } ?>

    <?php if(check_access('darbinieki-k-bilance')) { ?>
      <td></td>
      <td class="r" style="font-weight: bold;" id="deptkredsumma">0</td>
    <?php } ?>

    <?php if(check_access('darbinieki-k-atgadinajumi')) {?>
      <td></td>
    <?php } ?>

  </tr>

  <tr>

    <?php if(check_access('darbinieki-k-ligumi') || check_access('darbinieki-k-statistika') || check_access('darbinieki-k-provizijas') || check_access('darbinieki-k-premijas') || check_access('darbinieki-k-piemaksas') || check_access('darbinieki-k-bilance')) { ?>
      <td align="right"><strong>Vidēji:</strong></td>
    <?php } else { ?>
      <td align="right"></td>
    <?php }?>

    <?php if(check_access('darbinieki-k-ligumi')) {?>
      <td class="c" id="ligskaitsvid">0</td>
      <td></td>
      <td class="r" id="ligvidsummavid">0</td>
      <td></td>
    <?php } ?>

    <?php if(check_access('darbinieki-k-provizijas')) {?>
      <td class="r" id="provpercentvid">0</td>
      <td></td>
      <td class="r" id="vidaprprovvid">0</td>
      <td></td>
      <td class="r" id="apmprovpercentvid">0</td>
      <td class="r" id="izmprovvid">0</td>
    <?php } ?>

    <?php if(check_access('darbinieki-k-piemaksas')) {?>
      <td></td>
      <td class="r" id="izmpiemvid"></td>
    <?php } ?>

    <?php if(check_access('darbinieki-k-bilance')) { ?>
      <td class="r" id="rekapmpercentvid">0</td>
      <td></td>
    <?php } ?>

    <?php if(check_access('darbinieki-k-atgadinajumi')) {?>
      <td></td>
    <?php } ?>

  </tr>

  <? } ?>

</table>

<script type="text/javascript">
$(document).ready(function() {

  var table = $("#darbinieki_kopsavilkums");
  var head = $("#darbinieki_kopsavilkums_filter");
  var groups = $("#darbinieki_kopsavilkums_groups");

  <? if (check_access('partneri-admin')) { ?>

    var cache_key = getCacheKeyFromClass(table);

    var sortListFromCache = cache_key ? $.jStorage.get(cache_key + '_sort', []) : [];

    table.tablesorter({
      debug: false,
      highlightClass: 'highlight',
      sortList: sortListFromCache,
      headers: {
        1: { sorter:'digit' },
        2: { sorter:'currency' },
        3: { sorter:'currency' },
        4: { sorter:'currency' },
        5: { sorter:'digit' },
        7: { sorter:'currency' },
        8: { sorter:'currency' },
        9: { sorter:'currency' },
        10: { sorter:'digit' },
        11: { sorter:'currency' },
        12: { sorter:'currency' }
      }
    }).bind('sortEnd', function() {

      if (cache_key) $.jStorage.set(cache_key + '_sort', table.get(0).config.sortList);

      stripeTable(table);

      initTableGroupColors(table);

    });

    initTableFilter(table, head, processTable, '', searchlist, sp, groups);

  <? } else { ?>

    stripeTable(table);

    $('thead', table).prepend('<tr class="groups">'+groups.html()+'</tr>');

    groups.remove();

  <? } ?>

  initTableCols(table, 'darb_kopsavilkums', <?= Zend_Json::encode(get_tab_kolonnas('darb_kopsavilkums')) ?>);

  initTableGroupColors(table);

});

function processTable() {

  var pardskaits = 0;
  var ligumuskaits = 0;
  var ligkopsumma = 0;
  var ligvidsumma = 0;
  var apmligsumma = 0;

  var provpercentsumma = 0;
  var aprprovsumma = 0;
  var vidaprprovsumma = 0;
  var apmaksajamaprovsumma = 0;
  var apmprovpercentsumma = 0;
  var izmprovsumma = 0;

  var aprpiemsumma = 0;
  var izmpiemsumma = 0;

  var rekapmpercentsumma = 0;
  var deptkredsumma = 0;

  jQuery.each(searchlist, function(k,v){

    var row = $('#tr' + k);

    if (row.is(':visible') && sp['s_ligumu_sk'][k] > 0) {

      pardskaits ++;

      if(sp['s_ligumu_sk'][k] > 0) ligumuskaits += sp['s_ligumu_sk'][k];
      if(sp['s_lig_kopsumma'][k] > 0) ligkopsumma += sp['s_lig_kopsumma'][k];
      if(sp['s_lig_vid_summa'][k] > 0) ligvidsumma += sp['s_lig_vid_summa'][k];
      if(sp['s_apm_lig_kopsumma'][k] > 0) apmligsumma += sp['s_apm_lig_kopsumma'][k];

      if(sp['s_vid_prov_proc'][k] > 0) provpercentsumma += sp['s_vid_prov_proc'][k];
      if(sp['s_apr_prov'][k] > 0) aprprovsumma += sp['s_apr_prov'][k];
      if(sp['s_vid_apr_prov'][k] > 0) vidaprprovsumma += sp['s_vid_apr_prov'][k];
      if(sp['s_apmaksajama_prov'][k] > 0) apmaksajamaprovsumma += sp['s_apmaksajama_prov'][k];
      if(sp['s_apmaksajama_prov_percent'][k] > 0) apmprovpercentsumma += sp['s_apmaksajama_prov_percent'][k];
      if(sp['s_izm_prov'][k] > 0) izmprovsumma += sp['s_izm_prov'][k];

      if(sp['s_apr_piem'][k] > 0) aprpiemsumma += sp['s_apr_piem'][k];
      if(sp['s_izm_piem'][k] > 0) izmpiemsumma += sp['s_izm_piem'][k];

      if(sp['s_rek_apm_percent'][k] > 0) rekapmpercentsumma += sp['s_rek_apm_percent'][k];

      deptkredsumma += sp['s_dept_cred'][k];

    }

  });

  $('#ligskaits').html(ligumuskaits);
  $('#ligskaitsvid').html(Math.round(ligumuskaits / pardskaits * 10) / 10);
  $('#ligkopsumma').html('EUR&nbsp;' + ligkopsumma.toFixed(2));
  $('#ligvidsummavid').html('EUR&nbsp;' + (ligvidsumma / pardskaits).toFixed(2));
  $('#apmligsumma').html('EUR&nbsp;' + apmligsumma.toFixed(2));

  //((Math.round(apmprovpercentsumma / pardskaits * 20) / 20) + '%')

  $('#provpercentvid').html((Math.round(provpercentsumma / pardskaits * 20) / 20) + '%');
  $('#aprprovsumma').html('EUR&nbsp;' + aprprovsumma.toFixed(2));
  $('#vidaprprovvid').html('EUR&nbsp;' + (vidaprprovsumma / pardskaits).toFixed(2));
  $('#apmaksajamaprovsumma').html('EUR&nbsp;' + apmaksajamaprovsumma.toFixed(2));
  $('#apmprovpercentvid').html((Math.round(apmprovpercentsumma / pardskaits * 20) / 20) + '%');
  $('#izmprovsumma').html('EUR&nbsp;' + izmprovsumma.toFixed(2));

  $('#aprpiemsumma').html('EUR&nbsp;' + aprpiemsumma.toFixed(2));
  $('#izmpiemsumma').html('EUR&nbsp;' + izmpiemsumma.toFixed(2));

  $('#izmprovvid').html('EUR&nbsp;' + (izmprovsumma / pardskaits).toFixed(2));
  $('#izmpiemvid').html('EUR&nbsp;' + (izmpiemsumma / pardskaits).toFixed(2));

  $('#rekapmpercentvid').html((Math.round(rekapmpercentsumma / pardskaits * 20) / 20) + '%');
  $('#deptkredsumma').html('EUR&nbsp;' + deptkredsumma.toFixed(2));

  initTableGroupColors($("#darbinieki_kopsavilkums"));

}

$("#date_from").datepicker({dateFormat:'dd.mm.yy'});
$("#date_to").datepicker({dateFormat:'dd.mm.yy'});

$("form#period_filter").each(function() {

  var form = $(this);

  $('input#date_from, input#date_to, select#stats_quarter, input#inactive', form).change(function() {
    form.submit();
  });

  $('a.clear_form', form).click(function() {
    $('input#date_from, input#date_to', form).val('');
    form.submit();
  });

});

</script>

<? if (!is_admin()) { ?>

  <form id="liguma_check_form" method="" action="">

    Meklēt klientu: <input class="nosaukums" type="text" />

    <input type="submit" value="Meklēt" />

    <div class="result">


    </div>

  </form>

  <script>
  $('form#liguma_check_form').submit(function() {

     var loading_html = '<div class="loading" style="text-align: center;"><img alt="Notiek ielāde..." src="css/loading.gif" /></div>';

     var holder = $('div.result', this);

     holder.html(loading_html);

     $.get('ajax.php?action=check_client', {nosauk: $('.nosaukums', this).val()}, function(result) {

       holder.html(result);

     })

     return false;

  });
  </script>

<? } ?>