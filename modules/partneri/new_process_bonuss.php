<?php
if(!check_access('darbinieki-admin') || (!check_access('darbinieki-bonuss') && $pardeveja_id != $_SESSION['user']['id'])) {
  die('Jums nav pieejas tiesību šai darbībai');
}

if (!empty($_POST)) {

    if (check_access('darbinieki-labot-bonuss')) {

      // inserting files
      process_files(array('darb_izpilde'), $pardeveja_id);

    }


    // inserting comments
    process_comments(array('darb_izpilde'), $pardeveja_id);

  if(check_access('darbinieki-bonuss')) {
  $errors = validate_darbinieks_bonuss_form($_POST);

  if (empty($errors)) {

    $log_data = array();

    $_POST = trim_array($_POST);

    if (check_access('darbinieki-labot-bonuss') && isset($_POST['form_sent'])) {

      $log_data['bonuss_plans']['old_m'] = db_get_assoc_all(db_query("SELECT * FROM `".DB_PREF."bonusi_plans` WHERE `vaditajs_id` = " . $pardeveja_id));
      $log_data['bonuss_procenti']['old_m'] = db_get_assoc_all(db_query("SELECT * FROM `".DB_PREF."bonusi_premijas` WHERE `vaditajs_id` = " . $pardeveja_id));

      $year = esc($_POST['year']);
      $vaditajs_id = esc($_GET['id']);

      //  Plāns
      db_query('DELETE FROM '.DB_PREF.'bonusi_plans WHERE vaditajs_id = ' . $vaditajs_id . ' AND gads = ' . $year);
      foreach((array)$_POST['plans'] as $period => $data) {
        //  Nosakam periodu

        list($type, $period) = explode('-', $period);
        if($type == 'quarter') {
          $quarter = esc($period);
          $month = '(null)';
        } elseif($type == 'month') {
          $quarter = '(null)';
          $month = esc($period);
        } elseif($type == 'year') {
          $quarter = '(null)';
          $month = '(null)';
        }

        $min = $data['min'] ? str_replace(',', '.', esc($data['min'])) : '0';
        $opt = $data['opt'] ? str_replace(',', '.', esc($data['opt'])) : '0';

        db_query('
          INSERT INTO '.DB_PREF.'bonusi_plans
          SET
            vaditajs_id = ' . $vaditajs_id . ',
            gads = ' . $year . ',
            ceturksnis = ' . ($quarter ? $quarter : '(null)') . ',
            menesis = ' . ($month ? $month : '(null)') . ',
            minimalais = ' . $min . ',
            optimalais = ' . $opt . '
        ');
      }

      //  Plāns
      db_query('DELETE FROM '.DB_PREF.'bonusi_premijas WHERE vaditajs_id = ' . $vaditajs_id . ' AND gads = ' . $year);
      foreach((array)$_POST['proc'] as $period => $data) {
        //  Nosakam periodu

        list($type, $period) = explode('-', $period);
        if($type == 'quarter') {
          $quarter = esc($period);
          $month = '(null)';
        } elseif($type == 'month') {
          $quarter = '(null)';
          $month = esc($period);
        } elseif($type == 'year') {
          $quarter = '(null)';
          $month = '(null)';
        }

        $min = $data['min'] ? str_replace(',', '.', esc($data['min'])) : '0';
        $opt = $data['opt'] ? str_replace(',', '.', esc($data['opt'])) : '0';

        db_query('
          INSERT INTO '.DB_PREF.'bonusi_premijas
          SET
            vaditajs_id = ' . $vaditajs_id . ',
            gads = ' . $year . ',
            ceturksnis = ' . ($quarter ? $quarter : '(null)') . ',
            menesis = ' . ($month ? $month : '(null)') . ',
            min = ' . $min . ',
            opt = ' . $opt . '
        ');
      }

      $log_data['bonuss_plans']['new_m'] = db_get_assoc_all(db_query("SELECT * FROM `".DB_PREF."bonusi_plans` WHERE `vaditajs_id` = " . $pardeveja_id));
      $log_data['bonuss_plans']['title'] = sprintf('Laboja vadītāja %s bonusu plānu', db_get_val(db_query("SELECT vards FROM `".DB_PREF."pardeveji` WHERE id = ".$pardeveja_id." LIMIT 1")));
      $log_data['bonuss_procenti']['new_m'] = db_get_assoc_all(db_query("SELECT * FROM `".DB_PREF."bonusi_premijas` WHERE `vaditajs_id` = " . $pardeveja_id));
      $log_data['bonuss_procenti']['title'] = sprintf('Laboja vadītāja %s bonusu procentus', db_get_val(db_query("SELECT vards FROM `".DB_PREF."pardeveji` WHERE id = ".$pardeveja_id." LIMIT 1")));

    }

    log_add("laboja", $log_data);

    header('Location: ?c=partneri&a=labot&id=' . $pardeveja_id . '&subtab=' . $subtab . '&bonuss_year=' . ($year ? $year : ''));
    die();

    }
    }

}
?>