<?php
if(!check_access('darbinieki-bonuss') && !$d['vaditajs']) {
  die('Jums nav pieejas tiesību šai sadaļai');
}

//  Bonuss
$bonusi = array();
$years = array();
//for($year = BONUSS_START_YEAR; $year <= date('Y'); $year++) {
for($year = date('Y'); $year >= BONUSS_START_YEAR; $year--) {
  $data = get_bonuss_stats($pardeveja_id, $year, true, true);
  $years[] = $year;
  $rows = cleanup_bonuss($data, $year);
  foreach($rows as $row) {
    $bonusi[] = $row;
  }
}

?>

<h4>Saņemtie bonusi</h4>

<script>
var searchlist_bonuss = [];

var sp_bonuss = create_filter_field_array(['s_gads', 's_ceturksnis', 's_menesis',
  /*'s_summa', 's_min_proc', 's_opt_proc', 's_virs_min', 's_virs_opt', 's_summa_min',
  's_summa_opt',*/ 's_kopsumma_wo_pvn', 's_pvn', 's_kopsumma'
]);
</script>

<table style="display:none;">
  <tr id="bonusi_filter">
    <th>
      <select class="search_field advonchange" searchclass="s_gads" meth="standart">
        <option value=""></option>
        <?php foreach($years as $byear){ ?>
          <option value="<?=$byear;?>"><?=$byear;?></option>
        <?php } ?>
      </select>
      <a class="search_field" href="#">x</a>
    </th>

    <th>
      <select class="search_field advonchange" searchclass="s_ceturksnis" meth="standart">
        <option value=""></option>
        <?php foreach(range(1, 4) as $bquarter){ ?>
          <option value="<?=$bquarter;?>"><?=$_vars['quarters_roman'][$bquarter];?></option>
        <?php } ?>
      </select>
      <a class="search_field" href="#">x</a>
    </th>

    <th>
      <select class="search_field advonchange" searchclass="s_menesis" meth="standart">
        <option value=""></option>
        <?php foreach(range(1, 12) as $bmonth){ ?>
          <option value="<?=$bmonth;?>"><?=$_vars['menesi'][$bmonth];?></option>
        <?php } ?>
      </select>
      <a class="search_field" href="#">x</a>
    </th>

    <!--<th><span><input class="search_field" meth="coin" searchclass="s_summa" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <th><span><input class="search_field" meth="coin" searchclass="s_min_proc" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <th><span><input class="search_field" meth="coin" searchclass="s_opt_proc" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <th><span><input class="search_field" meth="coin" searchclass="s_virs_min" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <th><span><input class="search_field" meth="coin" searchclass="s_virs_opt" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <th><span><input class="search_field" meth="coin" searchclass="s_summa_min" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <th><span><input class="search_field" meth="coin" searchclass="s_summa_opt" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    -->
    <th><span><input class="search_field" meth="coin" searchclass="s_kopsumma_wo_pvn" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <th><span><input class="search_field" meth="coin" searchclass="s_pvn" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <th><span><input class="search_field" meth="coin" searchclass="s_kopsumma" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
  </tr>
</table>

<table id="bonusi" class="data" style="width: 970px;">

  <thead>
      <tr class="last header">
        <th>Gads</th>
        <th>Ceturksnis</th>
        <th>Mēnesis</th>
        <!--
        <th>Sasniegtais EUR</th>
        <th>Proc. min. %</th>
        <th>Proc. opt. %</th>
        <th>Virs min. EUR</th>
        <th>Virs opt. EUR</th>
        <th>Summa min. EUR</th>
        <th>Summa opt. EUR</th>
        -->
        <th>Summa EUR (bez PVN)</th>
        <th>PVN&nbsp;EUR</th>
        <th>Summa<br />kopā&nbsp;EUR<br />(ar PVN)</th>
      </tr>
    </thead>

  <tbody class="main">

    <? $inid = -1;?>

    <? foreach($bonusi as $bonuss) { ?>

      <?php
      $inid ++;
      $month_quarter = $bonuss['month'] ? ceil($bonuss['month']/3) : null;
      $pvn = $bonuss['kopsumma_wo_pvn'] * ($d['pvn_likme'] / 100);
      $kopsumma = $bonuss['kopsumma_wo_pvn'] + $pvn;
      ?>
      <tr id="bonuss_tr<?=$inid;?>" class="tr_bonuss">
        <td class="c"><?= $bonuss['year'] ?></td>
        <td class="c"><?= $bonuss['quarter'] ? $_vars['quarters_roman'][$bonuss['quarter']] : ($month_quarter ?  $_vars['quarters_roman'][$month_quarter] : ''); ?></td>
        <td class="c"><?= ($bonuss['month'] ? $_vars['menesi'][$bonuss['month']] : ($bonuss['quarter'] ? 'ceturksnis' : 'gads')); ?></td>
        <!--<td class="r"><?= $bonuss['summa'] ?></td>
        <td class="c"><?= $bonuss['min_proc'] ?></td>
        <td class="c"><?= $bonuss['opt_proc'] ?></td>
        <td class="r"><?= format_currency($bonuss['virs_min']); ?></td>
        <td class="r"><?= format_currency($bonuss['virs_opt']); ?></td>
        <td class="r"><?= format_currency($bonuss['summa_min']); ?></td>
        <td class="r"><?= format_currency($bonuss['summa_opt']); ?></td>    -->
        <td class="r bonuss_kopsumma_wo_pvn"><?= format_currency($bonuss['kopsumma_wo_pvn']); ?></td>
        <td class="r bonuss_pvn"><?php echo format_currency($pvn);?></td>
        <td class="r last bonuss_kopsumma"><?php echo format_currency($kopsumma);?></td>
        <td style="display: none;">
          <input type="hidden" class="row_id" value="<?=$inid;?>" />
          <script type="text/javascript">
           searchlist_bonuss[<?=$inid;?>] = '<?=addslashes(mb_strtoupper($bonuss['bid']));?>';
           sp_bonuss['s_gads'][<?=$inid;?>] = '<?=addslashes(mb_strtoupper($bonuss['year']));?>';
           sp_bonuss['s_ceturksnis'][<?=$inid;?>] = '<?=addslashes(mb_strtoupper(($bonuss['quarter'] ? $bonuss['quarter'] : ($month_quarter ? $month_quarter : null))));?>';
           sp_bonuss['s_menesis'][<?=$inid;?>] = '<?=addslashes(mb_strtoupper($bonuss['month']));?>';
           /*sp_bonuss['s_summa'][<?=$inid;?>] = '<?=addslashes(mb_strtoupper($bonuss['summa']));?>';
           sp_bonuss['s_min_proc'][<?=$inid;?>] = '<?=addslashes(mb_strtoupper($bonuss['min_proc']));?>';
           sp_bonuss['s_opt_proc'][<?=$inid;?>] = '<?=addslashes(mb_strtoupper($bonuss['opt_proc']));?>';
           sp_bonuss['s_virs_min'][<?=$inid;?>] = '<?=addslashes(mb_strtoupper($bonuss['virs_min']));?>';
           sp_bonuss['s_virs_opt'][<?=$inid;?>] = '<?=addslashes(mb_strtoupper($bonuss['virs_opt']));?>';
           sp_bonuss['s_summa_min'][<?=$inid;?>] = '<?=addslashes(mb_strtoupper($bonuss['summa_min']));?>';
           sp_bonuss['s_summa_opt'][<?=$inid;?>] = '<?=addslashes(mb_strtoupper($bonuss['summa_opt']));?>'; */
           sp_bonuss['s_kopsumma_wo_pvn'][<?=$inid;?>] = '<?=addslashes(mb_strtoupper($bonuss['kopsumma_wo_pvn']));?>';
           sp_bonuss['s_pvn'][<?=$inid;?>] = '<?=addslashes(mb_strtoupper($pvn));?>';
           sp_bonuss['s_kopsumma'][<?=$inid;?>] = '<?=addslashes(mb_strtoupper($kopsumma));?>';

          </script>
        </td>
      </tr>

    <? } ?>

  </tbody>

  <tr>
    <td align="right" colspan="3"><strong>Kopā:</strong></td>
    <!--<td class="r" id="bonuss_summa_total">0</td>
    <td></td>
    <td></td>
    <td class="r" id="bonuss_virs_min_total">0</td>
    <td class="r" id="bonuss_virs_opt_total">0</td>
    <td class="r" id="bonuss_summa_min_total">0</td>
    <td class="r" id="bonuss_summa_opt_total">0</td>       -->
    <td class="r" id="bonuss_kopsumma_wo_pvn_total">0</td>
    <td class="r" id="bonuss_pvn_total">0</td>
    <td class="r" id="bonuss_kopsumma_total">0</td>
  </tr>

</table>

<script>
$(document).ready(function() {

  var table = $("#bonusi");
  var head = $("#bonusi_filter");

  table.tablesorter({
    debug: false,
    highlightClass: 'highlight',
    headers: {
      0: {sorter: 'text'},
      1: {sorter: 'text'},
      2: {sorter: 'text'},
      3: {sorter: 'digit'},
      4: {sorter: 'digit'},
      5: {sorter: 'digit'},
    }
  }).bind('sortStart', function() {
    table.trigger("update");
  }).bind('sortEnd', function() {
    stripeTable(table);
  });

  initTableFilter(table, head, processTable, 'bonuss', searchlist_bonuss, sp_bonuss);

});

function processTable() {
  //  Bonuss
  /*var bonuss_summa_total = 0
  var bonuss_virs_min_total = 0;
  var bonuss_virs_opt_total = 0;
  var bonuss_summa_min_total = 0;
  var bonuss_summa_opt_total = 0  */
  var bonuss_kopsumma_wo_pvn_total = 0
  var bonuss_pvn_total = 0
  var bonuss_kopsumma_total = 0

  jQuery.each(searchlist_bonuss, function(k,v){

    var row = $('#bonuss_tr' + k);

    if (row.is(':visible')) {

      /*if(sp_bonuss['s_summa'][k] > 0) bonuss_summa_total += parseFloat(sp_bonuss['s_summa'][k])
      if(sp_bonuss['s_virs_min'][k] > 0) bonuss_virs_min_total += parseFloat(sp_bonuss['s_virs_min'][k])
      if(sp_bonuss['s_virs_opt'][k] > 0) bonuss_virs_opt_total += parseFloat(sp_bonuss['s_virs_opt'][k])
      if(sp_bonuss['s_summa_min'][k] > 0) bonuss_summa_min_total += parseFloat(sp_bonuss['s_summa_min'][k])
      if(sp_bonuss['s_summa_opt'][k] > 0) bonuss_summa_opt_total += parseFloat(sp_bonuss['s_summa_opt'][k])   */
      if(sp_bonuss['s_kopsumma_wo_pvn'][k] > 0) bonuss_kopsumma_wo_pvn_total += parseFloat(sp_bonuss['s_kopsumma_wo_pvn'][k])
      if(sp_bonuss['s_pvn'][k] > 0) bonuss_pvn_total += parseFloat(sp_bonuss['s_pvn'][k])
      if(sp_bonuss['s_kopsumma'][k] > 0) bonuss_kopsumma_total += parseFloat(sp_bonuss['s_kopsumma'][k])

    }

    /*$('#bonuss_summa_total').html(bonuss_summa_total.toFixed(2));
    $('#bonuss_virs_min_total').html(bonuss_virs_min_total.toFixed(2));
    $('#bonuss_virs_opt_total').html(bonuss_virs_opt_total.toFixed(2));
    $('#bonuss_summa_min_total').html(bonuss_summa_min_total.toFixed(2));
    $('#bonuss_summa_opt_total').html(bonuss_summa_opt_total.toFixed(2));    */
    $('#bonuss_kopsumma_wo_pvn_total').html(bonuss_kopsumma_wo_pvn_total.toFixed(2));
    $('#bonuss_pvn_total').html(bonuss_pvn_total.toFixed(2));
    $('#bonuss_kopsumma_total').html(bonuss_kopsumma_total.toFixed(2));
  });
}
</script>

<h4>Korekcijas</h4>

<?php
$korekcijas_data = corrections_total($pardeveja_id);
?>

<script>
var searchlist_korekcija = [];
var sp_korekcija = create_filter_field_array(['s_kopsumma_wo_pvn', 's_pvn', 's_kopsumma'
]);
</script>

<table style="display:none;">
  <tr id="korekcijas_filter">
    <th></th>
    <th><span><input class="search_field" meth="coin" searchclass="s_kopsumma_wo_pvn" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <th><span><input class="search_field" meth="coin" searchclass="s_pvn" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <th><span><input class="search_field" meth="coin" searchclass="s_kopsumma" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
  </tr>
</table>

<table id="korekcijas" class="data" style="width: 970px;">

  <thead>
      <tr class="last header">
        <th></th>
        <th>Summa EUR (bez PVN)</th>
        <th>PVN&nbsp;EUR</th>
        <th>Summa<br />kopā&nbsp;EUR<br />(ar PVN)</th>
      </tr>
    </thead>

  <tbody class="main">

    <? $inid = -1;
    foreach($korekcijas_data as $i => $korekcija) {
      $inid ++;
      $korekcija['nosaukums'] = 'Tekošā korekcija par iepriekšējiem periodiem';

      $pvn = $korekcija['kopsumma_wo_pvn'] * ($d['pvn_likme'] / 100);
      $kopsumma = $korekcija['kopsumma_wo_pvn'] + $pvn;
      ?>
      <tr id="korekcija_tr<?=$inid;?>" class="tr_bonuss">
        <td class="c"><?= $korekcija['nosaukums'] ?></td>
        <td class="r korekcija_kopsumma_wo_pvn">
          <?= format_currency($korekcija['kopsumma_wo_pvn']); ?>
          <?php if(!bccomp($korekcija['pvn'], '0', 2)) { ?>
            <span class="bez_pvn_rek_ico" style="margin-top: 2px;">!</span>
          <?php }  ?>
        </td>
        <td class="r korekcija_pvn"><?php echo format_currency($pvn);?></td>
        <td class="r last korekcija_kopsumma"><?php echo format_currency($kopsumma);?></td>
        <td style="display: none;">
          <input type="hidden" class="row_id" value="<?=$inid;?>" />
          <script type="text/javascript">
           searchlist_korekcija[<?=$inid;?>] = '<?=addslashes(mb_strtoupper($i));?>';
           sp_korekcija['s_kopsumma_wo_pvn'][<?=$inid;?>] = '<?=addslashes(format_currency($korekcija['kopsumma_wo_pvn']));?>';
           sp_korekcija['s_pvn'][<?=$inid;?>] = '<?=addslashes(format_currency($pvn));?>';
           sp_korekcija['s_kopsumma'][<?=$inid;?>] = '<?=addslashes(format_currency($kopsumma));?>';

          </script>
        </td>
      </tr>

    <? } ?>

  </tbody>

  <tr>
    <td align="right"><strong>Kopā:</strong></td>
    <td class="r" id="korekcija_kopsumma_wo_pvn_total">0</td>
    <td class="r" id="korekcija_pvn_total">0</td>
    <td class="r" id="korekcija_kopsumma_total">0</td>
  </tr>

</table>

<script>

$(document).ready(function() {

  $('#korekcijas .bez_pvn_rek_ico').wTooltip({
    content: 'Korekcija ir par bonusiem, kuri tika izmaksāti bez PVN',
    follow: true,
    offsetY: 0,
    offsetX: 10
  });

});

$(document).ready(function() {

  var table = $("#korekcijas");
  var head = $("#korekcijas_filter");

  table.tablesorter({
    debug: false,
    highlightClass: 'highlight',
    headers: {
      0: {sorter: 'text'},
      1: {sorter: 'digit'},
      2: {sorter: 'digit'},
      3: {sorter: 'digit'},
    }
  }).bind('sortStart', function() {
    table.trigger("update");
  }).bind('sortEnd', function() {
    stripeTable(table);
  });

  initTableFilter(table, head, processTableK, 'korekcija', searchlist_korekcija, sp_korekcija);

});

function processTableK() {
  var korekcija_kopsumma_wo_pvn_total = 0
  var korekcija_pvn_total = 0
  var korekcija_kopsumma_total = 0

  jQuery.each(searchlist_korekcija, function(k,v){

    var row = $('#korekcija_tr' + k);

    if (row.is(':visible')) {

      if(sp_korekcija['s_kopsumma_wo_pvn'][k] != 0) {
        korekcija_kopsumma_wo_pvn_total += parseFloat(sp_korekcija['s_kopsumma_wo_pvn'][k])
      }
      if(sp_korekcija['s_pvn'][k] != 0) korekcija_pvn_total += parseFloat(sp_korekcija['s_pvn'][k])
      if(sp_korekcija['s_kopsumma'][k] != 0) korekcija_kopsumma_total += parseFloat(sp_korekcija['s_kopsumma'][k])

    }

    $('#korekcija_kopsumma_wo_pvn_total').html(korekcija_kopsumma_wo_pvn_total.toFixed(2));
    $('#korekcija_pvn_total').html(korekcija_pvn_total.toFixed(2));
    $('#korekcija_kopsumma_total').html(korekcija_kopsumma_total.toFixed(2));
  });
}
</script>

