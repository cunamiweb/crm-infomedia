<?php

if (isset($_GET['subtab2']) && in_array($_GET['subtab2'], array('provizijas', 'premijas'))) {
  $subtab2 = $_GET['subtab2'];

} elseif(isset($_GET['subtab2']) && $_GET['subtab2'] == 'bonusi' && $d['vaditajs']) {
  $subtab2 = 'bonusi';
} elseif($d['vaditajs']) {
  $subtab2 = 'bonusi';
} else {
  $subtab2 = 'provizijas';
}

log_add("atvera", "Atvēra darbinieka " . $d['vards'] . " maksājumus");

?>

<form action="?c=partneri<?= !empty($pardeveja_id) ? '&a=labot&id=' . $pardeveja_id . '&subtab=maksajumi&subtab2=' . $subtab2 : '&a=jauns' ;?>" id="fullformplace" method="post" enctype="multipart/form-data">

  <div class="subtab2 sub-menu"><span>
    <?php
    if(!empty($pardeveja_id)) {
      $s2link = '&a=labot&id=' . $pardeveja_id . '&subtab=maksajumi&subtab2=';
    }
    ?>
    <a href="?c=partneri<?= !empty($pardeveja_id) ? $s2link . 'provizijas' : '&a=jauns' ;?>" <?php echo $subtab2 == 'provizijas' ? 'class="active"' : '';?>>Komisijas</a>

  </span></div>

  <?php
  require('maksajumi_' . $subtab2 . '.php');
  ?>

</form>