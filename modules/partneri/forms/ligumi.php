<?
log_add("atvera", "Atvēra darbinieka " . $d['vards'] . " līgumus");

$active_page = !empty($_GET['page']) ? (int)$_GET['page'] : 1;

if (!isset($_GET['show_in_page'])) {
  $show_in_page = 30;
} elseif ($_GET['show_in_page'] > 0) {
  $show_in_page = (int)$_GET['show_in_page'];
} else {
  $show_in_page = '';
}

$order_bys = array(
  'lig_nr' => 'g.ligumanr',
  'nosauk' => 'g.nosaukums',
  'indekss' => 'g.faktaddr_index',
  'lig_statuss' => '',
  'kopapm_statuss' => '',
  'lig_datums' => 'g.ligumadat',
  'san_datums' => 'g.sanemsanas_datums',
  'san_forma' => 'g.sanemsanas_forma',
  'lig_no' => 'g.reklama_no',
  'lig_lidz' => 'g.reklama_lidz',
  'summa' => 'rekinu_summa',
  'pvn' => 'rekinu_pvn_summa',
  'kopsumma' => 'rekinu_kopsumma',
  'apm_summa' => 'apm_kopsumma',
  'prov_proc' => 'g.provizija_pardevejam',
  'atl_vad' => 'g.atlidziba_vaditajam',
  'prov_bez_pvn' => 'prov_summa',
  'izm_prov_bez_pvn' => 'apm_prov_summa',
  'izm_prov_proc' => '',
  'akt_atgad' => 'aktivi_atgadinajumi'
);

$order = '';
$direction = 'asc';

if (!empty($_GET['order']) && isset($order_bys[$_GET['order']])) {
  $order = $order_bys[$_GET['order']];
}

if (!empty($_GET['direction']) && in_array(strtolower($_GET['direction']), array('asc', 'desc'))) {
  $direction = strtolower($_GET['direction']);
};


$query = db_query("
  SELECT r.*
  FROM `".DB_PREF."rekini` r
  LEFT JOIN `".DB_PREF."ligumi` g ON (g.id = r.liguma_id)
  WHERE
    g.`pardeveja_id` = ".$pardeveja_id."
");
$lig_rekini = array();

while($row = db_get_assoc($query)){
  $lig_rekini[$row['liguma_id']][] = htmlesc($row);
}

$query = db_query("
  SELECT
    s.liguma_id,
    p.*
  FROM `".DB_PREF."ligumi_saistitie_pardeveji` s
  LEFT JOIN `".DB_PREF."pardeveji` p ON (p.id = s.pardeveja_id)
");

$saist_pard = array();

while($row = db_get_assoc($query)){
  $saist_pard[$row['liguma_id']][] = $row;
}

$where = array();
$having = array();

if (isset($_GET['fligumu_id'])) {
  if (getGet('fligumu_id'))
    $where[] = 'g.id IN ('.getGet('fligumu_id').')';
  else
    $where[] = 'g.id IS NULL';
}

$where[] = 'g.pardeveja_id = '.$pardeveja_id;

if (getGet('fnosaukums') !== '') $where[] = 'g.nosaukums LIKE "%'.getGet('fnosaukums').'%"';
if (getGet('fliguma_nr') !== '') $where[] = 'g.ligumanr LIKE "%'.getGet('fliguma_nr').'%"';
if (getGet('ffaktaddr_index') !== '') $where[] = 'g.faktaddr_index LIKE "%'.getGet('ffaktaddr_index').'%"';

if (getGet('flig_no') !== '') $where[] = 'g.ligumadat >= "'.to_db_date(getGet('flig_no')).'"';
if (getGet('flig_lidz') !== '') $where[] = 'g.ligumadat <= "'.to_db_date(getGet('flig_lidz')).'"';
if (getGet('flig_sanemts_no') !== '') $where[] = 'g.sanemsanas_datums >= "'.to_db_date(getGet('flig_sanemts_no')).'"';
if (getGet('flig_sanemts_lidz') !== '') $where[] = 'g.sanemsanas_datums <= "'.to_db_date(getGet('flig_sanemts_lidz')).'"';
if (getGet('frekl_sak_no') !== '') $where[] = 'g.reklama_no >= "'.to_db_date(getGet('frekl_sak_no')).'"';
if (getGet('frekl_sak_lidz') !== '') $where[] = 'g.reklama_no <= "'.to_db_date(getGet('frekl_sak_lidz')).'"';
if (getGet('frekl_beig_no') !== '') $where[] = 'g.reklama_lidz >= "'.to_db_date(getGet('frekl_beig_no')).'"';
if (getGet('frekl_beig_lidz') !== '') $where[] = 'g.reklama_lidz <= "'.to_db_date(getGet('frekl_beig_lidz')).'"';

if (pt_multiselect_is_active('fsanems_forma')) $where[] = 'g.sanemsanas_forma IN (' . implode(',', array_map('intval', getGet('fsanems_forma'))) . ')';

if (getGet('flig_summa') !== '') $having[] = 'FLOOR(rekinu_summa) = '.(int)getGet('flig_summa');
if (getGet('flig_pvn') !== '') $having[] = 'FLOOR(rekinu_pvn_summa) = '.(int)getGet('flig_pvn');
if (getGet('flig_kopsumma') !== '') $having[] = 'FLOOR(rekinu_kopsumma) = '.(int)getGet('flig_kopsumma');
if (getGet('fapmaks_summa') !== '') $having[] = 'FLOOR(apm_kopsumma) = '.(int)getGet('fapmaks_summa');

if (getGet('fprov_percent') !== '') $having[] = 'FLOOR(g.provizija_pardevejam) = '.(int)getGet('fprov_percent');
if (getGet('fatl_vad') !== '') $having[] = 'FLOOR(g.atlidziba_vaditajam) = '.(int)getGet('fatl_vad');
if (getGet('fprov_summa') !== '') $having[] = 'FLOOR(prov_summa) = '.(int)getGet('fprov_summa');
if (getGet('fapm_prov_summa') !== '') $having[] = 'FLOOR(apm_prov_summa) = '.(int)getGet('fapm_prov_summa');

$atg_having = array();
if (pt_is_selected(1, 'fakt_atgad')) $atg_having[] = 'aktivi_atgadinajumi > 0';
if (pt_is_selected(2, 'fakt_atgad')) $atg_having[] = 'aktivi_atgadinajumi = 0';
if (!empty($atg_having)) $having[] = '(' . implode(' OR ', $atg_having) . ')';

$sql = "
  SELECT
    g.*,
    b.beigu_statuss,
    b.atlikt_lidz as beigu_statuss_atlikt_lidz,
    b.auto_atteikums as beigu_statuss_auto_atteikums,
    SUM(r.summa) as rekinu_summa,
    SUM(r.kopsumma) - SUM(r.summa) as rekinu_pvn_summa,
    SUM(r.kopsumma) as rekinu_kopsumma,
    SUM(r.apm_summa) as apm_kopsumma,
    (SUM(r.summa) * (g.provizija_pardevejam) / 100) as prov_summa,
    (
      SELECT COUNT(*)
      FROM `".DB_PREF."atgadinajumi`
      WHERE
        liguma_id = g.id AND
        statuss = 1 AND
        datums <= CURDATE() AND
        redzamiba IN ('kopejs', ".(is_admin() ? "'admin'" : "'pardevejs'").")
    ) as aktivi_atgadinajumi,
    (
      SELECT COUNT(*)
      FROM `".DB_PREF."ligumi_saistitie_pardeveji`
      WHERE liguma_id = g.id
    ) as saistito_pardeveju_skaits,
    (
      SELECT SUM(sd.summa)
      FROM `".DB_PREF."pardeveji_rekini_detalas` sd
      LEFT JOIN `".DB_PREF."pardeveji_rekini` sp ON (sp.id = sd.pardeveja_rekina_id)
      LEFT JOIN `".DB_PREF."rekini` sr ON (sr.id = sd.rekina_id)
      WHERE
        sr.liguma_id = g.id AND
        sp.pardeveja_id = g.pardeveja_id AND
        sp.statuss = 2
    ) as apm_prov_summa
  FROM `".DB_PREF."ligumi` g
  LEFT JOIN `".DB_PREF."ligumi_beigusies` b ON (g.id = b.liguma_id)
  LEFT JOIN `".DB_PREF."rekini` r ON (
    r.liguma_id = g.id AND
    r.atkapsanas_rekins = 0
  )
";

if (!empty($where)) {
  $sql .= " WHERE " . implode(' AND ', $where);
}

$sql .= " GROUP BY g.id";

if (!empty($having)) {
  $sql .= " HAVING " . implode(' AND ', $having);
}

$sql .= " ORDER BY " . (!empty($order) ? $order . " " . $direction : ' IF(g.`sanemsanas_datums` IS NULL, 0, 1) ASC, g.`sanemsanas_datums` DESC, g.`ligumadat` DESC, g.`ligumanr` DESC');

$query = db_query($sql);

$alldata = array();

// apply filtering to complex columns

while($row = db_get_assoc($query)){

  $row['liguma_statuss'] = get_liguma_statuss($row, !empty($lig_rekini[$row['id']]) ? $lig_rekini[$row['id']] : null);

  if (pt_multiselect_is_active('flig_statuss')) {

    $show = false;

    if (pt_is_selected($row['liguma_statuss'], 'flig_statuss')) {
      $show = true;
    } elseif ($row['liguma_statuss'] == 6 && (empty($row['beigu_statuss']) || $row['beigu_statuss'] == 'termins') && pt_is_selected('6_termins', 'flig_statuss')) {
      $show = true;
    } elseif ($row['liguma_statuss'] == 6 && $row['beigu_statuss'] == 'parslegts' && pt_is_selected('6_parslegts', 'flig_statuss')) {
      $show = true;
    } elseif ($row['liguma_statuss'] == 6 && $row['beigu_statuss'] == 'atteikums' && pt_is_selected('6_atteikums', 'flig_statuss')) {
      $show = true;
    } elseif ($row['liguma_statuss'] == 6 && !empty($row['beigu_statuss_auto_atteikums']) && pt_is_selected('6_atteikums_p', 'flig_statuss')) {
      $show = true;
    }

    if (!$show) continue;

  }

  $row['norekinu_statuss'] = get_apmaksas_statuss(!empty($lig_rekini[$row['id']]) ? $lig_rekini[$row['id']] : null);

  if (pt_multiselect_is_active('fkopapm_statuss')) {

    $show = false;

    if (pt_is_selected($row['norekinu_statuss'], 'fkopapm_statuss')) {
      $show = true;
    } else if (pt_is_selected('2_barteris', 'fkopapm_statuss')) {
      if ($row['norekinu_statuss'] == 2 && is_barteris(!empty($lig_rekini[$row['id']]) ? $lig_rekini[$row['id']] : null)) $show = true;
    }

    if (!$show) continue;

  }

  $row['max_nokavetas_dienas'] = 0;
  if ($row['norekinu_statuss'] == 4) {
    $row['max_nokavetas_dienas'] = get_max_nokavetas_dienas(!empty($lig_rekini[$row['id']]) ? $lig_rekini[$row['id']] : null);
  }

  $row['apm_prov_percent'] = 0;
  if ($row['rekinu_summa'] * ($row['provizija_pardevejam'] / 100) > 0) {
    $row['apm_prov_percent'] = $row['apm_prov_summa'] * 100 / ($row['rekinu_summa'] * ($row['provizija_pardevejam'] / 100));
  }

  if (getGet('fapm_prov_percent') !== '') {
    if ((int)floor($row['apm_prov_percent']) !== (int)getGet('fapm_prov_percent')) {
      continue;
    }
  }

  $alldata[] = $row;

}

$total_rows = count($alldata);

// add apply sorting to complex columns

if (getGet('order') == 'lig_statuss') {

  function sort_by_lig_statuss_asc($a, $b) {
    return $a['liguma_statuss'] == $b['liguma_statuss'] ? 0 : (($a['liguma_statuss'] < $b['liguma_statuss']) ? -1 : 1);
  }

  function sort_by_lig_statuss_desc($a, $b) {
    return $a['liguma_statuss'] == $b['liguma_statuss'] ? 0 : (($a['liguma_statuss'] > $b['liguma_statuss']) ? -1 : 1);
  }

  usort($alldata, 'sort_by_lig_statuss_' . $direction);

}

if (getGet('order') == 'kopapm_statuss') {

  function sort_by_kopapm_statuss_asc($a, $b) {

    if ($a['norekinu_statuss'] == $b['norekinu_statuss']) {
      return $a['max_nokavetas_dienas'] == $b['max_nokavetas_dienas'] ? 0 : ($a['max_nokavetas_dienas'] < $b['max_nokavetas_dienas'] ? -1 : 1);
    } else {
      return $a['norekinu_statuss'] < $b['norekinu_statuss'] ? -1 : 1;
    }

  }

  function sort_by_kopapm_statuss_desc($a, $b) {

    if ($a['norekinu_statuss'] == $b['norekinu_statuss']) {
      return $a['max_nokavetas_dienas'] == $b['max_nokavetas_dienas'] ? 0 : ($a['max_nokavetas_dienas'] > $b['max_nokavetas_dienas'] ? -1 : 1);
    } else {
      return $a['norekinu_statuss'] > $b['norekinu_statuss'] ? -1 : 1;
    }

  }

  usort($alldata, 'sort_by_kopapm_statuss_' . $direction);

}

if (getGet('order') == 'izm_prov_proc') {

  function sort_by_izm_prov_proc_asc($a, $b) {
    return $a['apm_prov_percent'] == $b['apm_prov_percent'] ? 0 : (($a['apm_prov_percent'] < $b['apm_prov_percent']) ? -1 : 1);
  }

  function sort_by_izm_prov_proc_desc($a, $b) {
    return $a['apm_prov_percent'] == $b['apm_prov_percent'] ? 0 : (($a['apm_prov_percent'] > $b['apm_prov_percent']) ? -1 : 1);
  }

  usort($alldata, 'sort_by_izm_prov_proc_' . $direction);

}


// limit rows to show

if ($show_in_page > 0) {

  $visibledata = array_slice($alldata, ($active_page - 1) * $show_in_page, $show_in_page);

  $hiddendata = array_slice($alldata, 0, ($active_page - 1) * $show_in_page);

  $hiddendata = array_merge(
    $hiddendata,
    array_slice($alldata, (($active_page - 1) * $show_in_page) + $show_in_page)
  );

} else {
  $visibledata = $alldata;
  $hiddendata = array();
}

$visible_rows = count($visibledata);

?>

<form action="" id="darbinieka_ligumi_form" method="GET" style="clear: both;">


  <input type="hidden" name="c" value="darbinieki" />
  <input type="hidden" name="a" value="labot" />
  <input type="hidden" name="id" value="<?= $pardeveja_id ?>" />
  <input type="hidden" name="subtab" value="ligumi" />

  <input type="hidden" name="order" value="<?= getGet('order') ?>" />
  <input type="hidden" name="direction" value="<?= getGet('direction') ?>" />

  <?php if (isset($_GET['fligumu_id'])) { ?>
    <input name="fligumu_id" type="hidden" value="<?= getGet('fligumu_id') ?>" />
  <?php } ?>

  <input type="hidden" name="page" value="<?= $active_page ?>" />
  <input type="hidden" name="show_in_page" value="<?= $show_in_page ?>" />

  <input class="ui-state-default ui-corner-all" style="width: 5em; visibility: hidden; position: absolute;" type="submit" value="Atlasīt" />

  <?php print_paginator($total_rows, $active_page, $show_in_page) ?>

  <script>
  var searchlist = [];

  var sp = create_filter_field_array([
    's_summa', 's_pvn', 's_kopsumma', 's_apm', 's_kopsumma_bez_pvn',
    's_prov', 's_apm_prov', 's_prov_percent', 's_atl_vad'
  ]);
  </script>

  <table cellpadding="3" cellspacing="0" class="data" id="darbinieka_ligumi" style="width: auto;">

    <thead>

      <tr class="groups">
        <th colspan="2"></th>
        <th colspan="2" class="bgc-sta">Statuss</th>
        <th colspan="5" class="bgc-lig">Līgums</th>
        <th colspan="2" class="bgc-pak">Pakalpojums</th>
        <th colspan="4" class="bgc-rek">Norēķini</th>
        <th colspan="5" class="bgc-pro">Komisija</th>
        <th>Atg.</th>
      </tr>

      <tr class="header">
        <th class="header" colname="npk">Npk.</th>
        <? print_th('Nosaukums', 'nosauk', array('width' => 200)) ?>
        <? print_th('Līg. statuss', 'lig_statuss', array('width' => 50)) ?>
        <? print_th('Norēķinu statuss', 'kopapm_statuss', array('width' => 50)) ?>

        <? print_th('Līg. datums', 'lig_datums', array('width' => 50)) ?>
        <? print_th('Līg. nr.', 'lig_nr', array('width' => 60)) ?>
        <? print_th('Indekss', 'indekss', array('width' => 40)) ?>
        <? print_th('Saņemš. datums', 'san_datums', array('width' => 50)) ?>
        <? print_th('Saņemš. forma', 'san_forma', array('width' => 50)) ?>

        <? print_th('Līgums no', 'lig_no', array('width' => 50)) ?>
        <? print_th('Līgums līdz', 'lig_lidz', array('width' => 50)) ?>

        <? print_th('Summa<br />(bez PVN)', 'summa', array('width' => 50)) ?>
        <? print_th('PVN', 'pvn', array('width' => 50)) ?>
        <? print_th('Kopsumma<br />(ar PVN)', 'kopsumma', array('width' => 50)) ?>
        <? print_th('Apmaksāts&nbsp;EUR', 'apm_summa', array('width' => 50)) ?>
        <? print_th('Partnera komisija&nbsp;%', 'prov_proc', array('width' => 50)) ?>
        <? print_th('Vadītāja provīzija&nbsp;%', 'prov_proc', array('width' => 50)) ?>
        <? print_th('Komisija&nbsp;EUR<br />(bez PVN)', 'prov_bez_pvn', array('width' => 50)) ?>
        <? print_th('Izmaksātā komisija<br />EUR (bez PVN)', 'izm_prov_bez_pvn', array('width' => 50)) ?>
        <? print_th('Izmaksātā<br />komisija %', 'izm_prov_proc', array('width' => 50)) ?>
        <? print_th('Aktīvie<br />atgādinājumi', 'akt_atgad', array('width' => 40)) ?>
      </tr>

      <tr class="filter last">

        <th></th>
        <th><span><input class="sfield" name="fnosaukums" type="input" value="<?= getGet('fnosaukums') ?>" style="width:80%;"><a class="sfield_clear" href="#">x</a></span></th>

        <th>
          <select name="flig_statuss[]" class="sfield" multiple="multiple" size="1" style="width: 70px;">
            <option value=""></option>
            <?php foreach($_vars['liguma_statuss'] as $k=>$p){ ?>
              <option <? pt_print_selected($k, 'flig_statuss') ?> value="<?=$k;?>"><?=$p;?></option>
              <? if ($k == 6) { ?>
                <option <? pt_print_selected('6_termins', 'flig_statuss') ?> value="6_termins">&nbsp;&nbsp;&nbsp;<?= ucfirst($_vars['liguma_beigu_statuss']['termins']) ?></option>
                <option <? pt_print_selected('6_parslegts', 'flig_statuss') ?> value="6_parslegts">&nbsp;&nbsp;&nbsp;<?= ucfirst($_vars['liguma_beigu_statuss']['parslegts']) ?></option>
                <option <? pt_print_selected('6_atteikums', 'flig_statuss') ?> value="6_atteikums">&nbsp;&nbsp;&nbsp;<?= ucfirst($_vars['liguma_beigu_statuss']['atteikums']) ?></option>
                <option <? pt_print_selected('6_atteikums_p', 'flig_statuss') ?> value="6_atteikums_p">&nbsp;&nbsp;&nbsp;(Pārtraukts)</option>
              <? } ?>
            <?php } ?>
          </select>
          <a class="sfield_clear" href="#">x</a>
        </th>

        <th>
          <select name="fkopapm_statuss[]" class="sfield" multiple="multiple" size="1" style="width: 70px;">
            <option value=""></option>
            <?php foreach($_vars['kopapmaksas_statuss'] as $k=>$p){ ?>
              <option <? pt_print_selected($k, 'fkopapm_statuss') ?> value="<?=$k;?>"><?=$p;?></option>
              <? if ($k == 2) { ?>
                <option <? pt_print_selected('2_barteris', 'fkopapm_statuss') ?> value="2_barteris">&nbsp;&nbsp;&nbsp;Barteris</option>
              <? } ?>
            <?php } ?>
          </select>
          <a class="sfield_clear" href="#">x</a>
        </th>

        <th>
          <span><input class="sfield kalendari" name="flig_no" type="input" value="<?= getGet('flig_no') ?>" style="width:80%;"><a class="sfield_clear" href="#">x</a></span><br>
          <span><input class="sfield kalendari" name="flig_lidz" type="input" value="<?= getGet('flig_lidz') ?>" style="width:80%;"><a class="sfield_clear" href="#">x</a></span>
        </th>

        <th><span><input class="sfield" name="fliguma_nr" type="input" value="<?= getGet('fliguma_nr') ?>" style="width:80%;"><a class="sfield_clear" href="#">x</a></span></th>
        <th><span><input class="sfield" name="ffaktaddr_index" type="input" value="<?= getGet('ffaktaddr_index') ?>" style="width:80%;"><a class="sfield_clear" href="#">x</a></span></th>

        <th>
          <span><input class="sfield kalendari" name="flig_sanemts_no" type="input" value="<?= getGet('flig_sanemts_no') ?>" style="width:80%;"><a class="sfield_clear" href="#">x</a></span><br>
          <span><input class="sfield kalendari" name="flig_sanemts_lidz" type="input" value="<?= getGet('flig_sanemts_lidz') ?>" style="width:80%;"><a class="sfield_clear" href="#">x</a></span>
        </th>

        <th>
          <select name="fsanems_forma[]" class="sfield" multiple="multiple" size="1" style="width: 50px;">
            <option value=""></option>
            <?php foreach($_vars['sanemsanas_forma'] as $k=>$p){ ?>
              <option <? pt_print_selected($k, 'fsanems_forma') ?> value="<?=$k;?>"><?=$p;?></option>
            <?php } ?>
          </select>
          <a class="sfield_clear" href="#">x</a>
        </th>

        <th>
          <span><input class="sfield kalendari" name="frekl_sak_no" type="input" value="<?= getGet('frekl_sak_no') ?>" style="width:80%;"><a class="sfield_clear" href="#">x</a></span><br>
          <span><input class="sfield kalendari" name="frekl_sak_lidz" type="input" value="<?= getGet('frekl_sak_lidz') ?>" style="width:80%;"><a class="sfield_clear" href="#">x</a></span>
        </th>

        <th>
          <span><input class="sfield kalendari" name="frekl_beig_no" type="input" value="<?= getGet('frekl_beig_no') ?>" style="width:80%;"><a class="sfield_clear" href="#">x</a></span><br>
          <span><input class="sfield kalendari" name="frekl_beig_lidz" type="input" value="<?= getGet('frekl_beig_lidz') ?>" style="width:80%;"><a class="sfield_clear" href="#">x</a></span>
        </th>

        <th><span><input class="sfield" name="flig_summa" type="input" value="<?= getGet('flig_summa') ?>" style="width:80%;"><a class="sfield_clear" href="#">x</a></span></th>
        <th><span><input class="sfield" name="flig_pvn" type="input" value="<?= getGet('flig_pvn') ?>" style="width:80%;"><a class="sfield_clear" href="#">x</a></span></th>
        <th><span><input class="sfield" name="flig_kopsumma" type="input" value="<?= getGet('flig_kopsumma') ?>" style="width:80%;"><a class="sfield_clear" href="#">x</a></span></th>
        <th><span><input class="sfield" name="fapmaks_summa" type="input" value="<?= getGet('fapmaks_summa') ?>" style="width:80%;"><a class="sfield_clear" href="#">x</a></span></th>

        <th><span><input class="sfield" name="fprov_percent" type="input" value="<?= getGet('fprov_percent') ?>" style="width:80%;"><a class="sfield_clear" href="#">x</a></span></th>
        <th><span><input class="sfield" name="fatl_vad" type="input" value="<?= getGet('fatl_vad') ?>" style="width:80%;"><a class="sfield_clear" href="#">x</a></span></th>
        <th><span><input class="sfield" name="fprov_summa" type="input" value="<?= getGet('fprov_summa') ?>" style="width:80%;"><a class="sfield_clear" href="#">x</a></span></th>
        <th><span><input class="sfield" name="fapm_prov_summa" type="input" value="<?= getGet('fapm_prov_summa') ?>" style="width:80%;"><a class="sfield_clear" href="#">x</a></span></th>
        <th><span><input class="sfield" name="fapm_prov_percent" type="input" value="<?= getGet('fapm_prov_percent') ?>" style="width:80%;"><a class="sfield_clear" href="#">x</a></span></th>

        <th>
          <select name="fakt_atgad[]" class="sfield" multiple="multiple" size="1" style="width: 40px;">
            <option value=""></option>
            <option <? pt_print_selected(1, 'fakt_atgad') ?> value="1">Ir</option>
            <option <? pt_print_selected(2, 'fakt_atgad') ?> value="2">Nav</option>
          </select>
          <a class="sfield_clear" href="#">x</a>
        </th>

      </tr>

    </thead>

    <tbody class="main">

      <?php
      $npk = $total_rows - (($active_page - 1) * $show_in_page);
      $inid = -1;

      foreach($visibledata as $row) {

        $inid ++;

        ?>

        <tr id="lig_tr<?=$inid;?>" class="lig_row" title="<?=$row['nosaukums'] ?>">
          <td class="c s_id"><?=$npk;?></td>
          <td>
            <a href="?c=ligumi&a=labot&id=<?=$row['id'];?>"><b><?=$row['nosaukums'] ?></b></a>

            <? if (!empty($row['saistito_pardeveju_skaits'])) { ?>

              <?php
              $lig_saist_pard = array();
              if (!empty($saist_pard[$row['id']])) {
                foreach($saist_pard[$row['id']] as $v) {
                  $lig_saist_pard[] = $v['vards'];
                }
              }
              ?>

              <span class="saist_pard_ico" title="Saistītie pārdevēji: <?php echo implode(', ', $lig_saist_pard) ?>">s</span>

            <? } ?>

          </td>

          <? if ($row['liguma_statuss'] == 6) { ?>
            <? $active = (empty($row['beigu_statuss']) || ($row['beigu_statuss'] == 'termins' && $row['beigu_statuss_atlikt_lidz'] <= date('Y-m-d'))) ? true : false; ?>
            <td class="c">
              <div class="lig_statuss_6 <?= !empty($row['beigu_statuss']) ? 'lig_beigu_statuss_' . $row['beigu_statuss'] : 'lig_beigu_statuss_termins' ?>">
                <a class="edit_link" href="#" onclick="showLigumaBeiguStatussDialog(<?= $row['id'] ?>); return false;"><img src="css/edit.png" alt="Labot statusu" title="Labot statusu" /></a>
                <span class="<?= $active ? 'lig_beigu_statuss_active' : '' ?>">
                  <span class="stat_name"><?= $_vars['liguma_statuss'][6] ?></span>
                  (<?= !empty($row['beigu_statuss']) ? $_vars['liguma_beigu_statuss'][$row['beigu_statuss']] : $_vars['liguma_beigu_statuss']['termins'] ?><?= !empty($row['beigu_statuss_auto_atteikums']) ? ', <span title="Līgums pirms reklāmas beigām tika pārtraukts">P</span>': '' ?>)
                </span>
              </div>
            </td>
          <? } else { ?>
            <td class="c"><?= !empty($_vars['liguma_statuss'][$row['liguma_statuss']]) ? '<div class="'.( !empty($_vars['liguma_statuss'][$row['liguma_statuss']]) ? 'lig_statuss_'.$row['liguma_statuss'] : '' ).'">' . $_vars['liguma_statuss'][$row['liguma_statuss']] . '</div>' : ''?></td>
          <? } ?>

          <? if ($row['norekinu_statuss'] == 2 && is_barteris(!empty($lig_rekini[$row['id']]) ? $lig_rekini[$row['id']] : null)) { ?>
            <td class="c">
              <div class="norek_statuss_2">
                <a href="?c=ligumi&a=labot&id=<?=$row['id'];?>&subtab=2">Barteris</a>
              </div>
            </td>
          <? } else { ?>
            <td class="c">
              <div class="<?= !empty($_vars['kopapmaksas_statuss'][$row['norekinu_statuss']]) ? 'norek_statuss_'.$row['norekinu_statuss'] : "" ?>">
                <?= !empty($_vars['kopapmaksas_statuss'][$row['norekinu_statuss']]) ? '<a href="?c=ligumi&a=labot&id='.$row['id'].'&subtab=2" class="norek_statuss_'.$row['norekinu_statuss'].'">' . $_vars['kopapmaksas_statuss'][$row['norekinu_statuss']] . '</a>' : "" ?>

                <? if ($row['norekinu_statuss'] == 4) { ?>
                  <span style="font-size: 0.75em;">(<?= $row['max_nokavetas_dienas'] ?>)</span>
                <? } ?>
              </div>
            </td>
          <? } ?>

          <td class="c"><strong><?= !empty($row['ligumadat'])?date("d.m.Y",strtotime($row['ligumadat'])):"" ?></strong></td>
          <td class="c"><?= $row['ligumanr'] ?></td>
          <td class="c"><?= preg_replace('/^lv\-/i', '', $row['faktaddr_index']) ?></td>
          <td class="c"><em><?= !empty($row['sanemsanas_datums'])?date("d.m.Y",strtotime($row['sanemsanas_datums'])):"" ?></em></td>
          <td class="c"><?= !empty($row['sanemsanas_forma']) ? '<div class="'.( !empty($row['sanemsanas_forma']) ? 'sanform_statuss_'.$row['sanemsanas_forma'] : '' ).'">' . $_vars['sanemsanas_forma'][$row['sanemsanas_forma']] . '</div>' : '' ?></td>

          <td class="c"><strong><?= !empty($row['reklama_no'])?date("d.m.Y",strtotime($row['reklama_no'])):"" ?></strong></td>
          <td class="c"><strong><?= !empty($row['reklama_lidz'])?date("d.m.Y",strtotime($row['reklama_lidz'])):"" ?></strong></td>

          <td class="r"><strong><?= format_currency($row['rekinu_summa']) ?></strong></td>
          <td class="r"><?= format_currency($row['rekinu_kopsumma'] - $row['rekinu_summa']) ?></td>
          <td class="r"><?= format_currency($row['rekinu_kopsumma']) ?></td>
          <td class="r"><?= format_currency($row['apm_kopsumma']) ?></td>

          <td class="c prov_percent" style="white-space: nowrap;">
            <span style="display:none;"><?= round($row['provizija_pardevejam'], 2) ?></span>
            <input class="disablethistoo" style="text-align: center; width:2em;" type="text" name="lig_provizija[<?= $row['id'] ?>]" value="<?= round($row['provizija_pardevejam'], 2) ?>" />%
          </td>
          <td class="c atl_vad" style="white-space: nowrap;">
            <span style="display:none;"><?= round($row['atlidziba_vaditajam'], 2) ?></span>
            <input class="<?php echo check_access('partneri-vaditajs') ? 'disablethistoo canedit' : 'always_disabled';?>" style="text-align: center; width:2em;" type="text" name="atlidziba_vaditajam[<?= $row['id'] ?>]" value="<?= round($row['atlidziba_vaditajam'], 2) ?>" />%
          </td>
          <td class="r prov"><span>0</span></td>
          <td class="r prov_apm"><span><?= format_currency($row['apm_prov_summa']) ?></span></td>
          <td class="r prov_apm_percent"><span>0</span></td>

          <td class="c"><a href="?c=ligumi&a=labot&id=<?= $row['id'] ?>&subtab=3"><?= (!empty($row['aktivi_atgadinajumi'])) ? 'Ir ('.$row['aktivi_atgadinajumi'].')' : 'Nav' ?></a></td>

          <td style="display: none;">
            <script>
              searchlist[<?=$inid;?>] = '<?=$row['id'];?>';

              sp['s_summa'][<?=$inid;?>] = '<?=!empty($row['rekinu_summa']) ? $row['rekinu_summa'] : "" ?>';
              sp['s_pvn'][<?=$inid;?>] = '<?= round($row['rekinu_kopsumma'] - $row['rekinu_summa'], 2) ?>';
              sp['s_kopsumma'][<?=$inid;?>] = '<?=!empty($row['rekinu_kopsumma']) ? $row['rekinu_kopsumma'] : "" ?>';
              sp['s_apm'][<?=$inid;?>] = '<?=!empty($row['apm_kopsumma']) ? $row['apm_kopsumma'] : "" ?>';
              sp['s_prov'][<?=$inid;?>] = 0;
              sp['s_apm_prov'][<?=$inid;?>] = '<?= $row['apm_prov_summa'] ?>';

              // this is for calculations only
              sp['s_kopsumma_bez_pvn'][<?=$inid;?>] = '<?=!empty($row['rekinu_summa']) ? $row['rekinu_summa'] : "" ?>';

            </script>
          </td>
        </tr>

        <? $npk -- ?>

      <?php } ?>


    </tbody>

    <tr>

      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td align="right"><strong>Kopā:</strong></td>
      <td class="r" id="ligumusumma" style="font-weight: bold;">0</td>
      <td class="r" id="pvnsumma">0</td>
      <td class="r" id="ligumukopsumma">0</td>
      <td class="r" id="apmsumma">0</td>
      <td></td>
      <td></td>
      <td class="r" id="provsumma">0</td>
      <td class="r" id="apmprovsumma">0</td>
      <td></td>
      <td></td>

    </tr>

    <tr>

      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td align="right"><strong>Vidēji:</strong></td>
      <td class="r" id="ligumusummavid" style="font-weight: bold;">0</td>
      <td></td>
      <td class="r" id="ligumukopsummavid">0</td>
      <td></td>
      <td class="c" id="provpercentvid">0</td>
      <td class="c" id="atlvadvid">0</td>
      <td class="r" id="provsummavid">0</td>
      <td class="r" id="apmprovsummavid">0</td>
      <td class="r" id="apmprovpercentvid">0</td>
      <td></td>

    </tr>

  </table>

  <?php print_paginator($total_rows, $active_page, $show_in_page) ?>

</form>

<? if (!empty($hiddendata)) { ?>

  <?
  // we are adding contracts from other pages to sp and searchlist arrays as well
  // this data is for totals calculations
  ?>

  <script>

    <? foreach($hiddendata as $row) { ?>

      <? $inid ++ ?>

      searchlist[<?=$inid;?>] = '<?=$row['id'];?>';

      sp['s_prov_percent'][<?=$inid;?>] = <?= round($row['provizija_pardevejam'], 2) ?>;

      sp['s_summa'][<?=$inid;?>] = '<?=!empty($row['rekinu_summa']) ? $row['rekinu_summa'] : "" ?>';
      sp['s_pvn'][<?=$inid;?>] = '<?= round($row['rekinu_kopsumma'] - $row['rekinu_summa'], 2) ?>';
      sp['s_kopsumma'][<?=$inid;?>] = '<?=!empty($row['rekinu_kopsumma']) ? $row['rekinu_kopsumma'] : "" ?>';
      sp['s_apm'][<?=$inid;?>] = '<?=!empty($row['apm_kopsumma']) ? $row['apm_kopsumma'] : "" ?>';
      sp['s_prov'][<?=$inid;?>] = 0;
      sp['s_apm_prov'][<?=$inid;?>] = '<?= $row['apm_prov_summa'] ?>';

      // this is for calculations only
      sp['s_kopsumma_bez_pvn'][<?=$inid;?>] = '<?=!empty($row['rekinu_summa']) ? $row['rekinu_summa'] : "" ?>';

    <? } ?>

  </script>

<? } ?>

<form id="fullformplace" action="?c=partneri&a=labot&id=<?= $pardeveja_id ?>&subtab=ligumi" method="post" enctype="multipart/form-data" style="clear: both;">

  <? show_comments_block('darb_ligumi', $pardeveja_id) ?>

  <? show_files_block('darb_ligumi', $pardeveja_id) ?>

  <div class="hidethistoo non_admin_edit">
    <input type="submit" onClick="return saveAll(this, 'darbinieks_ligumi')" class="non_admin_edit submit show_in_act_panel" value="Saglabāt">
    <input type="button" onclick="return cancelSaves();" class="non_admin_edit show_in_act_panel" value="Atcelt">
  </div>

</form>

<script>

$(document).ready(function() {

  processTable();

  $('.prov_percent input').keyup(function() {
    processTable();
  });

  $('.saist_pard_ico').wTooltip({
    follow: true,
    offsetY: 0,
    offsetX: 10
  });

  initPlainSortableTable($('#darbinieka_ligumi_form'));

  initTableCols($('#darbinieka_ligumi'), 'darb_ligumi', <?= Zend_Json::encode(get_tab_kolonnas('darb_ligumi')) ?>);

  initTableGroupColors($('#darbinieka_ligumi'));

  $('#fullformplace').submit(function() {

    var form = $(this);


    //  Prov
    form.find('#prov_percent_inputs').remove();
    form.append('<div id="prov_percent_inputs"></div>')

    var inputs_wrap = form.find('#prov_percent_inputs');

    inputs_wrap.css({display: 'none'});

    $('.prov_percent input').each(function() {

      inputs_wrap.append($(this).clone());

    });

    //  Vadītāja atlīdzība
    form.find('#atl_vad_inputs').remove();
    form.append('<div id="atl_vad_inputs"></div>')

    var atl_vad_inputs = form.find('#atl_vad_inputs');

    atl_vad_inputs.css({display: 'none'});

    $('.atl_vad input').each(function() {

      atl_vad_inputs.append($(this).clone());

    });

  });

});

function processTable() {

  var ligumuskaits = 0;
  var ligumusumma = 0;
  var pvnsumma = 0;
  var ligumukopsumma = 0;
  var apmsumma = 0;
  var provsumma = 0;
  var apmprovsumma = 0;

  var vid_prov_percent = 0;
  var vid_atl_vad = 0;

  var is_visible;
  var atl_vad_is_visible;

  jQuery.each(searchlist, function(k,v){

    is_visible = true;
    atl_vad_is_visible = true;

    if (typeof(sp['s_prov_percent'][k]) != 'undefined') {
      is_visible = false;
    }

    if (typeof(sp['s_atl_vad'][k]) != 'undefined') {
      atl_vad_is_visible = false;
    }

    if (is_visible) {
      var row = $('#lig_tr' + k);
      var prov_percent = $(".prov_percent input", row).val();
    } else {
      var prov_percent = sp['s_prov_percent'][k];
    }

    if (atl_vad_is_visible) {
      var row = $('#lig_tr' + k);
      var atl_vad = $(".atl_vad input", row).val();
    } else {
      var atl_vad = sp['s_atl_vad'][k];
    }

    var prov = sp['s_kopsumma_bez_pvn'][k] * (prov_percent / 100);
    prov = prov.toFixed(2);

    sp['s_prov'][k] = prov;

    var prov_apm_percent = 0;
    if (prov > 0) {
      prov_apm_percent = sp['s_apm_prov'][k] * 100 / prov;
    }
    prov_apm_percent = Math.round(prov_apm_percent * 100) / 100;

    vid_prov_percent += parseFloat(prov_percent);
    vid_atl_vad += parseFloat(atl_vad);

    ligumuskaits ++;

    if (is_visible) {
      $(".prov_apm_percent span", row).html(prov_apm_percent + '%');
      $(".prov span", row).html(prov);
      $(".prov_percent span", row).html(prov_percent);
    }

    if (atl_vad_is_visible) {
      $(".atl_vad span", row).html(atl_vad);
    }

    if(sp['s_summa'][k] > 0) ligumusumma += parseFloat(sp['s_summa'][k].replace(',','.'));
    if(sp['s_pvn'][k] > 0) pvnsumma += parseFloat(sp['s_pvn'][k].replace(',','.'));
    if(sp['s_kopsumma'][k] > 0) ligumukopsumma += parseFloat(sp['s_kopsumma'][k].replace(',','.'));
    if(sp['s_apm'][k] > 0) apmsumma += parseFloat(sp['s_apm'][k].replace(',','.'));
    if(sp['s_prov'][k] > 0) provsumma += parseFloat(sp['s_prov'][k].replace(',','.'));
    if(sp['s_apm_prov'][k] > 0) apmprovsumma += parseFloat(sp['s_apm_prov'][k].replace(',','.'));

  });

  $('#ligumusumma').html('EUR&nbsp;' + parseFloat(ligumusumma).toFixed(2));
  $('#ligumusummavid').html('EUR&nbsp;' + parseFloat(ligumusumma / ligumuskaits).toFixed(2) );

  $('#pvnsumma').html('EUR&nbsp;' + parseFloat(pvnsumma).toFixed(2));

  $('#ligumukopsumma').html('EUR&nbsp;' + parseFloat(ligumukopsumma).toFixed(2));
  $('#ligumukopsummavid').html('EUR&nbsp;' + parseFloat(ligumukopsumma / ligumuskaits).toFixed(2));

  $('#apmsumma').html('EUR&nbsp;' + parseFloat(apmsumma).toFixed(2));

  $('#provpercentvid').html((Math.round(parseFloat(vid_prov_percent / ligumuskaits) * 100) / 100) + '%')
  $('#atlvadvid').html((Math.round(parseFloat(vid_atl_vad / ligumuskaits) * 100) / 100) + '%')

  $('#provsumma').html('EUR&nbsp;' + parseFloat(provsumma).toFixed(2));
  $('#provsummavid').html('EUR&nbsp;' + parseFloat(provsumma / ligumuskaits).toFixed(2));

  $('#apmprovsumma').html('EUR&nbsp;' + parseFloat(apmprovsumma).toFixed(2));
  $('#apmprovsummavid').html('EUR&nbsp;' + parseFloat(apmprovsumma / ligumuskaits).toFixed(2));

  $('#apmprovpercentvid').html((Math.round(parseFloat(((apmprovsumma / ligumuskaits) * 100) / (provsumma / ligumuskaits)) * 100) / 100) + '%');

}
</script>