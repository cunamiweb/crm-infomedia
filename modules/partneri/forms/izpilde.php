<?php
if(!check_access('darbinieki-plana-izpilde') && ($pardeveja_id != $_SESSION['user']['id'] || !check_access('darbinieki-sava-plana-izpilde'))) {
  die('Jums nav pieejas tiesību šai sadaļai.');
}

log_add("atvera", "Atvēra darbinieka " . $d['vards'] . " plāna izpildi");

$query = db_query("
  SELECT m.*
  FROM `".DB_PREF."pardeveji_min_izpilde` m
  WHERE m.`pardeveja_id` = ".$pardeveja_id."
  ORDER BY m.sakot_no ASC
");

$min_izpilde = array();

while($row = db_get_assoc($query)){
  $min_izpilde[] = htmlesc($row);
}

$sel_year = (!empty($_GET['izpilde_year']) && $_GET['izpilde_year'] <= date('Y')) ? $_GET['izpilde_year'] : date('Y');





?>

<? // Prototypes // ?>
<div style="display:none;">


  <table id="minizpildeprot">
    <tbody>
      <tr>
        <td>
          <select name="min_izpilde_new[sakot_no_y][]">
            <? for($year = 2011; $year <= date('Y') + 1; $year ++) { ?>
              <option value="<?= $year ?>"><?= $year ?></option>
            <? } ?>
          </select>
          -
          <select name="min_izpilde_new[sakot_no_m][]">
            <? for($month = 1; $month <= 12; $month ++) { ?>
              <? $month = str_pad($month, 2, '0', STR_PAD_LEFT) ?>
              <option value="<?= $month ?>"><?= mb_substr($_vars['menesi'][(int)$month], 0, 3) ?></option>
            <? } ?>
          </select>
        </td>
        <td width="90" align="center">
          <input type="text" name="min_izpilde_new[lig_skaits][]" style="text-align: center; width: 3em; " />
        </td>
        <td width="90" align="right">
          <input type="text" name="min_izpilde_new[lig_apgroz][]" style="text-align: right; width: 5em;" />
        </td>
        <td>
          <button class="hidethistoo ui-state-default ui-corner-all" onclick="$(this).closest('tr').remove();">Dzēst</button>
        </td>
      </tr>
    </tbody>
  </table>

</div>

<form action="" method="get" id="izpilde_filter">
  <input type="hidden" name="c" value="darbinieki" />
  <input type="hidden" name="a" value="labot" />
  <input type="hidden" name="subtab" value="izpilde" />
  <input type="hidden" name="id" value="<?= $pardeveja_id ?>" />
  <div>
    Gads:
    <span>
      <select id="izpilde_year" name="izpilde_year">
        <? for($year = 2011; $year <= date('Y'); $year ++) { ?>
          <option <?= ($year == $sel_year) ? 'selected="selected"' : '' ?> value="<?= $year ?>"><?= $year ?></option>
        <? } ?>
      </select>
    </span>
	</div>
</form>

<form action="?c=darbinieki<?= !empty($pardeveja_id) ? '&a=labot&id=' . $pardeveja_id . '&subtab=izpilde' : '&a=jauns' ;?>" id="fullformplace" method="post" enctype="multipart/form-data">

<?php
  /* Start: NEW */
?>
<div id="izpilde_container">
<table cellpadding="3" cellspacing="0" class="data" id="plana_izpilde" style="width: auto;">

    <?php print_plana_izpilde_head() ?>

    <tbody>

     <?php
      $total_p = array();
      $querters = range(1,4);
      foreach ($querters as $quarter) {

        //  Ceturkša dati
        //$qrow = get_izpildits($pardeveja_id, $sel_year, $quarter);
        $qrow = get_izpilde_historical($pardeveja_id, $sel_year, $quarter);

        //  Ceturkšņa dati bez piedziņas
        $qrow_p = get_izpildits($pardeveja_id, $sel_year, $quarter, null, true);
        $total_p[$sel_year.'-'.$quarter] = $qrow_p;

        print_plana_izpilde_row($sel_year.'-'.$quarter, $qrow, array(), $qrow_p);

        //  Mēneši
        $months = array(
          ($quarter * 3) - 2, //  Pirmais ceturkšņa mēnesis
          ($quarter * 3) - 1, // Otrais
          ($quarter * 3)  // Trešais
        );

        $options = array(
            'month_rows' => true,
            'allow_open_month' => false
        );

        //if(!is_new_premium_sys(array('year' => $sel_year, 'quarter' => $quarter))) {
          foreach($months as $month) {
            //$mrow = get_izpildits($pardeveja_id, $sel_year, null, $month);
            $mrow = get_izpilde_historical($pardeveja_id, $sel_year, null, $month);
            $mrow_p = get_izpildits($pardeveja_id, $sel_year, null, $month, true);
            $period = $sel_year . '-' . str_pad($month, 2, '0', STR_PAD_LEFT); // gads-menesis(ar diviem cipariem)

            print_plana_izpilde_row($period, $mrow, $options, $mrow_p);
          }
        //}

      }

      //  Par gadu
      //$total = get_izpildits($pardeveja_id, $sel_year);
      $total = get_izpilde_historical($pardeveja_id, $sel_year);
      print_plana_izpilde_row('total', $total, array(), $total_p);
      ?>

    </tbody>

</table>

  <?php
  /* End: NEW */

  if (is_admin()) { ?>

    <table id="min_izpilde_table">

      <tr>
        <th>Sākot no</th>
        <th>Min. līg. sk.</th>
        <th align="right">Min. apgr.</th>
        <th></th>
      </tr>

      <? foreach($min_izpilde as $v) { ?>

        <tr>
          <td>
            <select name="min_izpilde[<?= $v['id'] ?>][sakot_no_y]" class="limited_admin_edit">
              <? for($year = 2011; $year <= date('Y') + 1; $year ++) { ?>
                <option <?= ($year == date('Y', strtotime($v['sakot_no']))) ? 'selected="selected"' : '' ?> value="<?= $year ?>"><?= $year ?></option>
              <? } ?>
            </select>
            -
            <select name="min_izpilde[<?= $v['id'] ?>][sakot_no_m]" class="limited_admin_edit">
              <? for($month = 1; $month <= 12; $month ++) { ?>
                <? $month = str_pad($month, 2, '0', STR_PAD_LEFT) ?>
                <option <?= ($month == date('m', strtotime($v['sakot_no']))) ? 'selected="selected"' : '' ?> value="<?= $month ?>"><?= mb_substr($_vars['menesi'][(int)$month], 0, 3) ?></option>
              <? } ?>
            </select>
          </td>
          <td width="90" align="center">
            <input type="text" name="min_izpilde[<?= $v['id'] ?>][lig_skaits]" value="<?= $v['lig_skaits'] ?>" style="text-align: center; width: 3em; "  class="limited_admin_edit" />
          </td>
          <td width="80" align="right">
            <input type="text" name="min_izpilde[<?= $v['id'] ?>][lig_apgroz]" value="<?= $v['lig_apgroz'] ?>" style="text-align: right; width: 5em;"  class="limited_admin_edit" />
          </td>
          <td>
            <button class="hidethistoo ui-state-default ui-corner-all limited_admin_edit" onclick="$(this).closest('tr').remove();">Dzēst</button>
          </td>
        </tr>

      <? } ?>

      <tr>
        <td colspan="4" align="right">
          <button onClick="$(this).closest('tr').before($('#minizpildeprot tbody').html()); return false;" class="ui-state-default ui-corner-all hidethistoo limited_admin_edit">Pievienot jaunu</button>
        </td>
      </tr>

    </table>

  <?php } ?>


  </div>  <div class="clr"></div>

  <? show_comments_block('darb_izpilde', isset($_GET['id']) ? $_GET['id'] : null) ?>

  <? show_files_block('darb_izpilde', isset($_GET['id']) ? $_GET['id'] : null) ?>

  <div class="hidethistoo non_admin_edit">
    <input type="submit" onClick="return saveAll(this, 'darbinieks_izpilde')" class="non_admin_edit submit show_in_act_panel" value="Saglabāt">
    <input type="button" onclick="return cancelSaves();" class="non_admin_edit show_in_act_panel" value="Atcelt">
  </div>

  <input type="hidden" name="form_sent" value="1" />

  <script>
  initPlanaIzpildeTable();

  $("form#izpilde_filter").each(function() {

    var form = $(this);

    $('select#izpilde_year', form).change(function() {
      form.submit();
    });

  });
  </script>

</form>