<?
//  Izveidojam datumu range no piešķirtajām prēmijām (visām visos laikos), ko izmantot maksājumu atlases kverijos
//  TODO: ja šo sistēmas versiju paredzēts darbināt vēl 3+ gadus bez refactoringa,
//    tad izdomāt scale'ojamāku variantu, lai šitas datumu range nosacījums neizaug drausmīgi liels dažiem darbiniekiem
$result = db_query("
  SELECT *
  FROM `".DB_PREF."premijas` p
  WHERE
    p.pardevejs_id = " .  esc($_GET['id']) . " AND
    p.ir_premija = 1 AND
    p.premijas_apmers != 0
");

if(mysqli_num_rows($result) == 0) {
  echo "<p>Darbiniekam nav nevienas prēmijas.</p>";
  die();
}

$ligumi = array();
$quarters = array();

$yearly_stats = array();

function getIzpildeQuarter($year, $quarter, &$cache)
{
  if(!isset($cache[$year][$quarter])) {
    $cache[$year][$quarter] = get_izpildits($_GET['id'], $year, $quarter, null, true);
  }

  return $cache[$year][$quarter];
}

$quarters_ligumi = array();

/**
 * Atrod gadu un ceturksni pēc līguma id
 * @param array $row - rēķina rinda
 * return mixed false ja neatrod; [year, quarter] ja atrod
 */
function getYearQuarter($row)
{
  global $quarters_ligumi;
  foreach($quarters_ligumi as $ligumi_year => $ligumi_quarters) {
    foreach($ligumi_quarters as $ligumi_quarter => $l_ids) {
      if(in_array($row['liguma_id'], $l_ids)) {
        return array($ligumi_year, $ligumi_quarter);
      }
    }
  }
}

while($row = db_get_assoc($result)) {
  if(is_quarter_fixed($row['gads'], $row['ceturksnis'])) {
    //  No gada un ceturkšņa numura jāuztaisa datumu range.
    //  Ceturkšņa pirmā mēneša 1. datums līdz ceturkšņa pēdējā mēneša pēdējais datums (31?)
    $quarters[] = array('year' => $row['gads'], 'quarter' => $row['ceturksnis']);

    //$first_m = str_pad((3 * $row['ceturksnis'] - 2), 2, '0', STR_PAD_LEFT);
    //$last_m = str_pad((3 * $row['ceturksnis']), 2, '0', STR_PAD_LEFT);
    $data = getIzpildeQuarter($row['gads'], $row['ceturksnis'], $yearly_stats);
    $ligumi = array_merge($ligumi, $data['kopa']['ligumu_id']);

    if(!isset($quarters_ligumi[$row['gads']][$row['ceturksnis']])) {
      $quarters_ligumi[$row['gads']][$row['ceturksnis']] = array();
    }
    $quarters_ligumi[$row['gads']][$row['ceturksnis']] = array_merge($quarters_ligumi[$row['gads']][$row['ceturksnis']], $data['kopa']['ligumu_id']);
    //$date_queries[] = 'r.apm_termins >= "' . $row['gads'] . '-' . $first_m . '-01" AND r.apm_termins <= "' . $row['gads'] . '-' . $last_m . '-31"';
    //$date_queries[] = 'EXTRACT(YEAR_MONTH FROM r.apm_termins) BETWEEN "' . $row['gads'].$first_m . '" AND "' . $row['gads'].$last_m . '"';
  }
}

$result = db_query("
  SELECT
  r.*,
  g.id as liguma_id,
  g.nosaukums as klienta_nosaukums,
  r.pvn as rekina_pvn,
  r.izr_datums as rekina_izr_datums,
  r.kopsumma as rekina_kopsumma,
  r.apm_termins as rekina_apm_termins,
  (
      SELECT SUM(no_summas)
      FROM `".DB_PREF."pardeveji_rekini_premijas_detalas`
      WHERE rekina_id = r.id
  ) as premiju_izmaksu_summa,
  p.pvn_likme,
  MAX(m.datums) as apm_datums
  FROM `".DB_PREF."rekini` r
  LEFT JOIN `".DB_PREF."ligumi` g ON (r.liguma_id = g.id)
  LEFT JOIN `".DB_PREF."pardeveji_rekini_premijas_detalas` d ON (r.id = d.rekina_id)
  LEFT JOIN `".DB_PREF."pardeveji` p ON (p.id = g.pardeveja_id)
  LEFT JOIN `".DB_PREF."rekini_maksajumi` m ON (m.rekina_id = r.id)
  WHERE
    g.pardeveja_id = ".esc($_GET['id'])." AND
    d.id IS NULL AND
    r.atkapsanas_rekins = 0 AND
    r.nodots_piedz = 0 AND
    -- r.barteris = 0 AND
    r.anulets = 0 AND
    r.liguma_id IN (" . (!empty($ligumi) ? implode(', ', $ligumi) : '0') . ")
  GROUP BY r.id
  HAVING
    r.apm_summa >= IFNULL(r.kopsumma, 0) AND
    IFNULL(r.kopsumma, 0) - IFNULL(premiju_izmaksu_summa, 0) > 0
  ORDER BY apm_datums DESC, r.id DESC
");

/*$result = db_query("
  SELECT
    m.*,
    g.id as liguma_id,
    g.nosaukums as klienta_nosaukums,
    r.pvn as rekina_pvn,
    r.rek_nr,
    r.izr_datums as rekina_izr_datums,
    r.kopsumma as rekina_kopsumma,
    r.apm_termins as rekina_apm_termins,
    (
      SELECT SUM(no_summas)
      FROM `".DB_PREF."pardeveji_rekini_premijas_detalas`
      WHERE rekina_id = m.rekina_id
    ) as premiju_izmaksu_summa,
    p.pvn_likme
  FROM `".DB_PREF."rekini_maksajumi` m
  LEFT JOIN `".DB_PREF."rekini` r ON (m.rekina_id = r.id)
  LEFT JOIN `".DB_PREF."ligumi` g ON (r.liguma_id = g.id)
  LEFT JOIN `".DB_PREF."pardeveji_rekini_premijas_detalas` d ON (m.id = d.rekina_maksajuma_id)
  LEFT JOIN `".DB_PREF."pardeveji` p ON (p.id = g.pardeveja_id)
  WHERE
    g.pardeveja_id = ".esc($_GET['id'])." AND
    d.id IS NULL AND
    r.atkapsanas_rekins = 0 AND
    (
      r.nodots_piedz_datums IS NULL OR
      r.nodots_piedz_datums > m.datums
    ) AND
    (" . implode(' OR ', $date_queries) . ")
  HAVING
    m.summa <= IFNULL(r.kopsumma, 0) - IFNULL(premiju_izmaksu_summa, 0)
  ORDER BY m.datums DESC, m.id DESC
");
*/
$apmaksas = array();
while($row = db_get_assoc($result)) {
  $apmaksas[$row['id']] = $row;
}
?>

<h4>Saņemtās prēmijas</h4>

<script>
var searchlist_uzizm = [];

var sp_uzizm = create_filter_field_array([
  's_klienta_nosaukums', 's_rek_nr', 's_rek_izr_datums', 's_summa', 's_datums',
  's_prem_percent', 's_prem_summa', 's_prem_pvn', 's_prem_kopsumma', 's_ceturksnis'
]);
</script>

<table style="display:none;">

  <tr id="uz_izmaksu_filter">

    <th><span><input class="search_field" meth="standart" searchclass="s_klienta_nosaukums" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <th><span><input class="search_field" meth="standart" searchclass="s_rek_nr" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>

    <th>
      <span><input class="search_field kalendari" meth="from" searchclass="s_rek_izr_datums" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span><br>
      <span><input class="search_field kalendari" meth="to" searchclass="s_rek_izr_datums" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span>
    </th>

    <th><span><input class="search_field" meth="coin" searchclass="s_summa" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>

    <th>
      <span><input class="search_field kalendari" meth="from" searchclass="s_datums" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span><br>
      <span><input class="search_field kalendari" meth="to" searchclass="s_datums" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span>
    </th>

    <th><span><input class="search_field" meth="coin" searchclass="s_prem_percent" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <th><span><input class="search_field" meth="coin" searchclass="s_prem_summa" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <th><span><input class="search_field" meth="coin" searchclass="s_prem_pvn" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <th><span><input class="search_field" meth="coin" searchclass="s_prem_kopsumma" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <th>
      <select class="search_field advonchange" searchclass="s_ceturksnis" meth="standart">
        <option value=""></option>
        <?php foreach($quarters as $quarter){ ?>
          <option value="<?=$quarter['year']. '-' .$quarter['quarter'];?>"><?=$quarter['year'] . ' ' . $_vars['quarters_roman'][$quarter['quarter']];?></option>
        <?php } ?>
      </select>
      <a class="search_field" href="#">x</a>
    </th>
  </tr>

</table>

<table id="uz_izmaksu" class="data" style="width: 970px;">

  <thead>

    <tr class="last header">
      <th>Klients</th>
      <th>Rēķina nr.</th>
      <th>Rēķins izstādīts</th>
      <th>Saņemtā summa EUR</th>
      <th>Apmaksas datums</th>
      <th>Premija %</th>
      <th>Bez PVN EUR</th>
      <th>PVN EUR</th>
      <th>Kopā EUR</th>
      <th>Ceturksnis</th>
    </tr>

  </thead>

  <tbody class="main">

    <? $inid = -1;
    $contract_premiums = array();
    ?>

    <? foreach($apmaksas as $apmaksa) { ?>

      <? $inid ++;  ?>

      <?
      //  Iegūst līguma prēmiju. Ātrdarbībai glabājam masīvā katram ceturksnim un pirms rēķināšanas paskatamies vai jau nav aprēķināta.
      list($year, $quarter) = getYearQuarter($apmaksa);
      if(!isset($contract_premiums[$year][$quarter])) {
        $contract_premiums[$year][$quarter] = get_contract_premium($year, $quarter, (int)$_GET['id']);
      }
      $contract_premium = $contract_premiums[$year][$quarter];
      //$contract_premium = 4.16;

      //$apm_summa_bez_pvn = bcdiv($apmaksa['kopsumma'], (1 + ($apmaksa['rekina_pvn'] / 100)), 2);
      $apm_summa_bez_pvn  = $apmaksa['summa'];
      $prem_summa = round($apm_summa_bez_pvn * ($contract_premium / 100), 2);

      $prem_pvn = round($prem_summa * ($apmaksa['pvn_likme'] / 100), 2);
      ?>

      <tr id="uzizm_tr<?=$inid;?>" class="tr_uzizm">
        <td><a href="?c=ligumi&a=labot&id=&a=labot&id=<?= $apmaksa['liguma_id'] ?>&subtab=2"><?= $apmaksa['klienta_nosaukums'] ?></a></td>
        <td><?= $apmaksa['rek_nr'] ?></td>
        <td class="c"><?= date('d.m.Y', strtotime($apmaksa['rekina_izr_datums'])) ?></td>
        <td class="r"><?= $apmaksa['kopsumma'] ?></td>
        <td class="c"><?= date('d.m.Y', strtotime($apmaksa['apm_datums'])); ?></td>
        <td class="c"><?= number_format($contract_premium, 2) ?>%</td>
        <td class="r"><?= format_currency($prem_summa) ?></td>
        <td class="r"><?= format_currency($prem_pvn) ?></td>
        <td class="r"><strong><?= format_currency($prem_summa + $prem_pvn) ?></strong></td>
        <td class="c last"><?=$year . '.g ' . $_vars['quarters_roman'][$quarter];?></td>
        <td style="display: none;">
          <script>

            searchlist_uzizm[<?=$inid;?>] = '<?=addslashes(mb_strtoupper($apmaksa['rek_nr']));?>';

            sp_uzizm['s_klienta_nosaukums'][<?=$inid;?>] = '<?=addslashes(mb_strtoupper($apmaksa['klienta_nosaukums']));?>';
            sp_uzizm['s_rek_nr'][<?=$inid;?>] = '<?=addslashes(mb_strtoupper($apmaksa['rek_nr']));?>';
            sp_uzizm['s_rek_izr_datums'][<?=$inid;?>] = <?= !empty($apmaksa['rekina_izr_datums']) ? date('Ymd', strtotime($apmaksa['rekina_izr_datums'])) : 0 ?>;
            sp_uzizm['s_summa'][<?=$inid;?>] = '<?=!empty($apmaksa['kopsumma']) ? $apmaksa['kopsumma'] : "" ?>';
            sp_uzizm['s_datums'][<?=$inid;?>] = <?= !empty($apmaksa['apm_datums']) ? date('Ymd', strtotime($apmaksa['apm_datums'])) : 0 ?>;
            sp_uzizm['s_prem_percent'][<?=$inid;?>] = '<?=!empty($contract_premium) ? $contract_premium : "" ?>';
            sp_uzizm['s_prem_summa'][<?=$inid;?>] = '<?=!empty($prem_summa) ? $prem_summa : "" ?>';
            sp_uzizm['s_prem_pvn'][<?=$inid;?>] = '<?=!empty($prem_pvn) ? $prem_pvn : "" ?>';
            sp_uzizm['s_prem_kopsumma'][<?=$inid;?>] = '<?=$prem_summa + $prem_pvn ?>';
            sp_uzizm['s_ceturksnis'][<?=$inid;?>] = '<?=$year. '-' .$quarter;?>';
          </script>
        </td>
      </tr>

    <? } ?>

  </tbody>

  <tr>

    <td align="right" colspan="3"><strong>Kopā:</strong></td>
    <td class="r" id="uzizm_sanemta_summa">0</td>
    <td></td>
    <td></td>
    <td class="r" id="uzizm_prem_summa">0</td>
    <td class="r" id="uzizm_prem_pvn">0</td>
    <td class="r" id="uzizm_prem_kopsumma" style="font-weight: bold;">0</td>
    <td></td>
  </tr>

</table>

<script>
$(document).ready(function() {

  var table = $("#uz_izmaksu");
  var head = $("#uz_izmaksu_filter");

  table.tablesorter({
    debug: false,
    highlightClass: 'highlight',
    headers: {
      0: {sorter: 'latviantext'},
      2: {sorter: 'latviandate'},
      4: {sorter: 'latviandate'}
    }
  }).bind('sortStart', function() {
    table.trigger("update");
  }).bind('sortEnd', function() {
    stripeTable(table);
  });

  initTableFilter(table, head, processUzIzmTable, 'uzizm', searchlist_uzizm, sp_uzizm);

});

function processUzIzmTable() {

  var uzizm_sanemta_summa = 0;
  var uzizm_prem_summa = 0;
  var uzizm_prem_pvn = 0;
  var uzizm_prem_kopsumma = 0;

  jQuery.each(searchlist_uzizm, function(k,v){

    var row = $('#uzizm_tr' + k);

    if (row.is(':visible')) {

      if (sp_uzizm['s_summa'][k] > 0) uzizm_sanemta_summa += parseFloat(sp_uzizm['s_summa'][k]);
      if (sp_uzizm['s_prem_summa'][k] > 0) uzizm_prem_summa += parseFloat(sp_uzizm['s_prem_summa'][k]);
      if (sp_uzizm['s_prem_pvn'][k] > 0) uzizm_prem_pvn += parseFloat(sp_uzizm['s_prem_pvn'][k]);
      if (sp_uzizm['s_prem_kopsumma'][k] > 0) uzizm_prem_kopsumma += parseFloat(sp_uzizm['s_prem_kopsumma'][k]);

    }

  });

  $('#uzizm_sanemta_summa').html(uzizm_sanemta_summa.toFixed(2));
  $('#uzizm_prem_summa').html(uzizm_prem_summa.toFixed(2));
  $('#uzizm_prem_pvn').html(uzizm_prem_pvn.toFixed(2));
  $('#uzizm_prem_kopsumma').html(uzizm_prem_kopsumma.toFixed(2));

}
</script>

<script>
var searchlist_nakmaks = [];

var sp_nakmaks = create_filter_field_array([
  's_klienta_nosaukums', 's_rek_nr', 's_rek_izr_datums', 's_apm_termins',
  's_rekina_statuss', 's_summa', 's_neapm_summa', 's_prem_percent', 's_prem_summa',
  's_prem_pvn', 's_prem_kopsumma', 's_ceturksnis'
]);
</script>

<?
/*
$result = db_query("
  SELECT
    r.*,
    g.nosaukums as klienta_nosaukums,
    r.pvn as rekina_pvn,
    r.apm_termins as rekina_apm_termins,
    (IFNULL(r.kopsumma, 0) - SUM(IFNULL(m.summa, 0))) as neapm_summa,
    (
      SELECT SUM(no_summas)
      FROM `".DB_PREF."pardeveji_rekini_premijas_detalas`
      WHERE rekina_id = r.id
    ) as prem_izmaksu_summa,
    p.pvn_likme
  FROM `".DB_PREF."rekini` r
  LEFT JOIN `".DB_PREF."ligumi` g ON (r.liguma_id = g.id)
  LEFT JOIN `".DB_PREF."rekini_maksajumi` m ON (m.rekina_id = r.id)
  LEFT JOIN `".DB_PREF."pardeveji` p ON (p.id = g.pardeveja_id)
  WHERE
    g.pardeveja_id = ".esc($_GET['id'])." AND
    r.anulets = 0 AND
    r.nodots_piedz_datums IS NULL AND
    r.atkapsanas_rekins = 0 AND
    (" . implode(' OR ', $date_queries) . ")
  GROUP BY r.id
  HAVING
    neapm_summa > 0 AND
    neapm_summa <= IFNULL(r.kopsumma, 0) - IFNULL(prem_izmaksu_summa, 0)
");   */

$result = db_query("
  SELECT
  r.*,
  g.id as liguma_id,
  g.nosaukums as klienta_nosaukums,
  r.pvn as rekina_pvn,
  r.izr_datums as rekina_izr_datums,
  r.kopsumma as rekina_kopsumma,
  r.apm_termins as rekina_apm_termins,
  (
      SELECT SUM(no_summas)
      FROM `".DB_PREF."pardeveji_rekini_premijas_detalas`
      WHERE rekina_id = r.id
  ) as premiju_izmaksu_summa,
  (
    SELECT SUM(summa)
    FROM `".DB_PREF."rekini_maksajumi`
    WHERE rekina_id = r.id
  ) as maksajumu_summa,
  p.pvn_likme
  FROM `".DB_PREF."rekini` r
  LEFT JOIN `".DB_PREF."ligumi` g ON (r.liguma_id = g.id)
  LEFT JOIN `".DB_PREF."pardeveji_rekini_premijas_detalas` d ON (r.id = d.rekina_id)
  LEFT JOIN `".DB_PREF."pardeveji` p ON (p.id = g.pardeveja_id)
  WHERE
    g.pardeveja_id = ".esc($_GET['id'])." AND
    d.id IS NULL AND
    r.atkapsanas_rekins = 0 AND
    r.nodots_piedz = 0 AND
    -- r.barteris = 0 AND
    r.anulets = 0 AND
    r.liguma_id IN (" . (!empty($ligumi) ? implode(', ', $ligumi) : '0') . ")
  HAVING
    IFNULL(r.apm_summa, 0) < r.kopsumma AND
    IFNULL(r.kopsumma, 0) - IFNULL(premiju_izmaksu_summa, 0) > 0
  ORDER BY r.apm_termins ASC, r.id DESC
");

$avansa_rekini = array();
while($row = db_get_assoc($result)) {
  $avansa_rekini[$row['id']] = $row;
}
?>

<h4>Avansa prēmijas</h4>

<table style="display:none;">

  <tr id="nakotnes_maksajumi_filter">

    <th><span><input class="search_field" meth="standart" searchclass="s_klienta_nosaukums" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <th><span><input class="search_field" meth="standart" searchclass="s_rek_nr" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>

    <th>
      <span><input class="search_field kalendari" meth="from" searchclass="s_rek_izr_datums" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span><br>
      <span><input class="search_field kalendari" meth="to" searchclass="s_rek_izr_datums" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span>
    </th>

    <th>
      <span><input class="search_field kalendari" meth="from" searchclass="s_apm_termins" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span><br>
      <span><input class="search_field kalendari" meth="to" searchclass="s_apm_termins" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span>
    </th>

  <th>
    <select class="search_field advonchange" searchclass="s_rekina_statuss" meth="standart" style="width: 70px;">
      <option value=""></option>
      <?php foreach($_vars['apmaksas_statuss'] as $k=>$p){ ?>
        <?php if ($k == 1) continue; // do not show "Nav sācies" ?>
        <option value="<?=$k;?>"><?=$p;?></option>
      <?php } ?>
    </select>
  </th>

    <th><span><input class="search_field" meth="coin" searchclass="s_summa" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <th><span><input class="search_field" meth="coin" searchclass="s_neapm_summa" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <th><span><input class="search_field" meth="coin" searchclass="s_prem_percent" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <th><span><input class="search_field" meth="coin" searchclass="s_prem_summa" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <th><span><input class="search_field" meth="coin" searchclass="s_prem_pvn" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <th><span><input class="search_field" meth="coin" searchclass="s_prem_kopsumma" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <th>
      <select class="search_field advonchange" searchclass="s_ceturksnis" meth="standart">
        <option value=""></option>
        <?php foreach($quarters as $quarter){ ?>
          <option value="<?=$quarter['year']. '-' .$quarter['quarter'];?>"><?=$quarter['year'] . ' ' . $_vars['quarters_roman'][$quarter['quarter']];?></option>
        <?php } ?>
      </select>
      <a class="search_field" href="#">x</a>
    </th>
  </tr>

</table>

<table id="nakotnes_maksajumi" class="data" style="width: 970px;">

  <thead>

    <tr class="last header">
      <th>Klients</th>
      <th>Rēķina nr.</th>
      <th>Rēķins izstādīts</th>
      <th>Apmaksas termiņš</th>
      <th>Apmaksas statuss</th>
      <th>Rēķina summa EUR (ar PVN)</th>
      <th>Rēķina neapm. summa EUR</th>
      <th>Prēmija %</th>
      <th>Bez PVN EUR</th>
      <th>PVN EUR</th>
      <th>Kopā EUR</th>
      <th>Ceturksnis</th>
    </tr>

  </thead>

  <tbody class="main">

    <? $inid = -1;

    ?>

    <? foreach($avansa_rekini as $avansa) { ?>

      <? $inid ++;  ?>

      <?
      $avansa['neapm_summa'] = $avansa['kopsumma'] - $avansa['maksajumu_summa'];

      //  Iegūst līguma prēmiju. Ātrdarbībai glabājam masīvā katram ceturksnim un pirms rēķināšanas paskatamies vai jau nav aprēķināta.
      list($year, $quarter) = getYearQuarter($avansa);
      if(!isset($contract_premiums[$year][$quarter])) {
        $contract_premiums[$year][$quarter] = get_contract_premium($year, $quarter, (int)$_GET['id']);
      }
      $contract_premium = $contract_premiums[$year][$quarter];
      //$contract_premium = 4.16;

      //$apm_summa_bez_pvn = bcdiv($avansa['kopsumma'], (1 + ($avansa['rekina_pvn'] / 100)), 2);
      $apm_summa_bez_pvn  = $avansa['summa'];
      $prem_summa = round($apm_summa_bez_pvn * ($contract_premium / 100), 2);

      $prem_pvn = round($prem_summa * ($avansa['pvn_likme'] / 100), 2);
      ?>

      <tr id="nakmaks_tr<?=$inid;?>" class="tr_nakmaks">
        <td><a href="?c=ligumi&a=labot&id=&a=labot&id=<?= $avansa['liguma_id'] ?>&subtab=2"><?= $avansa['klienta_nosaukums'] ?></a></td>
        <td><?= $avansa['rek_nr'] ?></td>
        <td class="c"><?= !empty($avansa['izr_datums']) ? date('d.m.Y', strtotime($avansa['izr_datums'])) : '' ?></td>
        <td class="c"><strong><?= !empty($avansa['apm_termins']) ? date('d.m.Y', strtotime($avansa['apm_termins'])) : '' ?></strong></td>

        <? $status = get_rekina_statuss($avansa); ?>
        <td class="c">
          <div class="<?= !empty($status) ? 'apm_statuss_'.$status : ''; ?>">
            <?= !empty($status) ? '<span>' . $_vars['apmaksas_statuss'][$status] . '</span>' : ''; ?>
          </div>
        </td>

        <td class="r"><?= $avansa['kopsumma'] ?></td>
        <td class="r"><?= $avansa['neapm_summa'] ?></td>
        <td class="c"><?= number_format($contract_premium, 2) ?>%</td>
        <td class="r"><?= format_currency($prem_summa) ?></td>
        <td class="r"><?= format_currency($prem_pvn) ?></td>
        <td class="r"><strong><?= format_currency($prem_summa + $prem_pvn) ?></strong></td>
        <td class="c last"><?=$year . '.g ' . $_vars['quarters_roman'][$quarter];?></td>
        <td style="display: none;">
          <script>

            searchlist_nakmaks[<?=$inid;?>] = '<?=addslashes(mb_strtoupper($avansa['rek_nr']));?>';

            sp_nakmaks['s_klienta_nosaukums'][<?=$inid;?>] = '<?=addslashes(mb_strtoupper($avansa['klienta_nosaukums']));?>';
            sp_nakmaks['s_rek_nr'][<?=$inid;?>] = '<?=addslashes(mb_strtoupper($avansa['rek_nr']));?>';
            sp_nakmaks['s_rek_izr_datums'][<?=$inid;?>] = <?= !empty($avansa['izr_datums']) ? date('Ymd', strtotime($avansa['izr_datums'])) : 0 ?>;
            sp_nakmaks['s_apm_termins'][<?=$inid;?>] = <?= !empty($avansa['apm_termins']) ? date('Ymd', strtotime($avansa['apm_termins'])) : 0 ?>;
            sp_nakmaks['s_rekina_statuss'][<?=$inid;?>] = '<?=$status?>';
            sp_nakmaks['s_summa'][<?=$inid;?>] = '<?=!empty($avansa['kopsumma']) ? $avansa['kopsumma'] : "" ?>';
            sp_nakmaks['s_neapm_summa'][<?=$inid;?>] = '<?=!empty($avansa['neapm_summa']) ? $avansa['neapm_summa'] : "" ?>';
            sp_nakmaks['s_prem_percent'][<?=$inid;?>] = '<?=!empty($contract_premium) ? $contract_premium : "" ?>';
            sp_nakmaks['s_prem_summa'][<?=$inid;?>] = '<?=!empty($prem_summa) ? $prem_summa : "" ?>';
            sp_nakmaks['s_prem_pvn'][<?=$inid;?>] = '<?=!empty($prem_pvn) ? $prem_pvn : "" ?>';
            sp_nakmaks['s_prem_kopsumma'][<?=$inid;?>] = '<?=$prem_summa + $prem_pvn ?>';
            sp_nakmaks['s_ceturksnis'][<?=$inid;?>] = '<?=$year. '-' .$quarter;?>';
          </script>
        </td>
      </tr>

    <? } ?>

  </tbody>

  <tr>

    <td align="right" colspan="5"><strong>Kopā:</strong></td>
    <td class="r" id="nakmaks_rekinu_summa">0</td>
    <td class="r" id="nakmaks_neapm_summa">0</td>
    <td></td>
    <td class="r" id="nakmaks_prem_summa">0</td>
    <td class="r" id="nakmaks_prem_pvn">0</td>
    <td class="r" id="nakmaks_prem_kopsumma" style="font-weight: bold;">0</td>
    <td></td>
  </tr>

</table>

<script>
$(document).ready(function() {

  var table = $("#nakotnes_maksajumi");
  var head = $("#nakotnes_maksajumi_filter");

  table.tablesorter({
    debug: false,
    highlightClass: 'highlight',
    headers: {
      0: {sorter: 'latviantext'},
      2: {sorter: 'latviandate'},
      3: {sorter: 'latviandate'}
    }
  }).bind('sortStart', function() {
    table.trigger("update");
  }).bind('sortEnd', function() {
    stripeTable(table);
  });

  initTableFilter(table, head, processNakMaksTable, 'nakmaks', searchlist_nakmaks, sp_nakmaks);

});

function processNakMaksTable() {

  var nakmaks_rekinu_summa = 0;
  var nakmaks_neapm_summa = 0;
  var nakmaks_prem_summa = 0;
  var nakmaks_prem_pvn = 0;
  var nakmaks_prem_kopsumma = 0;

  jQuery.each(searchlist_nakmaks, function(k,v){

    var row = $('#nakmaks_tr' + k);

    if (row.is(':visible')) {

      if (sp_nakmaks['s_summa'][k] > 0) nakmaks_rekinu_summa += parseFloat(sp_nakmaks['s_summa'][k]);
      if (sp_nakmaks['s_neapm_summa'][k] > 0) nakmaks_neapm_summa += parseFloat(sp_nakmaks['s_neapm_summa'][k]);
      if (sp_nakmaks['s_prem_summa'][k] > 0) nakmaks_prem_summa += parseFloat(sp_nakmaks['s_prem_summa'][k]);
      if (sp_nakmaks['s_prem_pvn'][k] > 0) nakmaks_prem_pvn += parseFloat(sp_nakmaks['s_prem_pvn'][k]);
      if (sp_nakmaks['s_prem_kopsumma'][k] > 0) nakmaks_prem_kopsumma += parseFloat(sp_nakmaks['s_prem_kopsumma'][k]);

    }

  });

  $('#nakmaks_rekinu_summa').html(nakmaks_rekinu_summa.toFixed(2));
  $('#nakmaks_neapm_summa').html(nakmaks_neapm_summa.toFixed(2));
  $('#nakmaks_prem_summa').html(nakmaks_prem_summa.toFixed(2));
  $('#nakmaks_prem_pvn').html(nakmaks_prem_pvn.toFixed(2));
  $('#nakmaks_prem_kopsumma').html(nakmaks_prem_kopsumma.toFixed(2));

}
</script>