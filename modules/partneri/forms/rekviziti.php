<?php

if (!empty($pardeveja_id))
	log_add("atvera", "Atvēra darbinieka " . $d['vards'] . " rekvizītus");
?>

<? // Prototypes // ?>
<div style="display:none;">

  <table id="epastsprot">>
    <tbody>
      <tr><td class="label">Epasts:</td><td><input style="width: 95%;" type="input" name="epasti_new[]" value=""> <a style="float: right; display: block;" class="hidethistoo" onclick="$(this).parent().parent().remove();" href="javascript:;">[x]</a></td></tr>
    </tbody>
  </table>

  <table id="telefonsprot">
    <tbody>
      <tr><td class="label">Telefons:</td><td><input style="width: 95%;" type="input" name="telefoni_new[]" value=""> <a style="float: right; display: block;" class="hidethistoo" onclick="$(this).parent().parent().remove();" href="javascript:;">[x]</a></td></tr>
    </tbody>
  </table>

</div>

<?php
//  Visu iespējamo amatu saraksts
$result = db_query("SELECT * FROM " . DB_PREF . "amati");
$amati = db_get_assoc_all($result);
?>

<div style="display:none;">
  <table id="amatsprot">
    <tbody>
      <tr>

        <td>
          <select name="amats_new[amats_id][]" style="width: 300px; "  class="limited_admin_edit">
            <?php
            foreach((array)$amati as $amats) {?>
              <option value="<?php echo $amats['id'];?>"><?php echo $amats['nosaukums'];?></option>
            <?php } ?>
          </select>
        </td>

        <td>
        <strong> sākot no </strong>
          <select name="amats_new[sakot_no_y][]" class="limited_admin_edit">
            <? for($year = 2011; $year <= date('Y') + 1; $year ++) { ?>
              <option value="<?= $year ?>"><?= $year ?></option>
            <? } ?>
          </select>
          -
          <select name="amats_new[sakot_no_q][]" class="limited_admin_edit">
            <? for($quarter = 1; $quarter <= 4; $quarter++) { ?>
              <option value="<?= $quarter ?>"><?php echo $_vars['quarters_roman'][$quarter]; ?></option>
            <? } ?>
          </select>
        </td>
        <td>
          <button class="hidethistoo ui-state-default ui-corner-all limited_admin_edit" onclick="$(this).closest('tr').remove();">Dzēst</button>
        </td>
      </tr>
    </tbody>
  </table>
</div>

<form action="?c=partneri<?= !empty($pardeveja_id) ? '&a=labot&id=' . $pardeveja_id . '&subtab=rekviziti' : '&a=jauns' ;?>" id="fullformplace" method="post" enctype="multipart/form-data">

  <? if (!empty($pardeveja_id)) { ?>
    <input class="non_admin_edit limited_admin_edit" type="hidden" name="pardeveja_id" value="<?= $pardeveja_id ?>" />
  <? } ?>

  <table width="750" class="data_form layout1">

    <tr><td class="label" width="170">Nosaukums:</td><td><input class="non_admin_edit limited_admin_edit" type="input" id="vardsplace" name="vards" value="<?=!empty($d['vards'])?$d['vards']:""?>"></td></tr>

    <?php foreach($telefoni as $v) { ?>
      <tr><td class="label">Telefons:</td><td><input class="non_admin_edit limited_admin_edit" style="width: 95%;" type="input" name="telefoni[<?= $v['id'] ?>]" value="<?= $v['telefons'] ?>"> <a style="float: right; display: block;" class="hidethistoo non_admin_edit limited_admin_edit" onclick="$(this).parent().parent().remove();" href="javascript:;">[x]</a></td></tr>
    <?php } ?>

    <tr><td colspan="2" align="right"><button onClick="$(this).parent().parent().before($('#telefonsprot tbody').html()); return false;" class="ui-state-default ui-corner-all hidethistoo limited_admin_edit non_admin_edit">Pievienot telefonu</button></td></tr>

    <?php foreach($epasti as $v) { ?>
      <tr><td class="label">Epasts:</td><td><input class="non_admin_edit limited_admin_edit" style="width: 95%;" type="input" name="epasti[<?= $v['id'] ?>]" value="<?= $v['epasts'] ?>"> <a style="float: right; display: block;" class="hidethistoo non_admin_edit limited_admin_edit" onclick="$(this).parent().parent().remove();" href="javascript:;">[x]</a></td></tr>
    <?php } ?>

    <tr ><td colspan="2" align="right"><button onClick="$(this).parent().parent().before($('#epastsprot tbody').html()); return false;" class="ui-state-default ui-corner-all hidethistoo non_admin_edit limited_admin_edit">Pievienot epastu</button></td></tr>

    <tr><td class="label" width="170">Kontaktpersona:</td><td><input class="non_admin_edit limited_admin_edit" type="input" id="kontaktpersona" name="kontaktpersona" value="<?=!empty($d['kontaktpersona'])?$d['kontaktpersona']:""?>"></td></tr>

    <tr><td colspan="2" class="sep"><div></div></td></tr>

    <?php if (empty($_GET['id'])) { ?>
      <tr><td class="label">Lietotājvārds:</td><td><input type="input" name="lietotajvards" id="lietotajvards" value="<?=!empty($d['lietotajvards'])?$d['lietotajvards']:""?>"></td></tr>
    <?php } else { ?>
      <tr><td class="label">Lietotājvārds:</td><td><?=!empty($d['lietotajvards'])?$d['lietotajvards']:""?></td></tr>
    <?php } ?>

    <tr><td class="label">Parole:</td><td><input class="non_admin_edit limited_admin_edit" id="password1" type="password" name="password1" value=""></td></tr>
    <tr><td class="label">Parole atkārtoti:</td><td><input class="non_admin_edit limited_admin_edit" id="password2" type="password" name="password2" value=""></td></tr>

    <tr><td class="label">Rādīt kopsavilkumā:</td><td><input type="checkbox" class="limited_admin_edit" name="aktivs" <?=!empty($d['aktivs'])?"checked":""?> /></td></tr>
    <tr><td class="label">Aktīvs no:</td><td><input type="input" name="aktivs_no" class="limited_admin_edit" id="aktivs_no" value="<?=!empty($d['aktivs_no'])?date('d.m.Y', strtotime($d['aktivs_no'])):''?>"></td></tr>
    <tr><td class="label">Aktīvs līdz:</td><td><input type="input" name="aktivs_lidz" class="limited_admin_edit" id="aktivs_lidz" value="<?=!empty($d['aktivs_lidz'])?date('d.m.Y', strtotime($d['aktivs_lidz'])):''?>"></td></tr>

    <tr><td colspan="2" class="sep"><div></div></td></tr>

    <tr><td class="label">Jur. pers. nosaukums:</td><td><input class="limited_admin_edit" type="input" id="jurpers_nosauk" name="jurpers_nosauk" value="<?=!empty($d['jurpers_nosauk'])?$d['jurpers_nosauk']:""?>"></td></tr>
    <tr><td class="label">Jur. pers. adrese:</td><td><input class="limited_admin_edit" type="input" id="jurpers_adrese" name="jurpers_adrese" value="<?=!empty($d['jurpers_adrese'])?$d['jurpers_adrese']:""?>"></td></tr>
    <tr><td class="label">Reģ. nr./ Pers. kods:</td><td><input class="limited_admin_edit" type="input" id="reg_nr" name="reg_nr" value="<?=!empty($d['reg_nr'])?$d['reg_nr']:""?>"></td></tr>
    <tr><td class="label">PVN maksātājs:</td><td><input class="limited_admin_edit" type="checkbox" name="pvn_maksatajs" <?=!empty($d['pvn_maksatajs'])?"checked":""?> /></td></tr>
    <tr><td class="label">PVN nr.:</td><td><input class="limited_admin_edit" type="input" id="pvn_nr" name="pvn_nr" value="<?=!empty($d['pvn_nr'])?$d['pvn_nr']:""?>"></td></tr>
    <tr><td class="label">PVN likme:</td><td><input class="limited_admin_edit" type="input" id="pvn_likme" name="pvn_likme" value="<?=!empty($d['pvn_likme'])?$d['pvn_likme']:""?>"></td></tr>
    <tr><td class="label">Konta numurs:</td><td><input class="limited_admin_edit" type="input" id="konta_nr" name="konta_nr" value="<?=!empty($d['konta_nr'])?$d['konta_nr']:""?>"></td></tr>
    <tr><td class="label">Norēķinu rekvizīti:</td><td><input class="limited_admin_edit" type="input" id="norek_rekviziti" name="norek_rekviziti" value="<?=!empty($d['norek_rekviziti'])?$d['norek_rekviziti']:""?>"></td></tr>
    <tr><td colspan="2" class="sep"><div></div></td></tr>
    <tr><td class="label">Sadarbības līguma nr.:</td><td><input class="limited_admin_edit" type="input" id="agenta_lig_nr" name="agenta_lig_nr" value="<?=!empty($d['agenta_lig_nr'])?$d['agenta_lig_nr']:""?>"></td></tr>
    <tr><td class="label">Sadarbības līguma datums:</td><td><input class="limited_admin_edit" type="input" name="agenta_lig_datums" id="agenta_lig_datums" value="<?=!empty($d['agenta_lig_datums'])?date('d.m.Y', strtotime($d['agenta_lig_datums'])):''?>"></td></tr>
    <tr><td class="label">Vadītājs:</td><td><input class="<?php echo check_access('partneri-vaditajs') ? 'canedit' : 'always_disabled';?>" type="checkbox" name="vaditajs" <?=!empty($d['vaditajs'])?"checked":""?> /></td></tr>
    <tr <?php echo isset($d['vaditajs']) && $d['vaditajs'] ? 'style="display:none;"' : '';?>>
      <td class="label"></td>
      <td>
        <?php
        $vaditaji = get_vaditaji();
        ?>
        <select name="vaditajs_id" class="<?php echo check_access('partneri-vaditajs') ? 'canedit' : 'always_disabled';?>" id="vaditajs_id">
          <option></option>
          <?php
          foreach($vaditaji as $vaditajs) {
            ?>
            <option value="<?php echo $vaditajs['id'];?>" <?php echo (isset($d['vaditajs_id']) && $d['vaditajs_id'] == $vaditajs['id']) ? 'selected' : ''?>><?php echo $vaditajs['vards'];?></option>
            <?php
          }
          ?>
        </select>
      </td>
    </tr>

  </table>

  <script>
  $(document).ready(function() {

    $("#agenta_lig_datums").datepicker({dateFormat:'dd.mm.yy'});
    $("#aktivs_no").datepicker({dateFormat:'dd.mm.yy'});
    $("#aktivs_lidz").datepicker({dateFormat:'dd.mm.yy'});

    $('#vardsplace').keyup(function(){
      $('#darbinieka_nosaukums').html($(this).val());
    });

  });
  </script>
  <div class="clr"></div>
  <? show_comments_block('darb_rekviziti', isset($_GET['id']) ? $_GET['id'] : null) ?>

  <? show_files_block('darb_rekviziti', isset($_GET['id']) ? $_GET['id'] : null, false, false, true, false, true) ?>

  <div class="hidethistoo non_admin_edit">
    <input type="submit" onClick="return saveAll(this, 'darbinieks_rekviziti')" class="non_admin_edit submit show_in_act_panel limited_admin_edit" value="Saglabāt">
    <input type="button" onclick="return cancelSaves();" class="non_admin_edit show_in_act_panel limited_admin_edit" value="Atcelt">
  </div>

</form>