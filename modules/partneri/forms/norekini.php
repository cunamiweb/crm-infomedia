<?php

log_add("atvera", "Atvēra darbinieka " . $d['vards'] . " rēķinus");
?>

<script>
var searchlist_rek = [];

var sp_rek = create_filter_field_array([
  's_izr_dat', 's_num', 's_summa', 's_pvn_percent', 's_pvn',
  's_kopsumma', 's_apm_dat'
]);
</script>

<form action="?c=partneri<?= !empty($pardeveja_id) ? '&a=labot&id=' . $pardeveja_id . '&subtab=norekini' : '&a=jauns' ;?>" id="fullformplace" method="post" enctype="multipart/form-data">

  <table style="display:none;">

    <tr id="darbinieku_norekini_filter">

      <th>
        <span><input class="search_field kalendari" meth="from" searchclass="s_izr_dat" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span><br>
        <span><input class="search_field kalendari" meth="to" searchclass="s_izr_dat" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span>
      </th>

      <th><span><input class="search_field" meth="standart" searchclass="s_num" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
      <th><span><input class="search_field" meth="coin" searchclass="s_summa" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
      <th><span><input class="search_field" meth="coin" searchclass="s_pvn_percent" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
      <th><span><input class="search_field" meth="coin" searchclass="s_pvn" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
      <th><span><input class="search_field" meth="coin" searchclass="s_kopsumma" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>

      <th>
        <span><input class="search_field kalendari" meth="from" searchclass="s_apm_dat" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span><br>
        <span><input class="search_field kalendari" meth="to" searchclass="s_apm_dat" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span>
      </th>

      <th></th>

    </tr>

  </table>

  <h3>Izrakstītie rēķini</h3>

  <? if (check_access('partneri-admin') && isset($_GET['id']) && check_access('partneri-labot-rekinus')) { ?>
    <p>
      <button class="ui-state-default ui-corner-all onlyinview" onclick="showRekinaIzveide(<?= $_GET['id'] ?>); return false;">Pievienot jaunu rēķinu</button>
    </p>
  <?php } ?>

  <table style="width: 700px;" id="darbinieku_norekini" class="data layout2">

    <thead>

      <tr class="header">
        <th width="100">Izrakst.&nbsp;datums</th>
        <th>Numurs</th>
        <th>Summa&nbsp;EUR<br />(bez&nbsp;PVN)</th>
        <th>PVN&nbsp;%</th>
        <th>PVN&nbsp;EUR</th>
        <th>Kopsumma&nbsp;EUR<br />(ar PVN)</th>
        <th>Apm.&nbsp;datums</th>
        <th></th>
      </tr>

    </thead>

    <tbody class="main">

      <? $inid = -1; ?>

      <? foreach($pard_rekini as $rekins) { ?>

        <? $inid ++;  ?>

        <tr id="rek_tr<?=$inid;?>" class="tr_rek">
          <td class="c"><input style="text-align: center;" type="text" name="rekini[<?=$rekins['id'];?>][izr_datums]" value="<?=!empty($rekins['izr_datums'])?date('d.m.Y', strtotime($rekins['izr_datums'])):"";?>" class="showcalendar <?php echo check_access('partneri-labot-rekinus') ? 'canedit' : 'always_disabled';?>"></td>
          <td class="c rekinanrclass">
            <?php if(check_access('partneri-labot-rekinus')) { ?>
              <input style="width: 5em; text-align: center;" class="canedit" type="text" name="rekini[<?=$rekins['id'];?>][rek_nr]" value="<?=$rekins['rek_nr'];?>" />
            <?php } else { // Hidden field for bookkeeper because he shouldnt be editing that ?>
              <span><?=$rekins['rek_nr'];?></span>
              <input type="hidden" name="rekini[<?=$rekins['id'];?>][rek_nr]" value="<?=$rekins['rek_nr'];?>" class="canedit" />
            <?php } ?>
          </td>
          <td class="r summa"><?=!empty($rekins['summa'])?$rekins['summa']:"";?></td>
          <td class="c pvn_percent"><?=!empty($rekins['pvn'])?round($rekins['pvn'], 2):0;?>%</td>
          <td class="r"><span class="pvn"><?= format_currency($rekins['kopsumma'] - $rekins['summa']) ?></span></td>
          <td class="kopsumma r" style="font-weight: bold;"><?=!empty($rekins['kopsumma'])?$rekins['kopsumma']:"";?></td>
          <td class="c"><input style="text-align: center;" type="text" name="rekini[<?=$rekins['id'];?>][apm_datums]" value="<?=!empty($rekins['apm_datums'])?date('d.m.Y', strtotime($rekins['apm_datums'])):"";?>" class="showcalendar apm_datums <?php echo check_access('partneri-labot-rekinus') ? 'canedit' : 'always_disabled';?>"></td>
          <td class="last" style="white-space: nowrap;">
            <a href="#" class="onlyinview" onclick="showRekinaDetalas(<?=$rekins['id'];?>); return false;" title="Detalizācija"><img src="css/zoom.png" alt="Detalizācija" /></a>
            <? if (check_access('partneri-labot-rekinus') && empty($rekins['uz_rokas'])) { ?>
              <a href="?c=partneri&a=gen_rek&rek_id=<?= $rekins['id'] ?>" class="onlyinview" title="Pārģenerēt PDF"><img src="css/pdf_refresh.gif" alt="Pārģen. PDF" /></a>
            <? } ?>
            <? if (!empty($rekins['fails_ep'])) { ?>
              <a class="onlyinview" href="?getfile=<?=$rekins['fails_ep'];?>" target="downloadframe" title="Lejupielādēt PDF"><img src="css/pdf.gif" alt="PDF" /></a>
            <? } ?>
            <? if (check_access('partneri-labot-rekinus')) { ?>
              <a href="#" class="hidethistoo canedit" onclick="deletePardRek(<?= $rekins['id'] ?>); return false;" title="Dzēst"><img src="css/del.png" alt="Dzēst" /></a>
            <? } ?>
          </td>

          <td style="display: none;">
          <script>

            searchlist_rek[<?=$inid;?>] = '<?=addslashes(mb_strtoupper($rekins['rek_nr']));?>';

            sp_rek['s_izr_dat'][<?=$inid;?>] = <?= !empty($rekins['izr_datums']) ? date('Ymd', strtotime($rekins['izr_datums'])) : 0 ?>;
            sp_rek['s_num'][<?=$inid;?>] = '<?=addslashes(mb_strtoupper($rekins['rek_nr']));?>';
            sp_rek['s_summa'][<?=$inid;?>] = '<?=!empty($rekins['summa']) ? $rekins['summa'] : "" ?>';
            sp_rek['s_pvn_percent'][<?=$inid;?>] = '<?=!empty($rekins['pvn']) ? $rekins['pvn'] : "" ?>';
            sp_rek['s_pvn'][<?=$inid;?>] = '<?= round($rekins['kopsumma'] - $rekins['summa'], 2) ?>';
            sp_rek['s_kopsumma'][<?=$inid;?>] = '<?=!empty($rekins['kopsumma']) ? $rekins['kopsumma'] : "" ?>';
            sp_rek['s_apm_dat'][<?=$inid;?>] = <?= !empty($rekins['apm_datums']) ? date('Ymd', strtotime($rekins['apm_datums'])) : 0 ?>;

          </script>
          </td>

        </tr>

      <? } ?>

    </tbody>

    <tr>
      <td colspan="2" align="right"><strong>Kopā:</strong></td>
      <td class="r" id="summakopa">0</td>
      <td></td>
      <td class="r" id="pvnkopa">0</td>
      <td class="r" id="kopsummakopa" style="font-weight: bold;">0</td>
      <td class="c" id="apmsummakopa">0</td>
      <td></td>
    </tr>

  </table>

  <script>

  function deletePardRek(pardeveja_rekina_id) {

    if (confirm('Vai tiešām izdzēst pārdevēja rēķinu?')) {

      $.post(
        "?c=partneri&a=del_rek",
        {pardeveja_rekina_id: pardeveja_rekina_id},
        function() {
          window.location.href = $('#fullformplace').attr('action') + '&subtab=norekini';
        }
      );

    }

    return false;

  }

  function showRekinaIzveide(darbinieka_id) {

    $('<iframe src="?c=partneri&a=rek_izveide&darbinieka_id='+darbinieka_id+'&without_nav=1" />').dialog({
        title: 'Jauns rēķins',
        autoOpen: true,
        width: 1280,
        height: $(window).height() - 50,
        modal: true,
        resizable: false,
        draggable: false,
        closeOnEscape: false
    }).width(1250);

  }

  function showRekinaDetalas(darbinieka_rek_id) {

    $('<iframe src="?c=partneri&a=rek_detalas&darbinieka_rek_id='+darbinieka_rek_id+'&without_nav=1" />').dialog({
        title: 'Rēķina detalizācija',
        autoOpen: true,
        width: 970,
        height: $(window).height() - 50,
        modal: true,
        resizable: false,
        draggable: false,
        closeOnEscape: false
    }).width(950);

  }

  $(document).ready(function() {

    var table = $("#darbinieku_norekini");
    var head = $("#darbinieku_norekini_filter");

    table.tablesorter({
      debug: false,
      highlightClass: 'highlight',
      headers: {
        0: { sorter:'latviandate' },
        2: { sorter:'currency' },
        3: { sorter:'digit' },
        4: { sorter:'digit' },
        5: { sorter:'currency' },
        7: { sorter: false }
      },
      textExtraction: function(node) {

        var node = $(node);
        var input = node.find(':input');

        if (input.length > 0) {
          if (input.is('select')) {
            return $(':selected', input).text();
          } else {
            return input.val();
          }
        } else {
          return $(node).text();
        }

      }
    }).bind('sortStart', function() {
      table.trigger("update");
    }).bind('sortEnd', function() {
      stripeTable(table);
    });

    initTableFilter(table, head, processRekTable, 'rek', searchlist_rek, sp_rek);

    $('.apm_datums', table).change(processRekTable);

    $(".showcalendar").datepicker({dateFormat:'dd.mm.yy'});

  });

  function processRekTable() {

    var summakopa = 0;
    var pvnkopa = 0;
    var kopsummakopa = 0;
    var apmsummakopa = 0;

    $('#darbinieku_norekini tr.tr_rek:visible').each(function() {

      if ($('.summa', this).html()) summakopa += parseFloat($('.summa', this).html());
      if ($('.pvn', this).html()) pvnkopa += parseFloat($('.pvn', this).html());
      if ($('.kopsumma', this).html()) kopsummakopa += parseFloat($('.kopsumma', this).html());

      if ($('.apm_datums', this).val() != '') {
        apmsummakopa += parseFloat($('.kopsumma', this).html());
      }

    });

    $('#summakopa').html('EUR&nbsp;' + summakopa.toFixed(2));
    $('#pvnkopa').html('EUR&nbsp;' + pvnkopa.toFixed(2));
    $('#kopsummakopa').html('EUR&nbsp;' + kopsummakopa.toFixed(2));
    $('#apmsummakopa').html('EUR&nbsp;' + apmsummakopa.toFixed(2));

  }
  </script>

  <? show_comments_block('darb_norekini', isset($_GET['id']) ? $_GET['id'] : null) ?>

  <? show_files_block_wperms('darb_norekini', isset($_GET['id']) ? $_GET['id'] : null, false, check_access('partneri-labot-rekinus'), check_access('partneri-labot-rekinus')); ?>

  <div class="hidethistoo non_admin_edit">
    <input type="submit" onClick="return saveAll(this, 'darbinieks_norekini')" class="non_admin_edit submit show_in_act_panel" value="Saglabāt">
    <input type="button" onclick="return cancelSaves();" class="non_admin_edit show_in_act_panel" value="Atcelt">
  </div>

</form>