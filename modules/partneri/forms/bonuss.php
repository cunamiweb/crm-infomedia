<?php
if((!check_access('darbinieki-admin') || !check_access('darbinieki-bonuss')) && (!check_access('darbinieki-savs-bonuss') || $pardeveja_id != $_SESSION['user']['id'])) {
  die('Jums nav pieejas tiesību šai sadaļai');
}

log_add("atvera", "Atvēra darbinieka " . $d['vards'] . " bonusu sadaļu");

$sel_year = (!empty($_GET['bonuss_year']) && $_GET['bonuss_year'] <= date('Y')) ? $_GET['bonuss_year'] : date('Y');
?>

<form action="" method="get" id="bonuss_filter">
  <input type="hidden" name="c" value="darbinieki" />
  <input type="hidden" name="a" value="labot" />
  <input type="hidden" name="subtab" value="bonuss" />
  <input type="hidden" name="id" value="<?= $pardeveja_id ?>" />
  <div>
    Gads:
    <span>
      <select id="bonuss_year" name="bonuss_year">
        <? for($year = 2011; $year <= date('Y'); $year ++) { ?>
          <option <?= ($year == $sel_year) ? 'selected="selected"' : '' ?> value="<?= $year ?>"><?= $year ?></option>
        <? } ?>
      </select>
    </span>
	</div>
</form>

<form action="?c=darbinieki<?= !empty($pardeveja_id) ? '&a=labot&id=' . $pardeveja_id . '&subtab=bonuss' : '&a=jauns' ;?>" id="fullformplace" method="post" enctype="multipart/form-data">
  <input type="hidden" name="year" value="<?php echo $sel_year;?>">
<?php
  /* Start: NEW */
?>
<div id="bonuss_container">
<div id="izpilde_container">
<table cellpadding="3" cellspacing="0" class="data bonuss_tabula" id="plana_izpilde" style="width: auto;">

    <?php print_bonuss_head() ?>

    <tbody>

     <?php
     // aprēķinam summu uz izmaksu - neoptimāls jo pēc tam tiek vēlreiz izsaukti tie paši stati. Ja būs ātrdarbības probl tad te mierīgi var optimizēt.
     $tlist = array();
     $balance = 0;
     for($i = BONUSS_START_YEAR; $i <= date('Y'); $i++) {
      $tlist[] = array('data' => get_bonuss_stats_full($pardeveja_id, $i, true), 'year' => $i);
     }

      foreach($tlist as $stats) {
        // Gads
        if($stats['year'] < (int)date('Y')) {
          $balance += $stats['data']['year']['balance'];
        }

        // Ceturkšņi
        foreach($stats['data']['quarters'] as $quarter => $qdata) {
          if(ceil(date('m') / 3) > $quarter || $stats['year'] < (int)date('Y')) { // vai ir vecāks ceturksnis par šībrīža
            $balance += $qdata['balance'];
          }
        }

        // Mēneši
        foreach($stats['data']['months'] as $month => $mdata) {
          if($month < (int)date('m') || $stats['year'] < (int)date('Y')) {
            $balance += $mdata['balance'];
          }
        }
      }
      //  Atņemam jau samaksātās korekcijas
      $corr_total = db_get_assoc(db_query('SELECT SUM(summa) as sum FROM ' . DB_PREF . 'bonusi_korekcijas WHERE vaditajs_id = ' . esc($pardeveja_id) . ' GROUP BY vaditajs_id' ));
      $balance -= $corr_total['sum'];

      //list($year_stats, $quarters_stats, $months_stats) = get_bonuss_stats($pardeveja_id, $sel_year);
      list($year_stats, $quarters_stats, $months_stats) = get_bonuss_stats_full($pardeveja_id, $sel_year);

      //var_dump($months_stats);


      $total = array(
        'summa' => 0,
        'plans_min' => 0,
        'plans_opt' => 0,
        'virs_min' => 0,
        'virs_opt' => 0,
        'summa_min' => 0,
        'summa_opt' => 0,
        'paid' => 0,
        'correction' => 0,
        'balance' => 0,
      );

      $querters = range(1,4);
      foreach ($querters as $quarter) {
        $data = array();

        //  Plānotais
        $data = $quarters_stats[$quarter];

        $total['summa'] += $data['summa'];
        $total['plans_min'] += $data['plans_min'];
        $total['plans_opt'] += $data['plans_opt'];
        $total['virs_min'] += $data['virs_min'];
        $total['virs_opt'] += $data['virs_opt'];
        $total['summa_min'] += $data['summa_min'];
        $total['summa_opt'] += $data['summa_opt'];
        $total['paid'] += $data['paid'];
        $total['correction'] += $data['correction'];
        $total['balance'] += $data['balance'];

        print_bonuss_row('quarter', $quarter, $data);

        //  Mēneši
        $months = array(
          ($quarter * 3) - 2, //  Pirmais ceturkšņa mēnesis
          ($quarter * 3) - 1, // Otrais
          ($quarter * 3)  // Trešais
        );

        foreach($months as $month) {
          $data = $months_stats[$month];

          $total['summa'] += $data['summa'];
          $total['plans_min'] += $data['plans_min'];
          $total['plans_opt'] += $data['plans_opt'];
          $total['virs_min'] += $data['virs_min'];
          $total['virs_opt'] += $data['virs_opt'];
          $total['summa_min'] += $data['summa_min'];
          $total['summa_opt'] += $data['summa_opt'];
          $total['paid'] += $data['paid'];
          $total['correction'] += $data['correction'];
          $total['balance'] += $data['balance'];

          print_bonuss_row('month', $month, $data, $quarter, date('Y') == $sel_year && date('m') == $month ? round($balance, 2) : false);
        }
      }

      //  Par gadu

      $total['summa'] += $year_stats['summa'];
      $total['plans_min'] += isset($year_stats['plans_min']) ? $year_stats['plans_min'] : 0;
      $total['plans_opt'] += isset($year_stats['plans_opt']) ? $year_stats['plans_opt'] : 0;
      $total['virs_min'] += isset($year_stats['virs_min']) ? $year_stats['virs_min'] : 0;
      $total['virs_opt'] += isset($year_stats['virs_opt']) ? $year_stats['virs_opt'] : 0;
      $total['summa_min'] += isset($year_stats['summa_min']) ? $year_stats['summa_min'] : 0;
      $total['summa_opt'] += isset($year_stats['summa_opt']) ? $year_stats['summa_opt'] : 0;
      $total['paid'] += $year_stats['paid'];
      $total['correction'] += $year_stats['correction'];
      $total['balance'] += $year_stats['balance'];

      print_bonuss_row('year', $year, $year_stats);
      ?>

    </tbody>
    <!--<tr>
      <td class="b-r">Kopā</td>
      <td style="text-align: left;"><?php echo format_currency($total['plans_min']);?></td>
      <td class="b-r" style="text-align: left;"><?php echo format_currency($total['plans_opt']);?></td>
      <td class="r"><?php echo format_currency($total['summa']);?></td>
      <td class="c b-r"></td>
      <td class="r"><?php echo format_currency($total['virs_min']);?></td>
      <td class="r"><?php echo format_currency($total['summa_min']);?></td>
      <td class="r"><?php echo format_currency($total['virs_opt']);?></td>
      <td class="r"><?php echo format_currency($total['summa_opt']);?></td>
      <td class="r"><?php echo format_currency($total['summa_min'] + $total['summa_opt']);?></td>

    </tr>-->

</table>



  </div></div>  <div class="clr"></div>

  <? show_comments_block('darb_izpilde', isset($_GET['id']) ? $_GET['id'] : null) ?>

  <? show_files_block('darb_izpilde', isset($_GET['id']) ? $_GET['id'] : null) ?>

  <div class="hidethistoo non_admin_edit">
    <input type="submit" onClick="return saveAll(this, 'darbinieks_izpilde')" class="non_admin_edit submit show_in_act_panel" value="Saglabāt">
    <input type="button" onclick="return cancelSaves();" class="non_admin_edit show_in_act_panel" value="Atcelt">
  </div>

  <input type="hidden" name="form_sent" value="1" />

  <script>
  initPlanaIzpildeTable(true);

  $("form#bonuss_filter").each(function() {

    var form = $(this);

    $('select#bonuss_year', form).change(function() {
      form.submit();
    });

  });
  </script>

</form>