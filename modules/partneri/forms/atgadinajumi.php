<?

log_add("atvera", "Atvēra darbinieka " . $d['vards'] . " atgādinājumus");

$query = db_query("
  SELECT g.*
  FROM `".DB_PREF."ligumi` g
  WHERE g.`pardeveja_id` = ".$pardeveja_id."
  ORDER BY g.nosaukums
");
$ligumi = array();

while($row = db_get_assoc($query)){
  $ligumi[] = htmlesc($row);
}

$where = array();
$having = array();

$where[] = "
  (
    a.pardeveja_id = ".$pardeveja_id." OR
    g.pardeveja_id = ".$pardeveja_id." OR
    g.id IN (
      SELECT liguma_id
      FROM `".DB_PREF."ligumi_saistitie_pardeveji`
      WHERE pardeveja_id = ".$pardeveja_id."
    )
  )
";

$where[] = "a.`redzamiba` IN ('kopejs', ".(is_admin() ? "'admin'" : "'pardevejs'").")";

$only_active = true;

if (!empty($_GET['atg_filter'])) {

  if (empty($_GET['only_active'])) {
    $only_active = false;
  }

}

if ($only_active == true) {
  $having[] = "priority_statuss = 0";
}

$sql = "
  SELECT
    a.*,
    g.nosaukums as klienta_nosaukums,
    IF (a.statuss = 1, IF (a.datums <= CURDATE(), 0, 1), a.statuss) as priority_statuss,
    (
      SELECT COUNT(*)
      FROM `".DB_PREF."ligumi_saistitie_pardeveji`
      WHERE
        pardeveja_id = ".$pardeveja_id." AND
        liguma_id = g.id
    ) as saistitais_ligums
  FROM `".DB_PREF."atgadinajumi` a
  LEFT JOIN `".DB_PREF."ligumi` g ON (a.liguma_id = g.id)
";

if (!empty($where)) {
  $sql .= " WHERE " . implode(' AND ', $where);
}

if (!empty($having)) {
  $sql .= " HAVING " . implode(' AND ', $having);
}

$sql .= "
  ORDER BY
    priority_statuss ASC,
    a.datums DESC,
    klienta_nosaukums ASC,
    a.id DESC
";

$query = db_query($sql);

$atgadinajumi = array();

while($row = db_get_assoc($query)){
  //  Atrod pēdējo komentāru
  if($row['atgadinajuma_veida_id'] && isset($_vars['atg_obj_tipi'][$row['atgadinajuma_veida_id']]) && isset($_vars['atg_obj_id'][$row['atgadinajuma_veida_id']])) {
    $object_id = $row[$_vars['atg_obj_id'][$row['atgadinajuma_veida_id']]];
    $object_type = $_vars['atg_obj_tipi'][$row['atgadinajuma_veida_id']];
    if($last = get_last_comment($object_id, $object_type)) {
      $row['komentars'] = $last['komentars'];
    } else {
      $row['komentars'] = '';
    }
  } else {
    //  Nav atgādinājumu veids vai tādam veidam nav definēts atbilstošais komentāra tips
    $row['komentars'] = false;
  }

  $atgadinajumi[$row['id']] = htmlesc($row);
}
?>

<?
//  Ja pēc logina ir atvests uz atgādinājumiem
if(isset($_GET['hidemenu']) && $_GET['hidemenu']) {
  //  Ja arī statusi ir neapstrādāti tad pēc šī vēl vest uz tiem
  if(get_nenomainitie_statusi(array('pardeveja_id' => $_SESSION['user']['id']))) {
    $next_url = '?c=ligumi&a=nenom_statusi';
  } else {
    $next_url = '?c=partneri';
  }
  ?>
  <script type="text/javascript">
    hideMenuBar("<?=$next_url;?>");
  </script>
  <?
}
  ?>
<? // Prototypes // ?>
<div style="display:none;">

  <table id="atgadinajumsprot">

    <tbody>

      <tr>
        <td><input type="input" class="showcalendarnew calendarcalc atgadinajumi_termimi" value="<?=date('d.m.Y');?>" name="atgadinajumi_new[datums][]"></td>
        <td class="status">Jauns</td>

        <? /* ?>
        <td>
          <select name="atgadinajumi_new[tema][]">
            <? foreach($_vars['atgadinajumu_temas'] as $temas_id => $temas_nosauk) { ?>
              <option value="<?= $temas_id ?>"><?= $temas_nosauk ?></option>
            <? } ?>
          </select>
        </td>
        <? */ ?>

        <td>
          <select name="atgadinajumi_new[redzamiba][]">
            <? if (is_admin()) { ?>
              <option value="admin">Adminiem</option>
            <? } else { ?>
              <option value="pardevejs">Pārdevējam</option>
            <? } ?>
            <option value="kopejs">Visiem</option>
          </select>
        </td>

        <td><textarea name="atgadinajumi_new[saturs][]" rows="2" cols="30"></textarea></td>
        <td></td>
        <td>
          <select style="max-width: 12em;" name="atgadinajumi_new[ligums][]">
            <option value="">Nepiesaistīts līgumam</option>
            <? foreach($ligumi as $v) { ?>
              <option value="<?= $v['id'] ?>"><?= $v['nosaukums'] ?></option>
            <? } ?>
          </select>
        </td>
        <td><?=date('d.m.Y');?></td>
        <td><?=$_vars['sys_pardeveji'][$_SESSION['user']['id']];?></td>
        <td></td>
        <td></td>
      </tr>

    </tbody>

  </table>

</div>

<div class="clear"></div>

<form action="" method="get" id="darb_atgadinajumi_filter" style="margin-bottom: 5px; float: left;">
  <input type="hidden" name="c" value="darbinieki" />
  <input type="hidden" name="a" value="labot" />
  <input type="hidden" name="id" value="<?= $pardeveja_id ?>" />
  <input type="hidden" name="subtab" value="atgadinajumi" />
  <input type="hidden" name="atg_filter" value="1" />
  <div style="text-align: left; margin-left: 10px; margin-top: 5px;">
    Rādīt tikai aktīvos:
    <span><input id="only_active" name="only_active" type="checkbox" value="1" <?= !empty($_GET['atg_filter']) ? (!empty($_GET['only_active']) ? 'checked="checked"' : '') : 'checked="checked"' ?> style="margin: 0 20px 0 0;" /></span>
  </div>
</form>

<div style="width:400px; float: right; margin: 12px 0.7% 0 0">
  <div style="background-color:#EDFBC6; width:100px; float:left;">AKTĪVS</div>
  <div style="background-color:#FBF8C6; width:100px; float:left;">NAV TERMIŅŠ</div>
  <div style="background-color:#87D97F; width:100px; float:left;">IZPILDĪTS</div>
  <div style="background-color:#D4D4D4; width:100px; float:left;">ATCELTS</div>
  <div style="clear:both;"></div>
</div>

<div class="clear"></div>

<form action="?c=partneri<?= !empty($pardeveja_id) ? '&a=labot&id=' . $pardeveja_id . '&subtab=atgadinajumi' : '&a=jauns' ;?>" id="fullformplace" method="post" enctype="multipart/form-data">

  <button class="non_admin_edit hidethistoo bookkeeper_admin_edit" onclick="return addAtgRow();">Pievienot jaunu atgādinājumu</button>

  <table width="99%" id="atgadinajumu_tabula" style="margin-top: 15px;">

    <tr>
      <th width="85">Datums</th>
      <th width="85">Statuss</th>
      <th width="80">Redzams</th>
      <th>Saturs</th>
      <th>Komentārs</th>
      <th width="130">Līgums</th>
      <th width="70">Izveidots</th>
      <th width="100">Izveidotājs</th>
      <th width="70">Izpildīts</th>
      <th width="100">Izpildīja</th>
    </tr>

    <? foreach($atgadinajumi as $atgadinajuma_id => $v ) { ?>

      <tr class="atg_id_<?= $atgadinajuma_id ?>">

        <td><input type="input" class="showcalendar calendarcalc atgadinajumi_termimi non_admin_edit bookkeeper_admin_edit <?= (!is_admin() && empty($v['pardevejs_var_labot'])) ? 'always_disabled' : '' ?>" value="<?=!empty($v['datums'])?date('d.m.Y', strtotime($v['datums'])):date('d.m.Y');?>" name="atgadinajumi[<?= $atgadinajuma_id ?>][datums]"></td>
        <td class="status">
          <span class="link_wrap">
            <? if ($v['statuss'] == 1) { ?>

              <? if (!check_access('darbinieki-atgadinajumi') && empty($v['pardevejs_var_labot'])) { ?>
                <span>Izpildīts</span>
                <span>Atcelts</span>
              <? } else { ?>
                <a onclick="return changeAtgStatus(<?= $atgadinajuma_id ?>, 2);" href="#">Izpildīts</a><br />
                <a onclick="return changeAtgStatus(<?= $atgadinajuma_id ?>, 3);" href="#">Atcelts</a>
              <? } ?>

            <? } else { ?>

              <? if ($v['statuss'] == 2) { ?>
                Izpildīts
              <? } elseif ($v['statuss'] == 3) { ?>
                Atcelts
              <? } ?>

              <? if (check_access('darbinieki-atgadinajumi') || !empty($v['pardevejs_var_labot'])) { ?>
                <a onclick="return changeAtgStatus(<?= $atgadinajuma_id ?>, 1);" href="#">X</a>
              <? } ?>

            <? } ?>
          </span>
          <?php if($v['created_pardeveja_id'] == $_SESSION['user']['id']) { ?>
            <button class="ui-state-default ui-corner-all hidethistoo" onclick="return deleteAtg('<?php echo $atgadinajuma_id;?>', this);">Dzēst</button>
          <?php } ?>
          <input type="hidden" class="bookkeeper_admin_edit" name="atgadinajumi[<?= $atgadinajuma_id ?>][statuss]" value="<?= $v['statuss'] ?>" />
        </td>

        <? /* ?>
        <td>
          <select name="atgadinajumi[<?= $atgadinajuma_id ?>][tema]" class="non_admin_edit">
            <? foreach($_vars['atgadinajumu_temas'] as $temas_id => $temas_nosauk) { ?>
              <option value="<?= $temas_id ?>" <?=(!empty($v['tema']) && $v['tema'] == $temas_id)?"selected":"";?>><?= $temas_nosauk ?></option>
            <? } ?>
          </select>
        </td>
        <? */ ?>

        <td>
          <select name="atgadinajumi[<?= $atgadinajuma_id ?>][redzamiba]" class="non_admin_edit bookkeeper_admin_edit <?= (!check_access('darbinieki-atgadinajumi') && empty($v['pardevejs_var_labot'])) ? 'always_disabled' : '' ?>">
            <? if (is_admin()) { ?>
              <option value="admin" <?=(!empty($v['redzamiba']) && $v['redzamiba'] == 'admin')?"selected":"";?>>Adminiem</option>
            <? } else { ?>
              <option value="pardevejs" <?=(!empty($v['redzamiba']) && $v['redzamiba'] == 'pardevejs')?"selected":"";?>>Pārdevējam</option>
            <? } ?>
            <option value="kopejs" <?=(!empty($v['redzamiba']) && $v['redzamiba'] == 'kopejs')?"selected":"";?>>Visiem</option>
          </select>
        </td>


        <td><textarea class="non_admin_edit bookkeeper_admin_edit <?= (!check_access('darbinieki-atgadinajumi') && empty($v['pardevejs_var_labot'])) ? 'always_disabled' : '' ?>" name="atgadinajumi[<?= $atgadinajuma_id ?>][saturs]" rows="2" cols="30"><?=!empty($v['saturs'])?$v['saturs']:"";?></textarea></td>
        <td>
          <?php if($v['komentars'] !== false) { ?>
            <textarea class="non_admin_edit" name="atgadinajumi[<?= $atgadinajuma_id ?>][komentars]" rows="2" cols="30"><?= $v['komentars'];?></textarea>
          <?php } ?>
        </td>
        <td>
          <a href="?c=ligumi&a=labot&id=<?= $v['liguma_id'] ?>"><?= $v['klienta_nosaukums'] ?></a>
          <? if (!empty($v['saistitais_ligums'])) { ?>
            <span class="saist_pard_ico">s</span>
          <? } ?>
        </td>
        <td><?=!empty($v['created'])?date('d.m.Y', strtotime($v['created'])):'';?></td>
        <? if (!empty($v['created_pardeveja_id'])) { ?>
          <td><a href="?c=partneri&a=labot&id=<?= $v['created_pardeveja_id'] ?>"><?=$_vars['sys_pardeveji'][$v['created_pardeveja_id']];?></a></td>
        <? } else { ?>
          <td><span class="atg_auto">auto</span></td>
        <? } ?>
        <td><?=!empty($v['apstr_laiks'])?date('d.m.Y', strtotime($v['apstr_laiks'])):'';?></td>
        <? if (!empty($v['apstr_pardeveja_id'])) { ?>
          <td><a href="?c=partneri&a=labot&id=<?= $v['apstr_pardeveja_id'] ?>"><?=$_vars['sys_pardeveji'][$v['apstr_pardeveja_id']];?></a></td>
        <? } else { ?>
          <td></td>
        <? } ?>

      </tr>

    <? } ?>

  </table>

  <br>

  <script>
  //atgadinajumi_tabula_visa
  $('.calendarcalc').change(refreshAtgStatuses);

  refreshAtgStatuses();

  $(".showcalendar").datepicker({dateFormat:'dd.mm.yy'});

  $("form#darb_atgadinajumi_filter").each(function() {

    var form = $(this);

    $('input#only_active', form).change(function() {
      form.submit();
    });

  });

  $('.saist_pard_ico').wTooltip({
    content: 'Saistītais līgums',
    follow: true,
    offsetY: 0,
    offsetX: 10
  });

  </script>

  <div class="hidethistoo non_admin_edit">
    <input type="submit" onClick="return saveAll(this, 'darbinieks_atgadinajumi')" class="non_admin_edit submit show_in_act_panel" value="Saglabāt">
    <input type="button" onclick="return cancelSaves();" class="non_admin_edit show_in_act_panel" value="Atcelt">
  </div>

</form>