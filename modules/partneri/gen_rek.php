<?
require_once("Zend/Pdf.php");

if (empty($_GET['rek_id'])) {
  die();
}

$rek_id = (int) $_GET['rek_id'];

$rek_data = db_get_assoc(db_query("
  SELECT *
  FROM `".DB_PREF."pardeveji_rekini`
  WHERE id = ".$rek_id."
"));

if(check_access('partneri-admin')) {
  if((!check_access('partneri-visi') && (!check_access('partneri-savi') || !$lietotajs->isPadotais($rek_data['pardeveja_id']))) || !check_access('partneri-labot-rekinus')) {
    die('Nav pieejas tiesību');
  }
} else {
  die('Nav pieejas tiesību');
}

//  Iegūstam prēmijas apmēru
$result = db_query("
  SELECT *
  FROM `".DB_PREF."pardeveji_rekini_premijas_detalas`
  WHERE pardeveja_rekina_id = " . $rek_id);

$prem_sum = '0';
while($row = db_get_assoc($result)) {
  $prem_sum = bcadd($prem_sum, $row['summa'], 2);
}

/*if(bccomp($prem_sum, '0', 2) != 0) {
  $template = 'rek_templates/darbinieku_premija.pdf';
  $pad = 12;
  $pad2 = 16;
} else {
  $template = 'rek_templates/darbinieku.pdf';
  $pad = 0;
  $pad2 = 0;
  $prem_sum = 0; // Lai pēc tam vieglāk pārbaudīt
}*/
$prem_sum = 0;

//  Iegūstam korekcijas apmēru
$korekcijas = db_get_assoc_all(db_query('SELECT * FROM '. DB_PREF . 'bonusi_korekcijas WHERE rekins_id = ' . $rek_id));
$korekciju_summa = 0;
foreach((array)$korekcijas as $korekcija) {
  $korekciju_summa = bcadd($korekciju_summa, $korekcija['summa'], 2);
}

if($korekciju_summa) {
  $template = 'rek_templates/darbinieku_premija.pdf';
  $pad = 13;
  $pad2 = 13;
} else {
  $template = 'rek_templates/darbinieku.pdf';
  $pad = 0;
  $pad2 = 0;
}


if (!empty($rek_data['fails_ep'])) {

  db_query("DELETE FROM `".DB_PREF."faili` WHERE `id` = ".(int)$rek_data['fails_ep']);
  db_query("UPDATE `".DB_PREF."pardeveji_rekini` SET `fails_ep` = NULL WHERE `id` = ".$rek_data['id']." LIMIT 1");

  if (is_file("faili/".(int)$rek_data['fails_ep'])) {
    unlink("faili/".(int)$rek_data['fails_ep']);
  }

}

$autodownload = array();

$pard_data = db_get_assoc(db_query("
  SELECT *
  FROM `".DB_PREF."pardeveji`
  WHERE id = ".$rek_data['pardeveja_id']."
"));

$default_font_size = 10;

$pdf = Zend_Pdf::load($template);

$font = Zend_Pdf_Font::fontWithPath('rek_templates/fonts/calibri.ttf');
$font_bold = Zend_Pdf_Font::fontWithPath('rek_templates/fonts/calibrib.ttf');

$page = $pdf->pages[0];

$ph = 841.8;

$page->setFont($font_bold, 12);

$page->drawText($rek_data['rek_nr'], 484, $ph - 68.5, 'UTF-8');

$page->setFont($font_bold, 7);

$datums_lat = date('Y', strtotime($rek_data['izr_datums'])) . '. gada ' . (int)date('d', strtotime($rek_data['izr_datums'])) . '. ' . mb_strtolower($_vars['menesi'][(int)date('m', strtotime($rek_data['izr_datums']))]);
$page->drawText($datums_lat, 470, $ph - 89, 'UTF-8');

//

$page->setFont($font_bold, $default_font_size);

$page->drawText($pard_data['jurpers_nosauk'], 151, $ph - 126, 'UTF-8');
$page->drawText($pard_data['jurpers_adrese'], 151, $ph - 137.5, 'UTF-8');
$page->drawText($pard_data['norek_rekviziti'], 151, $ph - 149.5, 'UTF-8');

$page->drawText(REKV_NOSAUK, 151, $ph - 162, 'UTF-8');
$page->drawText(REKV_JUR_ADR, 151, $ph - 174, 'UTF-8');
$page->drawText(REKV_BANKA, 151, $ph - 186, 'UTF-8');
$page->drawText(REKV_FIZ_ADR, 151, $ph - 198, 'UTF-8');

$page->drawText('Pārskaitījums', 151, $ph - 211, 'UTF-8'); // apm veids

$txt = sprintf(
  'Atlīdzība saskaņā ar Sadarbības līgumu par klientu piesaisti %s',
  //'Atlīdzība saskaņā ar komercaģenta līgumu Nr. %s %s',
  //$pard_data['agenta_lig_nr'],
  !empty($pard_data['agenta_lig_datums']) ? 'no ' . date('d.m.Y', strtotime($pard_data['agenta_lig_datums'])) : ''
);
$page->drawText($txt, 151, $ph - 223, 'UTF-8');

//

$page->drawText($pard_data['reg_nr'], 428, $ph - 126, 'UTF-8');
$page->drawText($pard_data['pvn_nr'], 428, $ph - 137.5, 'UTF-8');
$page->drawText($pard_data['konta_nr'], 428, $ph - 149.5, 'UTF-8');

$page->drawText(REKV_REG_NR, 428, $ph - 162, 'UTF-8');
$page->drawText(REKV_PVN_NR, 428, $ph - 174, 'UTF-8');
$page->drawText(REKV_KONTS, 428, $ph - 186, 'UTF-8');

$page->setFont($font, $default_font_size);

$pak_sum = bcsub($rek_data['summa'], $prem_sum, 2);
$pak_sum = $pak_sum - $korekciju_summa;

$page->drawText(DARB_REK_PAK_NOSAUK, 48, $ph - 262, 'UTF-8');
if($template == 'rek_templates/darbinieku_premija.pdf') {
  $page->drawText('LVL', 320, $ph - 262, 'UTF-8');
  $page->drawText('1', 375, $ph - 262, 'UTF-8');
  $page->drawText(format_currency($pak_sum, true), 410, $ph - 262, 'UTF-8');
  $page->drawText(round($rek_data['pvn'], 2) . '%', 466, $ph - 262, 'UTF-8');
  $page->drawText(format_currency($pak_sum, true), 500, $ph - 262, 'UTF-8');
} else {
  $page->drawText('LVL', 220, $ph - 262, 'UTF-8');
  $page->drawText('1', 310, $ph - 262, 'UTF-8');
  $page->drawText(format_currency($pak_sum, true), 380, $ph - 262, 'UTF-8');
  $page->drawText(round($rek_data['pvn'], 2) . '%', 451, $ph - 262, 'UTF-8');
  $page->drawText(format_currency($pak_sum, true), 500, $ph - 262, 'UTF-8');
}

if($prem_sum) {
  $page->drawText(DARB_REK_PREM_NOSAUK, 48, $ph - 262 - $pad, 'UTF-8');
  $page->drawText('LVL', 220 + 63, $ph - 262 - $pad, 'UTF-8');
  $page->drawText('1', 310 + 33, $ph - 262 - $pad, 'UTF-8');
  $page->drawText(format_currency($prem_sum, true), 389, $ph - 262 - $pad, 'UTF-8');
  $page->drawText(round($rek_data['pvn'], 2) . '%', 451, $ph - 262 - $pad, 'UTF-8');
  $page->drawText(format_currency($prem_sum, true), 508, $ph - 262 - $pad, 'UTF-8');
}

if($korekciju_summa) {
  $page->drawText(DARB_REK_KOREKC_NOSAUK, 48, $ph - 262 - $pad, 'UTF-8');
  $page->drawText('LVL', 320, $ph - 262 - $pad, 'UTF-8');
  $page->drawText('1', 375, $ph - 262 - $pad, 'UTF-8');
  $page->drawText(format_currency($korekciju_summa, true), 410, $ph - 262 - $pad, 'UTF-8');
  $page->drawText(round($rek_data['pvn'], 2) . '%', 466, $ph - 262 - $pad, 'UTF-8');
  $page->drawText(format_currency($korekciju_summa, true), 500, $ph - 262 - $pad, 'UTF-8');
}

$page->drawText(round($rek_data['pvn'], 2) . '%', 401.5, $ph - 286.2 - $pad2, 'UTF-8');
$page->drawText(round($rek_data['pvn'], 2) . '%', 450.5, $ph - 298.5 - $pad2, 'UTF-8');

$page->setFont($font_bold, $default_font_size);

$page->drawText(format_currency($rek_data['summa'], true), 508, $ph - 286.5 - $pad2, 'UTF-8');
$page->drawText(format_currency($rek_data['kopsumma'] - $rek_data['summa'], true), 508, $ph - 299 - $pad2, 'UTF-8');
$page->drawText(format_currency($rek_data['kopsumma'], true), 508, $ph - 311 - $pad2, 'UTF-8');


// details
/*
$page->setFont($font_bold, $default_font_size);

$page->drawText('RĒĶINA DETALIZĀCIJA', 242, $ph - 430, 'UTF-8');

$page->drawText('Klients', 48, $ph - 460, 'UTF-8');
$page->drawText('Rēķina Nr', 250, $ph - 460, 'UTF-8');
$page->drawText('Provīzija (bez PVN)', 350, $ph - 460, 'UTF-8');
$page->drawText('Avansā', 470, $ph - 460, 'UTF-8');

$page->setFont($font, $default_font_size);

$query = db_query("
  SELECT
    d.*,
    p.uz_rokas,
    g.nosaukums as klienta_nosaukums,
    r.rek_nr as liguma_rek_nr
  FROM `".DB_PREF."pardeveji_rekini_detalas` d
  LEFT JOIN `".DB_PREF."pardeveji_rekini` p ON (p.id = d.pardeveja_rekina_id)
  LEFT JOIN `".DB_PREF."rekini` r ON (r.id = d.rekina_id)
  LEFT JOIN `".DB_PREF."ligumi` g ON (g.id = r.liguma_id)
  LEFT JOIN `".DB_PREF."rekini_maksajumi` m ON (m.id = d.rekina_maksajuma_id)
  WHERE d.pardeveja_rekina_id = ".$rek_data['id']."
");

$row_pos = $ph - 475;

while($row = db_get_assoc($query)) {

  $nosaukums = mb_strlen($row['klienta_nosaukums']) > 35 ? mb_substr($row['klienta_nosaukums'], 0, 35) . '...' : $row['klienta_nosaukums'];

  $page->drawText($nosaukums, 48, $row_pos, 'UTF-8');
  $page->drawText($row['liguma_rek_nr'], 250, $row_pos, 'UTF-8');
  $page->drawText(format_currency($row['summa'], true), 350, $row_pos, 'UTF-8');
  $page->drawText(empty($row['rekina_maksajuma_id']) ? 'Jā' : 'Nē', 470, $row_pos, 'UTF-8');

  $row_pos -= 13;

}
*/
//

$pdf_output = $pdf->render();

$name = 'REKINS ' . $rek_data['rek_nr'] . '.pdf';

$name = prepare_filename($name);

db_query("
  INSERT INTO `".DB_PREF."faili` SET
    `objekta_id` = ".$rek_data['pardeveja_id'].",
    `objekta_tips` = 'darb_norekini',
    `pardeveja_id` = ".$_SESSION['user']['id'].",
    `fails` = '".esc($name)."',
    `izmers` = ".strlen($pdf_output)."
");

if(!save_file($pdf_output, "faili/" . db_last_id())) {

  db_query("DELETE FROM `".DB_PREF."faili` WHERE `id` = ".db_last_id());

} else {

  $autodownload[] = db_last_id();

  db_query("
    UPDATE `".DB_PREF."pardeveji_rekini`
    SET `fails_ep` = ".db_last_id()."
    WHERE `id` = ".$rek_data['id']."
    LIMIT 1
  ");

}

$_SESSION['autodownload_file_ids'] = $autodownload;

header("Location: ?c=partneri&a=labot&id=" . $rek_data['pardeveja_id'] . "&subtab=norekini");
die();
?>