<?

if (empty($_POST['pardeveja_rekina_id'])) {
  die();
}

$pardeveja_rekina_id = (int) $_POST['pardeveja_rekina_id'];

$rek_data = db_get_assoc(db_query("
  SELECT *
  FROM `".DB_PREF."pardeveji_rekini`
  WHERE id = ".$pardeveja_rekina_id."
"));

if(check_access('partneri-admin')) {
  if((!check_access('partneri-visi') && (!check_access('partneri-savi') || !$lietotajs->isPadotais($rek_data['pardeveja_id']))) || !check_access('partneri-labot-rekinus')) {
    die('Nav pieejas tiesību');
  }
} else {
  die('Nav pieejas tiesību');
}

if (empty($rek_data)) {
  die();
}

$pardevejs = db_get_assoc(db_query("SELECT * FROM `".DB_PREF."pardeveji` WHERE `id` = " . $rek_data['pardeveja_id']));

$result = db_query("
  DELETE FROM `".DB_PREF."pardeveji_rekini`
  WHERE `id` = ".$pardeveja_rekina_id."
  LIMIT 1
");

if ($result) {

  db_query("
    DELETE FROM `".DB_PREF."bonusi`
    WHERE `pardeveja_rekina_id` = ".$pardeveja_rekina_id."
  ");

  db_query("
    DELETE FROM `".DB_PREF."bonusi_korekcijas`
    WHERE `rekins_id` = ".$pardeveja_rekina_id."
  ");

  db_query("
    DELETE FROM `".DB_PREF."pardeveji_rekini_detalas`
    WHERE `pardeveja_rekina_id` = ".$pardeveja_rekina_id."
  ");

  db_query("
    DELETE FROM `".DB_PREF."pardeveji_rekini_premijas_detalas`
    WHERE `pardeveja_rekina_id` = ".$pardeveja_rekina_id."
  ");

  if (!empty($rek_data['fails_ep'])) {

    db_query("DELETE FROM `".DB_PREF."faili` WHERE `id` = ".(int)$rek_data['fails_ep']);

    if (is_file("faili/".(int)$rek_data['fails_ep'])) {
      unlink("faili/".(int)$rek_data['fails_ep']);
    }

  }

}

$log_data = array();

$log_data['rekins']['old'] = $rek_data;
$log_data['rekins']['title'] = sprintf('Dzēsa darbinieka %s rēķinu %s', $pardevejs['vards'], $rek_data['rek_nr']);

log_add("laboja", $log_data);

header("Location: ?c=partneri&a=labot&id=" . $rek_data['pardeveja_id'] . "&subtab=norekini");
die();

?>