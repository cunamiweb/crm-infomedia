<?
log_add("atvera", "Atvēra galveno atgādinājumu sadaļu");
require('list_process.php');
?>

<form action="" method="get" id="atgadinajumi_main_filter">
  <input type="hidden" name="c" value="atgadinajumi" />
  <input type="hidden" name="main_filter" value="1" />
  <div style="float: left; text-align: left; margin-left: 10px; margin-top: 5px;">
    Rādīt tikai aktīvos:
    <span><input id="only_active" name="only_active" type="checkbox" value="1" <?= !empty($_GET['main_filter']) ? (!empty($_GET['only_active']) ? 'checked="checked"' : '') : 'checked="checked"' ?> style="margin: 0 20px 0 0;" /></span>
    Rādīt arī tikai pārdevējam redzamos:
    <span><input id="show_private" name="show_private" type="checkbox" value="1" <?= !empty($_GET['main_filter']) ? (!empty($_GET['show_private']) ? 'checked="checked"' : '') : '' ?> style="margin: 0 20px 0 0;" /></span>
  </div>
</form>

<div style="margin:0 0.7% 0 0; float: right; width:400px;">
  <div style="background-color:#EDFBC6; width:100px; float:left;">AKTĪVS</div>
  <div style="background-color:#FBF8C6; width:100px; float:left;">NAV TERMIŅŠ</div>
  <div style="background-color:#87D97F; width:100px; float:left;">IZPILDĪTS</div>
  <div style="background-color:#D4D4D4; width:100px; float:left;">ATCELTS</div>
  <div style="clear:both;"></div>
</div>

<button id="labotdzestbutton" onClick="return editoradd(this, <?= is_admin() ? 1 : 0 ?>, <?= is_limited() ? $_SESSION['user']['ierobezots'] : 0 ;?>)">Labot</button>

<?

$where = array();
$having = array();

$show_private = false;
$only_active = true;

if (!empty($_GET['main_filter'])) {

  if (!empty($_GET['show_private'])) {
    $show_private = true;
  }

  if (empty($_GET['only_active'])) {
    $only_active = false;
  }

}

if ($show_private == false) {
  $where[] = "a.redzamiba != 'pardevejs'";
}

if(check_access('atgadinajumi-tikai-gramatvediba') && !is_superadmin()) {
   $where[] = "(a.atgadinajuma_veida_id IN (1, 8, 9, 10) OR
        a.pardeveja_id = " . $_SESSION['user']['id'] . " OR
        a.created_pardeveja_id = " . $_SESSION['user']['id'] . ")";
}

if(!check_access('atgadinajumi-visi'))  {
  $where[] = "(a.pardeveja_id = " . $_SESSION['user']['id'] . " OR
      a.created_pardeveja_id = " . $_SESSION['user']['id'] . ")";
}

if(check_access('atgadinajumi-vaditajs-tikai-savi') && !is_superadmin()) {
    $padotie = Pardevejs::model()->findAll(array('conditions' => array('vaditajs_id = ' . $_SESSION['user']['id'])));
    $padotie_ids = modelsToList($padotie, 'id');
    $padotie_ids[] = $_SESSION['user']['id'];

    $where[] =  "(a.pardeveja_id IN (".implode(',', $padotie_ids).") OR a.created_pardeveja_id IN (".implode(',', $padotie_ids).") OR g.pardeveja_id IN (".implode(',', $padotie_ids)."))";
}

if ($only_active == true) {
  $having[] = "priority_statuss = 0";
}

$sql = "
  SELECT
    a.*,
    g.nosaukums as klienta_nosaukums,
    p.vards as pardeveja_nosaukums,
    p2.id as liguma_pardeveja_id,
    p2.vards as liguma_pardeveja_nosaukums,
    IF (a.statuss = 1, IF (a.datums <= CURDATE(), 0, 1), a.statuss) as priority_statuss
  FROM `".DB_PREF."atgadinajumi` a
  LEFT JOIN `".DB_PREF."ligumi` g ON (a.liguma_id = g.id)
  LEFT JOIN `".DB_PREF."pardeveji` p ON (a.pardeveja_id = p.id)
  LEFT JOIN `".DB_PREF."pardeveji` p2 ON (g.pardeveja_id = p2.id)
";

if (!empty($where)) {
  $sql .= " WHERE " . implode(' AND ', $where);
}

if (!empty($having)) {
  $sql .= " HAVING " . implode(' AND ', $having);
}

$sql .= "
  ORDER BY
    priority_statuss ASC,
    a.datums DESC,
    klienta_nosaukums ASC,
    a.id DESC
";

$query = db_query($sql);

$atgadinajumi = array();

while($row = db_get_assoc($query)){
  $atgadinajumi[$row['id']] = htmlesc($row);
}
?>

<script>
var searchlist = [];

var sp = create_filter_field_array([
  's_datums', 's_statuss', 's_redzamiba', 's_saturs', 's_ligums', 's_pardevejs',
  's_izveidots', 's_izveidoja', 's_izpild_dat', 's_izpildija'
]);
</script>

<? // Prototypes // ?>
<div style="display:none;">

  <div id="komentarsprot__atgadinajumi">
    <table class="comment_form">
      <tr>
        <td width="70">Komentārs:</td>
        <td>
          <textarea name="komentari_new[_atgadinajumi][komentars][]" rows="2" cols="30"></textarea>
        </td>
      </tr>
    </table>
  </div>

</div>

<table style="display:none;">

  <tr id="atgadinajumu_tabula_filter">

    <th></th>

    <th>
      <span><input class="search_field kalendari" meth="from" searchclass="s_datums" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span><br>
      <span><input class="search_field kalendari" meth="to" searchclass="s_datums" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span>
    </th>

    <th>
      <select class="search_field advonchange" searchclass="s_statuss" meth="int" style="width: 50px;">
        <option value=""></option>
        <option value="1">Neapstrādāts</option>
        <option value="2">Izpildīts</option>
        <option value="3">Atcelts</option>
      </select>
    </th>

    <? /* ?>
    <th>
      <select class="search_field advonchange" searchclass="s_tema" meth="int" style="width: 100px;">
        <option value=""></option>
        <?php foreach($_vars['atgadinajumu_temas'] as $k=>$p){ ?>
          <option value="<?=$k;?>"><?=$p;?></option>
        <?php } ?>
      </select>
    </th>
    <? */ ?>

    <th>
      <select class="search_field advonchange" searchclass="s_redzamiba" meth="standart" style="width: 100px;">
        <option value=""></option>
        <option value="admin">Adminiem</option>
        <? if ($show_private) { ?>
          <option value="pardevejs">Pārdevējam</option>
        <? } ?>
        <option value="kopejs">Visiem</option>
      </select>
    </th>

    <th><span><input class="search_field" meth="standart" searchclass="s_saturs" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <th><span><input class="search_field" meth="standart" searchclass="s_ligums" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>

    <th>
      <select class="search_field advonchange" searchclass="s_pardevejs" meth="int" style="width: 100px;">
        <option value=""></option>
        <?php foreach($_vars['sys_pardeveji'] as $k=>$p){ ?>
          <option value="<?=$k;?>"><?=$p;?></option>
        <?php } ?>
      </select>
    </th>

    <th>
      <span><input class="search_field kalendari" meth="from" searchclass="s_izveidots" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span><br>
      <span><input class="search_field kalendari" meth="to" searchclass="s_izveidots" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span>
    </th>

    <th><span><input class="search_field" meth="standart" searchclass="s_izveidoja" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>

    <th>
      <span><input class="search_field kalendari" meth="from" searchclass="s_izpild_dat" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span><br>
      <span><input class="search_field kalendari" meth="to" searchclass="s_izpild_dat" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span>
    </th>

    <th><span><input class="search_field" meth="standart" searchclass="s_izpildija" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>

  </tr>

</table>

<form action="?c=atgadinajumi" id="fullformplace" method="post" enctype="multipart/form-data" class="clear">

<table width="99%" id="atgadinajumu_tabula" style="margin-top: 10px;" class="ckey_atgadinajumi_list">

  <thead>

    <tr>
      <th>Npk</th>
      <th width="85">Datums</th>
      <th width="85">Statuss</th>
      <th width="80">Redzams</th>
      <th>Saturs</th>
      <th width="140">Līgums</th>
      <th width="140">Darbinieks</th>
      <th width="70">Izveidots</th>
      <th width="100">Izveidotājs</th>
      <th width="70">Izpildīts</th>
      <th width="100">Izpildīja</th>
    </tr>

  </thead>

  <tbody class="main">

  <? $inid = -1 ?>

  <? foreach($atgadinajumi as $atgadinajuma_id => $v ) { ?>

    <?
    $inid ++;

    // Editing permissive class name if the reminder was created by this admin(type bookkeeper); empty string otherwise
    //  Dāvis Frīdenvalds 2012.02.06.
    $bookkeeper_can_edit = $v['created_pardeveja_id'] == $_SESSION['user']['id'] ? 'bookkeeper_admin_edit' : '';

    $minimal_can_edit = $v['created_pardeveja_id'] == $_SESSION['user']['id'] ? 'minimal_admin_edit' : '';
    ?>

    <tr id="tr<?=$inid;?>" class="atg_id_<?= $atgadinajuma_id ?> atg_row">

      <td class="s_npk c" align="center"><?= $inid + 1 ?></td>
      <td><input type="input" class="showcalendar calendarcalc atgadinajumi_termimi <?=$bookkeeper_can_edit;?> <?=$minimal_can_edit;?>" value="<?=!empty($v['datums'])?date('d.m.Y', strtotime($v['datums'])):date('d.m.Y');?>" name="atgadinajumi[<?= $atgadinajuma_id ?>][datums]"></td>
      <td class="status">
        <span class="link_wrap">
          <? if ($v['statuss'] == 1) { ?>
            <a onclick="return changeAtgStatus(<?= $atgadinajuma_id ?>, 2);" href="#">Izpildīts</a><br />
            <a onclick="return changeAtgStatus(<?= $atgadinajuma_id ?>, 3);" href="#">Atcelts</a>
          <? } else { ?>
            <? if ($v['statuss'] == 2) { ?>
              Izpildīts
            <? } elseif ($v['statuss'] == 3) { ?>
              Atcelts
            <? } ?>
            <a onclick="return changeAtgStatus(<?= $atgadinajuma_id ?>, 1);" href="#">X</a>
          <? } ?>
        </span>
        <?php if($v['created_pardeveja_id'] == $_SESSION['user']['id']) { ?>
          <button class="ui-state-default ui-corner-all hidethistoo" onclick="return deleteAtg('<?php echo $atgadinajuma_id;?>', this);">Dzēst</button>
        <?php } ?>
        <input type="hidden" class="<?=$bookkeeper_can_edit;?> <?=$minimal_can_edit;?>" name="atgadinajumi[<?= $atgadinajuma_id ?>][statuss]" value="<?= $v['statuss'] ?>" />
      </td>

      <? /* ?>
      <td>
        <select name="atgadinajumi[<?= $atgadinajuma_id ?>][tema]">
          <? foreach($_vars['atgadinajumu_temas'] as $temas_id => $temas_nosauk) { ?>
            <option value="<?= $temas_id ?>" <?=(!empty($v['tema']) && $v['tema'] == $temas_id)?"selected":"";?>><?= $temas_nosauk ?></option>
          <? } ?>
        </select>
      </td>
      <? */ ?>

      <td align="center">
        <select name="atgadinajumi[<?= $atgadinajuma_id ?>][redzamiba]" class="<?=$bookkeeper_can_edit;?>">
          <option value="admin" <?=(!empty($v['redzamiba']) && $v['redzamiba'] == 'admin')?"selected":"";?>>Adminiem</option>
          <? if ($show_private) { ?>
            <option value="pardevejs" <?=(!empty($v['redzamiba']) && $v['redzamiba'] == 'pardevejs')?"selected":"";?>>Pārdevējam</option>
          <? } ?>
          <option value="kopejs" <?=(!empty($v['redzamiba']) && $v['redzamiba'] == 'kopejs')?"selected":"";?>>Visiem</option>
        </select>
      </td>

      <td>
        <textarea name="atgadinajumi[<?= $atgadinajuma_id ?>][saturs]" rows="2" cols="30" class="<?=$bookkeeper_can_edit;?>"><?=!empty($v['saturs'])?$v['saturs']:"";?></textarea>
      </td>

      <? if (!empty($v['liguma_id'])) { ?>
        <td><a href="?c=ligumi&a=labot&id=<?= $v['liguma_id'] ?>"><?= $v['klienta_nosaukums'] ?></a></td>
      <? } else { ?>
        <td></td>
      <? } ?>

      <? if (!empty($v['pardeveja_id'])) { ?>
        <td><a href="?c=darbinieki&a=labot&id=<?= $v['pardeveja_id'] ?>"><?= $v['pardeveja_nosaukums'] ?></a></td>
      <? } else if (!empty($v['liguma_pardeveja_id'])) { ?>
        <td><a href="?c=darbinieki&a=labot&id=<?= $v['liguma_pardeveja_id'] ?>"><?= $v['liguma_pardeveja_nosaukums'] ?></a></td>
      <? } else { ?>
        <td></td>
      <? } ?>

      <td><?=!empty($v['created'])?date('d.m.Y', strtotime($v['created'])):'';?></td>

      <? if (!empty($v['created_pardeveja_id'])) { ?>
        <td><a href="?c=darbinieki&a=labot&id=<?= $v['created_pardeveja_id'] ?>"><?=$_vars['sys_pardeveji'][$v['created_pardeveja_id']];?></a></td>
      <? } else { ?>
        <td><span class="atg_auto">auto</span></td>
      <? } ?>

      <td><?=!empty($v['apstr_laiks'])?date('d.m.Y', strtotime($v['apstr_laiks'])):'';?></td>

      <? if (!empty($v['apstr_pardeveja_id'])) { ?>
        <td><a href="?c=darbinieki&a=labot&id=<?= $v['apstr_pardeveja_id'] ?>"><?=$_vars['sys_pardeveji'][$v['apstr_pardeveja_id']];?></a></td>
      <? } else { ?>
        <td></td>
      <? } ?>

      <td style="display: none;">
      <script>
        searchlist[<?=$inid;?>] = '';
        sp['s_datums'][<?=$inid;?>] = <?= !empty($v['datums']) ? date('Ymd', strtotime($v['datums'])) : 0 ?>;
        sp['s_statuss'][<?=$inid;?>] = <?= $v['statuss'] ?>;
        <? /* ?>
        sp['s_tema'][<?=$inid;?>] = <?= $v['tema'] ?>;
        <? */ ?>
        sp['s_redzamiba'][<?=$inid;?>] = '<?= $v['redzamiba'] ?>';
        sp['s_saturs'][<?=$inid;?>] = '<?=addslashes(mb_strtoupper($v['saturs']));?>';
        sp['s_ligums'][<?=$inid;?>] = '<?=addslashes(mb_strtoupper($v['klienta_nosaukums']));?>';

        <? if (!empty($v['pardeveja_id'])) { ?>
          sp['s_pardevejs'][<?=$inid;?>] = '<?= $v['pardeveja_id'] ?>';
        <? } elseif (!empty($v['liguma_pardeveja_id'])) { ?>
          sp['s_pardevejs'][<?=$inid;?>] = '<?= $v['liguma_pardeveja_id'] ?>';
        <? } else { ?>
          sp['s_pardevejs'] = ';
        <? } ?>

        sp['s_izveidots'][<?=$inid;?>] = <?= !empty($v['created']) ? date('Ymd', strtotime($v['created'])) : 0 ?>;

        <? if (!empty($v['created_pardeveja_id'])) { ?>
          sp['s_izveidoja'][<?=$inid;?>] = '<?=addslashes(mb_strtoupper($_vars['sys_pardeveji'][$v['created_pardeveja_id']]));?>';
        <? } else { ?>
          sp['s_izveidoja'][<?=$inid;?>] = 'auto';
        <? } ?>

        sp['s_izpild_dat'][<?=$inid;?>] = <?= !empty($v['apstr_laiks']) ? date('Ymd', strtotime($v['apstr_laiks'])) : 0 ?>;

        <? if (!empty($v['apstr_pardeveja_id'])) { ?>
          sp['s_izpildija'][<?=$inid;?>] = '<?=addslashes(mb_strtoupper($_vars['sys_pardeveji'][$v['apstr_pardeveja_id']]));?>';
        <? } else { ?>
          sp['s_izpildija'][<?=$inid;?>] = '';
        <? } ?>

      </script>
      </td>

    </tr>

  <? } ?>

  </tbody>

</table>

<? show_comments_block('_atgadinajumi', null, false, !check_access('atgadinajumi-komentet')) ?>

<div class="hidethistoo">
  <input type="submit" onClick="return saveAll(this, 'atgadinajumi')" class="submit show_in_act_panel"  value="Saglabāt">
  <input type="button" onclick="return cancelSaves();" class="show_in_act_panel" value="Atcelt">
</div>

</form>

<script>
$(document).ready(function() {

  var table = $("#atgadinajumu_tabula");
  var head = $("#atgadinajumu_tabula_filter");

  var cache_key = getCacheKeyFromClass(table);

  var sortListFromCache = cache_key ? $.jStorage.get(cache_key + '_sort', []) : [];

  table.tablesorter({
    debug: false,
    highlightClass: 'highlight',
    sortList: sortListFromCache,
    headers: {
      0: { sorter: false },
      1: { sorter:'latviandate' },
      4: { sorter:'latviantext' },
      5: { sorter:'latviantext' },
      6: { sorter:'latviantext' },
      7: { sorter:'latviandate' },
      8: { sorter:'latviantext' },
      9: { sorter:'latviandate' },
      10: { sorter:'latviantext' }
    },
    textExtraction: function(node) {

      var node = $(node);
      var input = node.find(':input');

      if (input.length > 0) {
        return input.val();
      } else {
        return $(node).text();
      }

    }
  }).bind('sortStart', function() {
    table.trigger("update");
  }).bind('sortEnd', function() {

    if (cache_key) $.jStorage.set(cache_key + '_sort', table.get(0).config.sortList);

    updateNpk(table);
    stripeTable(table);

  });

  initTableFilter(table, head, processTable, '', searchlist, sp);

});

function processTable() {

  var table = $(this);

  updateNpk(table);

}


function updateNpk(table) {

  // updating npk

  var visible_rows = $('tr.atg_row:visible', table);

  var npk = 1;

  visible_rows.each(function() {

    $('.s_npk', this).html(npk);

    npk ++;

  });

}

$('.calendarcalc').change(refreshAtgStatuses);

refreshAtgStatuses();

$(".showcalendar").datepicker({dateFormat:'dd.mm.yy'});

$("form#atgadinajumi_main_filter").each(function() {

  var form = $(this);

  $('input#only_active, input#show_private', form).change(function() {
    form.submit();
  });

});

editordisable();
</script>