<?php
if (!empty($_POST)) {

  $errors = validate_atgadinajumi_form($_POST);

  if (empty($errors)) {

    $_POST = trim_array($_POST);

    // updating reminders

    if (!empty($_POST['atgadinajumi'])) {

      foreach($_POST['atgadinajumi'] as $atgadinajuma_id => $a) {

        $old_data = db_get_assoc(db_query("SELECT * FROM `".DB_PREF."atgadinajumi` WHERE id = ".$atgadinajuma_id." LIMIT 1"));

        if(check_access('atgadinajumi-labot-visus') || $old_data['created_pardeveja_id'] == $_SESSION['user']['id']) {

          $sql = "
            UPDATE `".DB_PREF."atgadinajumi`
            SET
              datums = '".esc(date('Y-m-d', strtotime($a['datums'])))."',
              redzamiba = '".$a['redzamiba']."',
              saturs = '".esc($a['saturs'])."'
            WHERE id = ".$atgadinajuma_id."
            LIMIT 1
          ";

          db_query($sql);

          $log_data = array();

          $log_data['atgadinajums']['old'] = $old_data;
          $log_data['atgadinajums']['new'] = db_get_assoc(db_query("SELECT * FROM `".DB_PREF."atgadinajumi` WHERE id = ".$atgadinajuma_id." LIMIT 1"));
          $log_data['atgadinajums']['title'] = sprintf('Laboja atgādinājumu ar ID %s', $atgadinajuma_id);

          log_add("laboja", $log_data);

        }
      }

      // inserting comments

      process_comments(array('_atgadinajumi'));

    }

    header('Location: ?c=atgadinajumi');
    die();

  }

}
?>