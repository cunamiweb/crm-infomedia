<?php
if(!check_access('nozares-saraksts')) {
  die('Jums nav pieejas tiesību šai sadaļai.');
}

$id = getGet('id');

if($a == 'labot' && $id) {
  $nozare = Nozare::model()->findByPk($id);
  log_add("atvera", "Atvēra nozari " . $nozare->title_lv);
} else {
  $nozare = new Nozare;
  $nozare->country = getGet('country') ? getGet('country') : 1;
}

require("new_process.php");

$currency = getGet('currency') ? getGet('currency') : 'EUR';

$virsnozares = Nozare::model()->findAll(array('conditions' => array('parent_id = 0', 'country = ' . $nozare->country)));

//  tmp defaultais meklētājs
$mekletajs = Mekletajs::model()->find(array('conditions' => array(
  'valsts = ' . esc($nozare->country),
  'tips = "google"'
)));

if(!$mekletajs) {
  throw new Exception('Cannot find default search engine');
}
?>

<!-- Prototypes -->
<div style="display:none;">

  <!-- Pievienot a/v -->
  <div id="atslegvards">
    <form class="av_form" name="add-atslegvards" method="POST" action="" onsubmit="validateAV(this); return false;">
      <input type="hidden" name="nozare_id" value="<?php echo $nozare->id;?>" />
      <input type="hidden" name="StandartaPiedavajums[mekletajs_id]" value="<?php echo $mekletajs->id;?>">
      <table class="data_form layout1">
        <tr><td class="label" width="100">Atslēgvārds</td><td><input type="text" name="StandartaPiedavajums[atslegvards]" id="standartapiedavajums_atslegvards" value=""></td></tr>
      </table>
      <input type="submit" value="Pievienot" />
    </form>
  </div>

  <!-- Mainīt cenas -->
  <div id="change_prices">
    <form method="POST" action="">
      <table class="data_form layout1">
        <tr>
          <td class="label" width="110">Standarta cena</td>
          <td><input type="checkbox" name="enable_override" class="enable_override" onchange="$(this).parent().parent().find('.standartapiedavajums_override').toggleDisabled()" /></td>
          <td><input type="text" name="StandartaPiedavajums[override]" class="standartapiedavajums_override" value="" disabled="disabled"></td>
        </tr>
        <tr>
          <td class="label" width="110">Spec cena</td>
          <td><input type="checkbox" name="enable_override_s" class="enable_override_s" onchange="$(this).parent().parent().find('.standartapiedavajums_override_s').toggleDisabled()" /></td>
          <td><input type="text" name="StandartaPiedavajums[override_s]" class="standartapiedavajums_override_s" value="" disabled="disabled"></td>
        </tr>
        <tr>
          <td class="label" width="110">Top cena</td>
          <td><input type="checkbox" name="enable_override_t" class="enable_override_t" onchange="$(this).parent().parent().find('.standartapiedavajums_override_t').toggleDisabled()" /></td>
          <td><input type="text" name="StandartaPiedavajums[override_t]" class="standartapiedavajums_override_t" value="" disabled="disabled"></td>
        </tr>
      </table>
      <input type="submit" value="Mainīt" />
    </form>
  </div>
</div>

<button id="labotdzestbutton" onClick="return editoradd(this, <?php echo is_admin() ? 1 : 0 ?>, <?php echo is_limited() ? $_SESSION['user']['ierobezots'] : 0 ?>)">Labot</button>

<form action="?c=nozares<?= !empty($id) ? '&a=labot&id=' . $id : '&a=jauns' ;?>" id="fullformplace" method="post" enctype="multipart/form-data">
    <input type="hidden" name="Nozare[country]" id="nozare_country" value="<?php echo $nozare->country;?>" />
  <?php if($id) { ?>
    <input type="hidden" name="Nozare[id]" id="nozare_id" value="<?php echo $id;?>">
  <?php } ?>
  <table width="750" class="data_form layout1">
      <tr>
        <td class="label" width="170">Virs-nozare:</td>
        <td>
          <select class="limited_admin_edit" id="nozare_parent_id" name="Nozare[parent_id]">
            <option value="">- Virsnozare -</option>
            <?php foreach($virsnozares as $virsnozare) {  ?>
              <option value="<?php echo $virsnozare->id;?>" <?php echo $virsnozare->id == $nozare->parent_id ? 'selected' : '';?>><?php echo $virsnozare->title_lv;?></option>
            <?php } ?>
          </select>
        </td>
      </tr>

    <tr><td class="label" width="170">Nosaukums:</td><td><input class="limited_admin_edit" type="input" id="nozare_title_lv" name="Nozare[title_lv]" value="<?php echo $nozare->title_lv;?>"></td></tr>

   </table>

   <?php if($id) { ?>
   <div>
     <a href="?c=nozares&a=labot&id=<?php echo $id;?>&currency=LVL">LVL</a>
     <a href="?c=nozares&a=labot&id=<?php echo $id;?>&currency=EUR">EUR</a>
   </div>
   <input type="hidden" class='sfield' name="order" value="<?php echo getRequest('order');?>">
   <input type="hidden" class='sfield' name="direction" value="<?php echo getRequest('direction');?>">

   <table cellpadding="3" cellspacing="0" id="atslegvardi_list" class="data ckey_pieteikumi_list" style="width: auto;">
      <thead>
        <tr class="header">
            <?php print_th('Atslēgas vārds', 'atslegvards');?>
            <?php print_th('Meklēšanas skaits', 'meklejumi');?>
            <th class="header">Google KWT klikšķa cena</th>
            <th class="header">Standarta cena</th>
            <th class="header">Speciālā cena</th>
            <th class="header">Top cena</th>
            <th class="hidethistoo" width="280" class="header">Darbības</th>
        </tr>
      </thead>
      <tbody class="main">
        <?php
        $i = 1;

        $criteria = array();

        if(getRequest('order') == 'atslegvards' || !getRequest('order')) {
          $order = 'atslegvards ' . (getRequest('direction') ? getRequest('direction') : 'asc');
        } elseif(getRequest('order') == 'meklejumi') {
          $order = 'meklejumi ' . (getRequest('direction') ? getRequest('direction') : 'desc');
        }

        $avs = $nozare->getStandartaPiedavajums(array('order' => $order));

        foreach((array)$avs as $av) { ?>
          <tr <?php echo $i%2 ? 'class="odd"' : '';?>>
            <td class="av"><a href="?c=nozares&a=cenu_izmainas&id=<?php echo $av->id;?>"><?php echo $av->atslegvards;?></a></td>
            <td class="r"><?php echo $av->meklejumi;?></td>
            <td class="r">
            <?php
              /*if($currency != 'LVL') {
                echo convert_currency($av->google_cpc, 'LVL', $currency);
              } else {   */
                echo $av->google_cpc . '$';
              /*}*/
            ?>
            </td>
            <td class="r cena">
              <?php if($av->override) { ?>
                <span class="bez_pvn_rek_ico" style="margin-top: 2px;">!</span>
              <?php } ?>
              <span class="value"><?php echo format_currency($av->cena($currency));?></span>
            </td>
            <td class="r cena_s">
              <?php if($av->override_s) { ?>
                <span class="bez_pvn_rek_ico" style="margin-top: 2px;">!</span>
              <?php } ?>
              <span class="value"><?php echo format_currency($av->cena_S($currency));?></span>
            </td>
            <td class="r cena_t">
              <?php if($av->override_t) { ?>
                <span class="bez_pvn_rek_ico" style="margin-top: 2px;">!</span>
              <?php } ?>
              <span class="value"><?php echo format_currency($av->cena_T($currency));?></span>
            </td>
            <td class="hidethistoo">
              <?php if(check_access('nozares-atjauninasana')) { ?>
                <button onclick="window.location = '?c=nozares&a=update_std&id=<?php echo $av->id;?>'; return false;" class="ui-state-default ui-corner-all">Atjaunināt</button>
              <?php } ?>

              <?php if(check_access('nozares-cenu-maina')) { ?>
                <button onclick="changePrices(this, <?php echo $av->id;?>, <?php echo $av->override !== null ? 1 : 0;?>,  <?php echo $av->override_s !== null ? 1 : 0;?>, <?php echo $av->override_t !== null ? 1 : 0;?>); return false;" class="ui-state-default ui-corner-all">Mainit cenas</button>
              <?php } ?>

              <?php if(0 && $av->hasOverridenPrices() && check_access('nozares-cenu-maina')) { //disabled ?>
                <button onclick="window.location='?c=nozares&a=remove_price_overrides&id=<?php echo $av->id;?>'; return false;" class="ui-state-default ui-corner-all">Noņemt manuālās cenas</button>
              <?php } ?>

              <?php if(check_access('nozares-labot')) { ?>
                <button onclick="deleteAv(<?php echo $av->id;?>); return false;" class="ui-state-default ui-corner-all">Dzēst</button>
              <?php } ?>

              <?php if(is_admin() && 0) { ?>
                <a href="?c=nozares&a=test_cpc&id=<?php echo $av->id;?>" target="_blank">test idea service cpc</a>
              <?php } ?>
            </td>
          </tr>
        <?php
        $i++;
        } ?>
      </tbody>
  </table>
  <?php } ?>

   <div class="hidethistoo">
    <?php if(check_access('nozares-labot')) { ?>
      <input type="submit" onClick="return saveAll(this, 'nozare')" class="submit show_in_act_panel" value="Saglabāt">
    <?php } ?>
    <input type="button" onclick="return cancelSaves();" class="show_in_act_panel" value="Atcelt">
    <?php if($id && check_access('nozares-labot')) { ?>
      <input type="button" onclick="return newAv();" class="show_in_act_panel" value="Pievienot jaunu a.v.">
      <input type="button" onclick="return deleteNozare();" class="show_in_act_panel align_left" value="Dzēst nozari">
    <? } ?>
  </div>
</form>

<script type="text/javascript">

  editordisable();

  function deleteAv(id)
  {
    var conf = confirm('Dzēst atslēgvārdu?');
    if(conf) {
      window.location = '?c=nozares&a=dzest_std_av&id=' + id;
    }
  }

  function newAv()
  {
    var forma = $("#atslegvards").clone();
    forma.removeAttr('id');
    forma.dialog({title:'Jauns atslēgvārds'});
  }



  function validateAV(form)
  {
    var error_msgs = '';
    $.post('validate.php?action=std_piedavajums', $(form).serialize(), function(data){
      data = $.parseJSON(data)
      if (data['success']) {
        had_errors = false;
      } else {

        had_errors = true;

        $.each(data['errors'], function(tab_name, errors) {

          $.each(errors, function(field_name, error_msg) {
            $('#' + field_name).addClass('input_has_error');
            if (error_msg != '') {
              error_msgs += "- " + error_msg + "\n";
            }
          });
        });
      }
      if (had_errors) {
        if (error_msgs != '') alert(error_msgs);
        return false;
      } else {
        $(form).unbind('onsubmit').submit()
      }
    });

    return false;
  }

  <?php if($id) {?>
  function deleteNozare()
  {
    var conf = confirm('Dzēst šo nozari?');
    if(conf) {
      window.location = '?c=nozares&a=dzest&id=<?php echo $id;?>'
    }

    return false;
  }
  <?php } ?>

  <?php if(empty($id)) { ?>
    editoradd($('#labotdzestbutton'), <?= is_admin() ? 1 : 0 ?>, <?= is_limited() ? 1 : 0 ?>);
  <?php } ?>
</script>