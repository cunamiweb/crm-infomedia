<?php
if(!check_access('nozares-labot')) {
  die();
}

$id = getGet('id');

if($id) {
  $nozare = Nozare::model()->findByPk($id);

  //  Var dzēst tikai tad ja nav apakšnozaru un atslēgvārdu (līgumos nevis standarta piedāvājums)
  $apaksnozares = $nozare->getApaksnozares();
  $atslegvardi = $nozare->getAtslegvardi();

  if(empty($apaksnozares) && empty($atslegvardi)) {

    $log_data = array();
    $log_data['nozare']['old'] = $nozare->asArray();
    $log_data['nozare']['title'] = 'Izdzēsa nozari - ' . $nozare->title_lv;

    $nozare->disabled = 1;
    //if($nozare->delete()) {
    if($nozare->save()) {
      log_add("laboja", $log_data);
      header('Location: ?c=nozares');
    } else {
      die('Nozari neizdevās izdzēst');
    }
  } else {
    die('Nevar dzēst, jo nozarei ir apakšnozares vai piesaistīti atslēgvārdi.');
  }
}
?>