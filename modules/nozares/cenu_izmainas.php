<?php
if(!check_access('nozares-cenu-izmainas')) {
  die();
}

$subtab = isset($_GET['subtab']) && in_array($_GET['subtab'], array('top', 'spec', 'std', 'auto')) ? $_GET['subtab'] : 'std';


if(getGet('id')) {
  $av = Atslegvards::model()->findByPk(getGet('id'), true);
  $subtab = 'single';
} else {
  $av = null;
}
?>

<?php if(!$av) { ?>
  <div class="sub-menu">
    <span>
        <a href="?c=nozares&a=cenu_izmainas&subtab=std" <?php echo $subtab == 'std' ? 'class="active"' : '';?>>Standarta</a> |
        <a href="?c=nozares&a=cenu_izmainas&subtab=top" <?php echo $subtab == 'top' ? 'class="active"' : '';?>>Top</a> |
        <a href="?c=nozares&a=cenu_izmainas&subtab=spec" <?php echo $subtab == 'spec' ? 'class="active"' : '';?>>Speciālās</a> |
        <a href="?c=nozares&a=cenu_izmainas&subtab=auto" <?php echo $subtab == 'auto' ? 'class="active"' : '';?>>Automātiskās</a>
    </span>
  </div>
<?php } else { ?>
  <h3><?php echo $av->atslegvards;?></h3>
<?php } ?>

<!-- Prototypes -->
<div style="display:none">

  <!-- Mainīt cenas -->
  <div id="change_prices">
    <form method="POST" action="">
      <table class="data_form layout1">
        <tr>
          <td class="label" width="110">Standarta cena</td>
          <td><input type="checkbox" name="enable_override" class="enable_override" onchange="$(this).parent().parent().find('.standartapiedavajums_override').toggleDisabled()" /></td>
          <td><input type="text" name="StandartaPiedavajums[override]" class="standartapiedavajums_override" value="" disabled="disabled"></td>
        </tr>
        <tr>
          <td class="label" width="110">Spec cena</td>
          <td><input type="checkbox" name="enable_override_s" class="enable_override_s" onchange="$(this).parent().parent().find('.standartapiedavajums_override_s').toggleDisabled()" /></td>
          <td><input type="text" name="StandartaPiedavajums[override_s]" class="standartapiedavajums_override_s" value="" disabled="disabled"></td>
        </tr>
        <tr>
          <td class="label" width="110">Top cena</td>
          <td><input type="checkbox" name="enable_override_t" class="enable_override_t" onchange="$(this).parent().parent().find('.standartapiedavajums_override_t').toggleDisabled()" /></td>
          <td><input type="text" name="StandartaPiedavajums[override_t]" class="standartapiedavajums_override_t" value="" disabled="disabled"></td>
        </tr>
      </table>
      <input type="submit" value="Mainīt" />
    </form>
  </div>

</div>

<?php

//  Select
$sql = 'SELECT l.atslegvards, l.datums, l.mekletajs_id, a.meklejumi, a.id as av_id, a.override, a.override_s, a.override_t';

if($subtab == 'top') {
  $sql .= ', pirms_t, pec_t';

} elseif($subtab == 'spec') {
  $sql .= ', pirms_s, pec_s';

} elseif($subtab == 'std') {
  $sql .= ', pirms, pec';

} else {
  $sql .= ', pirms, pec, pirms_s, pec_s, pirms_t, pec_t';
}

$sql .= ' FROM ' . DB_PREF . 'piedavajumi_cenas_log l';

$sql .= ' LEFT JOIN '.DB_PREF.'piedavajumi_av a ON a.atslegvards = l.atslegvards AND a.mekletajs_id = l.mekletajs_id AND a.tips = "std"';

//  Where
$conditions = array();

if($subtab == 'top') {
  $conditions[] = 'pirms_t != pec_t';
  $conditions[] = 'izmainas_iemesls = "manual" OR izmainas_iemesls = "config"';

} elseif($subtab == 'spec') {
  $conditions[] = 'pirms_s != pec_s';
  $conditions[] = 'izmainas_iemesls = "manual" OR izmainas_iemesls = "config"';

} elseif($subtab == 'std') {
  $conditions[] = 'pirms != pec';
  $conditions[] = 'izmainas_iemesls = "manual" OR izmainas_iemesls = "config"';

} elseif($subtab == 'auto') {
  $conditions[] = 'pirms != pec OR pirms_t != pec_t OR pirms_s != pec_s';
  $conditions[] = 'izmainas_iemesls = "google"';
} else {
  $conditions[] = 'pirms != pec OR pirms_t != pec_t OR pirms_s != pec_s';
}

if(getGet('fdatums_no')) {
  $conditions[] = 'DATE(l.datums) >= "' . date('Y-m-d', strtotime(getGet('fdatums_no'))) . '"';
}

if(getGet('fdatums_lidz')) {
  $conditions[] = 'DATE(l.datums) <= "' . date('Y-m-d', strtotime(getGet('fdatums_lidz'))) . '"';
}

//  Konkrēta av cenu vēsture
if($av) {
  $conditions[] = 'l.atslegvards = "' . $av->atslegvards . '"';
  $conditions[] = 'l.mekletajs_id = ' . $av->mekletajs_id;
}



if(!empty($conditions)) {
  $sql .= ' WHERE (' . implode(') AND (', $conditions) . ')';
}

//  Pagination
$total_rows = db_num_rows(db_query($sql));
$active_page = getGet('page') ? getGet('page') : 1;
$show_in_page = getGet('show_in_page') ? getGet('show_in_page') : 30;

//  Group by
$sql .= ' GROUP BY l.id';

//  Order
$sql .= ' ORDER BY ';
$dir = getGet('direction') && in_array(strtoupper(getGet('direction')), array('DESC', 'ASC')) ? getGet('direction') : 'DESC';

$allowed_orders = array('meklejumi', 'datums', 'atslegvards');
$order = getGet('order') && in_array(getGet('order'), $allowed_orders) ? getGet('order') : 'datums';

if($order == 'atslegvards') {
  $sql .= 'l.atslegvards ' . $dir;
} else {
  $sql .= $order . ' ' . $dir;
}


//  Limit
$offset = ($active_page - 1) * $show_in_page;
$limit = $show_in_page;

$sql .= ' LIMIT ' . $offset . ', ' . $limit;

$result = db_get_assoc_all(db_query($sql));

$mekletaji = Mekletajs::model()->findAll();

/*$set = array(); //  Cenu salīdzināšanai
$data = array(); // Izmaiņu dati

$sql = '
  SELECT *
  FROM ' . DB_PREF . 'piedavajumi_av_cpc_vesture h
  WHERE
    h.statuss = 1
  ORDER BY h.datums DESC
';

$cpc_history = db_get_assoc_all(db_query($sql));

foreach($cpc_history as $log) {
  if(isset($set[$log['mekletajs_id']][$log['atslegvards']])) {
    // Ir jau listē - salīdzinam vai mainījies cena
    if(bccomp($set[$log['mekletajs_id']][$log['atslegvards']], $log['cpc'], 4) != 0) {

      //  Atšķiras

      $stds = StandartaPiedavajums::model()->findAll(array('conditions' => array('atslegvards = "'.$log['atslegvards'].'"')));

      $data[] = array(
        'atslegvards'     => $log['atslegvards'],
        'mekletajs_id'    => $log['mekletajs_id'],
        'jauna'           => $set[$log['mekletajs_id']][$log['atslegvards']],
        'veca'            => $log['cpc'],
        'datums'          => $log['datums'],
        'stds'            => $stds,
      );
    }
  }

  $set[$log['mekletajs_id']][$log['atslegvards']] = $log['cpc'];
} */



?>

<button id="labotdzestbutton" onClick="return editoradd(this, <?php echo is_admin() ? 1 : 0 ?>, <?php echo is_limited() ? $_SESSION['user']['ierobezots'] : 0 ?>)">Labot</button>

<form action="" id="fullformplace" method="GET" style="clear: both;">

  <input type="hidden" name="c" class="show_in_act_panel" value="nozares" />
  <input type="hidden" name="a" class="show_in_act_panel" value="cenu_izmainas" />
  <input type="hidden" name="subtab" class="show_in_act_panel" value="<?php echo $subtab;?>" />

  <input type="hidden" name="order" class="show_in_act_panel" value="<?= getGet('order') ?>" />
  <input type="hidden" name="direction" class="show_in_act_panel" value="<?= getGet('direction') ?>" />

  <input type="hidden" name="page" class="show_in_act_panel" value="<?= $active_page ?>" />
  <input type="hidden" name="show_in_page" class="show_in_act_panel" value="<?= $show_in_page ?>" />

  <div>
    <?php print_paginator($total_rows, $active_page, $show_in_page); ?>
  </div>

  <table cellpadding="3" cellspacing="0" id="atslegvardi_list" class="data ckey_pieteikumi_list" style="width: auto;">
    <thead>
      <tr class="header">
          <?php if(!$av) { ?>
            <?php print_th('Atslēgvards', 'atslegvards', array('width' => 200, 'rowspan' => 2)); ?>

            <th class="header" width="200" rowspan="2">Nozares</th>
            <th class="header" width="70" rowspan="2">Meklētājs</th>
            <?php print_th('Meklēšanas skaits', 'meklejumi', array('width' => 100, 'rowspan' => 2));?>

          <?php } ?>

          <?php if($subtab == 'std' || $subtab == 'auto' || $subtab == 'single') { ?>
            <th class="header" width="170" colspan="2">Standarta cena</th>
          <?php } ?>

          <?php if($subtab == 'spec' || $subtab == 'auto' || $subtab == 'single') { ?>
            <th class="header" width="170" colspan="2">Speciālā cena</th>
          <?php } ?>

          <?php if($subtab == 'top' || $subtab == 'auto' || $subtab == 'single') { ?>
            <th class="header" width="170" colspan="2">Top cena</th>
          <?php } ?>

          <?php print_th('Datums', 'datums');?>

          <th class="hidethistoo" rowspan="2">
            Darbības
          </th>
      </tr>
      <tr class="filter">
        <th class="c">Pirms</th>
        <th class="c">Pēc</th>

        <?php if($subtab == 'auto' || $subtab == 'single') { ?>
          <th class="c">Pirms</th>
          <th class="c">Pēc</th>

          <th class="c">Pirms</th>
          <th class="c">Pēc</th>


        <?php } ?>

        <th>
          <span><input name="fdatums_no" class="sfield kalendari" type="input" value="<?= getGet('fdatums_no') ?>" style="width:80%;"><a class="sfield_clear" href="#">x</a></span><br>
          <span><input name="fdatums_lidz" class="sfield kalendari" type="input" value="<?= getGet('fdatums_lidz') ?>" style="width:80%;"><a class="sfield_clear" href="#">x</a></span>
        </th>

      </tr>

    </thead>
    <tbody class="main">
      <?php
      if(!empty($result)) {
        $i = 1;
        foreach($result as $row) {

          //  Nosakam kura tieši ir mainījusies neskaitot tad ja ir neīstais tab's
          $changed = array();

          if(in_array($subtab, array('std', 'auto', 'single')) &&  $row['pirms'] != $row['pec']) {
            $changed[] = 'std';
          }

          if(in_array($subtab, array('top', 'auto', 'single')) &&  $row['pirms_t'] != $row['pec_t']) {
            $changed[] = 't';
          }

          if(in_array($subtab, array('spec', 'auto', 'single')) &&  $row['pirms_s'] != $row['pec_s']) {
            $changed[] = 's';
          }

          if(!empty($changed)) {
            $mekletajs = isset($mekletaji[$row['mekletajs_id']]) ? $mekletaji[$row['mekletajs_id']] : null;

            //  Nozares
            $stds = StandartaPiedavajums::model()->findAll(array('conditions' => array('atslegvards = "' . $row['atslegvards'].'" AND mekletajs_id = '. $row['mekletajs_id'])));
            ?>

            <tr <?php echo $i % 2 ? 'class="odd"' : '';?>>
              <?php if(!$av) { ?>
                <td><strong><a href="?c=nozares&a=cenu_izmainas&id=<?php echo $row['av_id'];?>"><?php echo $row['atslegvards'];?></a></strong></td>

                <td class="c">
                  <?php if(!empty($stds)) {
                    foreach($stds as $std) { ?>
                    <a href="?c=nozares&a=labot&id=<?php echo $std->nozare_id;?>"><?php echo $std->getNozare()->title_lv;?></a><br />
                  <?php }
                  } ?>
                </td>
                <td class="c"><?php echo $mekletajs ? $mekletajs->nosaukums : 'N/A'; ?></td>
                <td class="r"><?php echo $row['meklejumi'];?></td>
              <?php } ?>
              <?php if($subtab == 'std' || $subtab == 'auto' || $subtab == 'single') { ?>
                <td class="r" <?php echo in_array('std', $changed) ? 'style="font-weight: bold;"' : ''; ?>><?php echo $row['pirms'];?></td>
                <td class="r" <?php echo in_array('std', $changed) ? 'style="font-weight: bold;"' : ''; ?>><?php echo $row['pec'];?></td>
              <?php } ?>

              <?php if($subtab == 'spec' || $subtab == 'auto' || $subtab == 'single') { ?>
                <td class="r" <?php echo in_array('s', $changed) ? 'style="font-weight: bold;"' : ''; ?>><?php echo $row['pirms_s'];?></td>
                <td class="r" <?php echo in_array('s', $changed) ? 'style="font-weight: bold;"' : ''; ?>><?php echo $row['pec_s'];?></td>
              <?php } ?>

              <?php if($subtab == 'top' || $subtab == 'auto' || $subtab == 'single') { ?>
                <td class="r" <?php echo in_array('t', $changed) ? 'style="font-weight: bold;"' : ''; ?>><?php echo $row['pirms_t'];?></td>
                <td class="r" <?php echo in_array('t', $changed) ? 'style="font-weight: bold;"' : ''; ?>><?php echo $row['pec_t'];?></td>
              <?php } ?>

              <td><?php echo date('d.m.Y', strtotime($row['datums']));?></td>

              <td class="hidethistoo">
                <button onclick="changePrices(this, <?php echo $row['av_id'];?>, <?php echo $row['override'] !== null ? 1 : 0;?>,  <?php echo $row['override_s'] !== null ? 1 : 0;?>, <?php echo $row['override_t'] !== null ? 1 : 0;?>); return false;" class="ui-state-default ui-corner-all">Mainit cenas</button>
              </td>
            </tr>

            <?php
            $i++;
          }
        }
      } else { ?>
        <tr><td colspan="11">Nav bijusi neviena cenu izmaiņa.</td></tr>
      <?php
      } ?>
    </tbody>
  </table>
  <div>
    <?php print_paginator($total_rows, $active_page, $show_in_page); ?>
  </div>

  <div class="hidethistoo">
    <input type="button" onclick="return cancelSaves();" class="show_in_act_panel" value="Atcelt">
  </div>

</form>
<script type="text/javascript">
  editordisable();
  initPlainSortableTable($('#fullformplace'));
</script>