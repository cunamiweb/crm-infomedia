<?php
if(!check_access('nozares-saraksts')) {
  die();
}

//  Country
$country_id = getGet('country') ? getGet('country') : 1;

$valsts = Valsts::model()->findByPk($country_id);
if(!$valsts) {
  throw new Exception('No country with ID = ' . $country_id);
}

if(getGet('search')) {
  $avs = StandartaPiedavajums::model()->findAll(array('conditions' => array('atslegvards LIKE "%'.getGet('search').'%"')));

  $nozare_ids = array();
  foreach((array)$avs as $av) {
    $nozare_ids[] = $av->nozare_id;
    $nozare_ids[] = $av->getNozare()->getVirsnozare()->id;
  }
}

$conditions = array(
    'parent_id = 0',
    'country = ' . $valsts->id,
);
if(isset($nozare_ids) && !empty($nozare_ids)) {
  $conditions[] = 'id IN ('.implode(',', $nozare_ids).')';
}
$parents = Nozare::model()->findAll(array(
  'conditions' => $conditions,
  'order' => 'title_lv',
));

$conditions = array(
  'parent_id != 0',
  'country = ' . $valsts->id,
);
if(isset($nozare_ids) && !empty($nozare_ids)) {
  $conditions[] = 'id IN ('.implode(',', $nozare_ids).')';
}
$unordered_children = Nozare::model()->findAll(array(
  'conditions' => $conditions,
  'order' => 'title_lv'
));

$children = array();

foreach($unordered_children as $child) {
  if(!isset($children[$child->parent_id])) {
    $children[$child->parent_id] = array($child);
  } else {
    $children[$child->parent_id][]= $child;
  }
}

$nozares_in_bold = array();
$rows = db_get_assoc_all(db_query('SELECT nozare_id FROM ' . DB_PREF . 'piedavajumi_av WHERE tips = "std"')); // ātrdarbības dēļ
foreach($rows as $std) {
  if(isset($unordered_children[$std['nozare_id']])) {
    $nozares_in_bold[] = $unordered_children[$std['nozare_id']]->id;
    $nozares_in_bold[] = $unordered_children[$std['nozare_id']]->parent_id;
  }
}

?>

<div class="sub-menu">
  <span>
      <a href="?c=nozares&country=1" <?php echo $valsts->id == 1 ? 'class="active"' : '';?>>Latvija</a> |
      <a href="?c=nozares&country=2" <?php echo $valsts->id == 2 ? 'class="active"' : '';?>>Lietuva</a> |
      <a href="?c=nozares&country=3" <?php echo $valsts->id == 3 ? 'class="active"' : '';?>>Igaunija</a> |
      <a href="?c=nozares&country=4" <?php echo $valsts->id == 4 ? 'class="active"' : '';?>>Krievija</a> |
			<a href="?c=nozares&country=5" <?php echo $valsts->id == 5 ? 'class="active"' : '';?>>Baltkrievija</a>
  </span>
</div>

<span>
  <?php if(check_access('nozares-labot')) { ?>
    &nbsp;
    <button style="float: right;" onclick="document.location='?c=nozares&a=jauns&country=<?php echo $valsts->id;?>'; return false;" class="ui-state-default ui-corner-all">Pievienot jaunu nozari</button>
  <?php } ?>
</span>


<div id="nozares_list" style="width: 350px; text-align: left;">
<form method="GET">
  <input type="hidden" name="c" value="nozares" />
  <input type="hidden" name="country" value="<?php echo $valsts->id;?>" />
  <input type="text" style="width: 100px;" name="search" /> <input type="submit" value="Meklēt atslēgvārdu">
</form>

<?php
if(getGet('search') && empty($avs)) {
  die('Netika atrasts neviens atslēgvārds');
}
?>
<?php foreach($parents as $parent) { ?>
  <h2 style="font-size: 1.2em;cursor:pointer; font-weight: <?php echo in_array($parent->id, $nozares_in_bold) ? 'bold' : 'normal';?>" onclick="$(this).next().toggle();"><?php echo $parent->title_lv;?></h2>
  <div style="display:none" class="maincat">
    <?php if(isset($children[$parent->id])) { ?>
      <ul>
      <?php foreach((array)$children[$parent->id] as $child) { ?>
        <li id="subcat_<?php echo $child->id;?>"><a href="?c=nozares&a=labot&id=<?php echo $child->id;?>" target="_blank" style="text-decoration: none; color: #000; font-weight: <?php echo in_array($child->id, $nozares_in_bold) ? 'bold' : 'normal';?>"><?php echo $child->title_lv;?></a></li>
      <?php } ?>
      </ul>
    <?php } ?>
  </div>
<?php } ?>
</div>

<?php if(isset($avs) && !empty($avs)) { ?>
  <table cellpadding="3" cellspacing="0" id="atslegvardi_list" class="data ckey_pieteikumi_list" style="width: auto;">
      <thead>
        <tr class="header">
            <?php print_th('Atslēgas vārds', 'atslegvards');?>
            <?php print_th('Meklēšanas skaits', 'meklejumi');?>
            <th class="header">Google klikšķa cena</th>
            <th class="header">Standarta cena</th>
            <th class="header">Speciālā cena</th>
            <th class="header">Top cena</th>
            <th class="hidethistoo" width="120" class="header">Darbības</th>
        </tr>
      </thead>
      <tbody class="main">
        <?php
        $i = 1;
        $currency = 'EUR';

        // lai izvairītos no vienādiem
        $already_output = array();

        foreach((array)$avs as $av) {
          if(!in_array($av->atslegvards, $already_output)) {?>
            <tr <?php echo $i%2 ? 'class="odd"' : '';?>>
              <td class="av"><a href="?c=nozares&a=cenu_izmainas&id=<?php echo $av->id;?>"><?php echo $av->atslegvards;?></a></td>
              <td class="r"><?php echo $av->meklejumi;?></td>
              <td class="r">
              <?php
                /*if($currency != 'EUR') {
                  echo convert_currency($av->google_cpc, 'EUR', $currency);
                } else {     */
                  echo $av->google_cpc . '$';
                //}
              ?>
              </td>
              <td class="r cena">
                <?php if($av->override) { ?>
                  <span class="bez_pvn_rek_ico" style="margin-top: 2px;">!</span>
                <?php } ?>
                <span class="value"><?php echo format_currency($av->cena($currency));?></span>
              </td>
              <td class="r cena_s">
                <?php if($av->override_s) { ?>
                  <span class="bez_pvn_rek_ico" style="margin-top: 2px;">!</span>
                <?php } ?>
                <span class="value"><?php echo format_currency($av->cena_S($currency));?></span>
              </td>
              <td class="r cena_t">
                <?php if($av->override_t) { ?>
                  <span class="bez_pvn_rek_ico" style="margin-top: 2px;">!</span>
                <?php } ?>
                <span class="value"><?php echo format_currency($av->cena_T($currency));?></span>
              </td>
              <td class="hidethistoo">
                <button onclick="changePrices(this, <?php echo $av->id;?>, <?php echo $av->override !== null ? 1 : 0;?>,  <?php echo $av->override_s !== null ? 1 : 0;?>, <?php echo $av->override_t !== null ? 1 : 0;?>); return false;" class="ui-state-default ui-corner-all">Mainit cenas</button>
                <?php if(check_access('nozares-atjauninasana')) { ?>
                  <button onclick="window.location = '?c=nozares&a=update_std&id=<?php echo $av->id;?>&search=<?php echo getRequest('search');?>&country=<?php echo getRequest('country');?>'; return false;" class="ui-state-default ui-corner-all">Atjaunināt</button>
                <?php } ?>
              </td>
            </tr>
            <?php
            $i++;
            $already_output[] = $av->atslegvards;
          }
        } ?>
      </tbody>
  </table>
<?php } ?>

<script type="text/javascript">
/*function searchAv()
{
  var av = $('input[name=search]').val();
  if(av) {
    $.getJSON('ajax.php?action=search_std_av', {avstring:av}, function(data){
      if(data[0].length > 0) {
        $("#nozares_list h2, #nozares_list li").hide();

        for(i = 0; i < data[0].length; i++) {
          $('#subcat_' + data[0][i]).show().closest('.maincat').show().prev('h2').show();
        }

        $(data[1]).insertAfter('#nozares_list');
        $("#change_prices input[name=meklet]").val(av);
      } else {
        alert('Nav atrasts neviens standarta atslēgvārds');
      }
    });
  }
}   */

<?php if(isset($avs)) { ?>
  $(document).ready(function(){
    $('.maincat').show();
  });
<?php } ?>
</script>

<!-- Prototypes -->
<div style="display:none;">


  <!-- Mainīt cenas -->
  <div id="change_prices">
    <form method="POST" action="">
      <table class="data_form layout1">
        <tr>
          <td class="label" width="110">Standarta cena</td>
          <td><input type="checkbox" name="enable_override" class="enable_override" onchange="$(this).parent().parent().find('.standartapiedavajums_override').toggleDisabled()" /></td>
          <td><input type="text" name="StandartaPiedavajums[override]" class="standartapiedavajums_override" value="" disabled="disabled"></td>
        </tr>
        <tr>
          <td class="label" width="110">Spec cena</td>
          <td><input type="checkbox" name="enable_override_s" class="enable_override_s" onchange="$(this).parent().parent().find('.standartapiedavajums_override_s').toggleDisabled()" /></td>
          <td><input type="text" name="StandartaPiedavajums[override_s]" class="standartapiedavajums_override_s" value="" disabled="disabled"></td>
        </tr>
        <tr>
          <td class="label" width="110">Top cena</td>
          <td><input type="checkbox" name="enable_override_t" class="enable_override_t" onchange="$(this).parent().parent().find('.standartapiedavajums_override_t').toggleDisabled()" /></td>
          <td><input type="text" name="StandartaPiedavajums[override_t]" class="standartapiedavajums_override_t" value="" disabled="disabled"></td>
        </tr>
      </table>
      <input type="submit" value="Mainīt" />
    </form>
  </div>

</div>


