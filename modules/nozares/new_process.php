<?php
if(check_access('nozares-labot')) {

  $id = getGet('id');

  if(isset($_POST['StandartaPiedavajums']) && $id) {

    $nozare = Nozare::model()->findByPk($id);

    $atslegvards = new StandartaPiedavajums;
    $atslegvards->setValues($_POST['StandartaPiedavajums']);
    //$atslegvards->izmaina = str_replace(',', '.', $_POST['StandartaPiedavajums']['izmaina']);
    $atslegvards->nozare_id = $nozare->id;
    $atslegvards->tips = 'std';

    if($atslegvards->save()) {

      header('Location: ?c=nozares&a=update_std&id='.$atslegvards->id);
    } else {
      die('Atslēgvārdu neizdevās saglabāt');
    }
  }

  if(isset($_POST['Nozare'])) {

    if(empty($errors)) {

      $log_data = array();

      if($id) {
        //  Labo
        $nozare = Nozare::model()->findByPk($id);

        $log_data['nozare']['old'] = $nozare->asArray();

        $nozare->setValues($_POST['Nozare']);
        if($nozare->save()) {
          $log_data['nozare']['new'] = $nozare->asArray();
          $log_data['nozare']['title'] = 'Laboja nozari';
          log_add("laboja", $log_data);

          if(!$nozare->parent_id) {
            header('Location: ?c=nozares');
          }
        } else {
          die('Neizdevās saglabāt');
        }
      } else {
        //  Jauns
        $nozare = new Nozare;

        $log_data['nozare']['old'] = array();

        $nozare->setValues($_POST['Nozare']);

        if($nozare->save()) {
          $log_data['nozare']['new'] = $nozare->asArray();
          $log_data['nozare']['title'] = 'Pievienoja jaunu nozari';
          log_add("laboja", $log_data);

          if(!$nozare->parent_id) {
            header('Location: ?c=nozares&country=' . $nozare->country);
          } else {
            header('Location: ?c=nozares&a=labot&id=' . $nozare->id);
          }

        } else {
          die('Neizdevās saglabāt');
        }
      }
    }
  }
}
?>