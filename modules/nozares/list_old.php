<?php
if(!is_admin()) {
  die();
}

//  Country
$country_id = getGet('country') ? getGet('country') : 1;

$valsts = Valsts::model()->findByPk($country_id);
if(!$valsts) {
  throw new Exception('No country with ID = ' . $country_id);
}

//  Conditions
$conditions = array();

$conditions[] = 'country = ' . $valsts->id;

//  Tikai lielās
$conditions[] = 'parent_id = 0';

$nozares = Nozare::model()->findAll(array(
  'conditions' => $conditions,
));

$total_rows = count($nozares);
$active_page = getGet('page') ? getGet('page') : 1;
$show_in_page = getGet('show_in_page') ? getGet('show_in_page') : 30;

$nozares = Nozare::model()->findAll(array(
  'conditions' => $conditions,
  'order' => 'title_lv',
  'offset' => ($active_page - 1) * $show_in_page,
  'limit' => $show_in_page,
));

?>

<div class="sub-menu">
  <span>
      <a href="?c=nozares&country=1" <?php echo $valsts->id == 1 ? 'class="active"' : '';?>>Latvija</a> |
      <a href="?c=nozares&country=2" <?php echo $valsts->id == 2 ? 'class="active"' : '';?>>Lietuva</a> |
      <a href="?c=nozares&country=3" <?php echo $valsts->id == 3 ? 'class="active"' : '';?>>Igaunija</a>
  </span>
</div>

<form action="" id="fullformplace" method="GET" style="clear: both;">

  <input type="hidden" name="c" value="nozares" />

  <input type="hidden" name="order" value="<?= getGet('order') ?>" />
  <input type="hidden" name="direction" value="<?= getGet('direction') ?>" />

  <input type="hidden" name="page" value="<?= $active_page ?>" />
  <input type="hidden" name="show_in_page" value="<?= $show_in_page ?>" />

  <div>
    <?php print_paginator($total_rows, $active_page, $show_in_page); ?>
    <span>

      <?php if(is_admin()) { ?>
        &nbsp;
        <button style="float: right;" onclick="document.location='?c=nozares&a=jauns&country=<?php echo $valsts->id;?>'; return false;" class="ui-state-default ui-corner-all">Pievienot jaunu nozari</button>
      <?php } ?>
    </span>
  </div>

  <table cellpadding="3" cellspacing="0" id="nozares_kopsavilkums" class="data ckey_nozares_list" style="width: auto;">

    <thead>

      <tr class="header">
          <th width="400" class="header" colname="nosaukums">Nosaukums</th>
          <th width="150" class="header">Darbības</th>
      </tr>

    </thead>

    <tbody class="main">
      <?php
        foreach((array)$nozares as $nozare) {
          $apaksnozares = $nozare->getApaksnozares();
          ?>
          <tr title="<?php echo $nozare->title_lv;?>">
            <td style="font-weight: bold;"><?php echo $nozare->title_lv;?></td>
            <td>

              <?php if(empty($apaksnozares)) { ?>
                <button onclick="deleteNozare(<?php echo $nozare->id;?>); return false;" class="ui-state-default ui-corner-all">Dzēst</button>
              <?php } else {?>
                <a href="#" onclick="$('.children_of_<?php echo $nozare->id;?>').toggle(); return false;">Apakšnozares (<?php echo count($apaksnozares);?>)</a>
              <?php }?>
            </td>
          </tr>
        <?php
          if(!empty($apaksnozares)) {
            foreach($apaksnozares as $apaksnozare) { ?>
              <tr style="display: none;" class="children_of_<?php echo $nozare->id;?>" title="<?php echo $apaksnozare->title_lv;?>">
                <td><?php echo $apaksnozare->title_lv;?></td>
                <td>
                  <button onclick="document.location='?c=nozares&a=labot&id=<?php echo $apaksnozare->id;?>'; return false;" class="ui-state-default ui-corner-all">Apskatīt</button>
                </td>
              </tr>
            <?php }
          }
        } ?>
    </tbody>
    </table>
</form>
<script type="text/javascript">
function deleteNozare(id)
{
  var conf = confirm('Dzēst nozari?');
  if(conf) {
    window.location = '?c=nozares&a=dzest&id=' + id;
  } else {
    return false;
  }
}
</script>