<?php
if(!check_access('nozares-atjauninasana')) {
  die();
}

$id = getGet('id');
if($id) {
  $model = StandartaPiedavajums::model()->findByPk($id);

  $last_prices = $model->priceArray();

  //  Mēneša meklējumi
  $model->updateSearchCount();

  //  CPC
  $model->updateCPC();

  //  Sinhronizē ar pārējiem
  $model->synchronize(array('google_cpc', 'google_clicks', 'meklejumi'));

  //  Ielogo cenu izmaiņu
  $model->logPriceChange('google', $last_prices);

  if(isset($_GET['search'], $_GET['country'])) {
    //  Atgriežas uz av meklēšanas rezultātiem
    header('Location: ?c=nozares&country=' . $_GET['country'] . '&search=' . $_GET['search']);
  } else {
    //  Atgriežas uz nozares skatu
    header('Location: ?c=nozares&a=labot&id=' . $model->nozare_id);
  }
}

?>