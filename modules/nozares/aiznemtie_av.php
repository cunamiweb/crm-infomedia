<?php
if(!check_access('nozares-aiznemtie-av')) {
  die();
}

$order = getGet('order') && in_array(getGet('order'), array('atslegvards', 'count')) ? getGet('order') : 'atslegvards';
$dir = getGet('direction') && in_array(getGet('direction'), array('desc', 'asc')) ? getGet('direction') : 'asc';

$subtab = getGet('subtab') ? getGet('subtab') : '3poz';

$limited = isset($_GET['limited']) && $_GET['limited'];
?>

<div class="sub-menu">
  <span>
      <a href="?c=nozares&a=aiznemtie_av&subtab=3poz<?php echo $limited ? '&limited=1' : '';?>" <?php echo $subtab == '3poz' ? 'class="active"' : '';?>>3 pozīcijas</a> |
      <a href="?c=nozares&a=aiznemtie_av&subtab=top3<?php echo $limited ? '&limited=1' : '';?>" <?php echo $subtab == 'top3' ? 'class="active"' : '';?>>Top 3</a>
  </span>
</div>

<?php
/**
 * Sakarā ar komplicēto līgumu statusa funkciju, sākumā ar vienu kveriju atlasām
 * visus av kuriem izpildās skaita nosacījums pie vienkāršotiem līguma aktivitātes
 * nosacījumiem, un pēc tam jau rindojot vēlreiz katra av katram līgumam pārbaudam statusu
 */
$valstis = Valsts::model()->findAll();

if($subtab == '3poz') {

  $date = date('Y-m-d');

  $sqltmp = array();

  foreach($valstis as $valsts) {
    $sqltmp[] = '
      SELECT g.atslegvards, g.domeni as valsts, COUNT(l.id) as count, GROUP_CONCAT(l.id) as lids
      FROM '.DB_PREF.'pielikumi_tiessaite g
      LEFT JOIN '.DB_PREF.'ligumi l ON g.liguma_id = l.id
      WHERE
        DATE(l.reklama_no) <= "'.$date.'"
        AND DATE(l.reklama_lidz) >= "'.$date.'"
        AND g.domeni = "'.$valsts->country_code.'"
      GROUP BY g.atslegvards
      HAVING count >= 3
    ';
  }

  $sql = '(' . implode(') UNION (', $sqltmp) . ')';

  $sql .= ' ORDER BY ' . $order . ' ' . $dir;

  $rows = db_get_assoc_all(db_query($sql));


  //  Post process
  $data = array();
  foreach($rows as $row) {
    $qualifies = true;

    $row['count'] = (int)$row['count'];
    $legit_ids = array();


    //  Pārbaudam infomedia pakalpojumu
    if($row['valsts'] == 'lv') {
      $infomedia = db_get_assoc_all(db_query('
        SELECT g.atslegvards, l.id as lid
        FROM '.DB_PREF.'pielikumi_infomedia g
        LEFT JOIN '.DB_PREF.'ligumi l ON g.liguma_id = l.id
        WHERE
          DATE(l.reklama_no) <= "'.$date.'"
          AND DATE(l.reklama_lidz) >= "'.$date.'"
          AND g.atslegvards = "'.$row['atslegvards'].'"
      '));

      $has_infomedia = false;
      foreach((array)$infomedia as $irow) {
        if(get_liguma_status_by_id($irow['lid']) == 3) {
          $has_infomedia = true;
          $legit_ids[] = $irow['lid'];
        }
      }

      if($has_infomedia) {
        $row['count'] += 1; //  Infomedijas atslēgvārdi, vienalga cik daudz viņu ir, palielina skaitu visi kopā tikai par 1.
      }
    }

    //  Ejam cauri atrastajiem google pakalpojumiem un pārbaudam vai līgumi aktīvi
    $ligumi_ids = explode(',', $row['lids']);
    foreach($ligumi_ids as $ligums_id) {
      if(get_liguma_status_by_id($ligums_id) != 3) {
        $row['count'] -= 1;
        if($row['count'] < 6) {
          $qualifies = false;
          break;
        }
      } else {
        $legit_ids[] = $ligums_id;
      }
    }

    if($qualifies) {
      $row['lids'] = $legit_ids;
      $data[] = $row;
    }
  }
} elseif($subtab == 'top3') {

  $date = date('Y-m-d');

  $sqltmp = array();

  foreach($valstis as $valsts) {
    $sqltmp[] = '
      SELECT g.atslegvards, g.domeni as valsts, COUNT(l.id) AS count, GROUP_CONCAT(l.id) AS lids
      FROM '.DB_PREF.'pielikumi_tiessaite g
      LEFT JOIN '.DB_PREF.'ligumi l ON g.liguma_id = l.id
      WHERE
        g.top3 = 1
        AND DATE(l.reklama_no) <= "'.$date.'"
        AND DATE(l.reklama_lidz) >= "'.$date.'"
        AND g.domeni = "'.$valsts->country_code.'"
      GROUP BY g.atslegvards
    ';
  }

  $sql = '(' . implode(') UNION (', $sqltmp) . ')';

  $sql .= ' ORDER BY ' . $order . ' ' . $dir;

  $rows = db_get_assoc_all(db_query($sql));


  //  Post process
  $data = array();
  foreach($rows as $row) {
    $qualifies = true;

    $row['count'] = (int)$row['count'];
    $ligumi_ids = explode(',', $row['lids']);

    $legit_ids = array();
    foreach($ligumi_ids as $ligums_id) {
      if(get_liguma_status_by_id($ligums_id) != 3) {
        $row['count'] -= 1;
        if($row['count'] < 1) {
          $qualifies = false;
          break;
        }
      } else {
        $legit_ids[] = $ligums_id;
      }
    }

    if($qualifies) {
      $row['lids'] = $legit_ids;
      $data[] = $row;
    }
  }
}

// Ordering
/*usort($data, 'aiznemtieSort');

function aiznemtieSort($a, $b)
{
  $order = getGet('order') && in_array(getGet('order'), array('atslegvards', 'count')) ? getGet('order') : 'atslegvards';
  $dir = getGet('direction') && in_array(getGet('direction'), array('desc', 'asc')) ? getGet('direction') : 'asc';

  if($order == 'atslegvards') {
    return strcmp($a['atslegvards'], $b['atslegvards']) * ($dir == 'desc' ? -1 : 1);
  } else {

    if($a[$order] == $b[$order]) {
      return 0;
    }

    if($a[$order] > $b[$order]) {
      return $dir == 'desc' ? -1 : 1;
    } else {
      return $dir == 'desc' ? 1 : -1;
    }
  }
}  */

$total_rows = count($data);
$active_page = getGet('page') ? getGet('page') : 1;
//$show_in_page = getGet('show_in_page') ? getGet('show_in_page') : isset($_GET['show_in_page']) ? '' : 30;

$show_in_page = isset($_GET['show_in_page']) ? ($_GET['show_in_page'] ? $_GET['show_in_page'] : '') : 30;

if($show_in_page) {
  $data = array_slice($data, ($active_page-1)*$show_in_page, $show_in_page);
}

?>

<form action="" id="fullformplace" method="GET" style="clear: both;">

  <input type="hidden" name="c" value="nozares" />
  <input type="hidden" name="a" value="aiznemtie_av" />
  <input type="hidden" name="subtab" value="<?php echo $subtab;?>" />

  <input type="hidden" name="order" value="<?= getGet('order') ?>" />
  <input type="hidden" name="direction" value="<?= getGet('direction') ?>" />

  <input type="hidden" name="page" value="<?= $active_page ?>" />
  <input type="hidden" name="show_in_page" value="<?= $show_in_page ?>" />

  <div>
    <?php print_paginator($total_rows, $active_page, $show_in_page); ?>
  </div>

  <table cellpadding="3" cellspacing="0" id="atslegvardi_list" class="data ckey_pieteikumi_list" style="width: auto;">
    <thead>
    <tr class="header">
      <?php print_th('Atslēgvards', 'atslegvards', array('width' => 200)); ?>
      <th>Valsts</th>
      <?php print_th('Pozīcijas', 'count', array('width' => 80)); ?>
    </tr>
    </thead>

    <tbody>
    <?php
    $i = 1;
    foreach($data as $row) { ?>
      <tr  <?php echo $i % 2 ? 'class="odd"' : '';?>>
        <td><strong><?php echo mb_strtolower($row['atslegvards']);?></strong></td>
        <td><?php echo strtoupper($row['valsts']);?></td>
        <td class="c">
          <?php if(is_admin()) { ?>
            <a href="?c=ligumi&fligumu_id=<?php echo implode(',', $row['lids']);?>" target="_blank"><?php echo $row['count'];?></a>
          <?php } else { ?>
            <?php echo $row['count'];?>
          <?php } ?>
        </td>
      </tr>
    <?php
      $i++;
    } ?>
    </tbody>

  </table>
</form>