<?php
if(!check_access('nozares-labot')) {
  die();
}

$id = getGet('id');

if($id) {
  $atslegvards = StandartaPiedavajums::model()->findByPk($id);

  $log_data = array();
  $log_data['std_atslegvards']['old'] = $atslegvards->asArray();
  $log_data['std_atslegvards']['title'] = 'Izdzēsa standarta piedāvājumu - ' . $atslegvards->atslegvards;

  if($atslegvards->delete()) {
    log_add("laboja", $log_data);
    header('Location: ?c=nozares&a=labot&id='.$atslegvards->nozare_id);
  } else {
    die('Atslēgvārdu neizdevās izdzēst');
  }
}
?>