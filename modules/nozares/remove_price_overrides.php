<?php
if(!check_access('nozares-cenu-maina')) {
  die();
}

$id = getGet('id');  
$model = StandartaPiedavajums::model()->findByPk($id);
if($model) {
  $model->override = null;
  $model->override_s = null;
  $model->override_t = null;

  if($model->save()) {
    header('Location: ?c=nozares&a=labot&id=' . $model->nozare_id);
  } else {
    die('Manuālo cenu neizdevās noņemt.');
  }
}
?>