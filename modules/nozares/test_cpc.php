<?php

$id = getGet('id');
if($id) {
  $model = StandartaPiedavajums::model()->findByPk($id);

  $adwords = new Adwords;
  $result = $adwords->getIdeaCPC(array($model->atslegvards), array($model->getMekletajs()->getValsts()->country_code));
  if($result && isset($result[$model->atslegvards])) {
    echo round($result[$model->atslegvards], 2) . '$';
  } else {
    echo 'Nevar noteikt';
  }
}

?>