<?php
$a = empty($_GET['a']) ? "" : $_GET['a'];

if(in_array($a, array('', 'list', 'cenu_izmainas', 'labot', 'aiznemtie_av')) && (!isset($_GET['limited']) || !$_GET['limited'])) { ?>
<div class="sub-menu">
  <span>
    <?php
    $atags = array();

    if(check_access('nozares-saraksts')) {
      $atags[] = '<a href="?c=nozares&a=list" ' . (!$a || $a == 'list' || $a == 'labot' ? 'class="active"' : '') . '>Nozaru saraksts</a>';
    }

    if(check_access('nozares-cenu-izmainas')) {
      $atags[] = '<a href="?c=nozares&a=cenu_izmainas" '. ($a == 'cenu_izmainas' ? 'class="active"' : '') . '>Cenu izmaiņas</a>';
    }

    if(check_access('nozares-aiznemtie-av')) {
      $atags[] = '<a href="?c=nozares&a=aiznemtie_av" '. ($a == 'aiznemtie_av' ? 'class="active"' : '') . '>Aizņemtie a/v</a>';
    }

    echo implode(' | ', $atags);
    ?>
  </span>
</div>
<?php } ?>

<?php

switch($a){

  case "jauns": case 'labot': $include = "new.php";  break;
  case "dzest": $include = "delete.php";  break;
  case "test": $include = "test.php";  break;
  case "update_std": $include = "update_std.php";  break;
  case "test_cpc": $include = "test_cpc.php";  break;
  case "dzest_std_av": $include = "delete_std_av.php";  break;
  case "cenu_izmainas": $include = "cenu_izmainas.php";  break;
  case "aiznemtie_av": $include = "aiznemtie_av.php";  break;
  case "remove_price_overrides": $include = "remove_price_overrides.php";  break;
  default: $include = "list.php";

}

include($include);

?>