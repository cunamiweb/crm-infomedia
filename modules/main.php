<?php
if (!empty($_GET['c'])) {
  $c = strtolower($_GET['c']);
} else {
  $c = check_access('ligumi-saraksts') ? 'ligumi' : 'darbinieki';
}

if (!empty($_GET['a'])) {
  $a = strtolower($_GET['a']);
} else {
  $a = null;
}

$lietotajs = Pardevejs::model()->findByPk($_SESSION['user']['id']);
?>

<?php if (empty($_GET['without_nav'])) { ?>

<center>

  <div class="main-menu-wrap">

    <? if (check_access('ligumi-saraksts')) { ?>

      <form class="top-quick-search" action="" method="get">
          <input type="hidden" name="c" value="ligumi" />
          <input type="text" name="fsearch" value="<?= ($c == 'ligumi') ? getGet('fsearch') : '' ?>" />
          <input style="width: 8em; font-size: 13px;" type="submit" value="Meklēt klientu" />
      </form>

    <? } ?>

    <div style="float:right; width: 350px; text-align: right; font-weight: normal;">
      <span style="margin-right: 20px; font-size: 13px;">Sveiks, <strong><?=$_vars['sys_pardeveji'][$_SESSION['user']['id']];?></strong>!</span>
      <?php if(is_admin()) { ?>
        <a style="font-size: 13px; text-decoration: none;" href="index.php?c=uzstadijumi&a=clearcache" class="clear_cache_link">Iztīrīt kešatmiņu</a>&nbsp;&nbsp;
      <?php } ?>
      <a style="font-size: 13px; text-decoration: none;" href="?logout=true">Izlogoties</a>
    </div>

    <script type="text/javascript">
      $(".clear_cache_link").click(function(e){
        e.preventDefault();
        if(confirm('Vai iztīrīt kešatmiņu? Tiks iztīrīti visi pēdējās 24h iekešotie dati.')) {
          window.location = e.target.href;
        }
      });
    </script>

    <div class="main-menu" <?= !is_admin() ? 'style="text-align: left"' : '' ?>>
      <?php

      $menu = array();

      //  Klienti
      if (check_access('ligumi-saraksts')) {
        $menu[] = '<a href="?c=ligumi&ftikai_ipasie=1"><img src="css/star_small.png" alt="" /></a>
          <a '.(($c == 'ligumi' && (!isset($_GET['a']) || $_GET['a'] != 'nenom_statusi')) ? 'class="active"' : '').' href="?c=ligumi">Klienti</a>';
      }

      //  Darbinieki
      if(check_access('darbinieki-saraksts')) {
        $menu[] = '<a '. ($c == 'darbinieki' && !isset($_GET['a']) ? 'class="active"' : '') .' href="?c=darbinieki">Darbinieki</a>';
      }

      //  Aģentūras
      if(check_access('partneri')) {
        $menu[] = '<a '. ($c == 'partneri'  ? 'class="active"' : '') .' href="?c=partneri">Partneri</a>';
      }

      //  Kopsavilkums (darbinieks)
      if(check_access('darbinieki-savs-kopsavilkums', null, false)) {
        if($lietotajs->tips == 'partner') {
          $menu[] = '<a ' . ($c == 'partneri' && !isset($_GET['a']) ? 'class="active"' : '') . ' href="?c=partneri">Kopsavilkums</a>';
        } else {
          $menu[] = '<a ' . ($c == 'darbinieki' && !isset($_GET['a']) ? 'class="active"' : '') . ' href="?c=darbinieki">Kopsavilkums</a>';
        }
      }


        //  Savi Līgumi
        if(check_access('darbinieki-savi-ligumi', null, false)) {
          if($lietotajs->tips == 'partner') {
            $menu[] = '<a '.(($c == 'partneri' && getGet('a') == 'labot' && getGet('subtab') == 'ligumi') ? 'class="active"' : '') . ' href="?c=partneri&a=labot&id='. $_SESSION['user']['id'] . '&subtab=ligumi">Līgumi</a>';
          } else {
            $menu[] = '<a '.(($c == 'darbinieki' && getGet('a') == 'labot' && getGet('subtab') == 'ligumi') ? 'class="active"' : '') . ' href="?c=darbinieki&a=labot&id='. $_SESSION['user']['id'] . '&subtab=ligumi">Līgumi</a>';
          }
        }

        //  Savi Saistītie līgumi
        if(check_access('darbinieki-savi-ligumi', null, false) && get_saist_lig_skaits($_SESSION['user']['id'])) {
          if($lietotajs->tips == 'partner') {
            $menu[] = '<a ' . (($c == 'partneri' && getGet('a') == 'labot' && getGet('subtab') == 'saistitie_ligumi') ? 'class="active"' : '') . ' href="?c=partneri&a=labot&id='. $_SESSION['user']['id'] .'&subtab=saistitie_ligumi">Saistītie līgumi</a>';
          } else {
            $menu[] = '<a ' . (($c == 'darbinieki' && getGet('a') == 'labot' && getGet('subtab') == 'saistitie_ligumi') ? 'class="active"' : '') . ' href="?c=darbinieki&a=labot&id='. $_SESSION['user']['id'] .'&subtab=saistitie_ligumi">Saistītie līgumi</a>';
          }
        }
                             
        //  Savi Plāna izpilde
        if(check_access('darbinieki-sava-plana-izpilde', null, false)) {

          if($lietotajs->tips == 'partner') {
            $menu[] = '<a '. (($c == 'partneri' && getGet('a') == 'labot' && getGet('subtab') == 'izpilde') ? 'class="active"' : '') . ' href="?c=partneri&a=labot&id='.$_SESSION['user']['id'].'&subtab=' . ($lietotajs->vaditajs ? 'bonuss' : 'izpilde').'">Plāna izpilde</a>';
          } else {
            $menu[] = '<a '. (($c == 'darbinieki' && getGet('a') == 'labot' && getGet('subtab') == 'izpilde') ? 'class="active"' : '') . ' href="?c=darbinieki&a=labot&id='.$_SESSION['user']['id'].'&subtab=' . ($lietotajs->vaditajs ? 'bonuss' : 'izpilde').'">Plāna izpilde</a>';
          }
        }

        //  Savi Statistika
        if(check_access('darbinieki-sava-statistika', null, false)) {
          if($lietotajs->tips == 'partner') {
            $menu[] = '<a '. (($c == 'partneri' && getGet('a') == 'labot' && getGet('subtab') == 'statistika') ? 'class="active"' : ''). ' href="?c=partneri&a=labot&id='.$_SESSION['user']['id'].'&subtab=statistika">Statistika</a>';
          } else {
            $menu[] = '<a '. (($c == 'darbinieki' && getGet('a') == 'labot' && getGet('subtab') == 'statistika') ? 'class="active"' : ''). ' href="?c=darbinieki&a=labot&id='.$_SESSION['user']['id'].'&subtab=statistika">Statistika</a>';
          }
        }

        //  Savi Rēķini
        if(check_access('darbinieki-savi-rekini', null, false)) {
          if($lietotajs->tips == 'partner') {
            $menu[] = '<a '. (($c == 'partneri' && getGet('a') == 'labot' && getGet('subtab') == 'norekini') ? 'class="active"' : ''). ' href="?c=partneri&a=labot&id='.$_SESSION['user']['id'].'&subtab=norekini">Rēķini</a>';
          } else {
            $menu[] = '<a '. (($c == 'darbinieki' && getGet('a') == 'labot' && getGet('subtab') == 'norekini') ? 'class="active"' : ''). ' href="?c=darbinieki&a=labot&id='.$_SESSION['user']['id'].'&subtab=norekini">Rēķini</a>';
          }
        }

        //  Savi Maksājumi
        if(check_access('darbinieki-savi-maksajumi', null, false)) {
          if($lietotajs->tips == 'partner') {
            $menu[] = '<a '. (($c == 'partneri' && getGet('a') == 'labot' && getGet('subtab') == 'maksajumi') ? 'class="active"' : '') . ' href="?c=partneri&a=labot&id='.$_SESSION['user']['id'].'&subtab=maksajumi">Maksājumi</a>';
          } else {
            $menu[] = '<a '. (($c == 'darbinieki' && getGet('a') == 'labot' && getGet('subtab') == 'maksajumi') ? 'class="active"' : '') . ' href="?c=darbinieki&a=labot&id='.$_SESSION['user']['id'].'&subtab=maksajumi">Maksājumi</a>';
          }
        }

        //  Savi Rekvizīti
        if(check_access('darbinieki-savi-rekviziti', null, false)) {
          if($lietotajs->tips == 'partner') {
            $menu[] = '<a '. (($c == 'partneri' && getGet('a') == 'labot' && getGet('subtab') == 'rekviziti') ? 'class="active"' : '') . ' href="?c=partneri&a=labot&id='.$_SESSION['user']['id'].'&subtab=rekviziti">Rekvizīti</a>';
          } else {
            $menu[] = '<a '. (($c == 'darbinieki' && getGet('a') == 'labot' && getGet('subtab') == 'rekviziti') ? 'class="active"' : '') . ' href="?c=darbinieki&a=labot&id='.$_SESSION['user']['id'].'&subtab=rekviziti">Rekvizīti</a>';
          }
        }


        //  Savi Atgādinājumi
        if(check_access('darbinieki-savi-atgadinajumi', null, false)) {
          $akt_atg_skaits = get_active_reminder_count($_SESSION['user']['id']);
          if($lietotajs->tips == 'partner') {
            $menu[] = '<a '. (($c == 'partneri' && getGet('a') == 'labot' && getGet('subtab') == 'atgadinajumi') ? 'class="active"' : '') . ' href="?c=partneri&a=labot&id='.$_SESSION['user']['id'].'&subtab=atgadinajumi">Atgādinājumi '.(!empty($akt_atg_skaits) ? '(' . $akt_atg_skaits . ')' : '') . '</a>';
          } else {
            $menu[] = '<a '. (($c == 'darbinieki' && getGet('a') == 'labot' && getGet('subtab') == 'atgadinajumi') ? 'class="active"' : '') . ' href="?c=darbinieki&a=labot&id='.$_SESSION['user']['id'].'&subtab=atgadinajumi">Atgādinājumi '.(!empty($akt_atg_skaits) ? '(' . $akt_atg_skaits . ')' : '') . '</a>';
          }
        }

      //  Atgādinājumi
      if (check_access('atgadinajumi')) {
        $where = array();
        $sql = "
          SELECT COUNT(*)
          FROM `".DB_PREF."atgadinajumi` a
          LEFT JOIN `".DB_PREF."ligumi` g ON (a.liguma_id = g.id)";

        if(check_access('atgadinajumi-tikai-gramatvediba') && !is_superadmin()) {
          $where[] = '(
              a.`atgadinajuma_veida_id` IN (1, 8, 9 , 10) OR
              a.`pardeveja_id` = ' . $_SESSION['user']['id'] . ' OR
              a.`created_pardeveja_id` = ' . $_SESSION['user']['id'] . '
            )';
        }

        if(check_access('atgadinajumi-vaditajs-tikai-savi') && !is_superadmin()) {
          $padotie = Pardevejs::model()->findAll(array('conditions' => array('vaditajs_id = ' . $_SESSION['user']['id'])));
          $padotie_ids = modelsToList($padotie, 'id');
          $padotie_ids[] = $_SESSION['user']['id'];

          $where[] =  "(a.pardeveja_id IN (".implode(',', $padotie_ids).") OR a.created_pardeveja_id IN (".implode(',', $padotie_ids).") OR g.pardeveja_id IN (".implode(',', $padotie_ids)."))";
        }

        if(!check_access('atgadinajumi-visi')) {
          $where[] = '(
              a.`pardeveja_id` = ' . $_SESSION['user']['id'] . ' OR
              a.`created_pardeveja_id` = ' . $_SESSION['user']['id'] . '
            )';
        }

        $where[] = "a.statuss = 1";
        $where[] = "a.datums <= CURDATE()";
        $where[] = "a.`redzamiba` IN ('kopejs', 'admin')";

        $sql .= 'WHERE ' . implode(' AND ', $where);

        $akt_atg_skaits = db_get_val(db_query($sql));

        $menu[] = '<a '. (($c == 'atgadinajumi') ? 'class="active"' : '') . ' href="?c=atgadinajumi">Atgādinājumi ' . (!empty($akt_atg_skaits) ? '(' . $akt_atg_skaits . ')' : '') . '</a>';
      }

      //  Forums
      if(check_access('forums')) {
        $jaunu_temu_skaits = get_forums_jaunu_temu_skaits();
        $menu[] = '<a '. (($c == 'forums') ? 'class="active"' : '') . ' href="?c=forums">Forums '. (!empty($jaunu_temu_skaits) ? '('.$jaunu_temu_skaits.')' : '') . '</a>';
      }

      //  Atskaites
      if (check_access('atskaites-ligumu-stati') || check_access('atskaites-plana-izpilde') || check_access('atskaites-av-stati') || check_access('atskaites-zurnals')) {
        $menu[] = '<a '. (($c == 'atskaites') ? 'class="active"' : '') . ' href="?c=atskaites">Atskaites</a>';
      } elseif(check_access('atskaites-unikalie-av')) {
        $menu[] = '<a '. (($c == 'atskaites') ? 'class="active"' : '') . ' href="?c=atskaites&a=unikalie_av">Unikālie a/v</a>';
      }

      //  Nenomainītie statusi
      if (check_access('ligumi-nenomainitie-statusi') && !check_access('ligumi-nenomainitie-statusi-visi')) {

        //  Vadītājs ar padotajiem
        $pardeveju_ids = array();
        if($lietotajs->vaditajs) {
          $pardeveju_ids[] = $lietotajs->id;
          foreach((array)$lietotajs->getPadotie() as $padotais) {
            $pardeveju_ids[] = $padotais->id;
          }
        }

        if(count($pardeveju_ids) > 1) {
          $nenom_statusi = get_nenomainitie_statusi(array('pardeveju_ids' => $pardeveju_ids));
        } else {
          $nenom_statusi = get_nenomainitie_statusi(array('pardeveja_id' => $_SESSION['user']['id']));
        }

      } else if (check_access('ligumi-nenomainitie-statusi-visi')) {
        $nenom_statusi = get_nenomainitie_statusi();
      }

      if (!empty($nenom_statusi)) {
        $menu[] = '<a '.(($c == 'ligumi' && isset($_GET['a']) && $_GET['a'] == 'nenom_statusi') ? 'class="active"' : '') . ' ' . (!check_access('ligumi-nenomainitie-statusi-visi') ? 'style="color: white; background: red;"' : '') . ' href="?c=ligumi&amp;a=nenom_statusi">Nenomainītie statusi ('.count($nenom_statusi).')</a>';
      }

      //  Piedāvājumi
      if(check_access('piedavajumi') && check_access('piedavajumi-admin')) {
        $neapstradati_piedavajumi = Piedavajums::model()->findAll(array('conditions' => array('(statuss = 0 || statuss = -1) AND requested = 1')));
        $menu[] = '<a '. ($c == 'piedavajumi' ? 'class="active"' : ''). ' href="?c=piedavajumi" '. (!empty($neapstradati_piedavajumi) ? 'style="color: white; background: red;"' : ''). '>Piedāvājumi ('. count($neapstradati_piedavajumi). ')</a>';
      } elseif(check_access('piedavajumi')) {
        $menu[] = '<a '. ($c == 'piedavajumi' ? 'class="active"' : '') . ' href="?c=piedavajumi">Piedāvājumi</a>';
      }

      if(!is_admin() && check_access('nozares-aiznemtie-av', null)) {
        $menu[] = '<a ' . ($c == 'nozares' && $a == 'aiznemtie_av' ? 'class="active"' : '') . ' href="?c=nozares&a=aiznemtie_av&limited=1">Aizņemtie a/v</a>';
      }

      //  Nozares
      if(check_access('nozares-saraksts')) {
        $menu[] = '<a '. ($c == 'nozares' ? 'class="active"' : '') . ' href="?c=nozares">Nozares</a>';
      }

      //  Uzstadijumi
      if(check_access('uzstadijumi')) {
        $menu[] = '<a '. ($c == 'uzstadijumi' ? 'class="active"' : '').' href="?c=uzstadijumi">Uzstādījumi</a>';
      }

      //  Output the menu
      if(!empty($menu)) {
        echo implode(' | ', $menu);
      }

      ?>

    </div>

  </div>

  <div class="content-wrap">

    <?php } ?>

    <?php

    $default = is_admin() ? 'modules/ligumi/ligumi.php' : 'modules/darbinieki/darbinieki.php';

    switch($c){

      case "atgadinajumi": include("modules/atgadinajumi/atgadinajumi.php"); break;
      case "darbinieki": include("modules/darbinieki/darbinieki.php"); break;
      case "partneri": include("modules/partneri/partneri.php"); break;
      case "forums": include("modules/forums/forums.php"); break;
      case "ligumi": include("modules/ligumi/ligumi.php"); break;
      case "atskaites": include("modules/atskaites/atskaites.php"); break;
      case "piedavajumi": include("modules/piedavajumi/piedavajumi.php"); break;
      case "nozares": include("modules/nozares/nozares.php"); break;
      case "uzstadijumi": include("modules/uzstadijumi/uzstadijumi.php"); break;
      default: include($default); break;

    }

    ?>

    <?php if (empty($_GET['without_nav'])) { ?>

  </div>

</center>
<?php } ?>