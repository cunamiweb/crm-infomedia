<?php
if(!check_access('uzstadijumi')) {
  die('Jums nav pieejas tiesību šai sadaļai.');
}

require("list_process.php");

$uzstadijumi = Uzstadijums::model()->findAll(array('order' => 'ordering'));
?>

<button id="labotdzestbutton" onClick="return editoradd(this, <?php echo is_admin() ? 1 : 0 ?>, <?php echo is_limited() ? $_SESSION['user']['ierobezots'] : 0 ?>)">Labot</button>

<form action="?c=uzstadijumi" id="fullformplace" method="post" enctype="multipart/form-data">
  <table width="750" class="data_form layout1">
    <?php foreach($uzstadijumi as $uzstadijums) { ?>
    <tr>
      <?php if($uzstadijums->label == 'seperator') { ?>
        <td colspan="2"><hr /></td>
      <?php } else if($uzstadijums->label == 'header') { ?>

        <td colspan="2" style="text-align: center;"><strong><?php echo $uzstadijums->value;?></strong></td>

      <?php } else { ?>
        <td class="label" width="400"><?php echo $uzstadijums->label;?>:</td>
        <td><input class="limited_admin_edit" type="input" id="uzstadijums_<?php echo $uzstadijums->id;?>_value" name="Uzstadijums[<?php echo $uzstadijums->id;?>][value]" value="<?php echo $uzstadijums->value;?>"></td>
      <?php } ?>
    </tr>
    <?php } ?>
  </table>
  <div class="hidethistoo">
    <input type="submit" onClick="return saveAll(this, 'uzstadijumi')" class="submit show_in_act_panel" value="Saglabāt">
    <input type="button" onclick="return cancelSaves();" class="show_in_act_panel" value="Atcelt">
  </div>
</form>

<script type="text/javascript">
  editordisable();
</script>