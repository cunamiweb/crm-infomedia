<?php
if(!is_superadmin()) {
  die();
}

if(isset($_POST['Tiesiba']) && !empty($_POST['Tiesiba'])) {

  $log_data = array();
  $log_data['grupu_tiesibas']['title'] = 'Laboja grupu tiesības';

  //  Ielogo un izdzēš visas saglabātās tiesības
  $log_data['grupu_tiesibas']['old'] = db_get_assoc_all(db_query('SELECT * FROM ' . DB_PREF . 'tiesibas_grupas'));
  db_query('DELETE FROM ' . DB_PREF . 'tiesibas_grupas');

  foreach($_POST['Tiesiba'] as $tiesiba_id => $groups) {
    foreach($groups as $group => $perm) {
      $existing = db_query('INSERT INTO ' . DB_PREF . 'tiesibas_grupas (grupa, tiesibas_id, piekluve) VALUES ("'.esc($group).'", '.esc($tiesiba_id).', '.esc($perm).')');
    }
  }

  $log_data['grupu_tiesibas']['new'] = db_get_assoc_all(db_query('SELECT * FROM ' . DB_PREF . 'tiesibas_grupas'));

  log_add('laboja', $log_data);

  header('Location: ?c=uzstadijumi&a=grupu_tiesibas');
}
?>