<?php
if(!check_access('uzstadijumi')) {
  die('Jums nav pieejas tiesību šai sadaļai.');
}

if(isset($_POST['Uzstadijums'])) {

  $log['uzstadijumi']['old_m'] = db_get_assoc_all(db_query('SELECT id, value FROM '.DB_PREF.'uzstadijumi'));

  $old_x = get_config('piedavajumi_x');
  $old_xs = get_config('piedavajumi_xs');
  $old_xt = get_config('piedavajumi_xt');


  //  Skatāmies vai ir mainīti x, xs, xt uzstādījumi
  if($old_x != $_POST['Uzstadijums']['piedavajumi_x']['value'] || $old_xs != $_POST['Uzstadijums']['piedavajumi_xs']['value'] || $old_xt != $_POST['Uzstadijums']['piedavajumi_xt']['value']) {
    $x_has_changed = true;
    $models = StandartaPiedavajums::model()->findAll();

    //  Old prices
    $old = array();
    foreach($models as $model) {
        $old[$model->id] = $model->priceArray();
    }
  } else {
    $x_has_changed = false;
  }

  foreach($_POST['Uzstadijums'] as $id => $values) {
    $model = Uzstadijums::model()->findByPk($id);
    $model->value = $values['value'];

    $model->save();
  }

  update_config();

  $log['uzstadijumi']['new_m'] = db_get_assoc_all(db_query('SELECT id, value FROM '.DB_PREF.'uzstadijumi'));
  //$log['title'] = 'Mainīja konfigurāciju';

  log_add('laboja', $log);

  if($x_has_changed) {
    //  Ja mainījušies cenu koeficienti tad jāpiefiksē visiem av cenu izmaiņas
    foreach($models as $model) {
      $model->logPriceChange('config', $old[$model->id]);
    }
  }
}
?>