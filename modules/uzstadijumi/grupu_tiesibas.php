<?php
if(!is_superadmin()) {
  die();
}

include('grupu_tiesibas_process.php');

$perm_types = Tiesiba::model()->findAll(array('order' => 'kategorija, ordering, nosaukums'));

$perm_groups = array();
foreach($perm_types as $type) {

  if(!isset($perm_groups[$type->kategorija])) {
    $perm_groups[$type->kategorija] = array();
  }

  $perm_groups[$type->kategorija][] = $type;
}
?>

<button id="labotdzestbutton" onClick="return editoradd(this, <?= is_admin() ? 1 : 0 ?>, <?= is_limited() ? $_SESSION['user']['ierobezots'] : 0 ?>)">Labot</button>
<form action="?c=uzstadijumi&a=grupu_tiesibas" id="fullformplace" method="post">

  <div class="hidethistoo">
    <input type="submit" onClick="return saveAll(this, 'grupu_tiesibas')" class="submit show_in_act_panel" value="Saglabāt">
    <input type="button" onclick="return cancelSaves();" class="show_in_act_panel" value="Atcelt">
  </div>

  <table cellpadding="3" cellspacing="0" class="data" style="width: auto;">

  <thead>

    <tr class="header">
      <th width="700">Tiesības</th>
      <th width="60">Pārdevēji</th>
      <th width="60">Aģentūras</th>
      <th width="60">Admini</th>
    </tr>

  </thead>
  <tbody class="main">
    <?php foreach((array)$perm_groups as $name => $types) {?>
      <tr><td class="r" style="font-weight: bold;" colspan="3"><?php echo $name;?></td></tr>
      <?php foreach((array)$types as $type) { ?>
        <tr>
          <td class="r">
            <strong><?php echo $type->nosaukums;?></strong>
            <?php if($type->paskaidrojums) { ?>
              <br /><?php echo $type->paskaidrojums;?>
            <?php } ?>
          </td>

          <?php
          $groups = $type->getGroups();
          ?>
          <td class="c">
            <select name="Tiesiba[<?php echo $type->id;?>][sales]" class="with_status_colors classprefix-perms_status">
              <option class="status-1" value="1" <?php echo $groups['sales'] == 1 ? 'selected="SELECTED"' : '';?>>Jā</option>
              <option class="status-2" value="-1" <?php echo $groups['sales'] == -1 ? 'selected="SELECTED"' : '';?>>Nē</option>
            </select>
          </td>
          <td class="c">
            <select name="Tiesiba[<?php echo $type->id;?>][partner]" class="with_status_colors classprefix-perms_status">
              <option class="status-1" value="1" <?php echo $groups['partner'] == 1 ? 'selected="SELECTED"' : '';?>>Jā</option>
              <option class="status-2" value="-1" <?php echo $groups['partner'] == -1 ? 'selected="SELECTED"' : '';?>>Nē</option>
            </select>
          </td>
          <td class="c">
            <select name="Tiesiba[<?php echo $type->id;?>][admin]" class="with_status_colors classprefix-perms_status">
              <option class="status-1" value="1" <?php echo $groups['admin'] == 1 ? 'selected="SELECTED"' : '';?>>Jā</option>
              <option class="status-2" value="-1" <?php echo $groups['admin'] == -1 ? 'selected="SELECTED"' : '';?>>Nē</option>
            </select>
          </td>
        </tr>
      <?php } ?>
    <?php } ?>
  </tbody>
  </table>
</form>

<script type="text/javascript">
initSelectsWithStatusColors();
editordisable();
</script>

