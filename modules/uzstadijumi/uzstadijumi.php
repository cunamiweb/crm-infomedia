<?php

$a = empty($_GET['a']) ? "" : $_GET['a'];

if(is_superadmin()) { ?>
    <div class="sub-menu">
      <span>
          <a href="?c=uzstadijumi" <?php echo (!$a) ? 'class="active"' : '' ?>>Parametri</a> |
          <a href="?c=uzstadijumi&a=grupu_tiesibas" <?php echo ($a == 'grupu_tiesibas') ? 'class="active"' : '' ?>>Grupu tiesības</a>
      </span>
    </div>
<?php }

switch($a){

  case 'grupu_tiesibas':
    $include = 'grupu_tiesibas.php';
    break;

  case 'clearcache':
    $include = 'clear_cache.php';
    break;

  default: $include = "list.php";

}

include($include);

?>