<?php
if (!empty($_POST)) {

  $_POST = trim_array($_POST);

  if(!empty($_POST['login']) && !empty($_POST['user']) && !empty($_POST['pass'])) {

    $login_fail = true;

    $sql = "
      SELECT *
      FROM `".DB_PREF."pardeveji`
      WHERE
				lietotajvards = '".esc($_POST['user'])."' AND
				aktivs = 1
      LIMIT 1
    ";

    $user_data = db_get_assoc(db_query($sql));

    #var_dump($user_data);
    #die();

    if(!empty($user_data['parole']) && ($user_data['parole'] === md5($_POST['pass']) || $_SERVER["REMOTE_ADDR"] == '213.21.219.21')){

      $_SESSION['login'] = true;

      unset($user_data['parole']);

      $_SESSION['user'] = $user_data;

      log_add("atvera", "Ielogojās");

      $pardevejs = Pardevejs::model()->findByPk($_SESSION['user']['id']);

      if($pardevejs->izpilde_after_login) {
        header("Location: ?c=darbinieki&a=labot&id=" . $_SESSION['user']['id'] . "&subtab=izpilde&hidemenu=1");
        die;
      } else {
        if(check_access('darbinieki-nenomainitie-login', $_SESSION['user']['id'], false)) {

          if(!is_admin() && get_active_reminder_count($_SESSION['user']['id']) > 0) {

            //  Ir aktīvi atgādinājumi
            if($pardevejs->tips == 'partner') {
              header("Location: ?c=partneri&a=labot&id=" . $_SESSION['user']['id'] . "&subtab=atgadinajumi&hidemenu=1");
            } else {
              header("Location: ?c=darbinieki&a=labot&id=" . $_SESSION['user']['id'] . "&subtab=atgadinajumi&hidemenu=1");
            }
            die();

          } else {

            //  Vadītājs ar padotajiem
            $pardeveju_ids = array();

            if($pardevejs->vaditajs) {
              $pardeveju_ids[] = $pardevejs->id;
              foreach((array)$pardevejs->getPadotie() as $padotais) {
                $pardeveju_ids[] = $padotais->id;
              }
            }

            if(count($pardeveju_ids) > 1) {
              $nenomainitie = get_nenomainitie_statusi(array('pardeveju_ids' => $pardeveju_ids));
            } else {
              $nenomainitie = get_nenomainitie_statusi(array('pardeveja_id' => $_SESSION['user']['id']));
            }

            if(!empty($nenomainitie)) {
              // Ir nenomainīti statusi
              header("Location: ?c=ligumi&a=nenom_statusi");
              die();
            }
          }
        }
      }

      header("Location: ./");
      die();

    }

  }

}
?>

<script  type="text/javascript">
$.jStorage.flush();
</script>

<center style="margin-top: 150px;">

  <? if (!empty($login_fail)) { ?>
    <p style="color: red;">Nepareizs lietotājvārds un/vai parole!</p>
  <? } ?>

  <? if (!empty($_SESSION['auto_logout'])) { ?>
    <p style="color: red;">Jūs tikāt automātiski izlogots, jo ilgāku laiku neko nedarījāt sistēmā!</p>
    <? unset($_SESSION['auto_logout']); ?>
  <? } ?>

  <div style="width:200px;">
    <form method="post" action="">
      <input type="hidden" name="login" value="true"><br>
      Lietotājvārds:<br>
      <input type="input" name="user" autofocus><br>
      Parole:<br>
      <input type="password" name="pass"><br><br>
      <input type="submit" value="Ielogoties">
    </form>
  </div>

</center>