<?php
if (!check_access('darbinieki-rekviziti')) {
  die('Nav pieejas tiesību veikt šo darbību');
}

if(!isset($_GET['id'])) {
  die('Nav norādīts darbinieka id.');
}

$pardevejs = Pardevejs::model()->findByPk($_GET['id']);

if(!$pardevejs) {

  die('Neeksistējošs pārdevējs');

} else {

  if($pardevejs->delete()) {

    header('Location: ?c=darbinieki');

  } else {

    die('Pārdevēju neizdevās izdzēst');
  }
}

?>