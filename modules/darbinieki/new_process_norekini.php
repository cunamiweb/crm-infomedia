<?php
if (!check_access('darbinieki-rekini') && ($pardeveja_id != $_SESSION['user']['id'] || !check_access('darbinieki-savi-rekini'))) {
    die('Jums nav pieejas tiesību veikt šo darbību.');
}

if (!empty($_POST) && check_access('darbinieki-rekini')) {

  $errors = validate_darbinieks_norekini_form($_POST);

  if (empty($errors)) {

    $pardevejs = db_get_assoc(db_query("SELECT * FROM `".DB_PREF."pardeveji` WHERE `id` = " . $pardeveja_id));

    $_POST = trim_array($_POST);

    if (is_admin()) {

      if (!empty($_POST['rekini'])) {

        foreach($_POST['rekini'] as $rekina_id => $r) {

          $log_data = array();

          $old_data = db_get_assoc(db_query("
            SELECT *
            FROM `".DB_PREF."pardeveji_rekini`
            WHERE id = ".$rekina_id."
          "));

          $log_data['norekins']['old'] = $old_data;

          $rek_nr = !empty($r['rek_nr']) ? "'" . esc($r['rek_nr']) . "'" : "''";
          $izr_datums = !empty($r['izr_datums']) ? "'".date('Y-m-d', strtotime($r['izr_datums']))."'" : 'null';
          $apm_datums = !empty($r['apm_datums']) ? "'".date('Y-m-d', strtotime($r['apm_datums']))."'" : 'null';

          $statuss = 1;
          if (!empty($r['apm_datums'])) {
            $statuss = 2;
          }

          if(check_access('darbinieki-labot-rekinu-num-dat')) {
          $sql = "
            UPDATE `".DB_PREF."pardeveji_rekini`
            SET
              rek_nr = ".$rek_nr.",
              izr_datums = ".$izr_datums.",
              statuss = ".$statuss.",
              apm_datums = ".$apm_datums."
            WHERE id = ".$rekina_id."
            LIMIT 1
          ";

          db_query($sql);

          } elseif(check_access('darbinieki-rekini-apmaksas-datums')) {
            $sql = "
              UPDATE `".DB_PREF."pardeveji_rekini`
              SET
                statuss = ".$statuss.",
                apm_datums = ".$apm_datums."
              WHERE id = ".$rekina_id."
              LIMIT 1
            ";

            db_query($sql);
          }

          $log_data['norekins']['new'] = db_get_assoc(db_query("
            SELECT *
            FROM `".DB_PREF."pardeveji_rekini`
            WHERE id = ".$rekina_id."
          "));

          $log_data['norekins']['title'] = sprintf('Laboja pārdevēja %s rēķinu "%s"', $pardevejs['vards'], $r['rek_nr']);

          log_add("laboja", $log_data);

        }
      }
    }

    // inserting files
    process_files(array('darb_norekini'), $pardeveja_id);

    // inserting comments
    process_comments(array('darb_norekini'), $pardeveja_id);

    header('Location: ?c=darbinieki&a=labot&id=' . $pardeveja_id . '&subtab=' . $subtab);
    die();

  }
}


?>