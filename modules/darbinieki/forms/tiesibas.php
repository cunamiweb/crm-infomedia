<?php
if(!is_superadmin()) {
  die();
}
$perm_types = Tiesiba::model()->findAll(array('order' => 'kategorija, ordering, nosaukums'));

$perm_groups = array();
foreach($perm_types as $type) {

  if(!isset($perm_groups[$type->kategorija])) {
    $perm_groups[$type->kategorija] = array();
  }

  $perm_groups[$type->kategorija][] = $type;
}
?>

<button id="labotdzestbutton" onClick="return editoradd(this, <?= is_admin() ? 1 : 0 ?>, <?= is_limited() ? $_SESSION['user']['ierobezots'] : 0 ?>)">Labot</button>

<form action="?c=darbinieki<?= !empty($pardeveja_id) ? '&a=labot&id=' . $pardeveja_id . '&subtab=tiesibas' : '&a=jauns' ;?>" id="fullformplace" method="post" enctype="multipart/form-data">

  <? if (!empty($pardeveja_id)) { ?>
    <input class="non_admin_edit limited_admin_edit" type="hidden" name="pardeveja_id" value="<?= $pardeveja_id ?>" />
  <? } ?>

  <div class="hidethistoo">
    <input type="submit" onClick="return saveAll(this, 'grupu_tiesibas')" class="submit show_in_act_panel" value="Saglabāt">
    <input type="button" onclick="return cancelSaves();" class="show_in_act_panel" value="Atcelt">
  </div>

  <table cellpadding="3" cellspacing="0" class="data" style="width: auto;">

  <thead>

    <tr class="header">
      <th width="500">Tiesības</th>
      <th width="60">Pieeja</th>
      <th width="60">Rozultējošais</th>
    </tr>

  </thead>
  <tbody class="main">
    <?php foreach((array)$perm_groups as $name => $types) {?>
      <tr><td class="r" style="font-weight: bold;" colspan="3"><?php echo $name;?></td></tr>
      <?php foreach((array)$types as $type) { ?>
        <tr>
          <td class="r">
            <strong><?php echo $type->nosaukums;?></strong>
            <?php if($type->paskaidrojums) { ?>
              <br /><?php echo $type->paskaidrojums;?>
            <?php } ?>
          </td>

          <?php
          if($pardeveja_id) {
            $perm = $type->getUser($pardeveja_id);
          } else {
            $perm = 0;
          }
          ?>
          <td class="c">
            <select name="Tiesiba[<?php echo $type->id;?>]" class="with_status_colors classprefix-perms_status">
              <option class="status-1" value="1" <?php echo $perm == 1 ? 'selected="SELECTED"' : '';?>>Jā</option>
              <option class="status-2" value="-1" <?php echo $perm == -1 ? 'selected="SELECTED"' : '';?>>Nē</option>
              <option class="status-0" value="0" <?php echo $perm == 0 ? 'selected="SELECTED"' : '';?>>Noklusētais</option>
            </select>
          </td>
          <td class="c">
            <?php echo check_access($type->alias, $pardeveja_id) ? '<span style="color: green;">Jā</span>' : '<span style="color: red;">Nē</span>'; ?>
          </td>
        </tr>
      <?php } ?>
    <?php } ?>
  </tbody>
  </table>
</form>

<script type="text/javascript">
initSelectsWithStatusColors();
editordisable();
</script>