<?
if(!check_access('darbinieki-ligumi') && ($pardeveja_id != $_SESSION['user']['id'] || !check_access('darbinieki-savi-ligumi'))) {
  die('Jums nav pieejas tiesību šai sadaļai');
}
log_add("atvera", "Atvēra darbinieka " . $d['vards'] . " līgumus");

$active_page = !empty($_GET['page']) ? (int)$_GET['page'] : 1;

$pardeveju_ids = array($pardeveja_id);
foreach((array)$pardevejs->getPadotie() as $padotais) {
  $pardeveju_ids[] = $padotais->id;
}

if (!isset($_GET['show_in_page'])) {
  $show_in_page = 30;
} elseif ($_GET['show_in_page'] > 0) {
  $show_in_page = (int)$_GET['show_in_page'];
} else {
  $show_in_page = '';
}

$order_bys = array(
  'pardevejs' => 'p.vards',
  'lig_nr' => 'g.ligumanr',
  'nosauk' => 'g.nosaukums',
  'indekss' => 'g.faktaddr_index',
  'lig_statuss' => '',
  'kopapm_statuss' => '',
  'lig_datums' => 'g.ligumadat',
  'san_datums' => 'g.sanemsanas_datums',
  'san_forma' => 'g.sanemsanas_forma',
  'lig_no' => 'g.reklama_no',
  'lig_lidz' => 'g.reklama_lidz',
  'summa' => 'rekinu_summa',
  'pvn' => 'rekinu_pvn_summa',
  'kopsumma' => 'rekinu_kopsumma',
  'apm_summa' => 'apm_kopsumma',
  'prov_proc' => 'g.provizija_pardevejam',
  'atl_vad' => 'g.atlidziba_vaditajam',
  'prov_bez_pvn' => 'prov_summa',
  'izm_prov_bez_pvn' => 'apm_prov_summa',
  'izm_prov_proc' => '',
  'akt_atgad' => 'aktivi_atgadinajumi',
  'apkalp_maksa_proc' => 'g.apkalp_maksa_proc',
);

$order = '';
$direction = 'asc';

if (!empty($_GET['order']) && isset($order_bys[$_GET['order']])) {
  $order = $order_bys[$_GET['order']];
}

if (!empty($_GET['direction']) && in_array(strtolower($_GET['direction']), array('asc', 'desc'))) {
  $direction = strtolower($_GET['direction']);
};


$sql = 'SELECT r.*
  FROM `'.DB_PREF.'rekini` r
  LEFT JOIN `'.DB_PREF.'ligumi` g ON (g.id = r.liguma_id)
  WHERE ';

if($pardevejs->vaditajs && $pardevejs->id == KORP_VADITAJA_ID && $lietotajs->id != $pardevejs->id) {
  //  Korp vadītājam rāda gan savus gan partneru līgumus
  $sql .= 'g.pardeveja_id IN (' . implode(', ', $pardeveju_ids) . ')';
} else {
  $sql .= 'g.`pardeveja_id` = '.$pardeveja_id;
}
$query = db_query($sql);
$lig_rekini = array();

while($row = db_get_assoc($query)){
  $lig_rekini[$row['liguma_id']][] = htmlesc($row);
}

$query = db_query("
  SELECT
    s.liguma_id,
    p.*
  FROM `".DB_PREF."ligumi_saistitie_pardeveji` s
  LEFT JOIN `".DB_PREF."pardeveji` p ON (p.id = s.pardeveja_id)
");

$saist_pard = array();

while($row = db_get_assoc($query)){
  $saist_pard[$row['liguma_id']][] = $row;
}

$query = db_query("
  SELECT
    p.*,
    v.nosaukums,
    v.parent_id
  FROM `".DB_PREF."pakalpojumi` p
  LEFT JOIN `".DB_PREF."pakalpojumi_veidi` v ON (p.pakalpojuma_veida_id = v.id)
  ORDER BY v.seciba ASC
");

$pakalpojumi = array();

while($row = db_get_assoc($query)){
  $pakalpojumi[$row['liguma_id']][$row['parent_id']][] = $row;
}


//  MPK pakalpojumi
$mpk_pakalpojumi = array();

// Search
$result = db_query('
  SELECT g.id, k.site, COUNT(k.id) as count
  FROM '.DB_PREF.'mpk_keywords k
  LEFT JOIN '.DB_PREF.'ligumi g ON g.id = k.ligums_id
  WHERE
    g.mpk = 1
  GROUP BY k.ligums_id, k.site
');
while($row = db_get_assoc($result)) {
  $mpk_pakalpojumi[$row['id']][$row['site']]['search'] = $row['count'];
}

// Categories
$result = db_query('
  SELECT g.id, k.site, COUNT(k.id) as count
  FROM '.DB_PREF.'mpk_categories k
  LEFT JOIN '.DB_PREF.'ligumi g ON g.id = k.ligums_id
  WHERE
    g.mpk = 1
  GROUP BY k.ligums_id, k.site
');
while($row = db_get_assoc($result)) {
  $mpk_pakalpojumi[$row['id']][$row['site']]['category'] = $row['count'];
}

// Displays
$result = db_query('
  SELECT g.id, k.site, COUNT(k.id) as count
  FROM '.DB_PREF.'mpk_displays k
  LEFT JOIN '.DB_PREF.'ligumi g ON g.id = k.ligums_id
  WHERE
    g.mpk = 1 AND k.enabled = 1
  GROUP BY k.ligums_id, k.site
');
while($row = db_get_assoc($result)) {
  $mpk_pakalpojumi[$row['id']][$row['site']]['display'] = $row['count'];
}

// Analytics
$result = db_query('
  SELECT g.id, k.site, COUNT(k.id) as count
  FROM '.DB_PREF.'mpk_analytics k
  LEFT JOIN '.DB_PREF.'ligumi g ON g.id = k.ligums_id
  WHERE
    g.mpk = 1 AND k.enabled = 1
  GROUP BY k.ligums_id, k.site
');
while($row = db_get_assoc($result)) {
  $mpk_pakalpojumi[$row['id']][$row['site']]['analytics'] = $row['count'];
}

// Conversions
$result = db_query('
  SELECT g.id, k.site, COUNT(k.id) as count
  FROM '.DB_PREF.'mpk_conversions k
  LEFT JOIN '.DB_PREF.'ligumi g ON g.id = k.ligums_id
  WHERE
    g.mpk = 1 AND k.enabled = 1
  GROUP BY k.ligums_id, k.site
');
while($row = db_get_assoc($result)) {
  $mpk_pakalpojumi[$row['id']][$row['site']]['conversions'] = $row['count'];
}

// Conversions
$result = db_query('
  SELECT g.id as id, k.id as kid
  FROM '.DB_PREF.'mpk_others k
  LEFT JOIN '.DB_PREF.'ligumi g ON g.id = k.ligums_id
  WHERE
    g.mpk = 1 AND k.description IS NOT NULL && k.description != ""
  GROUP BY k.ligums_id
');
while($row = db_get_assoc($result)) {
  $mpk_pakalpojumi[$row['id']]['other'] = 1;
}

$visi_pielikumi = array();

foreach(array('tiessaite', 'infomedia', 'yandex', 'citi') as $pielikuma_tips) {
  $query = db_query("
    SELECT p.*
    FROM `".DB_PREF."pielikumi_".$pielikuma_tips."` p
    LEFT JOIN `".DB_PREF."ligumi` g ON g.id = p.liguma_id
    WHERE g.mpk = 0
  ");

  while($row = db_get_assoc($query)){
    $visi_pielikumi[$row['liguma_id']][$pielikuma_tips][] = $row;
  }
}

//  MPK Atslegvardi

$visi_mpk_atslegvardi = array();
$result = db_query('
  SELECT k.*
  FROM '.DB_PREF.'mpk_keywords k
  LEFT JOIN '.DB_PREF.'ligumi g ON g.id = k.ligums_id
  WHERE g.mpk = 1
');
while($row = db_get_assoc($result)) {
  $visi_mpk_atslegvardi[$row['ligums_id']][] = $row;
}

//  MPK Tēmas
$visi_mpk_temas = array();
$result = db_query('
  SELECT k.*
  FROM '.DB_PREF.'mpk_categories k
  LEFT JOIN '.DB_PREF.'ligumi g ON g.id = k.ligums_id
  WHERE g.mpk = 1
');
while($row = db_get_assoc($result)) {
  $visi_mpk_temas[$row['ligums_id']][] = $row;
}

//  MPK Displeji
$visi_mpk_displeji = array();
$result = db_query('
  SELECT k.*
  FROM '.DB_PREF.'mpk_displays k
  LEFT JOIN '.DB_PREF.'ligumi g ON g.id = k.ligums_id
  WHERE g.mpk = 1 AND k.enabled = 1
');
while($row = db_get_assoc($result)) {
  $visi_mpk_displeji[$row['ligums_id']][] = $row;
}

//  MPK konversijas
$visi_mpk_konversijas = array();
$result = db_query('
  SELECT k.*
  FROM '.DB_PREF.'mpk_conversions k
  LEFT JOIN '.DB_PREF.'ligumi g ON g.id = k.ligums_id
  WHERE g.mpk = 1 AND k.enabled = 1
');
while($row = db_get_assoc($result)) {
  $visi_mpk_konversijas[$row['ligums_id']][] = $row;
}

//  MPK analitika
$visi_mpk_analitika = array();
$result = db_query('
  SELECT k.*
  FROM '.DB_PREF.'mpk_analytics k
  LEFT JOIN '.DB_PREF.'ligumi g ON g.id = k.ligums_id
  WHERE g.mpk = 1 AND k.enabled = 1
');
while($row = db_get_assoc($result)) {
  $visi_mpk_analitika[$row['ligums_id']][] = $row;
}

//  MPK other
$visi_mpk_other = array();
$result = db_query('
  SELECT k.*
  FROM '.DB_PREF.'mpk_others k
  LEFT JOIN '.DB_PREF.'ligumi g ON g.id = k.ligums_id
  WHERE g.mpk = 1 AND k.description != "" AND k.description IS NOT NULL
');
while($row = db_get_assoc($result)) {
  $visi_mpk_other[$row['ligums_id']][] = $row;
}

$query = db_query("
  SELECT *
  FROM `".DB_PREF."pielikumi_kontu_linki`
");

$kontu_linki = array();

while($row = db_get_assoc($query)){
  $kontu_linki[$row['liguma_id']][$row['tips']][$row['konts']] = $row;
}

$where = array();
$having = array();

if (isset($_GET['fligumu_id'])) {
  if (getGet('fligumu_id'))
    $where[] = 'g.id IN ('.getGet('fligumu_id').')';
  else
    $where[] = 'g.id IS NULL';
}

//  Rādīt partneru līgumus tikai ja admins skatās korp vadītāju (skatītājs != skatāmais)
if($pardevejs->vaditajs && $pardevejs->id == KORP_VADITAJA_ID && $lietotajs->id != $pardevejs->id) {
  //  Korp vadītājam rāda gan savus gan partneru līgumus
  $where[] = 'g.pardeveja_id IN (' . implode(', ', $pardeveju_ids) . ')';

  $show_liguma_sledzejs = true;
  $show_vaditaja_atl = true;
  $show_pakalpojums = true;
  $show_apkalp_maksa = true;

} else if($pardevejs->id == KORP_VADITAJA_ID) {
  $where[] = 'g.pardeveja_id = '.$pardeveja_id;

  $show_liguma_sledzejs = false;
  $show_vaditaja_atl = false;
  $show_pakalpojums = true;
  $show_apkalp_maksa = true;

} else {
  $where[] = 'g.pardeveja_id = '.$pardeveja_id;

  $show_liguma_sledzejs = false;
  $show_vaditaja_atl = false;
  $show_pakalpojums = false;
  $show_apkalp_maksa = false;
}

if(getGet('s_pardeveja_id')) {
  $s_pardeveju_ids = array();
  foreach((array)getGet('s_pardeveja_id') as $pid) {
    $s_pardeveju_ids[] = esc($pid);
  }
  $where[] = 'g.pardeveja_id IN ('.implode(', ', $s_pardeveju_ids).')';
}

if (getGet('fnosaukums') !== '') $where[] = 'g.nosaukums LIKE "%'.getGet('fnosaukums').'%"';
if (getGet('fliguma_nr') !== '') $where[] = 'g.ligumanr LIKE "%'.getGet('fliguma_nr').'%"';
if (getGet('ffaktaddr_index') !== '') $where[] = 'g.faktaddr_index LIKE "%'.getGet('ffaktaddr_index').'%"';

if (getGet('flig_no') !== '') $where[] = 'g.ligumadat >= "'.to_db_date(getGet('flig_no')).'"';
if (getGet('flig_lidz') !== '') $where[] = 'g.ligumadat <= "'.to_db_date(getGet('flig_lidz')).'"';
if (getGet('flig_sanemts_no') !== '') $where[] = 'g.sanemsanas_datums >= "'.to_db_date(getGet('flig_sanemts_no')).'"';
if (getGet('flig_sanemts_lidz') !== '') $where[] = 'g.sanemsanas_datums <= "'.to_db_date(getGet('flig_sanemts_lidz')).'"';
if (getGet('frekl_sak_no') !== '') $where[] = 'g.reklama_no >= "'.to_db_date(getGet('frekl_sak_no')).'"';
if (getGet('frekl_sak_lidz') !== '') $where[] = 'g.reklama_no <= "'.to_db_date(getGet('frekl_sak_lidz')).'"';
if (getGet('frekl_beig_no') !== '') $where[] = 'g.reklama_lidz >= "'.to_db_date(getGet('frekl_beig_no')).'"';
if (getGet('frekl_beig_lidz') !== '') $where[] = 'g.reklama_lidz <= "'.to_db_date(getGet('frekl_beig_lidz')).'"';

if (getGet('fapkalp_maksa_proc') !== '') $where[] = 'g.apkalp_maksa_proc = "'.getGet('fapkalp_maksa_proc').'"';

if (pt_multiselect_is_active('fsanems_forma')) $where[] = 'g.sanemsanas_forma IN (' . implode(',', array_map('intval', getGet('fsanems_forma'))) . ')';

if (pt_multiselect_is_active('fpakalpojums')) {

  $where[] = '
    (
      g.id IN (
        SELECT liguma_id
        FROM `'.DB_PREF.'pakalpojumi`
        WHERE
          pakalpojuma_veida_id IN (' . implode(',', array_map('intval', getGet('fpakalpojums'))) . ') OR
          pakalpojuma_veida_id IN (
            SELECT id FROM `'.DB_PREF.'pakalpojumi_veidi` WHERE `parent_id` IN (' . implode(',', array_map('intval', getGet('fpakalpojums'))) . ')
          )

       ) AND g.mpk = 0
    )
  ';

}


if (getGet('flig_summa') !== '') $having[] = 'FLOOR(rekinu_summa) = '.(int)getGet('flig_summa');
if (getGet('flig_pvn') !== '') $having[] = 'FLOOR(rekinu_pvn_summa) = '.(int)getGet('flig_pvn');
if (getGet('flig_kopsumma') !== '') $having[] = 'FLOOR(rekinu_kopsumma) = '.(int)getGet('flig_kopsumma');
if (getGet('fapmaks_summa') !== '') $having[] = 'FLOOR(apm_kopsumma) = '.(int)getGet('fapmaks_summa');

if (getGet('fprov_percent') !== '') $having[] = 'FLOOR(g.provizija_pardevejam) = '.(int)getGet('fprov_percent');

if ($show_vaditaja_atl && getGet('fatl_vad') !== '') $having[] = 'FLOOR(g.atlidziba_vaditajam) = '.(int)getGet('fatl_vad');

if (getGet('fprov_summa') !== '') $having[] = 'FLOOR(prov_summa) = '.(int)getGet('fprov_summa');
if (getGet('fapm_prov_summa') !== '') $having[] = 'FLOOR(apm_prov_summa) = '.(int)getGet('fapm_prov_summa');

$atg_having = array();
if (pt_is_selected(1, 'fakt_atgad')) $atg_having[] = 'aktivi_atgadinajumi > 0';
if (pt_is_selected(2, 'fakt_atgad')) $atg_having[] = 'aktivi_atgadinajumi = 0';
if (!empty($atg_having)) $having[] = '(' . implode(' OR ', $atg_having) . ')';

$sql = "
  SELECT
    g.*,
    b.beigu_statuss,
    b.atlikt_lidz as beigu_statuss_atlikt_lidz,
    b.auto_atteikums as beigu_statuss_auto_atteikums,
    SUM(r.summa) as rekinu_summa,
    SUM(r.kopsumma) - SUM(r.summa) as rekinu_pvn_summa,
    SUM(r.kopsumma) as rekinu_kopsumma,
    SUM(r.apm_summa) as apm_kopsumma,
    (SUM(r.summa) * (g.provizija_pardevejam) / 100) as prov_summa,
    (
      SELECT COUNT(*)
      FROM `".DB_PREF."atgadinajumi`
      WHERE
        liguma_id = g.id AND
        statuss = 1 AND
        datums <= CURDATE() AND
        redzamiba IN ('kopejs', ".(is_admin() ? "'admin'" : "'pardevejs'").")
    ) as aktivi_atgadinajumi,
    (
      SELECT COUNT(*)
      FROM `".DB_PREF."ligumi_saistitie_pardeveji`
      WHERE liguma_id = g.id
    ) as saistito_pardeveju_skaits,
    (
      SELECT SUM(sd.summa)
      FROM `".DB_PREF."pardeveji_rekini_detalas` sd
      LEFT JOIN `".DB_PREF."pardeveji_rekini` sp ON (sp.id = sd.pardeveja_rekina_id)
      LEFT JOIN `".DB_PREF."rekini` sr ON (sr.id = sd.rekina_id)
      WHERE
        sr.liguma_id = g.id AND
        sp.pardeveja_id = g.pardeveja_id AND
        sp.statuss = 2
    ) as apm_prov_summa,
    p.vards as p_vards
  FROM `".DB_PREF."ligumi` g
  LEFT JOIN `".DB_PREF."ligumi_beigusies` b ON (g.id = b.liguma_id)
  LEFT JOIN `".DB_PREF."pardeveji` p ON (p.id = g.pardeveja_id)
  LEFT JOIN `".DB_PREF."rekini` r ON (
    r.liguma_id = g.id AND
    r.atkapsanas_rekins = 0
  )
";

if (!empty($where)) {
  $sql .= " WHERE " . implode(' AND ', $where);
}

$sql .= " GROUP BY g.id";

if (!empty($having)) {
  $sql .= " HAVING " . implode(' AND ', $having);
}

$sql .= " ORDER BY " . (!empty($order) ? $order . " " . $direction : ' IF(g.`sanemsanas_datums` IS NULL, 0, 1) ASC, g.`sanemsanas_datums` DESC, g.`ligumadat` DESC, g.`ligumanr` DESC');

$query = db_query($sql);

$alldata = array();

// apply filtering to complex columns

while($row = db_get_assoc($query)){

  if (pt_multiselect_is_active('fpakalpojums_mpk')) {

    $mpk_filters = getGet('fpakalpojums_mpk');

    if(!empty($mpk_filters)) {

      if(!$row['mpk']) continue;

      $continue = true;

      //  Google search
      if(in_array('search', $mpk_filters)) {
        if(!empty($visi_mpk_atslegvardi[$row['id']])) {
          foreach($visi_mpk_atslegvardi[$row['id']] as $av) {
            if($av['site'] == 'google') {
              $continue = false;
              break;
            }
          }
        }
      }

      //  Yandex direct
      if(in_array('direct', $mpk_filters)) {
        if(!empty($visi_mpk_atslegvardi[$row['id']])) {
          foreach($visi_mpk_atslegvardi[$row['id']] as $av) {
            if($av['site'] == 'yandex') {
              $continue = false;
              break;
            }
          }
        }
      }

      //  Google display
      if(in_array('display', $mpk_filters)) {

        //  Categories
        if(!empty($visi_mpk_temas[$row['id']])) {
          foreach($visi_mpk_temas[$row['id']] as $av) {
            if($av['site'] == 'google') {
              $continue = false;
              break;
            }
          }
        }

        //  Display
        if(!empty($visi_mpk_displeji[$row['id']])) {
          foreach($visi_mpk_displeji[$row['id']] as $av) {
            if($av['site'] == 'google') {
              $continue = false;
              break;
            }
          }
        }
      }

      //  Yandex konteksts
      if(in_array('kontext', $mpk_filters)) {
        if(!empty($visi_mpk_temas[$row['id']])) {
          foreach($visi_mpk_temas[$row['id']] as $av) {
            if($av['site'] == 'yandex') {
              $continue = false;
              break;
            }
          }
        }
      }

      //  Google konversijas
      if(in_array('conversions', $mpk_filters)) {
        if(!empty($visi_mpk_konversijas[$row['id']])) {
          foreach($visi_mpk_konversijas[$row['id']] as $av) {
            if($av['site'] == 'google') {
              $continue = false;
              break;
            }
          }
        }
      }

      //  Google analytics
      if(in_array('analytics', $mpk_filters)) {
        if(!empty($visi_mpk_analitika[$row['id']])) {
          foreach($visi_mpk_analitika[$row['id']] as $av) {
            if($av['site'] == 'google') {
              $continue = false;
              break;
            }
          }
        }
      }

      //  Yandex metrika
      if(in_array('metrika', $mpk_filters)) {
        if(!empty($visi_mpk_analitika[$row['id']])) {
          foreach($visi_mpk_analitika[$row['id']] as $av) {
            if($av['site'] == 'google') {
              $continue = false;
              break;
            }
          }
        }
      }

      //  Other
      if(in_array('other', $mpk_filters)) {
        if(isset($visi_mpk_other[$row['id']])) {
          $continue = false;
        }
      }

      if($continue) continue;
    }
  }

  $row['liguma_statuss'] = get_liguma_statuss($row, !empty($lig_rekini[$row['id']]) ? $lig_rekini[$row['id']] : null);

  if (pt_multiselect_is_active('flig_statuss')) {

    $show = false;

    if (pt_is_selected($row['liguma_statuss'], 'flig_statuss')) {
      $show = true;
    } elseif ($row['liguma_statuss'] == 6 && (empty($row['beigu_statuss']) || $row['beigu_statuss'] == 'termins') && pt_is_selected('6_termins', 'flig_statuss')) {
      $show = true;
    } elseif ($row['liguma_statuss'] == 6 && $row['beigu_statuss'] == 'parslegts' && pt_is_selected('6_parslegts', 'flig_statuss')) {
      $show = true;
    } elseif ($row['liguma_statuss'] == 6 && $row['beigu_statuss'] == 'atteikums' && pt_is_selected('6_atteikums', 'flig_statuss')) {
      $show = true;
    } elseif ($row['liguma_statuss'] == 6 && !empty($row['beigu_statuss_auto_atteikums']) && pt_is_selected('6_atteikums_p', 'flig_statuss')) {
      $show = true;
    }

    if (!$show) continue;

  }

  $row['norekinu_statuss'] = get_apmaksas_statuss(!empty($lig_rekini[$row['id']]) ? $lig_rekini[$row['id']] : null);

  if (pt_multiselect_is_active('fkopapm_statuss')) {

    $show = false;

    if (pt_is_selected($row['norekinu_statuss'], 'fkopapm_statuss')) {
      $show = true;
    } else if (pt_is_selected('2_barteris', 'fkopapm_statuss')) {
      if ($row['norekinu_statuss'] == 2 && is_barteris(!empty($lig_rekini[$row['id']]) ? $lig_rekini[$row['id']] : null)) $show = true;
    }

    if (!$show) continue;

  }

  $row['max_nokavetas_dienas'] = 0;
  if ($row['norekinu_statuss'] == 4) {
    $row['max_nokavetas_dienas'] = get_max_nokavetas_dienas(!empty($lig_rekini[$row['id']]) ? $lig_rekini[$row['id']] : null);
  }

  //  Maksājumu lietas korp vadītājam.
  //  Tas viss tāpēc ka korp vadītāja līgumos jāredz arī partneru līgumi un ar tiem saistītajām provīziju un izmaksu summām jābūt tām kas attiecas uz vadītāju nevis pārdevēju

  if(1) {
    //  Partnera līgums

    $row['apm_prov_summa'] = db_get_val(db_query('
      SELECT SUM(sd.summa)
      FROM '.DB_PREF.'pardeveji_rekini_detalas sd
      LEFT JOIN '.DB_PREF.'pardeveji_rekini sp ON (sp.id = sd.pardeveja_rekina_id)
      LEFT JOIN '.DB_PREF.'rekini sr ON (sr.id = sd.rekina_id)
      WHERE
        sr.liguma_id = '.$row['id'].' AND
        sp.pardeveja_id = '.$pardevejs->id.' AND
        sp.statuss = 2
    '));

    // pārējos - prov_summa uc nav jēgas te overraidot jo te vēl tālāk ir superīgais variants, ka sākumā izvada nulles un pēc tam vēlreiz visam cauri iet JS kurš pārrēķina un pārrsaksta vērtības...
  }

  //  Savs līgum
  $row['apm_prov_percent'] = 0;
  if ($row['rekinu_summa'] * ($row['provizija_pardevejam'] / 100) > 0) {
    $row['apm_prov_percent'] = $row['apm_prov_summa'] * 100 / ($row['rekinu_summa'] * ($row['provizija_pardevejam'] / 100));
  }


  if (getGet('fapm_prov_percent') !== '') {
    if ((int)floor($row['apm_prov_percent']) !== (int)getGet('fapm_prov_percent')) {
      continue;
    }
  }

  $row['pakalpojumu_saraksts'] = '';

  if (!$row['mpk'] && !empty($pakalpojumi[$row['id']])) {

    $visu_pak_saraksts = array();
    foreach($pakalpojumi[$row['id']] as $pak_parent_id => $v) {
      $pak_saraksts = array();
      foreach($v as $v2) {
        $pak_saraksts[] = $v2['nosaukums'] . (!empty($v2['pakalpojuma_apr']) ? "(".$v2['pakalpojuma_apr'].")" : "");
      }
      $visu_pak_saraksts[] = '<strong>'.$_vars['pakalpojumi'][$pak_parent_id]['nosaukums'].':</strong><br />' . implode(', ', $pak_saraksts);
    }
    $row['pakalpojumu_saraksts'] = implode('<br />', $visu_pak_saraksts);

  } elseif(!empty($mpk_pakalpojumi) && !empty($mpk_pakalpojumi[$row['id']])) {
    $visu_pak_saraksts = array();
    foreach($mpk_pakalpojumi[$row['id']] as $site => $pak) {
      if($site != 'other') {
        $pak_saraksts = array();
        foreach($pak as $pak_type => $count) {
          $pak_saraksts[] = $_vars[$site.'_pakalpojumi'][$pak_type];
        }
        $visu_pak_saraksts[] = '<strong>'.$_vars['mekletaji'][$site].':</strong><br />' . implode(', ', $pak_saraksts);
      } else {
        $visu_pak_saraksts[] = '<strong>Cits</strong>';
      }
    }
    $row['pakalpojumu_saraksts'] = implode('<br />', $visu_pak_saraksts);
  }

  $alldata[] = $row;

}

$total_rows = count($alldata);

// add apply sorting to complex columns

if (getGet('order') == 'pakalpojums') {

  function sort_by_pakalpojums_asc($a, $b) {
    return strncasecmp(strip_tags($a['pakalpojumu_saraksts']), strip_tags($b['pakalpojumu_saraksts']), 20);
  }

  function sort_by_pakalpojums_desc($a, $b) {
    return strncasecmp(strip_tags($b['pakalpojumu_saraksts']), strip_tags($a['pakalpojumu_saraksts']), 20);
  }

  usort($alldata, 'sort_by_pakalpojums_' . $direction);

}

if (getGet('order') == 'lig_statuss') {

  function sort_by_lig_statuss_asc($a, $b) {
    return $a['liguma_statuss'] == $b['liguma_statuss'] ? 0 : (($a['liguma_statuss'] < $b['liguma_statuss']) ? -1 : 1);
  }

  function sort_by_lig_statuss_desc($a, $b) {
    return $a['liguma_statuss'] == $b['liguma_statuss'] ? 0 : (($a['liguma_statuss'] > $b['liguma_statuss']) ? -1 : 1);
  }

  usort($alldata, 'sort_by_lig_statuss_' . $direction);

}

if (getGet('order') == 'kopapm_statuss') {

  function sort_by_kopapm_statuss_asc($a, $b) {

    if ($a['norekinu_statuss'] == $b['norekinu_statuss']) {
      return $a['max_nokavetas_dienas'] == $b['max_nokavetas_dienas'] ? 0 : ($a['max_nokavetas_dienas'] < $b['max_nokavetas_dienas'] ? -1 : 1);
    } else {
      return $a['norekinu_statuss'] < $b['norekinu_statuss'] ? -1 : 1;
    }

  }

  function sort_by_kopapm_statuss_desc($a, $b) {

    if ($a['norekinu_statuss'] == $b['norekinu_statuss']) {
      return $a['max_nokavetas_dienas'] == $b['max_nokavetas_dienas'] ? 0 : ($a['max_nokavetas_dienas'] > $b['max_nokavetas_dienas'] ? -1 : 1);
    } else {
      return $a['norekinu_statuss'] > $b['norekinu_statuss'] ? -1 : 1;
    }

  }

  usort($alldata, 'sort_by_kopapm_statuss_' . $direction);

}

if (getGet('order') == 'izm_prov_proc') {

  function sort_by_izm_prov_proc_asc($a, $b) {
    return $a['apm_prov_percent'] == $b['apm_prov_percent'] ? 0 : (($a['apm_prov_percent'] < $b['apm_prov_percent']) ? -1 : 1);
  }

  function sort_by_izm_prov_proc_desc($a, $b) {
    return $a['apm_prov_percent'] == $b['apm_prov_percent'] ? 0 : (($a['apm_prov_percent'] > $b['apm_prov_percent']) ? -1 : 1);
  }

  usort($alldata, 'sort_by_izm_prov_proc_' . $direction);

}


// limit rows to show

if ($show_in_page > 0) {

  $visibledata = array_slice($alldata, ($active_page - 1) * $show_in_page, $show_in_page);

  $hiddendata = array_slice($alldata, 0, ($active_page - 1) * $show_in_page);

  $hiddendata = array_merge(
    $hiddendata,
    array_slice($alldata, (($active_page - 1) * $show_in_page) + $show_in_page)
  );

} else {
  $visibledata = $alldata;
  $hiddendata = array();
}

$visible_rows = count($visibledata);

?>

<form action="" id="darbinieka_ligumi_form" method="GET" style="clear: both;">


  <input type="hidden" name="c" value="darbinieki" />
  <input type="hidden" name="a" value="labot" />
  <input type="hidden" name="id" value="<?= $pardeveja_id ?>" />
  <input type="hidden" name="subtab" value="ligumi" />

  <input type="hidden" name="order" value="<?= getGet('order') ?>" />
  <input type="hidden" name="direction" value="<?= getGet('direction') ?>" />

  <?php if (isset($_GET['fligumu_id'])) { ?>
    <input name="fligumu_id" type="hidden" value="<?= getGet('fligumu_id') ?>" />
  <?php } ?>

  <input type="hidden" name="page" value="<?= $active_page ?>" />
  <input type="hidden" name="show_in_page" value="<?= $show_in_page ?>" />

  <input class="ui-state-default ui-corner-all" style="width: 5em; visibility: hidden; position: absolute;" type="submit" value="Atlasīt" />

  <?php print_paginator($total_rows, $active_page, $show_in_page) ?>

  <script>
  var searchlist = [];

  var sp = create_filter_field_array([
    's_summa', 's_pvn', 's_kopsumma', 's_apm', 's_kopsumma_bez_pvn',
    's_prov', 's_apm_prov', 's_prov_percent', 's_pardeveja_id'
  ]);
  </script>

  <table cellpadding="3" cellspacing="0" class="data" id="darbinieka_ligumi" style="width: auto;">

    <thead>

      <tr class="groups">
        <th colspan="2"></th>
        <th colspan="2" class="bgc-sta">Statuss</th>
        <th colspan="<?php echo $show_liguma_sledzejs ? 6 : 5;?>" class="bgc-lig">Līgums</th>
        <th colspan="<?php echo $show_pakalpojums ? 3 : 2;?>" class="bgc-pak">Pakalpojums</th>
        <th colspan="<?php echo $show_apkalp_maksa ? 5 : 4;?>" class="bgc-rek">Norēķini</th>
        <th  class="bgc-pro" colspan="<?php echo $show_vaditaja_atl ? 5 : 4;?>">Provīzija</th>
        <th>Atg.</th>
      </tr>

      <tr class="header">
        <th class="header" colname="npk">Npk.</th>

        <? print_th('Nosaukums', 'nosauk', array('width' => 200)) ?>
        <? print_th('Līg. statuss', 'lig_statuss', array('width' => 50)) ?>
        <? print_th('Norēķinu statuss', 'kopapm_statuss', array('width' => 50)) ?>

        <? print_th('Līg. datums', 'lig_datums', array('width' => 50)) ?>
        <? print_th('Līg. nr.', 'lig_nr', array('width' => 60)) ?>
        <?
        if($show_liguma_sledzejs) {
          print_th('Līguma slēdzējs', 'pardevejs', array('width' => 200));
        }
        ?>
        <? print_th('Indekss', 'indekss', array('width' => 40)) ?>
        <? print_th('Saņemš. datums', 'san_datums', array('width' => 50)) ?>
        <? print_th('Saņemš. forma', 'san_forma', array('width' => 50)) ?>

        <? print_th('Līgums no', 'lig_no', array('width' => 50)) ?>
        <? print_th('Līgums līdz', 'lig_lidz', array('width' => 50)) ?>

        <?php if($show_pakalpojums) { ?>
          <? print_th('Pakalpojums', 'pakalpojums', array('width' => 150)) ?>
        <?php } ?>

        <? print_th('Summa<br />(bez PVN)', 'summa', array('width' => 50)) ?>
        <? print_th('PVN', 'pvn', array('width' => 50)) ?>
        <? print_th('Kopsumma<br />(ar PVN)', 'kopsumma', array('width' => 50)) ?>
        <? print_th('Apmaksāts&nbsp;EUR', 'apm_summa', array('width' => 50)) ?>

        <?php if($show_apkalp_maksa) { ?>
          <? print_th('Apkalpošanas maksa (%)', 'apkalp_maksa_proc', array('width' => 150)) ?>
        <?php } ?>

        <?php if($show_vaditaja_atl) { ?>
          <? print_th('Partnera komisija&nbsp;%', 'prov_proc', array('width' => 50)) ?>
        <?php } else { ?>
          <? print_th('Provīzija&nbsp;%', 'prov_proc', array('width' => 50)) ?>
        <?php } ?>

        <?php if($show_vaditaja_atl) { ?>
          <? print_th('Vadītāja provīzija&nbsp;%', 'prov_proc', array('width' => 50)) ?>
        <?php } ?>

        <? print_th('Provīzija&nbsp;EUR<br />(bez PVN)', 'prov_bez_pvn', array('width' => 50)) ?>
        <? print_th('Izmaksātā provīzija<br />EUR (bez PVN)', 'izm_prov_bez_pvn', array('width' => 50)) ?>
        <? print_th('Izmaksātā<br />provīzija %', 'izm_prov_proc', array('width' => 50)) ?>
        <? print_th('Aktīvie<br />atgādinājumi', 'akt_atgad', array('width' => 40)) ?>
      </tr>

      <tr class="filter last">

        <th></th>

        <th><span><input class="sfield" name="fnosaukums" type="input" value="<?= getGet('fnosaukums') ?>" style="width:80%;"><a class="sfield_clear" href="#">x</a></span></th>

        <th>
          <select name="flig_statuss[]" class="sfield" multiple="multiple" size="1" style="width: 70px;">
            <option value=""></option>
            <?php foreach($_vars['liguma_statuss'] as $k=>$p){ ?>
              <option <? pt_print_selected($k, 'flig_statuss') ?> value="<?=$k;?>"><?=$p;?></option>
              <? if ($k == 6) { ?>
                <option <? pt_print_selected('6_termins', 'flig_statuss') ?> value="6_termins">&nbsp;&nbsp;&nbsp;<?= ucfirst($_vars['liguma_beigu_statuss']['termins']) ?></option>
                <option <? pt_print_selected('6_parslegts', 'flig_statuss') ?> value="6_parslegts">&nbsp;&nbsp;&nbsp;<?= ucfirst($_vars['liguma_beigu_statuss']['parslegts']) ?></option>
                <option <? pt_print_selected('6_atteikums', 'flig_statuss') ?> value="6_atteikums">&nbsp;&nbsp;&nbsp;<?= ucfirst($_vars['liguma_beigu_statuss']['atteikums']) ?></option>
                <option <? pt_print_selected('6_atteikums_p', 'flig_statuss') ?> value="6_atteikums_p">&nbsp;&nbsp;&nbsp;(Pārtraukts)</option>
              <? } ?>
            <?php } ?>
          </select>
          <a class="sfield_clear" href="#">x</a>
        </th>

        <th>
          <select name="fkopapm_statuss[]" class="sfield" multiple="multiple" size="1" style="width: 70px;">
            <option value=""></option>
            <?php foreach($_vars['kopapmaksas_statuss'] as $k=>$p){ ?>
              <option <? pt_print_selected($k, 'fkopapm_statuss') ?> value="<?=$k;?>"><?=$p;?></option>
              <? if ($k == 2) { ?>
                <option <? pt_print_selected('2_barteris', 'fkopapm_statuss') ?> value="2_barteris">&nbsp;&nbsp;&nbsp;Barteris</option>
              <? } ?>
            <?php } ?>
          </select>
          <a class="sfield_clear" href="#">x</a>
        </th>

        <th>
          <span><input class="sfield kalendari" name="flig_no" type="input" value="<?= getGet('flig_no') ?>" style="width:80%;"><a class="sfield_clear" href="#">x</a></span><br>
          <span><input class="sfield kalendari" name="flig_lidz" type="input" value="<?= getGet('flig_lidz') ?>" style="width:80%;"><a class="sfield_clear" href="#">x</a></span>
        </th>

        <th><span><input class="sfield" name="fliguma_nr" type="input" value="<?= getGet('fliguma_nr') ?>" style="width:80%;"><a class="sfield_clear" href="#">x</a></span></th>

         <?php if($show_liguma_sledzejs) {?>
        <th>
          <select class="search_field advonchange" name="s_pardeveja_id[]" searchclass="s_pardeveja_id" multiple="multiple" meth="standart" style="width: 100px;">
            <option value=""></option>
            <option value="<?php echo $pardevejs->id;?>" <?php echo !getGet('s_pardeveja_id') || in_array($pardevejs->id, getGet('s_pardeveja_id')) ? 'selected="SELECTED"' : '';?>><?php echo $pardevejs->vards;?></option>
            <?php foreach((array)$pardevejs->getPadotie() as $padotais) { ?>
              <option value="<?php echo $padotais->id;?>" <?php echo !getGet('s_pardeveja_id') || in_array($padotais->id, getGet('s_pardeveja_id')) ? 'selected="SELECTED"' : '';?>><?php echo $padotais->vards;?></option>
            <?php } ?>
          </select>
        </th>
        <?php } ?>

        <th><span><input class="sfield" name="ffaktaddr_index" type="input" value="<?= getGet('ffaktaddr_index') ?>" style="width:80%;"><a class="sfield_clear" href="#">x</a></span></th>

        <th>
          <span><input class="sfield kalendari" name="flig_sanemts_no" type="input" value="<?= getGet('flig_sanemts_no') ?>" style="width:80%;"><a class="sfield_clear" href="#">x</a></span><br>
          <span><input class="sfield kalendari" name="flig_sanemts_lidz" type="input" value="<?= getGet('flig_sanemts_lidz') ?>" style="width:80%;"><a class="sfield_clear" href="#">x</a></span>
        </th>

        <th>
          <select name="fsanems_forma[]" class="sfield" multiple="multiple" size="1" style="width: 50px;">
            <option value=""></option>
            <?php foreach($_vars['sanemsanas_forma'] as $k=>$p){ ?>
              <option <? pt_print_selected($k, 'fsanems_forma') ?> value="<?=$k;?>"><?=$p;?></option>
            <?php } ?>
          </select>
          <a class="sfield_clear" href="#">x</a>
        </th>

        <th>
          <span><input class="sfield kalendari" name="frekl_sak_no" type="input" value="<?= getGet('frekl_sak_no') ?>" style="width:80%;"><a class="sfield_clear" href="#">x</a></span><br>
          <span><input class="sfield kalendari" name="frekl_sak_lidz" type="input" value="<?= getGet('frekl_sak_lidz') ?>" style="width:80%;"><a class="sfield_clear" href="#">x</a></span>
        </th>

        <th>
          <span><input class="sfield kalendari" name="frekl_beig_no" type="input" value="<?= getGet('frekl_beig_no') ?>" style="width:80%;"><a class="sfield_clear" href="#">x</a></span><br>
          <span><input class="sfield kalendari" name="frekl_beig_lidz" type="input" value="<?= getGet('frekl_beig_lidz') ?>" style="width:80%;"><a class="sfield_clear" href="#">x</a></span>
        </th>

         <?php if($show_pakalpojums) { ?>
          <th>

          <span>MPT <select name="fpakalpojums[]" class="sfield" multiple="multiple" size="1" style="width: 100px;">
            <option value=""></option>
            <?php foreach($_vars['mpt_filter'] as $k=>$p){
              if(is_array($p)) { ?>
                <optgroup label="<?php echo $k;?>">
                <?php foreach($p as $j=>$l){ ?>
                  <option <? pt_print_selected($j, 'fpakalpojums') ?> value="<?=$j;?>"><?=$l;?></option>
                <?php } ?>
                </optgroup>
              <?php } else { ?>
                <option <? pt_print_selected($p, 'fpakalpojums') ?> value="<?=$p;?>"><?=$k;?></option>
              <?php } ?>
            <?php } ?>
          </select><a class="sfield_clear" href="#">x</a></span><br />

          <span>MPK <select name="fpakalpojums_mpk[]" class="sfield" multiple="multiple" size="1" style="width: 100px;">
            <option value=""></option>
            <?php foreach($_vars['mpk_filter'] as $k=>$p){
              if(is_array($p)) { ?>
                <optgroup label="<?php echo $k;?>">
                <?php foreach($p as $j=>$l){ ?>
                  <option <? pt_print_selected($j, 'fpakalpojums_mpk') ?> value="<?=$j;?>"><?=$l;?></option>
                <?php } ?>
                </optgroup>
              <?php } else { ?>
                <option <? pt_print_selected($p, 'fpakalpojums_mpk') ?> value="<?=$p;?>"><?=$k;?></option>
              <?php } ?>
            <?php } ?>
          </select><a class="sfield_clear" href="#">x</a></span>
        </th>
        <?php } ?>

        <th><span><input class="sfield" name="flig_summa" type="input" value="<?= getGet('flig_summa') ?>" style="width:80%;"><a class="sfield_clear" href="#">x</a></span></th>
        <th><span><input class="sfield" name="flig_pvn" type="input" value="<?= getGet('flig_pvn') ?>" style="width:80%;"><a class="sfield_clear" href="#">x</a></span></th>
        <th><span><input class="sfield" name="flig_kopsumma" type="input" value="<?= getGet('flig_kopsumma') ?>" style="width:80%;"><a class="sfield_clear" href="#">x</a></span></th>
        <th><span><input class="sfield" name="fapmaks_summa" type="input" value="<?= getGet('fapmaks_summa') ?>" style="width:80%;"><a class="sfield_clear" href="#">x</a></span></th>

        <?php if($show_apkalp_maksa) { ?>
           <th><span><input class="sfield" name="fapkalp_maksa_proc" type="input" value="<?= getGet('fapkalp_maksa_proc') ?>" style="width:80%;"><a class="sfield_clear" href="#">x</a></span></th>
        <?php } ?>

        <th><span><input class="sfield" name="fprov_percent" type="input" value="<?= getGet('fprov_percent') ?>" style="width:80%;"><a class="sfield_clear" href="#">x</a></span></th>

        <?php if($show_vaditaja_atl) {?>
         <th><span><input class="sfield" name="fatl_vad" type="input" value="<?= getGet('fatl_vad') ?>" style="width:80%;"><a class="sfield_clear" href="#">x</a></span></th>
        <?php } ?>

        <th><span><input class="sfield" name="fprov_summa" type="input" value="<?= getGet('fprov_summa') ?>" style="width:80%;"><a class="sfield_clear" href="#">x</a></span></th>
        <th><span><input class="sfield" name="fapm_prov_summa" type="input" value="<?= getGet('fapm_prov_summa') ?>" style="width:80%;"><a class="sfield_clear" href="#">x</a></span></th>
        <th><span><input class="sfield" name="fapm_prov_percent" type="input" value="<?= getGet('fapm_prov_percent') ?>" style="width:80%;"><a class="sfield_clear" href="#">x</a></span></th>

        <th>
          <select name="fakt_atgad[]" class="sfield" multiple="multiple" size="1" style="width: 40px;">
            <option value=""></option>
            <option <? pt_print_selected(1, 'fakt_atgad') ?> value="1">Ir</option>
            <option <? pt_print_selected(2, 'fakt_atgad') ?> value="2">Nav</option>
          </select>
          <a class="sfield_clear" href="#">x</a>
        </th>

      </tr>

    </thead>

    <tbody class="main">

      <?php
      $npk = $total_rows - (($active_page - 1) * $show_in_page);
      $inid = -1;

      foreach($visibledata as $row) {

        $inid ++;

        ?>

        <tr id="lig_tr<?=$inid;?>" class="lig_row" title="<?=$row['nosaukums'] ?>">
          <td class="c s_id"><?=$npk;?></td>


          <td>
            <a href="?c=ligumi&a=labot&id=<?=$row['id'];?>"><b><?=$row['nosaukums'] ?></b></a>

            <? if (!empty($row['saistito_pardeveju_skaits'])) { ?>

              <?php
              $lig_saist_pard = array();
              if (!empty($saist_pard[$row['id']])) {
                foreach($saist_pard[$row['id']] as $v) {
                  $lig_saist_pard[] = $v['vards'];
                }
              }
              ?>

              <span class="saist_pard_ico" title="Saistītie pārdevēji: <?php echo implode(', ', $lig_saist_pard) ?>">s</span>

            <? } ?>

          </td>

          <? if ($row['liguma_statuss'] == 6) { ?>
            <? $active = (empty($row['beigu_statuss']) || ($row['beigu_statuss'] == 'termins' && $row['beigu_statuss_atlikt_lidz'] <= date('Y-m-d'))) ? true : false; ?>
            <td class="c">
              <div class="lig_statuss_6 <?= !empty($row['beigu_statuss']) ? 'lig_beigu_statuss_' . $row['beigu_statuss'] : 'lig_beigu_statuss_termins' ?>">
                <a class="edit_link" href="#" onclick="showLigumaBeiguStatussDialog(<?= $row['id'] ?>); return false;"><img src="css/edit.png" alt="Labot statusu" title="Labot statusu" /></a>
                <span class="<?= $active ? 'lig_beigu_statuss_active' : '' ?>">
                  <span class="stat_name"><?= $_vars['liguma_statuss'][6] ?></span>
                  (<?= !empty($row['beigu_statuss']) ? $_vars['liguma_beigu_statuss'][$row['beigu_statuss']] : $_vars['liguma_beigu_statuss']['termins'] ?><?= !empty($row['beigu_statuss_auto_atteikums']) ? ', <span title="Līgums pirms reklāmas beigām tika pārtraukts">P</span>': '' ?>)
                </span>
              </div>
            </td>
          <? } else { ?>
            <td class="c"><?= !empty($_vars['liguma_statuss'][$row['liguma_statuss']]) ? '<div class="'.( !empty($_vars['liguma_statuss'][$row['liguma_statuss']]) ? 'lig_statuss_'.$row['liguma_statuss'] : '' ).'">' . $_vars['liguma_statuss'][$row['liguma_statuss']] . '</div>' : ''?></td>
          <? } ?>

          <? if ($row['norekinu_statuss'] == 2 && is_barteris(!empty($lig_rekini[$row['id']]) ? $lig_rekini[$row['id']] : null)) { ?>
            <td class="c">
              <div class="norek_statuss_2">
                <a href="?c=ligumi&a=labot&id=<?=$row['id'];?>&subtab=2">Barteris</a>
              </div>
            </td>
          <? } else { ?>
            <td class="c">
              <div class="<?= !empty($_vars['kopapmaksas_statuss'][$row['norekinu_statuss']]) ? 'norek_statuss_'.$row['norekinu_statuss'] : "" ?>">
                <?= !empty($_vars['kopapmaksas_statuss'][$row['norekinu_statuss']]) ? '<a href="?c=ligumi&a=labot&id='.$row['id'].'&subtab=2" class="norek_statuss_'.$row['norekinu_statuss'].'">' . $_vars['kopapmaksas_statuss'][$row['norekinu_statuss']] . '</a>' : "" ?>

                <? if ($row['norekinu_statuss'] == 4) { ?>
                  <span style="font-size: 0.75em;">(<?= $row['max_nokavetas_dienas'] ?>)</span>
                <? } ?>
              </div>
            </td>
          <? } ?>

          <td class="c"><strong><?= !empty($row['ligumadat'])?date("d.m.Y",strtotime($row['ligumadat'])):"" ?></strong></td>
          <td class="c"><?= $row['ligumanr'] ?></td>
          <?php if($show_liguma_sledzejs) {?>
            <td><a href="?c=<?php echo Pardevejs::model()->findByPk($row['pardeveja_id'])->tips == 'partner' ? 'partneri' : 'darbinieki';?>&a=labot&id=<?php echo $row['pardeveja_id'];?>"><?php echo $row['p_vards'];?></a></td>
          <?php } ?>
          <td class="c"><?= preg_replace('/^lv\-/i', '', $row['faktaddr_index']) ?></td>
          <td class="c"><em><?= !empty($row['sanemsanas_datums'])?date("d.m.Y",strtotime($row['sanemsanas_datums'])):"" ?></em></td>
          <td class="c"><?= !empty($row['sanemsanas_forma']) ? '<div class="'.( !empty($row['sanemsanas_forma']) ? 'sanform_statuss_'.$row['sanemsanas_forma'] : '' ).'">' . $_vars['sanemsanas_forma'][$row['sanemsanas_forma']] . '</div>' : '' ?></td>

          <td class="c"><strong><?= !empty($row['reklama_no'])?date("d.m.Y",strtotime($row['reklama_no'])):"" ?></strong></td>
          <td class="c"><strong><?= !empty($row['reklama_lidz'])?date("d.m.Y",strtotime($row['reklama_lidz'])):"" ?></strong></td>

          <?php if($show_pakalpojums) {?>
            <td><?php echo $row['mpk'] ? '<strong style="color:green">MPK</strong>' : '<strong style="color: blue;">MPT</strong>';?><br /><?= $row['pakalpojumu_saraksts'] ?></td>
          <?php } ?>

          <td class="r"><strong><?= format_currency($row['rekinu_summa']) ?></strong></td>
          <td class="r"><?= format_currency($row['rekinu_kopsumma'] - $row['rekinu_summa']) ?></td>
          <td class="r"><?= format_currency($row['rekinu_kopsumma']) ?></td>
          <td class="r"><?= format_currency($row['apm_kopsumma']) ?></td>

          <?php if($show_apkalp_maksa) { ?>
             <td class="r"><?= $row['apkalp_maksa_proc']; ?>%</td>
          <?php } ?>

          <?php if($show_vaditaja_atl) {
          if($row['pardeveja_id'] == $pardevejs->id && $pardevejs->id == KORP_VADITAJA_ID) {
            //  Ja šitas līgums ir vadītājam tad rāda - | provīzija
            ?>
              <td class="c atl_vad" style="white-space: nowrap;">-</td>
              <td class="c prov_percent prov_percent_hack" style="white-space: nowrap;">
                <span style="display:none;"><?= round($row['provizija_pardevejam'], 2) ?></span>
                <input class="disablethistoo" style="text-align: center; width:2em;" type="text" name="lig_provizija[<?= $row['id'] ?>]" value="<?= round($row['provizija_pardevejam'], 2) ?>" />%
              </td>
          <? } else {
            //  Ja partnera līgums tad - partnera provīzija | vadītāja atlīdzība
            ?>
              <td class="c prov_percent" style="white-space: nowrap;">
                <span style="display:none;"><?= round($row['provizija_pardevejam'], 2) ?></span>
                <input class="disablethistoo" style="text-align: center; width:2em;" type="text" name="lig_provizija[<?= $row['id'] ?>]" value="<?= round($row['provizija_pardevejam'], 2) ?>" />%
              </td>
              <td class="c atl_vad prov_percent_hack" style="white-space: nowrap;">
                <span style="display:none;"><?= round($row['atlidziba_vaditajam'], 2) ?></span>
                <input class="<?php echo check_access('partneri-vaditajs') ? 'disablethistoo canedit' : 'always_disabled';?>" style="text-align: center; width:2em;" type="text" name="atlidziba_vaditajam[<?= $row['id'] ?>]" value="<?= round($row['atlidziba_vaditajam'], 2) ?>" />%
              </td>
          <?php }
            } else {
              //  Nav vadītāja profils. Rādam tikai provīziju
            ?>
            <td class="c prov_percent prov_percent_hack" style="white-space: nowrap;">
                <span style="display:none;"><?= round($row['provizija_pardevejam'], 2) ?></span>
                <input class="disablethistoo" style="text-align: center; width:2em;" type="text" name="lig_provizija[<?= $row['id'] ?>]" value="<?= round($row['provizija_pardevejam'], 2) ?>" />%
            </td>
          <?php } ?>

          <td class="r prov"><span>0</span></td>
          <td class="r prov_apm"><span><?= format_currency($row['apm_prov_summa']) ?></span></td>
          <td class="r prov_apm_percent"><span>0</span></td>

          <td class="c"><a href="?c=ligumi&a=labot&id=<?= $row['id'] ?>&subtab=3"><?= (!empty($row['aktivi_atgadinajumi'])) ? 'Ir ('.$row['aktivi_atgadinajumi'].')' : 'Nav' ?></a></td>

          <td style="display: none;">
            <script>
              searchlist[<?=$inid;?>] = '<?=$row['id'];?>';

              sp['s_pardeveja_id'][<?=$inid;?>] = '<?= $row['pardeveja_id'] ?>';

              sp['s_summa'][<?=$inid;?>] = '<?=!empty($row['rekinu_summa']) ? $row['rekinu_summa'] : "" ?>';
              sp['s_pvn'][<?=$inid;?>] = '<?= round($row['rekinu_kopsumma'] - $row['rekinu_summa'], 2) ?>';
              sp['s_kopsumma'][<?=$inid;?>] = '<?=!empty($row['rekinu_kopsumma']) ? $row['rekinu_kopsumma'] : "" ?>';
              sp['s_apm'][<?=$inid;?>] = '<?=!empty($row['apm_kopsumma']) ? $row['apm_kopsumma'] : "" ?>';
              sp['s_prov'][<?=$inid;?>] = 0;
              sp['s_apm_prov'][<?=$inid;?>] = '<?= $row['apm_prov_summa'] ?>';

              // this is for calculations only
              sp['s_kopsumma_bez_pvn'][<?=$inid;?>] = '<?=!empty($row['rekinu_summa']) ? $row['rekinu_summa'] : "" ?>';

            </script>
          </td>
        </tr>

        <? $npk -- ?>

      <?php } ?>


    </tbody>

    <tr>

      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>

      <?php if($show_liguma_sledzejs) { ?>
        <td></td>
      <?php } ?>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <?php if($show_pakalpojums) {?>
         <td></td>
      <?php } ?>
      <td align="right"><strong>Kopā:</strong></td>
      <td class="r" id="ligumusumma" style="font-weight: bold;">0</td>
      <td class="r" id="pvnsumma">0</td>
      <td class="r" id="ligumukopsumma">0</td>
      <td class="r" id="apmsumma">0</td>
      <td></td>
      <?php if($show_vaditaja_atl) { ?>
        <td></td>
      <?php } ?>
      <td class="r" id="provsumma">0</td>
      <td class="r" id="apmprovsumma">0</td>
      <td></td>
      <td></td>

    </tr>

    <tr>

      <td></td>

      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>

      <?php if($show_liguma_sledzejs) { ?>
        <td></td>
      <?php } ?>

      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <?php if($show_pakalpojums) {?>
         <td></td>
      <?php } ?>
      <td align="right"><strong>Vidēji:</strong></td>
      <td class="r" id="ligumusummavid" style="font-weight: bold;">0</td>
      <td></td>
      <td class="r" id="ligumukopsummavid">0</td>
      <td></td>
      <!--<td class="c" id="provpercentvid">0</td>-->
      <td class="c" id="provpercentvid"></td>

      <?php if($show_vaditaja_atl) { ?>
        <!--<td class="c" id="atlvadvid">0</td>-->
        <td class="c" id="atlvadvid"></td>
      <?php } ?>

      <td class="r" id="provsummavid">0</td>
      <td class="r" id="apmprovsummavid">0</td>
      <td class="r" id="apmprovpercentvid">0</td>
      <td></td>

    </tr>

  </table>

  <?php print_paginator($total_rows, $active_page, $show_in_page) ?>

</form>

<? if (!empty($hiddendata)) { ?>

  <?
  // we are adding contracts from other pages to sp and searchlist arrays as well
  // this data is for totals calculations
  ?>

  <script>

    <? foreach($hiddendata as $row) { ?>

      <? $inid ++ ?>

      searchlist[<?=$inid;?>] = '<?=$row['id'];?>';

      sp['s_prov_percent'][<?=$inid;?>] = <?= round($row['provizija_pardevejam'], 2) ?>;

      sp['s_summa'][<?=$inid;?>] = '<?=!empty($row['rekinu_summa']) ? $row['rekinu_summa'] : "" ?>';
      sp['s_pvn'][<?=$inid;?>] = '<?= round($row['rekinu_kopsumma'] - $row['rekinu_summa'], 2) ?>';
      sp['s_kopsumma'][<?=$inid;?>] = '<?=!empty($row['rekinu_kopsumma']) ? $row['rekinu_kopsumma'] : "" ?>';
      sp['s_apm'][<?=$inid;?>] = '<?=!empty($row['apm_kopsumma']) ? $row['apm_kopsumma'] : "" ?>';
      sp['s_prov'][<?=$inid;?>] = 0;
      sp['s_apm_prov'][<?=$inid;?>] = '<?= $row['apm_prov_summa'] ?>';
      sp['s_pardeveja_id'][<?=$inid;?>] = '<?= $row['pardeveja_id'] ?>';

      // this is for calculations only
      sp['s_kopsumma_bez_pvn'][<?=$inid;?>] = '<?=!empty($row['rekinu_summa']) ? $row['rekinu_summa'] : "" ?>';

    <? } ?>

  </script>

<? } ?>

<form id="fullformplace" action="?c=darbinieki&a=labot&id=<?= $pardeveja_id ?>&subtab=ligumi" method="post" enctype="multipart/form-data" style="clear: both;">

  <? show_comments_block('darb_ligumi', $pardeveja_id) ?>

  <? show_files_block('darb_ligumi', $pardeveja_id) ?>

  <div class="hidethistoo non_admin_edit">
    <input type="submit" onClick="return saveAll(this, 'darbinieks_ligumi')" class="non_admin_edit submit show_in_act_panel" value="Saglabāt">
    <input type="button" onclick="return cancelSaves();" class="non_admin_edit show_in_act_panel" value="Atcelt">
  </div>

</form>

<script>

$(document).ready(function() {

  processTable();

  $('.prov_percent input').keyup(function() {
    processTable();
  });

  $('.saist_pard_ico').wTooltip({
    follow: true,
    offsetY: 0,
    offsetX: 10
  });

  initPlainSortableTable($('#darbinieka_ligumi_form'));

  initTableCols($('#darbinieka_ligumi'), 'darb_ligumi', <?= Zend_Json::encode(get_tab_kolonnas('darb_ligumi')) ?>);

  initTableGroupColors($('#darbinieka_ligumi'));

  $('#fullformplace').submit(function() {

    var form = $(this);

    form.find('#prov_percent_inputs').remove();
    form.append('<div id="prov_percent_inputs"></div>')

    var inputs_wrap = form.find('#prov_percent_inputs');

    inputs_wrap.css({display: 'none'});

    $('.prov_percent input').each(function() {

      inputs_wrap.append($(this).clone());

    });

    <?php if($show_vaditaja_atl) { ?>
      //  Vadītāja atlīdzība
      form.find('#atl_vad_inputs').remove();
      form.append('<div id="atl_vad_inputs"></div>')

      var atl_vad_inputs = form.find('#atl_vad_inputs');

      atl_vad_inputs.css({display: 'none'});

      $('.atl_vad input').each(function() {

        atl_vad_inputs.append($(this).clone());

      });
    <?php } ?>

  });

});

function processTable() {

  var ligumuskaits = 0;
  var ligumusumma = 0;
  var pvnsumma = 0;
  var ligumukopsumma = 0;
  var apmsumma = 0;
  var provsumma = 0;
  var apmprovsumma = 0;

  var vid_prov_percent = 0;


  var is_visible;

  jQuery.each(searchlist, function(k,v){

    is_visible = true;

    if (typeof(sp['s_prov_percent'][k]) != 'undefined') {
      is_visible = false;

    }

    if (is_visible) {
      var row = $('#lig_tr' + k);
      var prov_percent = $(".prov_percent_hack input", row).val();
    } else {
      var prov_percent = sp['s_prov_percent'][k];
    }

    var prov = sp['s_kopsumma_bez_pvn'][k] * (prov_percent / 100);
    prov = prov.toFixed(2);

    sp['s_prov'][k] = prov;

    var prov_apm_percent = 0;
    if (prov > 0) {
      prov_apm_percent = sp['s_apm_prov'][k] * 100 / prov;
    }
    prov_apm_percent = Math.round(prov_apm_percent * 100) / 100;

    vid_prov_percent += parseFloat(prov_percent);

    ligumuskaits ++;

    if (is_visible) {
      $(".prov_apm_percent span", row).html(prov_apm_percent + '%');
      $(".prov span", row).html(prov);
      $(".prov_percent span", row).html(prov_percent);
    }

    if(sp['s_summa'][k] > 0) ligumusumma += parseFloat(sp['s_summa'][k].replace(',','.'));
    if(sp['s_pvn'][k] > 0) pvnsumma += parseFloat(sp['s_pvn'][k].replace(',','.'));
    if(sp['s_kopsumma'][k] > 0) ligumukopsumma += parseFloat(sp['s_kopsumma'][k].replace(',','.'));
    if(sp['s_apm'][k] > 0) apmsumma += parseFloat(sp['s_apm'][k].replace(',','.'));
    if(sp['s_prov'][k] > 0) provsumma += parseFloat(sp['s_prov'][k].replace(',','.'));
    if(sp['s_apm_prov'][k] > 0) apmprovsumma += parseFloat(sp['s_apm_prov'][k].replace(',','.'));

  });

  $('#ligumusumma').html('EUR&nbsp;' + parseFloat(ligumusumma).toFixed(2));
  $('#ligumusummavid').html('EUR&nbsp;' + parseFloat(ligumusumma / ligumuskaits).toFixed(2) );

  $('#pvnsumma').html('EUR&nbsp;' + parseFloat(pvnsumma).toFixed(2));

  $('#ligumukopsumma').html('EUR&nbsp;' + parseFloat(ligumukopsumma).toFixed(2));
  $('#ligumukopsummavid').html('EUR&nbsp;' + parseFloat(ligumukopsumma / ligumuskaits).toFixed(2));

  $('#apmsumma').html('EUR&nbsp;' + parseFloat(apmsumma).toFixed(2));

  $('#provpercentvid').html((parseFloat(provsumma / ligumusumma) * 100).toFixed(1) + '%')

  $('#provsumma').html('EUR&nbsp;' + parseFloat(provsumma).toFixed(2));
  $('#provsummavid').html('EUR&nbsp;' + parseFloat(provsumma / ligumuskaits).toFixed(2));

  $('#apmprovsumma').html('EUR&nbsp;' + parseFloat(apmprovsumma).toFixed(2));
  $('#apmprovsummavid').html('EUR&nbsp;' + parseFloat(apmprovsumma / ligumuskaits).toFixed(2));

  $('#apmprovpercentvid').html((Math.round(parseFloat(((apmprovsumma / ligumuskaits) * 100) / (provsumma / ligumuskaits)) * 100) / 100) + '%');

}
</script>