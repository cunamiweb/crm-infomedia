<?php
if(!check_access('darbinieki-statistika') && ($pardeveja_id != $_SESSION['user']['id'] || !check_access('darbinieki-sava-statistika'))) {
  die('Jums nav pieejas tiesību šai sadaļai');
}

log_add("atvera", "Atvēra darbinieka " . $d['vards'] . " statistiku");

$data = array();

if (!empty($pardeveja_id)) {

  $sel_year = (!empty($_GET['stats_year']) && $_GET['stats_year'] <= date('Y')) ? $_GET['stats_year'] : date('Y');

  $quarter_from = $sel_year . '-1';
  $quarter_to = $sel_year . '-4';

  if ($sel_year == date('Y')) {

    if (date('m') <= 3) {
      $show_till = 1;
    } elseif (date('m') <= 6) {
      $show_till = 2;
    } elseif (date('m') <= 9) {
      $show_till = 3;
    } elseif (date('m') <= 12) {
      $show_till = 4;
    }

  } else {
    $show_till = 4;
  }

  $data = get_statistika('quarter', $quarter_from, $quarter_to, $pardeveja_id, $sel_year . '-' . $show_till);

}
?>

<form action="" method="get" id="stats_filter">
  <input type="hidden" name="c" value="darbinieki" />
  <input type="hidden" name="a" value="labot" />
  <input type="hidden" name="subtab" value="statistika" />
  <input type="hidden" name="id" value="<?= $pardeveja_id ?>" />
  <div>
    Gads:
    <span>
      <select id="stats_year" name="stats_year" type="input">
        <? for($year = 2011; $year <= date('Y'); $year ++) { ?>
          <option <?= ($year == $sel_year) ? 'selected="selected"' : '' ?> value="<?= $year ?>"><?= $year ?></option>
        <? } ?>
      </select>
    </span>
	</div>
</form>

<form action="?c=darbinieki<?= !empty($pardeveja_id) ? '&a=labot&id=' . $pardeveja_id . '&subtab=statistika' : '&a=jauns' ;?>" id="fullformplace" method="post" enctype="multipart/form-data">

  <table cellpadding="3" cellspacing="0" class="data" id="ceturksnu_statistika" style="width: auto;">

    <?php print_statistika_head() ?>

    <tbody>

      <?php
      foreach ($data as $i => $d) {
        print_statistika_rows($i, $d, array('pardeveja_id' => $pardeveja_id));
      }
      ?>

    </tbody>

  </table>

  <? show_comments_block('darb_statistika', isset($_GET['id']) ? $_GET['id'] : null) ?>

  <? show_files_block('darb_statistika', isset($_GET['id']) ? $_GET['id'] : null) ?>

  <div class="hidethistoo non_admin_edit">
    <input type="submit" onClick="return saveAll(this, 'darbinieks_statistika')" class="non_admin_edit submit show_in_act_panel" value="Saglabāt">
    <input type="button" onclick="return cancelSaves();" class="non_admin_edit show_in_act_panel" value="Atcelt">
  </div>

  <input type="hidden" name="form_sent" value="1" />

  <script>
  initStatistikaTable();

  $("form#stats_filter").each(function() {

    var form = $(this);

    $('select#stats_year', form).change(function() {
      form.submit();
    });

  });
  </script>

</form>