<?
//  Savi līgumi
$pardeveju_ids = array($pardevejs->id);

//  Ja useris ir vadītājs un viņam ir padotie tad ņem arī padoto līgumu maksājumus
if($pardevejs->id == KORP_VADITAJA_ID) {
  $padotie = $pardevejs->getPadotie();
  foreach((array)$padotie as $padotais) {
    if($padotais->tips == 'partner') {
      $pardeveju_ids[] = $padotais->id;
    }
  }
}

  $query = db_query("
    SELECT
      m.*,
      g.id as liguma_id,
      g.nosaukums as klienta_nosaukums,
      r.pvn as rekina_pvn,
      g.provizija_pardevejam,
      g.atlidziba_vaditajam,
      r.rek_nr,
      r.izr_datums as rekina_izr_datums,
      r.kopsumma as rekina_kopsumma,
      (
        SELECT SUM(no_summas)
        FROM `".DB_PREF."pardeveji_rekini_detalas` d2
        LEFT JOIN ".DB_PREF."pardeveji_rekini pr2 ON pr2.id = d2.pardeveja_rekina_id
        WHERE
          rekina_id = m.rekina_id
          AND pr2.pardeveja_id = ".$pardevejs->id."
      ) as prov_izmaksu_summa,
      p.pvn_likme,
      g.pardeveja_id
    FROM `".DB_PREF."rekini_maksajumi` m
    LEFT JOIN `".DB_PREF."rekini` r ON (m.rekina_id = r.id)
    LEFT JOIN `".DB_PREF."ligumi` g ON (r.liguma_id = g.id)
    LEFT JOIN (
    	SELECT d1.* FROM `".DB_PREF."pardeveji_rekini_detalas` d1
    	LEFT JOIN ".DB_PREF."pardeveji_rekini pr ON (d1.pardeveja_rekina_id = pr.id)
    	WHERE pr.pardeveja_id = ".$pardevejs->id."
    ) d ON (m.id = d.rekina_maksajuma_id)
    LEFT JOIN `".DB_PREF."pardeveji` p ON (p.id = g.pardeveja_id)
    WHERE
      g.pardeveja_id IN (".implode(', ', $pardeveju_ids).") AND
      d.id IS NULL AND
      r.atkapsanas_rekins = 0 AND
      (
        r.nodots_piedz_datums IS NULL OR
        r.nodots_piedz_datums > m.datums
      )
    HAVING
      m.summa <= IFNULL(r.kopsumma, 0) - IFNULL(prov_izmaksu_summa, 0)
    ORDER BY m.datums DESC, m.id DESC
  ");

  $apmaksas = array();
  while($row = db_get_assoc($query)) {
    $apmaksas[$row['id']] = $row;
  }
  ?>

  <h4>Saņemtās provīzijas</h4>

  <script>
  var searchlist_uzizm = [];

  var sp_uzizm = create_filter_field_array([
    's_klienta_nosaukums', 's_rek_nr', 's_rek_izr_datums', 's_summa', 's_datums',
    's_prov_percent', 's_prov_summa', 's_prov_pvn', 's_prov_kopsumma', 's_pardeveja_id'
  ]);
  </script>

  <table style="display:none;">

    <tr id="uz_izmaksu_filter">

      <th><span><input class="search_field" meth="standart" searchclass="s_klienta_nosaukums" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>

      <th>
        <select class="search_field advonchange" searchclass="s_pardeveja_id" multiple="multiple" meth="standart" style="width: 100px;">
          <option value=""></option>
          <option value="<?php echo $pardevejs->id;?>" selected="SELECTED"><?php echo $pardevejs->vards;?></option>
          <?php foreach((array)$pardevejs->getPadotie() as $padotais) { ?>
            <option value="<?php echo $padotais->id;?>" selected="SELECTED"><?php echo $padotais->vards;?></option>
          <?php } ?>
        </select>
      </th>

      <th><span><input class="search_field" meth="standart" searchclass="s_rek_nr" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>

      <th>
        <span><input class="search_field kalendari" meth="from" searchclass="s_rek_izr_datums" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span><br>
        <span><input class="search_field kalendari" meth="to" searchclass="s_rek_izr_datums" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span>
      </th>

      <th><span><input class="search_field" meth="coin" searchclass="s_summa" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>

      <th>
        <span><input class="search_field kalendari" meth="from" searchclass="s_datums" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span><br>
        <span><input class="search_field kalendari" meth="to" searchclass="s_datums" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span>
      </th>

      <th><span><input class="search_field" meth="coin" searchclass="s_prov_percent" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
      <th><span><input class="search_field" meth="coin" searchclass="s_prov_summa" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
      <th><span><input class="search_field" meth="coin" searchclass="s_prov_pvn" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
      <th><span><input class="search_field" meth="coin" searchclass="s_prov_kopsumma" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>

    </tr>

  </table>

  <table id="uz_izmaksu" class="data" style="width: 970px;">

    <thead>

      <tr class="last header">
        <th>Klients</th>
        <th>Līguma slēdzējs</th>
        <th>Rēķina nr.</th>
        <th>Rēķins izstādīts</th>
        <th>Saņemtā summa EUR</th>
        <th>Apmaksas datums</th>
        <th>Provīzija %</th>
        <th>Bez PVN EUR</th>
        <th>PVN EUR</th>
        <th>Kopā EUR</th>
      </tr>

    </thead>

    <tbody class="main">

      <? $inid = -1; ?>

      <? foreach($apmaksas as $apmaksa) { ?>

        <? $inid ++;  ?>

        <?
        $apm_summa_bez_pvn = $apmaksa['summa'] / (1 + ($apmaksa['rekina_pvn'] / 100));

        if($apmaksa['pardeveja_id'] == $pardevejs->id) {
          $komisija = $apmaksa['provizija_pardevejam'];
        } else {
          //  Apskatās kāda vadītāja komisija norādīta partnerim
          /*if($padotais = Pardevejs::model()->findByPk($apmaksa['pardeveja_id'])) {
           $komisija = $padotais->atlidziba_vaditajam;
          } else {
            throw new Exception('Nevar atrast padoto pēc ID = ' . $apmaksa['pardeveja_id']);
          }  */
          $komisija = $apmaksa['atlidziba_vaditajam'];
        }

        //  Ja nedabūja komisijas vērtību vai, ja tā ir 0, tad nemaz nerāda šo maksājumu
        if(!isset($komisija) || !$komisija) {
          continue;
        }

        $prov_summa = $apm_summa_bez_pvn * ($komisija / 100);

        $prov_pvn = $prov_summa * ($apmaksa['pvn_likme'] / 100);
        ?>

        <tr id="uzizm_tr<?=$inid;?>" class="tr_uzizm">
          <td><a href="?c=ligumi&a=labot&id=&a=labot&id=<?= $apmaksa['liguma_id'] ?>&subtab=2"><?= $apmaksa['klienta_nosaukums'] ?></a></td>
          <td><?php echo Pardevejs::model()->findByPk($apmaksa['pardeveja_id'])->vards;?></td>
          <td><?= $apmaksa['rek_nr'] ?></td>
          <td class="c"><?= date('d.m.Y', strtotime($apmaksa['rekina_izr_datums'])) ?></td>
          <td class="r"><?= $apmaksa['summa'] ?></td>
          <td class="c"><?= date('d.m.Y', strtotime($apmaksa['datums'])) ?></td>
          <td class="c"><?= round($komisija, 2) ?>%</td>
          <td class="r"><?= format_currency($prov_summa) ?></td>
          <td class="r"><?= format_currency($prov_pvn) ?></td>
          <td class="r last"><strong><?= format_currency($prov_summa + $prov_pvn) ?></strong></td>
          <td style="display: none;">
            <script>

              searchlist_uzizm[<?=$inid;?>] = '<?=addslashes(mb_strtoupper($apmaksa['rek_nr']));?>';

              sp_uzizm['s_klienta_nosaukums'][<?=$inid;?>] = '<?=addslashes(mb_strtoupper($apmaksa['klienta_nosaukums']));?>';
              sp_uzizm['s_pardeveja_id'][<?=$inid;?>] = '<?=$apmaksa['pardeveja_id'];?>';
              sp_uzizm['s_rek_nr'][<?=$inid;?>] = '<?=addslashes(mb_strtoupper($apmaksa['rek_nr']));?>';
              sp_uzizm['s_rek_izr_datums'][<?=$inid;?>] = <?= !empty($apmaksa['rekina_izr_datums']) ? date('Ymd', strtotime($apmaksa['rekina_izr_datums'])) : 0 ?>;
              sp_uzizm['s_summa'][<?=$inid;?>] = '<?=!empty($apmaksa['summa']) ? $apmaksa['summa'] : "" ?>';
              sp_uzizm['s_datums'][<?=$inid;?>] = <?= !empty($apmaksa['datums']) ? date('Ymd', strtotime($apmaksa['datums'])) : 0 ?>;
              sp_uzizm['s_prov_percent'][<?=$inid;?>] = '<?=!empty($apmaksa['provizija_pardevejam']) ? $apmaksa['provizija_pardevejam'] : "" ?>';
              sp_uzizm['s_prov_summa'][<?=$inid;?>] = '<?=!empty($prov_summa) ? $prov_summa : "" ?>';
              sp_uzizm['s_prov_pvn'][<?=$inid;?>] = '<?=!empty($prov_pvn) ? $prov_pvn : "" ?>';
              sp_uzizm['s_prov_kopsumma'][<?=$inid;?>] = '<?=$prov_summa + $prov_pvn ?>';

            </script>
          </td>
        </tr>

      <? } ?>

    </tbody>

    <tr>

      <td align="right" colspan="4"><strong>Kopā:</strong></td>
      <td class="r" id="uzizm_sanemta_summa">0</td>
      <td></td>
      <td></td>
      <td class="r" id="uzizm_prov_summa">0</td>
      <td class="r" id="uzizm_prov_pvn">0</td>
      <td class="r" id="uzizm_prov_kopsumma" style="font-weight: bold;">0</td>

    </tr>

  </table>

  <script>
  $(document).ready(function() {

    var table = $("#uz_izmaksu");
    var head = $("#uz_izmaksu_filter");

    table.tablesorter({
      debug: false,
      highlightClass: 'highlight',
      headers: {
        0: {sorter: 'latviantext'},
        3: {sorter: 'latviandate'},
        5: {sorter: 'latviandate'}
      }
    }).bind('sortStart', function() {
      table.trigger("update");
    }).bind('sortEnd', function() {
      stripeTable(table);
    });

    initTableFilter(table, head, processUzIzmTable, 'uzizm', searchlist_uzizm, sp_uzizm);

  });

  function processUzIzmTable() {

    var uzizm_sanemta_summa = 0;
    var uzizm_prov_summa = 0;
    var uzizm_prov_pvn = 0;
    var uzizm_prov_kopsumma = 0;

    jQuery.each(searchlist_uzizm, function(k,v){

      var row = $('#uzizm_tr' + k);

      if (row.is(':visible')) {

        if (sp_uzizm['s_summa'][k] > 0) uzizm_sanemta_summa += parseFloat(sp_uzizm['s_summa'][k]);
        if (sp_uzizm['s_prov_summa'][k] > 0) uzizm_prov_summa += parseFloat(sp_uzizm['s_prov_summa'][k]);
        if (sp_uzizm['s_prov_pvn'][k] > 0) uzizm_prov_pvn += parseFloat(sp_uzizm['s_prov_pvn'][k]);
        if (sp_uzizm['s_prov_kopsumma'][k] > 0) uzizm_prov_kopsumma += parseFloat(sp_uzizm['s_prov_kopsumma'][k]);

      }

    });

    $('#uzizm_sanemta_summa').html(uzizm_sanemta_summa.toFixed(2));
    $('#uzizm_prov_summa').html(uzizm_prov_summa.toFixed(2));
    $('#uzizm_prov_pvn').html(uzizm_prov_pvn.toFixed(2));
    $('#uzizm_prov_kopsumma').html(uzizm_prov_kopsumma.toFixed(2));

  }
  </script>

  <script>
  var searchlist_nakmaks = [];

  var sp_nakmaks = create_filter_field_array([
    's_klienta_nosaukums', 's_rek_nr', 's_rek_izr_datums', 's_apm_termins',
    's_rekina_statuss', 's_summa', 's_neapm_summa', 's_prov_percent', 's_prov_summa',
    's_prov_pvn', 's_prov_kopsumma', 's_pardeveja_id'
  ]);
  </script>

  <?
  $query = db_query("
    SELECT
      r.*,
      g.nosaukums as klienta_nosaukums,
      r.pvn as rekina_pvn,
      g.provizija_pardevejam,
      g.atlidziba_vaditajam,
      (IFNULL(r.kopsumma, 0) - SUM(IFNULL(m.summa, 0))) as neapm_summa,
      (
        SELECT SUM(no_summas)
        FROM `".DB_PREF."pardeveji_rekini_detalas` d2
        LEFT JOIN `".DB_PREF."pardeveji_rekini` pr2 ON pr2.id = d2.pardeveja_rekina_id
        WHERE
          rekina_id = r.id
          AND pr2.pardeveja_id = ".$pardevejs->id."
      ) as prov_izmaksu_summa,
      p.pvn_likme,
      g.pardeveja_id
    FROM `".DB_PREF."rekini` r
    LEFT JOIN `".DB_PREF."ligumi` g ON (r.liguma_id = g.id)
    LEFT JOIN `".DB_PREF."rekini_maksajumi` m ON (m.rekina_id = r.id)
    LEFT JOIN `".DB_PREF."pardeveji` p ON (p.id = g.pardeveja_id)
    WHERE
      g.pardeveja_id IN (".implode(', ', $pardeveju_ids).") AND
      r.anulets = 0 AND
      r.nodots_piedz_datums IS NULL AND
      r.atkapsanas_rekins = 0
    GROUP BY r.id
    HAVING
      neapm_summa > 0
     AND
    neapm_summa <= IFNULL(r.kopsumma, 0) - IFNULL(prov_izmaksu_summa, 0)
   ORDER BY r.apm_termins ASC
  ");

  $avansa_rekini = array();
  while($row = db_get_assoc($query)) {
    $avansa_rekini[$row['id']] = $row;
  }
  ?>

  <h3>Avansa provīzijas</h3>

  <table style="display:none;">

    <tr id="nakotnes_maksajumi_filter">

      <th><span><input class="search_field" meth="standart" searchclass="s_klienta_nosaukums" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>

      <th>
        <select class="search_field advonchange" searchclass="s_pardeveja_id" multiple="multiple" meth="standart" style="width: 100px;">
          <option value=""></option>
          <option value="<?php echo $pardevejs->id;?>" selected="SELECTED"><?php echo $pardevejs->vards;?></option>
          <?php foreach((array)$pardevejs->getPadotie() as $padotais) { ?>
            <option value="<?php echo $padotais->id;?>" selected="SELECTED"><?php echo $padotais->vards;?></option>
          <?php } ?>
        </select>
      </th>

      <th><span><input class="search_field" meth="standart" searchclass="s_rek_nr" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>

      <th>
        <span><input class="search_field kalendari" meth="from" searchclass="s_rek_izr_datums" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span><br>
        <span><input class="search_field kalendari" meth="to" searchclass="s_rek_izr_datums" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span>
      </th>

      <th>
        <span><input class="search_field kalendari" meth="from" searchclass="s_apm_termins" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span><br>
        <span><input class="search_field kalendari" meth="to" searchclass="s_apm_termins" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span>
      </th>

    <th>
      <select class="search_field advonchange" searchclass="s_rekina_statuss" meth="standart" style="width: 70px;">
        <option value=""></option>
        <?php foreach($_vars['apmaksas_statuss'] as $k=>$p){ ?>
          <?php if ($k == 1) continue; // do not show "Nav sācies" ?>
          <option value="<?=$k;?>"><?=$p;?></option>
        <?php } ?>
      </select>
    </th>

      <th><span><input class="search_field" meth="coin" searchclass="s_summa" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
      <th><span><input class="search_field" meth="coin" searchclass="s_neapm_summa" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
      <th><span><input class="search_field" meth="coin" searchclass="s_prov_percent" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
      <th><span><input class="search_field" meth="coin" searchclass="s_prov_summa" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
      <th><span><input class="search_field" meth="coin" searchclass="s_prov_pvn" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
      <th><span><input class="search_field" meth="coin" searchclass="s_prov_kopsumma" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>

    </tr>

  </table>

  <table id="nakotnes_maksajumi" class="data" style="width: 970px;">

    <thead>

      <tr class="last header">
        <th>Klients</th>
        <th>Līguma slēdzējs</th>
        <th>Rēķina nr.</th>
        <th>Rēķins izstādīts</th>
        <th>Apmaksas termiņš</th>
        <th>Apmaksas statuss</th>
        <th>Rēķina summa EUR (ar PVN)</th>
        <th>Rēķina neapm. summa EUR</th>
        <th>Provīzija %</th>
        <th>Bez PVN LEURs</th>
        <th>PVN EUR</th>
        <th>Kopā EUR</th>
      </tr>

    </thead>

    <tbody class="main">

      <? $inid = -1; ?>

      <? foreach($avansa_rekini as $avansa) { ?>

        <? $inid ++;  ?>

        <?
        //   Nosaka komisijas procentu
        if($avansa['pardeveja_id'] == $pardevejs->id) {
          $komisija_a = $avansa['provizija_pardevejam'];
        } else {
          //  Apskatās kāda vadītāja komisija norādīta partnerim
          /*if($padotais = Pardevejs::model()->findByPk($avansa['pardeveja_id'])) {
           $komisija_a = $padotais->atlidziba_vaditajam;
          } else {
            throw new Exception('Nevar atrast padoto pēc ID = ' . $avansa['pardeveja_id']);
          }*/
          $komisija_a = $avansa['atlidziba_vaditajam'];
        }

        //  Ja nedabūja komisijas vērtību vai, ja tā ir 0, tad nemaz nerāda šo maksājumu
        if(!isset($komisija_a) || !$komisija_a) {
          continue;
        }

        $apm_summa_bez_pvn = $avansa['neapm_summa'] / (1 + ($avansa['rekina_pvn'] / 100));
        $prov_summa = $apm_summa_bez_pvn * ($komisija_a / 100);

        $prov_pvn = $prov_summa * ($avansa['pvn_likme'] / 100);
        ?>

        <tr id="nakmaks_tr<?=$inid;?>" class="tr_nakmaks">
          <td><a href="?c=ligumi&a=labot&id=&a=labot&id=<?= $avansa['liguma_id'] ?>&subtab=2"><?= $avansa['klienta_nosaukums'] ?></a></td>
          <td><?php echo Pardevejs::model()->findByPk($avansa['pardeveja_id'])->vards;?></td>
          <td><?= $avansa['rek_nr'] ?></td>
          <td class="c"><?= !empty($avansa['izr_datums']) ? date('d.m.Y', strtotime($avansa['izr_datums'])) : '' ?></td>
          <td class="c"><strong><?= !empty($avansa['apm_termins']) ? date('d.m.Y', strtotime($avansa['apm_termins'])) : '' ?></strong></td>

          <? $status = get_rekina_statuss($avansa); ?>
          <td class="c">
            <div class="<?= !empty($status) ? 'apm_statuss_'.$status : ''; ?>">
              <?= !empty($status) ? '<span>' . $_vars['apmaksas_statuss'][$status] . '</span>' : ''; ?>
            </div>
          </td>

          <td class="r"><?= $avansa['kopsumma'] ?></td>
          <td class="r"><?= $avansa['neapm_summa'] ?></td>
          <td class="c"><?= round($komisija_a, 2) ?>%</td>
          <td class="r"><?= format_currency($prov_summa) ?></td>
          <td class="r"><?= format_currency($prov_pvn) ?></td>
          <td class="r last"><strong><?= format_currency($prov_summa + $prov_pvn) ?></strong></td>
          <td style="display: none;">
            <script>

              searchlist_nakmaks[<?=$inid;?>] = '<?=addslashes(mb_strtoupper($avansa['rek_nr']));?>';

              sp_nakmaks['s_klienta_nosaukums'][<?=$inid;?>] = '<?=addslashes(mb_strtoupper($avansa['klienta_nosaukums']));?>';
              sp_nakmaks['s_pardeveja_id'][<?=$inid;?>] = '<?=$avansa['pardeveja_id'];?>';
              sp_nakmaks['s_rek_nr'][<?=$inid;?>] = '<?=addslashes(mb_strtoupper($avansa['rek_nr']));?>';
              sp_nakmaks['s_rek_izr_datums'][<?=$inid;?>] = <?= !empty($avansa['izr_datums']) ? date('Ymd', strtotime($avansa['izr_datums'])) : 0 ?>;
              sp_nakmaks['s_apm_termins'][<?=$inid;?>] = <?= !empty($avansa['apm_termins']) ? date('Ymd', strtotime($avansa['apm_termins'])) : 0 ?>;
              sp_nakmaks['s_rekina_statuss'][<?=$inid;?>] = '<?=$status?>';
              sp_nakmaks['s_summa'][<?=$inid;?>] = '<?=!empty($avansa['kopsumma']) ? $avansa['kopsumma'] : "" ?>';
              sp_nakmaks['s_neapm_summa'][<?=$inid;?>] = '<?=!empty($avansa['neapm_summa']) ? $avansa['neapm_summa'] : "" ?>';
              sp_nakmaks['s_prov_percent'][<?=$inid;?>] = '<?=!empty($avansa['provizija_pardevejam']) ? $avansa['provizija_pardevejam'] : "" ?>';
              sp_nakmaks['s_prov_summa'][<?=$inid;?>] = '<?=!empty($prov_summa) ? $prov_summa : "" ?>';
              sp_nakmaks['s_prov_pvn'][<?=$inid;?>] = '<?=!empty($prov_pvn) ? $prov_pvn : "" ?>';
              sp_nakmaks['s_prov_kopsumma'][<?=$inid;?>] = '<?=$prov_summa + $prov_pvn ?>';

            </script>
          </td>
        </tr>

      <? } ?>

    </tbody>

    <tr>

      <td align="right" colspan="6"><strong>Kopā:</strong></td>
      <td class="r" id="nakmaks_rekinu_summa">0</td>
      <td class="r" id="nakmaks_neapm_summa">0</td>
      <td></td>
      <td class="r" id="nakmaks_prov_summa">0</td>
      <td class="r" id="nakmaks_prov_pvn">0</td>
      <td class="r" id="nakmaks_prov_kopsumma" style="font-weight: bold;">0</td>

    </tr>

  </table>

  <script>
  $(document).ready(function() {

    var table = $("#nakotnes_maksajumi");
    var head = $("#nakotnes_maksajumi_filter");

    table.tablesorter({
      debug: false,
      highlightClass: 'highlight',
      headers: {
        0: {sorter: 'latviantext'},
        3: {sorter: 'latviandate'},
        4: {sorter: 'latviandate'}
      }
    }).bind('sortStart', function() {
      table.trigger("update");
    }).bind('sortEnd', function() {
      stripeTable(table);
    });

    initTableFilter(table, head, processNakMaksTable, 'nakmaks', searchlist_nakmaks, sp_nakmaks);

  });

  function processNakMaksTable() {

    var nakmaks_rekinu_summa = 0;
    var nakmaks_neapm_summa = 0;
    var nakmaks_prov_summa = 0;
    var nakmaks_prov_pvn = 0;
    var nakmaks_prov_kopsumma = 0;

    jQuery.each(searchlist_nakmaks, function(k,v){

      var row = $('#nakmaks_tr' + k);

      if (row.is(':visible')) {

        if (sp_nakmaks['s_summa'][k] > 0) nakmaks_rekinu_summa += parseFloat(sp_nakmaks['s_summa'][k]);
        if (sp_nakmaks['s_neapm_summa'][k] > 0) nakmaks_neapm_summa += parseFloat(sp_nakmaks['s_neapm_summa'][k]);
        if (sp_nakmaks['s_prov_summa'][k] > 0) nakmaks_prov_summa += parseFloat(sp_nakmaks['s_prov_summa'][k]);
        if (sp_nakmaks['s_prov_pvn'][k] > 0) nakmaks_prov_pvn += parseFloat(sp_nakmaks['s_prov_pvn'][k]);
        if (sp_nakmaks['s_prov_kopsumma'][k] > 0) nakmaks_prov_kopsumma += parseFloat(sp_nakmaks['s_prov_kopsumma'][k]);

      }

    });

    $('#nakmaks_rekinu_summa').html(nakmaks_rekinu_summa.toFixed(2));
    $('#nakmaks_neapm_summa').html(nakmaks_neapm_summa.toFixed(2));
    $('#nakmaks_prov_summa').html(nakmaks_prov_summa.toFixed(2));
    $('#nakmaks_prov_pvn').html(nakmaks_prov_pvn.toFixed(2));
    $('#nakmaks_prov_kopsumma').html(nakmaks_prov_kopsumma.toFixed(2));

  }
  </script>