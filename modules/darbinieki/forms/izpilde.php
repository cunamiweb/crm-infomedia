<?php

if(!check_access('darbinieki-plana-izpilde') && ($pardeveja_id != $_SESSION['user']['id'] || !check_access('darbinieki-sava-plana-izpilde'))) {
  die('Jums nav pieejas tiesību plāna izpildes sadaļai.');
}

log_add("atvera", "Atvēra darbinieka " . $d['vards'] . " plāna izpildi");

$query = db_query("
  SELECT m.*
  FROM `".DB_PREF."pardeveji_min_izpilde` m
  WHERE m.`pardeveja_id` = ".$pardeveja_id."
  ORDER BY m.sakot_no ASC
");

$min_izpilde = array();

$cur_year = (int)date('Y');
$cur_quarter = (int)ceil(date('m')/3);

while($row = db_get_assoc($query)){
  $min_izpilde[] = htmlesc($row);
}

$sel_year = (!empty($_GET['izpilde_year']) && $_GET['izpilde_year'] <= date('Y')) ? $_GET['izpilde_year'] : date('Y');


$viewer = Pardevejs::model()->findByPk($_SESSION['user']['id']);

$izpilde_class = isset($_GET['hidemenu']) ? 'large' : '';
?>

<?
//  Ja pēc logina ir atvests uz izpildi. nosaka next urli. dublējas ar to kas ir iekš login.php
if(isset($_GET['hidemenu']) && $_GET['hidemenu']) {
  $next_url = '/';
  if(check_access('darbinieki-nenomainitie-login', $_SESSION['user']['id'], false)) {

    if(!is_admin() && get_active_reminder_count($_SESSION['user']['id']) > 0) {

      //  Ir aktīvi atgādinājumi
      if($pardevejs->tips == 'partner') {
        $next_url = "?c=partneri&a=labot&id=" . $_SESSION['user']['id'] . "&subtab=atgadinajumi&hidemenu=1";
      } else {
        $next_url = "?c=darbinieki&a=labot&id=" . $_SESSION['user']['id'] . "&subtab=atgadinajumi&hidemenu=1";
      }

    } else {

      //  Vadītājs ar padotajiem
      $pardeveju_ids = array();

      if($pardevejs->vaditajs) {
        $pardeveju_ids[] = $pardevejs->id;
        foreach((array)$pardevejs->getPadotie() as $padotais) {
          $pardeveju_ids[] = $padotais->id;
        }
      }

      if(count($pardeveju_ids) > 1) {
        $nenomainitie = get_nenomainitie_statusi(array('pardeveju_ids' => $pardeveju_ids));
      } else {
        $nenomainitie = get_nenomainitie_statusi(array('pardeveja_id' => $_SESSION['user']['id']));
      }

      if(!empty($nenomainitie)) {
        // Ir nenomainīti statusi
        $next_url = "?c=ligumi&a=nenom_statusi";
      }
    }
  }
  ?>
  <script type="text/javascript">
    hideMenuBar("<?=$next_url;?>");
  </script>
<? } ?>

<? // Prototypes // ?>
<div style="display:none;">


  <table id="minizpildeprot">
    <tbody>
      <tr>
        <td>
          <select name="min_izpilde_new[sakot_no_y][]">
            <? for($year = 2011; $year <= date('Y') + 1; $year ++) { ?>
              <option value="<?= $year ?>"><?= $year ?></option>
            <? } ?>
          </select>
          -
          <select name="min_izpilde_new[sakot_no_m][]">
            <? for($month = 1; $month <= 12; $month ++) { ?>
              <? $month = str_pad($month, 2, '0', STR_PAD_LEFT) ?>
              <option value="<?= $month ?>"><?= mb_substr($_vars['menesi'][(int)$month], 0, 3) ?></option>
            <? } ?>
          </select>
        </td>
        <td width="90" align="center">
          <input type="text" name="min_izpilde_new[lig_skaits][]" style="text-align: center; width: 3em; " />
        </td>
        <td width="90" align="right">
          <input type="text" name="min_izpilde_new[lig_apgroz][]" style="text-align: right; width: 5em;" />
        </td>
        <td>
          <button class="hidethistoo ui-state-default ui-corner-all" onclick="$(this).closest('tr').remove();">Dzēst</button>
        </td>
      </tr>
    </tbody>
  </table>

</div>

<form action="" method="get" id="izpilde_filter">
  <input type="hidden" name="c" value="darbinieki" />
  <input type="hidden" name="a" value="labot" />
  <input type="hidden" name="subtab" value="izpilde" />
  <input type="hidden" name="id" value="<?= $pardeveja_id ?>" />
  <div>
    Gads:
    <span>
      <select id="izpilde_year" name="izpilde_year">
        <? for($year = 2011; $year <= date('Y'); $year ++) { ?>
          <option <?= ($year == $sel_year) ? 'selected="selected"' : '' ?> value="<?= $year ?>"><?= $year ?></option>
        <? } ?>
      </select>
    </span>
	</div>
</form>

<form action="?c=darbinieki<?= !empty($pardeveja_id) ? '&a=labot&id=' . $pardeveja_id . '&subtab=izpilde' : '&a=jauns' ;?>" id="fullformplace" method="post" enctype="multipart/form-data">

<div id="izpilde_container" class="<?php echo $izpilde_class;?>">
<table cellpadding="0" cellspacing="0" class="data" id="plana_izpilde" style="width: auto;">

    <?php
    if($sel_year >= PREMIJAS_NEW2_START_YEAR) {
      $new2_izkartojums = true;
    } else {
      $new2_izkartojums = false;
    }

    //  Totals hack
    $glob_total = array(
      'kopa' => array(
        'ligumu_id' => array(),
        'ligumu_id_all' => array(),
        'apgr' => 0,
        'skaits' => 0
      ),
      'atdoti' => array(
        'ligumu_id' => array(),
        'ligumu_id_all' => array(),
        'apgr' => 0,
        'skaits' => 0
        ),
      'real' => array(
        'ligumu_id' => array(),
        'ligumu_id_all' => array(),
        'apgr' => 0,
        'skaits' => 0
      ),
    );

    $glob_total_historical = array(
      'kopa' => array(
        'ligumu_id' => array(),
        'ligumu_id_all' => array(),
        'apgr' => 0,
        'skaits' => 0
      ),
      'atdoti' => array(
        'ligumu_id' => array(),
        'ligumu_id_all' => array(),
        'apgr' => 0,
        'skaits' => 0
        ),
      'real' => array(
        'ligumu_id' => array(),
        'ligumu_id_all' => array(),
        'apgr' => 0,
        'skaits' => 0
      ),
    );

    print_plana_izpilde_head($new2_izkartojums) ?>

    <tbody>

     <?php
      $total_p = array();
      $querters = range(1,4);
      foreach ($querters as $quarter) {

        //  Vai ir tekošais vai vēlāks ceturksnis. Ja tā ir tad jāņem virtuālie
        if($sel_year == $cur_year && $quarter >= $cur_quarter) {

          $izpilde_function = 'get_izpildits_virtual';
          //$izpilde_function = 'get_izpildits';
          $historical_izpilde_function = 'get_izpilde_virtual_historical';
          $virtual = true;

        } else {

          $izpilde_function = 'get_izpildits';
          $historical_izpilde_function = 'get_izpilde_historical';
          $virtual = false;
        }

        //  Ceturkša dati
        //$qrow = get_izpildits($pardeveja_id, $sel_year, $quarter);
        $qrow_cache_key = $historical_izpilde_function . '-' . $pardeveja_id . '-' . $sel_year . '-' . $quarter;
        $qrow = cache_get($qrow_cache_key);

        if(!$qrow) {
          $qrow = $historical_izpilde_function($pardeveja_id, $sel_year, $quarter);
          cache_set($qrow_cache_key, $qrow);
        }

        //  Ceturkšņa dati bez piedziņas
        $qrow_p_cache_key = $izpilde_function . '-' . $pardeveja_id . '-' . $sel_year . '-' . $quarter . '-null-true';
        $qrow_p = cache_get($qrow_p_cache_key);

        if(!$qrow_p) {
          $qrow_p = $izpilde_function($pardeveja_id, $sel_year, $quarter, null, true);
          cache_set($qrow_p_cache_key, $qrow_p);
        }

        /*if($quarter == 1) {
          var_dump($qrow);
        }             */

        $total_p[$sel_year.'-'.$quarter] = $qrow_p;

        //  Totals hack
        foreach($qrow as $group => $data) {
          if(isset($data['ligumu_id_all'])) {
            $glob_total_historical[$group]['ligumu_id_all'] = array_merge($glob_total_historical[$group]['ligumu_id_all'],$data['ligumu_id_all']);
          }
          $glob_total_historical[$group]['ligumu_id'] = array_merge($glob_total_historical[$group]['ligumu_id'],$data['ligumu_id']);
          $glob_total_historical[$group]['apgr'] +=  $data['apgr'];
          $glob_total_historical[$group]['skaits'] += $data['skaits'];

        }

        foreach($qrow_p as $group => $data) {
          if(isset($data['ligumu_id_all'])) {
            $glob_total[$group]['ligumu_id_all'] = array_merge($glob_total_historical[$group]['ligumu_id_all'],$data['ligumu_id_all']);
          }
          $glob_total[$group]['ligumu_id'] = array_merge($glob_total_historical[$group]['ligumu_id'],$data['ligumu_id']);
          $glob_total[$group]['apgr'] +=  $data['apgr'];
          $glob_total[$group]['skaits'] += $data['skaits'];
        }

        //  Ja skatās pārdevējs kurš neredz visu
        if(!is_admin() && $viewer->izpilde_show_level != 'full') {

          if($sel_year == $cur_year && $quarter > $cur_quarter) { //  Vai ir tālāk par tekošo

            if($quarter == $cur_quarter+1) {

              //  Nākamais aiz tekošā

              if($viewer->izpilde_show_level == 'current') {
                continue;
              }

            } else {

              //  Vēl tālāk
                continue;

            }
          }
        }

        print_plana_izpilde_row($sel_year.'-'.$quarter, $qrow, array(), $qrow_p, $virtual, $new2_izkartojums);

        //  Mēneši
        $months = array(
          ($quarter * 3) - 2, //  Pirmais ceturkšņa mēnesis
          ($quarter * 3) - 1, // Otrais
          ($quarter * 3)  // Trešais
        );

        $options = array(
            'month_rows' => true,
            'allow_open_month' => false
        );

        //if(!is_new_premium_sys(array('year' => $sel_year, 'quarter' => $quarter))) {
          foreach($months as $month) {
            //$mrow = get_izpildits($pardeveja_id, $sel_year, null, $month);

            $mrow_cache_key = $historical_izpilde_function . '-' . $pardeveja_id . '-' . $sel_year . '-null-' . $month;
            $mrow = cache_get($mrow_cache_key);

            if(!$mrow) {
              $mrow = $historical_izpilde_function($pardeveja_id, $sel_year, null, $month);
              cache_set($mrow_cache_key, $mrow);
            }

            $mrow_p_cache_key = $izpilde_function . '-' . $pardeveja_id . '-' . $sel_year . '-null-' . $month;
            $mrow_p = cache_get($mrow_p_cache_key);

            if(!$mrow_p) {
              $mrow_p = $izpilde_function($pardeveja_id, $sel_year, null, $month, true);
              cache_set($mrow_p_cache_key, $mrow_p);
            }


            $period = $sel_year . '-' . str_pad($month, 2, '0', STR_PAD_LEFT); // gads-menesis(ar diviem cipariem)

            print_plana_izpilde_row($period, $mrow, $options, $mrow_p, $virtual, $new2_izkartojums);
          }
        //}

      }

      //  Par gadu

      //  Ja skatās pārdevējs kurš neredz visu
      if(is_admin() || $viewer->izpilde_show_level == 'full') {

        //$total = get_izpildits($pardeveja_id, $sel_year);
        $total_cache_key = 'getIzpildeHistorical-'.$pardeveja_id.'-'.$sel_year;
        $total = cache_get($total_cache_key);

        if(!$total) {
          $total = get_izpilde_historical($pardeveja_id, $sel_year);
          cache_set($total_cache_key, $total);
        }


        if($new2_izkartojums) {
          print_plana_izpilde_row('total', $glob_total_historical, array(), $glob_total, false, $new2_izkartojums);
        } else {
          print_plana_izpilde_row('total', $total, array(), $total_p, false, $new2_izkartojums);
        }
       }
      ?>

    </tbody>

</table>

  <?php

  if (is_admin()) { ?>

    <table id="min_izpilde_table">

      <tr>
        <th>Sākot no</th>
        <th>Min. līg. sk.</th>
        <th align="right">Min. apgr.</th>
        <th></th>
      </tr>

      <? foreach($min_izpilde as $v) { ?>

        <tr>
          <td>
            <select name="min_izpilde[<?= $v['id'] ?>][sakot_no_y]" class="limited_admin_edit">
              <? for($year = 2011; $year <= date('Y') + 1; $year ++) { ?>
                <option <?= ($year == date('Y', strtotime($v['sakot_no']))) ? 'selected="selected"' : '' ?> value="<?= $year ?>"><?= $year ?></option>
              <? } ?>
            </select>
            -
            <select name="min_izpilde[<?= $v['id'] ?>][sakot_no_m]" class="limited_admin_edit">
              <? for($month = 1; $month <= 12; $month ++) { ?>
                <? $month = str_pad($month, 2, '0', STR_PAD_LEFT) ?>
                <option <?= ($month == date('m', strtotime($v['sakot_no']))) ? 'selected="selected"' : '' ?> value="<?= $month ?>"><?= mb_substr($_vars['menesi'][(int)$month], 0, 3) ?></option>
              <? } ?>
            </select>
          </td>
          <td width="90" align="center">
            <input type="text" name="min_izpilde[<?= $v['id'] ?>][lig_skaits]" value="<?= $v['lig_skaits'] ?>" style="text-align: center; width: 3em; "  class="limited_admin_edit" />
          </td>
          <td width="80" align="right">
            <input type="text" name="min_izpilde[<?= $v['id'] ?>][lig_apgroz]" value="<?= $v['lig_apgroz'] ?>" style="text-align: right; width: 5em;"  class="limited_admin_edit" />
          </td>
          <td>
            <button class="hidethistoo ui-state-default ui-corner-all limited_admin_edit" onclick="$(this).closest('tr').remove();">Dzēst</button>
          </td>
        </tr>

      <? } ?>

      <tr>
        <td colspan="4" align="right">
          <button onClick="$(this).closest('tr').before($('#minizpildeprot tbody').html()); return false;" class="ui-state-default ui-corner-all hidethistoo limited_admin_edit">Pievienot jaunu</button>
        </td>
      </tr>

    </table>

    <div class="clr"></div>

    <table class="data_form layout1">

      <tr>
        <td class="label">Izpildes tabula pēc ielogošanās</td>
        <td><input class="<?php echo check_access('darbinieki-plana-izpilde') ? 'canedit' : 'always_disable';?>" type="checkbox" name="izpilde_after_login" <?php echo $pardevejs->izpilde_after_login ? 'checked="checked"' : '';?>></td>
      </tr>

      <tr>
        <td class="label">Pārdevējam redzams</td>
        <td>
          <select class="<?php echo check_access('darbinieki-plana-izpilde') ? 'canedit' : 'always_disable';?>" name="izpilde_show_level">
            <option value="current" <?php echo $pardevejs->izpilde_show_level == 'current' ? 'selected="SELECTED"' : '';?>>Tikai tekošais ceturksnis</option>
            <option value="curnext" <?php echo $pardevejs->izpilde_show_level == 'curnext' ? 'selected="SELECTED"' : '';?>>Tekošais un nākamais ceturksnis</option>
            <option value="full" <?php echo $pardevejs->izpilde_show_level == 'full' ? 'selected="SELECTED"' : '';?>>Viss</option>
          </select>
        </td>
      </tr>

    </table>


  <?php } ?>


  </div>  <div class="clr"></div>

  <?php if(is_admin()) {?>

    <? show_comments_block('darb_izpilde', isset($_GET['id']) ? $_GET['id'] : null) ?>

    <? show_files_block('darb_izpilde', isset($_GET['id']) ? $_GET['id'] : null) ?>

  <?php } ?>

  <div class="hidethistoo non_admin_edit">
    <input type="submit" onClick="return saveAll(this, 'darbinieks_izpilde')" class="non_admin_edit submit show_in_act_panel" value="Saglabāt">
    <input type="button" onclick="return cancelSaves();" class="non_admin_edit show_in_act_panel" value="Atcelt">
  </div>

  <input type="hidden" name="form_sent" value="1" />

  <script>
  initPlanaIzpildeTable();

  $("form#izpilde_filter").each(function() {

    var form = $(this);

    $('select#izpilde_year', form).change(function() {
      form.submit();
    });

  });

  //  Atver tekošo
  var cur_ceturksnis = <?php echo ceil(date('m')/3);?>;
  $(".open_months").eq(cur_ceturksnis-1).trigger('click');
  </script>

</form>