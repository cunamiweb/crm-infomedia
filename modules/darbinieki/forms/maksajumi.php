<?php
if (!check_access('darbinieki-maksajumi') && ($pardeveja_id != $_SESSION['user']['id'] || !check_access('darbinieki-savi-maksajumi'))) {
  die('Jums nav pieejas tiesību šai sadaļai');
}

if (isset($_GET['subtab2']) && in_array($_GET['subtab2'], array('provizijas', 'premijas'))) {
  //  Ja izvēlētas provīzijas vai premijas
  $subtab2 = $_GET['subtab2'];
} elseif(isset($_GET['subtab2']) && $_GET['subtab2'] == 'bonusi' && $d['vaditajs']) {
  //  Ja izvēlēts bonusi un apskatāmais useris ir vadītājs
  $subtab2 = 'bonusi';
} elseif($d['vaditajs'] && check_access('darbinieki-vaditajs-default-tab-bonusi', $pardevejs->id)) {
  //  Ja nav izvēlēts nekas un apskatāmajam userim tiesībās ir uzlikts ka tam defaultais tabs ir bonusi
  $subtab2 = 'bonusi';
} else {
  //  Ja nav izvēlēts nekas
  $subtab2 = 'provizijas';
}

log_add("atvera", "Atvēra darbinieka " . $d['vards'] . " maksājumus");

?>

<form action="?c=darbinieki<?= !empty($pardeveja_id) ? '&a=labot&id=' . $pardeveja_id . '&subtab=maksajumi&subtab2=' . $subtab2 : '&a=jauns' ;?>" id="fullformplace" method="post" enctype="multipart/form-data">

  <div class="subtab2 sub-menu"><span>
    <?php
    if(!empty($pardeveja_id)) {
      $s2link = '&a=labot&id=' . $pardeveja_id . '&subtab=maksajumi&subtab2=';
    }
    ?>
    <a href="?c=darbinieki<?= !empty($pardeveja_id) ? $s2link . 'provizijas' : '&a=jauns' ;?>" <?php echo $subtab2 == 'provizijas' ? 'class="active"' : '';?>>Provīzijas</a>
    <?php if(!$d['vaditajs']) { ?>
      <a href="?c=darbinieki<?= !empty($pardeveja_id) ? $s2link . 'premijas' : '&a=jauns' ;?>" <?php echo $subtab2 == 'premijas' ? 'class="active"' : '';?>>Prēmijas</a>
    <?php } else { ?>
      <a href="?c=darbinieki<?= !empty($pardeveja_id) ? $s2link . 'bonusi' : '&a=jauns' ;?>" <?php echo $subtab2 == 'bonusi' ? 'class="active"' : '';?>>Bonusi</a>
    <?php } ?>
  </span></div>

  <?php
  require('maksajumi_' . $subtab2 . '.php');
  ?>

</form>