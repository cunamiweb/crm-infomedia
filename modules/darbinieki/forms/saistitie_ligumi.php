<?php
if(!check_access('darbinieki-ligumi') && ($pardeveja_id != $_SESSION['user']['id'] || !check_access('darbinieki-savi-ligumi'))) {
  die('Jums nav pieejas tiesību šai sadaļai');
}

log_add("atvera", "Atvēra darbinieka " . $d['vards'] . " saistītos līgumus"); 
?>

<script>
var searchlist = [];

var sp = create_filter_field_array([
  's_nosaukums', 's_piesaist_datums', 's_lig_nr', 's_indekss', 's_pardevejs', 's_lig_stat', 's_norek_stat', 's_ligdat',
  's_rekl_no', 's_rekl_lidz', 's_summa',
  's_pvn', 's_kopsumma', 's_akt_atgad'
]);
</script>

<form action="?c=darbinieki<?= !empty($pardeveja_id) ? '&a=labot&id=' . $pardeveja_id . '&subtab=saistitie_ligumi' : '&a=jauns' ;?>" id="fullformplace" method="post" enctype="multipart/form-data">

  <table style="display:none;">

    <tr id="darbinieka_saistitie_ligumi_groups">
      <th colspan="3"></th>
      <th colspan="2" class="bgc-sta">Statuss</th>
      <th colspan="4" class="bgc-lig">Līgums</th>
      <th colspan="2" class="bgc-pak">Pakalpojums</th>
      <th colspan="3" class="bgc-rek">Norēķini</th>
      <th>Atg.</th>
    </tr>

    <tr id="darbinieka_saistitie_ligumi_filter">

      <th></th>
      <th><span><input class="search_field" meth="standart" searchclass="s_nosaukums" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>

	<th>
        <span><input class="search_field kalendari" meth="from" searchclass="s_piesaist_datums" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span><br>
        <span><input class="search_field kalendari" meth="to" searchclass="s_piesaist_datums" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span>
      </th>
		
      <th>
        <select class="search_field advonchange" searchclass="s_lig_stat" meth="standart" style="width: 70px;">
          <option value=""></option>
          <?php foreach($_vars['liguma_statuss'] as $k=>$p){ ?>
            <?php if ($k == 6) { ?>
              <option value="6_termins"><?=$p;?> (<?= ucfirst($_vars['liguma_beigu_statuss']['termins']) ?>)</option>
              <option value="6_termins_p"><?=$p;?> (<?= ucfirst($_vars['liguma_beigu_statuss']['termins']) ?>, Pārtraukts)</option>
              <option value="6_parslegts"><?=$p;?> (<?= ucfirst($_vars['liguma_beigu_statuss']['parslegts']) ?>)</option>
              <option value="6_parslegts_p"><?=$p;?> (<?= ucfirst($_vars['liguma_beigu_statuss']['parslegts']) ?>, Pārtraukts)</option>
              <option value="6_atteikums"><?=$p;?> (<?= ucfirst($_vars['liguma_beigu_statuss']['atteikums']) ?>)</option>
              <option value="6_atteikums_p"><?=$p;?> (<?= ucfirst($_vars['liguma_beigu_statuss']['atteikums']) ?>, Pārtraukts)</option>
            <?php } else { ?>
              <option value="<?=$k;?>"><?=$p;?></option>
            <?php } ?>
          <?php } ?>
        </select>
        <a class="search_field" href="#">x</a>
      </th>

      <th>
        <select class="search_field advonchange" searchclass="s_norek_stat" meth="int" style="width: 70px;">
          <option value=""></option>
          <?php foreach($_vars['kopapmaksas_statuss'] as $k=>$p){ ?>
            <option value="<?=$k;?>"><?=$p;?></option>
          <?php } ?>
        </select>
        <a class="search_field" href="#">x</a>
      </th>

      <th>
        <span><input class="search_field kalendari" meth="from" searchclass="s_ligdat" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span><br>
        <span><input class="search_field kalendari" meth="to" searchclass="s_ligdat" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span>
      </th>

      <th><span><input class="search_field" meth="standart" searchclass="s_lig_nr" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>

      <th>
        <select class="search_field advonchange" searchclass="s_pardevejs" meth="int">
          <option value=""></option>
          <?php foreach($_vars['sys_pardeveji'] as $k=>$p){ ?>
            <option value="<?=$k;?>"><?=$p;?></option>
          <?php } ?>
        </select>
        <a class="search_field" href="#">x</a>
      </th>

      <th><span><input class="search_field" meth="standart" searchclass="s_indekss" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>

      <th>
        <span><input class="search_field kalendari" meth="from" searchclass="s_rekl_no" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span><br>
        <span><input class="search_field kalendari" meth="to" searchclass="s_rekl_no" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span>
      </th>

      <th>
        <span><input class="search_field kalendari" meth="from" searchclass="s_rekl_lidz" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span><br>
        <span><input class="search_field kalendari" meth="to" searchclass="s_rekl_lidz" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span>
      </th>

      <th><span><input class="search_field" meth="coin" searchclass="s_summa" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
      <th><span><input class="search_field" meth="coin" searchclass="s_pvn" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
      <th><span><input class="search_field" meth="coin" searchclass="s_kopsumma" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>

      <th>
        <select class="search_field advonchange" searchclass="s_akt_atgad" meth="int" style="width: 40px;">
          <option value=""></option>
          <option value="1">Ir</option>
          <option value="2">Nav</option>
        </select>
        <a class="sfield_clear" href="#">x</a>
      </th>

    </tr>

  </table>

  <table cellpadding="3" cellspacing="0" class="data <?= isset($_GET['id']) ? 'ckey_darbinieka_saistitie_ligumi_' . $_GET['id'] : '' ?>" id="darbinieka_saistitie_ligumi" style="width: auto;">

    <thead>

      <tr class="header">
        <th class="header" colname="npk">Npk.</th>
        <th width="100" colname="nosauk">Nosaukums</th>
        <th width="50" colname="piesaist_datums">Piešķiršanas datums</th>
        <th width="50" colname="lig_statuss">Līg. statuss</th>
        <th width="50" colname="norek_statuss">Norēķinu statuss</th>

        <th width="50" colname="lig_datums">Līg. datums</th>
        <th width="60" colname="lig_nr">Līg. nr.</th>
        <th width="80" colname="darbinieks">Darbinieks</th>
        <th width="40" colname="indekss">Indekss</th>

        <th width="50" colname="lig_no">Līgums no</th>
        <th width="50" colname="lig_lidz">Līgums līdz</th>
        <th width="45" colname="summa">Summa<br />(bez PVN)</th>
        <th width="30" colname="pvn">PVN</th>
        <th width="45" colname="kopsumma">Kopsumma<br />(ar PVN)</th>
        <th width="40" colname="akt_atgad">Aktīvie<br />atgādinājumi</th>
      </tr>

    </thead>

    <tbody class="main">

      <?php
      $i = count($saistitie_ligumi);
      $inid = -1;

      foreach($saistitie_ligumi as $row) {

        $row['liguma_statuss'] = get_liguma_statuss($row, !empty($saistito_lig_rekini[$row['id']]) ? $saistito_lig_rekini[$row['id']] : null);
        $row['norekinu_statuss'] = get_apmaksas_statuss(!empty($saistito_lig_rekini[$row['id']]) ? $saistito_lig_rekini[$row['id']] : null);

        $inid ++;

        ?>

        <tr id="lig_tr<?=$inid;?>" class="lig_row" title="<?=$row['nosaukums'] ?>">
          <td class="c s_id"><?=$i;?></td>
          <td><a href="?c=ligumi&a=labot&id=<?=$row['id'];?>"><b><?=$row['nosaukums'] ?></b></a></td>
          <td class="c"><span style="display:none;"><?=date("Y.m.d",strtotime($row['piesaist_datums']));?>|</span><strong><?= !empty($row['piesaist_datums'])?date("d.m.Y",strtotime($row['piesaist_datums'])):"" ?></strong></td>

          <?php if ($row['liguma_statuss'] == 6) { ?>
            <?php $active = (empty($row['beigu_statuss']) || ($row['beigu_statuss'] == 'termins' && $row['beigu_statuss_atlikt_lidz'] <= date('Y-m-d'))) ? true : false; ?>
            <td class="c">
              <div class="lig_statuss_6 <?= !empty($row['beigu_statuss']) ? 'lig_beigu_statuss_' . $row['beigu_statuss'] : 'lig_beigu_statuss_termins' ?>">
                <a class="edit_link" href="#" onclick="showLigumaBeiguStatussDialog(<?= $row['id'] ?>); return false;"><img src="css/edit.png" alt="Labot statusu" title="Labot statusu" /></a>
                <span class="<?= $active ? 'lig_beigu_statuss_active' : '' ?>">
                  <span class="stat_name"><?= $_vars['liguma_statuss'][6] ?></span>
                  (<?= !empty($row['beigu_statuss']) ? $_vars['liguma_beigu_statuss'][$row['beigu_statuss']] : $_vars['liguma_beigu_statuss']['termins'] ?><?= !empty($row['beigu_statuss_auto_atteikums']) ? ', <span title="Līgums pirms reklāmas beigām tika pārtraukts">P</span>': '' ?>)
                </span>
              </div>
            </td>
          <?php } else { ?>
            <td class="c"><?= !empty($_vars['liguma_statuss'][$row['liguma_statuss']]) ? '<div class="'.( !empty($_vars['liguma_statuss'][$row['liguma_statuss']]) ? 'lig_statuss_'.$row['liguma_statuss'] : '' ).'">' . $_vars['liguma_statuss'][$row['liguma_statuss']] . '</div>' : ''?></td>
          <?php } ?>

          <?
          $max_nokavetas_dienas = 0;
          if ($row['norekinu_statuss'] == 4) {
            $max_nokavetas_dienas = get_max_nokavetas_dienas(!empty($saistito_lig_rekini[$row['id']]) ? $saistito_lig_rekini[$row['id']] : null);
          }
          ?>

          <?php if ($row['norekinu_statuss'] == 2 && is_barteris(!empty($saistito_lig_rekini[$row['id']]) ? $saistito_lig_rekini[$row['id']] : null)) { ?>
            <td class="c">
              <span style="display: none;"><?= $row['norekinu_statuss'] . '_' . str_pad($max_nokavetas_dienas, 5, "0", STR_PAD_LEFT) ?></span>
              <div class="norek_statuss_2">
                <a href="?c=ligumi&a=labot&id=<?=$row['id'];?>&subtab=2">Barteris</a>
              </div>
            </td>
          <?php } else { ?>
            <td class="c">
              <span style="display: none;"><?= $row['norekinu_statuss'] . '_' . str_pad($max_nokavetas_dienas, 5, "0", STR_PAD_LEFT) ?></span>
              <div class="<?= !empty($_vars['kopapmaksas_statuss'][$row['norekinu_statuss']]) ? 'norek_statuss_'.$row['norekinu_statuss'] : "" ?>">
                <?= !empty($_vars['kopapmaksas_statuss'][$row['norekinu_statuss']]) ? '<a href="?c=ligumi&a=labot&id='.$row['id'].'&subtab=2" class="norek_statuss_'.$row['norekinu_statuss'].'">' . $_vars['kopapmaksas_statuss'][$row['norekinu_statuss']] . '</a>' : "" ?>

                <?php if ($row['norekinu_statuss'] == 4) { ?>
                  <span style="font-size: 0.75em;">(<?= $max_nokavetas_dienas ?>)</span>
                <?php } ?>
              </div>
            </td>
          <?php } ?>

          <td class="c"><span style="display:none;"><?=date("Y.m.d",strtotime($row['ligumadat']));?>|</span><strong><?= !empty($row['ligumadat'])?date("d.m.Y",strtotime($row['ligumadat'])):"" ?></strong></td>

          <td class="c"><?= $row['ligumanr'] ?></td>

          <?php if (check_access('darbinieki-admin')) { ?>
            <td><a href="?c=darbinieki&a=labot&id=<?=$row['pardeveja_id'];?>"><?=$row['pardeveja_vards'];?></a></td>
          <?php } else { ?>
            <td><?=$row['pardeveja_vards'];?></td>
          <?php } ?>

          <td class="c"><?= preg_replace('/^lv\-/i', '', $row['faktaddr_index']) ?></td>

          <td class="c"><span style="display:none;"><?=date("Y.m.d",strtotime($row['reklama_no']));?>|</span><strong><?= !empty($row['reklama_no'])?date("d.m.Y",strtotime($row['reklama_no'])):"" ?></strong></td>
          <td class="c"><span style="display:none;"><?=date("Y.m.d",strtotime($row['reklama_lidz']));?>|</span><strong><?= !empty($row['reklama_lidz'])?date("d.m.Y",strtotime($row['reklama_lidz'])):"" ?></strong></td>
          <td class="r"><?= format_currency($row['rekinu_summa']) ?></td>
          <td class="r"><?= format_currency($row['rekinu_kopsumma'] - $row['rekinu_summa']) ?></td>
          <td class="r"><?= format_currency($row['rekinu_kopsumma']) ?></td>

          <td class="c"><a href="?c=ligumi&a=labot&id=<?= $row['id'] ?>&subtab=3"><?= (!empty($row['aktivi_atgadinajumi'])) ? 'Ir ('.$row['aktivi_atgadinajumi'].')' : 'Nav' ?></a></td>

          <td style="display: none;">
            <script>
              searchlist[<?=$inid;?>] = '<?=addslashes(mb_strtoupper($row['nosaukums']));?>';
              sp['s_nosaukums'][<?=$inid;?>] = '<?=addslashes(mb_strtoupper($row['nosaukums']));?>';
              sp['s_piesaist_datums'][<?=$inid;?>] = <?= !empty($row['piesaist_datums']) ? date('Ymd', strtotime($row['piesaist_datums'])) : 0 ?>;
              sp['s_lig_nr'][<?=$inid;?>] = '<?=addslashes(mb_strtoupper($row['ligumanr']));?>';
              sp['s_indekss'][<?=$inid;?>] = '<?=addslashes(mb_strtoupper($row['faktaddr_index']));?>';
              sp['s_pardevejs'][<?=$inid;?>] = <?= $row['pardeveja_id'] ?>;

              <?php if ($row['liguma_statuss'] == 6) { ?>
                sp['s_lig_stat'][<?=$inid;?>] = '<?= $row['liguma_statuss'] . '_' . (!empty($row['beigu_statuss']) ? $row['beigu_statuss'] : 'termins') ?>';
              <?php } else { ?>
                sp['s_lig_stat'][<?=$inid;?>] = '<?= $row['liguma_statuss'] ?>';
              <?php } ?>

              sp['s_norek_stat'][<?=$inid;?>] = <?= $row['norekinu_statuss'] ?>;
              sp['s_ligdat'][<?=$inid;?>] = <?= !empty($row['ligumadat']) ? date('Ymd', strtotime($row['ligumadat'])) : 0 ?>;
              sp['s_rekl_no'][<?=$inid;?>] = <?= !empty($row['reklama_no']) ? date('Ymd', strtotime($row['reklama_no'])) : 0 ?>;
              sp['s_rekl_lidz'][<?=$inid;?>] = <?= !empty($row['reklama_lidz']) ? date('Ymd', strtotime($row['reklama_lidz'])) : 0 ?>;
              sp['s_summa'][<?=$inid;?>] = '<?=!empty($row['rekinu_summa']) ? $row['rekinu_summa'] : "" ?>';
              sp['s_pvn'][<?=$inid;?>] = '<?= round($row['rekinu_kopsumma'] - $row['rekinu_summa'], 2) ?>';
              sp['s_kopsumma'][<?=$inid;?>] = '<?=!empty($row['rekinu_kopsumma']) ? $row['rekinu_kopsumma'] : "" ?>';

              sp['s_akt_atgad'][<?=$inid;?>] = '<?= (!empty($row['aktivi_atgadinajumi'])) ? '1' : '2' ?>';

            </script>
          </td>
        </tr>

        <?php $i -- ?>

      <?php } ?>


    </tbody>

    <tr>

      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td align="right"><strong>Kopā:</strong></td>
      <td id="ligumusumma" class="r">0</td>
      <td id="pvnsumma" class="r">0</td>
      <td id="ligumukopsumma" class="r">0</td>
      <td></td>

    </tr>

    <tr>

      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td align="right"><strong>Vidēji:</strong></td>
      <td id="ligumusummavid" class="r">0</td>
      <td></td>
      <td id="ligumukopsummavid" class="r">0</td>
      <td></td>

    </tr>

  </table>

  <script>

  $(document).ready(function() {

    var table = $("#darbinieka_saistitie_ligumi");
    var head = $("#darbinieka_saistitie_ligumi_filter");
    var groups = $("#darbinieka_saistitie_ligumi_groups");

    var cache_key = getCacheKeyFromClass(table);

    var sortListFromCache = cache_key ? $.jStorage.get(cache_key + '_sort', []) : [];

    table.tablesorter({
      debug: false,
      highlightClass: 'highlight',
      sortList: sortListFromCache,
      headers: {
        0: { sorter: false },
        1: { sorter:'latviantext' },
        11: { sorter:'currency' },
        12: { sorter:'currency' },
        13: { sorter:'currency' }
      }
    }).bind('sortEnd', function() {

      if (cache_key) $.jStorage.set(cache_key + '_sort', table.get(0).config.sortList);

      updateNpk(table);
      stripeTable(table);

      initTableGroupColors(table);

    });

    initTableFilter(table, head, processTable, 'lig', searchlist, sp, groups);
    table.trigger("update");

    initTableCols($('#darbinieka_saistitie_ligumi'), 'darb_saistitie_ligumi', <?= Zend_Json::encode(get_tab_kolonnas('darb_saistitie_ligumi')) ?>);

    initTableGroupColors(table);

  });

  function processTable(table) {

    var table = $(this);

    var ligumuskaits = 0;
    var ligumusumma = 0;
    var pvnsumma = 0;
    var ligumukopsumma = 0;

    jQuery.each(searchlist, function(k,v){

      var row = $('#lig_tr' + k);

      if (row.is(':visible')) {

        ligumuskaits ++;

        if(sp['s_summa'][k] > 0) ligumusumma += parseFloat(sp['s_summa'][k].replace(',','.'));
        if(sp['s_pvn'][k] > 0) pvnsumma += parseFloat(sp['s_pvn'][k].replace(',','.'));
        if(sp['s_kopsumma'][k] > 0) ligumukopsumma += parseFloat(sp['s_kopsumma'][k].replace(',','.'));

      }

    });

    $('#ligumusumma').html('EUR&nbsp;' + parseFloat(ligumusumma).toFixed(2));
    $('#ligumusummavid').html('EUR&nbsp;' + parseFloat(ligumusumma / ligumuskaits).toFixed(2));

    $('#pvnsumma').html('EUR&nbsp;' + parseFloat(pvnsumma).toFixed(2));

    $('#ligumukopsumma').html('EUR&nbsp;' + parseFloat(ligumukopsumma).toFixed(2));
    $('#ligumukopsummavid').html('EUR&nbsp;' + parseFloat(ligumukopsumma / ligumuskaits).toFixed(2));

    updateNpk(table);

    initTableGroupColors(table);

  }

  function updateNpk(table) {

    // updating npk

    var visible_rows = $('tr.lig_row:visible', table);

    var npk = visible_rows.length;

    visible_rows.each(function() {

      $('.s_id', this).html(npk);

      npk --;

    });

  }
  </script>

  <?php show_comments_block('darb_saistitie_ligumi', isset($_GET['id']) ? $_GET['id'] : null) ?>

  <?php show_files_block('darb_saistitie_ligumi', isset($_GET['id']) ? $_GET['id'] : null) ?>

  <div class="hidethistoo non_admin_edit">
    <input type="submit" onClick="return saveAll(this, 'darbinieks_saistitie_ligumi')" class="non_admin_edit submit show_in_act_panel" value="Saglabāt">
    <input type="button" onclick="return cancelSaves();" class="non_admin_edit show_in_act_panel" value="Atcelt">
  </div>

  <input type="hidden" name="form_sent" value="1" />

</form>