<?
if (empty($_GET['darbinieka_rek_id'])) {
  die();
}

$darbinieka_rek_id = (int) $_GET['darbinieka_rek_id'];


$rekins = db_get_assoc(db_query("
  SELECT *
  FROM `".DB_PREF."pardeveji_rekini`
  WHERE id = ".$darbinieka_rek_id."
"));

if (empty($rekins)) {
  die();
}

if (!check_access('darbinieki-admin') && $_SESSION['user']['id'] != $rekins['pardeveja_id']) {
  die();
}

$darbinieks = db_get_assoc(db_query("
  SELECT *
  FROM `".DB_PREF."pardeveji`
  WHERE id = ".$rekins['pardeveja_id']."
"));

log_add("atvera", "Atvēra darbinieka " . $darbinieks['vards'] . " rēķina " . $rekins['rek_nr'] . " detalizāciju");

// Prēmiju ceturkšņu noteikšanai

$yearly_stats = array();

function getIzpildeQuarter($year, $quarter, &$cache)
{
  global $darbinieks;
  if(!isset($cache[$year][$quarter])) {
    $cache[$year][$quarter] = get_izpildits($darbinieks['id'], $year, $quarter, null, true);
  }

  return $cache[$year][$quarter];
}

$quarters_ligumi = array();

function getYearQuarter($row)
{
  global $quarters_ligumi;

  foreach($quarters_ligumi as $ligumi_year => $ligumi_quarters) {
    foreach($ligumi_quarters as $ligumi_quarter => $l_ids) {
      if(in_array($row['liguma_id'], $l_ids)) {
        return array($ligumi_year, $ligumi_quarter);
      }
    }
  }
}

$result = db_query("
  SELECT *
  FROM `".DB_PREF."premijas` p
  WHERE
    p.pardevejs_id = " .  $darbinieks['id'] . " AND
    p.ir_premija = 1 AND
    p.premijas_apmers != 0
");

while($row = db_get_assoc($result)) {
  if(is_quarter_fixed($row['gads'], $row['ceturksnis'])) {
    //  No gada un ceturkšņa numura jāuztaisa datumu range.
    //  Ceturkšņa pirmā mēneša 1. datums līdz ceturkšņa pēdējā mēneša pēdējais datums (31?)
    $data = getIzpildeQuarter($row['gads'], $row['ceturksnis'], $yearly_stats);

    if(!isset($quarters_ligumi[$row['gads']][$row['ceturksnis']])) {
      $quarters_ligumi[$row['gads']][$row['ceturksnis']] = array();
    }
    $quarters_ligumi[$row['gads']][$row['ceturksnis']] = array_merge($quarters_ligumi[$row['gads']][$row['ceturksnis']], $data['kopa']['ligumu_id']);
  }
}

//  End: premiju cet not.

$bonusi = array();
$apmaksas = array();
$avansa = array();
$p_apmaksas = array();
$p_avansa = array();
$piemaksas = array();

//  Bonusi
$bonusi = db_get_assoc_all(db_query('
  SELECT b.*, p.uz_rokas
  FROM '.DB_PREF.'bonusi b
  LEFT JOIN `'.DB_PREF.'pardeveji_rekini` p ON (p.id = b.pardeveja_rekina_id)
  WHERE
    b.pardeveja_rekina_id = ' . $darbinieka_rek_id . '
  ORDER BY gads, ceturksnis, menesis
'));

//  Sorting
usort($bonusi, 'bonusSort');
function bonusSort($a, $b) {
  $asort = $a['gads'] . ($a['ceturksnis'] ? $a['ceturksnis'] . '13' : ($a['menesis'] ? ceil($a['menesis']/3) . str_pad($a['menesis'], 2, "0", STR_PAD_LEFT) : '000'));
  $bsort = $b['gads'] . ($b['ceturksnis'] ? $b['ceturksnis'] . '13' : ($b['menesis'] ? ceil($b['menesis']/3) . str_pad($b['menesis'], 2, "0", STR_PAD_LEFT) : '000'));
  if($asort == $bsort) {
    return 0;
  } else {
    return $asort < $bsort ? 1 : -1;
  }
}

//  Korekcijas
$korekcijas = db_get_assoc_all(db_query(
  'SELECT k.*, p.uz_rokas
  FROM '. DB_PREF .'bonusi_korekcijas as k
  LEFT JOIN '.DB_PREF.'pardeveji_rekini p ON p.id = k.rekins_id
  WHERE k.rekins_id  = ' . $darbinieka_rek_id));


//  Provīzijas un piemaksas
$query = db_query("
  SELECT
    d.*,
    p.uz_rokas,
    g.nosaukums as klienta_nosaukums,
    r.rek_nr as liguma_rek_nr,
    r.kopsumma as rekina_kopsumma,
    r.pvn as rekina_pvn,
    m.summa as maksajuma_summa,
    m.datums as maksajuma_datums,
    g.pardeveja_id
  FROM `".DB_PREF."pardeveji_rekini_detalas` d
  LEFT JOIN `".DB_PREF."pardeveji_rekini` p ON (p.id = d.pardeveja_rekina_id)
  LEFT JOIN `".DB_PREF."rekini` r ON (r.id = d.rekina_id)
  LEFT JOIN `".DB_PREF."ligumi` g ON (g.id = r.liguma_id)
  LEFT JOIN `".DB_PREF."rekini_maksajumi` m ON (m.id = d.rekina_maksajuma_id)
  WHERE d.pardeveja_rekina_id = ".$darbinieka_rek_id."
");

while($row = db_get_assoc($query)) {

  if (!empty($row['rekina_maksajuma_id']) && !empty($row['rekina_id'])) {
    $apmaksas[] = $row;
  } elseif (empty($row['rekina_maksajuma_id']) && !empty($row['rekina_id'])) {
    $avansa[] = $row;
  } elseif (empty($row['rekina_maksajuma_id']) && empty($row['rekina_id'])) {
    $piemaksas[] = $row;
  }
}

//  Prēmijas
$query = db_query("
  SELECT
    d.*,
    p.uz_rokas,
    g.nosaukums as klienta_nosaukums,
    g.id as liguma_id,
    r.rek_nr as liguma_rek_nr,
    r.kopsumma as rekina_kopsumma,
    SUM(m.summa) as maksajuma_summa,
    r.pvn as rekina_pvn,
    MAX(m.datums) as apmaksas_datums
  FROM `".DB_PREF."pardeveji_rekini_premijas_detalas` d
  LEFT JOIN `".DB_PREF."pardeveji_rekini` p ON (p.id = d.pardeveja_rekina_id)
  LEFT JOIN `".DB_PREF."rekini` r ON (r.id = d.rekina_id)
  LEFT JOIN `".DB_PREF."rekini_maksajumi` m ON (m.rekina_id = r.id)
  LEFT JOIN `".DB_PREF."ligumi` g ON (g.id = r.liguma_id)
  WHERE d.pardeveja_rekina_id = ".$darbinieka_rek_id."
  GROUP BY r.id
  HAVING rekina_kopsumma IS NOT NULL
");


while($row = db_get_assoc($query)) {

  if (bccomp(($row['rekina_kopsumma'] - $row['maksajuma_summa']), '0') == 1) {
    $p_avansa[] = $row;
  } else {
    $p_apmaksas[] = $row;
  }
}
?>

<h1 style="text-align: center;">Rēķins Nr. <strong><?= $rekins['rek_nr'] ?></strong></h1>
<?php
if(!empty($bonusi)) {?>
  <h2 style="margin-bottom: 0;">Saņemtie bonusi</h2>
  <table id="bonusi" class="data" style="width: 99%;">

    <thead>

      <tr class="last header">
        <th align="center">Uz<br />rokas</th>
        <th>Periods</th>
        <th>Sasniegtais EUR</th>
        <th>Proc. min. %</th>
        <th>Proc. opt. %</th>
        <th>Virs min. EUR</th>
        <th>Virs opt. EUR</th>
        <th>Summa min. EUR</th>
        <th>Summa opt. EUR</th>
        <th>Summa EUR (bez pvn)</th>
        <!--
        <th>PVN<br />(provīzija)&nbsp;EUR</th>
        <th>Summa<br />kopā&nbsp;EUR</th>
        -->
      </tr>

    </thead>
    <tbody class="main">
      <?php
      $total = array();
      $total['summa'] = 0;
      $total['virs_min'] = 0;
      $total['virs_opt'] = 0;
      $total['summa_min'] = 0;
      $total['summa_opt'] = 0;
      $total['kopsumma_wo_pvn'] = 0;

      foreach($bonusi as $bonuss) {

          $kopsumma_wo_pvn = $bonuss['summa_min'] + $bonuss['summa_opt'];

          $total['summa'] += $bonuss['summa'];
          $total['virs_min'] += $bonuss['virs_min'];
          $total['virs_opt'] += $bonuss['virs_opt'];
          $total['summa_min'] += $bonuss['summa_min'];
          $total['summa_opt'] += $bonuss['summa_opt'];
          $total['kopsumma_wo_pvn'] += $kopsumma_wo_pvn;
          ?>
          <tr>
            <td class="c"><?= !empty($bonuss['uz_rokas']) ? 'Jā' : 'Nē' ?></td>
            <td class="c"><?= $bonuss['gads'] . ($bonuss['ceturksnis'] ? ' ' . $_vars['quarters_roman'][$bonuss['ceturksnis']] : '') . ($bonuss['menesis'] ? ' ' . $_vars['menesi'][$bonuss['menesis']] : '') ?></td>
            <td class="r"><?= format_currency($bonuss['summa']); ?></td>
            <td class="c"><?= $bonuss['min_proc'] ?></td>
            <td class="c"><?= $bonuss['opt_proc'] ?></td>
            <td class="r"><?= format_currency($bonuss['virs_min']); ?></td>
            <td class="r"><?= format_currency($bonuss['virs_opt']); ?></td>
            <td class="r"><?= format_currency($bonuss['summa_min']); ?></td>
            <td class="r"><?= format_currency($bonuss['summa_opt']); ?></td>
            <td class="r bonuss_kopsumma_wo_pvn"><?= format_currency($kopsumma_wo_pvn); ?></td>
          </tr>

        <?php } ?>
    </tbody>

    <tr>

      <td align="left" colspan="2"><strong>Kopā:</strong></td>
      <td class="r"><?= format_currency($total['summa']) ?></td>
      <td colspan="2"></td>
      <td class="r"><?= format_currency($total['virs_min']) ?></td>
      <td class="r"><?= format_currency($total['virs_opt']) ?></td>
      <td class="r"><?= format_currency($total['summa_min']) ?></td>
      <td class="r"><?= format_currency($total['summa_opt']) ?></td>
      <td class="r"><?= format_currency($total['kopsumma_wo_pvn']) ?></td>
    </tr>

    </table>
<?php } ?>

<?php

//  Korekcijas

if(!empty($korekcijas)) {?>
  <h2 style="margin-bottom: 0;">Korekcijas</h2>
  <table id="korekcijas" class="data" style="width: 99%;">

    <thead>

      <tr class="last header">
        <th align="center">Uz<br />rokas</th>
        <th></th>
        <th>Summa EUR (bez pvn)</th>
      </tr>

    </thead>
    <tbody class="main">
      <?php
      $total = array(
        'kopsumma_wo_pvn' => 0,
      );



      foreach($korekcijas as $korekcija) {

          $total['kopsumma_wo_pvn'] += $korekcija['summa'];
          $nosaukums = 'Tekošā korekcija par iepriekšējiem periodiem';

          ?>
          <tr>
            <td class="c"><?= !empty($korekcija['uz_rokas']) ? 'Jā' : 'Nē' ?></td>
            <td class="c"><?=$nosaukums;?></td>
            <td class="r korekcija_kopsumma_wo_pvn">
              <?php
                echo format_currency($korekcija['summa']);
              ?>
            </td>
          </tr>

        <?php } ?>
    </tbody>

    <tr>

      <td align="left" colspan="2"><strong>Kopā:</strong></td>
      <td class="r"><?= format_currency($total['kopsumma_wo_pvn']) ?></td>
    </tr>

    </table>
<?php } ?>

<? if (!empty($apmaksas)) { ?>

  <h2 style="margin-bottom: 0;">Saņemtās provīzijas</h2>

  <table id="maksajumi" class="data" style="width: 99%;">

    <thead>

      <tr class="last header">
        <th align="center">Uz<br />rokas</th>
        <th>Klients</th>
        <th>Līguma slēdzējs</th>
        <th>Rēķina nr.</th>
        <th>Rēķina<br />summa&nbsp;EUR<br />(ar PVN)</th>
        <th>Saņemtā<br />samaksa&nbsp;EUR</th>
        <th>Apmaksas<br />Datums</th>
        <th>Provīzija&nbsp;%</th>
        <th>Provīzijas<br />summa&nbsp;EUR<br />(bez PVN)</th>
        <!--
        <th>PVN<br />(provīzija)&nbsp;EUR</th>
        <th>Summa<br />kopā&nbsp;EUR</th>
        -->
      </tr>

    </thead>

    <tbody class="main">

      <?
        $total = array();
        $total['rekina_summa'] = 0;
        $total['sanemta_samaksa'] = 0;
        $total['provizijas_summa'] = 0;
        $total['provizijas_pvn'] = 0;
        $total['summa_kopa'] = 0;
      ?>

      <? foreach($apmaksas as $apmaksa) { ?>

        <?
          $total['rekina_summa'] += $apmaksa['rekina_kopsumma'];
          $total['sanemta_samaksa'] += $apmaksa['maksajuma_summa'];
          $total['provizijas_summa'] += $apmaksa['summa'];
        ?>

        <tr>
          <td class="c"><?= !empty($apmaksa['uz_rokas']) ? 'Jā' : 'Nē' ?></td>
          <td><?= $apmaksa['klienta_nosaukums'] ?></td>
          <td><?php echo Pardevejs::model()->findByPk($apmaksa['pardeveja_id'])->vards;?></td>
          <td><?= $apmaksa['liguma_rek_nr'] ?></td>
          <td class="r"><?= $apmaksa['rekina_kopsumma'] ?></td>
          <td class="r">
            <? if (floatval($apmaksa['rekina_pvn']) == 0) { ?>
              <span class="bez_pvn_rek_ico">!</span>
            <? } ?>
            <?= $apmaksa['maksajuma_summa'] ?>
          </td>
          <td class="c"><?= date('d.m.Y', strtotime($apmaksa['maksajuma_datums'])) ?></td>
          <td class="c"><?= round($apmaksa['prov_likme'], 2) ?>%</td>
          <td class="r"><?= $apmaksa['summa'] ?></td>
          <!--
          <td><?= format_currency($apmaksa['kopsumma'] - $apmaksa['summa']) ?></td>
          <td><?= $apmaksa['kopsumma'] ?></td>
          -->
        </tr>

      <? } ?>

    </tbody>


    <tr>

      <td align="left" colspan="4"><strong>Kopā:</strong></td>
      <td class="r"><?= format_currency($total['rekina_summa']) ?></td>
      <td class="r"><?= format_currency($total['sanemta_samaksa']) ?></td>
      <td></td>
      <td></td>
      <td class="r"><?= format_currency($total['provizijas_summa']) ?></td>
      <!--<td><?= format_currency($total['provizijas_summa'] * ($rekins['pvn'] / 100)) ?></td>
      <td><?= format_currency($total['provizijas_summa'] * (1 + ($rekins['pvn'] / 100))) ?></td>-->

    </tr>


  </table>

<? } ?>

<? if (!empty($avansa)) { ?>

  <h2 style="margin-bottom: 0;">Avansa provīzijas</h2>

  <table id="avansa_summas" class="data" style="width: 99%;">

    <thead>

      <tr class="last header">
        <th align="center">Uz<br />rokas</th>
        <th>Klients</th>
        <th>Līguma slēdzējs</th>
        <th>Rēķina<br />nr.</th>
        <th>Rēķina<br />summa&nbsp;EUR<br />(ar PVN)</th>
        <th>Neapmaksātā<br />summa&nbsp;EUR</th>
        <th>Provīzija&nbsp;%</th>
        <th>Provīzijas<br />summa&nbsp;EUR<br />(bez PVN)</th>
        <!--
        <th>PVN<br />(provīzija)&nbsp;EUR</th>
        <th>Summa<br />kopā&nbsp;EUR</th>
        -->
      </tr>

    </thead>

    <tbody class="main">

      <?
        $total = array();
        $total['rekina_kopsumma'] = 0;
        $total['neapm_summa'] = 0;
        $total['provizijas_summa'] = 0;
      ?>

      <? foreach($avansa as $apmaksa) { ?>

        <?
          $total['rekina_kopsumma'] += $apmaksa['rekina_kopsumma'];
          $total['neapm_summa'] += $apmaksa['no_summas'];
          $total['provizijas_summa'] += $apmaksa['summa'];
        ?>

        <tr>
          <td class="c"><?= !empty($apmaksa['uz_rokas']) ? 'Jā' : 'Nē' ?></td>
          <td><?= $apmaksa['klienta_nosaukums'] ?></td>
          <td><?php echo Pardevejs::model()->findByPk($apmaksa['pardeveja_id'])->vards;?></td>
          <td><?= $apmaksa['liguma_rek_nr'] ?></td>
          <td class="r"><?= $apmaksa['rekina_kopsumma'] ?></td>
          <td class="r">
            <? if (floatval($apmaksa['rekina_pvn']) == 0) { ?>
              <span class="bez_pvn_rek_ico">!</span>
            <? } ?>
            <?= $apmaksa['no_summas'] ?>
          </td>
          <td class="c"><?= round($apmaksa['prov_likme'], 2) ?>%</td>
          <td class="r"><?= $apmaksa['summa'] ?></td>
          <!--
          <td><?= format_currency($apmaksa['kopsumma'] - $apmaksa['summa']) ?></td>
          <td><?= $apmaksa['kopsumma'] ?></td>
          -->
        </tr>

      <? } ?>

    </tbody>



    <tr>

      <td align="left" colspan="4"><strong>Kopā:</strong></td>
      <td class="r"><?= format_currency($total['rekina_kopsumma']) ?></td>
      <td class="r"><?= format_currency($total['neapm_summa']) ?></td>
      <td></td>
      <td class="r"><?= format_currency($total['provizijas_summa']) ?></td>
      <!--<td><?= format_currency($total['provizijas_summa'] * ($rekins['pvn'] / 100)) ?></td>
      <td><?= format_currency($total['provizijas_summa'] * (1 + ($rekins['pvn'] / 100))) ?></td>-->

    </tr>



  </table>

<? } ?>

<? if (!empty($p_apmaksas)) { ?>
  <h2 style="margin-bottom: 0;">Saņemtās prēmijas</h2>

  <table id="p_maksajumi" class="data" style="width: 99%;">

    <thead>

      <tr class="last header">
        <th align="center">Uz<br />rokas</th>
        <th>Klients</th>
        <th>Rēķina nr.</th>
        <th>Rēķina<br />summa&nbsp;EUR<br />(ar PVN)</th>
        <th>Saņemtā<br />samaksa&nbsp;EUR</th>
        <th>Apmaksas<br />Datums</th>
        <th>Prēmija&nbsp;%</th>
        <th>Prēmijas<br />summa&nbsp;EUR<br />(bez PVN)</th>
        <th>Ceturksnis</th>
        <!--
        <th>PVN<br />(prēmija)&nbsp;EUR</th>
        <th>Summa<br />kopā&nbsp;EUR</th>
        -->
      </tr>

    </thead>

    <tbody class="main">

      <?
        $total = array();
        $total['rekina_summa'] = 0;
        $total['sanemta_samaksa'] = 0;
        $total['premijas_summa'] = 0;
        $total['premijas_pvn'] = 0;
        $total['summa_kopa'] = 0;
      ?>

      <?
      $i = 0;
      foreach($p_apmaksas as $apmaksa) { ?>
        <?
          $total['rekina_summa'] += $apmaksa['rekina_kopsumma'];
          $total['sanemta_samaksa'] += $apmaksa['maksajuma_summa'];
          $total['premijas_summa'] += $apmaksa['summa'];

          list($year, $quarter) = getYearQuarter($apmaksa);

        ?>

        <tr>
          <td class="c"><?= !empty($apmaksa['uz_rokas']) ? 'Jā' : 'Nē' ?></td>
          <td><?= $apmaksa['klienta_nosaukums'] ?></td>
          <td><?= $apmaksa['liguma_rek_nr'] ?></td>
          <td class="r"><?= $apmaksa['rekina_kopsumma'] ?></td>
          <td class="r">
            <? if (floatval($apmaksa['rekina_pvn']) == 0) { ?>
              <span class="bez_pvn_rek_ico">!</span>
            <? } ?>
            <?= $apmaksa['maksajuma_summa'] ?>
          </td>
          <td class="c"><?= date('d.m.Y', strtotime($apmaksa['apmaksas_datums'])) ?></td>
          <td class="c"><?= round($apmaksa['prem_likme'], 2) ?>%</td>
          <td class="r"><?= $apmaksa['summa'] ?></td>
          <td class="c"><?=$year . '.g ' . $_vars['quarters_roman'][$quarter];?></td>
          <!--
          <td><?= format_currency($apmaksa['kopsumma'] - $apmaksa['summa']) ?></td>
          <td><?= $apmaksa['kopsumma'] ?></td>
          -->
        </tr>

      <? } ?>

    </tbody>

    <tr>

      <td align="left" colspan="3"><strong>Kopā:</strong></td>
      <td class="r"><?= format_currency($total['rekina_summa']); ?></td>
      <td class="r"><?= format_currency($total['sanemta_samaksa']); ?></td>
      <td></td>
       <td></td>
       <td class="r"><?= format_currency($total['premijas_summa']); ?></td>
       <td></td>
    </tr>

  </table>
<? } ?>

<? if (!empty($p_avansa)) { ?>

  <h2 style="margin-bottom: 0;">Avansa prēmijas</h2>

  <table id="p_avansa_summas" class="data" style="width: 99%;">

    <thead>

      <tr class="last header">
        <th align="center">Uz<br />rokas</th>
        <th>Klients</th>
        <th>Rēķina<br />nr.</th>
        <th>Rēķina<br />summa&nbsp;EUR<br />(ar PVN)</th>
        <th>Neapmaksātā<br />summa&nbsp;EUR</th>
        <th>Prēmija&nbsp;%</th>
        <th>Prēmijas<br />summa&nbsp;EUR<br />(bez PVN)</th>
        <th>Ceturksnis</th>
        <!--
        <th>PVN<br />(provīzija)&nbsp;EUR</th>
        <th>Summa<br />kopā&nbsp;EUR</th>
        -->
      </tr>

    </thead>

    <tbody class="main">

      <?
        $total = array();
        $total['rekina_kopsumma'] = 0;
        $total['neapm_summa'] = 0;
        $total['premijas_summa'] = 0;
      ?>

      <? foreach($p_avansa as $apmaksa) { ?>

        <?
          $total['rekina_kopsumma'] += $apmaksa['rekina_kopsumma'];
          $total['neapm_summa'] += $apmaksa['no_summas'];
          $total['premijas_summa'] += $apmaksa['summa'];

          list($year, $quarter) = getYearQuarter($apmaksa);
        ?>

        <tr>
          <td class="c"><?= !empty($apmaksa['uz_rokas']) ? 'Jā' : 'Nē' ?></td>
          <td><?= $apmaksa['klienta_nosaukums'] ?></td>
          <td><?= $apmaksa['liguma_rek_nr'] ?></td>
          <td class="r"><?= $apmaksa['rekina_kopsumma'] ?></td>
          <td class="r">
            <? if (floatval($apmaksa['rekina_pvn']) == 0) { ?>
              <span class="bez_pvn_rek_ico">!</span>
            <? } ?>
            <?= $apmaksa['no_summas'] ?>
          </td>
          <td class="c"><?= round($apmaksa['prem_likme'], 2) ?>%</td>
          <td class="r"><?= $apmaksa['summa'] ?></td>
          <td class="c"><?=$year . '.g ' . $_vars['quarters_roman'][$quarter];?></td>
          <!--
          <td><?= format_currency($apmaksa['kopsumma'] - $apmaksa['summa']) ?></td>
          <td><?= $apmaksa['kopsumma'] ?></td>
          -->
        </tr>

      <? } ?>

    </tbody>

    <tr>

      <td align="left" colspan="3"><strong>Kopā:</strong></td>
      <td class="r"><?= format_currency($total['rekina_kopsumma']) ?></td>
      <td class="r"><?= format_currency($total['neapm_summa']) ?></td>
      <td></td>
      <td class="r"><?= format_currency($total['premijas_summa']) ?></td>
      <td></td>

    </tr>

  </table>

<? } ?>

<? if (!empty($piemaksas)) { ?>

  <h2 style="margin-bottom: 0;">Piemaksas</h2>

  <table id="piemaksas" class="data" style="width: 99%;">

    <thead>

      <tr class="last header">
        <th style="width: 5em;" align="center">Uz<br />rokas</th>
        <th>Paskaidrojums</th>
        <th style="width: 8em;">Summa EUR<br />(bez PVN)</th>
        <!--
        <th style="width: 8em;">PVN EUR</th>
        <th style="width: 8em;">Summa<br />kopā&nbsp;EUR</th>
        -->
      </tr>

    </thead>

    <tbody class="main">

      <?
        $total = array();
        $total['summa'] = 0;
      ?>

      <? foreach($piemaksas as $piemaksa) { ?>

        <?
          $total['summa'] += $piemaksa['summa'];
        ?>

        <tr>
          <td class="c"><?= !empty($piemaksa['uz_rokas']) ? 'Jā' : 'Nē' ?></td>
          <td><?= $piemaksa['paskaidrojums'] ?></td>
          <td class="r"><?= $piemaksa['summa'] ?></td>
          <!--
          <td><?= format_currency($piemaksa['kopsumma'] - $piemaksa['summa']) ?></td>
          <td><?= $piemaksa['kopsumma'] ?></td>
          -->
        </tr>

      <? } ?>

    </tbody>



    <tr>

      <td align="left" colspan="2"><strong>Kopā:</strong></td>
      <td class="r"><?= format_currency($total['summa']) ?></td>
      <!--<td><?= format_currency($total['summa'] * ($rekins['pvn'] / 100)) ?></td>
      <td><?= format_currency($total['summa'] * (1 + ($rekins['pvn'] / 100))) ?></td>-->

    </tr>



  </table>

<? } ?>

<div style="padding-right: 10px;">

  <div style="text-align: right;">
    Summa: <span style="float: right; width: 7em;">EUR <strong><?= format_currency($rekins['summa']) ?></strong></span>
  </div>

  <div style="text-align: right;">
    PVN: <span style="float: right; width: 7em;">EUR <strong><?= format_currency($rekins['kopsumma'] - $rekins['summa']) ?></strong></span>
  </div>

  <div style="text-align: right;">
    Kopsumma: <span style="float: right; width: 7em;">EUR <strong><?= format_currency($rekins['kopsumma']) ?></strong></span>
  </div>

</div>

<script>
stripeTable($('#bonusi'));
stripeTable($('#korekcijas'));
stripeTable($('#maksajumi'));
stripeTable($('#avansa_summas'));
stripeTable($('#p_maksajumi'));
stripeTable($('#p_avansa_summas'));
stripeTable($('#piemaksas'));

$(document).ready(function() {

  $('#korekcijas .bez_pvn_rek_ico').wTooltip({
    content: 'Korekcija ir par bonusiem, kuri tika izmaksāti bez PVN',
    follow: true,
    offsetY: 0,
    offsetX: 10
  });

  $('#maksajumi .bez_pvn_rek_ico').wTooltip({
    content: 'Maksājuma ir saņemts bez PVN',
    follow: true,
    offsetY: 0,
    offsetX: 10
  });


  $('#avansa_summas .bez_pvn_rek_ico').wTooltip({
    content: 'Maksājums tiks saņemts bez PVN',
    follow: true,
    offsetY: 0,
    offsetX: 10
  });

  $('#p_maksajumi .bez_pvn_rek_ico').wTooltip({
    content: 'Maksājums ir saņemts bez PVN',
    follow: true,
    offsetY: 0,
    offsetX: 10
  });


  $('#p_avansa_summas .bez_pvn_rek_ico').wTooltip({
    content: 'Maksājums tiks saņemts bez PVN',
    follow: true,
    offsetY: 0,
    offsetX: 10
  });

});
</script>
