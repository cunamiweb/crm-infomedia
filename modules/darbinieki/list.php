<?
$agenturas = false;

require('list_process.php');

log_add("atvera", "Atvēra darbinieku kopsavilkumu");

if (date('m') <= 3) {
  //$cur_quarter .= 1;
  $cur_quarter_nr = 1;
} elseif (date('m') <= 6) {
  //$cur_quarter .= 2;
  $cur_quarter_nr = 2;
} elseif (date('m') <= 9) {
  //$cur_quarter .= 3;
  $cur_quarter_nr = 3;
} elseif (date('m') <= 12) {
  //$cur_quarter .= 4;
  $cur_quarter_nr = 4;
}

$cur_quarter = '2011-' . $cur_quarter_nr;

// par vecākiem ceturkšņiem dati var nebūt precīzi, tāpēc nemaz nepiedāvājam
$y = '2011';
$q = '1';

$quarters_roman = array(1 => 'I', 2 => 'II', 3 => 'III', 4 => 'IV');

$quarters = array();

//while($y . '-' . $q <= $cur_quarter) {
while($y < date('Y') || ($y == date('Y') && $q <= $cur_quarter_nr)) {
  $quarters[] = array(
    $y . '-' . $q,
    $y . ' - ' . $quarters_roman[$q]
  );

  if ($q < 4) {
    $q ++;
  } else {
    $y++;
    $q = 1;
  }

}

if (!empty($_GET['stats_quarter'])) {
  $stats_quarter = $_GET['stats_quarter'];
} else {
  $stats_quarter = $cur_quarter;
}

?>

<?php if(check_access('darbinieki-saraksts-filtrs')) {?>
<form action="" method="get" id="period_filter">
  <input type="hidden" name="c" value="darbinieki" />
  <?php if($agenturas) { ?>
    <input type="hidden" name="a" value="agenturas" />
  <?php } ?>
  <div class="item" style="float: left; text-align: left;">
    Periods:
    <span><input id="date_from" name="date_from" type="input" style="width:80px;" value="<?= !empty($_GET['date_from']) ? $_GET['date_from'] : '' ?>" />
    -
    <input id="date_to" name="date_to" type="input" style="width:80px;" value="<?= !empty($_GET['date_to']) ? $_GET['date_to'] : '' ?>">
    <a class="clear_form" href="#">x</a></span>
  </div>
  <div class="item" style="float: left; text-align: left; margin-left: 30px;">
    Statistika par ceturksni:
    <span>
      <select id="stats_quarter" name="stats_quarter" type="input">
        <? foreach($quarters as $y => $q) { ?>
          <option <?= ($q[0] == $stats_quarter) ? 'selected="selected"' : '' ?> value="<?= $q[0] ?>"><?= $q[1] ?></option>
        <? } ?>
      </select>
    </span>
	</div>
  <div class="item" style="float: left; text-align: left; margin-left: 30px;">
    Rādīt neaktīvos:
    <span><input id="inactive" name="inactive" type="checkbox" value="1" <?= !empty($_GET['inactive']) ? 'checked="checked"' : '' ?> /></span>
  </div>
</form>
<?php } ?>


<? if (check_access('darbinieki-jauns')) { ?>
  <button style="float: right;" onclick="document.location='?c=darbinieki&a=jauns'" class="ui-state-default ui-corner-all">Pievienot jaunu darbinieku</button>
<? } ?>

<div class="clear"></div>

<script>
var searchlist = [];

var sp = create_filter_field_array([
  's_pardevejs', 's_ligumu_sk', 's_lig_kopsumma', 's_lig_vid_summa',
  's_apm_lig_kopsumma', 's_vid_prov_proc', 's_apr_prov', 's_vid_apr_prov',
  's_apmaksajama_prov', 's_apmaksajama_prov_percent', 's_izm_prov',
  's_apr_piem', 's_izm_piem', 's_rek_apm_percent', 's_dept_cred',
  's_stat_sk', 's_stat_summa', 's_stat_proc_sk', 's_stat_proc_summa',
  's_stat_atdoti_sk', 's_stat_atdoti_apgr', 's_akt_atg', 's_apr_prem', 's_apm_prem', 's_izm_prem'
]);
</script>

<?  // Prototypes // ?>
<div style="display:none;">

  <div id="komentarsprot__darb_kopsavilkums">
    <table class="comment_form">
      <tr>
        <td width="70">Komentārs:</td>
        <td>
          <textarea name="komentari_new[_darb_kopsavilkums][komentars][]" rows="2" cols="30"></textarea>
        </td>
      </tr>
    </table>
  </div>

</div>

<table style="display:none;">

  <tr id="darbinieki_kopsavilkums_groups">
    <th></th>
    <?php if(check_access('darbinieki-k-ligumi')) { ?>
      <th colspan="4" class="bgc-lig">Līgumi</th>
    <?php } ?>

    <?php if(check_access('darbinieki-k-statistika')) {?>
      <th colspan="4" class="bgc-cta">Statistika par ceturksni</th>
    <?php } ?>

    <?php if(check_access('darbinieki-k-provizijas')) {?>
      <th colspan="6" class="bgc-pro">Provīzijas</th>
    <?php } ?>

    <?php if(check_access('darbinieki-k-premijas')) {?>
      <th colspan="3" class="bgc-prem">Prēmijas</th>
    <?php } ?>

    <?php if(check_access('darbinieki-k-piemaksas')) {?>
      <th colspan="2" class="bgc-pie">Piemaksas</th>
    <?php } ?>

    <?php if(check_access('darbinieki-k-bilance')) { ?>
      <th colspan="2" class="bgc-bil">Bilance</th>
    <?php } ?>

    <?php if(check_access('darbinieki-k-atgadinajumi')) {?>
      <th>Atg.</th>
    <?php } ?>
  </tr>

  <tr id="darbinieki_kopsavilkums_filter">

    <th>
      <select class="search_field advonchange" searchclass="s_pardevejs" meth="int">
        <option value=""></option>
        <?php foreach($_vars['sys_pardeveji_sales_only'] as $k=>$p){ ?>
          <option value="<?=$k;?>"><?=$p;?></option>
        <?php } ?>
      </select>
      <a class="search_field" href="#">x</a>
    </th>

    <?php if(check_access('darbinieki-k-ligumi')) {?>
      <th><span><input class="search_field" meth="int" searchclass="s_ligumu_sk" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
      <th><span><input class="search_field" meth="coin" searchclass="s_lig_kopsumma" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
      <th><span><input class="search_field" meth="coin" searchclass="s_lig_vid_summa" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
      <th><span><input class="search_field" meth="coin" searchclass="s_apm_lig_kopsumma" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <?php } ?>

    <?php if(check_access('darbinieki-k-statistika')) {?>
      <th><span><input class="search_field" meth="coin" searchclass="s_stat_sk" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
      <th><span><input class="search_field" meth="coin" searchclass="s_stat_summa" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
      <th><span><input class="search_field" meth="coin" searchclass="s_stat_proc_sk" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
      <th><span><input class="search_field" meth="coin" searchclass="s_stat_proc_summa" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <?php } ?>

    <?php if(check_access('darbinieki-k-provizijas')) {?>
      <th><span><input class="search_field" meth="coin" searchclass="s_vid_prov_proc" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
      <th><span><input class="search_field" meth="coin" searchclass="s_apr_prov" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
      <th><span><input class="search_field" meth="coin" searchclass="s_vid_apr_prov" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
      <th><span><input class="search_field" meth="coin" searchclass="s_apmaksajama_prov" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
      <th><span><input class="search_field" meth="coin" searchclass="s_apmaksajama_prov_percent" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
      <th><span><input class="search_field" meth="coin" searchclass="s_izm_prov" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <?php } ?>

    <?php if(check_access('darbinieki-k-premijas')) {?>
      <th><span><input class="search_field" meth="coin" searchclass="s_apr_prem" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
      <th><span><input class="search_field" meth="coin" searchclass="s_apm_prem" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
      <th><span><input class="search_field" meth="coin" searchclass="s_izm_prem" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <?php } ?>

    <?php if(check_access('darbinieki-k-piemaksas')) {?>
      <th><span><input class="search_field" meth="coin" searchclass="s_apr_piem" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
      <th><span><input class="search_field" meth="coin" searchclass="s_izm_piem" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <?php } ?>

    <?php if(check_access('darbinieki-k-bilance')) { ?>
      <th><span><input class="search_field" meth="coin" searchclass="s_rek_apm_percent" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
      <th><span><input class="search_field" meth="coin" searchclass="s_dept_cred" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <?php } ?>

    <?php if(check_access('darbinieki-k-atgadinajumi')) {?>
      <th>
        <select class="search_field advonchange" searchclass="s_akt_atg" meth="int">
          <option value=""></option>
          <option value="1">Ir</option>
          <option value="2">Nav</option>
        </select>
        <a class="search_field" href="#">x</a>
      </th>
    <?php } ?>

  </tr>

</table>                                                               
<table cellpadding="3" cellspacing="0" id="darbinieki_kopsavilkums" class="data ckey_darbinieki_list" style="width: auto;">

  <thead>

    <tr class="header">
      <th width="150" class="header" colname="darbinieks">Darbinieks</th>

      <?php if(check_access('darbinieki-k-ligumi')) {?>
        <th width="40" class="header" colname="lig_sk">Līgumu sk.</th>
        <th width="50" class="header" colname="lig_kopsumma">Līg.&nbsp;kopsumma<br />EUR&nbsp;(bez PVN)</th>
        <th width="50" class="header" colname="lig_vid_summa">Līg.&nbsp;vid.<br />summa&nbsp;EUR<br />(bez PVN)</th>
        <th width="50" class="header" colname="lig_apm_kopsumma">Apmaksātā līg.<br />kopsumma<br />(ar PVN)</th>
      <?php } ?>

      <?php if(check_access('darbinieki-k-statistika')) {?>
        <th width="50" class="header" colname="stat_sk">Līg.<br />skaits</th>
        <th width="50" class="header" colname="stat_summa">Līg.<br />kopsumma</th>
        <th width="50" class="header" colname="stat_proc_sk">%&nbsp;no<br />atdoto&nbsp;skaita</th>
        <th width="50" class="header" colname="stat_proc_summa">%&nbsp;no<br />atdoto&nbsp;apgr.</th>
      <?php } ?>

      <?php if(check_access('darbinieki-k-provizijas')) {?>
        <th width="50" class="header" colname="prov_procent_vid">Provīzija&nbsp;%<br />(vid.)</th>
        <th width="50" class="header" colname="apr_prov_summa">Apr.&nbsp;provīzija<br />EUR&nbsp;(bez PVN)</th>
        <th width="50" class="header" colname="vid_apr_prov_summa">Vid. apr.<br />provīzija&nbsp;EUR<br />(bez PVN)</th>
        <th width="50" class="header" colname="apm_prov_summa">Apmaksātā<br />provīzija&nbsp;EUR<br />(bez PVN)</th>
        <th width="50" class="header" colname="apm_prov_procent">Apmaksātā<br />provīzija&nbsp;%<br /></th>
        <th width="50" class="header" colname="izm_prov_summa">Izmaksātā<br />provīz.&nbsp;EUR<br />(bez PVN)</th>
      <?php } ?>

      <?php if(check_access('darbinieki-k-premijas')) {?>
        <th width="50" class="header" colname="apr_prem_summa">Aprēķinātā<br />prēmija&nbsp;EUR<br />(bez PVN)</th>
        <th width="50" class="header" colname="apr_prem_summa">Apmaksātā<br />prēmija&nbsp;EUR<br />(bez PVN)</th>
        <th width="50" class="header" colname="izm_prem_summa">Izmaksātā<br />prēmija.&nbsp;EUR<br />(bez PVN)</th>
      <?php } ?>

      <?php if(check_access('darbinieki-k-piemaksas')) {?>
        <th width="50" class="header" colname="apr_piem_summa">Aprēķinātās<br />piemaksas&nbsp;EUR<br />(bez PVN)</th>
        <th width="50" class="header" colname="izm_piem_summa">Izmaksātās<br />piemaksas&nbsp;EUR<br />(bez PVN)</th>
      <?php } ?>

      <?php if(check_access('darbinieki-k-bilance')) { ?>
        <th width="50" class="header" colname="rek_apm_procent">Rēķinu<br />apmaksa&nbsp;%</th>
        <th width="50" class="header" colname="deb_kred_bez_pvn">Debepts/Kredīts<br />EUR&nbsp;(bez PVN)</th>
      <?php } ?>

      <?php if(check_access('darbinieki-k-atgadinajumi')) {?>
        <th width="40" class="header" colname="akt_atgad">Aktīvie<br />atgādinājumi</th>
      <?php } ?>
    </tr>

  </thead>

  <tbody class="main">

    <?php
    $period_where = array();

    if (!empty($_GET['date_from'])) {
      $period_where[] = "g.sanemsanas_datums >= '".esc(date('Y-m-d', strtotime($_GET['date_from'])))."'";
    }

    if (!empty($_GET['date_to'])) {
      $period_where[] = "g.sanemsanas_datums <= '".esc(date('Y-m-d', strtotime($_GET['date_to'])))."'";
    }

    $pardeveju_ids = array();

    $ligumu_summa = array();
    $katra_liguma_summa = array();
    $katra_liguma_summa_bez_pvn = array();
    $katra_liguma_apm_summa = array();
    $katra_liguma_apm_summa_bez_pvn = array();
    $ligumu_apm_summa = array();
    $prov_proc_summa = array();
    $apr_prov = array();
    $apmaksajama_prov = array();
    $izmaks_prov = array();
    $apr_piem = array();
    $izmaks_piem = array();
    $uz_izmaksu_prov = array();
    $apr_prem = array();
    $apm_prem = array();
    $izm_prem = array();

    $premiums_cache = array(); // not real cache. confusing name for this var

    /*function getContractPremium($row, &$cache, $merchant_id)
    {
      list($year, $quarter) = getYearQuarter($row, $merchant_id);

      if(!isset($cache[$merchant_id][$year][$quarter])) {
        $cache[$merchant_id][$year][$quarter] = get_contract_premium($year, $quarter, $merchant_id);
        //get_statistika('quarter', $stats_quarter, $stats_quarter, $row['id']);
        //  $stats quarter = '2011-1'
      }
      return $cache[$merchant_id][$year][$quarter];
    }                 */

    function getContractPremium($row, &$cache, $merchant_id) {
      list($year, $quarter) = getYearQuarter($row, $merchant_id);


      if(CACHING_ENABLED) {    // real cache

        $cache_key = 'getContractPremium_' . $merchant_id . '_' . $year . '_' . $quarter;

        $contract_premium = cache_get($cache_key);

        if(!$contract_premium) {   // cache not found. calculate and save cache

          $contract_premium = get_contract_premium($year, $quarter, $merchant_id);
          cache_set($cache_key, $contract_premium);

        }

        return $contract_premium;

      } else {

        if(!isset($cache[$merchant_id][$year][$quarter])) {
          $cache[$merchant_id][$year][$quarter] = get_contract_premium($year, $quarter, $merchant_id);
          //get_statistika('quarter', $stats_quarter, $stats_quarter, $row['id']);
          //  $stats quarter = '2011-1'
        }
        return $cache[$merchant_id][$year][$quarter];
      }
    }

    $quarters_ligumi = array();

    /**
     * Atrod gadu un ceturksni pēc līguma id
     * @param array $row - rēķina rinda
     * return mixed false ja neatrod; [year, quarter] ja atrod
     */
    function getYearQuarter($row, $merchant_id)
    {
      $cache_key = 'getYearQuarter_' . $row['liguma_id'] . '_' . $merchant_id;

      $yearQuarter = cache_get($cache_key);

      if(!$yearQuarter) {
        global $quarters_ligumi;
        foreach($quarters_ligumi[$merchant_id] as $ligumi_year => $ligumi_quarters) {
          foreach($ligumi_quarters as $ligumi_quarter => $l_ids) {
            if(in_array($row['liguma_id'], $l_ids)) {
              $yearQuarter = array($ligumi_year, $ligumi_quarter);
              cache_set($cache_key, $yearQuarter);
            }
          }
        }
      }

      return $yearQuarter;
    }

    function getIzpildeQuarter($year, $quarter, &$cache, $darbinieka_id)
    {
      if(CACHING_ENABLED) {

        $cache_key = 'getIzpildeQuarter_' . $darbinieka_id . '_' . $year . '_' . $quarter;

        $izpilde_quarter = cache_get($cache_key);

        if(!$izpilde_quarter) {
          $izpilde_quarter = get_izpildits($darbinieka_id, $year, $quarter, null, true);
          cache_set($cache_key, $izpilde_quarter);
        }

        return $izpilde_quarter;

      } else {

        if(!isset($cache[$darbinieka_id][$year][$quarter])) {
          $cache[$darbinieka_id][$year][$quarter] = get_izpildits($darbinieka_id, $year, $quarter, null, true);
        }

        return $cache[$darbinieka_id][$year][$quarter];

      }
    }

    $sql = "
      SELECT
        p.*,
        (
          SELECT SUM(sd.summa)
          FROM `".DB_PREF."pardeveji_rekini_detalas` sd
          LEFT JOIN `".DB_PREF."pardeveji_rekini` sr ON (sr.id = sd.pardeveja_rekina_id)
          WHERE
            sr.pardeveja_id = p.id AND
            sr.statuss = 2 AND
            sd.rekina_id IS NOT NULL AND
            sd.rekina_maksajuma_id IS NULL
        ) as izmaksats_avansaa_bez_pvn,
        (
          SELECT COUNT(*)
          FROM `".DB_PREF."atgadinajumi` sa
          USE INDEX (pardeveja_id_index)
          LEFT JOIN `".DB_PREF."ligumi` sg ON (sa.liguma_id = sg.id)
          WHERE
            (
              sa.pardeveja_id = p.id OR
              sg.pardeveja_id = p.id OR
              sg.id IN (
                SELECT liguma_id
                FROM `".DB_PREF."ligumi_saistitie_pardeveji`
                WHERE pardeveja_id = p.id
              )
            ) AND
            sa.statuss = 1 AND
            sa.datums <= CURDATE() AND
            sa.redzamiba IN ('kopejs', ".(is_admin() ? "'admin'" : "'pardevejs'").")
        ) as aktivi_atgadinajumi
      FROM `".DB_PREF."pardeveji` p
    ";

		$where = array();

    //  Nerādīt partnerus
    $where[] = '(p.vaditajs_id IS NULL || p.vaditajs_id != ' . KORP_VADITAJA_ID . ')';

		if (check_access('darbinieki-admin')) {
		  if(getRequest('pardevejs_id'))  {
        $where[] = 'p.id = ' . (int)getRequest('pardevejs_id');
		  } else {
			  $where[] = 'p.tips = "sales"';
      }
		} else {
			$where[] = 'p.id = ' . (int)$_SESSION['user']['id'];
		}

		if (empty($_GET['inactive']))
			$where[] = 'p.aktivs = 1';

		if (!empty($where))
			$sql .= " WHERE " . implode(' AND ', $where);

    $sql .= "
      ORDER BY p.`vards` ASC
    ";

    $query = db_query($sql);

    if(!isset($data)) {
      $data = array();
    }

    while($row = db_get_assoc($query)) {

      $cache_key_statistika = 'get_statistika_quarter_' . $stats_quarter . '_' . $row['id'];

      $row['quarter_stats'] = cache_get($cache_key_statistika);

      if(!$row['quarter_stats']) {
        $row['quarter_stats'] = get_statistika('quarter', $stats_quarter, $stats_quarter, $row['id']);
        cache_set($cache_key_statistika, $row['quarter_stats']);
      }

      $data[] = $row;

      $pardeveju_ids[] = $row['id'];

      $ligumu_summa[$row['id']] = 0;
      $ligumu_apm_summa[$row['id']] = 0;
      $prov_proc_summa[$row['id']] = 0;
      $izmaks_prov[$row['id']] = 0;
      $apr_piem[$row['id']] = 0;
      $izmaks_piem[$row['id']] = 0;
      $uz_izmaksu_prov[$row['id']] = 0;
      $apr_prem[$row['id']] = 0;
      $apm_prem[$row['id']] = 0;
      $izm_prem[$row['id']] = 0;
    }

    // -----
		// Līgumi

    $sql = "
      SELECT g.*
      FROM `".DB_PREF."ligumi` g
    ";

    $lig_where = $period_where;

    if ($pardeveju_ids) {
      $lig_where[] = "pardeveja_id IN (".implode(',', $pardeveju_ids).")";
    }

    if (!empty($lig_where)) {
      $sql .= "WHERE " . implode(" AND ", $lig_where);
    }

    $query = db_query($sql);
    $ligumi = array();

    while($row = db_get_assoc($query)){
      if(in_array($row['pardeveja_id'], $pardeveju_ids)) {
        $ligumi[$row['pardeveja_id']][$row['id']] = $row;

        $prov_proc_summa[$row['pardeveja_id']] += $row['provizija_pardevejam'];

        $katra_liguma_summa[$row['pardeveja_id']][$row['id']] = 0;
        $katra_liguma_summa_bez_pvn[$row['pardeveja_id']][$row['id']] = 0;
        $katra_liguma_apm_summa[$row['pardeveja_id']][$row['id']] = 0;
        $katra_liguma_apm_summa_bez_pvn[$row['pardeveja_id']][$row['id']] = 0;
      }
    }

    // -----

    //  Prēmijas

    foreach($pardeveju_ids as $pardeveja_id) {

      $cache_prem_data_key = 'premData_' . $pardeveja_id;

      $cached_prem_data = cache_get($cache_prem_data_key);

      if(!$cached_prem_data) {

        $apr_prem[$pardeveja_id] = 0;
        $apm_prem[$pardeveja_id] = 0;

        //  Izmaksātās prēmijas.

          $query = "
              SELECT d.summa, d.rekina_id
              FROM `".DB_PREF."pardeveji_rekini_premijas_detalas` d
              LEFT JOIN `".DB_PREF."rekini` r ON r.id = d.rekina_id
              LEFT JOIN `".DB_PREF."pardeveji_rekini` pr ON pr.id = d.pardeveja_rekina_id
              WHERE
                pr.pardeveja_id = " . $pardeveja_id . "
                AND pr.apm_datums IS NOT NULL
              ";
          if(isset($_GET['date_from']) && $_GET['date_from']) {
            $query .= " AND r.apm_termins >= '" . esc(date('Y-m-d', strtotime($_GET['date_from']))) . "' ";
          }

          if(isset($_GET['date_to']) && $_GET['date_to']) {
            $query .= " AND r.apm_termins <= '" . esc(date('Y-m-d', strtotime($_GET['date_to']))) . "' ";
          }

          $result = db_query($query);
          $izm_prem[$pardeveja_id] = 0;
          $premijas_izmaksatas_par_rekiniem = array();

          while($iprow = db_get_assoc($result)) {
            $izm_prem[$pardeveja_id] += $iprow['summa'];
            $premijas_izmaksatas_par_rekiniem[$iprow['rekina_id']] = $iprow['summa'];
          }

        //  Apr

        $result = db_query("
          SELECT *
          FROM `".DB_PREF."premijas` p
          WHERE
            p.pardevejs_id = " .  esc($pardeveja_id) . " AND
            p.ir_premija = 1
        ");

        $premium = mysqli_num_rows($result) > 0 ? true : false;

        if($premium) {
          $prem_ligumu_id = array();
          while($row = db_get_assoc($result)) {
            //  No gada un ceturkšņa numura jāuztaisa datumu range.
            //  Ceturkšņa pirmā mēneša 1. datums līdz ceturkšņa pēdējā mēneša pēdējais datums (31?)

            $statdata = getIzpildeQuarter($row['gads'], $row['ceturksnis'], $yearly_stats, $pardeveja_id);

            $prem_ligumu_id = array_merge($prem_ligumu_id, $statdata['kopa']['ligumu_id']);

              if(!isset($quarters_ligumi[$pardeveja_id][$row['gads']][$row['ceturksnis']])) {
                $quarters_ligumi[$pardeveja_id][$row['gads']][$row['ceturksnis']] = array();
              }
              $quarters_ligumi[$pardeveja_id][$row['gads']][$row['ceturksnis']] = array_merge($quarters_ligumi[$pardeveja_id][$row['gads']][$row['ceturksnis']], $statdata['kopa']['ligumu_id']);
          }

          //  Apmaksātās prēmijas

          $query = "
            SELECT
            r.*,
            r.id as rekina_id,
            g.id as liguma_id,
            g.nosaukums as klienta_nosaukums,
            r.pvn as rekina_pvn,
            r.izr_datums as rekina_izr_datums,
            r.kopsumma as rekina_kopsumma,
            p.pvn_likme
            FROM `".DB_PREF."rekini` r
            LEFT JOIN `".DB_PREF."ligumi` g ON (r.liguma_id = g.id)
            LEFT JOIN `".DB_PREF."pardeveji` p ON (p.id = g.pardeveja_id)
            WHERE
              g.pardeveja_id = ".esc($pardeveja_id)." AND
              r.atkapsanas_rekins = 0 AND
              r.nodots_piedz = 0 AND
              r.barteris = 0 AND
              r.anulets = 0 AND
              r.liguma_id IN (" . (!empty($prem_ligumu_id) ? implode(', ', $prem_ligumu_id) : '0') . ")";


          if(isset($_GET['date_from']) && $_GET['date_from']) {
            $query .= " AND r.apm_termins >= '" . esc(date('Y-m-d', strtotime($_GET['date_from']))) . "' ";
          }

          if(isset($_GET['date_to']) && $_GET['date_to']) {
            $query .= " AND r.apm_termins <= '" . esc(date('Y-m-d', strtotime($_GET['date_to']))) . "' ";
          }
          // by agris add spece before HAVING
          $query .= " HAVING
              r.apm_summa >= IFNULL(r.kopsumma, 0)
            ";

          $result = db_query($query);


          while($row = db_get_assoc($result)) {
            if(isset($premijas_izmaksatas_par_rekiniem[$row['id']])) {
              //  Jau ir izmaksāts
              $apm_prem[$pardeveja_id] += $premijas_izmaksatas_par_rekiniem[$row['id']];
            } else {
              $contract_premium = getContractPremium($row, $premiums_cache, $pardeveja_id);

              //$apm_summa_bez_pvn = bcdiv($row['kopsumma'], (1 + ($row['rekina_pvn'] / 100)), 2);
              $apm_summa_bez_pvn = $row['summa'];
              $prem_summa = round($apm_summa_bez_pvn * ($contract_premium / 100), 2);


              $apm_prem[$pardeveja_id] += $prem_summa;
            }
          }

          //  Aprēķinātās prēmijas

          $query = "
            SELECT
            r.*,
            g.id as liguma_id,
            g.nosaukums as klienta_nosaukums,
            r.pvn as rekina_pvn,
            r.izr_datums as rekina_izr_datums,
            r.kopsumma as rekina_kopsumma,
            r.apm_termins as rekina_apm_termins,
            p.pvn_likme
            FROM `".DB_PREF."rekini` r
            LEFT JOIN `".DB_PREF."ligumi` g ON (r.liguma_id = g.id)
            LEFT JOIN `".DB_PREF."pardeveji` p ON (p.id = g.pardeveja_id)
            WHERE
              g.pardeveja_id = ".esc($pardeveja_id)." AND
              r.atkapsanas_rekins = 0 AND
              r.nodots_piedz = 0 AND
              r.barteris = 0 AND
              r.anulets = 0 AND
              r.liguma_id IN (" . (!empty($prem_ligumu_id) ? implode(', ', $prem_ligumu_id) : '0') . ")
          ";

          if(isset($_GET['date_from']) && $_GET['date_from']) {
            $query .= " AND r.apm_termins >= '" . esc(date('Y-m-d', strtotime($_GET['date_from']))) . "' ";
          }

          if(isset($_GET['date_to']) && $_GET['date_to']) {
            $query .= " AND r.apm_termins <= '" . esc(date('Y-m-d', strtotime($_GET['date_to']))) . "' ";
          }




          $result = db_query($query);

          while($row = db_get_assoc($result)) {
            $contract_premium = getContractPremium($row, $premiums_cache, $pardeveja_id);

            //$apr_summa_bez_pvn = bcdiv($row['kopsumma'], (1 + ($row['rekina_pvn'] / 100)), 2);
            $apr_summa_bez_pvn = $row['summa'];
            $prem_summa = $apr_summa_bez_pvn * ($contract_premium / 100);

            $apr_prem[$pardeveja_id] += round($prem_summa, 2);
          }
        }

        cache_set($cache_prem_data_key, array(
          'apr' => $apr_prem[$pardeveja_id],
          'apm' => $apm_prem[$pardeveja_id],
          'izm' => $izm_prem[$pardeveja_id],
          'ligumi' => isset($quarters_ligumi[$pardeveja_id]) ? $quarters_ligumi[$pardeveja_id] : array(),
        ));

      } else {

        //  Got cache
        $apr_prem[$pardeveja_id] = $cached_prem_data['apr'];
        $apm_prem[$pardeveja_id] = $cached_prem_data['apm'];
        $izm_prem[$pardeveja_id] = $cached_prem_data['izm'];
        $quarters_ligumi[$pardeveja_id] = $cached_prem_data['ligumi'];

      }
    }

    // -----
		// Rēķini

    $sql = "
      SELECT
        r.*,
        r.apm_termins as rekina_apm_termins,
        g.pardeveja_id,
        (
          SELECT SUM(m.summa)
          FROM `".DB_PREF."rekini_maksajumi` m
          WHERE
            m.rekina_id = r.id AND
            (
              r.nodots_piedz_datums IS NULL OR
              r.nodots_piedz_datums > m.datums
            )
        ) as apm_summa_no_ka_rekinat_prov
      FROM `".DB_PREF."rekini` r
      LEFT JOIN `".DB_PREF."ligumi` g ON (r.liguma_id = g.id)
    ";

    $rek_where = $period_where;

    $rek_where[] = 'r.atkapsanas_rekins = 0';

    if ($pardeveju_ids) {
      $rek_where[] = "g.pardeveja_id IN (".implode(',', $pardeveju_ids).")";
    }

    if (!empty($rek_where)) {
      $sql .= "WHERE " . implode(" AND ", $rek_where);
    }

    $query = db_query($sql);
    $lig_rekini = array();

    while($row = db_get_assoc($query)){
      if(in_array($row['pardeveja_id'], $pardeveju_ids)) {
        $lig_rekini[$row['pardeveja_id']][] = $row;

        $ligumu_summa[$row['pardeveja_id']] += $row['summa'];

        $ligumu_apm_summa[$row['pardeveja_id']] += $row['apm_summa'];

        $katra_liguma_summa[$row['pardeveja_id']][$row['liguma_id']] += $row['kopsumma'];
        $summa_bez_pvn = $row['kopsumma'] / (1 + ($row['pvn'] / 100));
        $katra_liguma_summa_bez_pvn[$row['pardeveja_id']][$row['liguma_id']] += $summa_bez_pvn;

        $katra_liguma_apm_summa[$row['pardeveja_id']][$row['liguma_id']] += $row['apm_summa'];
        $apm_summa_bez_pvn = $row['apm_summa_no_ka_rekinat_prov'] / (1 + ($row['pvn'] / 100));
        $katra_liguma_apm_summa_bez_pvn[$row['pardeveja_id']][$row['liguma_id']] += $apm_summa_bez_pvn;

        //  Prēmija
        //$prem = getContractPremium($row, $premiums_cache, $row['pardeveja_id']);
        //$prem = 4;

        //$apr_prem[$row['pardeveja_id']] = bcadd($apr_prem[$row['pardeveja_id']], bcmul($summa_bez_pvn, bcdiv($prem, 100, 4), 2), 2);
        //$apm_prem[$row['pardeveja_id']] = bcadd($apm_prem[$row['pardeveja_id']], bcmul($apm_summa_bez_pvn, bcdiv($prem, 100, 4), 2), 2);;
      }
    }

    foreach($lig_rekini as $pardeveja_id => $rekini) {
      $ids = array();

      foreach((array)$rekini as $rekins) {
        $ids[] = $rekins['id'];
      }
    }

    foreach($katra_liguma_summa_bez_pvn as $pard_id => $lig) {

      $apr_prov[$pard_id] = 0;

      foreach($lig as $lig_id => $lig_kopsumma) {
        $apr_prov[$pard_id] += $lig_kopsumma * ($ligumi[$pard_id][$lig_id]['provizija_pardevejam'] / 100);
      }

    }

    foreach($katra_liguma_apm_summa_bez_pvn as $pard_id => $lig) {

      $apmaksajama_prov[$pard_id] = 0;

      foreach($lig as $lig_id => $apm_summa) {
        $apmaksajama_prov[$pard_id] += $apm_summa * ($ligumi[$pard_id][$lig_id]['provizija_pardevejam'] / 100);
      }

    }

    // -----
		// Pārdevēju rēķini

    $sql = "
      SELECT
        r.*,
        (
          SELECT SUM(summa)
          FROM `".DB_PREF."pardeveji_rekini_detalas`
          WHERE
            pardeveja_rekina_id = r.id AND
            rekina_id IS NOT NULL
        ) as prov_summa,
        (
          SELECT SUM(summa)
          FROM `".DB_PREF."pardeveji_rekini_detalas`
          WHERE
            pardeveja_rekina_id = r.id AND
            rekina_id IS NULL AND
            rekina_maksajuma_id IS NULL
        ) as piemaksu_summa
      FROM `".DB_PREF."pardeveji_rekini` r
    ";

    if ($pardeveju_ids) {
      $pard_rek_where[] = "r.pardeveja_id IN (".implode(',', $pardeveju_ids).")";
    }

    if (!empty($pard_rek_where)) {
      $sql .= "WHERE " . implode(" AND ", $pard_rek_where);
    }

    $query = db_query($sql);

    while($row = db_get_assoc($query)){

      if ($row['statuss'] == 2 && isset($izmaks_prov[$row['pardeveja_id']])) { // apmaksāts
        $izmaks_prov[$row['pardeveja_id']] += $row['prov_summa']; // rādam bez pvn
      }

      if(isset($apr_piem[$row['pardeveja_id']])) {
        $apr_piem[$row['pardeveja_id']] += $row['piemaksu_summa']; // rādam bez pvn
      }

      if ($row['statuss'] == 2 && isset($izmaks_piem[$row['pardeveja_id']])) { // apmaksāts
        $izmaks_piem[$row['pardeveja_id']] += $row['piemaksu_summa']; // rādam bez pvn
      }

    }

    // -----
    $sql = "
      SELECT
        m.*,
        r.pvn as rekina_pvn,
        r.kopsumma,
        d.id as detalas_id,
        g.pardeveja_id,
        g.provizija_pardevejam,
        (
          SELECT SUM(d2.no_summas)
          FROM `".DB_PREF."pardeveji_rekini_detalas` d2
          LEFT JOIN `".DB_PREF."pardeveji_rekini` pr2 ON (pr2.id = d2.pardeveja_rekina_id)
          WHERE
            d2.rekina_id = m.rekina_id AND
            d2.rekina_maksajuma_id IS NOT NULL AND
            pr2.statuss = 2
        ) as prov_izmaksu_summa
      FROM `".DB_PREF."rekini_maksajumi` m
      LEFT JOIN `".DB_PREF."rekini` r ON (m.rekina_id = r.id)
      LEFT JOIN `".DB_PREF."ligumi` g ON (r.liguma_id = g.id)
      LEFT JOIN `".DB_PREF."pardeveji` p ON (p.id = g.pardeveja_id)
      LEFT JOIN `".DB_PREF."pardeveji_rekini_detalas` d ON (m.id = d.rekina_maksajuma_id)
      LEFT JOIN `".DB_PREF."pardeveji_rekini` pr ON (pr.id = d.pardeveja_rekina_id)
    ";

    $uz_izmaksu_where = $period_where;

    if ($pardeveju_ids) {
      $uz_izmaksu_where[] = "g.pardeveja_id IN (".implode(',', $pardeveju_ids).")";
    }

    $uz_izmaksu_where[] = "
      (
        d.id IS NULL OR
        pr.statuss = 1
      ) AND
      r.atkapsanas_rekins = 0 AND
      (
        r.nodots_piedz_datums IS NULL OR
        r.nodots_piedz_datums > m.datums
      )
    ";

    if (!empty($uz_izmaksu_where)) {
      $sql .= "WHERE " . implode(" AND ", $uz_izmaksu_where);
    }

    $sql .= "
      HAVING m.summa <= IFNULL(r.kopsumma, 0) - IFNULL(prov_izmaksu_summa, 0)
      ORDER BY m.datums DESC, m.id DESC
    ";

    $query = db_query($sql);

    while($row = db_get_assoc($query)) {

      $apm_summa_bez_pvn = $row['summa'] / (1 + ($row['rekina_pvn'] / 100));
      $prov_summa = $apm_summa_bez_pvn * ($row['provizija_pardevejam'] / 100);


      if(isset($uz_izmaksu_prov[$row['pardeveja_id']])) {
        $uz_izmaksu_prov[$row['pardeveja_id']] += $prov_summa;
      }

    }

    // -----

    $inid = -1;
    ?>

    <?php foreach($data as $row) { ?>

      <?
      $inid ++;
      ?>

      <tr id="tr<?=$inid;?>" title="<?=$row['vards'];?>">

        <td><a href="?c=darbinieki&a=labot&id=<?=$row['id'];?>"><b><?=$row['vards'];?></b></a></td>

        <? if (!empty($ligumi[$row['id']])) { ?>

          <? $lig_skaits = count($ligumi[$row['id']]) ?>

          <?
            $rek_apm_proc = 0;

            if (!empty($apmaksajama_prov[$row['id']]) || !empty($apr_piem[$row['id']])) {
              $rek_apm_proc = round(round($izmaks_prov[$row['id']] + $izmaks_piem[$row['id']], 2) * 100 / round($apmaksajama_prov[$row['id']] + $apr_piem[$row['id']], 2), 1);
            }

            //$deb_kred = round(($apmaksajama_prov[$row['id']] + $apr_piem[$row['id']]) - ($izmaks_prov[$row['id']] + $izmaks_piem[$row['id']]), 2);
            $deb_kred = round($uz_izmaksu_prov[$row['id']] + $apr_piem[$row['id']] - $izmaks_piem[$row['id']] - $row['izmaksats_avansaa_bez_pvn'], 2);

            //  Pieskaitam prēmijas bilancei
            $deb_kred += ($apm_prem[$row['id']] - $izm_prem[$row['id']]);
          ?>
           <?php if(check_access('darbinieki-k-ligumi')) {?>
            <td class="c"><?= $lig_skaits ?></td>
            <td class="r"><?= format_currency($ligumu_summa[$row['id']]) ?></td>
            <td class="r"><?= format_currency($ligumu_summa[$row['id']] / $lig_skaits) ?></td>
            <td class="r"><?= format_currency($ligumu_apm_summa[$row['id']]) ?></td>
           <?php } ?>

           <?php if(check_access('darbinieki-k-statistika')) {?>
            <td class="r"><?= $row['quarter_stats'][$stats_quarter]['kopa']['skaits'] ?></td>
            <td class="r"><?= format_currency($row['quarter_stats'][$stats_quarter]['kopa']['apgr']) ?></td>
            <td class="r"><?= round($row['quarter_stats'][$stats_quarter]['kopa']['proc_atdot_sk'], 1) . '%' ?></td>
            <td class="r"><?= round($row['quarter_stats'][$stats_quarter]['kopa']['proc_atdot_apgr'], 1) . '%' ?></td>
           <?php } ?>

           <?php if(check_access('darbinieki-k-provizijas')) {?>
            <td class="r"><?= round($prov_proc_summa[$row['id']] / $lig_skaits, 1) . '%' ?></td>
            <td class="r"><?= format_currency($apr_prov[$row['id']]) ?></td>
            <td class="r"><?= format_currency($apr_prov[$row['id']] / $lig_skaits) ?></td>
            <td class="r"><?= format_currency($apmaksajama_prov[$row['id']]) ?></td>
            <td class="r"><?= !empty($apr_prov[$row['id']]) ? round(round($apmaksajama_prov[$row['id']], 2) * 100 / round($apr_prov[$row['id']], 2), 1) : 0 ?>%</td>
            <td class="r"><?= format_currency($izmaks_prov[$row['id']]) ?></td>
           <?php } ?>

           <?php if(check_access('darbinieki-k-premijas')) {?>
            <td class="r"><?= format_currency($apr_prem[$row['id']]) ?></td>
            <td class="r"><?= format_currency($apm_prem[$row['id']]) ?></td>
            <td class="r"><?= format_currency($izm_prem[$row['id']]) ?></td>
           <?php } ?>

           <?php if(check_access('darbinieki-k-piemaksas')) {?>
            <td class="r"><?= format_currency($apr_piem[$row['id']]) ?></td>
            <td class="r"><?= format_currency($izmaks_piem[$row['id']]) ?></td>
           <?php } ?>

          <?php if(check_access('darbinieki-k-bilance')) { ?>
            <td class="r"><?= $rek_apm_proc ?>%</td>
            <td class="r"><strong <?= $deb_kred < 0 ? 'style="color: red;"' : '' ?>><?= format_currency($deb_kred) ?></strong></td>
          <?php } ?>

          <?php if(check_access('darbinieki-k-atgadinajumi')) {?>
            <td class="c"><a href="?c=darbinieki&a=labot&id=<?=$row['id'];?>&subtab=atgadinajumi"><?= !empty($row['aktivi_atgadinajumi']) ? 'Ir ('.$row['aktivi_atgadinajumi'].')' : 'Nav' ?></a></td>
          <?php } ?>

          <td style="display: none">
          <script>
            searchlist[<?=$inid;?>] = '<?=addslashes(mb_strtoupper($row['vards']));?>';
            sp['s_pardevejs'][<?=$inid;?>] = <?= $row['id'] ?>;
            sp['s_ligumu_sk'][<?=$inid;?>] = <?= $lig_skaits ?>;
            sp['s_lig_kopsumma'][<?=$inid;?>] = <?= round($ligumu_summa[$row['id']], 2) ?>;
            sp['s_lig_vid_summa'][<?=$inid;?>] = <?= round($ligumu_summa[$row['id']] / $lig_skaits, 2) ?>;
            sp['s_apm_lig_kopsumma'][<?=$inid;?>] = <?= round($ligumu_apm_summa[$row['id']], 2) ?>;

            sp['s_stat_sk'][<?=$inid;?>] = <?= intval($row['quarter_stats'][$stats_quarter]['kopa']['skaits']) ?>;
            sp['s_stat_summa'][<?=$inid;?>] = <?= round($row['quarter_stats'][$stats_quarter]['kopa']['apgr'], 2) ?>;
            sp['s_stat_proc_sk'][<?=$inid;?>] = <?= round($row['quarter_stats'][$stats_quarter]['kopa']['proc_atdot_sk'], 1) ?>;
            sp['s_stat_proc_summa'][<?=$inid;?>] = <?= round($row['quarter_stats'][$stats_quarter]['kopa']['proc_atdot_apgr'], 1) ?>;

            sp['s_stat_atdoti_sk'][<?=$inid;?>] = <?= intval($row['quarter_stats'][$stats_quarter]['atdoti']['skaits']) ?>;
            sp['s_stat_atdoti_apgr'][<?=$inid;?>] = <?= round($row['quarter_stats'][$stats_quarter]['atdoti']['apgr'], 2) ?>;

            sp['s_vid_prov_proc'][<?=$inid;?>] = <?= round($prov_proc_summa[$row['id']] / $lig_skaits, 1) ?>;
            sp['s_apr_prov'][<?=$inid;?>] = <?= round($apr_prov[$row['id']], 2) ?>;
            sp['s_vid_apr_prov'][<?=$inid;?>] = <?= round($apr_prov[$row['id']] / $lig_skaits, 2) ?>;
            sp['s_apmaksajama_prov'][<?=$inid;?>] = <?= round($apmaksajama_prov[$row['id']], 2) ?>;
            sp['s_apmaksajama_prov_percent'][<?=$inid;?>] = <?= !empty($apr_prov[$row['id']]) ? round($apmaksajama_prov[$row['id']] * 100 / $apr_prov[$row['id']], 2) : 0 ?>;
            sp['s_izm_prov'][<?=$inid;?>] = <?= round($izmaks_prov[$row['id']], 2) ?>;

            sp['s_apr_prem'][<?=$inid;?>] = <?= round($apr_prem[$row['id']], 2) ?>;
            sp['s_apm_prem'][<?=$inid;?>] = <?= round($apm_prem[$row['id']], 2) ?>;
            sp['s_izm_prem'][<?=$inid;?>] = <?= round($izm_prem[$row['id']], 2) ?>;

            sp['s_apr_piem'][<?=$inid;?>] = <?= round($apr_piem[$row['id']], 2) ?>;
            sp['s_izm_piem'][<?=$inid;?>] = <?= round($izmaks_piem[$row['id']], 2) ?>;

            sp['s_rek_apm_percent'][<?=$inid;?>] = <?= $rek_apm_proc ?>;
            sp['s_dept_cred'][<?=$inid;?>] = <?= $deb_kred ?>;
            sp['s_akt_atg'][<?=$inid;?>] = '<?=!empty($row['aktivi_atgadinajumi']) ? '1' : '2';?>';
          </script>
          </td>

        <? } else { ?>

          <?php if(check_access('darbinieki-k-ligumi')) {?>
            <td class="c">0</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          <?php } ?>

          <?php if(check_access('darbinieki-k-statistika')) {?>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          <?php } ?>

          <?php if(check_access('darbinieki-k-provizijas')) {?>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          <?php } ?>

          <?php if(check_access('darbinieki-k-premijas')) {?>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          <?php } ?>

          <?php if(check_access('darbinieki-k-piemaksas')) {?>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          <?php } ?>

          <?php if(check_access('darbinieki-k-bilance')) { ?>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          <? } ?>

          <?php if(check_access('darbinieki-k-atgadinajumi')) {?>
            <td class="c"><a href="?c=darbinieki&a=labot&id=<?=$row['id'];?>&subtab=atgadinajumi"><?= !empty($row['aktivi_atgadinajumi']) ? 'Ir ('.$row['aktivi_atgadinajumi'].')' : 'Nav' ?></a></td>
          <? } ?>


          <td style="display: none;">
          <script>
            searchlist[<?=$inid;?>] = '<?=addslashes(mb_strtoupper($row['vards']));?>';
            sp['s_pardevejs'][<?=$inid;?>] = <?= $row['id'] ?>;
            sp['s_ligumu_sk'][<?=$inid;?>] = 0;
            sp['s_lig_kopsumma'][<?=$inid;?>] = 0;
            sp['s_lig_vid_summa'][<?=$inid;?>] = 0;
            sp['s_apm_lig_kopsumma'][<?=$inid;?>] = 0;

            sp['s_stat_sk'][<?=$inid;?>] = 0;
            sp['s_stat_summa'][<?=$inid;?>] = 0;
            sp['s_stat_proc_sk'][<?=$inid;?>] = 0;
            sp['s_stat_proc_summa'][<?=$inid;?>] = 0;

            sp['s_stat_atdoti_sk'][<?=$inid;?>] = 0;
            sp['s_stat_atdoti_apgr'][<?=$inid;?>] = 0;

            sp['s_vid_prov_proc'][<?=$inid;?>] = 0;
            sp['s_apr_prov'][<?=$inid;?>] = 0;
            sp['s_vid_apr_prov'][<?=$inid;?>] = 0;
            sp['s_apmaksajama_prov'][<?=$inid;?>] = 0;
            sp['s_apmaksajama_prov_percent'][<?=$inid;?>] = 0;
            sp['s_izm_prov'][<?=$inid;?>] = 0;

            sp['s_apr_prem'][<?=$inid;?>] = 0;
            sp['s_apm_prem'][<?=$inid;?>] = 0;
            sp['s_izm_prem'][<?=$inid;?>] = 0;

            sp['s_apr_piem'][<?=$inid;?>] = 0;
            sp['s_izm_piem'][<?=$inid;?>] = 0;

            sp['s_rek_apm_percent'][<?=$inid;?>] = 0;
            sp['s_dept_cred'][<?=$inid;?>] = 0;
            sp['s_akt_atg'][<?=$inid;?>] = '<?=!empty($row['aktivi_atgadinajumi']) ? '1' : '2';?>';
          </script>
          </td>

        <? } ?>

      </tr>

    <?php } ?>


  </tbody>

  <? if (check_access('darbinieki-admin')) { ?>

  <tr>

    <?php if(check_access('darbinieki-k-ligumi') || check_access('darbinieki-k-statistika') || check_access('darbinieki-k-provizijas') || check_access('darbinieki-k-premijas') || check_access('darbinieki-k-piemaksas') || check_access('darbinieki-k-bilance')) { ?>
      <td align="right"><strong>Kopā:</strong></td>
    <?php } else { ?>
      <td align="right"></td>
    <?php }?>

    <?php if(check_access('darbinieki-k-ligumi')) {?>
      <td class="c" id="ligskaits">0</td>
      <td class="r" id="ligkopsumma">0</td>
      <td></td>
      <td class="r" id="apmligsumma">0</td>
    <?php } ?>

    <?php if(check_access('darbinieki-k-statistika')) {?>
      <td class="c" id="statskaits"></td>
      <td class="r" id="statkopsumma"></td>
      <td class="r" id="statprocnoatdotisk"></td>
      <td class="r" id="statprocnoatdotiapgr"></td>
    <?php } ?>

    <?php if(check_access('darbinieki-k-provizijas')) {?>
      <td></td>
      <td class="r" id="aprprovsumma">0</td>
      <td></td>
      <td class="r" id="apmaksajamaprovsumma">0</td>
      <td></td>
      <td class="r" id="izmprovsumma">0</td>
    <?php } ?>

    <?php if(check_access('darbinieki-k-premijas')) {?>
      <td class="r" id="aprpremsumma">0</td>
      <td class="r" id="apmpremsumma">0</td>
      <td class="r" id="izmpremsumma">0</td>
    <?php } ?>

    <?php if(check_access('darbinieki-k-piemaksas')) {?>
      <td class="r" id="aprpiemsumma">0</td>
      <td class="r" id="izmpiemsumma">0</td>
    <?php } ?>

    <?php if(check_access('darbinieki-k-bilance')) { ?>
      <td></td>
      <td class="r" style="font-weight: bold;" id="deptkredsumma">0</td>
    <?php } ?>

    <?php if(check_access('darbinieki-k-atgadinajumi')) {?>
      <td></td>
    <?php } ?>

  </tr>

  <tr>

    <?php if(check_access('darbinieki-k-ligumi') || check_access('darbinieki-k-statistika') || check_access('darbinieki-k-provizijas') || check_access('darbinieki-k-premijas') || check_access('darbinieki-k-piemaksas') || check_access('darbinieki-k-bilance')) { ?>
      <td align="right"><strong>Vidēji:</strong></td>
    <?php } else { ?>
      <td align="right"></td>
    <?php }?>

    <?php if(check_access('darbinieki-k-ligumi')) {?>
      <td class="c" id="ligskaitsvid">0</td>
      <td></td>
      <td class="r" id="ligvidsummavid">0</td>
      <td></td>
    <?php } ?>

    <?php if(check_access('darbinieki-k-statistika')) {?>
      <td class="c" id="statskaitsvid"></td>
      <td></td>
      <td></td>
      <td></td>
    <?php } ?>

    <?php if(check_access('darbinieki-k-provizijas')) {?>
      <td class="r" id="provpercentvid">0</td>
      <td></td>
      <td class="r" id="vidaprprovvid">0</td>
      <td></td>
      <td class="r" id="apmprovpercentvid">0</td>
      <td class="r" id="izmprovvid">0</td>
    <?php } ?>

    <?php if(check_access('darbinieki-k-premijas')) {?>
      <td></td>
      <td></td>
      <td></td>
    <?php } ?>

    <?php if(check_access('darbinieki-k-piemaksas')) {?>
      <td></td>
      <td class="r" id="izmpiemvid"></td>
    <?php } ?>

    <?php if(check_access('darbinieki-k-bilance')) { ?>
      <td class="r" id="rekapmpercentvid">0</td>
      <td></td>
    <?php } ?>

    <?php if(check_access('darbinieki-k-atgadinajumi')) {?>
      <td></td>
    <?php } ?>

  </tr>

  <? } ?>

</table>

<script type="text/javascript">
$(document).ready(function() {

  var table = $("#darbinieki_kopsavilkums");
  var head = $("#darbinieki_kopsavilkums_filter");
  var groups = $("#darbinieki_kopsavilkums_groups");

  <? if (check_access('darbinieki-admin')) { ?>

    var cache_key = getCacheKeyFromClass(table);

    var sortListFromCache = cache_key ? $.jStorage.get(cache_key + '_sort', []) : [];

    table.tablesorter({
      debug: false,
      highlightClass: 'highlight',
      sortList: sortListFromCache,
      headers: {
        1: { sorter:'digit' },
        2: { sorter:'currency' },
        3: { sorter:'currency' },
        4: { sorter:'currency' },
        5: { sorter:'digit' },
        6: { sorter:'currency' },
        7: { sorter:'digit' },
        8: { sorter:'digit' },
        9: { sorter:'digit' },
        10: { sorter:'currency' },
        11: { sorter:'currency' },
        12: { sorter:'currency' },
        13: { sorter:'digit' },
        14: { sorter:'currency' },
        15: { sorter:'currency' },
        16: { sorter:'currency' },
        17: { sorter:'digit' },
        18: { sorter:'currency' }
      }
    }).bind('sortEnd', function() {

      if (cache_key) $.jStorage.set(cache_key + '_sort', table.get(0).config.sortList);

      stripeTable(table);

      initTableGroupColors(table);

    });

    initTableFilter(table, head, processTable, '', searchlist, sp, groups);

  <? } else { ?>

    stripeTable(table);

    $('thead', table).prepend('<tr class="groups">'+groups.html()+'</tr>');

    groups.remove();

  <? } ?>

  initTableCols(table, 'darb_kopsavilkums', <?= Zend_Json::encode(get_tab_kolonnas('darb_kopsavilkums')) ?>);

  initTableGroupColors(table);

});

function processTable() {

  var pardskaits = 0;
  var ligumuskaits = 0;
  var ligkopsumma = 0;
  var ligvidsumma = 0;
  var apmligsumma = 0;

  var statskaits = 0;
  var statkopsumma = 0;

  var statsatdotiskaits = 0;
  var statsatdotiapgr = 0;

  var provpercentsumma = 0;
  var aprprovsumma = 0;
  var vidaprprovsumma = 0;
  var apmaksajamaprovsumma = 0;
  var apmprovpercentsumma = 0;
  var izmprovsumma = 0;

  var aprpremsumma = 0;
  var apmpremsumma = 0;
  var izmpremsumma = 0;

  var aprpiemsumma = 0;
  var izmpiemsumma = 0;

  var rekapmpercentsumma = 0;
  var deptkredsumma = 0;

  jQuery.each(searchlist, function(k,v){

    var row = $('#tr' + k);

    if (row.is(':visible') && sp['s_ligumu_sk'][k] > 0) {

      pardskaits ++;

      if(sp['s_ligumu_sk'][k] > 0) ligumuskaits += sp['s_ligumu_sk'][k];
      if(sp['s_lig_kopsumma'][k] > 0) ligkopsumma += sp['s_lig_kopsumma'][k];
      if(sp['s_lig_vid_summa'][k] > 0) ligvidsumma += sp['s_lig_vid_summa'][k];
      if(sp['s_apm_lig_kopsumma'][k] > 0) apmligsumma += sp['s_apm_lig_kopsumma'][k];

      if(sp['s_stat_sk'][k] > 0) statskaits += sp['s_stat_sk'][k];
      if(sp['s_stat_summa'][k] > 0) statkopsumma += sp['s_stat_summa'][k];

      if(sp['s_stat_atdoti_sk'][k] > 0) statsatdotiskaits += sp['s_stat_atdoti_sk'][k];
      if(sp['s_stat_atdoti_apgr'][k] > 0) statsatdotiapgr += sp['s_stat_atdoti_apgr'][k];

      if(sp['s_vid_prov_proc'][k] > 0) provpercentsumma += sp['s_vid_prov_proc'][k];
      if(sp['s_apr_prov'][k] > 0) aprprovsumma += sp['s_apr_prov'][k];
      if(sp['s_vid_apr_prov'][k] > 0) vidaprprovsumma += sp['s_vid_apr_prov'][k];
      if(sp['s_apmaksajama_prov'][k] > 0) apmaksajamaprovsumma += sp['s_apmaksajama_prov'][k];
      if(sp['s_apmaksajama_prov_percent'][k] > 0) apmprovpercentsumma += sp['s_apmaksajama_prov_percent'][k];
      if(sp['s_izm_prov'][k] > 0) izmprovsumma += sp['s_izm_prov'][k];

      if(sp['s_apr_prem'][k] > 0) aprpremsumma += sp['s_apr_prem'][k];
      if(sp['s_apm_prem'][k] > 0) apmpremsumma += sp['s_apm_prem'][k];
      if(sp['s_izm_prem'][k] > 0) izmpremsumma += sp['s_izm_prem'][k];

      if(sp['s_apr_piem'][k] > 0) aprpiemsumma += sp['s_apr_piem'][k];
      if(sp['s_izm_piem'][k] > 0) izmpiemsumma += sp['s_izm_piem'][k];

      if(sp['s_rek_apm_percent'][k] > 0) rekapmpercentsumma += sp['s_rek_apm_percent'][k];

      deptkredsumma += sp['s_dept_cred'][k];

    }

  });

  $('#ligskaits').html(ligumuskaits);
  $('#ligskaitsvid').html(Math.round(ligumuskaits / pardskaits * 10) / 10);
  $('#ligkopsumma').html('EUR&nbsp;' + ligkopsumma.toFixed(2));
  $('#ligvidsummavid').html('EUR&nbsp;' + (ligvidsumma / pardskaits).toFixed(2));
  $('#apmligsumma').html('EUR&nbsp;' + apmligsumma.toFixed(2));

  $('#statskaits').html(statskaits);
  $('#statskaitsvid').html(Math.round(statskaits / pardskaits * 10) / 10);
  $('#statkopsumma').html('EUR&nbsp;' + statkopsumma.toFixed(2));

  $('#statprocnoatdotisk').html((Math.round((statskaits / statsatdotiskaits) * 100 * 10) / 10) + '%');
  $('#statprocnoatdotiapgr').html((Math.round((statkopsumma / statsatdotiapgr) * 100 * 10) / 10) + '%');

  //((Math.round(apmprovpercentsumma / pardskaits * 20) / 20) + '%')

  $('#provpercentvid').html((Math.round(provpercentsumma / pardskaits * 20) / 20) + '%');
  $('#aprprovsumma').html('EUR&nbsp;' + aprprovsumma.toFixed(2));
  $('#vidaprprovvid').html('EUR&nbsp;' + (vidaprprovsumma / pardskaits).toFixed(2));
  $('#apmaksajamaprovsumma').html('EUR&nbsp;' + apmaksajamaprovsumma.toFixed(2));
  $('#apmprovpercentvid').html((Math.round(apmprovpercentsumma / pardskaits * 20) / 20) + '%');
  $('#izmprovsumma').html('EUR&nbsp;' + izmprovsumma.toFixed(2));

  $('#aprpremsumma').html('EUR&nbsp;' + aprpremsumma.toFixed(2));
  $('#apmpremsumma').html('EUR&nbsp;' + apmpremsumma.toFixed(2));
  $('#izmpremsumma').html('EUR&nbsp;' + izmpremsumma.toFixed(2));

  $('#aprpiemsumma').html('EUR&nbsp;' + aprpiemsumma.toFixed(2));
  $('#izmpiemsumma').html('EUR&nbsp;' + izmpiemsumma.toFixed(2));

  $('#izmprovvid').html('EUR&nbsp;' + (izmprovsumma / pardskaits).toFixed(2));
  $('#izmpiemvid').html('EUR&nbsp;' + (izmpiemsumma / pardskaits).toFixed(2));

  $('#rekapmpercentvid').html((Math.round(rekapmpercentsumma / pardskaits * 20) / 20) + '%');
  $('#deptkredsumma').html('EUR&nbsp;' + deptkredsumma.toFixed(2));

  initTableGroupColors($("#darbinieki_kopsavilkums"));

}

$("#date_from").datepicker({dateFormat:'dd.mm.yy'});
$("#date_to").datepicker({dateFormat:'dd.mm.yy'});

$("form#period_filter").each(function() {

  var form = $(this);

  $('input#date_from, input#date_to, select#stats_quarter, input#inactive', form).change(function() {
    form.submit();
  });

  $('a.clear_form', form).click(function() {
    $('input#date_from, input#date_to', form).val('');
    form.submit();
  });

});

</script>

<? if (check_access('darbinieki-adminu-saraksts') && !$agenturas) { ?>

  <script>
  var a_searchlist = [];

  var a_sp = create_filter_field_array([
    's_pardevejs',
    's_akt_atg'
  ]);
  </script>

  <? // Prototypes // ?>
  <div style="display:none;">

    <div id="komentarsprot__admin_kopsavilkums">
      <table class="comment_form">
        <tr>
          <td width="70">Komentārs:</td>
          <td>
            <textarea name="a_komentari_new[_admin_kopsavilkums][komentars][]" rows="2" cols="30"></textarea>
          </td>
        </tr>
      </table>
    </div>

  </div>

  <table style="display:none;">

  <tr id="administratori_groups">
    <th></th>

    <?php if(check_access('darbinieki-k-atgadinajumi')) {?>
      <th>Atg.</th>
    <?php } ?>
  </tr>

  <tr id="administratori_filter">

    <th>
      <select class="search_field advonchange" searchclass="s_pardevejs" meth="int">
        <option value=""></option>
        <?php foreach($_vars['sys_pardeveji_admin_only'] as $k=>$p){ ?>
          <option value="<?=$k;?>"><?=$p;?></option>
        <?php } ?>
      </select>
      <a class="search_field" href="#">x</a>
    </th>


    <?php if(check_access('darbinieki-k-atgadinajumi')) {?>
      <th>
        <select class="search_field advonchange" searchclass="s_akt_atg" meth="int">
          <option value=""></option>
          <option value="1">Ir</option>
          <option value="2">Nav</option>
        </select>
        <a class="search_field" href="#">x</a>
      </th>
    <?php } ?>

  </tr>

</table>

  <table cellpadding="3" cellspacing="0" id="administratori" class="data ckey_administratori_list">

  <thead>

    <tr class="header">
      <th width="150" class="header" colname="darbinieks">Administrators</th>

      <?php if(check_access('darbinieki-k-atgadinajumi')) {?>
        <th width="40" class="header" colname="akt_atgad">Aktīvie<br />atgādinājumi</th>
      <?php } ?>
      <th></th>
    </tr>

  </thead>
  <tbody class="main">

    <?php

    $pardeveju_ids = array();

    $sql = "
      SELECT
        p.*,
        (
          SELECT COUNT(*)
          FROM `".DB_PREF."atgadinajumi` sa
          USE INDEX (pardeveja_id_index)
          LEFT JOIN `".DB_PREF."ligumi` sg ON (sa.liguma_id = sg.id)
          WHERE
            (
              sa.pardeveja_id = p.id OR
              sg.pardeveja_id = p.id OR
              sg.id IN (
                SELECT liguma_id
                FROM `".DB_PREF."ligumi_saistitie_pardeveji`
                WHERE pardeveja_id = p.id
              )
            ) AND
            sa.statuss = 1 AND
            sa.datums <= CURDATE() AND
            sa.redzamiba IN ('kopejs', ".(is_admin() ? "'admin'" : "'pardevejs'").")
        ) as aktivi_atgadinajumi
      FROM `".DB_PREF."pardeveji` p
    ";

		$where = array();

		$where[] = 'p.tips = "admin"';

		if (empty($_GET['inactive']))
			$where[] = 'p.aktivs = 1';

		if (!empty($where))
			$sql .= " WHERE " . implode(' AND ', $where);

    $sql .= "
      ORDER BY p.`vards` ASC
    ";

    $query = db_query($sql);

    $data = array();

    while($row = db_get_assoc($query)) {
      $data[] = $row;

      $pardeveju_ids[] = $row['id'];
    }

    $inid = -1;
    ?>

    <?php foreach($data as $row) { ?>

      <?
      $inid ++;
      ?>

      <tr id="tr<?=$inid;?>" title="<?=$row['vards'];?>">

        <td><a href="?c=darbinieki&a=labot&id=<?=$row['id'];?>"><b><?=$row['vards'];?></b></a></td>

        <?php if(check_access('darbinieki-k-atgadinajumi')) {?>
          <td class="c"><a href="?c=darbinieki&a=labot&id=<?=$row['id'];?>&subtab=atgadinajumi"><?= !empty($row['aktivi_atgadinajumi']) ? 'Ir ('.$row['aktivi_atgadinajumi'].')' : 'Nav' ?></a></td>
        <? } ?>

        <td></td>


        <td style="display: none;">
        <script>
          a_searchlist[<?=$inid;?>] = '<?=addslashes(mb_strtoupper($row['vards']));?>';
          a_sp['s_pardevejs'][<?=$inid;?>] = <?= $row['id'] ?>;
          a_sp['s_akt_atg'][<?=$inid;?>] = '<?=!empty($row['aktivi_atgadinajumi']) ? '1' : '2';?>';
        </script>
        </td>
      </tr>

    <?php } ?>


  </tbody>
  </table>
  <div style="clear: both;"></div>

</script>

  <script>
  stripeTable($('#administratori'));

  $(document).ready(function(){
    var dwidth = $("#darbinieki_kopsavilkums").outerWidth();
    $("#administratori").width(dwidth + 182);
  })

  </script>

  <form action="?c=darbinieki" id="fullformplace" method="post" enctype="multipart/form-data">

    <? show_comments_block('_darb_kopsavilkums', null, true) ?>

  </form>

<? } ?>

<? if (!is_admin()) { ?>

  <form id="liguma_check_form" method="" action="">

    Meklēt klientu: <input class="nosaukums" type="text" />

    <input type="submit" value="Meklēt" />

    <div class="result">


    </div>

  </form>

  <script>
  $('form#liguma_check_form').submit(function() {

     var loading_html = '<div class="loading" style="text-align: center;"><img alt="Notiek ielāde..." src="css/loading.gif" /></div>';

     var holder = $('div.result', this);

     holder.html(loading_html);

     $.get('ajax.php?action=check_client', {nosauk: $('.nosaukums', this).val()}, function(result) {

       holder.html(result);

     })

     return false;

  });
  </script>

<? } ?>