<?php

if (!check_access('darbinieki-jauns') && empty($_GET['id'])) {
  die('Jums nav pieejas tiesību jauna darbinieka izveidošanai.');
}

if (!check_access('darbinieki-admin') && ($_GET['id'] != $_SESSION['user']['id'])) {
  die('Jums nav pieejas tiesību šī darbinieka datiem.');
}


if (!empty($_GET['id'])) {
  $pardeveja_id = (int) $_GET['id'];

  $pardevejs = Pardevejs::model()->findByPk($pardeveja_id);

  $query = db_query("SELECT * FROM `".DB_PREF."pardeveji` WHERE `id` = " . $pardeveja_id);
  $d = db_get_assoc($query);

  $d = htmlesc($d);
} else {
  $pardevejs = new Pardevejs;
}

if(is_bookkeeper()) {  // Default tab for bookkeepers is billing
  $subtab = $pardeveja_id == $_SESSION['user']['id'] ? 'atgadinajumi' : 'norekini';
} elseif(is_minimal()) { // .. rekvizīti minimālajam adminam
  $subtab = $pardeveja_id == $_SESSION['user']['id'] ? 'atgadinajumi' : 'rekviziti';
} else {               // .. contracts for other admins
  /*if(isset($d['vaditajs']) && $d['vaditajs']) {
    $subtab = 'bonuss';
  } else {
    $subtab = 'ligumi';
  }*/
  if($pardevejs->isPardVad()) {
    $subtab = 'bonuss';
  } else {
    $subtab = 'ligumi';
  }
}

if (isset($_GET['subtab']) && in_array($_GET['subtab'], array('ligumi', 'saistitie_ligumi', 'rekviziti', 'statistika', 'norekini', 'maksajumi', 'atgadinajumi', 'izpilde', 'bonuss', 'tiesibas'))) {
  $subtab = $_GET['subtab'];
}

if (empty($pardeveja_id)) {
  $subtab = 'rekviziti';
}

require("new_process_".$subtab.".php");

$d = array();
$epasti = array();
$telefoni = array();
$pard_rekini = array();
$saistitie_ligumi = array();
$saistito_lig_rekini = array();

$akt_atg_skaits = 0;

if(!empty($pardeveja_id)){

  $query = db_query("SELECT * FROM `".DB_PREF."pardeveji` WHERE `id` = " . $pardeveja_id);
  $d = db_get_assoc($query);

  $d = htmlesc($d);

  if ($subtab == 'rekviziti') {

    $query = db_query("SELECT * FROM `".DB_PREF."pardeveji_epasti` WHERE `pardeveja_id` = " . $pardeveja_id . " ORDER BY id ASC");
    $epasti = array();

    while($row = db_get_assoc($query)){
      $epasti[] = htmlesc($row);
    }

    $query = db_query("SELECT * FROM `".DB_PREF."pardeveji_telefoni` WHERE `pardeveja_id` = " . $pardeveja_id . " ORDER BY id ASC");
    $telefoni = array();

    while($row = db_get_assoc($query)){
      $telefoni[] = htmlesc($row);
    }

  }

  if ($subtab == 'saistitie_ligumi') {

    $query = db_query("
      SELECT
        g.*,
        b.beigu_statuss,
        b.atlikt_lidz as beigu_statuss_atlikt_lidz,
        SUM(r.summa) as rekinu_summa,
        SUM(r.kopsumma) as rekinu_kopsumma,
        SUM(r.apm_summa) as apm_kopsumma,
        (
          SELECT COUNT(*)
          FROM `".DB_PREF."atgadinajumi`
          WHERE
            liguma_id = g.id AND
            statuss = 1 AND
            datums <= CURDATE() AND
            redzamiba IN ('kopejs', ".(is_admin() ? "'admin'" : "'pardevejs'").")
        ) as aktivi_atgadinajumi,
        p.vards as pardeveja_vards,
		s.piesaist_datums
      FROM `".DB_PREF."ligumi` g
      LEFT JOIN `".DB_PREF."ligumi_beigusies` b ON (g.id = b.liguma_id)
      LEFT JOIN `".DB_PREF."pardeveji` p ON (p.id = g.pardeveja_id)
      LEFT JOIN `".DB_PREF."rekini` r ON (
        r.liguma_id = g.id AND
        r.atkapsanas_rekins = 0
      )
	  LEFT JOIN `".DB_PREF."ligumi_saistitie_pardeveji` s ON s.liguma_id = g.id
      WHERE g.id IN (
        SELECT liguma_id
        FROM `".DB_PREF."ligumi_saistitie_pardeveji`
        WHERE pardeveja_id = ".$pardeveja_id."
      )
      GROUP BY g.id
      ORDER BY s.piesaist_datums DESC, IF(g.`sanemsanas_datums` IS NULL, 0, 1) ASC, g.`sanemsanas_datums` DESC, g.ligumadat DESC
    ");
    $saistitie_ligumi = array();

    while($row = db_get_assoc($query)){
      $saistitie_ligumi[] = htmlesc($row);
    }

    $query = db_query("
      SELECT r.*
      FROM `".DB_PREF."rekini` r
      LEFT JOIN `".DB_PREF."ligumi` g ON (g.id = r.liguma_id)
      WHERE g.id IN (
        SELECT liguma_id
        FROM `".DB_PREF."ligumi_saistitie_pardeveji`
        WHERE pardeveja_id = ".$pardeveja_id."
      )
    ");
    $saistito_lig_rekini = array();

    while($row = db_get_assoc($query)){
      $saistito_lig_rekini[$row['liguma_id']][] = htmlesc($row);
    }

  }

  if ($subtab == 'norekini') {

    $query = db_query("
      SELECT r.*
      FROM `".DB_PREF."pardeveji_rekini` r
      WHERE r.`pardeveja_id` = ".$pardeveja_id . "
      ORDER BY r.izr_datums DESC, r.id DESC
    ");
    $pard_rekini = array();

    while($row = db_get_assoc($query)){
      $pard_rekini[] = htmlesc($row);
    }

  }

  $akt_atg_skaits = db_get_val(db_query("
    SELECT COUNT(*)
    FROM `".DB_PREF."atgadinajumi` a
    LEFT JOIN `".DB_PREF."ligumi` g ON (a.liguma_id = g.id)
    WHERE
      a.statuss = 1 AND
      a.datums <= CURDATE() AND
      (
        a.pardeveja_id = ".$pardeveja_id." OR
        g.pardeveja_id = ".$pardeveja_id." OR
        g.id IN (
          SELECT liguma_id
          FROM `".DB_PREF."ligumi_saistitie_pardeveji`
          WHERE pardeveja_id = ".$pardeveja_id."
        )
      ) AND
      a.`redzamiba` IN ('kopejs', ".(is_admin() ? "'admin'" : "'pardevejs'").")
  "));

}
?>
<iframe src="" style="display:none;" id="downloadframe" name="downloadframe"></iframe>

<? show_autodownload_iframes() ?>

<? // Prototypes // ?>
<div style="display:none;">

  <? $form_names = array('darb_rekviziti', 'darb_atgadinajumi', 'darb_ligumi', 'darb_saistitie_ligumi', 'darb_norekini', 'darb_statistika', 'darb_izpilde') ?>

  <? foreach($form_names as $form_name) { ?>

    <?
    $fields_query = db_query("SELECT * FROM `".DB_PREF."formu_lauki` WHERE forma = '".esc($form_name)."' ORDER BY id ASC");
    $fields = array();

    while($row = db_get_assoc($fields_query)) {
      $fields[] = $row;
    }
    ?>

    <div id="komentarsprot_<?= $form_name ?>">
      <table class="comment_form">
        <tr>
          <td width="70">Komentārs:</td>
          <td>
            <textarea name="komentari_new[<?= $form_name ?>][komentars][]" rows="2" cols="30"></textarea>
          </td>
        </tr>
        <? if (!empty($fields)) { ?>
        <tr>
          <td>Lauks:</td>
          <td>
            <select name="komentari_new[<?= $form_name ?>][formas_lauka_id][]">
              <option value="">Nepiesaistīts laukam</option>
              <? foreach($fields as $row) { ?>
                <option value="<?= $row['id'] ?>"><?= $row['nosaukums'] ?></option>
              <? } ?>
            </select>
          </td>
        </tr>
        <? } ?>
      </table>
    </div>

  <? } ?>

</div>

<? if (check_access('darbinieki-admin')) { ?>
  <h1 style="font-size:20px;" id="darbinieka_nosaukums"><?=!empty($d['vards'])?$d['vards']:""?></h1>
<? } ?>

<? if (check_access('darbinieki-admin')) { ?>

  <? if (!empty($pardevejs->id)) { ?>

    <div class="sub-menu">

      <span>
        <?php
        $menu = array();

        if(check_access('darbinieki-ligumi') || ($pardevejs->id == $_SESSION['user']['id'] && check_access('darbinieki-savi-ligumi'))) {
          $menu[] = '<a href="?c=darbinieki&a=labot&id='.$pardevejs->id.'&subtab=ligumi" '. (($subtab == 'ligumi') ? 'class="active"' : '').'>Līgumi</a>';
        }

        if((check_access('darbinieki-ligumi') || ($pardevejs->id == $_SESSION['user']['id'] && check_access('darbinieki-savi-ligumi'))) && get_saist_lig_skaits($pardevejs->id)) {
          $menu[] = '<a href="?c=darbinieki&a=labot&id='.$pardevejs->id.'&subtab=saistitie_ligumi" '. (($subtab == 'saistitie_ligumi') ? 'class="active"' : '').'>Saistītie līgumi</a>';
        }

        //  Korporatīvajam pagaidām paslēpjam
        if((check_access('darbinieki-statistika') || ($pardevejs->id == $_SESSION['user']['id'] && check_access('darbinieki-sava-statistika'))) && $pardevejs->id != KORP_VADITAJA_ID) {
          $menu[] = '<a href="?c=darbinieki&a=labot&id='.$pardevejs->id.'&subtab=statistika" '. (($subtab == 'statistika') ? 'class="active"' : '').'>Statistika</a>';
        }

        if((check_access('darbinieki-plana-izpilde') || ($pardevejs->id == $_SESSION['user']['id'] && check_access('darbinieki-sava-plana-izpilde'))) && (!isset($d['vaditajs']) || !$d['vaditajs']) ) {
          $menu[] = '<a href="?c=darbinieki&a=labot&id='.$pardevejs->id.'&subtab=izpilde" '. (($subtab == 'izpilde') ? 'class="active"' : '').'>Plāna izpilde</a>';
        }

        if(isset($d['vaditajs']) && $d['vaditajs'] && (check_access('darbinieki-bonuss') || $pardevejs->id == $_SESSION['user']['id'])) {
          $menu[] = '<a href="?c=darbinieki&a=labot&id='.$pardevejs->id.'&subtab=bonuss" '. (($subtab == 'bonuss') ? 'class="active"' : '').'>Plāna izpilde</a>';
        }

        if (check_access('darbinieki-rekini') || ($pardevejs->id == $_SESSION['user']['id']) && check_access('darbinieki-savi-rekini')) { // Visible for admin, bookkeeper, seller but not to limited
          $menu[] = '<a href="?c=darbinieki&a=labot&id='.$pardevejs->id.'&subtab=norekini" '. (($subtab == 'norekini') ? 'class="active"' : '').'>Rēķini</a>';
        }

        if(check_access('darbinieki-maksajumi') || ($pardevejs->id == $_SESSION['user']['id']) && check_access('darbinieki-savi-maksajumi')) {
          $menu[] = '<a href="?c=darbinieki&a=labot&id='.$pardevejs->id.'&subtab=maksajumi" '. (($subtab == 'maksajumi') ? 'class="active"' : '').'>Maksājumi</a>';
        }

        if(check_access('darbinieki-rekviziti') || ($pardevejs->id == $_SESSION['user']['id']) && check_access('darbinieki-savi-rekviziti')) {
          $menu[] = '<a href="?c=darbinieki&a=labot&id='.$pardevejs->id.'&subtab=rekviziti" '. (($subtab == 'rekviziti') ? 'class="active"' : '').'>Rekvizīti</a>';
        }

        if(check_access('darbinieki-atgadinajumi') || ($pardevejs->id == $_SESSION['user']['id']) && check_access('darbinieki-savi-atgadinajumi')) {
          $menu[] = '<a href="?c=darbinieki&a=labot&id='.$pardevejs->id.'&subtab=atgadinajumi" '. (($subtab == 'atgadinajumi') ? 'class="active"' : '').'>Atgādinājumi ' . (!empty($akt_atg_skaits) ? '(' . $akt_atg_skaits . ')' : '').'</a>';
        }

        if(is_superadmin() && !$d['superadmin']) {
          $menu[] = '<a href="?c=darbinieki&a=labot&id='.$pardevejs->id.'&subtab=tiesibas" '. (($subtab == 'tiesibas') ? 'class="active"' : '').'>Tiesības</a>';
        }

        echo implode(' | ', $menu);
        ?>
      </span>

    </div>

  <? } ?>

<? } ?>

<? if ($subtab != 'maksajumi') { ?>
  <button id="labotdzestbutton" onClick="return editoradd(this, <?= is_admin() ? 1 : 0 ?>, <?= is_limited() ? $_SESSION['user']['ierobezots'] : 0 ?>)">Labot</button>
<? } ?>

<div class="hidethistoo">
  <?php if($pardevejs->canDelete() && check_access('darbinieki-rekviziti')) { ?>
    <input type="button" onclick="window.location = '?c=darbinieki&a=delete&id=<?php echo $pardevejs->id;?>'" class="show_in_act_panel align_left" value="Dzēst darbinieku">
  <?php } ?>
</div>

<? require("forms/".$subtab.".php"); ?>

<script>
editordisable();

<? if(empty($pardeveja_id)) { ?>
  editoradd($('#labotdzestbutton'), <?= is_admin() ? 1 : 0 ?>, <?= is_limited() ? 1 : 0 ?>);
<? } ?>
</script>