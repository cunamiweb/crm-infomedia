<?php
if (!check_access('darbinieki-rekviziti') && ($pardeveja_id != $_SESSION['user']['id'] || !check_access('darbinieki-savi-rekviziti'))) {
  die();
}

if (!empty($_POST)) {

  $errors = validate_darbinieks_rekviziti_form($_POST);

  if (empty($errors)) {

    $log_data = array();

    if (!empty($pardeveja_id)) {
      $log_data['klients']['old'] = db_get_assoc(db_query("SELECT * FROM `".DB_PREF."pardeveji` WHERE id = ".$pardeveja_id." LIMIT 1"));
      $log_data['epasti']['old_m'] = db_get_assoc_all(db_query("SELECT * FROM `".DB_PREF."pardeveji_epasti` WHERE pardeveja_id = ".$pardeveja_id));
      $log_data['telefoni']['old_m'] = db_get_assoc_all(db_query("SELECT * FROM `".DB_PREF."pardeveji_telefoni` WHERE pardeveja_id = ".$pardeveja_id));
    }

    $_POST = trim_array($_POST);

    if (check_access('darbinieki-rekviziti')) {
      $agenta_lig_datums = !empty($_POST['agenta_lig_datums']) ? "'".esc(date('Y-m-d', strtotime($_POST['agenta_lig_datums'])))."'" : 'null';
      $pvn_likme = (float) str_replace(',', '.', $_POST['pvn_likme']);
      $aktivs_no = !empty($_POST['aktivs_no']) ? "'".esc(date('Y-m-d', strtotime($_POST['aktivs_no'])))."'" : 'null';
      $aktivs_lidz = !empty($_POST['aktivs_lidz']) ? "'".esc(date('Y-m-d', strtotime($_POST['aktivs_lidz'])))."'" : 'null';
    }

    $password = '';

    if (!empty($_POST['password1'])) {
      $password = md5($_POST['password1']);
    }

    $vaditajs = isset($_POST['vaditajs']) && $_POST['vaditajs'] ? 1 : 0;
    if(!isset($_POST['vaditajs_id']) || !$_POST['vaditajs_id']) {
      $vaditajs_id = '(null)';
    } else {
      $vaditajs_id = $_POST['vaditajs_id'];
    }

    if(getGet('a') == 'labot') {

      if (check_access('darbinieki-rekviziti')) {

        $sql = "
          UPDATE `".DB_PREF."pardeveji`
          SET
            " . (check_access('darbinieki-labot-vaditaju') ? "vaditajs = " . $vaditajs . ", " : "") ."
            " . (check_access('darbinieki-labot-vaditaju') ? "vaditajs_id = " . $vaditajs_id . ", " : "") ."
            vards = '".esc($_POST['vards'])."',
            jurpers_nosauk = '".esc($_POST['jurpers_nosauk'])."',
            jurpers_adrese = '".esc($_POST['jurpers_adrese'])."',
            reg_nr = '".esc($_POST['reg_nr'])."',
            pvn_maksatajs = ".(!empty($_POST['pvn_maksatajs']) ? 1 : 0).",
            pvn_nr = '".esc($_POST['pvn_nr'])."',
            konta_nr = '".esc($_POST['konta_nr'])."',
            norek_rekviziti = '".esc($_POST['norek_rekviziti'])."',
            agenta_lig_nr = '".esc($_POST['agenta_lig_nr'])."',
            agenta_lig_datums = ".$agenta_lig_datums.",
            pvn_likme = ".$pvn_likme.",
            parole = ".(!empty($password) ? "'".esc($password)."'" : 'parole').",
            aktivs = ".(!empty($_POST['aktivs']) ? 1 : 0).",
            aktivs_no = ".$aktivs_no.",
            aktivs_lidz = ".$aktivs_lidz."
          WHERE id = ".$pardeveja_id."
          LIMIT 1
        ";

      } else {

        $sql = "
          UPDATE `".DB_PREF."pardeveji`
          SET
            vards = '".esc($_POST['vards'])."',
            parole = ".(!empty($password) ? "'".esc($password)."'" : 'parole')."
          WHERE id = ".$pardeveja_id."
          LIMIT 1
        ";

      }

      db_query($sql);

    } else {

      $sql = "
        INSERT INTO `".DB_PREF."pardeveji`
        SET
          " . (check_access('darbinieki-labot-vaditaju') ? "vaditajs = " . $vaditajs . ", " : "") ."
          " . (check_access('darbinieki-labot-vaditaju') ? "vaditajs_id = " . $vaditajs_id . ", " : "") ."
          vards = '".esc($_POST['vards'])."',
          lietotajvards = '".esc($_POST['lietotajvards'])."',
          tips = 'sales',
          jurpers_nosauk = '".esc($_POST['jurpers_nosauk'])."',
          jurpers_adrese = '".esc($_POST['jurpers_adrese'])."',
          reg_nr = '".esc($_POST['reg_nr'])."',
          pvn_maksatajs = ".(!empty($_POST['pvn_maksatajs']) ? 1 : 0).",
          pvn_nr = '".esc($_POST['pvn_nr'])."',
          konta_nr = '".esc($_POST['konta_nr'])."',
          norek_rekviziti = '".esc($_POST['norek_rekviziti'])."',
          agenta_lig_nr = '".esc($_POST['agenta_lig_nr'])."',
          agenta_lig_datums = ".$agenta_lig_datums.",
          pvn_likme = ".$pvn_likme.",
          parole = ".(!empty($password) ? "'".esc($password)."'" : 'parole').",
					aktivs = ".(!empty($_POST['aktivs']) ? 1 : 0).",
          aktivs_no = ".$aktivs_no.",
          aktivs_lidz = ".$aktivs_lidz."
      ";

      db_query($sql);

      $pardeveja_id = db_last_id();

    }


    if (getGet('a') == 'labot') {

      // deleting old email addresses

      $sql = "
        DELETE FROM `".DB_PREF."pardeveji_epasti`
        WHERE
          pardeveja_id = ".$pardeveja_id."
          " . (!empty($_POST['epasti']) ? "AND id NOT IN (".implode(',', array_keys($_POST['epasti'])).") " : '')."
      ";

      db_query($sql);

      // updating email addresses

      if (!empty($_POST['epasti'])) {

        foreach($_POST['epasti'] as $epasta_id => $epasts) {

          $sql = "
            UPDATE `".DB_PREF."pardeveji_epasti`
            SET
              epasts = '".esc($epasts)."'
            WHERE id = ".$epasta_id."
            LIMIT 1
          ";

          db_query($sql);

        }

      }

    }

    // adding email addresses

    if (!empty($_POST['epasti_new'])) {

      foreach($_POST['epasti_new'] as $epasts) {

        if (!empty($epasts)) {

          $sql = "
            INSERT INTO `".DB_PREF."pardeveji_epasti` (
              pardeveja_id,
              epasts
            ) VALUES (
              ".$pardeveja_id.",
              '".esc($epasts)."'
            )
          ";

          db_query($sql);

        }

      }

    }


    if(getGet('a') == 'labot') {

      // deleting old tel numbers

      $sql = "
        DELETE FROM `".DB_PREF."pardeveji_telefoni`
        WHERE
          pardeveja_id = ".$pardeveja_id."
          " . (!empty($_POST['telefoni']) ? "AND id NOT IN (".implode(',', array_keys($_POST['telefoni'])).") " : '')."
      ";

      db_query($sql);

      // updating tel numbers

      if (!empty($_POST['telefoni'])) {

        foreach($_POST['telefoni'] as $telefona_id => $tel) {

          $sql = "
            UPDATE `".DB_PREF."pardeveji_telefoni`
            SET
              telefons = '".esc($tel)."'
            WHERE id = ".$telefona_id."
            LIMIT 1
          ";

          db_query($sql);

        }

      }

    }

    // adding tel numbers

    if (!empty($_POST['telefoni_new'])) {

      foreach($_POST['telefoni_new'] as $tel) {

        if (!empty($tel)) {

          $sql = "
            INSERT INTO `".DB_PREF."pardeveji_telefoni` (
              pardeveja_id,
              telefons
            ) VALUES (
              ".$pardeveja_id.",
              '".esc($tel)."'
            )
          ";

          db_query($sql);

        }

      }

    }

    //  Amati
    if(check_access('darbinieki-labot-amatu')) {

      //  Log data
      $log_data['amati']['old_m'] = db_get_assoc_all(db_query("SELECT * FROM ".DB_PREF."pardeveji_amati WHERE pardevejs_id = " . $pardeveja_id));

      // Dzēšam noņemtos amatus
      $sql = "
        DELETE FROM `".DB_PREF."pardeveji_amati`
        WHERE
          pardevejs_id = ".$pardeveja_id."
          " . (!empty($_POST['amats']) ? "AND id NOT IN (".implode(',', array_keys($_POST['amats'])).") " : '')."
      ";

      db_query($sql);


      //  Labojam esošos amatus
      if(isset($_POST['amats'])) {

        foreach((array)$_POST['amats'] as $pa_id => $amats_data) {
          $month = ((int)$amats_data['sakot_no_q'] * 3) - 2; // Ceturkšņa pirmais mēnesis
          $sakot_no = date('Y-m-d', mktime(0,0,0,$month,1,$amats_data['sakot_no_y']));
          $sql = "
            UPDATE ".DB_PREF."pardeveji_amati
            SET
              amats_id = ".esc($amats_data['amats_id']).",
              sakot_no = '".$sakot_no."'
            WHERE
              id = " . esc($pa_id) . "
            LIMIT 1
          ";
          db_query($sql);
        }
      }

      // Pievienojam jaunus amatus
      if(isset($_POST['amats_new'])) {

        foreach((array)$_POST['amats_new']['sakot_no_y'] as $i => $sakot_no_y) {

          $month = ((int)$_POST['amats_new']['sakot_no_q'][$i] * 3) - 2; // Ceturkšņa pirmais mēnesis
          $sakot_no = date('Y-m-d', mktime(0,0,0,$month,1,$sakot_no_y));
          $amats_id = (int)$_POST['amats_new']['amats_id'][$i];

          $sql = "
            INSERT INTO `".DB_PREF."pardeveji_amati` (
              pardevejs_id,
              amats_id,
              sakot_no
            ) VALUES (
              ".$pardeveja_id.",
              ".$amats_id.",
              '".$sakot_no."'
            )
          ";


          db_query($sql);
        }
      }
    }

    // inserting files
    process_files(array('darb_rekviziti'), $pardeveja_id);

    // inserting comments
    process_comments(array('darb_rekviziti'), $pardeveja_id);

    $log_data['klients']['new'] = db_get_assoc(db_query("SELECT * FROM `".DB_PREF."pardeveji` WHERE id = ".$pardeveja_id." LIMIT 1"));
    $log_data['klients']['title'] = sprintf('Laboja pārdevēja %s rekvizītus', $log_data['klients']['new']['vards']);

    $log_data['amati']['new_m'] = db_get_assoc_all(db_query("SELECT * FROM ".DB_PREF."pardeveji_amati WHERE pardevejs_id = " . $pardeveja_id));
    $log_data['amati']['title'] = sprintf('Laboja pārdevēja %s amatus', $log_data['klients']['new']['vards']);

    $log_data['epasti']['new_m'] = db_get_assoc_all(db_query("SELECT * FROM `".DB_PREF."pardeveji_epasti` WHERE pardeveja_id = ".$pardeveja_id));
    $log_data['epasti']['title'] = sprintf('Laboja pārdevēja %s e-pastus', $log_data['klients']['new']['vards']);

    $log_data['telefoni']['new_m'] = db_get_assoc_all(db_query("SELECT * FROM `".DB_PREF."pardeveji_telefoni` WHERE pardeveja_id = ".$pardeveja_id));
    $log_data['telefoni']['title'] = sprintf('Laboja pārdevēja %s telefonus', $log_data['klients']['new']['vards']);

    log_add("laboja", $log_data);

    header('Location: ?c=darbinieki&a=labot&id=' . $pardeveja_id . '&subtab=' . $subtab);
    die();

  }

}
?>