<?

if (!check_access('darbinieki-rekini')) {
  die('Jums nav pieejas tiesību šai darbībai.');
}

if (empty($_GET['darbinieka_id'])) {
  die();
}

$darbinieka_id = (int) $_GET['darbinieka_id'];

$darbinieks = db_get_assoc(db_query("
  SELECT *
  FROM `".DB_PREF."pardeveji`
  WHERE id = ".$darbinieka_id."
")); // legacy

$darbinieks_model = Pardevejs::model()->findByPk($darbinieka_id);

//  Savi līgumi
$pardeveju_ids = array($darbinieks_model->id);

//  Ja useris ir vadītājs un viņam ir padotie tad ņem arī padoto līgumu maksājumus
if($darbinieks_model->id == KORP_VADITAJA_ID) {
  $padotie = $darbinieks_model->getPadotie();
  foreach((array)$padotie as $padotais) {
    if($padotais->tips == 'partner') {
      $pardeveju_ids[] = $padotais->id;
    }
  }
}

if (empty($darbinieks)) {
  die();
}

$rek_prefix = date('y') . '/';

$p = explode(' ', $darbinieks['vards']);

foreach($p as $word) {
  $rek_prefix .= strtoupper(translit(mb_substr($word, 0, 1)));
}

//  Bonuss
$bonusi = array();
if($darbinieks['vaditajs']) {

  //for($year = BONUSS_START_YEAR; $year <= date('Y'); $year++) {
  for($year = date('Y'); $year >= BONUSS_START_YEAR; $year--) {
    $bonusi[$year] = get_bonuss_stats($darbinieka_id, $year, true, true);
  }
}

//  Korekcija
if($darbinieks['vaditajs']) {
  $korekcijas = corrections_total($darbinieka_id);
} else {
  $korekcijas = array();
}

//  Provīzija
$query = db_query("
  SELECT
    m.*,
    g.nosaukums as klienta_nosaukums,
    r.pvn as rekina_pvn,
    g.provizija_pardevejam,
    g.atlidziba_vaditajam,
    r.rek_nr,
    r.kopsumma as rekina_kopsumma,
    (
        SELECT SUM(no_summas)
        FROM `".DB_PREF."pardeveji_rekini_detalas` d2
        LEFT JOIN ".DB_PREF."pardeveji_rekini pr2 ON pr2.id = d2.pardeveja_rekina_id
        WHERE
          rekina_id = m.rekina_id
          AND pr2.pardeveja_id = ".$darbinieka_id."
    ) as prov_izmaksu_summa,
    g.pardeveja_id
  FROM `".DB_PREF."rekini_maksajumi` m
  LEFT JOIN `".DB_PREF."rekini` r ON (m.rekina_id = r.id)
  LEFT JOIN `".DB_PREF."ligumi` g ON (r.liguma_id = g.id)
  LEFT JOIN (
    	SELECT d1.* FROM `".DB_PREF."pardeveji_rekini_detalas` d1
    	LEFT JOIN ".DB_PREF."pardeveji_rekini pr ON (d1.pardeveja_rekina_id = pr.id)
    	WHERE pr.pardeveja_id = ".$darbinieka_id."
    ) d ON (m.id = d.rekina_maksajuma_id)
  WHERE
    g.pardeveja_id IN (".implode(', ', $pardeveju_ids).") AND
    d.id IS NULL AND
    r.atkapsanas_rekins = 0 AND
    (
      r.nodots_piedz_datums IS NULL OR
      r.nodots_piedz_datums > m.datums
    )
  HAVING
    m.summa <= IFNULL(r.kopsumma, 0) - IFNULL(prov_izmaksu_summa, 0)
  ORDER BY m.datums DESC, m.id DESC
");

$apmaksas = array();
while($row = db_get_assoc($query)) {
  $apmaksas[$row['id']] = $row;
}


$query = db_query("
  SELECT
    r.*,
    g.nosaukums as klienta_nosaukums,
    r.pvn as rekina_pvn,
    g.provizija_pardevejam,
    g.atlidziba_vaditajam,
    (IFNULL(r.kopsumma, 0) - SUM(IFNULL(m.summa, 0))) as neapm_summa,
    (
        SELECT SUM(no_summas)
        FROM `".DB_PREF."pardeveji_rekini_detalas` d2
        LEFT JOIN `".DB_PREF."pardeveji_rekini` pr2 ON pr2.id = d2.pardeveja_rekina_id
        WHERE
          rekina_id = r.id
          AND pr2.pardeveja_id = ".$darbinieka_id."
    ) as prov_izmaksu_summa,
    g.pardeveja_id
  FROM `".DB_PREF."rekini` r
  LEFT JOIN `".DB_PREF."ligumi` g ON (r.liguma_id = g.id)
  LEFT JOIN `".DB_PREF."rekini_maksajumi` m ON (m.rekina_id = r.id)
  WHERE
    g.pardeveja_id IN (".implode(', ', $pardeveju_ids).") AND
    r.atkapsanas_rekins = 0 AND
    r.anulets = 0 AND
    r.nodots_piedz_datums IS NULL
  GROUP BY r.id
  HAVING
    neapm_summa > 0 AND
    neapm_summa <= IFNULL(r.kopsumma, 0) - IFNULL(prov_izmaksu_summa, 0)
  ORDER BY r.apm_termins ASC
");

$avansa_rekini = array();
while($row = db_get_assoc($query)) {
  $avansa_rekini[$row['id']] = $row;
}


//  Prēmijas

$quarters_ligumi = array();

/**
 * Atrod gadu un ceturksni pēc līguma id
 * @param array $row - rēķina rinda
 * return mixed false ja neatrod; [year, quarter] ja atrod
 */
function getYearQuarter($row)
{
  global $quarters_ligumi;
  foreach($quarters_ligumi as $ligumi_year => $ligumi_quarters) {
    foreach($ligumi_quarters as $ligumi_quarter => $l_ids) {
      if(in_array($row['liguma_id'], $l_ids)) {
        return array($ligumi_year, $ligumi_quarter);
      }
    }
  }
}

//  Saglabātās (aprēķinātās) prēmijas - ātrdarbībai
$premiums_cache = array();

/**
 * Iegūst un atgriež līguma prēmijas likmi. Sākumā izvelk gadu un ceturksni,
 * tad skatās vai tam ceturksnim jau ir aprēķināta prēmija. Ja ir tad atgriež
 * jau aprēķināto, ja nav tad rēķina un ieliek to aprēķināto masīvā.
 *
 * @param array $row - rēķina rinda
 * @param array &$premiums_cache - saglabātās aprēķinātās prēmijas
 * @return decimal līguma prēmijas likme
 */
function getContractPremium($row, &$cache, $merchant_id)
{
  list($year, $quarter) = getYearQuarter($row);

  if(!isset($cache[$year][$quarter])) {
    $cache[$year][$quarter] = get_contract_premium($year, $quarter, $merchant_id);
  }
  return $cache[$year][$quarter];
}

$yearly_stats = array();

function getIzpildeQuarter($year, $quarter, &$cache)
{
  global $darbinieka_id;
  if(!isset($cache[$year][$quarter])) {
    $cache[$year][$quarter] = get_izpildits($darbinieka_id, $year, $quarter, null, true);
  }

  return $cache[$year][$quarter];
}

$p_avansa_rekini = array();
$p_apmaksas = array();

$ligumi = array();
$quarters = array();

$result = db_query("
  SELECT *
  FROM `".DB_PREF."premijas` p
  WHERE
    p.pardevejs_id = " .  esc($darbinieka_id) . " AND
    p.ir_premija = 1 AND
    p.premijas_apmers != 0
");

$premium = mysqli_num_rows($result) > 0 ? true : false;

if($premium) {
  while($row = db_get_assoc($result)) {
    if(is_quarter_fixed($row['gads'], $row['ceturksnis'])) {
      //  No gada un ceturkšņa numura jāuztaisa datumu range.
      //  Ceturkšņa pirmā mēneša 1. datums līdz ceturkšņa pēdējā mēneša pēdējais datums (31?)

      $data = getIzpildeQuarter($row['gads'], $row['ceturksnis'], $yearly_stats);

      $quarters[] = array('year' => $row['gads'], 'quarter' => $row['ceturksnis']);

      $ligumi = array_merge($ligumi, $data['kopa']['ligumu_id']);

        if(!isset($quarters_ligumi[$row['gads']][$row['ceturksnis']])) {
          $quarters_ligumi[$row['gads']][$row['ceturksnis']] = array();
        }
        $quarters_ligumi[$row['gads']][$row['ceturksnis']] = array_merge($quarters_ligumi[$row['gads']][$row['ceturksnis']], $data['kopa']['ligumu_id']);
      //$first_m = str_pad((3 * $row['ceturksnis'] - 2), 2, '0', STR_PAD_LEFT);
      //$last_m = str_pad((3 * $row['ceturksnis']), 2, '0', STR_PAD_LEFT);

      //$date_queries[] = 'r.apm_termins >= "' . $row['gads'] . '-' . $first_m . '-01" AND r.apm_termins <= "' . $row['gads'] . '-' . $last_m . '-31"';
      //$date_queries[] = 'EXTRACT(YEAR_MONTH FROM r.apm_termins) BETWEEN "' . $row['gads'].$first_m . '" AND "' . $row['gads'].$last_m . '"';
    }
  }

  $result = db_query("
    SELECT
    r.*,
    g.id as liguma_id,
    g.nosaukums as klienta_nosaukums,
    r.pvn as rekina_pvn,
    r.izr_datums as rekina_izr_datums,
    r.kopsumma as rekina_kopsumma,
    r.apm_termins as rekina_apm_termins,
    MAX(m.datums) as apm_datums,
    (
        SELECT SUM(no_summas)
        FROM `".DB_PREF."pardeveji_rekini_premijas_detalas`
        WHERE rekina_id = r.id
    ) as premiju_izmaksu_summa,
    p.pvn_likme
    FROM `".DB_PREF."rekini` r
    LEFT JOIN `".DB_PREF."ligumi` g ON (r.liguma_id = g.id)
    LEFT JOIN `".DB_PREF."pardeveji_rekini_premijas_detalas` d ON (r.id = d.rekina_id)
    LEFT JOIN `".DB_PREF."pardeveji` p ON (p.id = g.pardeveja_id)
    LEFT JOIN `".DB_PREF."rekini_maksajumi` m ON (m.rekina_id = r.id)
    WHERE
      g.pardeveja_id = ".esc($darbinieka_id)." AND
      d.id IS NULL AND
      r.atkapsanas_rekins = 0 AND
      r.nodots_piedz = 0 AND
      -- r.barteris = 0 AND
      r.anulets = 0 AND
      r.liguma_id IN (" . (!empty($ligumi) ? implode(', ', $ligumi) : '0') . ")
    GROUP BY r.id
    HAVING
      r.apm_summa >= IFNULL(r.kopsumma, 0) AND
      IFNULL(r.kopsumma, 0) - IFNULL(premiju_izmaksu_summa, 0) > 0
    ORDER BY apm_datums DESC, r.id DESC
  ");


  while($row = db_get_assoc($result)) {
    $p_apmaksas[$row['id']] = $row;
  }

  $result = db_query("
    SELECT
    r.*,
    g.id as liguma_id,
    g.nosaukums as klienta_nosaukums,
    r.pvn as rekina_pvn,
    r.izr_datums as rekina_izr_datums,
    r.kopsumma as rekina_kopsumma,
    r.apm_termins as rekina_apm_termins,
    (
        SELECT SUM(no_summas)
        FROM `".DB_PREF."pardeveji_rekini_premijas_detalas`
        WHERE rekina_id = r.id
    ) as premiju_izmaksu_summa,
    (
      SELECT SUM(summa)
      FROM `".DB_PREF."rekini_maksajumi`
      WHERE rekina_id = r.id
    ) as maksajumu_summa,
    p.pvn_likme
    FROM `".DB_PREF."rekini` r
    LEFT JOIN `".DB_PREF."ligumi` g ON (r.liguma_id = g.id)
    LEFT JOIN `".DB_PREF."pardeveji_rekini_premijas_detalas` d ON (r.id = d.rekina_id)
    LEFT JOIN `".DB_PREF."pardeveji` p ON (p.id = g.pardeveja_id)
    WHERE
      g.pardeveja_id = ".esc($darbinieka_id)." AND
      d.id IS NULL AND
      r.atkapsanas_rekins = 0 AND
      r.nodots_piedz = 0 AND
      -- r.barteris = 0 AND
      r.anulets = 0 AND
      r.liguma_id IN (" . (!empty($ligumi) ? implode(', ', $ligumi) : '0') . ")
    HAVING
      IFNULL(r.apm_summa, 0) < r.kopsumma AND
      IFNULL(r.kopsumma, 0) - IFNULL(premiju_izmaksu_summa, 0) > 0
    ORDER BY r.apm_termins ASC, r.id DESC
  ");


  while($row = db_get_assoc($result)) {
    $p_avansa_rekini[$row['id']] = $row;
  }

}

//  Komisijas procents - ja savs līgums tad provīzija, ja lietotājs ir vadītājs un konkrētais līgums ir padotā līgums, tad vadītāja atlīdzību
function getKomisija($row, $pardevejs)
{
  if($row['pardeveja_id'] == $pardevejs->id) {
    $komisija = $row['provizija_pardevejam'];
  } else {
    //  Apskatās kāda vadītāja komisija norādīta partnerim
    /*if($padotais = Pardevejs::model()->findByPk($row['pardeveja_id'])) {
     $komisija = $padotais->atlidziba_vaditajam;
    } else {
      throw new Exception('Nevar atrast padoto pēc ID = ' . $row['pardeveja_id']);
    } */
    $komisija = $row['atlidziba_vaditajam'];
  }
  return $komisija;
}


if (!empty($_POST)) {

  $_POST = trim_array($_POST);

  $errors = array();

  $uz_rokas_skaits = 0;
  $rekinaa_skaits = 0;

  //  Bonuss
  if (!empty($_POST['bonuss'])) {
    foreach($_POST['bonuss'] as $bonuss_id => $v) {
      if ($v['uz_rokas'] === '0') {
        $rekinaa_skaits ++;
      } else if ($v['uz_rokas'] === '1') {
        $uz_rokas_skaits ++;
      }
    }
  }

  //  Korekcija
  if(!empty($_POST['korekcija'])) {
    foreach($_POST['korekcija'] as $v) {
      if($v['uz_rokas'] === '0') {
        $rekinaa_skaits ++;
      } else if($v['uz_rokas'] === '1') {
        $uz_rokas_skaits++;
      }
    }
  }

  //  Provīzija
  if (!empty($_POST['apm'])) {

    foreach($_POST['apm'] as $apm_id => $v) {

      $komisija = getKomisija($apmaksas[$apm_id], $darbinieks_model);
      if($komisija /*&& bccomp($komisija, 0, 2) == 1*/) { // vai komisija ir dabūta un ir lielāka par 0.00

        $apmaksas[$apm_id]['komisija'] = $komisija;

        if ($v['uz_rokas'] === '0') {
          $rekinaa_skaits ++;
        } else if ($v['uz_rokas'] === '1') {
          $uz_rokas_skaits ++;
        }
      }
    }
  }

  if (!empty($_POST['avansa'])) {

    foreach($_POST['avansa'] as $rek_id => $v) {

      $komisija = getKomisija($avansa_rekini[$rek_id], $darbinieks_model);
      if($komisija /*&& bccomp($komisija, 0, 2) == 1*/) { // vai komisija ir dabūta un ir lielāka par 0.00

       $avansa_rekini[$rek_id]['komisija'] = $komisija;

        if ($v['uz_rokas'] === '0') {
          $rekinaa_skaits ++;
        } else if ($v['uz_rokas'] === '1') {
          $uz_rokas_skaits ++;
        }
      }
    }
  }

  //  Prēmija
  if (!empty($_POST['p_apm'])) {

    foreach($_POST['p_apm'] as $apm_id => $v) {

      if ($v['uz_rokas'] === '0') {
        $rekinaa_skaits ++;
      } else if ($v['uz_rokas'] === '1') {
        $uz_rokas_skaits ++;
      }

    }

  }

  if (!empty($_POST['p_avansa'])) {

    foreach($_POST['p_avansa'] as $rek_id => $v) {

      if ($v['uz_rokas'] === '0') {
        $rekinaa_skaits ++;
      } else if ($v['uz_rokas'] === '1') {
        $uz_rokas_skaits ++;
      }

    }

  }

  //  Piemaksas
  if (!empty($_POST['piemaksas'])) {

    foreach($_POST['piemaksas'] as $i => $v) {

      if (!empty($v['summa'])) {

        if ($v['uz_rokas'] === '0') {
          $rekinaa_skaits ++;
        } else if ($v['uz_rokas'] === '1') {
          $uz_rokas_skaits ++;
        }

      }

    }

  }

  if (empty($_POST['izr_datums'])) {
    $errors[] = 'Nav norādīts izrakstīšanas datums!';
  }

  if (empty($_POST['rek_nr'])) {
    $errors[] = 'Nav norādīts rēķina numurs!';
  }

  if (empty($uz_rokas_skaits) && empty($rekinaa_skaits)) {
    $errors[] = 'Nekas netika atzīmēts!';
  }

  if (empty($errors)) {

    $log_data = array();

    $izr_datums = date('Y-m-d', strtotime($_POST['izr_datums']));

    if (!empty($uz_rokas_skaits)) {

      $rek_nr = get_darb_rek_next_no('C', $darbinieka_id);

      db_query("
        INSERT INTO `".DB_PREF."pardeveji_rekini` SET
          pardeveja_id = ".$darbinieka_id.",
          rek_nr = '".esc($rek_nr)."',
          izr_datums = '".esc($izr_datums)."',
          summa = 0,
          pvn = 0,
          kopsumma = 0,
          uz_rokas = 1,
					sia='".esc($_POST['from'])."'
      ");

      $pardeveja_rekina_id = db_last_id();

      $rekina_summa = 0;

      //  Bonusi
      if(!empty($_POST['bonuss'])) {
        foreach($_POST['bonuss'] as $bonuss_id => $v) {
          if ($v['uz_rokas'] === '1') {

            list($year, $type, $period) = explode('-', $bonuss_id);
            if($type == 'quarter') {
              $quarter = esc($period);
              $month = '(null)';
              $b_data = $bonusi[$year]['quarters'][$period];
            } elseif($type == 'month') {
              $quarter = '(null)';
              $month = esc($period);
              $b_data = $bonusi[$year]['months'][$period];
            } elseif($type == 'year') {
              $quarter = '(null)';
              $month = '(null)';
              $b_data = $bonusi[$year]['year'];
            }

            db_query("
              INSERT INTO `".DB_PREF."bonusi` SET
                gads = " . $year . ",
                ceturksnis = " . $quarter . ",
                menesis = " . $month . ",
                vaditajs_id = " . esc($darbinieka_id) . ",
                summa = " . $b_data['summa'] . ",
                ligumi = '" . $b_data['ligumi'] . "',
                min_proc = " . $b_data['min_proc'] . ",
                opt_proc = " . $b_data['opt_proc'] . ",
                virs_min = " . $b_data['virs_min'] . ",
                virs_opt = " . $b_data['virs_opt'] . ",
                summa_min = " . $b_data['summa_min'] . ",
                summa_opt = " . $b_data['summa_opt'] . ",
                plans_min = " . $b_data['plans_min'] . ",
                plans_opt = " . $b_data['plans_opt'] . ",
                pardeveja_rekina_id = ".$pardeveja_rekina_id.",
                rekini = '" . $b_data['rekini'] ."'
            ");

            $rekina_summa += round($b_data['summa_min'] + $b_data['summa_opt'], 2);

          }
        }
      }

      // Korekcijas
      if(!empty($_POST['korekcija'])) {
        foreach($_POST['korekcija'] as $korekcija_id => $v) {
          if ($v['uz_rokas'] === '1') {
            $korekcija = $korekcijas[$korekcija_id];

            db_query("
              INSERT INTO ".DB_PREF."bonusi_korekcijas
              SET
                vaditajs_id = " . esc($darbinieka_id) . ",
                rekins_id = ".$pardeveja_rekina_id.",
                summa = " .  round($korekcija['kopsumma_wo_pvn'], 2) . ",
                par_pvn = " . $korekcija['pvn'] . "
            ");

            $rekina_summa += round($korekcija['kopsumma_wo_pvn'], 2);
          }
        }
      }

      // Provīzijas - saņemtās

      if (!empty($_POST['apm'])) {

        foreach($_POST['apm'] as $apm_id => $v) {

          if ($v['uz_rokas'] === '1') {

            $apm_data = $apmaksas[$apm_id];

            if(isset($apm_data['komisija'])) {  // Tikai tad ja iepriekš skaitot, tika iegūta un piefiksēta komisija

              $apm_summa_bez_pvn = $apm_data['summa'] / (1 + ($apm_data['rekina_pvn'] / 100));
              $prov_summa = round($apm_summa_bez_pvn * ($apm_data['komisija'] / 100), 2);

              db_query("
                INSERT INTO `".DB_PREF."pardeveji_rekini_detalas` SET
                  pardeveja_rekina_id = ".$pardeveja_rekina_id.",
                  rekina_id = ".$apm_data['rekina_id'].",
                  rekina_maksajuma_id = ".$apm_id.",
                  summa = '".$prov_summa."',
                  pvn_likme = 0,
                  kopsumma = '".$prov_summa."',
                  no_summas = '".$apm_data['summa']."',
                  prov_likme = '".$apm_data['komisija']."'
              ");

              $rekina_summa += $prov_summa;
            }
          }
        }
      }

      //  Provīzijas - avansa

      if (!empty($_POST['avansa'])) {

        foreach($_POST['avansa'] as $rek_id => $v) {

          if ($v['uz_rokas'] === '1') {

            $rek_data = $avansa_rekini[$rek_id];

            if(isset($rek_data['komisija'])) {  // Tikai tad ja iepriekš skaitot, tika iegūta un piefiksēta komisija

              $neapm_summa_bez_pvn = $rek_data['neapm_summa'] / (1 + ($rek_data['rekina_pvn'] / 100));
              $prov_summa = round($neapm_summa_bez_pvn * ($rek_data['komisija'] / 100), 2);

              db_query("
                INSERT INTO `".DB_PREF."pardeveji_rekini_detalas` SET
                  pardeveja_rekina_id = ".$pardeveja_rekina_id.",
                  rekina_id = ".$rek_id.",
                  rekina_maksajuma_id = null,
                  summa = '".$prov_summa."',
                  pvn_likme = 0,
                  kopsumma = '".$prov_summa."',
                  no_summas = '".$rek_data['neapm_summa']."',
                  prov_likme = '".$rek_data['komisija']."'
              ");

              $rekina_summa += $prov_summa;
            }
          }
        }
      }

      // Prēmijas - saņemtās

      if (!empty($_POST['p_apm'])) {

        foreach($_POST['p_apm'] as $rek_id => $v) {

          if ($v['uz_rokas'] === '1') {

            $apm_data = $p_apmaksas[$rek_id];

            $contract_premium = getContractPremium($apm_data, $premiums_cache, $darbinieka_id);

            //$apm_summa_bez_pvn = bcdiv($apm_data['kopsumma'], (1 + ($apm_data['rekina_pvn'] / 100)), 2);
            $apm_summa_bez_pvn = $apm_data['summa'];
            $prem_summa = $apm_summa_bez_pvn * ($contract_premium / 100);

            db_query("
              INSERT INTO `".DB_PREF."pardeveji_rekini_premijas_detalas` SET
                pardeveja_rekina_id = ".$pardeveja_rekina_id.",
                rekina_id = ".$rek_id.",
                summa = '".$prem_summa."',
                pvn_likme = 0,
                kopsumma = '".round($prem_summa, 2)."',
                no_summas = '".$apm_data['kopsumma']."',
                prem_likme = '".$contract_premium."'
            ");

            $rekina_summa += round($prem_summa, 2);

          }

        }

      }

      // Prēmijas - avansa

      if (!empty($_POST['p_avansa'])) {

        foreach($_POST['p_avansa'] as $rek_id => $v) {

          if ($v['uz_rokas'] === '1') {

            $rek_data = $p_avansa_rekini[$rek_id];

            $contract_premium = getContractPremium($rek_data, $premiums_cache, $darbinieka_id);

            //$neapm_summa_bez_pvn = bcdiv($rek_data['kopsumma'],(1 + ($rek_data['rekina_pvn'] / 100)), 2);
            $neapm_summa_bez_pvn = $rek_data['summa'];
            $prem_summa = $neapm_summa_bez_pvn * ($contract_premium / 100);

            db_query("
              INSERT INTO `".DB_PREF."pardeveji_rekini_premijas_detalas` SET
                pardeveja_rekina_id = ".$pardeveja_rekina_id.",
                rekina_id = ".$rek_id.",
                summa = '".$prem_summa."',
                pvn_likme = 0,
                kopsumma = '".round($prem_summa, 2)."',
                no_summas = '".$rek_data['kopsumma']."',
                prem_likme = '".$contract_premium."'
            ");

            $rekina_summa += round($prem_summa, 2);

          }

        }

      }

      // Piemaksas

      if (!empty($_POST['piemaksas'])) {

        foreach($_POST['piemaksas'] as $i => $v) {

          $summa = round(str_replace(',', '.', $v['summa']), 2);

          if ($v['uz_rokas'] === '1' && !empty($summa)) {

            db_query("
              INSERT INTO `".DB_PREF."pardeveji_rekini_detalas` SET
                pardeveja_rekina_id = ".$pardeveja_rekina_id.",
                rekina_id = null,
                rekina_maksajuma_id = null,
                summa = '".$summa."',
                pvn_likme = 0,
                kopsumma = '".$summa."',
                no_summas = 0,
                prov_likme = 0,
                paskaidrojums = '".esc($v['paskaidrojums'])."'
            ");

            $rekina_summa += $summa;

          }

        }

      }

      db_query("
        UPDATE `".DB_PREF."pardeveji_rekini` SET
          summa = '".$rekina_summa."',
          kopsumma = '".$rekina_summa."'
        WHERE id = ".$pardeveja_rekina_id."
        LIMIT 1
      ");

      $log_data['rekins_uz_rokas']['new'] = db_get_assoc(db_query("SELECT * FROM `".DB_PREF."pardeveji_rekini` WHERE `id` = " . $pardeveja_rekina_id));
      $log_data['rekins_uz_rokas']['title'] = sprintf('Pievienots jauns rēķins uz rokas %s pārdevējam %s', $log_data['rekins_uz_rokas']['new']['rek_nr'], $darbinieks['vards']);

      $log_data['rekins_uz_rokas_detalas']['new_m'] = db_get_assoc_all(db_query("SELECT * FROM `".DB_PREF."pardeveji_rekini_detalas` WHERE pardeveja_rekina_id = " . $pardeveja_rekina_id));
      $log_data['rekins_uz_rokas_detalas']['title'] = sprintf('Rēķina %s detaļas', $log_data['rekins_uz_rokas']['new']['rek_nr']);

    }

    if (!empty($rekinaa_skaits)) {

      $pvn_likme = (float) str_replace(',', '.', $_POST['pvn_likme']);

      $rek_nr = $_POST['rek_nr'];

      db_query("
        INSERT INTO `".DB_PREF."pardeveji_rekini` SET
          pardeveja_id = ".$darbinieka_id.",
          rek_nr = '".esc($rek_nr)."',
          izr_datums = '".esc($izr_datums)."',
          summa = 0,
          pvn = '".$pvn_likme."',
          kopsumma = 0,
          uz_rokas = 0,
					sia='".esc($_POST['from'])."'
      ");

      $pardeveja_rekina_id = db_last_id();
      $pdf_rekina_id = $pardeveja_rekina_id;

      $pamatsumma = 0;

      $piemaksu_summa = 0;
      $piemaksu_kopsumma = 0;

      //  Bonusi
      if(isset($_POST['bonuss'])) {
      foreach($_POST['bonuss'] as $bonuss_id => $v) {
        if ($v['uz_rokas'] === '0') {

          list($year, $type, $period) = explode('-', $bonuss_id);
          if($type == 'quarter') {
            $quarter = esc($period);
            $month = '(null)';
            $b_data = $bonusi[$year]['quarters'][$period];
          } elseif($type == 'month') {
            $quarter = '(null)';
            $month = esc($period);
            $b_data = $bonusi[$year]['months'][$period];
          } elseif($type == 'year') {
            $quarter = '(null)';
            $month = '(null)';
            $b_data = $bonusi[$year]['year'];
          }

          db_query("
            INSERT INTO `".DB_PREF."bonusi` SET
              gads = " . $year . ",
              ceturksnis = " . $quarter . ",
              menesis = " . $month . ",
              vaditajs_id = " . esc($darbinieka_id) . ",
              summa = " . $b_data['summa'] . ",
              ligumi = '" . $b_data['ligumi'] . "',
              min_proc = " . $b_data['min_proc'] . ",
              opt_proc = " . $b_data['opt_proc'] . ",
              virs_min = " . $b_data['virs_min'] . ",
              virs_opt = " . $b_data['virs_opt'] . ",
              summa_min = " . $b_data['summa_min'] . ",
              summa_opt = " . $b_data['summa_opt'] . ",
              plans_min = " . $b_data['plans_min'] . ",
              plans_opt = " . $b_data['plans_opt'] . ",
              pardeveja_rekina_id = ".$pardeveja_rekina_id.",
              rekini = '" . $b_data['rekini'] . "'
          ");

          $pamatsumma += round($b_data['summa_min'] + $b_data['summa_opt'], 2);
          }
        }
      }

      //  Korekcija
      if(!empty($_POST['korekcija'])) {
        foreach($_POST['korekcija'] as $korekcija_id => $v) {
          if ($v['uz_rokas'] === '0') {
            $korekcija = $korekcijas[$korekcija_id];

            db_query("
              INSERT INTO ".DB_PREF."bonusi_korekcijas
              SET
                vaditajs_id = " . esc($darbinieka_id) . ",
                rekins_id = ".$pardeveja_rekina_id.",
                summa = " .  round($korekcija['kopsumma_wo_pvn'], 2) . ",
                par_pvn = " . $korekcija['pvn'] . "
            ");

            $pamatsumma += round($korekcija['kopsumma_wo_pvn'], 2);
          }
        }
      }

      //  Prvīzijas - saņemtās
      if (!empty($_POST['apm'])) {

        foreach($_POST['apm'] as $apm_id => $v) {

          if ($v['uz_rokas'] === '0') {

            $apm_data = $apmaksas[$apm_id];

            if(isset($apm_data['komisija'])) {  // Tikai tad ja iepriekš skaitot, tika iegūta un piefiksēta komisija

              $apm_summa_bez_pvn = $apm_data['summa'] / (1 + ($apm_data['rekina_pvn'] / 100));
              $prov_summa = round($apm_summa_bez_pvn * ($apm_data['komisija'] / 100), 2);

              $prov_summa_ar_pvn = round($prov_summa + ($prov_summa * ($pvn_likme / 100)), 2);

              db_query("
                INSERT INTO `".DB_PREF."pardeveji_rekini_detalas` SET
                  pardeveja_rekina_id = ".$pardeveja_rekina_id.",
                  rekina_id = ".$apm_data['rekina_id'].",
                  rekina_maksajuma_id = ".$apm_id.",
                  summa = '".$prov_summa."',
                  pvn_likme = '".$pvn_likme."',
                  kopsumma = '".$prov_summa_ar_pvn."',
                  no_summas = '".$apm_data['summa']."',
                  prov_likme = '".$apm_data['komisija']."'
              ");

              $pamatsumma += $prov_summa;
            }
          }
        }
      }

      //  Provīzijas - avansa
      if (!empty($_POST['avansa'])) {

        foreach($_POST['avansa'] as $rek_id => $v) {

          if ($v['uz_rokas'] === '0') {

            $rek_data = $avansa_rekini[$rek_id];

            if(isset($rek_data['komisija'])) {  // Tikai tad ja iepriekš skaitot, tika iegūta un piefiksēta komisija

              $neapm_summa_bez_pvn = $rek_data['neapm_summa'] / (1 + ($rek_data['rekina_pvn'] / 100));
              $prov_summa = round($neapm_summa_bez_pvn * ($rek_data['komisija'] / 100), 2);

              $prov_summa_ar_pvn = round($prov_summa + ($prov_summa * ($pvn_likme / 100)), 2);

              db_query("
                INSERT INTO `".DB_PREF."pardeveji_rekini_detalas` SET
                  pardeveja_rekina_id = ".$pardeveja_rekina_id.",
                  rekina_id = ".$rek_id.",
                  rekina_maksajuma_id = null,
                  summa = '".$prov_summa."',
                  pvn_likme = '".$pvn_likme."',
                  kopsumma = '".$prov_summa_ar_pvn."',
                  no_summas = '".$rek_data['neapm_summa']."',
                  prov_likme = '".$rek_data['komisija']."'
              ");

              $pamatsumma += $prov_summa;
            }
          }
        }
      }

      //  Prēmijas - saņemtās
      if (!empty($_POST['p_apm'])) {

        foreach($_POST['p_apm'] as $rek_id => $v) {

          if ($v['uz_rokas'] === '0') {

            $apm_data = $p_apmaksas[$rek_id];

            $contract_premium = getContractPremium($apm_data, $premiums_cache, $darbinieka_id);

            //$apm_summa_bez_pvn = bcdiv($apm_data['kopsumma'], (1 + ($apm_data['rekina_pvn'] / 100)), 2);
            $apm_summa_bez_pvn = $apm_data['summa'];
            $prem_summa = round($apm_summa_bez_pvn * ($contract_premium / 100), 2);

            $prem_summa_ar_pvn = round($prem_summa + ($prem_summa * ($pvn_likme / 100)), 2);

            db_query("
              INSERT INTO `".DB_PREF."pardeveji_rekini_premijas_detalas` SET
                pardeveja_rekina_id = ".$pardeveja_rekina_id.",
                rekina_id = ".$rek_id.",
                summa = '".$prem_summa."',
                pvn_likme = '".$pvn_likme."',
                kopsumma = '".$prem_summa_ar_pvn."',
                no_summas = '".$apm_data['kopsumma']."',
                prem_likme = '".$contract_premium."'
            ");

            $pamatsumma += $prem_summa;

          }

        }

      }

      //  Prēmijas - avansa
      if (!empty($_POST['p_avansa'])) {

        foreach($_POST['p_avansa'] as $rek_id => $v) {

          if ($v['uz_rokas'] === '0') {

            $rek_data = $p_avansa_rekini[$rek_id];

            $contract_premium = getContractPremium($rek_data, $premiums_cache, $darbinieka_id);

            //$neapm_summa_bez_pvn = bcdiv($rek_data['kopsumma'], (1 + ($rek_data['rekina_pvn'] / 100)), 2);
            $neapm_summa_bez_pvn = $rek_data['summa'];
            $prem_summa = round($neapm_summa_bez_pvn * ($contract_premium / 100), 2);

            $prem_summa_ar_pvn = round($prem_summa + ($prem_summa * ($pvn_likme / 100)), 2);

            db_query("
              INSERT INTO `".DB_PREF."pardeveji_rekini_premijas_detalas` SET
                pardeveja_rekina_id = ".$pardeveja_rekina_id.",
                rekina_id = ".$rek_id.",
                summa = '".round($prem_summa, 2)."',
                pvn_likme = '".$pvn_likme."',
                kopsumma = '".$prem_summa_ar_pvn."',
                no_summas = '".$rek_data['kopsumma']."',
                prem_likme = '".$contract_premium."'
            ");

            $pamatsumma += $prem_summa;
          }
        }
      }

      //  Piemaksas
      if (!empty($_POST['piemaksas'])) {

        foreach($_POST['piemaksas'] as $i => $v) {

          $summa = round(str_replace(',', '.', $v['summa']), 2);
          $summa_ar_pvn = round(str_replace(',', '.', $v['kopsumma']), 2);

          if ($v['uz_rokas'] === '0' && !empty($summa)) {

            db_query("
              INSERT INTO `".DB_PREF."pardeveji_rekini_detalas` SET
                pardeveja_rekina_id = ".$pardeveja_rekina_id.",
                rekina_id = null,
                rekina_maksajuma_id = null,
                summa = '".$summa."',
                pvn_likme = '".$pvn_likme."',
                kopsumma = '".$summa_ar_pvn."',
                no_summas = 0,
                prov_likme = 0,
                paskaidrojums = '".esc($v['paskaidrojums'])."'
            ");

            $piemaksu_summa += $summa;
            $piemaksu_kopsumma += $summa_ar_pvn;

          }

        }

      }

      $rekina_summa = $pamatsumma + $piemaksu_summa;

      $rekina_summa_ar_pvn = $pamatsumma + ($pamatsumma * ($pvn_likme / 100));
      $rekina_summa_ar_pvn += $piemaksu_kopsumma;

      db_query("
        UPDATE `".DB_PREF."pardeveji_rekini` SET
          summa = '".$rekina_summa."',
          kopsumma = '".$rekina_summa_ar_pvn."'
        WHERE id = ".$pardeveja_rekina_id."
        LIMIT 1
      ");

      $log_data['rekins']['new'] = db_get_assoc(db_query("SELECT * FROM `".DB_PREF."pardeveji_rekini` WHERE `id` = " . $pardeveja_rekina_id));
      $log_data['rekins']['title'] = sprintf('Pievienots jauns rēķins %s pārdevējam %s', $log_data['rekins']['new']['rek_nr'], $darbinieks['vards']);

      $log_data['rekins_detalas']['new_m'] = db_get_assoc_all(db_query("SELECT * FROM `".DB_PREF."pardeveji_rekini_detalas` WHERE pardeveja_rekina_id = " . $pardeveja_rekina_id));
      $log_data['rekins_detalas']['title'] = sprintf('Rēķina %s detaļas', $log_data['rekins']['new']['rek_nr']);

    }

    log_add("laboja", $log_data);

    if (!empty($pdf_rekina_id)) {
      $refresh_url = '?c=darbinieki&a=gen_rek&rek_id=' . $pdf_rekina_id;
    } else {
      $refresh_url = '?c=darbinieki&a=labot&id=' . $darbinieka_id . '&subtab=norekini';
    }

    ?>
    <script type="text/javascript">
    window.parent.location.href = '<?= $refresh_url ?>';
    </script>
    <?
    die();

  }
}


include('modules/ligumi/rekinu_rekviziti.php');

?>

<? if (!empty($errors)) { ?>
  <ul class="errors">
    <? foreach($errors as $error) { ?>
      <li><?= $error ?></li>
    <? } ?>
  </ul>
<? } ?>

<script>
var searchlist_bonuss = [];

var sp_bonuss = create_filter_field_array(['s_gads', 's_ceturksnis', 's_menesis',
  /*'s_summa', 's_min_proc', 's_opt_proc', 's_virs_min', 's_virs_opt', 's_summa_min',
  's_summa_opt',*/ 's_kopsumma_wo_pvn', 's_pvn', 's_kopsumma'
]);

var searchlist_korekcija = [];

var sp_korekcija = create_filter_field_array(['s_kopsumma_wo_pvn', 's_pvn', 's_kopsumma']);


var searchlist_maks = [];

var sp_maks= create_filter_field_array([
  's_klienta_nosaukums', 's_rek_nr', 's_rekina_kopsumma', 's_summa', 's_datums',
  's_prov_percent', 's_prov_summa', 's_prov_pvn', 's_prov_kopsumma', 's_pardeveja_id'
]);

var searchlist_avansa = [];

var sp_avansa = create_filter_field_array([
  's_klienta_nosaukums', 's_rek_nr', 's_izr_datums', 's_rekina_kopsumma', 's_neapm_summa',
  's_prov_percent', 's_prov_summa', 's_prov_pvn', 's_prov_kopsumma', 's_pardeveja_id'
]);

var searchlist_p_maks = [];

var sp_p_maks= create_filter_field_array([
  's_klienta_nosaukums', 's_rek_nr', 's_rekina_kopsumma', 's_summa', 's_datums',
  's_prem_percent', 's_prem_summa', 's_prem_pvn', 's_prem_kopsumma', 's_ceturksnis'
]);

var searchlist_p_avansa = [];

var sp_p_avansa = create_filter_field_array([
  's_klienta_nosaukums', 's_rek_nr', 's_izr_datums', 's_rekina_kopsumma', 's_neapm_summa',
  's_prem_percent', 's_prem_summa', 's_prem_pvn', 's_prem_kopsumma', 's_ceturksnis'
]);
</script>


<table style="display:none;">
  <tr id="bonusi_filter">
    <th style="text-align: center;"><a onclick="selectAllInCol('bonusi', 'radio_n'); return false;" style="font-size: 1em; " href="#">Visi</a></th>
    <th style="text-align: center;"><a onclick="selectAllInCol('bonusi', 'uz_rokas'); return false;" style="font-size: 1em; " href="#">Visi</a></th>
    <th style="text-align: center;"><a onclick="selectAllInCol('bonusi', 'ieklaut_rekina'); return false;" style="font-size: 1em; " href="#">Visi</a></th>

    <th>
      <select class="search_field advonchange" searchclass="s_gads" meth="standart">
        <option value=""></option>
        <?php foreach(array_keys($bonusi) as $byear){ ?>
          <option value="<?=$byear;?>"><?=$byear;?></option>
        <?php } ?>
      </select>
      <a class="search_field" href="#">x</a>
    </th>

    <th>
      <select class="search_field advonchange" searchclass="s_ceturksnis" meth="standart">
        <option value=""></option>
        <?php foreach(range(1, 4) as $bquarter){ ?>
          <option value="<?=$bquarter;?>"><?=$_vars['quarters_roman'][$bquarter];?></option>
        <?php } ?>
      </select>
      <a class="search_field" href="#">x</a>
    </th>

    <th>
      <select class="search_field advonchange" searchclass="s_menesis" meth="standart">
        <option value=""></option>
        <?php foreach(range(1, 12) as $bmonth){ ?>
          <option value="<?=$bmonth;?>"><?=$_vars['menesi'][$bmonth];?></option>
        <?php } ?>
      </select>
      <a class="search_field" href="#">x</a>
    </th>

    <!--<th><span><input class="search_field" meth="coin" searchclass="s_summa" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <th><span><input class="search_field" meth="coin" searchclass="s_min_proc" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <th><span><input class="search_field" meth="coin" searchclass="s_opt_proc" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <th><span><input class="search_field" meth="coin" searchclass="s_virs_min" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <th><span><input class="search_field" meth="coin" searchclass="s_virs_opt" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <th><span><input class="search_field" meth="coin" searchclass="s_summa_min" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <th><span><input class="search_field" meth="coin" searchclass="s_summa_opt" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    -->
    <th><span><input class="search_field" meth="coin" searchclass="s_kopsumma_wo_pvn" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <th><span><input class="search_field" meth="coin" searchclass="s_pvn" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <th><span><input class="search_field" meth="coin" searchclass="s_kopsumma" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
  </tr>
</table>

<table style="display:none;">
  <tr id="korekcijas_filter">
    <th style="text-align: center;"><a onclick="selectAllInCol('korekcijas', 'radio_n'); return false;" style="font-size: 1em; " href="#">Visi</a></th>
    <th style="text-align: center;"><a onclick="selectAllInCol('korekcijas', 'uz_rokas'); return false;" style="font-size: 1em; " href="#">Visi</a></th>
    <th style="text-align: center;"><a onclick="selectAllInCol('korekcijas', 'ieklaut_rekina'); return false;" style="font-size: 1em; " href="#">Visi</a></th>
    <th></th>
    <th><span><input class="search_field" meth="coin" searchclass="s_kopsumma_wo_pvn" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <th><span><input class="search_field" meth="coin" searchclass="s_pvn" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <th><span><input class="search_field" meth="coin" searchclass="s_kopsumma" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
  </tr>
</table>

<table style="display:none;">

  <tr id="maksajumi_filter">

    <th style="text-align: center;"><a onclick="selectAllInCol('maksajumi', 'radio_n'); return false;" style="font-size: 1em; " href="#">Visi</a></th>
    <th style="text-align: center;"><a onclick="selectAllInCol('maksajumi', 'uz_rokas'); return false;" style="font-size: 1em; " href="#">Visi</a></th>
    <th style="text-align: center;"><a onclick="selectAllInCol('maksajumi', 'ieklaut_rekina'); return false;" style="font-size: 1em; " href="#">Visi</a></th>

    <th><span><input class="search_field" meth="standart" searchclass="s_klienta_nosaukums" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>

    <th>
      <select class="search_field advonchange" searchclass="s_pardeveja_id" multiple="multiple" meth="standart" style="width: 100px;">
        <option value=""></option>
        <option value="<?php echo $darbinieks_model->id;?>" selected="SELECTED"><?php echo $darbinieks_model->vards;?></option>
        <?php foreach((array)$darbinieks_model->getPadotie() as $padotais) { ?>
          <option value="<?php echo $padotais->id;?>" selected="SELECTED"><?php echo $padotais->vards;?></option>
        <?php } ?>
      </select>
    </th>

    <th><span><input class="search_field" meth="standart" searchclass="s_rek_nr" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <th><span><input class="search_field" meth="coin" searchclass="s_rekina_kopsumma" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <th><span><input class="search_field" meth="coin" searchclass="s_summa" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>

    <th>
      <span><input class="search_field kalendari" meth="from" searchclass="s_datums" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span><br>
      <span><input class="search_field kalendari" meth="to" searchclass="s_datums" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span>
    </th>

    <th><span><input class="search_field" meth="coin" searchclass="s_prov_percent" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <th><span><input class="search_field" meth="coin" searchclass="s_prov_summa" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <th><span><input class="search_field" meth="coin" searchclass="s_prov_pvn" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <th><span><input class="search_field" meth="coin" searchclass="s_prov_kopsumma" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>

  </tr>
</table>

<table style="display:none;">

  <tr id="avansa_summas_filter">

    <th style="text-align: center;"><a onclick="selectAllInCol('avansa_summas', 'radio_n'); return false;" style="font-size: 1em; " href="#">Visi</a></th>
    <th style="text-align: center;"><a onclick="selectAllInCol('avansa_summas', 'uz_rokas'); return false;" style="font-size: 1em; " href="#">Visi</a></th>
    <th style="text-align: center;"><a onclick="selectAllInCol('avansa_summas', 'ieklaut_rekina'); return false;" style="font-size: 1em; " href="#">Visi</a></th>

    <th><span><input class="search_field" meth="standart" searchclass="s_klienta_nosaukums" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>

    <th>
      <select class="search_field advonchange" searchclass="s_pardeveja_id" multiple="multiple" meth="standart" style="width: 100px;">
        <option value=""></option>
        <option value="<?php echo $darbinieks_model->id;?>" selected="SELECTED"><?php echo $darbinieks_model->vards;?></option>
        <?php foreach((array)$darbinieks_model->getPadotie() as $padotais) { ?>
          <option value="<?php echo $padotais->id;?>" selected="SELECTED"><?php echo $padotais->vards;?></option>
        <?php } ?>
      </select>
    </th>

    <th><span><input class="search_field" meth="standart" searchclass="s_rek_nr" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>

    <th>
      <span><input class="search_field kalendari" meth="from" searchclass="s_izr_datums" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span><br>
      <span><input class="search_field kalendari" meth="to" searchclass="s_izr_datums" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span>
    </th>

    <th><span><input class="search_field" meth="coin" searchclass="s_rekina_kopsumma" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <th><span><input class="search_field" meth="coin" searchclass="s_neapm_summa" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <th><span><input class="search_field" meth="coin" searchclass="s_prov_percent" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <th><span><input class="search_field" meth="coin" searchclass="s_prov_summa" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <th><span><input class="search_field" meth="coin" searchclass="s_prov_pvn" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <th><span><input class="search_field" meth="coin" searchclass="s_prov_kopsumma" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>

  </tr>
</table>

<!-- Prem --->
<table style="display:none;">

  <tr id="p_maksajumi_filter">

    <th style="text-align: center;"><a onclick="selectAllInCol('p_maksajumi', 'radio_n'); return false;" style="font-size: 1em; " href="#">Visi</a></th>
    <th style="text-align: center;"><a onclick="selectAllInCol('p_maksajumi', 'uz_rokas'); return false;" style="font-size: 1em; " href="#">Visi</a></th>
    <th style="text-align: center;"><a onclick="selectAllInCol('p_maksajumi', 'ieklaut_rekina'); return false;" style="font-size: 1em; " href="#">Visi</a></th>

    <th><span><input class="search_field" meth="standart" searchclass="s_klienta_nosaukums" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <th><span><input class="search_field" meth="standart" searchclass="s_rek_nr" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <th><span><input class="search_field" meth="coin" searchclass="s_rekina_kopsumma" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <th><span><input class="search_field" meth="coin" searchclass="s_summa" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>

    <th>
      <span><input class="search_field kalendari" meth="from" searchclass="s_datums" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span><br>
      <span><input class="search_field kalendari" meth="to" searchclass="s_datums" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span>
    </th>

    <th><span><input class="search_field" meth="coin" searchclass="s_prem_percent" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <th><span><input class="search_field" meth="coin" searchclass="s_prem_summa" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <th><span><input class="search_field" meth="coin" searchclass="s_prem_pvn" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <th><span><input class="search_field" meth="coin" searchclass="s_prem_kopsumma" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <th>
      <select class="search_field advonchange" searchclass="s_ceturksnis" meth="standart">
        <option value=""></option>
        <?php foreach($quarters as $quarter){ ?>
          <option value="<?=$quarter['year']. '-' .$quarter['quarter'];?>"><?=$quarter['year'] . ' ' . $_vars['quarters_roman'][$quarter['quarter']];?></option>
        <?php } ?>
      </select>
      <a class="search_field" href="#">x</a>
    </th>
  </tr>
</table>

<table style="display:none;">

  <tr id="p_avansa_summas_filter">

    <th style="text-align: center;"><a onclick="selectAllInCol('p_avansa_summas', 'radio_n'); return false;" style="font-size: 1em; " href="#">Visi</a></th>
    <th style="text-align: center;"><a onclick="selectAllInCol('p_avansa_summas', 'uz_rokas'); return false;" style="font-size: 1em; " href="#">Visi</a></th>
    <th style="text-align: center;"><a onclick="selectAllInCol('p_avansa_summas', 'ieklaut_rekina'); return false;" style="font-size: 1em; " href="#">Visi</a></th>

    <th><span><input class="search_field" meth="standart" searchclass="s_klienta_nosaukums" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <th><span><input class="search_field" meth="standart" searchclass="s_rek_nr" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>

    <th>
      <span><input class="search_field kalendari" meth="from" searchclass="s_izr_datums" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span><br>
      <span><input class="search_field kalendari" meth="to" searchclass="s_izr_datums" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span>
    </th>

    <th><span><input class="search_field" meth="coin" searchclass="s_rekina_kopsumma" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <th><span><input class="search_field" meth="coin" searchclass="s_neapm_summa" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <th><span><input class="search_field" meth="coin" searchclass="s_prem_percent" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <th><span><input class="search_field" meth="coin" searchclass="s_prem_summa" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <th><span><input class="search_field" meth="coin" searchclass="s_prem_pvn" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <th><span><input class="search_field" meth="coin" searchclass="s_prem_kopsumma" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <th>
      <select class="search_field advonchange" searchclass="s_ceturksnis" meth="standart">
        <option value=""></option>
        <?php foreach($quarters as $quarter){ ?>
          <option value="<?=$quarter['year']. '-' .$quarter['quarter'];?>"><?=$quarter['year'] . ' ' . $_vars['quarters_roman'][$quarter['quarter']];?></option>
        <?php } ?>
      </select>
      <a class="search_field" href="#">x</a>
    </th>
  </tr>
</table>

<form action="" method="post">

  <?php if(!empty($bonusi)) { ?>
  <h2 style="margin-bottom: 0;">Bonusi</h2>
  <table id="bonusi" class="data" style="width: 99%;">
    <thead>
      <tr class="last header">
        <th align="center">Ne-<br />iekļaut</th>
        <th align="center">Uz<br />rokas</th>
        <th align="center">Iekļaut<br />rēķinā</th>
        <th>Gads</th>
        <th>Ceturksnis</th>
        <th>Mēnesis</th>
        <!--
        <th>Sasniegtais Ls</th>
        <th>Proc. min. %</th>
        <th>Proc. opt. %</th>
        <th>Virs min. Ls</th>
        <th>Virs opt. Ls</th>
        <th>Summa min. Ls</th>
        <th>Summa opt. Ls</th>
        -->
        <th>Summa EUR (bez PVN)</th>
        <th>PVN&nbsp;EUR</th>
        <th>Summa<br />kopā&nbsp;EUR<br />(ar PVN)</th>
      </tr>
    </thead>

    <tbody class="main">
      <? $inid = -1; ?>

      <?php
      $bonusi_d1 = array(); //  Izskrienam cauri un izveidojam sarakstu (viendimensijas masīvs) ar derīgiem bonusiem
      foreach($bonusi as $gads => $bstats) {
        //var_dump('ibnb');
        $rows = cleanup_bonuss($bstats, $gads);
        //var_dump($rows);
        foreach($rows as $row) {
          $bonusi_d1[] = $row;
        }
      }

      foreach($bonusi_d1 as $bonuss) {
        $inid ++;
        $month_quarter = $bonuss['month'] ? ceil($bonuss['month']/3) : null;
        ?>
        <tr id="bonuss_tr<?=$inid;?>" class="tr_bonuss">
          <td align="center" class="radio radio_n"><input checked="checked" type="radio" name="bonuss[<?= $bonuss['bid'] ?>][uz_rokas]" value="n" /></td>
          <td align="center" class="radio uz_rokas"><input type="radio" name="bonuss[<?= $bonuss['bid'] ?>][uz_rokas]" value="1" /></td>
          <td align="center" class="radio ieklaut_rekina"><input type="radio" name="bonuss[<?= $bonuss['bid'] ?>][uz_rokas]" value="0" /></td>
          <td class="c"><?= $bonuss['year'] ?></td>
          <td class="c"><?= $bonuss['quarter'] ? $_vars['quarters_roman'][$bonuss['quarter']] : ($month_quarter ?  $_vars['quarters_roman'][$month_quarter] : ''); ?></td>
          <td class="c"><?= $bonuss['month'] ? $_vars['menesi'][$bonuss['month']] : ($bonuss['quarter'] ? 'ceturksnis' : 'gads'); ?></td>
          <!--<td class="r"><?= $bonuss['summa'] ?></td>
          <td class="c"><?= $bonuss['min_proc'] ?></td>
          <td class="c"><?= $bonuss['opt_proc'] ?></td>
          <td class="r"><?= format_currency($bonuss['virs_min']); ?></td>
          <td class="r"><?= format_currency($bonuss['virs_opt']); ?></td>
          <td class="r"><?= format_currency($bonuss['summa_min']); ?></td>
          <td class="r"><?= format_currency($bonuss['summa_opt']); ?></td>    -->
          <td class="r bonuss_kopsumma_wo_pvn"><?= format_currency($bonuss['kopsumma_wo_pvn']); ?></td>
          <td class="r bonuss_pvn"></td>
          <td class="r last bonuss_kopsumma"></td>
          <td style="display: none;">
            <input type="hidden" class="row_id" value="<?=$inid;?>" />
            <script type="text/javascript">
             searchlist_bonuss[<?=$inid;?>] = '<?=addslashes(mb_strtoupper($bonuss['bid']));?>';
             sp_bonuss['s_gads'][<?=$inid;?>] = '<?=addslashes(mb_strtoupper($bonuss['year']));?>';
             sp_bonuss['s_ceturksnis'][<?=$inid;?>] = '<?=addslashes(mb_strtoupper(($bonuss['quarter'] ? $bonuss['quarter'] : ($month_quarter ? $month_quarter : null))));?>';
             sp_bonuss['s_menesis'][<?=$inid;?>] = '<?=addslashes(mb_strtoupper($bonuss['month']));?>';
             /*sp_bonuss['s_summa'][<?=$inid;?>] = '<?=addslashes(mb_strtoupper($bonuss['summa']));?>';
             sp_bonuss['s_min_proc'][<?=$inid;?>] = '<?=addslashes(mb_strtoupper($bonuss['min_proc']));?>';
             sp_bonuss['s_opt_proc'][<?=$inid;?>] = '<?=addslashes(mb_strtoupper($bonuss['opt_proc']));?>';
             sp_bonuss['s_virs_min'][<?=$inid;?>] = '<?=addslashes(mb_strtoupper($bonuss['virs_min']));?>';
             sp_bonuss['s_virs_opt'][<?=$inid;?>] = '<?=addslashes(mb_strtoupper($bonuss['virs_opt']));?>';
             sp_bonuss['s_summa_min'][<?=$inid;?>] = '<?=addslashes(mb_strtoupper($bonuss['summa_min']));?>';
             sp_bonuss['s_summa_opt'][<?=$inid;?>] = '<?=addslashes(mb_strtoupper($bonuss['summa_opt']));?>'; */
             sp_bonuss['s_kopsumma_wo_pvn'][<?=$inid;?>] = '<?=addslashes(mb_strtoupper($bonuss['kopsumma_wo_pvn']));?>';
             sp_bonuss['s_pvn'][<?=$inid;?>] = 0;
             sp_bonuss['s_kopsumma'][<?=$inid;?>] = 0;

            </script>
          </td>
        </tr>

      <?php } ?>
    </tbody>

      <tr>
        <td align="right" colspan="6"><strong>Kopā:</strong></td>
        <!--<td class="r" id="bonuss_summa_total">0</td>
        <td></td>
        <td></td>
        <td class="r" id="bonuss_virs_min_total">0</td>
        <td class="r" id="bonuss_virs_opt_total">0</td>
        <td class="r" id="bonuss_summa_min_total">0</td>
        <td class="r" id="bonuss_summa_opt_total">0</td>       -->
        <td class="r" id="bonuss_kopsumma_wo_pvn_total">0</td>
        <td class="r" id="bonuss_pvn_total">0</td>
        <td class="r" id="bonuss_kopsumma_total">0</td>
      </tr>
  </table>

  <?php } ?>

  <?php

  // Korekcijas

  if(!empty($korekcijas)) { ?>
  <h2 style="margin-bottom: 0;">Korekcijas</h2>
  <table id="korekcijas" class="data" style="width: 99%;">
    <thead>
      <tr class="last header">
        <th align="center">Ne-<br />iekļaut</th>
        <th align="center">Uz<br />rokas</th>
        <th align="center">Iekļaut<br />rēķinā</th>
        <th></th>
        <th>Summa EUR (bez PVN)</th>
        <th>PVN&nbsp;EUR</th>
        <th>Summa<br />kopā&nbsp;EUR<br />(ar PVN)</th>
      </tr>
    </thead>

    <tbody class="main">
      <? $inid = -1; ?>

      <?php

      foreach($korekcijas as $i => $korekcija) {
        $inid ++;
        $korekcija['nosaukums']  = 'Tekošā korekcija par iepriekšējiem periodiem';
        ?>
        <tr id="korekcija_tr<?=$inid;?>" class="tr_korekcija">
          <td align="center" class="radio radio_n"><input checked="checked" type="radio" name="korekcija[<?= $i ?>][uz_rokas]" value="n" /></td>
          <td align="center" class="radio uz_rokas"><input type="radio" name="korekcija[<?= $i ?>][uz_rokas]" value="1" /></td>
          <td align="center" class="radio ieklaut_rekina"><input type="radio" name="korekcija[<?= $i ?>][uz_rokas]" value="0" /></td>
          <td class="c"><?php echo $korekcija['nosaukums'];?></td>
          <td class="r korekcija_kopsumma_wo_pvn">
            <?= format_currency($korekcija['kopsumma_wo_pvn']); ?>
            <? if (bccomp($korekcija['pvn'], '0', 2) == 0) { ?>
              <span class="bez_pvn_rek_ico" style="margin-top: 2px;">!</span>
            <? } ?>
          </td>
          <td class="r korekcija_pvn"></td>
          <td class="r last korekcija_kopsumma"></td>
          <td style="display: none;">
            <input type="hidden" class="row_id" value="<?=$inid;?>" />
            <script type="text/javascript">
             searchlist_korekcija[<?=$inid;?>] = '<?=$i;?>';
             sp_korekcija['s_kopsumma_wo_pvn'][<?=$inid;?>] = '<?=addslashes(round($korekcija['kopsumma_wo_pvn'], 2));?>';
             sp_korekcija['s_pvn'][<?=$inid;?>] = 0;
             sp_korekcija['s_kopsumma'][<?=$inid;?>] = 0;

            </script>
          </td>
        </tr>

      <?php } ?>
    </tbody>

      <tr>
        <td align="right" colspan="4"><strong>Kopā:</strong></td>
        <td class="r" id="korekcija_kopsumma_wo_pvn_total">0</td>
        <td class="r" id="korekcija_pvn_total">0</td>
        <td class="r" id="korekcija_kopsumma_total">0</td>
      </tr>
  </table>

  <?php } ?>

  <h2 style="margin-bottom: 0;">Saņemtās provīzijas</h2>

  <table id="maksajumi" class="data" style="width: 99%;">

    <thead>

      <tr class="last header">
        <th align="center">Ne-<br />iekļaut</th>
        <th align="center">Uz<br />rokas</th>
        <th align="center">Iekļaut<br />rēķinā</th>
        <th>Klients</th>
        <th>Līguma slēdzējs</th>
        <th>Rēķina nr.</th>
        <th>Rēķina<br />summa&nbsp;EUR<br />(ar PVN)</th>
        <th>Saņemtā<br />samaksa&nbsp;EUR</th>
        <th>Apmaksas<br />datums</th>
        <th>Provīzija %</th>
        <th>Provīzijas<br />summa&nbsp;EUR<br />(bez PVN)</th>
        <th>PVN<br />(provīzija)&nbsp;EUR</th>
        <th>Summa<br />kopā&nbsp;EUR<br />(ar PVN)</th>
      </tr>

    </thead>

    <tbody class="main">

      <? $inid = -1; ?>

      <? foreach($apmaksas as $apmaksa) { ?>

        <? $inid ++;  ?>

        <?
        $komisija = getKomisija($apmaksa, $darbinieks_model);

        if(!$komisija) {continue;}

        $apm_summa_bez_pvn = $apmaksa['summa'] / (1 + ($apmaksa['rekina_pvn'] / 100));
        $prov_summa = $apm_summa_bez_pvn * ($komisija / 100);
        ?>

        <tr id="maks_tr<?=$inid;?>" class="tr_maks">
          <td align="center" class="radio radio_n"><input checked="checked" type="radio" name="apm[<?= $apmaksa['id'] ?>][uz_rokas]" value="n" /></td>
          <td align="center" class="radio uz_rokas"><input type="radio" name="apm[<?= $apmaksa['id'] ?>][uz_rokas]" value="1" /></td>
          <td align="center" class="radio ieklaut_rekina"><input type="radio" name="apm[<?= $apmaksa['id'] ?>][uz_rokas]" value="0" /></td>
          <td><?= $apmaksa['klienta_nosaukums'] ?></td>
          <td><?php echo Pardevejs::model()->findByPk($apmaksa['pardeveja_id'])->vards;?></td>
          <td><?= $apmaksa['rek_nr'] ?></td>
          <td class="r"><?= $apmaksa['rekina_kopsumma'] ?></td>
          <td class="r"><?= $apmaksa['summa'] ?></td>
          <td class="c"><?= date("d.m.Y", strtotime($apmaksa['datums'])) ?></td>
          <td class="c"><?= round($komisija, 2) ?>%</td>
          <td class="r prov_summa"><?= format_currency($prov_summa) ?></td>
          <td class="r">
            <span class="prov_pvn" style="float: right; margin-left: 5px;"></span>
            <? if (floatval($apmaksa['rekina_pvn']) == 0) { ?>
              <span class="bez_pvn_rek_ico" style="margin-top: 2px;">!</span>
            <? } ?>
          </td>
          <td class="r last prov_summa_ar_pvn"></td>
          <td style="display: none;">
            <input type="hidden" class="row_id" value="<?=$inid;?>" />
            <script>

              searchlist_maks[<?=$inid;?>] = '<?=addslashes(mb_strtoupper($apmaksa['rek_nr']));?>';

              sp_maks['s_klienta_nosaukums'][<?=$inid;?>] = '<?=addslashes(mb_strtoupper($apmaksa['klienta_nosaukums']));?>';
              sp_maks['s_pardeveja_id'][<?=$inid;?>] = '<?=$apmaksa['pardeveja_id'];?>';
              sp_maks['s_rek_nr'][<?=$inid;?>] = '<?=addslashes(mb_strtoupper($apmaksa['rek_nr']));?>';
              sp_maks['s_rekina_kopsumma'][<?=$inid;?>] = '<?=!empty($apmaksa['rekina_kopsumma']) ? $apmaksa['rekina_kopsumma'] : "" ?>';
              sp_maks['s_summa'][<?=$inid;?>] = '<?=!empty($apmaksa['summa']) ? $apmaksa['summa'] : "" ?>';
              sp_maks['s_datums'][<?=$inid;?>] = <?= !empty($apmaksa['datums']) ? date('Ymd', strtotime($apmaksa['datums'])) : 0 ?>;
              sp_maks['s_prov_percent'][<?=$inid;?>] = '<?=$komisija;?>';
              sp_maks['s_prov_summa'][<?=$inid;?>] = '<?=!empty($prov_summa) ? $prov_summa : "" ?>';
              sp_maks['s_prov_pvn'][<?=$inid;?>] = 0;
              sp_maks['s_prov_kopsumma'][<?=$inid;?>] = 0;

            </script>
          </td>
        </tr>

      <? } ?>

    </tbody>

    <tr>

      <td align="right" colspan="6"><strong>Kopā:</strong></td>
      <td class="r" id="maks_rekinu_summa">0</td>
      <td class="r" id="maks_sanemta_summa">0</td>
      <td></td>
      <td></td>
      <td class="r" id="maks_prov_summa">0</td>
      <td class="r" id="maks_prov_pvn">0</td>
      <td class="r" id="maks_prov_kopsumma">0</td>

    </tr>

  </table>

  <h2 style="margin-bottom: 0;">Avansa provīzijas</h2>

  <table id="avansa_summas" class="data" style="width: 99%;">

    <thead>

      <tr class="last header">
        <th align="center">Ne-<br />iekļaut</th>
        <th align="center">Uz<br />rokas</th>
        <th align="center">Iekļaut<br />rēķinā</th>
        <th>Klients</th>
        <th>Līguma slēdzējs</th>
        <th>Rēķina&nbsp;nr.</th>
        <th>Izrakstīšanas<br />datums</th>
        <th>Rēķina<br />summa&nbsp;EUR<br />(ar PVN)</th>
        <th>Neapmaksātā<br />summa&nbsp;EUR</th>
        <th>Provīzija %</th>
        <th>Provīzijas<br />summa&nbsp;EUR<br />(bez PVN)</th>
        <th>PVN<br />(provīzija)&nbsp;EUR</th>
        <th>Summa<br />kopā&nbsp;EUR<br />(ar PVN)</th>
      </tr>

    </thead>

    <tbody class="main">

      <? $inid = -1; ?>

      <? foreach($avansa_rekini as $avansa) { ?>

        <? $inid ++;  ?>

        <?
        $komisija = getKomisija($avansa, $darbinieks_model);

        if(!$komisija) {continue;}

        $neapm_summa_bez_pvn = $avansa['neapm_summa'] / (1 + ($avansa['rekina_pvn'] / 100));
        $prov_summa = $neapm_summa_bez_pvn * ($komisija / 100);
        ?>

        <tr id="avansa_tr<?=$inid;?>" class="tr_avansa">
          <td align="center" class="radio radio_n"><input checked="checked" type="radio" name="avansa[<?= $avansa['id'] ?>][uz_rokas]" value="n" /></td>
          <td align="center" class="radio uz_rokas"><input type="radio" name="avansa[<?= $avansa['id'] ?>][uz_rokas]" value="1" /></td>
          <td align="center" class="radio ieklaut_rekina"><input type="radio" name="avansa[<?= $avansa['id'] ?>][uz_rokas]" value="0" /></td>
          <td><?= $avansa['klienta_nosaukums'] ?></td>
          <td><?php echo Pardevejs::model()->findByPk($avansa['pardeveja_id'])->vards;?></td>
          <td><?= $avansa['rek_nr'] ?></td>
          <td class="c"><?= date("d.m.Y", strtotime($avansa['izr_datums'])) ?></td>
          <td class="r"><?= $avansa['kopsumma'] ?></td>
          <td class="r"><?= $avansa['neapm_summa'] ?></td>
          <td class="c"><?= round($komisija, 2) ?>%</td>
          <td class="r prov_summa"><?= format_currency($prov_summa) ?></td>
          <td class="r">
            <span class="prov_pvn" style="float: right; margin-left: 5px;"></span>
            <? if (floatval($avansa['rekina_pvn']) == 0) { ?>
              <span class="bez_pvn_rek_ico" style="margin-top: 2px;">!</span>
            <? } ?>
          </td>
          <td class="r last prov_summa_ar_pvn"></td>
          <td style="display: none;">
            <input type="hidden" class="row_id" value="<?=$inid;?>" />
            <script>

              searchlist_avansa[<?=$inid;?>] = '<?=addslashes(mb_strtoupper($avansa['rek_nr']));?>';

              sp_avansa['s_klienta_nosaukums'][<?=$inid;?>] = '<?=addslashes(mb_strtoupper($avansa['klienta_nosaukums']));?>';
              sp_avansa['s_pardeveja_id'][<?=$inid;?>] = '<?=$avansa['pardeveja_id'];?>';
              sp_avansa['s_rek_nr'][<?=$inid;?>] = '<?=addslashes(mb_strtoupper($avansa['rek_nr']));?>';
              sp_avansa['s_izr_datums'][<?=$inid;?>] = <?= !empty($avansa['izr_datums']) ? date('Ymd', strtotime($avansa['izr_datums'])) : 0 ?>;
              sp_avansa['s_rekina_kopsumma'][<?=$inid;?>] = '<?=!empty($avansa['kopsumma']) ? $avansa['kopsumma'] : "" ?>';
              sp_avansa['s_neapm_summa'][<?=$inid;?>] = '<?=!empty($avansa['neapm_summa']) ? $avansa['neapm_summa'] : "" ?>';
              sp_avansa['s_prov_percent'][<?=$inid;?>] = '<?=$komisija;?>';
              sp_avansa['s_prov_summa'][<?=$inid;?>] = '<?=!empty($prov_summa) ? $prov_summa : "" ?>';
              sp_avansa['s_prov_pvn'][<?=$inid;?>] = 0;
              sp_avansa['s_prov_kopsumma'][<?=$inid;?>] = 0;
            </script>
          </td>
        </tr>

      <? } ?>

    </tbody>

    <tr>

      <td align="right" colspan="7"><strong>Kopā:</strong></td>
      <td class="r" id="avansa_rekinu_summa">0</td>
      <td class="r" id="avansa_neapm_summa">0</td>
      <td></td>
      <td class="r" id="avansa_prov_summa">0</td>
      <td class="r" id="avansa_prov_pvn">0</td>
      <td class="r" id="avansa_prov_kopsumma">0</td>

    </tr>

  </table>

  <h2 style="margin-bottom: 0;">Saņemtās prēmijas</h2>

  <table id="p_maksajumi" class="data" style="width: 99%;">

    <thead>

      <tr class="last header">
        <th align="center">Ne-<br />iekļaut</th>
        <th align="center">Uz<br />rokas</th>
        <th align="center">Iekļaut<br />rēķinā</th>
        <th>Klients</th>
        <th>Rēķina nr.</th>
        <th>Rēķina<br />summa&nbsp;EUR<br />(ar PVN)</th>
        <th>Saņemtā<br />samaksa&nbsp;EUR</th>
        <th>Apmaksas<br />datums</th>
        <th>Prēmija %</th>
        <th>Prēmijas<br />summa&nbsp;EUR<br />(bez PVN)</th>
        <th>PVN<br />(prēmija)&nbsp;EUR</th>
        <th>Summa<br />kopā&nbsp;EUR<br />(ar PVN)</th>
        <th>Ceturksnis</th>
      </tr>

    </thead>

    <tbody class="main">

      <? $inid = -1; ?>

      <? foreach($p_apmaksas as $apmaksa) { ?>

        <? $inid ++;  ?>

        <?
        list($year, $quarter) = getYearQuarter($apmaksa);
        //var_dump($premiums_cache);
        $contract_premium = getContractPremium($apmaksa, $premiums_cache, $darbinieka_id);

        //$apm_summa_bez_pvn = bcdiv($apmaksa['kopsumma'], (1 + ($apmaksa['rekina_pvn'] / 100)), 2);
        $apm_summa_bez_pvn = $apmaksa['summa'];
        $prem_summa = $apm_summa_bez_pvn * ($contract_premium / 100);
        ?>

        <tr id="p_maks_tr<?=$inid;?>" class="tr_p_maks">
          <td align="center" class="radio radio_n"><input checked="checked" type="radio" name="p_apm[<?= $apmaksa['id'] ?>][uz_rokas]" value="n" /></td>
          <td align="center" class="radio uz_rokas"><input type="radio" name="p_apm[<?= $apmaksa['id'] ?>][uz_rokas]" value="1" /></td>
          <td align="center" class="radio ieklaut_rekina"><input type="radio" name="p_apm[<?= $apmaksa['id'] ?>][uz_rokas]" value="0" /></td>
          <td><?= $apmaksa['klienta_nosaukums'] ?></td>
          <td><?= $apmaksa['rek_nr'] ?></td>
          <td class="r"><?= $apmaksa['rekina_kopsumma'] ?></td>
          <td class="r"><?= $apmaksa['kopsumma'] ?></td>
          <td class="c"><?= date("d.m.Y", strtotime($apmaksa['apm_datums'])) ?></td>
          <td class="c"><?= round($contract_premium, 2) ?>%</td>
          <td class="r prem_summa"><?= format_currency($prem_summa) ?></td>
          <td class="r">
            <span class="prem_pvn" style="float: right; margin-left: 5px;"></span>
            <? if (floatval($apmaksa['rekina_pvn']) == 0) { ?>
              <span class="bez_pvn_rek_ico" style="margin-top: 2px;">!</span>
            <? } ?>
          </td>
          <td class="r prem_summa_ar_pvn"></td>
          <td class="c last"><?=$year . '.g ' . $_vars['quarters_roman'][$quarter];?></td>
          <td style="display: none;">
            <input type="hidden" class="row_id" value="<?=$inid;?>" />
            <input type="hidden" name="p_apm[<?= $apmaksa['id']; ?>][rekin_apm_termins]" value="<?=$apmaksa['rekina_apm_termins'];?>" />
            <script>

              searchlist_p_maks[<?=$inid;?>] = '<?=addslashes(mb_strtoupper($apmaksa['rek_nr']));?>';

              sp_p_maks['s_klienta_nosaukums'][<?=$inid;?>] = '<?=addslashes(mb_strtoupper($apmaksa['klienta_nosaukums']));?>';
              sp_p_maks['s_rek_nr'][<?=$inid;?>] = '<?=addslashes(mb_strtoupper($apmaksa['rek_nr']));?>';
              sp_p_maks['s_rekina_kopsumma'][<?=$inid;?>] = '<?=!empty($apmaksa['rekina_kopsumma']) ? $apmaksa['rekina_kopsumma'] : "" ?>';
              sp_p_maks['s_summa'][<?=$inid;?>] = '<?=!empty($apmaksa['kopsumma']) ? $apmaksa['kopsumma'] : "" ?>';
              sp_p_maks['s_datums'][<?=$inid;?>] = <?= !empty($apmaksa['apm_termins']) ? date('Ymd', strtotime($apmaksa['apm_termins'])) : 0 ?>;
              sp_p_maks['s_prem_percent'][<?=$inid;?>] = '<?=!empty($contract_premium) ? $contract_premium : "" ?>';
              sp_p_maks['s_prem_summa'][<?=$inid;?>] = '<?=!empty($prem_summa) ? $prem_summa : "" ?>';
              sp_p_maks['s_prem_pvn'][<?=$inid;?>] = 0;
              sp_p_maks['s_prem_kopsumma'][<?=$inid;?>] = 0;
              sp_p_maks['s_ceturksnis'][<?=$inid;?>] = '<?=$year. '-' .$quarter;?>';

            </script>
          </td>
        </tr>

      <? } ?>

    </tbody>

    <tr>

      <td align="right" colspan="5"><strong>Kopā:</strong></td>
      <td class="r" id="p_maks_rekinu_summa">0</td>
      <td class="r" id="p_maks_sanemta_summa">0</td>
      <td></td>
      <td></td>
      <td class="r" id="p_maks_prem_summa">0</td>
      <td class="r" id="p_maks_prem_pvn">0</td>
      <td class="r" id="p_maks_prem_kopsumma">0</td>
       <td></td>
    </tr>

  </table>

  <h2 style="margin-bottom: 0;">Avansa prēmijas</h2>

  <table id="p_avansa_summas" class="data" style="width: 99%;">

    <thead>

      <tr class="last header">
        <th align="center">Ne-<br />iekļaut</th>
        <th align="center">Uz<br />rokas</th>
        <th align="center">Iekļaut<br />rēķinā</th>
        <th>Klients</th>
        <th>Rēķina&nbsp;nr.</th>
        <th>Izrakstīšanas<br />datums</th>
        <th>Rēķina<br />summa&nbsp;EUR<br />(ar PVN)</th>
        <th>Neapmaksātā<br />summa&nbsp;EUR</th>
        <th>Prēmija %</th>
        <th>Prēmijas<br />summa&nbsp;EUR<br />(bez PVN)</th>
        <th>PVN<br />(prēmija)&nbsp;EUR</th>
        <th>Summa<br />kopā&nbsp;EUR<br />(ar PVN)</th>
        <th>Ceturksnis</th>
      </tr>

    </thead>

    <tbody class="main">

      <? $inid = -1; ?>

      <? foreach($p_avansa_rekini as $avansa) { ?>

        <? $inid ++;  ?>

        <?
        list($year, $quarter) = getYearQuarter($avansa);

        $contract_premium = getContractPremium($avansa, $premiums_cache, $darbinieka_id);

        //$neapm_summa_bez_pvn = bcdiv($avansa['kopsumma'], (1 + ($avansa['rekina_pvn'] / 100)), 2);
        $neapm_summa_bez_pvn = $avansa['summa'];
        $prem_summa = $neapm_summa_bez_pvn * ($contract_premium / 100);
        ?>

        <tr id="p_avansa_tr<?=$inid;?>" class="tr_p_avansa">
          <td align="center" class="radio radio_n"><input checked="checked" type="radio" name="p_avansa[<?= $avansa['id'] ?>][uz_rokas]" value="n" /></td>
          <td align="center" class="radio uz_rokas"><input type="radio" name="p_avansa[<?= $avansa['id'] ?>][uz_rokas]" value="1" /></td>
          <td align="center" class="radio ieklaut_rekina"><input type="radio" name="p_avansa[<?= $avansa['id'] ?>][uz_rokas]" value="0" /></td>
          <td><?= $avansa['klienta_nosaukums'] ?></td>
          <td><?= $avansa['rek_nr'] ?></td>
          <td class="c"><?= date("d.m.Y", strtotime($avansa['izr_datums'])) ?></td>
          <td class="r"><?= $avansa['kopsumma'] ?></td>
          <td class="r"><?= $avansa['kopsumma'] ?></td>
          <td class="c"><?= round($contract_premium, 2) ?>%</td>
          <td class="r prem_summa"><?= format_currency($prem_summa) ?></td>
          <td class="r">
            <span class="prem_pvn" style="float: right; margin-left: 5px;"></span>
            <? if (floatval($avansa['rekina_pvn']) == 0) { ?>
              <span class="bez_pvn_rek_ico" style="margin-top: 2px;">!</span>
            <? } ?>
          </td>
          <td class="r prem_summa_ar_pvn"></td>
          <td class="c last"><?=$year . '.g ' . $_vars['quarters_roman'][$quarter];?></td>
          <td style="display: none;">
            <input type="hidden" class="row_id" value="<?=$inid;?>" />
            <input type="hidden" name="p_avansa[<?= $avansa['id'] ?>][rekina_apm_termins]" value="<?=$avansa['rekina_apm_termins'];?>" />
            <script>

              searchlist_p_avansa[<?=$inid;?>] = '<?=addslashes(mb_strtoupper($avansa['rek_nr']));?>';

              sp_p_avansa['s_klienta_nosaukums'][<?=$inid;?>] = '<?=addslashes(mb_strtoupper($avansa['klienta_nosaukums']));?>';
              sp_p_avansa['s_rek_nr'][<?=$inid;?>] = '<?=addslashes(mb_strtoupper($avansa['rek_nr']));?>';
              sp_p_avansa['s_izr_datums'][<?=$inid;?>] = <?= !empty($avansa['izr_datums']) ? date('Ymd', strtotime($avansa['izr_datums'])) : 0 ?>;
              sp_p_avansa['s_rekina_kopsumma'][<?=$inid;?>] = '<?=!empty($avansa['kopsumma']) ? $avansa['kopsumma'] : "" ?>';
              sp_p_avansa['s_neapm_summa'][<?=$inid;?>] = '<?=!empty($avansa['kopsumma']) ? $avansa['kopsumma'] : "" ?>';
              sp_p_avansa['s_prem_percent'][<?=$inid;?>] = '<?=!empty($contract_premium) ? $contract_premium : "" ?>';
              sp_p_avansa['s_prem_summa'][<?=$inid;?>] = '<?=!empty($prem_summa) ? $prem_summa : "" ?>';
              sp_p_avansa['s_prem_pvn'][<?=$inid;?>] = 0;
              sp_p_avansa['s_prem_kopsumma'][<?=$inid;?>] = 0;
              sp_p_avansa['s_ceturksnis'][<?=$inid;?>] = '<?=$year. '-' .$quarter;?>';
            </script>
          </td>
        </tr>

      <? } ?>

    </tbody>

    <tr>

      <td align="right" colspan="6"><strong>Kopā:</strong></td>
      <td class="r" id="p_avansa_rekinu_summa">0</td>
      <td class="r" id="p_avansa_neapm_summa">0</td>
      <td></td>
      <td class="r" id="p_avansa_prem_summa">0</td>
      <td class="r" id="p_avansa_prem_pvn">0</td>
      <td class="r" id="p_avansa_prem_kopsumma">0</td>
      <td></td>
    </tr>

  </table>

  <h2 style="margin-bottom: 0;">Piemaksas</h2>

  <table id="piemaksas" class="data" style="width: 99%;">

    <thead>

      <tr class="last header">
        <th style="width: 3.2em;" align="center">Ne-<br />iekļaut</th>
        <th style="width: 3.2em;" align="center">Uz<br />rokas</th>
        <th style="width: 3.2em;" align="center">Iekļaut<br />rēķinā</th>
        <th>Paskaidrojums</th>
        <th style="width: 6em;">Summa EUR<br />(bez PVN)</th>
        <th style="width: 6em;">PVN EUR</th>
        <th style="width: 6em;">Summa<br />kopā&nbsp;EUR<br />(ar PVN)</th>
      </tr>

    </thead>

    <tbody class="main">

      <tr class="tr_piem">
        <td align="center" class="radio radio_n"><input checked="checked" type="radio" name="piemaksas[0][uz_rokas]" value="n" /></td>
        <td align="center" class="radio uz_rokas"><input type="radio" name="piemaksas[0][uz_rokas]" value="1" /></td>
        <td align="center" class="radio ieklaut_rekina"><input type="radio" name="piemaksas[0][uz_rokas]" value="0" /></td>
        <td><input class="paskaidrojums" type="text" name="piemaksas[0][paskaidrojums]" value="" /></td>
        <td><input class="summa" type="text" name="piemaksas[0][summa]" value="" style="width: 4em; text-align: right;" /></td>
        <td class="r pvn"></td>
        <td class="last"><input style="text-align: right;" class="summa_ar_pvn" type="text" name="piemaksas[0][kopsumma]" value="" style="width: 4em;" /></td>
      </tr>

    </tbody>

  </table>

  <p>PVN likme rēķinam: <input id="pvn_likme" type="text" name="pvn_likme" value="<?= isset($_POST['pvn_likme']) ? $_POST['pvn_likme'] : $darbinieks['pvn_likme'] ?>" style="width: 80px;" /></p>

  <p>
    Kopējā summa uz rokas: <strong id="kopsumma_uz_rokas"></strong><br />
    Kopējā summa rēķinā: <strong id="kopsumma_rek"></strong><br />
    Kopējā summa: <strong id="kopsumma"></strong>
  </p>

  <p>
    <div style="margin-bottom: 5px;">Rēķ. numurs: <input id="rek_nr" type="text" name="rek_nr" value="<?= isset($_POST['rek_nr']) ? $_POST['rek_nr'] : get_darb_rek_next_no($rek_prefix, $darbinieka_id) ?>" style="width: 80px;" /> <span class="info">Šis numurs netiks izmantots rēķinam, kas tiek izmaksāts uz rokas.</span></div>
    <div>Izrakst. datums: <input id="izr_datums" type="text" name="izr_datums" class="showcalendar" value="<?= isset($_POST['izr_datums']) ? $_POST['izr_datums'] : date('d.m.Y') ?>" style="width: 80px;" /></div>
  </p>
	
	<p>
		<? /*<input type="radio" name="from" value="infomedia" id="from_infomedia" style="width:8px;" checked /> <label for="from_infomedia"><?=htmlspecialchars($rekinu_rekviziti['infomedia']['nosauk']); ?></label><br />*/ ?>
		<input type="radio" name="from" value="leads" id="from_leads" style="width:8px;" checked /> <label for="from_leads"><?=htmlspecialchars($rekinu_rekviziti['leads']['nosauk']); ?></label>
	</p>

  <button class="ui-state-default ui-corner-all" style="">Ģenerēt rēķinu</button>

</form>

<script type="text/javascript">

$('table td.radio').click(function() {

  $(this).find('input[type=radio]').prop('checked', true).trigger('click');

});

$(document).ready(function() {

  var table = $("#bonusi");
  var head = $("#bonusi_filter");

  table.tablesorter({
    debug: false,
    highlightClass: 'highlight',
    headers: {
      0: {sorter: false},
      1: {sorter: false},
      2: {sorter: false},
      3: {sorter: 'text'},
      4: {sorter: 'text'},
      5: {sorter: 'text'},
      /*6: {sorter: 'digit'},
      7: {sorter: 'digit'},
      8: {sorter: 'digit'},
      9: {sorter: 'digit'},
      10: {sorter: 'digit'},
      11: {sorter: 'digit'},
      12: {sorter: 'digit'}, */
      13: {sorter: 'digit'},
      14: {sorter: 'digit'},
      15: {sorter: 'digit'},
    }
  }).bind('sortStart', function() {
    table.trigger("update");
  }).bind('sortEnd', function() {
    stripeTable(table);
  });

  initTableFilter(table, head, processTables, 'bonuss', searchlist_bonuss, sp_bonuss);

});

$(document).ready(function() {

  var table = $("#korekcijas");
  var head = $("#korekcijas_filter");

  table.tablesorter({
    debug: false,
    highlightClass: 'highlight',
    headers: {
      0: {sorter: false},
      1: {sorter: false},
      2: {sorter: false},
      3: {sorter: 'text'},
      4: {sorter: 'digit'},
      5: {sorter: 'digit'},
      6: {sorter: 'digit'},
    }
  }).bind('sortStart', function() {
    table.trigger("update");
  }).bind('sortEnd', function() {
    stripeTable(table);
  });
  initTableFilter(table, head, processTables, 'korekcija', searchlist_korekcija, sp_korekcija);
});

$(document).ready(function() {

  var table = $("#maksajumi");
  var head = $("#maksajumi_filter");

  table.tablesorter({
    debug: false,
    highlightClass: 'highlight',
    headers: {
      0: {sorter: false},
      1: {sorter: false},
      2: {sorter: false},
      3: {sorter: 'latviantext'},
      5: {sorter: 'latviantext'},
      8: {sorter: 'latviandate'}
    }
  }).bind('sortStart', function() {
    table.trigger("update");
  }).bind('sortEnd', function() {
    stripeTable(table);
  });

  initTableFilter(table, head, processTables, 'maks', searchlist_maks, sp_maks);

});

$(document).ready(function() {

  var table = $("#avansa_summas");
  var head = $("#avansa_summas_filter");

  table.tablesorter({
    debug: false,
    highlightClass: 'highlight',
    headers: {
      0: {sorter: false},
      1: {sorter: false},
      2: {sorter: false},
      3: {sorter: 'latviantext'},
      5: {sorter: 'latviantext'}
    }
  }).bind('sortStart', function() {
    table.trigger("update");
  }).bind('sortEnd', function() {
    stripeTable(table);
  });

  initTableFilter(table, head, processTables, 'avansa', searchlist_avansa, sp_avansa);

});

$(document).ready(function() {

  var table = $("#p_maksajumi");
  var head = $("#p_maksajumi_filter");

  table.tablesorter({
    debug: false,
    highlightClass: 'highlight',
    headers: {
      0: {sorter: false},
      1: {sorter: false},
      2: {sorter: false},
      3: {sorter: 'latviantext'},
      4: {sorter: 'latviantext'},
      7: {sorter: 'latviandate'}
    }
  }).bind('sortStart', function() {
    table.trigger("update");
  }).bind('sortEnd', function() {
    stripeTable(table);
  });

  initTableFilter(table, head, processTables, 'p_maks', searchlist_p_maks, sp_p_maks);

});

$(document).ready(function() {

  var table = $("#p_avansa_summas");
  var head = $("#p_avansa_summas_filter");

  table.tablesorter({
    debug: false,
    highlightClass: 'highlight',
    headers: {
      0: {sorter: false},
      1: {sorter: false},
      2: {sorter: false},
      3: {sorter: 'latviantext'},
      4: {sorter: 'latviantext'}
    }
  }).bind('sortStart', function() {
    table.trigger("update");
  }).bind('sortEnd', function() {
    stripeTable(table);
  });

  initTableFilter(table, head, processTables, 'p_avansa', searchlist_p_avansa, sp_p_avansa);

});

$(document).ready(function() {

  stripeTable($('#piemaksas'));

  function addPremRow() {

    var cur_row = $(this).parents('tr.tr_piem');

    var new_row = cur_row.clone(true);

    cur_row.find('.summa, .summa_ar_pvn, .paskaidrojums').unbind('keyup', addPremRow);

    var i = $('tr.tr_piem').length;

    new_row.find('td.radio input').attr('name', 'piemaksas['+i+'][uz_rokas]');
    new_row.find('input.summa').attr('name', 'piemaksas['+i+'][summa]');
    new_row.find('input.summa_ar_pvn').attr('name', 'piemaksas['+i+'][kopsumma]');
    new_row.find('input.paskaidrojums').attr('name', 'piemaksas['+i+'][paskaidrojums]');

    cur_row.after(new_row);

    new_row.find('td.radio_n input').prop('checked', true);
    new_row.find('td.pvn').html('');
    new_row.find('input[type=text]').val('');

    stripeTable($('#piemaksas'));

  }

  $('tr.tr_piem:last').find('.summa, .summa_ar_pvn, .paskaidrojums').keyup(addPremRow);

});

$(document).ready(function() {

  $('.bez_pvn_rek_ico').wTooltip({
    content: 'Maksājums saņemts bez PVN',
    follow: true,
    offsetY: 0,
    offsetX: 10
  });

  $('#korekcijas .bez_pvn_rek_ico').wTooltip({
    content: 'Korekcija ir par bonusiem, kuri tika izmaksāti bez PVN',
    follow: true,
    offsetY: 0,
    offsetX: 10
  });

});

function processTables() {

  $('tr.tr_bonuss:hidden, tr.tr_maks:hidden, tr.tr_avansa:hidden, tr.tr_p_maks:hidden, tr.tr_p_avansa:hidden').each(function() {
    $(this).find('td.radio_n input[type=radio]').prop('checked', true);
  });

  var pvn_likme = parseFloat(ctop($('#pvn_likme').val()));

  var kopsumma_uz_rokas = 0;
  var kopsumma_rek = 0;

  //  Bonuss
  $('tr.tr_bonuss:visible').each(function() {

    var bonuss_kopsumma_wo_pvn = parseFloat(ctop($(this).find('.bonuss_kopsumma_wo_pvn').html()));
    var bonuss_pvn = bonuss_kopsumma_wo_pvn * (pvn_likme / 100);
    var bonuss_kopsumma = bonuss_kopsumma_wo_pvn + bonuss_pvn;

    var r = $(this).find('td.radio input[type=radio]:checked').val();

    if (r == '1') {
      bonuss_pvn = 0;
      bonuss_kopsumma = bonuss_kopsumma_wo_pvn;
      kopsumma_uz_rokas += bonuss_kopsumma_wo_pvn;
    } else if (r == '0') {

      kopsumma_rek += bonuss_kopsumma;

    }

    $(this).find('.bonuss_pvn').html(bonuss_pvn.toFixed(2));
    $(this).find('.bonuss_kopsumma').html(bonuss_kopsumma.toFixed(2));

    var id = $(this).find('.row_id').val();

    if ($(this).is('.tr_bonuss')) {
      sp_bonuss['s_pvn'][id] = bonuss_pvn;
      sp_bonuss['s_kopsumma'][id] = bonuss_kopsumma;
    }
  });

  //  Korekcija
  $('tr.tr_korekcija:visible').each(function() {

    var korekcija_kopsumma_wo_pvn = parseFloat(ctop($(this).find('.korekcija_kopsumma_wo_pvn').html()));
    var korekcija_pvn = korekcija_kopsumma_wo_pvn * (pvn_likme / 100);
    var korekcija_kopsumma = korekcija_kopsumma_wo_pvn + korekcija_pvn;

    var r = $(this).find('td.radio input[type=radio]:checked').val();

    if (r == '1') {
      korekcija_pvn = 0;
      korekcija_kopsumma = korekcija_kopsumma_wo_pvn;
      kopsumma_uz_rokas += korekcija_kopsumma_wo_pvn;
    } else if (r == '0') {

      kopsumma_rek += korekcija_kopsumma;

    }

    $(this).find('.korekcija_pvn').html(korekcija_pvn.toFixed(2));
    $(this).find('.korekcija_kopsumma').html(korekcija_kopsumma.toFixed(2));

    var id = $(this).find('.row_id').val();

    if ($(this).is('.tr_korekcija')) {
      sp_korekcija['s_pvn'][id] = korekcija_pvn;
      sp_korekcija['s_kopsumma'][id] = korekcija_kopsumma;
    }
  });

  //  Prov
  $('tr.tr_maks:visible, tr.tr_avansa:visible').each(function() {

    var prov_summa = parseFloat(ctop($(this).find('.prov_summa').html()));
    var prov_pvn = prov_summa * (pvn_likme / 100);
    var prov_summa_ar_pvn = prov_summa + prov_pvn;

    var r = $(this).find('td.radio input[type=radio]:checked').val();

    if (r == '1') {
      prov_pvn = 0;
      prov_summa_ar_pvn = prov_summa;
      kopsumma_uz_rokas += prov_summa;
    } else if (r == '0') {

      kopsumma_rek += prov_summa_ar_pvn;

    }

    $(this).find('.prov_pvn').html(prov_pvn.toFixed(2));
    $(this).find('.prov_summa_ar_pvn').html(prov_summa_ar_pvn.toFixed(2));

    var id = $(this).find('.row_id').val();

    if ($(this).is('.tr_maks')) {
      sp_maks['s_prov_pvn'][id] = prov_pvn;
      sp_maks['s_prov_kopsumma'][id] = prov_summa_ar_pvn;
    } else {
      sp_avansa['s_prov_pvn'][id] = prov_pvn;
      sp_avansa['s_prov_kopsumma'][id] = prov_summa_ar_pvn;
    }

  });

  //  Prem
  $('tr.tr_p_maks:visible, tr.tr_p_avansa:visible').each(function() {

    var prem_summa = parseFloat(ctop($(this).find('.prem_summa').html()));
    var prem_pvn = prem_summa * (pvn_likme / 100);
    var prem_summa_ar_pvn = prem_summa + prem_pvn;

    var r = $(this).find('td.radio input[type=radio]:checked').val();

    if (r == '1') {
      prem_pvn = 0;
      prem_summa_ar_pvn = prem_summa;
      kopsumma_uz_rokas += prem_summa;
    } else if (r == '0') {

      kopsumma_rek += prem_summa_ar_pvn;

    }

    $(this).find('.prem_pvn').html(prem_pvn.toFixed(2));
    $(this).find('.prem_summa_ar_pvn').html(prem_summa_ar_pvn.toFixed(2));

    var id = $(this).find('.row_id').val();

    if ($(this).is('.tr_p_maks')) {
      sp_p_maks['s_prem_pvn'][id] = prem_pvn;
      sp_p_maks['s_prem_kopsumma'][id] = prem_summa_ar_pvn;
    } else {
      sp_p_avansa['s_prem_pvn'][id] = prem_pvn;
      sp_p_avansa['s_prem_kopsumma'][id] = prem_summa_ar_pvn;
    }

  });

  $('tr.tr_piem').each(function() {

    if (isNumber($(this).find('.summa').val())) {

      var summa = parseFloat(ctop($(this).find('.summa').val()));
      var summa_ar_pvn = parseFloat(ctop($(this).find('.summa_ar_pvn').val()));

      var r = $(this).find('td.radio input[type=radio]:checked').val();

      if (r == '1') {
        kopsumma_uz_rokas += summa;
      } else if (r == '0') {
        kopsumma_rek += summa_ar_pvn;
      }
    }

  });

  $('#kopsumma_uz_rokas').html(kopsumma_uz_rokas.toFixed(2));
  $('#kopsumma_rek').html(kopsumma_rek.toFixed(2));
  $('#kopsumma').html((kopsumma_uz_rokas + kopsumma_rek).toFixed(2));

  //  Bonuss
  /*var bonuss_summa_total = 0
  var bonuss_virs_min_total = 0;
  var bonuss_virs_opt_total = 0;
  var bonuss_summa_min_total = 0;
  var bonuss_summa_opt_total = 0  */
  var bonuss_kopsumma_wo_pvn_total = 0
  var bonuss_pvn_total = 0
  var bonuss_kopsumma_total = 0

  jQuery.each(searchlist_bonuss, function(k,v){

    var row = $('#bonuss_tr' + k);

    if (row.is(':visible')) {

      /*if(sp_bonuss['s_summa'][k] > 0) bonuss_summa_total += parseFloat(sp_bonuss['s_summa'][k])
      if(sp_bonuss['s_virs_min'][k] > 0) bonuss_virs_min_total += parseFloat(sp_bonuss['s_virs_min'][k])
      if(sp_bonuss['s_virs_opt'][k] > 0) bonuss_virs_opt_total += parseFloat(sp_bonuss['s_virs_opt'][k])
      if(sp_bonuss['s_summa_min'][k] > 0) bonuss_summa_min_total += parseFloat(sp_bonuss['s_summa_min'][k])
      if(sp_bonuss['s_summa_opt'][k] > 0) bonuss_summa_opt_total += parseFloat(sp_bonuss['s_summa_opt'][k])   */
      if(sp_bonuss['s_kopsumma_wo_pvn'][k] > 0) bonuss_kopsumma_wo_pvn_total += parseFloat(sp_bonuss['s_kopsumma_wo_pvn'][k])
      if(sp_bonuss['s_pvn'][k] > 0) bonuss_pvn_total += parseFloat(sp_bonuss['s_pvn'][k])
      if(sp_bonuss['s_kopsumma'][k] > 0) bonuss_kopsumma_total += parseFloat(sp_bonuss['s_kopsumma'][k])

    }

    /*$('#bonuss_summa_total').html(bonuss_summa_total.toFixed(2));
    $('#bonuss_virs_min_total').html(bonuss_virs_min_total.toFixed(2));
    $('#bonuss_virs_opt_total').html(bonuss_virs_opt_total.toFixed(2));
    $('#bonuss_summa_min_total').html(bonuss_summa_min_total.toFixed(2));
    $('#bonuss_summa_opt_total').html(bonuss_summa_opt_total.toFixed(2));    */
    $('#bonuss_kopsumma_wo_pvn_total').html(bonuss_kopsumma_wo_pvn_total.toFixed(2));
    $('#bonuss_pvn_total').html(bonuss_pvn_total.toFixed(2));
    $('#bonuss_kopsumma_total').html(bonuss_kopsumma_total.toFixed(2));
  });

  //  Korekcija
  var korekcija_kopsumma_wo_pvn_total = 0
  var korekcija_pvn_total = 0
  var korekcija_kopsumma_total = 0


  jQuery.each(searchlist_korekcija, function(k,v){

    var row = $('#korekcija_tr' + k);

    if (row.is(':visible')) {

      if(sp_korekcija['s_kopsumma_wo_pvn'][k] != 0) korekcija_kopsumma_wo_pvn_total += parseFloat(sp_korekcija['s_kopsumma_wo_pvn'][k])
      if(sp_korekcija['s_pvn'][k] != 0) korekcija_pvn_total += parseFloat(sp_korekcija['s_pvn'][k])
      if(sp_korekcija['s_kopsumma'][k] != 0 ) korekcija_kopsumma_total += parseFloat(sp_korekcija['s_kopsumma'][k])

    }

    $('#korekcija_kopsumma_wo_pvn_total').html(korekcija_kopsumma_wo_pvn_total.toFixed(2));
    $('#korekcija_pvn_total').html(korekcija_pvn_total.toFixed(2));
    $('#korekcija_kopsumma_total').html(korekcija_kopsumma_total.toFixed(2));
  });

  //  Prov
  var maks_rekinu_summa = 0;
  var maks_sanemta_summa = 0;
  var maks_prov_summa = 0;
  var maks_prov_pvn = 0;
  var maks_prov_kopsumma = 0;

  jQuery.each(searchlist_maks, function(k,v){

    var row = $('#maks_tr' + k);

    if (row.is(':visible')) {

      if (sp_maks['s_rekina_kopsumma'][k] > 0) maks_rekinu_summa += parseFloat(sp_maks['s_rekina_kopsumma'][k]);
      if (sp_maks['s_summa'][k] > 0) maks_sanemta_summa += parseFloat(sp_maks['s_summa'][k]);
      if (sp_maks['s_prov_summa'][k] > 0) maks_prov_summa += parseFloat(sp_maks['s_prov_summa'][k]);
      if (sp_maks['s_prov_pvn'][k] > 0) maks_prov_pvn += parseFloat(sp_maks['s_prov_pvn'][k]);
      if (sp_maks['s_prov_kopsumma'][k] > 0) maks_prov_kopsumma += parseFloat(sp_maks['s_prov_kopsumma'][k]);

    }

  });

  $('#maks_rekinu_summa').html(maks_rekinu_summa.toFixed(2));
  $('#maks_sanemta_summa').html(maks_sanemta_summa.toFixed(2));
  $('#maks_prov_summa').html(maks_prov_summa.toFixed(2));
  $('#maks_prov_pvn').html(maks_prov_pvn.toFixed(2));
  $('#maks_prov_kopsumma').html(maks_prov_kopsumma.toFixed(2));


  var avansa_rekinu_summa = 0;
  var avansa_neapm_summa = 0;
  var avansa_prov_summa = 0;
  var avansa_prov_pvn = 0;
  var avansa_prov_kopsumma = 0;

  jQuery.each(searchlist_avansa, function(k,v){

    var row = $('#avansa_tr' + k);

    if (row.is(':visible')) {

      if (sp_avansa['s_rekina_kopsumma'][k] > 0) avansa_rekinu_summa += parseFloat(sp_avansa['s_rekina_kopsumma'][k]);
      if (sp_avansa['s_neapm_summa'][k] > 0) avansa_neapm_summa += parseFloat(sp_avansa['s_neapm_summa'][k]);
      if (sp_avansa['s_prov_summa'][k] > 0) avansa_prov_summa += parseFloat(sp_avansa['s_prov_summa'][k]);
      if (sp_avansa['s_prov_pvn'][k] > 0) avansa_prov_pvn += parseFloat(sp_avansa['s_prov_pvn'][k]);
      if (sp_avansa['s_prov_kopsumma'][k] > 0) avansa_prov_kopsumma += parseFloat(sp_avansa['s_prov_kopsumma'][k]);
    }

  });

  $('#avansa_rekinu_summa').html(avansa_rekinu_summa.toFixed(2));
  $('#avansa_neapm_summa').html(avansa_neapm_summa.toFixed(2));
  $('#avansa_prov_summa').html(avansa_prov_summa.toFixed(2));
  $('#avansa_prov_pvn').html(avansa_prov_pvn.toFixed(2));
  $('#avansa_prov_kopsumma').html(avansa_prov_kopsumma.toFixed(2));

  //  Prēmijas

  var p_maks_rekinu_summa = 0;
  var p_maks_sanemta_summa = 0;
  var p_maks_prem_summa = 0;
  var p_maks_prem_pvn = 0;
  var p_maks_prem_kopsumma = 0;

  jQuery.each(searchlist_p_maks, function(k,v){

    var row = $('#p_maks_tr' + k);

    if (row.is(':visible')) {

      if (sp_p_maks['s_rekina_kopsumma'][k] > 0) p_maks_rekinu_summa += parseFloat(sp_p_maks['s_rekina_kopsumma'][k]);
      if (sp_p_maks['s_summa'][k] > 0) p_maks_sanemta_summa += parseFloat(sp_p_maks['s_summa'][k]);
      if (sp_p_maks['s_prem_summa'][k] > 0) p_maks_prem_summa += parseFloat(sp_p_maks['s_prem_summa'][k]);
      if (sp_p_maks['s_prem_pvn'][k] > 0) p_maks_prem_pvn += parseFloat(sp_p_maks['s_prem_pvn'][k]);
      if (sp_p_maks['s_prem_kopsumma'][k] > 0) p_maks_prem_kopsumma += parseFloat(sp_p_maks['s_prem_kopsumma'][k]);

    }


  });

  $('#p_maks_rekinu_summa').html(p_maks_rekinu_summa.toFixed(2));
  $('#p_maks_sanemta_summa').html(p_maks_sanemta_summa.toFixed(2));
  $('#p_maks_prem_summa').html(p_maks_prem_summa.toFixed(2));
  $('#p_maks_prem_pvn').html(p_maks_prem_pvn.toFixed(2));
  $('#p_maks_prem_kopsumma').html(p_maks_prem_kopsumma.toFixed(2));

  var p_avansa_rekinu_summa = 0;
  var p_avansa_neapm_summa = 0;
  var p_avansa_prem_summa = 0;
  var p_avansa_prem_pvn = 0;
  var p_avansa_prem_kopsumma = 0;

  jQuery.each(searchlist_p_avansa, function(k,v){

    var row = $('#p_avansa_tr' + k);

    if (row.is(':visible')) {

      if (sp_p_avansa['s_rekina_kopsumma'][k] > 0) p_avansa_rekinu_summa += parseFloat(sp_p_avansa['s_rekina_kopsumma'][k]);
      if (sp_p_avansa['s_neapm_summa'][k] > 0) p_avansa_neapm_summa += parseFloat(sp_p_avansa['s_neapm_summa'][k]);
      if (sp_p_avansa['s_prem_summa'][k] > 0) p_avansa_prem_summa += parseFloat(sp_p_avansa['s_prem_summa'][k]);
      if (sp_p_avansa['s_prem_pvn'][k] > 0) p_avansa_prem_pvn += parseFloat(sp_p_avansa['s_prem_pvn'][k]);
      if (sp_p_avansa['s_prem_kopsumma'][k] > 0) p_avansa_prem_kopsumma += parseFloat(sp_p_avansa['s_prem_kopsumma'][k]);

    }

  });

  $('#p_avansa_rekinu_summa').html(p_avansa_rekinu_summa.toFixed(2));
  $('#p_avansa_neapm_summa').html(p_avansa_neapm_summa.toFixed(2));
  $('#p_avansa_prem_summa').html(p_avansa_prem_summa.toFixed(2));
  $('#p_avansa_prem_pvn').html(p_avansa_prem_pvn.toFixed(2));
  $('#p_avansa_prem_kopsumma').html(p_avansa_prem_kopsumma.toFixed(2));

}

function processPiemRow(foc_inp) {

  var pvn_likme = parseFloat(ctop($('#pvn_likme').val()));

  var row = foc_inp.parents('tr');

  if (row.length == 0) {
    return false;
  }

  var summa_inp = row.find('.summa');
  var summa_ar_pvn_inp = row.find('.summa_ar_pvn');

  if (foc_inp.is('.summa_ar_pvn')) {

    if (isNumber(summa_ar_pvn_inp.val())) {

      var summa_ar_pvn = parseFloat(ctop(summa_ar_pvn_inp.val()));

      if (row.find('td.radio input[type=radio]:checked').val() == '1') {
        var pvn_summa = 0;
        var summa = summa_ar_pvn;
      } else {
        var pvn_summa = summa_ar_pvn * (pvn_likme / 100) / (1 + pvn_likme / 100);
        var summa = summa_ar_pvn - pvn_summa;
      }

      row.find('.pvn').html(pvn_summa.toFixed(2));
      summa_inp.val(parseFloat(summa).toFixed(2));

    } else {

      row.find('.pvn').html('');
      summa_inp.val('')

    }

  } else {

    if (isNumber(summa_inp.val())) {

      var summa = parseFloat(ctop(summa_inp.val()));

      if (row.find('td.radio input[type=radio]:checked').val() == '1') {
        var pvn_summa = 0;
        var summa_ar_pvn = summa;
      } else {
        var pvn_summa = (summa * pvn_likme ) / 100;
        var summa_ar_pvn = summa + pvn_summa;
      }

      row.find('.pvn').html(pvn_summa.toFixed(2));
      summa_ar_pvn_inp.val(parseFloat(summa_ar_pvn).toFixed(2));

    } else {

      row.find('.pvn').html('');
      summa_ar_pvn_inp.val('')

    }

  }

}

processTables();

$('#pvn_likme').keyup(function() {

  $('tr.tr_piem').find('input.summa').each(function() {
    processPiemRow($(this));
  });

  processTables();

});

$('tr.tr_piem').find('input.summa, input.summa_ar_pvn').keyup(function() {

  processPiemRow($(this));
  processTables();

});

$('tr.tr_bonuss, tr.tr_korekcija, tr.tr_maks, tr.tr_avansa, tr.tr_p_maks, tr.tr_p_avansa').find('td.radio input[type=radio]').click(function(e) {

  e.stopPropagation();

  processTables();

});

$('tr.tr_piem td.radio input[type=radio]').click(function(e) {

  e.stopPropagation();

  processPiemRow($(this));
  processTables();

});

$(".showcalendar").datepicker({dateFormat:'dd.mm.yy'});

function selectAllInCol(table_id, class_name) {

  $('#' + table_id + ' td.' + class_name + ' input[type=radio]').prop('checked', true);

  processTables();

}
</script>