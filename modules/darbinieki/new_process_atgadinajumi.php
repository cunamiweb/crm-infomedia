<?php
if(!check_access('darbinieki-atgadinajumi') && ($pardeveja_id != $_SESSION['user']['id'] || !check_access('darbinieki-savi-atgadinajumi'))) {
  die('Jums nav pieejas tiesību veikt šo darbību.');
}

if (!empty($_POST) || !empty($_FILES)) {

  $errors = validate_darbinieks_atgadinajumi_form($_POST);

  if (empty($errors)) {

    $_POST = trim_array($_POST);

    // updating reminders

    if (!empty($_POST['atgadinajumi'])) {

      $old_comments = array();

      foreach($_POST['atgadinajumi'] as $atgadinajuma_id => $a) {

        if(isset($a['datums'], $a['redzamiba'], $a['saturs'])) {
          $log_data = array();

          $log_data['atgadinajums']['old'] = db_get_assoc(db_query("SELECT * FROM `".DB_PREF."atgadinajumi` WHERE id = ".$atgadinajuma_id." LIMIT 1"));

          $sql = "
            UPDATE `".DB_PREF."atgadinajumi`
            SET
              datums = '".esc(date('Y-m-d', strtotime($a['datums'])))."',
              redzamiba = '".$a['redzamiba']."',
              saturs = '".esc($a['saturs'])."'
            WHERE
              id = ".$atgadinajuma_id."
              ".(!is_admin() ? "AND pardevejs_var_labot = 1" : '')."
            LIMIT 1
          ";

          db_query($sql);

          $log_data['atgadinajums']['new'] = db_get_assoc(db_query("SELECT * FROM `".DB_PREF."atgadinajumi` WHERE id = ".$atgadinajuma_id." LIMIT 1"));
          $log_data['atgadinajums']['title'] = sprintf('Laboja atgādinājumu ar ID %s', $atgadinajuma_id);

          log_add("laboja", $log_data);
        }

        //  Pievieno komentāru
        if(isset($a['komentars']) && trim($a['komentars'])) {
          $atg = db_get_assoc(db_query("SELECT * FROM `".DB_PREF."atgadinajumi` WHERE id = ".$atgadinajuma_id." LIMIT 1"));
          $object_id = $atg[$_vars['atg_obj_id'][$atg['atgadinajuma_veida_id']]];
          $object_type = $_vars['atg_obj_tipi'][$atg['atgadinajuma_veida_id']];

          if(!isset($old_comments[$object_type])) {
            $old_comments[$object_type] = array();
            $old_comments[$object_type][$object_id] = array();
          } elseif(!isset($old_comments[$object_type][$object_id])) {
            $old_comments[$object_type][$object_id] = array();
          }

          //  Atrod pēdējo komentu
          $last = get_last_comment($object_id, $object_type);

          //  Ieliek pēdējo komentu šī objekta "veco komentāru" masīvā lai nepievienotos lieki komenti.
          if($last) {
            $old_comments[$object_type][$object_id][] = $last['komentars'];
          }

          //  Ja nav komentu vai pēdējais atšķiras no ievadītā, tad pievieno jaunu
          if(!in_array($a['komentars'], $old_comments[$object_type][$object_id])) {
            //  Pievieno komentāru
            db_query("
              INSERT INTO `".DB_PREF."komentari` SET
                `objekta_id` = " . (int)$object_id . ",
                `objekta_tips` = '" . $object_type . "',
                `pardeveja_id` = " . $_SESSION['user']['id'] . ",
                `komentars` = '" . esc($a['komentars']) . "'
            ");

            $komentara_id = db_last_id();

            $log_data = array();

            $log_data['komentars']['new'] = db_get_assoc(db_query("SELECT * FROM `".DB_PREF."komentari` WHERE id = " . $komentara_id));
            $log_data['komentars']['title'] = sprintf('Pievienoja jaunu komentāru ar ID %s', $komentara_id);

            log_add("laboja", $log_data);
          }
        }
      }
    }


    // adding reminders

    if (!empty($_POST['atgadinajumi_new']['datums'])) {

      foreach($_POST['atgadinajumi_new']['datums'] as $i => $datums) {

        $redzamiba = $_POST['atgadinajumi_new']['redzamiba'][$i];
        $saturs = $_POST['atgadinajumi_new']['saturs'][$i];

        $liguma_id = $_POST['atgadinajumi_new']['ligums'][$i];
        $atg_pardeveja_id = 'null';

        if (empty($liguma_id)) {
          $liguma_id = 'null';
          $atg_pardeveja_id = $pardeveja_id;
        }

        $sql = "
          INSERT INTO `".DB_PREF."atgadinajumi` (
            liguma_id,
            pardeveja_id,
            datums,
            redzamiba,
            saturs,
            created_pardeveja_id
          ) VALUES (
            ".$liguma_id.",
            ".$atg_pardeveja_id.",
            '".esc(date('Y-m-d', strtotime($datums)))."',
            '".$redzamiba."',
            '".esc($saturs)."',
            ".$_SESSION['user']['id']."
          )
        ";

        db_query($sql);

        $atgadinajuma_id = db_last_id();

        $log_data = array();

        $log_data['atgadinajums']['new'] = db_get_assoc(db_query("SELECT * FROM `".DB_PREF."atgadinajumi` WHERE id = ".$atgadinajuma_id." LIMIT 1"));
        $log_data['atgadinajums']['title'] = sprintf('Pievienoja atgādinājumu ar ID %s', $atgadinajuma_id);

        log_add("laboja", $log_data);


      }
    }

    if (check_access('darbinieki-admin')) {

      // inserting files
      process_files(array('darb_atgadinajumi'), $pardeveja_id);

    }

    // inserting comments
    process_comments(array('darb_atgadinajumi'), $pardeveja_id);

    header('Location: ?c=darbinieki&a=labot&id=' . $pardeveja_id . '&subtab=' . $subtab);
    die();

  }

}
?>