<?php
if(!check_access('darbinieki-statistika') && $pardeveja_id != $_SESSION['user']['id']) {
  die('Jums nav pieejas tiesību šai sadaļai');
}

if (!empty($_POST)) {

  $errors = validate_darbinieks_statistika_form($_POST);

  if (empty($errors)) {

    $_POST = trim_array($_POST);

    if (check_access('darbinieki-statistika-faili')) {

      // inserting files
      process_files(array('darb_statistika'), $pardeveja_id);

    }

    // inserting comments
    process_comments(array('darb_statistika'), $pardeveja_id);

    header('Location: ?c=darbinieki&a=labot&id=' . $pardeveja_id . '&subtab=' . $subtab);
    die();

  }

}
?>