<?php
if(!check_access('darbinieki-plana-izpilde') && ($pardeveja_id != $_SESSION['user']['id'] || !check_access('darbinieki-sava-plana-izpilde'))) {
  die();
}

if (!empty($_POST)) {

  $errors = validate_darbinieks_izpilde_form($_POST);

  if (empty($errors)) {

    $log_data = array();

    $_POST = trim_array($_POST);

    if (check_access('darbinieki-labot-plana-izpilde')) {

      $log_data['min_izpilde']['old_m'] = db_get_assoc_all(db_query("SELECT * FROM `".DB_PREF."pardeveji_min_izpilde` WHERE `pardeveja_id` = " . $pardeveja_id));

      // dzēšam noņemtos min izpildes ierakstus

      $sql = "
        DELETE FROM `".DB_PREF."pardeveji_min_izpilde`
        WHERE
          pardeveja_id = ".$pardeveja_id."
          " . (!empty($_POST['min_izpilde']) ? "AND id NOT IN (".implode(',', array_keys($_POST['min_izpilde'])).") " : '')."
      ";

      db_query($sql);

      // labojumam pārslēgšanas rindas ierakstus

      if (!empty($_POST['min_izpilde'])) {

        foreach($_POST['min_izpilde'] as $min_izpilde_id => $p) {

          $p['lig_apgroz'] = (float)str_replace(',', '.', $p['lig_apgroz']);

          $sql = "
            UPDATE `".DB_PREF."pardeveji_min_izpilde`
            SET
              sakot_no = '".($p['sakot_no_y'] . '-' . $p['sakot_no_m'] . '-01')."',
              lig_skaits = ".(int)$p['lig_skaits'].",
              lig_apgroz = ".$p['lig_apgroz']."
            WHERE id = ".$min_izpilde_id."
            LIMIT 1
          ";

          db_query($sql);

        }

      }

      // pievienojam jaunus pārslēgšanas rindas ierakstus

      if (!empty($_POST['min_izpilde_new']['sakot_no_y'])) {

        foreach($_POST['min_izpilde_new']['sakot_no_y'] as $i => $sakot_no_y) {

          $sakot_no_m = $_POST['min_izpilde_new']['sakot_no_m'][$i];
          $lig_skaits = (int)$_POST['min_izpilde_new']['lig_skaits'][$i];
          $lig_apgroz = (float)str_replace(',', '.', $_POST['min_izpilde_new']['lig_apgroz'][$i]);

          $sql = "
            INSERT INTO `".DB_PREF."pardeveji_min_izpilde` (
              pardeveja_id,
              sakot_no,
              lig_skaits,
              lig_apgroz
            ) VALUES (
              ".$pardeveja_id.",
              '".($sakot_no_y . '-' . $sakot_no_m . '-01')."',
              ".$lig_skaits.",
              ".$lig_apgroz."
            )
          ";

          db_query($sql);

        }

      }

      $log_data['min_izpilde']['new_m'] = db_get_assoc_all(db_query("SELECT * FROM `".DB_PREF."pardeveji_min_izpilde` WHERE `pardeveja_id` = " . $pardeveja_id));
      $log_data['min_izpilde']['title'] = sprintf('Laboja pārdevēja %s līgumu minimālās izpildes datus', db_get_val(db_query("SELECT vards FROM `".DB_PREF."pardeveji` WHERE id = ".$pardeveja_id." LIMIT 1")));

      //  Izpilde pēc ielogošanās un līmenis
      $log_data['pardevejs']['old'] = db_get_assoc(db_query("SELECT * FROM `".DB_PREF."pardeveji` WHERE `id` = " . $pardeveja_id));

      $pardevejs->izpilde_after_login = isset($_POST['izpilde_after_login']) ? 1 : 0;
      $pardevejs->izpilde_show_level = $_POST['izpilde_show_level'];
      $pardevejs->save();

      $log_data['pardevejs']['new'] = db_get_assoc(db_query("SELECT * FROM `".DB_PREF."pardeveji` WHERE `id` = " . $pardeveja_id));
      $log_data['pardevejs']['title'] = sprintf('Laboja pārdevēja %s plāna izpildes uzstādījumus', db_get_val(db_query("SELECT vards FROM `".DB_PREF."pardeveji` WHERE id = ".$pardeveja_id." LIMIT 1")));

    }

    if (check_access('darbinieki-labot-plana-izpilde')) {

      // inserting files
      process_files(array('darb_izpilde'), $pardeveja_id);

    }

    // inserting comments
    process_comments(array('darb_izpilde'), $pardeveja_id);

    log_add("laboja", $log_data);

    header('Location: ?c=darbinieki&a=labot&id=' . $pardeveja_id . '&subtab=' . $subtab);
    die();

  }

}
?>