<?php
if(!check_access('ligumi-atskaites')) {
  die('Nav pieejas tiesību.');
}

if(!isset($_GET['id'])) {
  die('Nav izvēlēts līgums.');
}

$ligums = Ligums::model()->findByPk($_GET['id']);

if(!$ligums) {
  die('Neeksistējošs līgums.');
}

/*
  Iegūstam datus no Google Adwords
*/

$data = getAdwordsReportData($_GET['id'], $_GET, false);

if(empty($data)) {
  echo '<br /><br />Nav atrasta neviena reklāmu kopa.<br /><br />';
} else {

$totals = array(
  'impressions' => 0,
  'clicks' => 0,
  'avg_pos' => array(),
);

$filter_name = mb_strtolower($ligums->nosaukums);

$filter_name = preg_replace('/([.]*)( sia)$/', '$1', trim($filter_name));
$filter_name = preg_replace('/([.]*)( ik)$/', '$1', trim($filter_name));

echo '<br />Attīrītais klienta nosaukums, kas tiek meklēts kampaņu nosaukumos: <strong>' . $filter_name . '</strong><br />';

?>
<table border='0' class="atskaites_testa_tabula data">
  <thead>
    <tr>
      <th class="c">Kampaņa</th>
      <th class="c">Reklāmu kopa</th>
      <th class="c">Seansi</th>
      <th class="c">Klikšķi</th>
      <th class="c">Vid.pozīcija</th>
    </tr>
  </thead>

  <tbody>
    <?php foreach($data as $row) {

      $totals['impressions'] += $row['impressions'];
      $totals['clicks'] += $row['clicks'];
      $totals['avg_pos'][] = round($row['avg_pos'], 2);
      ?>
      <tr>
        <td>
        <?php

          if(strstr(mb_strtolower($row['campaign_name']), $filter_name) === false) {
            echo '<strong style="color: red;">- </strong>';
          } else {
            echo '<strong style="color: green;">+ </strong>';
          }

          echo mb_strtolower($row['campaign_name']);

        ?>
        </td>
        <td><?php echo $row['ad_group'];?></td>
        <td class="r"><?php echo $row['impressions'];?></td>
        <td class="r"><?php echo $row['clicks'];?></td>
        <td class="r"><?php echo round($row['avg_pos'], 2);?></td>
      </tr>
    <?php } ?>


    <tr>
      <td style="font-weight: bold;">--</td>
      <td style="font-weight: bold;">--</td>
      <td class="r" style="font-weight: bold;"><?php echo $totals['impressions'];?></td>
      <td class="r" style="font-weight: bold;"><?php echo $totals['clicks'];?></td>
      <td class="r" style="font-weight: bold;"><?php echo round(array_sum(array_filter($totals['avg_pos']))/count(array_filter($totals['avg_pos'])), 2);?></td>
    </tr>
  </tbody>
</table>
<?php } ?>