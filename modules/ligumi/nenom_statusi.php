<?php
//if (is_admin() && empty($_SESSION['user']['piekluve_nenom_stat'])) {
if (!check_access('ligumi-nenomainitie-statusi')) {
  die();
}

$pardevejs = Pardevejs::model()->findByPk($_SESSION['user']['id']);

if (!check_access('ligumi-nenomainitie-statusi-visi')) {

  $darbinieks = db_get_assoc(db_query("
    SELECT *
    FROM `".DB_PREF."pardeveji`
    WHERE id = ".$_SESSION['user']['id']."
  "));

  log_add("atvera", "Atvēra darbinieka " . $darbinieks['vards'] . " nenomainīto statusu sarakstu");

  //  Vadītājs ar padotajiem
  $pardeveju_ids = array();
  if($pardevejs->vaditajs) {
    $pardeveju_ids[] = $pardevejs->id;
    foreach((array)$pardevejs->getPadotie() as $padotais) {
      $pardeveju_ids[] = $padotais->id;
    }
  }

  if(count($pardeveju_ids) > 1) {
    $ligumi = get_nenomainitie_statusi(array('pardeveju_ids' => $pardeveju_ids));
  } else {
    $ligumi = get_nenomainitie_statusi(array('pardeveja_id' => $_SESSION['user']['id']));
  }

} else {

  log_add("atvera", "Atvēra nenomainīto statusu sarakstu");

  $ligumi = get_nenomainitie_statusi();

  $query = db_query("
    SELECT
      s.liguma_id,
      p.*
    FROM `".DB_PREF."ligumi_saistitie_pardeveji` s
    LEFT JOIN `".DB_PREF."pardeveji` p ON (p.id = s.pardeveja_id)
  ");

  $saist_pard = array();

  while($row = db_get_assoc($query)){
    $saist_pard[$row['liguma_id']][$row['id']] = $row;
  }

}
?>

<script>
var searchlist = [];

var sp = create_filter_field_array([
  's_nosaukums', 's_ligumanr', 's_lig_stat', 's_reklama_no', 's_reklama_lidz'
]);
</script>

<table style="display:none;">

  <tr id="nenomainitie_statusi_filter">

    <?php if(is_admin()) { ?>
      <th>
      </th>
    <?php } ?>

    <th><span><input class="search_field" meth="standart" searchclass="s_nosaukums" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>
    <th><span><input class="search_field" meth="standart" searchclass="s_ligumanr" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span></th>

    <th>
      <span><input class="search_field kalendari" meth="from" searchclass="s_reklama_no" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span><br>
      <span><input class="search_field kalendari" meth="to" searchclass="s_reklama_no" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span>
    </th>

    <th>
      <span><input class="search_field kalendari" meth="from" searchclass="s_reklama_lidz" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span><br>
      <span><input class="search_field kalendari" meth="to" searchclass="s_reklama_lidz" type="input" value="" style="width:80%;"><a class="search_field" href="#">x</a></span>
    </th>

    <th></th>

  </tr>

</table>

<table style="width: 70%;" cellpadding="3" cellspacing="0" id="nenomainitie_statusi" class="data">

  <thead>

    <tr class="header">
      <?php if(check_access('ligumi-nenomainitie-statusi-visi')) { ?>
        <th>Darbinieks</th>
      <?php } ?>
      <th>Klients</th>
      <th>Līguma Nr.</th>
      <th>Līgums no</th>
      <th>Līgums līdz</th>
      <th>Statuss</th>
    </tr>

  </thead>

  <tbody class="main">

    <?php
    $i = count($ligumi);
    $inid = -1;

    foreach($ligumi as $row) {

      $inid ++;

      ?>

      <tr id="tr<?=$inid;?>">

        <?php if (check_access('ligumi-nenomainitie-statusi-visi')) { ?>

          <td style="white-space: nowrap;">
            <?php
            if (!empty($saist_pard[$row['id']])) {
              foreach($saist_pard[$row['id']] as $saist_pard_id => $v) {
                echo '<a href="?c=darbinieki&a=labot&id='.$saist_pard_id.'"><b>'.$_vars['sys_pardeveji'][$saist_pard_id].'</b></a><br />';
              }
            } else {
              echo '<a href="?c=darbinieki&a=labot&id='.$row['pardeveja_id'].'"><b>'.$_vars['sys_pardeveji'][$row['pardeveja_id']].'</b></a>';
            }
            ?>
          </td>

        <?php } ?>

        <td>
          <a href="?c=ligumi&a=labot&id=<?=$row['id'];?>"><b><?= $row['nosaukums'] ?></b></a>

          <?php if (!check_access('ligumi-nenomainitie-statusi-visi')) { ?>

            <? if (!empty($row['saistitais_ligums'])) { ?>
              <span class="saist_pard_ico">s</span>
            <?php  } ?>

          <?php } else { ?>

            <?php if (!empty($saist_pard[$row['id']])) { ?>
              <span class="saist_pard_ico" title="Līgumu noslēdza: <?php echo $_vars['sys_pardeveji'][$row['pardeveja_id']] ?>">s</span>
            <?php } ?>
          <?php } ?>
        </td>
        <td class="c"><?= $row['ligumanr'] ?></td>
        <td class="c"><span style="display:none;"><?=date("Y.m.d",strtotime($row['reklama_no']));?>|</span><?= !empty($row['reklama_no'])?date("d.m.Y",strtotime($row['reklama_no'])):"" ?></td>
        <td class="c"><span style="display:none;"><?=date("Y.m.d",strtotime($row['reklama_lidz']));?>|</span><?= !empty($row['reklama_lidz'])?date("d.m.Y",strtotime($row['reklama_lidz'])):"" ?></td>
        <td class="c" style="white-space: nowrap;">
          <div class="lig_statuss_6 lig_beigu_statuss_termins">
            <a href="#" onclick="showLigumaBeiguStatussDialog(<?= $row['id'] ?>); return false;"><strong><?= $_vars['liguma_statuss'][6] ?></strong> (termiņš) <img src="css/edit.png" alt="Mainīt" style="vertical-align: middle;" /></a>
          </div>
        </td>

        <td style="display: none;">
        <script>
          searchlist[<?=$inid;?>] = '<?=addslashes(mb_strtoupper($row['nosaukums']));?>';
          sp['s_nosaukums'][<?=$inid;?>] = '<?=addslashes(mb_strtoupper($row['nosaukums']));?>';
          sp['s_ligumanr'][<?=$inid;?>] = '<?=addslashes(mb_strtoupper($row['ligumanr']));?>';
          sp['s_lig_stat'][<?=$inid;?>] = 6;
          sp['s_reklama_no'][<?=$inid;?>] = <?= !empty($row['reklama_no']) ? date('Ymd', strtotime($row['reklama_no'])) : 0 ?>;
          sp['s_reklama_lidz'][<?=$inid;?>] = <?= !empty($row['reklama_lidz']) ? date('Ymd', strtotime($row['reklama_lidz'])) : 0 ?>;
        </script>
        </td>

      </tr>

      <? $i -- ?>

    <?php } ?>

  </tbody>

</table>

<script type="text/javascript">
$(document).ready(function() {

  var table = $("#nenomainitie_statusi");
  var head = $("#nenomainitie_statusi_filter");

  table.tablesorter({
    debug: false,
    highlightClass: 'highlight',
    headers: {
      <?php if (is_admin()) { ?>
        1: { sorter:'latviantext' },
        2: { sorter:'latviantext' }
      <?php } else { ?>
        0: { sorter:'latviantext' },
        1: { sorter:'latviantext' }
      <?php } ?>
    }
  }).bind('sortEnd', function() {

    stripeTable(table);

  });

  $('.saist_pard_ico').wTooltip({
    <?php echo !is_admin() ? "content: 'Saistītais līgums'," : "" ?>
    follow: true,
    offsetY: 0,
    offsetX: 10
  });

  initTableFilter(table, head, function() {}, '', searchlist, sp);

});
</script>