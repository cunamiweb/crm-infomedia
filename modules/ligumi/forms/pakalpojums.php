
<table class="data_form layout1">

  <tr>
    <td width="160" class="label" onchange="recreateRekiniNr()">Pakalpojuma tips</td>
    <td>
      <select name="mpk" id="pakalpojums_tips" class="<?php echo $pc_labot_pak;?>" onchange="setPakalpojumsFields()">
        <option value="0" <?php echo $ligums->mpk ? '' : 'selected';?>>MPT</option>
        <option value="1" <?php echo $ligums->mpk ? 'selected' : '';?>>MPK</option>
      </select>
    </td>
  </tr>

  <tr>
    <td width="160" class="label">Konta administrators</td>
    <td>
      <select class="<?php echo $pc_labot_pak;?>" name="konta_admin">
        <?php foreach($_vars['konta_admin'] as $value => $name) {?>
          <option value="<?php echo $value;?>" <?php echo $value == $ligums->konta_admin ? 'selected' : '';?>><?php echo $name;?></option>
        <?php } ?>

      </select>
    </td>
  </tr>

  <tr><td colspan="2" class="sep"><div></div></td></tr>

  <tr><td width="160" class="label">Nosaukums datubāzē:</td><td><input class="<?php echo $pc_labot_pak;?>" type="input" name="pak_nosaukums_db" value="<?=!empty($d['pak_nosaukums_db'])?$d['pak_nosaukums_db']:""?>"></td></tr>

  <tr class="only_mpk"><td width="160" class="label">Mājas lapa:</td><td><input class="<?php echo $pc_labot_pak;?>" type="input" name="website" value="<?php echo $ligums->website;?>"></td></tr>

  <tr><td colspan="2" class="sep"><div></div></td></tr>

  <tr class="only_mpk">
    <td width="160" class="label">Kampaņas tips</td>
    <td>
      <select class="<?php echo $pc_labot_pak;?>" name="campaign_type">
        <option value="test" <?php echo $ligums->campaign_type == 'test' ? 'selected' : '';?>>Testa</option>
        <option value="time" <?php echo $ligums->campaign_type == 'time' ? 'selected' : '';?>>Termiņa</option>
      </select>
    </td>
  </tr>

  <tr>
    <td class="label">Reklāmas termiņš:</td>
    <td>
      no: <input class="<?php echo $pc_labot_pak;?>" style="width: 6em; font-weight: bold; text-align: center;" type="input" name="reklama_no" id="rekno" value="<?=!empty($d['reklama_no'])?date('d.m.Y', strtotime($d['reklama_no'])):''?>">
      līdz: <input onchange="autoSetGoogleStatus(this)" class="<?php echo $pc_labot_pak;?>" style="width: 6em; font-weight: bold; text-align: center;" type="input" name="reklama_lidz" id="reklidz" value="<?=!empty($d['reklama_lidz'])?date('d.m.Y', strtotime($d['reklama_lidz'])):''?>">
      <script type="text/javascript">
      function autoSetGoogleStatus(enddate_field) {
        if($('#google_status').val() != '4' && $(enddate_field).val()) { // 4 = Atslēgts

          var dateparts = $(enddate_field).val().split('.');
          var date = new Date(dateparts[2], dateparts[1]-1, dateparts[0]);
          var now = new Date;

          if($('select[name=campaign_type]').val() == 'test' && date <= now) {
            //conf = confirm('Vai nomainīt google statusu uz "Atslēgts"?');
            $( "#set-google-dialog-confirm" ).dialog({
              resizable: false,
              height:140,
              width: 400,
              modal: true,
              buttons: {
                "Jā": function() {
                    $('#google_status').val(4);
                    $( this ).dialog( "close" );
                },
                'Nē': function() {
                    $( this ).dialog( "close" );
                }
              }
            });
          }
        }
      }
      </script>
      <div style="display:none; width: 400px;" id="set-google-dialog-confirm" title="Google statuss">
          <p><span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 20px 0;"></span>Vai nomainīt Google statusu uz "Atslēgts"?</p>
      </div>
    </td>
  </tr>

  <tr><td colspan="2" class="sep"><div></div></td></tr>

  <tr>
    <td class="label">Infomedia statuss:</td>
    <td>
      <select name="infomedia_statuss" class="<?php echo $pc_labot_pak;?> with_status_colors classprefix-infomedia_statuss">
        <?php foreach($_vars['infomediastatuss'] as $k=>$p){ ?>
          <option class="status-<?=$k;?>" value="<?=$k;?>" <?=($k==(!empty($d['infomedia_statuss'])?$d['infomedia_statuss']:1)?"selected":"")?>><?=$p;?></option>
        <?php } ?>
      </select>
    </td>
  </tr>

  <tr>
    <td class="label">Google statuss:</td>
    <td>
      <select name="google_statuss" id="google_status" onchange="autoSetEndDate(this);" class="<?php echo $pc_labot_pak;?> with_status_colors classprefix-google_statuss">
        <?php foreach($_vars['googlestatuss2'] as $k=>$p){ ?>
          <option class="status-<?=$k;?>" value="<?=$k;?>" <?=($k==(!empty($d['google_statuss'])?$d['google_statuss']:1)?"selected":"")?>><?=$p;?></option>
        <?php } ?>
      </select>
      <script type="text/javascript">
      function autoSetEndDate(google_field) {

        if($('select[name=campaign_type]').val() == 'test' &&  !$('#reklidz').val() && $(google_field).val() == '4') { // 4 = Atslēgts

          $( "#set-enddate-dialog-confirm" ).dialog({
            resizable: false,
            height:140,
            width: 400,
            modal: true,
            buttons: {
              "Jā": function() {
                  var date = new Date;
                  var curr_date = ('0' + date.getDate()).slice(-2);
                  var curr_month = date.getMonth() + 1; //Months are zero based
                  var curr_year = date.getFullYear();
                  $('#reklidz').val(curr_date + "." + curr_month + "." + curr_year)

                  $( this ).dialog( "close" );
              },
              'Nē': function() {
                  $( this ).dialog( "close" );
              }
            }
          });
        }
      }
      </script>
      <div style="display:none; width: 400px;" id="set-enddate-dialog-confirm" title="Reklāmas beigu termiņš">
          <p><span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 20px 0;"></span>Vai uzlikt reklāmas beigu termiņā šodienas datumu?</p>
      </div>
    </td>
  </tr>

  <tr class="only_mpt">
    <td colspan="2">

      <table class="lig_pakalpojumi" width="100%">

        <tr>
          <th width="70"></th>
          <th width="170">Pakalpojums</th>
          <th width="50">Skaits</th>
          <th width="600">Atslēgvārdi</th>
          <th width="40">Top 3</th>
          <th width="40">Profils</th>
          <th width="20"></th>
          <th width="50">Domēns</th>
          <th width="60">Konts</th>
          <th width="64" title="Reklāmas materiāli">Rekl. mat.</th>
        </tr>

        <tr>

          <td class="t"><strong>Infomedia</strong></td>

          <td valign="top">

            <div id="infomedia_pak_place">
              <? if(!empty($pakalpojumi[25])) { ?>
                <? foreach($pakalpojumi[25] as $k => $v) { ?>

                  <div class="row">
                    <select class="<?php echo $pc_labot_pak;?>" name="pakalpojumi[<?= $v['id'] ;?>][pakalpojuma_veida_id]" style="width: 92%;">
                      <?php foreach($_vars['pakalpojumi'] as $key=>$pva){ ?>
                        <? if ($pva['parent_id'] == 25) { ?>
                          <option value="<?=$key;?>" <?=(($v['pakalpojuma_veida_id']==$key)?"selected":"");?>><?=$pva['nosaukums'];?></option>
                        <? } ?>
                      <?php } ?>
                    </select>
                    <?php if(check_access('ligumi-labot-pilns')) {?>
                      <a href="#" onclick="delPakalp('infomedia', this); return false;" class="hidethistoo"><img src="css/del.png" width="16" height="16" alt="Dzēst" title="Dzēst" /></a>
                    <?php } ?>
                  </div>

                <? } ?>
              <? } ?>
            </div>

            <div style="text-align: right;">
              <?php if(check_access('ligumi-labot-pilns')) {?>
                <a href="#" onclick="addPakalp('infomedia'); return false;" class="hidethistoo"><img src="css/add.png" width="16" height="16" alt="Pievienot jaunu" title="Pievienot jaunu" /></a>
              <?php } ?>
            </div>

          </td>

          <td valign="top" style="text-align: center;">

            <div id="infomedia_pak_skaits_place">
              <? if(!empty($pakalpojumi[25])) { ?>
                <? foreach($pakalpojumi[25] as $k => $v) { ?>
                  <div class="row">
                    <input type="input" class="<?php echo $pc_labot_pak;?>" name="pakalpojumi[<?= $v['id'] ;?>][pakalpojuma_apr]" value="<?=$v['pakalpojuma_apr'];?>" style="width:93%; text-align: center;">
                  </div>
                <? } ?>
              <? } ?>
            </div>

          </td>

          <td valign="top" colspan="4" style="padding:0;">

            <div id="infomedia_av_place">
                <table width="100%" cellspacing="0" border="0" class="invisible_table">
                    <tbody>
                  <? if(!empty($pielikumi_infomedia)) { ?>
                    <?php foreach($pielikumi_infomedia as $med) {
                      $did = 'piel_infomedia_nozare_' . $med['id'];
                      ?>
                      <tr class="row">
                        <td width="200"><input class="<?php echo $pc_labot_pak;?>" type="input" name="piel_infomedia[<?= $med['id'] ?>][atslegvards]" value="<?=$med['atslegvards'];?>" style="width:170px;"></td>
                        <td width="400">
                          <?php echo nozare_select_box('piel_infomedia['.$med['id'].'][nozare_id]', $med['nozare_id'], array('id' => $did, 'class' => $pc_labot_pak)); ?>
                          <?php echo nozare_popup_content($did);?>
                          <?php if(check_access('ligumi-labot-pilns')) {?>
                            <a href="#" class="hidethistoo limited_admin_edit" onclick="$('#<?php echo $did;?>_dialog').dialog({title:'Nozares'}); return false;">Izvēlēties</a>
                          <?php } ?>
                        </td>
                        <td width="40"></td>
                        <td width="40"><input class="<?php echo $pc_labot_pak;?>" type="checkbox" name="piel_infomedia[<?= $med['id'] ?>][atra_saite]" value="1" <?=$med['atra_saite'] ? 'CHECKED' : '';?>></td>
                        <td width="20">
                          <?php if(check_access('ligumi-labot-pilns')) {?>
                            <a href="#" onclick="delAtslegv('infomedia', this); return false;" class="hidethistoo"><img src="css/del.png" width="16" height="16" alt="Dzēst" title="Dzēst" /></a></td>
                          <?php } ?>
                      </tr>
                    <? } ?>
                  <? } ?>
                </tbody>
              </table>
            </div>

            <div style="text-align: right;">
              <?php if(check_access('ligumi-labot-pilns')) {?>
                <a href="#" onclick="addAtslegv('infomedia'); return false;" class="hidethistoo"><img src="css/add.png" width="16" height="16" alt="Pievienot jaunu" title="Pievienot jaunu" /></a>
              <?php } ?>
            </div>

          </td>

          <td>
            <? if(!empty($pielikumi_infomedia)) { ?>
              <? foreach($pielikumi_infomedia as $med){ ?>
                <div class="row">
                  <a target="_blank" href="<?php echo get_google_url_new($med['atslegvards'], 'lv') ?>">lv</a>
                </div>
              <? } ?>
            <? } ?>
          </td>

          <td valign="top" style="text-align: center;">

            <div id="infomedia_av_konts_place">
              <? if(!empty($pielikumi_infomedia)) { ?>
                <? foreach($pielikumi_infomedia as $med){ ?>
                  <div class="row">
                    <input class="<?php echo $pc_labot_pak;?>" type="input" name="piel_infomedia[<?= $med['id'] ?>][google_konts]" value="<?=$med['google_konts'];?>" style="width: 93%;">
                  </div>
                <? } ?>
              <? } ?>
            </div>

          </td>

          <td align="center"  valign="top">
            <select class="<?php echo $pc_labot_pak;?>" name="infomedia_reklamas_mat">
              <? $yes_no = array(1=>"Ir",2=>"Nav",3=>"Daļēji"); ?>
              <? foreach($yes_no as $k=>$p) { ?>
                <option value="<?=$k;?>" <?=($k==(!empty($d['infomedia_reklamas_mat'])?$d['infomedia_reklamas_mat']:2)?"selected":"")?>><?=$p;?></option>
              <? } ?>
            </select>
          </td>

        </tr>

        <tr>

          <td class="t"><strong>Google</strong></td>

          <td valign="top">

            <div id="tiessaite_pak_place">
              <? if(!empty($pakalpojumi[26])) { ?>
                <? foreach($pakalpojumi[26] as $k => $v) { ?>

                  <div class="row">
                    <select class="<?php echo $pc_labot_pak;?>" name="pakalpojumi[<?= $v['id'] ;?>][pakalpojuma_veida_id]" style="width: 92%;">
                      <?php foreach($_vars['pakalpojumi'] as $key=>$pva){ ?>
                        <? if ($pva['parent_id'] == 26) { ?>
                          <option value="<?=$key;?>" <?=(($v['pakalpojuma_veida_id']==$key)?"selected":"");?>><?=$pva['nosaukums'];?></option>
                        <? } ?>
                      <?php } ?>
                    </select>
                    <?php if(check_access('ligumi-labot-pilns')) {?>
                      <a href="#" onclick="delPakalp('tiessaite', this); return false;" class="hidethistoo"><img src="css/del.png" width="16" height="16" alt="Dzēst" title="Dzēst" /></a>
                    <?php } ?>
                  </div>

                <? } ?>
              <? } ?>
            </div>

            <div style="text-align: right;">
              <?php if(check_access('ligumi-labot-pilns')) {?>
                <a href="#" onclick="addPakalp('tiessaite'); return false;" class="hidethistoo"><img src="css/add.png" width="16" height="16" alt="Pievienot jaunu" title="Pievienot jaunu" /></a>
              <?php } ?>
            </div>

          </td>

          <td valign="top" style="text-align: center;">

            <div id="tiessaite_pak_skaits_place">
              <? if(!empty($pakalpojumi[26])) { ?>
                <? foreach($pakalpojumi[26] as $k => $v) { ?>

                  <div class="row">
                    <input class="<?php echo $pc_labot_pak;?>" type="input" name="pakalpojumi[<?= $v['id'] ;?>][pakalpojuma_apr]" value="<?=$v['pakalpojuma_apr'];?>" style="width:93%; text-align: center;">
                  </div>

                <? } ?>
              <? } ?>
            </div>

          </td>

          <td valign="top" colspan="4"  style="padding:0;">

            <div id="tiessaite_av_place">
              <table width="100%" cellspacing="0" border="0" class="invisible_table"><tbody>
              <? if(!empty($pielikumi_tiessaite)) { ?>

                <? foreach($pielikumi_tiessaite as $med){
                  $did = 'piel_tiessaite_nozare_' . $med['id'];
                  ?>
                  <tr class="row">
                    <td width="200"><input class="<?php echo $pc_labot_pak;?>" type="input" name="piel_tiessaite[<?= $med['id'] ?>][atslegvards]" value="<?=$med['atslegvards'];?>" style="width:170px;"></td>
                    <td width="400">
                      <?php echo nozare_select_box('piel_tiessaite['.$med['id'].'][nozare_id]', $med['nozare_id'], array('id' => $did, 'class' => $pc_labot_pak)); ?>
                      <?php echo nozare_popup_content($did);?>
                      <?php if(check_access('ligumi-labot-pilns')) { ?>
                        <a href="#" class="hidethistoo limited_admin_edit" onclick="$('#<?php echo $did;?>_dialog').dialog({title:'Nozares'}); return false;">Izvēlēties</a>
                      <?php } ?>
                    </td>
                    <td width="40"><input class="<?php echo $pc_labot_pak;?>" style="" type="checkbox" name="piel_tiessaite[<?= $med['id'] ?>][top3]" value="1" <?=$med['top3'] ? 'CHECKED' : '';?>></td>
                    <td width="40"></td>
                    <td width="20">
                      <?php if(check_access('ligumi-labot-pilns')) {?>
                        <a href="#" onclick="delAtslegv('tiessaite', this); return false;" class="hidethistoo"><img src="css/del.png" width="16" height="16" alt="Dzēst" title="Dzēst" /></a></td>
                      <?php } ?>
                  </tr>
                <? } ?>
              <? } ?>
              </tbody></table>
            </div>

            <div style="text-align: right;">
              <?php if(check_access('ligumi-labot-pilns')) {?>
                <a href="#" onclick="addAtslegv('tiessaite'); return false;" class="hidethistoo"><img src="css/add.png" width="16" height="16" alt="Pievienot jaunu" title="Pievienot jaunu" /></a>
              <?php } ?>
            </div>

          </td>

          <td valign="top">

            <div id="tiessaite_av_domeni_place">
              <? if(!empty($pielikumi_tiessaite)) { ?>
                <? foreach($pielikumi_tiessaite as $med){ ?>
                  <div class="row" style="padding-bottom: 1px;">
                    <input class="<?php echo $pc_labot_pak;?>" class="hidethistoo" type="input" name="piel_tiessaite[<?= $med['id'] ?>][domeni]" value="<?=$med['domeni'];?>" style="width: 93%;">
                    <span class="onlyinview">
                      <?
                      $domeni_parts = array_filter(array_map('trim', preg_split("/[\s,;]/", strtolower($med['domeni']))));
                      $first = true;

                      foreach($domeni_parts as $domens) {

                        echo !$first ? ', ' : '';

                        if ($url = get_google_url_new($med['atslegvards'], $domens)) {
                          echo '<a target="_blank" href="'.$url.'">' . $domens . '</a>';
                        } else {
                          echo $domens;
                        }

                        $first = false;

                      }
                      ?>

                    </span>
                  </div>
                <? } ?>
              <? } ?>
            </div>

          </td>

          <td valign="top" style="text-align: center;">

            <div id="tiessaite_av_konts_place">
              <? if(!empty($pielikumi_tiessaite)) { ?>
                <? foreach($pielikumi_tiessaite as $med){ ?>
                  <div class="row">
                    <input class="<?php echo $pc_labot_pak;?>" type="input" name="piel_tiessaite[<?= $med['id'] ?>][google_konts]" value="<?=$med['google_konts'];?>" style="width: 93%;">
                  </div>
                <? } ?>
              <? } ?>
            </div>

          </td>

          <td align="center"  valign="top">
            <select class="<?php echo $pc_labot_pak;?>" name="tiessaite_reklamas_mat">
              <? $yes_no = array(1=>"Ir",2=>"Nav",3=>"Daļēji"); ?>
              <? foreach($yes_no as $k=>$p) { ?>
                <option value="<?=$k;?>" <?=($k==(!empty($d['tiessaite_reklamas_mat'])?$d['tiessaite_reklamas_mat']:2)?"selected":"")?>><?=$p;?></option>
              <? } ?>
            </select>
          </td>

        </tr>

        <tr>

          <td class="t"><strong>Yandex</strong></td>

          <td valign="top">

            <div id="yandex_pak_place">
              <? if(!empty($pakalpojumi[30])) { ?>
                <? foreach($pakalpojumi[30] as $k => $v) { ?>

                  <div class="row">
                    <select class="<?php echo $pc_labot_pak;?>" name="pakalpojumi[<?= $v['id'] ;?>][pakalpojuma_veida_id]" style="width: 92%;">
                      <?php foreach($_vars['pakalpojumi'] as $key=>$pva){ ?>
                        <? if ($pva['parent_id'] == 30) { ?>
                          <option value="<?=$key;?>" <?=(($v['pakalpojuma_veida_id']==$key)?"selected":"");?>><?=$pva['nosaukums'];?></option>
                        <? } ?>
                      <?php } ?>
                    </select>
                    <?php if(check_access('ligumi-labot-pilns')) {?>
                      <a href="#" onclick="delPakalp('yandex', this); return false;" class="hidethistoo"><img src="css/del.png" width="16" height="16" alt="Dzēst" title="Dzēst" /></a>
                    <?php } ?>
                  </div>

                <? } ?>
              <? } ?>
            </div>

            <div style="text-align: right;">
              <?php if(check_access('ligumi-labot-pilns')) {?>
                <a href="#" onclick="addPakalp('yandex'); return false;" class="hidethistoo"><img src="css/add.png" width="16" height="16" alt="Pievienot jaunu" title="Pievienot jaunu" /></a>
              <?php } ?>
            </div>

          </td>

          <td valign="top" style="text-align: center;">

            <div id="yandex_pak_skaits_place">
              <? if(!empty($pakalpojumi[30])) { ?>
                <? foreach($pakalpojumi[30] as $k => $v) { ?>

                  <div class="row">
                    <input class="<?php echo $pc_labot_pak;?>" type="input" name="pakalpojumi[<?= $v['id'] ;?>][pakalpojuma_apr]" value="<?=$v['pakalpojuma_apr'];?>" style="width:93%; text-align: center;">
                  </div>

                <? } ?>
              <? } ?>
            </div>

          </td>

          <td valign="top" colspan="4"  style="padding:0;">

            <div id="yandex_av_place">
              <table cellspacing="0" width="100%" border="0" class="invisible_table"><tbody>
              <? if(!empty($pielikumi_yandex)) { ?>
                  <? foreach($pielikumi_yandex as $med){
                    $did = 'piel_yandex_nozare_' . $med['id'];
                    ?>
                    <tr class="row">
                      <td width="200"><input class="<?php echo $pc_labot_pak;?>" type="input" name="piel_yandex[<?= $med['id'] ?>][atslegvards]" value="<?=$med['atslegvards'];?>" style="width:170px;"></td>
                      <td width="400">
                        <?php echo nozare_select_box('piel_yandex['.$med['id'].'][nozare_id]', $med['nozare_id'], array('id' => $did, 'class' => $pc_labot_pak)); ?>
                        <?php echo nozare_popup_content($did);?>
                        <?php if(check_access('ligumi-labot-pilns')) {?>
                          <a href="#" class="hidethistoo limited_admin_edit" onclick="$('#<?php echo $did;?>_dialog').dialog({title:'Nozares'}); return false;">Izvēlēties</a>
                        <?php } ?>
                      </td>
                      <td width="40"></td>
                      <td width="40"></td>
                      <td width="20">
                        <?php if(check_access('ligumi-labot-pilns')) {?>
                          <a href="#" onclick="delAtslegv('yandex', this); return false;" class="hidethistoo"><img src="css/del.png" width="16" height="16" alt="Dzēst" title="Dzēst" /></a>
                        <?php } ?>
                      </td>
                    </tr>
                  <? } ?>
               <? } ?>
               </tbody></table>
            </div>

            <div style="text-align: right;">
              <?php if(check_access('ligumi-labot-pilns')) {?>
                <a href="#" onclick="addAtslegv('yandex'); return false;" class="hidethistoo"><img src="css/add.png" width="16" height="16" alt="Pievienot jaunu" title="Pievienot jaunu" /></a>
              <?php } ?>
            </div>

          </td>

          <td valign="top">

            <div id="yandex_av_domeni_place">
              <? if(!empty($pielikumi_yandex)) { ?>
                <? foreach($pielikumi_yandex as $med){ ?>
                  <div class="row">
                    <input class="<?php echo $pc_labot_pak;?>" class="hidethistoo" type="input" name="piel_yandex[<?= $med['id'] ?>][domeni]" value="<?=$med['domeni'];?>" style="width: 93%;">
                    <span class="onlyinview">
                      <?
                      $domeni_parts = array_filter(array_map('trim', preg_split("/[\s,;]/", strtolower($med['domeni']))));
                      $first = true;

                      foreach($domeni_parts as $domens) {

                        echo !$first ? ', ' : '';

                        if ($url = get_yandex_url($med['atslegvards'], $domens)) {
                          echo '<a target="_blank" href="'.$url.'">' . $domens . '</a>';
                        } else {
                          echo $domens;
                        }

                        $first = false;

                      }
                      ?>

                    </span>
                  </div>
                <? } ?>
              <? } ?>
            </div>

          </td>

          <td valign="top" style="text-align: center;">

            <div id="yandex_av_konts_place">
              <? if(!empty($pielikumi_yandex)) { ?>
                <? foreach($pielikumi_yandex as $med){ ?>
                  <div class="row">
                    <input class="<?php echo $pc_labot_pak;?>" type="input" name="piel_yandex[<?= $med['id'] ?>][konts]" value="<?=$med['konts'];?>" style="width: 93%;">
                  </div>
                <? } ?>
              <? } ?>
            </div>

          </td>

          <td align="center"  valign="top">
            <select class="<?php echo $pc_labot_pak;?>" name="yandex_reklamas_mat">
              <? $yes_no = array(1=>"Ir",2=>"Nav",3=>"Daļēji"); ?>
              <? foreach($yes_no as $k=>$p) { ?>
                <option value="<?=$k;?>" <?=($k==(!empty($d['yandex_reklamas_mat'])?$d['yandex_reklamas_mat']:2)?"selected":"")?>><?=$p;?></option>
              <? } ?>
            </select>
          </td>

        </tr>

        <tr>

          <td class="t"><strong>Cits</strong></td>

          <td valign="top">

            <div id="cits_pak_place">
              <? if(!empty($pakalpojumi[27])) { ?>
                <? foreach($pakalpojumi[27] as $k => $v) { ?>

                  <div class="row">
                    <select class="<?php echo $pc_labot_pak;?>" name="pakalpojumi[<?= $v['id'] ;?>][pakalpojuma_veida_id]" style="width: 92%;">
                      <?php foreach($_vars['pakalpojumi'] as $key=>$pva){ ?>
                        <? if ($pva['parent_id'] == 27) { ?>
                          <option value="<?=$key;?>" <?=(($v['pakalpojuma_veida_id']==$key)?"selected":"");?>><?=$pva['nosaukums'];?></option>
                        <? } ?>
                      <?php } ?>
                    </select>
                    <?php if(check_access('ligumi-labot-pilns')) {?>
                      <a href="#" onclick="delPakalp('cits', this); return false;" class="hidethistoo"><img src="css/del.png" width="16" height="16" alt="Dzēst" title="Dzēst" /></a>
                    <?php } ?>
                  </div>

                <? } ?>
              <? } ?>
            </div>

            <div style="text-align: right;">
              <?php if(check_access('ligumi-labot-pilns')) {?>
                <a href="#" onclick="addPakalp('cits'); return false;" class="hidethistoo"><img src="css/add.png" width="16" height="16" alt="Pievienot jaunu" title="Pievienot jaunu" /></a>
              <?php } ?>
            </div>

          </td>

          <td valign="top" style="text-align: center;">

            <div id="cits_pak_skaits_place">
              <? if(!empty($pakalpojumi[27])) { ?>
                <? foreach($pakalpojumi[27] as $k => $v) { ?>
                  <div class="row">
                    <input class="<?php echo $pc_labot_pak;?>" type="input" name="pakalpojumi[<?= $v['id'] ;?>][pakalpojuma_apr]" value="<?=$v['pakalpojuma_apr'];?>" style="width:93%; text-align: center;">
                  </div>
                <? } ?>
              <? } ?>
            </div>

          </td>

          <td valign="top" colspan="4" style="padding: 0;">

            <div id="citi_av_place">
              <table cellspacing="0" width="100%" border="0" class="invisible_table"><tbody>
              <? if(!empty($pielikumi_citi)) { ?>

                <? foreach($pielikumi_citi as $med){
                  $did = 'piel_citi_nozare_' . $med['id'];
                  ?>
                  <tr class="row">
                    <td width="200">
                      <input class="<?php echo $pc_labot_pak;?>" type="input" name="piel_citi[<?= $med['id'] ?>][atslegvards]" value="<?=$med['atslegvards'];?>" style="width:170px;">
                    </td>
                    <td width="400">
                      <?php echo nozare_select_box('piel_citi['.$med['id'].'][nozare_id]', $med['nozare_id'], array('id' => $did, 'class' => $pc_labot_pak)); ?>
                      <?php echo nozare_popup_content($did);?>
                      <?php if(check_access('ligumi-labot-pilns')) {?>
                        <a href="#" class="hidethistoo limited_admin_edit" onclick="$('#<?php echo $did;?>_dialog').dialog({title:'Nozares'}); return false;">Izvēlēties</a>
                      <?php } ?>
                    </td>
                    <td width="40"></td>
                    <td width="40"></td>
                    <td width="20">
                      <?php if(check_access('ligumi-labot-pilns')) {?>
                        <a href="#" onclick="delAtslegv('citi', this); return false;" class="hidethistoo"><img src="css/del.png" width="16" height="16" alt="Dzēst" title="Dzēst" /></a>
                      <?php } ?>
                    </td>
                  </tr>
                <? } ?>
              <? } ?>
              </tbody></table>
            </div>

            <div style="text-align: right;">
              <?php if(check_access('ligumi-labot-pilns')) {?>
                <a href="#" onclick="addAtslegv('citi'); return false;" class="hidethistoo"><img src="css/add.png" width="16" height="16" alt="Pievienot jaunu" title="Pievienot jaunu" /></a>
              <?php } ?>
            </div>

          </td>

          <td></td>

          <td valign="top" style="text-align: center;">

            <div id="citi_av_konts_place">
              <? if(!empty($pielikumi_citi)) { ?>
                <? foreach($pielikumi_citi as $med){ ?>
                  <div class="row">
                    <input class="<?php echo $pc_labot_pak;?>" type="input" name="piel_citi[<?= $med['id'] ?>][konts]" value="<?=$med['konts'];?>" style="width: 93%;">
                  </div>
                <? } ?>
              <? } ?>
            </div>

          </td>

          <td></td>

        </tr>

      </table>

    </td>
  </tr>

  <!-- MPK -->
  <tr class="only_mpk">
    <td colspan="2">
      <table class="lig_pakalpojumi mpk_tabula" width="100%">
        <thead>
          <tr>
            <th width="70"></th>
            <th width="100">Pakalpojums</th>
            <th>Detaļas</th>
          </tr>
        </thead>
        <tbody>
          <!-- Google -->

          <!-- Search -->
          <tr class="google">
            <td rowspan="4"><strong>Google</strong></td>
            <td>Search</td>
            <td style="padding:0;">
              <table class="keywords inner" width="100%" cellpadding="0" cellspacing="0">
                <thead>
                  <tr>
                    <th class="rl" width="154">A/V</th>
                    <th class="rl">Nozare</th>
                    <th class="rl" width="30">Top3</th>
                    <th class="rl" width="30"></th>
                    <th class="rl" width="75">Domēns</th>
                    <th class="rl" width="75">Konts</th>
                    <th width="75">Rek.mat.</th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach((array)$ligums->getMpkKeywords('google') as $keyword) {?>
                    <tr>
                      <td class="rl">
                        <input name="Keyword[<?php echo $keyword->id;?>][keyword]" value="<?php echo $keyword->keyword;?>" type="text" class="<?php echo $pc_labot_pak;?>">
                        <input type="hidden" name="Keyword[<?php echo $keyword->id;?>][site]" value="<?php echo $keyword->site;?>">
                      </td>
                      <td class="rl">
                        <?php
                        $did = 'keyword_' . $keyword->id;
                        echo nozare_select_box('Keyword['.$keyword->id.'][nozare_id]', $keyword->nozare_id, array('id' => $did, 'class' => $pc_labot_pak));
                        echo nozare_popup_content($did);
                        if(check_access('ligumi-labot-pilns')) { ?>
                          <a href="#" class="hidethistoo <?php echo $pc_labot_pak;?>" onclick="$('#<?php echo $did;?>_dialog').dialog({title:'Nozares'}); return false;">Izvēlēties</a>
                        <?php } ?>
                      </td>
                      <td class="rl">
                        <?php
                        $t3_id = 'keyword_top3_'. $keyword->id;
                        ?>
                        <input type="checkbox" <?php echo $keyword->top3 ? 'checked' : '';?> onclick="$('#<?php echo $t3_id;?>').val($(this).is(':checked') ? 1 : 0);" class="<?php echo $pc_labot_pak;?>">
                        <input type="hidden" id="<?php echo $t3_id;?>" name="Keyword[<?php echo $keyword->id;?>][top3]" value="<?php echo $keyword->top3;?>" class="<?php echo $pc_labot_pak;?>">
                      </td>
                      <td class="rl"><img onclick="mpkDelKw(this)" class="del_kw hidethistoo <?php echo $pc_labot_pak;?>" src="css/del.png" width="16" height="16" alt="Dzēst" title="Dzēst"></td>
                      <td class="domain rl"><input name="Keyword[<?php echo $keyword->id;?>][domens]" type="text" value="<?php echo $keyword->domens;?>" class="<?php echo $pc_labot_pak;?>"></td>
                      <td class="account rl"><input name="Keyword[<?php echo $keyword->id;?>][konts]" type="text" value="<?php echo $keyword->konts;?>" class="<?php echo $pc_labot_pak;?>"></td>
                      <td>
                        <select name="Keyword[<?php echo $keyword->id;?>][status]" class="<?php echo $pc_labot_pak;?>">
                          <option value="1" <?php echo $keyword->status == 1 ? 'selected' : '';?>>Ir</option>
                          <option value="2" <?php echo $keyword->status == 2 ? 'selected' : '';?>>Nav</option>
                          <option value="3" <?php echo $keyword->status == 3 ? 'selected' : '';?>>Daļēji</option>
                        </select>
                      </td>
                    </tr>
                  <?php } ?>
                </tbody>
              </table>
              <img onclick="mpkNewKw('google');" class="new_kw hidethistoo <?php echo $pc_labot_pak;?>" src="css/add.png" width="16" height="16" alt="Pievienot jaunu" title="Pievienot jaunu">
            </td>
          </tr>

          <!-- Display -->

          <tr class="google">
            <td>Display</td>
            <td style="padding: 0">
              <table class="inner" width="100%" cellpadding="0" cellspacing="0">
                <thead>
                  <th width="154" class="rl">Reklāmas tips</th>
                  <th >Tēma</th>
                  <th width="30"  class="rl"></th>
                  <th width="75" class="rl">Domēns</th>
                  <th width="75" class="rl">Konts</th>
                  <th width="75">Rek.mat.</th>
                </thead>
              </table>
              <?php
              $categories = $ligums->getMpkCategories('google');
              $display_misc = $ligums->getMpkDisplayMisc('google');
              ?>
              <div style="width: 158px; float: left; text-align: left;">
                <input type="checkbox" <?php echo $display_misc->text_ad ? 'checked' : '';?> name="DisplayMisc[google][text_ad]"> Teksta<br />
                <input type="checkbox" <?php echo $display_misc->image_ad ? 'checked' : '';?> name="DisplayMisc[google][image_ad]"> Attēla
              </div>
              <div style="overflow:hidden;position:relative; border-left: 1px solid #787878;">
                <table class="categories inner" width="100%" cellpadding="0" cellspacing="0">
                  <tbody>

                    <?php foreach($categories as $category) {?>
                    <tr>
                      <td>
                        <input name="Category[<?php echo $category->id;?>][name]" style="width:90%" value="<?php echo $category->name;?>" class="<?php echo $pc_labot_pak;?>">
                        <input type="hidden" name="Category[<?php echo $category->id;?>][site]" value="<?php echo $category->site;?>">
                      </td>
                      <td class="rl" width="29"><img onclick="mpkDelKw(this)" class="del_kw hidethistoo <?php echo $pc_labot_pak;?>" src="css/del.png" width="16" height="16" alt="Dzēst" title="Dzēst"></td>
                      <td class="domain rl" width="71"><input name="Category[<?php echo $category->id;?>][domens]" type="text" value="<?php echo $category->domens;?>" class="<?php echo $pc_labot_pak;?>"></td>
                      <td class="account rl" width="71"><input name="Category[<?php echo $category->id;?>][konts]" type="text" value="<?php echo $category->konts;?>" class="<?php echo $pc_labot_pak;?>"></td>
                      <td width="71">
                        <select name="Category[<?php echo $category->id;?>][status]" class="<?php echo $pc_labot_pak;?>">
                          <option value="1" <?php echo $category->status == 1 ? 'selected' : '';?>>Ir</option>
                          <option value="2" <?php echo $category->status == 2 ? 'selected' : '';?>>Nav</option>
                          <option value="3" <?php echo $category->status == 3 ? 'selected' : '';?>>Daļēji</option>
                        </select>
                      </td>
                    </tr>
                    <?php } ?>
                  </tbody>
                  </table>

                  <img onclick="mpkNewCat('google');" class="new_kw hidethistoo <?php echo $pc_labot_pak;?>" src="css/add.png" width="16" height="16" alt="Pievienot jaunu" title="Pievienot jaunu">

                  <table class="inner" width="100%" cellpadding="0" cellspacing="0">
                    <tbody>

                      <?php foreach($_vars['mpk_google_displays'] as $code) {
                        $display = $ligums->getMpkDisplay('google', $code);
                        ?>
                        <tr>
                          <td class="rl" style="text-align: left;"><input type="checkbox" <?php echo $display->enabled ? 'checked' : '';?> name="Display[google][<?php echo $code;?>][enabled]" class="<?php echo $pc_labot_pak;?>" /> <?php echo $display->getHumanName();?></td>
                          <td class="domain rl" width="71"><input name="Display[google][<?php echo $code;?>][domens]" type="text" value="<?php echo $display->domens;?>" class="<?php echo $pc_labot_pak;?>"></td>
                          <td class="account rl" width="71"><input name="Display[google][<?php echo $code;?>][konts]" type="text" value="<?php echo $display->konts;?>" class="<?php echo $pc_labot_pak;?>"></td>
                          <td width="71">
                            <select name="Display[google][<?php echo $code;?>][status]" class="<?php echo $pc_labot_pak;?>">
                              <option value="1" <?php echo $display->status == 1 ? 'selected' : '';?>>Ir</option>
                              <option value="2" <?php echo $display->status == 2 ? 'selected' : '';?>>Nav</option>
                              <option value="3" <?php echo $display->status == 3 ? 'selected' : '';?>>Daļēji</option>
                            </select>
                          </td>
                        </tr>
                      <?php } ?>
                    </tbody>
                  </table>
                </div>
            </td>
          </tr>

          <!-- Konversijas -->
          <?php
          $google_conversion = $ligums->getMpkConversion('google');
          ?>
          <tr class="google">
            <td>Konversijas</td>
            <td style="padding: 0;">
              <table class="inner" width="100%" cellpadding="0" cellspacing="0">
                <tbody>
                  <tr>
                    <td class="rl">
                      <input type="checkbox" <?php echo $google_conversion->enabled ? 'checked' : '';?> name="Conversion[google][enabled]" style="display:block; float: left;" class="<?php echo $pc_labot_pak;?>">
                      <textarea name="Conversion[google][comment]" style="width: 400px; float: left; margin-left: 10px;" <?php echo $pc_labot_pak;?>><?php echo $google_conversion->comment;?></textarea>
                    </td>
                    <td class="domain rl" width="71"></td>
                    <td class="account rl" width="71"><input name="Conversion[google][konts]" type="text" value="<?php echo $google_conversion->konts;?>" class="<?php echo $pc_labot_pak;?>"></td>
                    <td width="71"></td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>

          <!-- Analytics -->
          <?php
          $google_analytics = $ligums->getMpkAnalytics('google');
          ?>
          <tr class="google" >
            <td>Analytics</td>
            <td style="padding: 0;">
              <table class="inner" width="100%" cellpadding="0" cellspacing="0">
                <tbody>
                  <tr>
                    <td class="rl">
                      <input name="Analytics[google][enabled]" type="checkbox" <?php echo $google_analytics->enabled ? 'checked' : '';?> style="display:block; float: left;" class="<?php echo $pc_labot_pak;?>">
                      <textarea name="Analytics[google][comment]" style="width: 400px;  float: left; margin-left: 10px;"><?php echo $google_analytics->comment;?></textarea>
                    </td>
                    <td class="domain rl" width="71"></td>
                    <td class="account rl" width="71"></td>
                    <td width="71"></td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>

          <tr><td colspan="3"></td></tr>

          <!-- Yandex -->

          <!-- Direct -->
          <tr class="yandex">
            <td rowspan="3"><strong>Yandex</strong></td>
            <td>Рекламная сеть</td>
            <td style="padding:0;">
              <table class="keywords inner" width="100%" cellpadding="0" cellspacing="0">
                <thead>
                  <tr>
                    <th class="rl">A/V</th>
                    <th class="rl" width="400">Nozare</th>
                    <th class="rl" width="30">Top3</th>
                    <th class="rl" width="30"></th>
                    <th class="rl" width="75">Domēns</th>
                    <th class="rl" width="75">Konts</th>
                    <th width="75">Rek.mat.</th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach((array)$ligums->getMpkKeywords('yandex') as $keyword) {?>
                  <tr>
                    <td class="rl">
                      <input name="Keyword[<?php echo $keyword->id;?>][keyword]" value="<?php echo $keyword->keyword;?>" type="text" class="<?php echo $pc_labot_pak;?>">
                      <input type="hidden" name="Keyword[<?php echo $keyword->id;?>][site]" value="<?php echo $keyword->site;?>">
                    </td>
                    <td class="rl">
                      <?php
                      $did = 'keyword_' . 1;
                      echo nozare_select_box('Keyword['.$keyword->id.'][nozare_id]', $keyword->nozare_id, array('id' => $did, 'class' => $pc_labot_pak));
                      echo nozare_popup_content($did);
                      if(check_access('ligumi-labot-pilns')) { ?>
                        <a href="#" class="hidethistoo <?php echo $pc_labot_pak;?>" onclick="$('#<?php echo $did;?>_dialog').dialog({title:'Nozares'}); return false;">Izvēlēties</a>
                      <?php } ?>
                    </td>
                    <td class="rl">
                       <?php
                        $t3_id = 'keyword_top3_'. $keyword->id;
                        ?>
                        <input type="checkbox" <?php echo $keyword->top3 ? 'checked' : '';?> onclick="$('#<?php echo $t3_id;?>').val($(this).is(':checked') ? 1 : 0);" class="<?php echo $pc_labot_pak;?>">
                        <input type="hidden" id="<?php echo $t3_id;?>" name="Keyword[<?php echo $keyword->id;?>][top3]" value="<?php echo $keyword->top3;?>" class="<?php echo $pc_labot_pak;?>">
                    </td>
                    <td class="rl"><img onclick="mpkDelKw(this)" class="del_kw hidethistoo <?php echo $pc_labot_pak;?>" src="css/del.png" width="16" height="16" alt="Dzēst" title="Dzēst"></td>
                    <td class="domain rl"><input name="Keyword[<?php echo $keyword->id;?>][domens]" type="text" value="<?php echo $keyword->domens;?>" class="<?php echo $pc_labot_pak;?>"></td>
                    <td class="account rl"><input name="Keyword[<?php echo $keyword->id;?>][konts]" type="text" value="<?php echo $keyword->konts;?>" class="<?php echo $pc_labot_pak;?>"></td>
                    <td>
                      <select name="Keyword[<?php echo $keyword->id;?>][status]" class="<?php echo $pc_labot_pak;?>">
                        <option value="1" <?php echo $keyword->status == 1 ? 'selected' : '';?>>Ir</option>
                        <option value="2" <?php echo $keyword->status == 2 ? 'selected' : '';?>>Nav</option>
                        <option value="3" <?php echo $keyword->status == 3 ? 'selected' : '';?>>Daļēji</option>
                      </select>
                    </td>
                  </tr>
                  <?php } ?>
                </tbody>
              </table>
              <img onclick="mpkNewKw('yandex');" class="new_kw hidethistoo <?php echo $pc_labot_pak;?>" src="css/add.png" width="16" height="16" alt="Pievienot jaunu" title="Pievienot jaunu">
            </td>
          </tr>

          <!-- Konteksts -->
          <tr class="yandex">
            <td>Контекстная Рекалама</td>
            <td style="padding: 0;">
                <table class="inner" width="100%" cellpadding="0" cellspacing="0">
                <thead>
                  <th width="243px" class="rl">Papildus info</th>
                  <th>Tēma</th>
                  <th width="30" class="rl"></th>
                  <th width="75" class="rl">Domēns</th>
                  <th width="75" class="rl">Konts</th>
                  <th width="75">Rek.mat.</th>
                </thead>
                </table>
                <div style="width: 247px; float: left; border-right: 1px solid #787878">
                   <?php
                   $display_misc = $ligums->getMpkDisplayMisc('yandex');
                   ?>
                   <textarea name="DisplayMisc[yandex][description]" style="margin:3px; width: 230px;" rows="5" class="<?php echo $pc_labot_pak;?>"><?php echo $display_misc->description;?></textarea>
                </div>
                <div style="margin-left: 247px; float: right;">
                  <table class="categories inner" width="100%" cellpadding="0" cellspacing="0">
                  <tbody>
                    <?php foreach((array)$ligums->getMpkCategories('yandex') as $category) {?>
                    <tr>
                      <td>
                        <input name="Category[<?php echo $category->id;?>][name]" style="width:90%" value="<?php echo $category->name;?>" class="<?php echo $pc_labot_pak;?>">
                        <input type="hidden" name="Category[<?php echo $category->id;?>][site]" value="<?php echo $category->site;?>">
                      </td>
                      <td class="rl" width="29"><img onclick="mpkDelKw(this)" class="del_kw hidethistoo <?php echo $pc_labot_pak;?>" src="css/del.png" width="16" height="16" alt="Dzēst" title="Dzēst"></td>
                      <td class="domain rl" width="71"><input name="Category[<?php echo $category->id;?>][domens]" type="text" value="<?php echo $category->domens;?>" class="<?php echo $pc_labot_pak;?>"></td>
                      <td class="account rl" width="71"><input name="Category[<?php echo $category->id;?>][konts]" type="text" value="<?php echo $category->konts;?>" class="<?php echo $pc_labot_pak;?>"></td>
                      <td width="71">
                        <select name="Category[<?php echo $category->id;?>][status]" class="<?php echo $pc_labot_pak;?>">
                          <option value="1" <?php echo $category->status == 1 ? 'selected' : '';?>>Ir</option>
                          <option value="2" <?php echo $category->status == 2 ? 'selected' : '';?>>Nav</option>
                          <option value="3" <?php echo $category->status == 3 ? 'selected' : '';?>>Daļēji</option>
                        </select>
                      </td>
                    </tr>
                    <?php } ?>
                  </tbody>
                  </table>
                  <img onclick="mpkNewCat('yandex');" class="new_kw hidethistoo <?php echo $pc_labot_pak;?>" src="css/add.png" width="16" height="16" alt="Pievienot jaunu" title="Pievienot jaunu">
                </div>
            </td>
          </tr>

          <!-- Metrika -->
          <?php
          $yandex_metrika = $ligums->getMpkAnalytics('yandex');
          ?>
          <tr class="yandex">
            <td>Метрика</td>
            <td style="padding: 0;">
              <table class="inner" width="100%" cellpadding="0" cellspacing="0">
                <tbody>
                  <tr>
                    <td class="rl">
                      <input name="Analytics[yandex][enabled]" type="checkbox" <?php echo $yandex_metrika->enabled ? 'checked' : '';?> style="display:block; float: left;" class="<?php echo $pc_labot_pak;?>">
                      <textarea name="Analytics[yandex][comment]" style="width: 400px;  float: left; margin-left: 10px;"><?php echo $yandex_metrika->comment;?></textarea>
                    </td>
                    <td class="domain rl" width="71"></td>
                    <td class="account rl" width="71"></td>
                    <td width="71"></td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
          <tr class="other">
            <td><strong>Cits</strong></td>
            <td colspan="2">
              <?php
              $other = $ligums->getMpkOther();
              ?>
              <textarea rows="3" style="width:95%" name="Other[description]"><?php echo $other->description;?></textarea>
            </td>
          </tr>
        </tbody>
      </table>
    </td>
  </tr>
</table>
<div style="clear:both"></div>

<script>
function setPakalpojumsFields() {
  var type = $('#pakalpojums_tips').val();
  if(parseInt(type)) { // 0 - mpt, 1 - mpk
    //  Show mpk, hide mpt
    $('.only_mpk').show();
    $('.only_mpt').hide();

  } else {
    //  Show mpt, hide mpk
    $('.only_mpt').show();
    $('.only_mpk').hide();
  }
}

//  Jauns MPK atslēgvārda rinda
function mpkNewKw(variant) {
  var row = $('#mpk_keyword_prot .'+variant).clone().removeClass(variant);
  if(row) {
    row.appendTo($('.mpk_tabula tr.' + variant + ' table.keywords'));
  }
}

//  Jauna MPK tēmas rinda
function mpkNewCat(variant) {
  var row = $('#mpk_category_prot .'+variant).clone().removeClass(variant);
  if(row) {
    row.appendTo($('.mpk_tabula tr.' + variant + ' table.categories'));
  }
}

function mpkDelKw(obj) {
  $(obj).parent().parent().remove();
}

//  Ready
$(function(){
  setPakalpojumsFields()

  $('select[name="campaign_type"]').change(function(e){
    if($(e.target).val() == 'test') {
      $('#reklidz').val('');
    }
  });
});

function addPakalp(veids) {

  $('#'+veids+'_pak_place').append( $('#'+veids+'_pak_prot').html() );
  $('#'+veids+'_pak_skaits_place').append( $('#'+veids+'_pak_skaits_prot').html() );

};

function delPakalp(veids, button) {

  var pos = $(button).parents('div.row').index() + 1;

  $('#'+veids+'_pak_place > div:nth-child('+pos+')').remove();
  $('#'+veids+'_pak_skaits_place > div:nth-child('+pos+')').remove();


}

function addAtslegv(veids) {

  $('#'+veids+'_av_place table tbody').append( $('#'+veids+'_av_prot tbody').html() );

  if (veids == 'tiessaite' || veids == 'yandex') {
    $('#'+veids+'_av_domeni_place').append( $('#'+veids+'_av_domeni_prot').html() );
  }

  $('#'+veids+'_av_konts_place').append( $('#'+veids+'_av_konts_prot').html() );

};

function delAtslegv(veids, button) {

  var pos = $(button).parents('tr.row').index() + 1;

  $('#'+veids+'_av_place > table tbody tr:nth-child('+pos+')').remove();

  if (veids == 'tiessaite' || veids == 'yandex') {
    $('#'+veids+'_av_domeni_place > div:nth-child('+pos+')').remove();
  }

  $('#'+veids+'_av_konts_place > div:nth-child('+pos+')').remove();

}

</script>

<? show_comments_block('lig_pakalpojums', isset($_GET['id']) ? $_GET['id'] : null) ?>

<? show_files_block('lig_pakalpojums', isset($_GET['id']) ? $_GET['id'] : null, false, false, false, false, true) ?>