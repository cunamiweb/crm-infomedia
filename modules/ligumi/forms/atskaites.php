<?php
$adwords_accounts = $ligums->getAdwordsAccounts();
?>
<div id="atskaites">
  <table width="600" class="data_form layout1">
    <tr>
      <td class="label" width="160">Datuma diapazons:</td>
      <td>
      No <input class="showcalendar search_field" type="input" name="atskaite_no" style="width: 100px;" value="<?php echo isset($_GET['atskaite_no']) ? $_GET['atskaite_no'] : '';?>">
      līdz <input class="showcalendar search_field" type="input" name="atskaite_lidz" style="width: 100px;" value="<?php echo isset($_GET['atskaite_lidz']) ? $_GET['atskaite_lidz'] : '';?>">
      </td>
    </tr>

    <tr>

      <?php if(count($adwords_accounts) > 1 && 0) { // atslēgts ?>
        <td class="label" width="160">Konts:</td>
        <td>
          <select name="adwords_account_number" class="search_field">

            <?php foreach($adwords_accounts as $account) { ?>
            <option value="<?php echo $account;?>"><?php echo $account;?></option>
            <?php } ?>

          </select>
        </td>

      <?php } else { ?>

        <td colspan="2">
          <input type="hidden" name="adwords_account_number" value="<?php echo $adwords_accounts[0];?>">
        </td>

      <?php } ?>

    </tr>
    <tr>
    <td></td>
    <td>
    <a href="#" type="button" id="atskaite_button" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" aria-disabled="false">
      <span class="ui-button-text">Ģenerēt atskaiti</span>
    </a>

    <?php if(check_access('ligumi-testa-atskaite')) { ?>
      <a href="#" type="button" id="atskaite_test_button" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" aria-disabled="false">
        <span class="ui-button-text">Ģenerēt testa atskaiti</span>
      </a>
    <?php } ?>

    <script type="text/javascript">
      $("#atskaite_button").click(function(e){
        e.stopPropagation();
        url = "?c=ligumi&a=gen_atskaite&id=<?php echo $ligums->id;?>&" + $("#atskaites input, #atskaites select").serialize();
        window.location = url;
      });

      $("#atskaite_test_button").click(function(e){
        e.stopPropagation();
        url = "?c=ligumi&a=labot&subtab=4&testa_atskaite=1&id=<?php echo $ligums->id;?>&" + $("#atskaites input, #atskaites select").serialize();
        window.location = url;
      });
    </script>
    </td>
    </tr>
  </table>

  <?php
  if(check_access('ligumi-testa-atskaite') && isset($_GET['testa_atskaite'])) {

    include 'modules/ligumi/gen_testa_atskaite.php';
  } ?>
</div>

<? show_files_block('adwords_atskaite', isset($_GET['id']) ? $_GET['id'] : null, false, false, false, false, true) ?>