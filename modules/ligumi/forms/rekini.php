<?php $default_pvn = 21 ?>

<?php
$pc_labot_rekini = check_access('ligumi-labot-pilns') || check_access('ligumi-labot-rekini') ? 'canedit' : 'always_disabled';
?>

<input type="hidden" name="makeedit" value="true">

<div style="background-color:#f0f0f0; padding:5px; margin: 15px 5px 5px 5px;">

  <div style="float: left;">

    <span style="vertical-align: middle;">Rēķ. piegāde:</span>

    <?php foreach($_vars['rekina_piegade'] as $k => $p) { ?>
      <input style="vertical-align: middle; margin-left: 15px; margin-right: 0; padding-right: 0; width: auto;" class="<?php echo $pc_labot_rekini;?> radiobtnlistener" val="<?=$k;?>" type="checkbox" name="piegades[<?=$k;?>][check]" <?=(!empty($piegades[$k])) ? "checked" : "";?>>
      <span style="vertical-align: middle; font-weight: bold;"><?=$p;?></span>
      <input class="<?php echo $pc_labot_rekini;?>" id="adressfieldme<?=$k;?>" value="<?=!empty($piegades[$k])?$piegades[$k]['adrese']:""?>" name="piegades[<?=$k;?>][adrese]" type="input" style="width: <?= ($k == 1) ? 200 : 300; ?>px; vertical-align: middle;">
    <?php } ?>

  </div>

  <div style="float: right;">
    <span>Konts:</span>
    <select name="bank_account" id="bank_account" onchange="recreateRekiniNr()" class="<?php echo $pc_labot_rekini;?>">
      <option value="">Automātisks</option>
      <?php foreach($_vars['bank_account_numbers'] as $name => $number) { ?>
        <option value="<?php echo $number;?>" <?php echo $number == $ligums->bank_account ? 'selected' : '';?>><?php echo strtoupper($name);?></option>
      <?php } ?>
    </select>&nbsp;

    <span>Auto summas</span>
    <input class="<?php echo $pc_labot_rekini;?>" style="margin: 0 10px 0 0; width: auto; top: 1px; position: relative;" type="checkbox" id="auto_summas" name="auto_summas" value="1" <?= $d['auto_summas'] ? 'checked="checked"' : '' ?> />
    Soda procenti: <b><input type="input" value="<?=(float)$d['soda_proc'];?>" class="<?php echo $pc_labot_rekini;?>" name="soda_proc" style="width:30px;" id="soda_proc">%</b>,
    rēķinu skaits
    <button onclick="return false;" class="ui-state-default ui-corner-all pievienot_rekinu hidethistoo <?php echo $pc_labot_rekini;?>">+</button>
    <b><span id="text_rekinu_skaits"><?=count($r);?></span></b>
    <button  onclick="return false;" class="ui-state-default ui-corner-all dzest_rekinu hidethistoo <?php echo $pc_labot_rekini;?>">-</button>
  </div>

  <div class="clear"></div>

</div>

<table id="rekinu_tabula" style="width:1820px;" class="data">

  <thead>

    <tr class="last header">
      <th>PDF</th>
      <th width="65">Rēķina<br />numurs</th>
      <th width="90">Izrakstīšanas datums</th>
      <th width="65">Rēķina statuss</th>
      <th width="30">Summa<br />(bez PVN)</th>
      <th width="30">PVN&nbsp;%</th>
      <th width="30">PVN</th>
      <th width="30">Kopsumma<br />(ar PVN)</th>
      <th width="90">Apmaksas termiņš</th>
      <th width="85">Apmaksas statuss</th>
      <th>Kav. dienas/<br />Soda nauda</th>
      <th width="80">Apmaksātā summa</th>
      <th width="90">Apmaksas datums</th>
      <th width="30"></th>
      <th width="30"></th>
      <th width="120">Apmaksātais reklāmas budžets</th>
      <th width="120">Pārskaitītais reklāmas budžets</th>
      <th>Pārskaitījuma datums</th>
    </tr>

  </thead>

  <tbody class="main">

  <?
  $total_rek_apm = 0;
  $total_b_apm = false;
  $total_b_parsk = 0;

  foreach($r as $rekins) { ?>

    <tr class="tr_rek">
      <td class="c" style="white-space: nowrap; background: white;">
        <? if (check_access('ligumi-gen-rek')) { ?>

          <?
          $default_izr_datums = '';
          if (!empty($rekins['pedejais_apm_datums'])) {
            $default_izr_datums = date('d.m.Y', strtotime($rekins['pedejais_apm_datums']));
          } elseif (!empty($rekins['izr_datums'])) {
            $default_izr_datums = date('d.m.Y', strtotime($rekins['izr_datums']));
          }

          $default_apm_termins = '';
          if (!empty($rekins['pedejais_apm_datums'])) {
            $default_apm_termins = date('d.m.Y', strtotime($rekins['pedejais_apm_datums']));
          } elseif (!empty($rekins['apm_termins'])) {
            $default_apm_termins = date('d.m.Y', strtotime($rekins['apm_termins']));
          }
          ?>

          <a class="onlyinview" href="#" onclick="showGenPdfDialog(<?= $rekins['id'] ?>, '<?=$default_izr_datums?>', '<?=$default_apm_termins?>'); return false;" title="Ģenerēt PDF"><img src="css/pdf_edit.gif" alt="Ģenerēt PDF" style="vertical-align: middle;" /></a>

          <? if (!empty($rekins['fails_avansa_ep']) || !empty($rekins['fails_avansa_pp']) || !empty($rekins['fails_gala_ep']) || !empty($rekins['fails_gala_pp'])) { ?>

            <a class="onlyinview" href="#" onclick="showPdfDialog(<?= $rekins['id'] ?>); return false;" title="Lejupielādēt PDF"><img src="css/pdf_down.gif" alt="PDF" style="vertical-align: middle;" /></a>

            <div class="pdflinks" id="pdflinks<?= $rekins['id'] ?>">

              <? if (!empty($rekins['fails_avansa_ep']) && is_file("faili/" . $rekins['fails_avansa_ep'])) { ?>
                <div class="i">
                  <a href="?getfile=<?=$rekins['fails_avansa_ep'];?>" target="downloadframe" title="Lejupielādēt PDF">Avansa rēķins (pa e-pastu)</a>
                  <? if (!empty($piegades[1]['adrese'])) { ?>
                    <a onclick="sendRekToEmail(this, <?= $rekins['id'] ?>, 'fails_avansa_ep', '<?= addslashes($piegades[1]['adrese']) ?>'); return false;" href="#" title="Nosūtīt pa e-pastu"><img src="css/email.png" alt="Nosūtīt pa e-pastu" /></a>
                  <? } ?>
                </div>
              <? } ?>

              <? if (!empty($rekins['fails_avansa_pp']) && is_file("faili/" . $rekins['fails_avansa_pp'])) { ?>
                <div class="i">
                  <a href="?getfile=<?=$rekins['fails_avansa_pp'];?>" target="downloadframe" title="Lejupielādēt PDF">Avansa rēķins (pa pastu)</a>
                </div>
              <? } ?>

              <? if (!empty($rekins['fails_gala_ep']) && is_file("faili/" . $rekins['fails_gala_ep'])) { ?>
                <div class="i">
                  <a href="?getfile=<?=$rekins['fails_gala_ep'];?>" target="downloadframe" title="Lejupielādēt PDF">Gala rēķins (pa e-pastu)</a>
                  <? if (!empty($piegades[1]['adrese'])) { ?>
                    <a onclick="sendRekToEmail(this, <?= $rekins['id'] ?>, 'fails_gala_ep', '<?= addslashes($piegades[1]['adrese']) ?>'); return false;" href="#" title="Nosūtīt pa e-pastu"><img src="css/email.png" alt="Nosūtīt pa e-pastu" /></a>
                  <? } ?>
                </div>
              <? } ?>

              <? if (!empty($rekins['fails_gala_pp']) && is_file("faili/" . $rekins['fails_gala_pp'])) { ?>
                <div class="i">
                  <a href="?getfile=<?=$rekins['fails_gala_pp'];?>" target="downloadframe" title="Lejupielādēt PDF">Gala rēķins (pa pastu)</a>
                </div>
              <? } ?>

            </div>

            <script>
            $('#pdflinks<?= $rekins['id'] ?>').dialog({
              modal: true,
              autoOpen: false
            })
            </script>

          <? } ?>

        <? } ?>
      </td>
      <td class="rekinanrclass">
        <span style="font-size: 0.9em;"></span><input type="hidden" name="rekini[<?=$rekins['id'];?>][rek_nr]" value="" class="<?php echo $pc_labot_rekini;?>" />
      </td>
      <td class="c">
        <input type="hidden" name="rekini[<?=$rekins['id'];?>][bank_account]" class="bank_account" value="<?php echo $rekins['bank_account'];?>">
        <input style="width: 6em; text-align: center;" type="input" name="rekini[<?=$rekins['id'];?>][izr_datums]" value="<?=!empty($rekins['izr_datums'])?date('d.m.Y', strtotime($rekins['izr_datums'])):"";?>" class="showcalendar <?php echo $pc_labot_rekini;?>"></td>
      <td class="c">
        <select name="rekini[<?=$rekins['id'];?>][statuss]" class="<?php echo $pc_labot_rekini;?>">
          <option value="1" <?=(!empty($rekins['statuss']) && $rekins['statuss'] == 1)?"selected":"";?>>Nav</option>
          <option value="2" <?=(!empty($rekins['statuss']) && $rekins['statuss'] == 2)?"selected":"";?>>Izsūtīts</option>
          <option value="3" <?=(!empty($rekins['statuss']) && $rekins['statuss'] == 3)?"selected":"";?>>Apstipr.</option>
        </select>
      </td>
      <td class="r"><input style="width: 4em; text-align: right;" type="input" class="summa <?php echo $pc_labot_rekini;?>" value="<?=!empty($rekins['summa'])?$rekins['summa']:"";?>" name="rekini[<?=$rekins['id'];?>][summa]"></td>
      <td class="r"><input style="width: 3em; text-align: right;" type="input" class="pvn <?php echo $pc_labot_rekini;?>" / value="<?=!empty($rekins['pvn'])?$rekins['pvn']:"";?>" name="rekini[<?=$rekins['id'];?>][pvn]"></td>
      <td style="white-space: nowrap;" class="r"><div class="pvnsumma" style="text-align:right;"><?= !empty($rekins['kopsumma']) ? format_currency($rekins['kopsumma'] - $rekins['summa']): "" ;?></div></td>
      <td class="r"><input style="width: 4em; text-align: right; font-weight: bold;" type="input" class="kopsumma <?php echo $pc_labot_rekini;?>" value="<?=!empty($rekins['kopsumma'])?$rekins['kopsumma']:"";?>" name="rekini[<?=$rekins['id'];?>][kopsumma]"></td>
      <td class="c"><input style="width: 6em; text-align: center; font-weight: bold;" type="input" name="rekini[<?=$rekins['id'];?>][apm_termins]" class="showcalendar calendarcalc apm_termins <?php echo $pc_labot_rekini;?>" value="<?=!empty($rekins['apm_termins'])?date('d.m.Y', strtotime($rekins['apm_termins'])):"";?>"></td>
      <td class="c"><span class="date_status" style="display: block;"></span></td>
      <td class="c">
        <span class="soda_nauda" style="display: block; height: 1.6em;"></span>
        <span class="<?= (empty($rekins['apm_soda_summa'])) ?> '<?php echo $pc_labot_rekini;?>' : ''?>" style="font-size: 0.8em; white-space: nowrap;">Samaksāts: <input class="<?php echo $pc_labot_rekini;?>" style="width: 3em;" type="input" value="<?=!empty($rekins['apm_soda_summa'])?$rekins['apm_soda_summa']:"";?>" name="rekini[<?=$rekins['id'];?>][apm_soda_summa]"></span>
      </td>
      <td class="c">
        <?php $total_rek_apm += $rekins['apm_summa'];?>
        <span class="apm_summa" style="vertical-align: middle;"><?=!empty($rekins['apm_summa']) && $rekins['apm_summa'] != '0.00'?$rekins['apm_summa']:"";?></span>
        <? if (check_access('ligumi-rekinu-apmaksas')) { ?>
          <a style="vertical-align: middle;" class="onlyinview" href="#" onclick="showRekiniApmaksas(<?= $rekins['id'] ?>); return false;" title="Atvērt rēķina apmaksas"><img src="css/plus.gif" alt="Apmaksas" style="vertical-align: middle;" /></a>
        <? } ?>
      </td>
      <td class="c"><?=!empty($rekins['pedejais_apm_datums'])?date('d.m.Y', strtotime($rekins['pedejais_apm_datums'])):"";?></td>
      <td valign="top" align="left" style="white-space: nowrap;">

        <div style="font-size: 0.8em; height: 22px;">
          <input type="checkbox" name="rekini[<?=$rekins['id'];?>][anulets]" class="anulets calendarcalc <?php echo $pc_labot_rekini;?>" <?=!empty($rekins['anulets'])?"checked":"";?>> Anulēts
          <input style="width: 6em;" type="input" name="rekini[<?=$rekins['id'];?>][anul_datums]" class="showcalendar <?php echo $pc_labot_rekini;?>" value="<?=!empty($rekins['anul_datums'])?date('d.m.Y', strtotime($rekins['anul_datums'])):"";?>">
        </div>

        <div style="font-size: 0.8em;">
          <input type="checkbox" name="rekini[<?=$rekins['id'];?>][nodots_piedz]" class="nodots_piedz calendarcalc <?php echo $pc_labot_rekini;?>" <?=!empty($rekins['nodots_piedz'])?"checked":"";?>> Nodots piedz.
          <input style="width: 6em;" type="input" name="rekini[<?=$rekins['id'];?>][nodots_piedz_datums]" class="showcalendar <?php echo $pc_labot_rekini;?>" value="<?=!empty($rekins['nodots_piedz_datums'])?date('d.m.Y', strtotime($rekins['nodots_piedz_datums'])):"";?>">
        </div>

      </td>
      <td valign="top" style="white-space: nowrap;">

        <div style="font-size: 0.8em; height: 22px;">
          <input type="checkbox" name="rekini[<?=$rekins['id'];?>][barteris]" class="barteris calendarcalc <?php echo $pc_labot_rekini;?>" <?=!empty($rekins['barteris'])?"checked":"";?>> Barteris
        </div>

        <div style="font-size: 0.8em;">
          <input type="checkbox" name="rekini[<?=$rekins['id'];?>][atkapsanas_rekins]" class="atkapsanas_rekins calendarcalc <?php echo $pc_labot_rekini;?>" <?=!empty($rekins['atkapsanas_rekins'])?"checked":"";?>> Atkāpš. rēķ.
        </div>

      </td>
      <td>
        <?php
        $apm_summa_bez_pvn = $rekins['apm_summa'] / (1 + $rekins['pvn']/100);
        //$apm_summa_bez_pvn = $rekins['apm_summa'];

        if($rekins['apm_budzets'] !== null) {
          $budzets = $rekins['apm_budzets'];
        } else if($ligums->mpk) {
          $budzets = ($apm_summa_bez_pvn - $apm_summa_bez_pvn * ($d['apkalp_maksa_proc']/100));
        } else {
          $budzets = false;
        }

        if($budzets !== false) {
          $total_b_apm += $budzets;
          $proc = bccomp($apm_summa_bez_pvn, 0, 2) ? number_format(round($budzets / $apm_summa_bez_pvn * 100, 1), 1) : '- ';
          ?>
          <span class="rek_budzets_ls" style="vertical-align: middle;"><?php echo number_format($budzets, 2);?><span>
        <?php } ?>

        <? if (check_access('ligumi-rekinu-apmaksas')) { ?>
          <a style="vertical-align: middle;" class="onlyinview" href="#" onclick="setManualBApm(<?= $rekins['id'] ?>, '<?php echo $budzets !== false ? $budzets : 0;?>'); return false;" title="Manuāli iestatīt apmaksātā budžeta summu"><img src="css/plus.gif" alt="Apmaksas" style="vertical-align: middle;" /></a>
        <? } ?>

        <?php if($budzets !== false) { ?>
          <br />
          <?php echo $proc . '%'; ?>
        <?php } ?>

      </td>
      <td>
        <span class="rek_budzets_ls" style="vertical-align: middle;"><?=!empty($rekins['parsk_budzets']) && $rekins['parsk_budzets'] != '0.00'?$rekins['parsk_budzets']:"";?></span>

        <? if (check_access('ligumi-rekinu-apmaksas')) { ?>
          <a style="vertical-align: middle;" class="onlyinview" href="#" onclick="showReklamasBudzets(<?= $rekins['id'] ?>); return false;" title="Atvērt reklāmas budžeta apmaksas"><img src="css/plus.gif" alt="Pārskaitījumi" style="vertical-align: middle;" /></a>
        <? } ?>
        <br />
        <?php
        $total_b_parsk += $rekins['parsk_budzets'];
        if(!bccomp($apm_summa_bez_pvn, 0, 2)) {
          echo '- %';
        } else {
          echo number_format(round($rekins['parsk_budzets']/$apm_summa_bez_pvn * 100, 1), 1) . '%';
        }
        ?>
      </td>
      <td class="c">
        <span class="rek_budzets_ls" style="vertical-align: middle;"><?php echo $rekins['pedejais_b_apm_datums'] ? date('d.m.Y', strtotime($rekins['pedejais_b_apm_datums'])) : '';?></span>

      </td>
    </tr>

  <? } ?>

  </tbody>

  <tbody>

    <tr>
      <td colspan="4" align="right" valign="top"><strong>Summa:</strong></td>
      <td valign="top" class="r"><input type="input" id="full_summa" value="0" style="width:70px; border:0px; text-align: right;" readonly="true"></td>
      <td></td>
      <td valign="top" class="r"><input type="input" id="full_pvn" value="0" style="width:70px; border:0px; text-align: right;" readonly="true"></td>
      <td valign="top" class="r"><input type="input" id="full_kopsumma" value="0" style="width:70px; border:0px; text-align: right; font-weight: bold;" readonly="true"></td>
      <td align="right" colspan="3"><strong>Kopā EUR:<br>Kopā %:</strong></td>
      <td align="center"><span id="atmsumm"></span><br><span id="atmproc"></span>%<br></td>
      <td></td>
      <td></td>
      <td></td>
      <td>
        <?php if($total_b_apm !== false) { ?>
          <?php echo number_format($total_b_apm, 2);?>
          <br />
          <?php echo $total_rek_apm ? number_format(round($total_b_apm / $total_rek_apm * 100, 1), 1) : '- ';?>%
        <?php } ?>
      </td>
      <td>
        <?php echo number_format($total_b_parsk, 2);?>
        <br />
        <?php echo $total_rek_apm ? number_format(round($total_b_parsk / $total_rek_apm * 100, 1), 1) : '- ';?>%
      </td>
      <td colspan="100"></td>
    </tr>

    <tr>
      <td colspan="3" align="right" valign="top"><strong>Apkalpošanas maksa:</strong></td>
      <td><input type="text" class="<?php echo $pc_labot_rekini;?>" value="<?php echo isset($d['apkalp_maksa_proc']) ? $d['apkalp_maksa_proc'] : 0;?>" id="apkalp_maksa_proc" name="apkalp_maksa_proc" style="width: 50px; text-align: right;"> %</td>
      <td valign="top" class="r"><input type="input" id="apkalp_maksa_summa" value="0" style="width:70px; border:0px; text-align: right;" readonly="true"></td>
      <td></td>
      <td valign="top" class="r"><input type="input" id="apkalp_maksa_pvn" value="0" style="width:70px; border:0px; text-align: right;" readonly="true"></td>
      <td valign="top" class="r"><input type="input" id="apkalp_maksa_kopsumma" value="0" style="font-weight: bold;width:70px; border:0px; text-align: right;" readonly="true"></td>
      <td></td>
      <td valign="top" colspan="100"></td>
    </tr>

  </tbody>

</table>

<?
include('modules/ligumi/rekinu_rekviziti.php');
?>

<script>
$('.radiobtnlistener').click(function() {

  if($(this).attr('val') == 1 && $(this).prop('checked')){

    $('.ligmasledzejcheck:checked').each(function() {
      var email = $(this).parents('table').filter(':first').find('.readthismail').val();
      $('#adressfieldme1').attr('value', email);
    })

  }

  if($(this).attr('val') == 2 && $(this).prop('checked')){
    $('#adressfieldme2').attr('value', $('#add1').val() + ", " + $('#add2').val() + ", " + $('#add3').val() );
  }

});

function showGenPdfDialog(rek_id, default_izr_datums, default_apm_termins) {

	<? /* '<div style="text-align:left;"><input type="radio" name="from" value="infomedia" id="from_infomedia" style="width:8px;" checked /> <label for="from_infomedia"><?=htmlspecialchars($rekinu_rekviziti['infomedia']['nosauk']); ?></label></div>'+ */ ?>

  var form = $(
    '<form class="liggenpdfform" method="get" action="">' +
      '<div class="i"><button class="ui-state-default ui-corner-all" onclick="window.location.href=\'?c=ligumi&a=gen_rek<?php echo $ligums->mpk ? '_mpk' : '';?>&tips=avansa&rek_id='+rek_id+'\&from=\'+$(\'form.liggenpdfform input[name=&quot;from&quot;]:checked\').val(); return false;">Izveidot avansa rēķinu</button></div>' +
      '<div class="i"><button class="ui-state-default ui-corner-all" onclick="$(this).parent().next().next().toggle(); return false;">Izveidot gala rēķinu</button></div>' +
			'<div style="text-align:left;"><input type="radio" name="from" value="leads" id="from_leads" style="width:8px;" checked /> <label for="from_leads"><?=htmlspecialchars($rekinu_rekviziti['leads']['nosauk']); ?></label><br><br></div>'+
      '<div class="gala_block">' +
        '<input type="hidden" name="c" value="ligumi" />' +
        '<input type="hidden" name="a" value="gen_rek<?php echo $ligums->mpk ? '_mpk' : '';?>">' +
        '<input type="hidden" name="tips" value="gala" />' +
        '<input type="hidden" name="rek_id" value="'+rek_id+'" />' +
        '<div style="margin-bottom: 5px; text-align: right; padding-right: 35px;">Izr. datums: <input class="showcalendar" type="text" name="izr_datums" value="'+default_izr_datums+'" /></div>' +
        '<div style="text-align: right; padding-right: 35px;">Apm. termiņš: <input class="showcalendar" type="text" name="apm_termins" value="'+default_apm_termins+'" /></div>' +
        '<input class="subm" type="submit" value="Izveidot" />' +
      '</div>' +
    '</form>'
  );

  $(form).dialog({
    modal: true
  });

  $(".showcalendar", form).datepicker({dateFormat:'dd.mm.yy'});

}

function showPdfDialog(rek_id) {

  $('#pdflinks' + rek_id).dialog('open');

}

function showRekiniApmaksas(rek_id) {

  $('<iframe src="?c=ligumi&a=rek_apmaksas&rek_id='+rek_id+'&without_nav=1" />').dialog({
      title: 'Rēķina apmaksas',
      autoOpen: true,
      width: 800,
      height: 500,
      modal: true,
      resizable: false,
      draggable: false,
      closeOnEscape: false,
      close: function() {
        document.location = $('#fullformplace').attr('action') +  '&subtab=2';
      }
  }).width(780);

}

function showReklamasBudzets(rek_id) {

  $('<iframe src="?c=ligumi&a=budzeta_parskaitijumi&rek_id='+rek_id+'&without_nav=1" />').dialog({
      title: 'Reklāmas budžeta pārskaitījumi',
      autoOpen: true,
      width: 800,
      height: 500,
      modal: true,
      resizable: false,
      draggable: false,
      closeOnEscape: false,
      close: function() {
        document.location = $('#fullformplace').attr('action') +  '&subtab=2';
      }
  }).width(780);

}

function sendRekToEmail(a, rek_id, tips, epasts) {

  if (!confirm('Vai nosūtīt rēķinu uz klienta e-pastu "'+epasts+'"?')) {
    return false;
  }

  var loading_html = '<img style="margin-left: 5px;" alt="Notiek ielāde..." src="css/loading.gif" />';

  $(a).after(loading_html);

  var params = {
    rek_id: rek_id,
    tips: tips
  }

  $.get('ajax.php?action=send_rek_to_email', params, function(result) {

    if (result['success']) {
      alert('Rēķins veiksmīgi nosūtīts!');
      window.location="?c=ligumi&a=labot&id=<?php echo $ligums->id;?>&subtab=2";
    } else {
      alert(result['error']);
    }

    $(a).next().remove();

  }, 'json');

}

function isMpkBankAccount(row)
{
  if(row.find('.bank_account').val()) {
    if(row.find('.bank_account').val() == "<?php echo $_vars['bank_account_numbers']['mpk'];?>") {
      return true;
    } else {
      return false;
    }
  }

  //  MPK konts jau saglabāts pie rēķina
  if($('#bank_account').val() == '<?php echo $_vars['bank_account_numbers']['mpk'];?>') {
    return true;
  }

  //  Nav norādīts konts
  if(!$('#bank_account').val()) {

    // MPK pakalpojums
    if($('#pakalpojums_tips').val() == '1') {
      return true;
    }

    var pardeveja_id = $('select[name=pardeveja_id]').val();

    //  Līguma slēdzējs ir korporatīvais
    if(pardeveja_id == '<?php echo KORP_VADITAJA_ID;?>') {
      return true;
    }

    //  Līguma slēdzējs ir partneris
    var partner_ids = [<?php echo '"'. implode('", "', modelsToList(Pardevejs::model()->partner()->findAll(), 'id')) . '"';?>];
    if($.inArray(pardeveja_id, partner_ids) != -1) {
      return true;
    }
  }

  return false;
}

function recreateRekiniNr() {

  var i = 0;
  var ligumnr = $('#orgligumanr').val();

  if($('.rekinanrclass').length == 1) {

    var row = $('.rekinanrclass').parents('tr.tr_rek');

    var nr = ligumnr;
    if (row.find('.barteris').is(':checked')) {
      nr += '/B';
    } else if (row.find('.atkapsanas_rekins').is(':checked')) {
      nr += '/A';
    }

    //  -P ja ir mpk konts
    if(isMpkBankAccount(row)) {
      nr += '-P';
    }

    $('.rekinanrclass span').html(nr);
    $('.rekinanrclass input').val(nr);

  } else {

    jQuery.each($('.rekinanrclass'), function(){

      i++;

      var row = $(this).parents('tr.tr_rek');

      var nr = ligumnr;

      if (row.find('.barteris').is(':checked')) {
        nr += '/B';
      } else if (row.find('.atkapsanas_rekins').is(':checked')) {
        nr += '/A';
      } else {
        nr += '/' + i;
      }

      //  -P ja ir mpk konts
      if(isMpkBankAccount(row)) {
        nr += '-P';
      }

      $('span', this).html(nr);
      $('input', this).val(nr);

    });

  }

}

$(document).ready(function() {

  recalcKopsummas();
recalcStatusi();
  recreateRekiniNr();

  $('.pvn').keyup(function() {

    processRekRow($(this).parents('tr').find('input.summa'));

    recalcKopsummas();
    recalcStatusi();

  });

  $('.calendarcalc').change(function() {

    recalcKopsummas();
    recalcStatusi();

  });

  $('.summa, .kopsumma').keyup(function() {

    processRekRow($(this));

    recalcKopsummas();
    recalcStatusi();

  });

  $(".showcalendar").datepicker({dateFormat:'dd.mm.yy'});

  $('#rekinu_tabula input.anulets').click(processAnuletsClick);

  $('#rekinu_tabula input.nodots_piedz').click(processNodotsPiedzClick);

  $('#rekinu_tabula input.barteris').click(processBarterisClick);

  $('#rekinu_tabula input.atkapsanas_rekins').click(processAtkapsanaClick);

  $('#auto_summas').click(function() {

    if ($(this).is(':checked') == false) {
      return true;
    }

    if (confirm('Rēķinu kopsummas tiks automātiski pārrēķinātas! Vai turpināt?')) {

      $('#rekinu_tabula input.summa').each(function() {
        processRekRow($(this));
      });

      recalcKopsummas();
      recalcStatusi();

      return true;

    }

    return false;

  })

  stripeTable($("#rekinu_tabula"));

  $('.dzest_rekinu').click(function(){

    $('#rekinu_tabula tbody.main tr:last').remove();

    recalcKopsummas();
    recreateRekiniNr();

    stripeTable($("table#rekinu_tabula"));

    $('#text_rekinu_skaits').html($('.rekinanrclass').length);

  });

  $('.pievienot_rekinu').click(function(){

    v =  '<tr class="tr_rek"><td></td><td class="rekinanrclass"><span style="font-size: 0.9em;"></span><input type="hidden" name="rekini_new[rek_nr][]" value="" /></td>'+
       '<td><input style="width: 6em;" type="input" name="rekini_new[izr_datums][]" value="" class="showcalendar"></td>'+
       '<td><select name="rekini_new[statuss][]"><option value="1">Nav</option><option value="2">Izsūtīts</option><option value="3">Apstipr.</option>'+
       '</select></td>'+
       '<td style="text-align: right;"><input style="width: 4em; text-align: right;" type="input" class="summa" value="" name="rekini_new[summa][]"></td>'+
       '<td style="text-align: right;"><input style="width: 3em; text-align: right;" type="input" class="pvn" value="<?= $default_pvn ?>" name="rekini_new[pvn][]"></td>'+
       '<td><div class="pvnsumma" style="text-align:center;"></div></td>'+
       '<td style="text-align: right;"><input style="width: 4em; text-align: right; font-weight: bold;" type="input" class="kopsumma" value="" name="rekini_new[kopsumma][]"></td>'+
       '<td><input style="width: 6em;" type="input" name="rekini_new[apm_termins][]" class="showcalendar calendarcalc" value=""></td>'+
       '<td align="center" style="text-align:center;"><span class="date_status" style="display: block;"></span></td>'+
       '<td></td>'+
       '<td></td>'+
       '<td></td>'+
       '<td valign="top" align="left" style="white-space: nowrap;">' +
         '<div style="font-size: 0.8em; height: 22px;">' +
           '<input type="checkbox" name="rekini_new[anulets][]" class="anulets calendarcalc"> Anulēts' +
           '<input style="width: 6em;" type="input" name="rekini_new[anul_datums][]" class="showcalendar" value=""><br />' +
         '</div>' +
         '<div style="font-size: 0.8em;">' +
           '<input type="checkbox" name="rekini_new[nodots_piedz][]" class="nodots_piedz calendarcalc"> Nodots piedz.' +
           '<input style="width: 6em;" type="input" name="rekini_new[nodots_piedz_datums][]" class="showcalendar" value="">' +
         '</div>' +
       '</td>' +
       '<td valign="top" style="white-space: nowrap;">'+
         '<div style="font-size: 0.8em; height: 22px;">' +
           '<input type="checkbox" name="rekini_new[barteris][]" class="barteris calendarcalc"> Barteris' +
         '</div>' +
         '<div style="font-size: 0.8em;">' +
           '<input type="checkbox" name="rekini_new[atkapsanas_rekins][]" class="atkapsanas_rekins calendarcalc"> Atkāpš. rēķ.' +
         '</div>' +
       '</td>' +
       '<td></td>' +
       '<td></td>' +
       '<td></td>' +
       '</tr>';

    $('#rekinu_tabula tbody.main').append( v );

    var new_row = $('#rekinu_tabula tbody.main tr:last');

    new_row.find('.summa, .kopsumma').keyup(function() {

      processRekRow($(this));

      recalcKopsummas();
      recalcStatusi();

    });

    new_row.find('.pvn').keyup(function() {

      processRekRow($(this).parents('tr').find('input.summa'));

      recalcKopsummas();
      recalcStatusi();

    });

    new_row.find('.calendarcalc').change(function() {

      recalcKopsummas();
      recalcStatusi();

    });

    new_row.find(".showcalendar").datepicker({dateFormat:'dd.mm.yy'});

    new_row.find('input.barteris').click(processBarterisClick);
    new_row.find('input.atkapsanas_rekins').click(processAtkapsanaClick);

    new_row.find('input.anulets').click(processAnuletsClick);
    new_row.find('input.nodots_piedz').click(processNodotsPiedzClick);

    recalcStatusi();
    recreateRekiniNr();

    stripeTable($("table#rekinu_tabula"));

    $('#text_rekinu_skaits').html($('.rekinanrclass').length);

  });

  function recalcKopsummas() {

    var full_summa = 0;
    var full_pvn = 0;
    var full_kopsumma = 0;

    //  Apkalpošanas maksas aprēķini
    var apkalp_maksa_proc = $('#apkalp_maksa_proc').val()/100 || 0;
    var apkalp_maksa_summa = 0;
    var apkalp_maksa_pvn = 0;
    var apkalp_maksa_kopsumma = 0;

    var apmkopsumma = 0;

    $('tr.tr_rek').each(function() {

      var rek_row = $(this);

      var summa_inp = rek_row.find('input.summa');
      var kopsumma_inp = rek_row.find('input.kopsumma');

      var atkaps_rekins = rek_row.find('input.atkapsanas_rekins').is(':checked');
      var apm_summa = rek_row.find('.apm_summa').html();

      if (!atkaps_rekins) {

        if(isNumber(summa_inp.val())) {
          full_summa += parseFloat(ctop(summa_inp.val()));
          apkalp_maksa_summa += parseFloat(ctop(summa_inp.val())) * apkalp_maksa_proc;
        }

        if(isNumber(kopsumma_inp.val())) {
          full_kopsumma += parseFloat(ctop(kopsumma_inp.val()));
          apkalp_maksa_kopsumma += parseFloat(ctop(kopsumma_inp.val())) *  apkalp_maksa_proc;
        }

        if(isNumber(summa_inp.val()) && isNumber(kopsumma_inp.val())) {
          full_pvn += parseFloat(ctop(kopsumma_inp.val())) - parseFloat(ctop(summa_inp.val()));
          apkalp_maksa_pvn += (parseFloat(ctop(kopsumma_inp.val())) * apkalp_maksa_proc) - (parseFloat(ctop(summa_inp.val())) * apkalp_maksa_proc);
        }

        if(isNumber(apm_summa)) {
          apmkopsumma += parseFloat(ctop(apm_summa));
        }

      }

    });



    $('#full_summa').val(full_summa.toFixed(2));
    $('#full_pvn').val(full_pvn.toFixed(2));
    $('#full_kopsumma').val(full_kopsumma.toFixed(2));

    $('#apkalp_maksa_summa').val(apkalp_maksa_summa.toFixed(2));
    $('#apkalp_maksa_pvn').val(apkalp_maksa_pvn.toFixed(2));
    $('#apkalp_maksa_kopsumma').val(apkalp_maksa_kopsumma.toFixed(2));

    $('#atmsumm').html(apmkopsumma.toFixed(2));

    // paid sum percent

    if(isNumber(apmkopsumma) && isNumber(full_kopsumma)){
      $('#atmproc').html((apmkopsumma / full_kopsumma * 100).toFixed(1));
    }else{
      $('#atmproc').html("-");
    }

  }

  function processRekRow(foc_inp) {

    var rek_row = foc_inp.parents('tr');

    if (rek_row.length == 0) {
      return false;
    }

    var auto_summas = $('#auto_summas').is(':checked');

    var summa_inp = rek_row.find('.summa');
    var kopsumma_inp = rek_row.find('.kopsumma');
    var atkaps_rekins = rek_row.find('input.atkapsanas_rekins').is(':checked');

    if (atkaps_rekins) {
      rek_row.find('.pvn').val('0.00');
    }

    var pvn_likme = parseFloat(ctop(rek_row.find('.pvn').val()));

    summa_inp.removeClass('input_has_error');

    if (summa_inp.val() != '' && !isNumber(summa_inp.val())) {
      summa_inp.addClass('input_has_error');
    }

    kopsumma_inp.removeClass('input_has_error');

    if (kopsumma_inp.val() != '' && !isNumber(kopsumma_inp.val())) {
      kopsumma_inp.addClass('input_has_error');
    }

    if (auto_summas) {

      if (foc_inp.is('.kopsumma')) {

        if (isNumber(kopsumma_inp.val())) {

          var kopsumma = parseFloat(ctop(kopsumma_inp.val()));

          var pvn_summa = 0;

          if (!atkaps_rekins) {
            pvn_summa = kopsumma * (pvn_likme / 100) / (1 + pvn_likme / 100);
          }

          summa_inp.val(parseFloat(kopsumma - pvn_summa).toFixed(2));

        } else {

          rek_row.find('.pvnsumma').html('');
          summa_inp.val('')

        }

      } else {

        if (isNumber(summa_inp.val())) {

          var summa = parseFloat(ctop(summa_inp.val()));

          var pvn_summa = 0;

          if (!atkaps_rekins) {
            pvn_summa = (summa * pvn_likme ) / 100;
          }

          kopsumma_inp.val(parseFloat(summa + pvn_summa).toFixed(2));

        } else {

          rek_row.find('.pvnsumma').html('');
          kopsumma_inp.val('')

        }

      }

    }

    if (isNumber(kopsumma_inp.val()) && isNumber(summa_inp.val())) {

      var kopsumma = parseFloat(ctop(kopsumma_inp.val()));
      var summa = parseFloat(ctop(summa_inp.val()));

      var pvn_summa = kopsumma - summa;

      rek_row.find('.pvnsumma').html(pvn_summa.toFixed(2));

    } else {

      rek_row.find('.pvnsumma').html('');;

    }

  }

  function recalcStatusi(){

    // jQuery.each($('.calendarcalc'), function() {
      jQuery.each($('#rekinu_tabula .tr_rek'), function() {

      var row = $(this);
      // var row = $(this).parent().parent();

      row.find('.date_status').removeClass('apm_statuss_1 apm_statuss_2 apm_statuss_3 apm_statuss_4 apm_statuss_5 apm_statuss_6 apm_statuss_7');

      if(row.find('.anulets').is(':checked')){

        row.find('.date_status').html('Anulēts').addClass('apm_statuss_6');

      } else if(row.find('.nodots_piedz').prop('checked')){

        row.find('.date_status').html('Nodots piedz.').addClass('apm_statuss_7');

      } else  { //if ($(this).is(':text'))


        var today = new Date();

        var apm_term = new Date();

        var parts = row.find('.apm_termins').val().split('.');
        // var parts = $(this).val().split('.');

        apm_term.setFullYear(parseInt(parts[2], 10), parseInt(parts[1], 10) - 1, parseInt(parts[0], 10),0,0,0);

        needSum = parseFloat(ctop(row.find('.kopsumma').val()));
        apmSum = parseFloat(ctop(row.find('.apm_summa').html()));

        if(apmSum >= needSum) {

          if (row.find('.barteris').is(':checked')) {
            row.find('.date_status').html('Barteris').addClass('apm_statuss_2');
          } else {
            row.find('.date_status').html('Apmaksāts').addClass('apm_statuss_2');
          }

        } else if (apmSum == 0 || isNaN(apmSum)){

          if(today > apm_term){
            row.find('.date_status').html('Parāds!').addClass('apm_statuss_3');
            calcSodaNauda(row);
          }else{
            row.find('.date_status').html('Nav apm. termiņš');
          }

        } else { // Nav apmaksāts pilnā apmērā... skatamies kalendāru

          if(today > apm_term){
            row.find('.date_status').html('Parāds!').addClass('apm_statuss_3');
            calcSodaNauda(row);
          }else{
            row.find('.date_status').html('Daļēji apmaksāts').addClass('apm_statuss_5');
          }

        }

      }

    });

  }

  function calcSodaNauda(rekins_row) {

    var apm_term = rekins_row.find('.apm_termins').val();

    var day_diff = days_between(apm_term, new Date());

    if (day_diff > 0) {

      var rek_sum = parseFloat(ctop(rekins_row.find('.kopsumma').val()));
      var apm_sum = parseFloat(ctop(rekins_row.find('.apm_summa').html()));

      if (isNaN(rek_sum)) rek_sum = 0;
      if (isNaN(apm_sum)) apm_sum = 0;

      var diff_sum = rek_sum - apm_sum;

      var day_sum = diff_sum * <?= (isset($d['soda_proc']) ? $d['soda_proc'] : 0)  / 100 ?>;

      var soda_nauda = day_sum * day_diff;

      rekins_row.find('.soda_nauda').html('(' + day_diff + ')&nbsp;' + soda_nauda.toFixed(2) + '&nbsp;EUR');

    }

  }

  function processAnuletsClick() {

    if (this.checked) {
      $(this).parents('.tr_rek').find('input.nodots_piedz').prop('checked', false);
    }

  }

  function processNodotsPiedzClick() {

    if (this.checked) {
      $(this).parents('.tr_rek').find('input.anulets').prop('checked', false);
    }

  }

  function processBarterisClick() {

    if (this.checked) {
      $(this).parents('.tr_rek').find('input.atkapsanas_rekins').prop('checked', false);
    }

    recreateRekiniNr();

  }

  function processAtkapsanaClick() {

    if (this.checked) {
      $(this).parents('.tr_rek').find('input.barteris').prop('checked', false);
    }

    recreateRekiniNr();

    processRekRow($(this));

    recalcKopsummas();
    recalcStatusi();

  }

});
</script>

<? show_comments_block('lig_rekini', isset($_GET['id']) ? $_GET['id'] : null) ?>

<? show_files_block('lig_rekini', isset($_GET['id']) ? $_GET['id'] : null, false, false, false, true, true) ?>