<?php
$pc_labot_pilns = check_access('ligumi-labot-pilns') ? 'canedit' : 'always_disabled';
?>
<table width="600" class="data_form layout1">

  <tr><td class="label" width="160">Līguma numurs:</td><td><input class="<?php echo $pc_labot_pilns;?>" type="input" id="orgligumanr" name="ligumanr" value="<?=!empty($d['ligumanr'])?$d['ligumanr']:""?>"></td></tr>
  <tr><td class="label">Līguma datums:</td><td><input class="<?php echo $pc_labot_pilns;?>" type="input" name="ligumadat" id="ligumadat" value="<?=!empty($d['ligumadat'])?date('d.m.Y', strtotime($d['ligumadat'])):''?>"></td></tr>
  <tr><td class="label">Saņemšanas datums:</td><td><input class="<?php echo $pc_labot_pilns;?>" type="input" name="sanemsanas_datums" id="sanemsanas_datums" value="<?=!empty($d['sanemsanas_datums'])?date('d.m.Y', strtotime($d['sanemsanas_datums'])):(!isset($_GET['id']) ? date('d.m.Y') : '')?>"></td></tr>
  <tr>
    <td class="label">Saņemšanas forma:</td>
    <td>
      <select name="sanemsanas_forma" class="with_status_colors classprefix-sanform_statuss <?php echo $pc_labot_pilns;?>">
        <?php foreach($_vars['sanemsanas_forma'] as $k => $p){ ?>
          <option class="status-<?=$k;?>" value="<?=$k;?>" <?=($k==(!empty($d['sanemsanas_forma'])?$d['sanemsanas_forma']:1)?"selected":"")?>><?=$p;?></option>
        <?php } ?>
      </select>
    </td>
  </tr>
  <tr>
    <td class="label">Saņemšanas avots:</td>
    <td>
      <select name="avots" class="with_status_colors classprefix-avots <?php echo $pc_labot_pilns;?>">
        <?php foreach($_vars['avoti'] as $k => $p){ ?>
          <option class="status-<?=$k;?>" value="<?=$k;?>" <?=($k==(!empty($d['avots'])?$d['avots']:1)?"selected":"")?>><?=$p;?></option>
        <?php } ?>
      </select>
    </td>
  </tr>


  <tr><td colspan="2" class="sep"><div></div></td></tr>

  <tr><td class="label">Nosaukums:</td><td><input class="<?php echo $pc_labot_pilns;?>" type="input" id="nosaukumsplace" name="nosaukums" value="<?=!empty($d['nosaukums'])?$d['nosaukums']:""?>"></td></tr>
  <tr><td class="label">Reģ.nr/pers.kods:</td><td><input class="<?php echo $pc_labot_pilns;?>" type="input" name="regnr" value="<?=!empty($d['regnr'])?$d['regnr']:""?>"></td></tr>
  <tr><td class="label">PVN nr.:</td><td><input class="<?php echo $pc_labot_pilns;?>" type="input" name="pvnnr" value="<?=!empty($d['pvnnr'])?$d['pvnnr']:""?>"></td></tr>

  <tr><td colspan="2" class="sep"><div></div></td></tr>

  <tr>
    <td class="label">Faktiskā adrese:<br />
    <?php if (check_access('ligumi-labot-pilns')) { ?>
      <button class="ui-state-default ui-corner-all hidethistoo" onClick="return copyFaktToJur();">Kopēt uz jur.</button>
    <?php } ?>
    </td>
    <td class="has_sublabels">
      <div><span class="sublabel">Iela</span> <input class="<?php echo $pc_labot_pilns;?>" type="input" name="faktaddr_street" id="add2" value="<?=!empty($d['faktaddr_street'])?$d['faktaddr_street']:""?>"></div>
      <div><span class="sublabel">Pilsēta</span> <input class="<?php echo $pc_labot_pilns;?>" type="input" name="faktaddr_city" id="add1" value="<?=!empty($d['faktaddr_city'])?$d['faktaddr_city']:""?>"></div>
      <div><span class="sublabel">Pasta indekss</span> <input class="<?php echo $pc_labot_pilns;?>" type="input" name="faktaddr_index" id="add3" value="<?=!empty($d['faktaddr_index'])?$d['faktaddr_index']:(!isset($_GET['id']) ? 'LV-' : '')?>"></div>
    </td>
  </tr>

  <tr>
    <td class="label">Juridiskā adrese:<br />
    <?php if (check_access('ligumi-labot-pilns')) { ?>
      <button class="ui-state-default ui-corner-all hidethistoo" onClick="return copyJurToFakt();">Kopēt uz fakt.</button>
    <?php } ?>
    </td>
    <td class="has_sublabels">
      <div><span class="sublabel">Iela</span> <input class="<?php echo $pc_labot_pilns;?>" type="input" name="juridaddr_street" id="juradd2" value="<?=!empty($d['juridaddr_street'])?$d['juridaddr_street']:""?>"></div>
      <div><span class="sublabel">Pilsēta</span> <input class="<?php echo $pc_labot_pilns;?>" type="input" name="juridaddr_city" id="juradd1" value="<?=!empty($d['juridaddr_city'])?$d['juridaddr_city']:""?>"></div>
      <div><span class="sublabel">Pasta indekss</span> <input class="<?php echo $pc_labot_pilns;?>" type="input" name="juridaddr_index" id="juradd3" value="<?=!empty($d['juridaddr_index'])?$d['juridaddr_index']:(!isset($_GET['id']) ? 'LV-' : '')?>"></div>
    </td>
  </tr>

  <tr><td colspan="2" class="sep"><div></div></td></tr>

  <tr>
    <td class="label">Kontaktpersona:</td>
    <td class="has_sublabels">

      <div id="kontplace">

        <? $is_first = true ?>

        <?php foreach($kontakti as $k) { ?>

          <div <?= !$is_first ? 'style="margin-top: 15px;"' : '' ?>>
            <div><span class="sublabel">Līguma slēdzējs:</span> <input style="width: auto; " type="checkbox" class="<?php echo $pc_labot_pilns;?> ligmasledzejcheck" name="kontakti[<?= $k['id'] ?>][ligumsledzejs]" <?=!empty($k['ligumsledzejs'])?"checked":""?>></div>
            <div class="clear"><span class="sublabel">Vārds:</span> <input class="<?php echo $pc_labot_pilns;?>" type="input" name="kontakti[<?= $k['id'] ?>][vards]" value="<?=!empty($k['vards'])?$k['vards']:""?>"></div>
            <div><span class="sublabel">Tālrunis:</span> <input class="<?php echo $pc_labot_pilns;?>" type="input" name="kontakti[<?= $k['id'] ?>][telefons]" value="<?=!empty($k['telefons'])?$k['telefons']:""?>"></div>
            <div><span class="sublabel">E-pasts:</span> <input type="input" class="<?php echo $pc_labot_pilns;?> readthismail" name="kontakti[<?= $k['id'] ?>][epasts]" value="<?=!empty($k['epasts'])?$k['epasts']:""?>"></div>
            <div class="clear" style="text-align: right;">
              <?php if (check_access('ligumi-labot-pilns')) { ?>
                <button class="hidethistoo ui-state-default ui-corner-all" onClick="$(this).parent().parent().html(''); return false;">Dzēst šo kontaktu</button>
              <?php } ?>
            </div>
          </div>

          <? $is_first = false ?>

        <?php } ?>

      </div>

      <script>
      function init_ligmasledzejcheck() {

        var cprocess = function() {
          $('.ligmasledzejcheck').not(this).prop('checked', false);
        }

        $('.ligmasledzejcheck').unbind('click');

        $('.ligmasledzejcheck').click(cprocess);


      }
      init_ligmasledzejcheck();
      </script>

      <?php if (check_access('ligumi-labot-pilns')) { ?>
        <button onClick="$('#kontplace').append($('#kontprot').html()); init_ligmasledzejcheck(); return false;" class="ui-state-default ui-corner-all hidethistoo">Pievienot kontaktu</button>
      <?php } ?>
    </td>
  </tr>

  <tr><td colspan="2" class="sep"><div></div></td></tr>

  <tr>
    <td class="label">Pārdevējs:</td>
    <td>

      <?php if(check_access('ligumi-labot-pilns')) { ?>
        <input name="pardeveja_id" type="hidden" value="<?= !empty($d['pardeveja_id']) ? $d['pardeveja_id'] : '' ?>" class="limited_admin_edit canedit" />
      <?php } ?>


      <select class="chosen" name="pardeveja_id" class="<?php echo $pc_labot_pilns;?>">

        <optgroup label="Pārdevēji">
          <?php foreach(Pardevejs::model()->sales()->findAll(array('order' => 'vards ASC')) as $p){ ?>
            <option value="<?=$p->id;?>" <?=($p->id==(!empty($d['pardeveja_id'])?$d['pardeveja_id']:$_SESSION['user']['id'])?"selected":"")?>><?=$p->vards;?></option>
          <?php } ?>
        </optgroup>

        <optgroup label="Partneri">
          <?php foreach(Pardevejs::model()->partner()->findAll(array('order' => 'vards ASC')) as $p){ ?>
            <option value="<?=$p->id;?>" <?=($p->id==(!empty($d['pardeveja_id'])?$d['pardeveja_id']:$_SESSION['user']['id'])?"selected":"")?>><?=$p->vards;?></option>
          <?php } ?>
        </optgroup>

        <optgroup label="Vadītāji">
          <?php foreach(Pardevejs::model()->manager()->findAll(array('order' => 'vards ASC')) as $p){ ?>
            <option value="<?=$p->id;?>" <?=($p->id==(!empty($d['pardeveja_id'])?$d['pardeveja_id']:$_SESSION['user']['id'])?"selected":"")?>><?=$p->vards;?></option>
          <?php } ?>
        </optgroup>

      </select>

      <? if (!empty($d['pardeveja_id'])) { ?>
        <a class="info" style="margin-left: 25px;" href="?c=darbinieki&a=labot&id=<?= $d['pardeveja_id'] ?>">Atvērt darbinieka līgumus</a>
      <? } ?>


    </td>
  </tr>

  <tr>
    <td class="label">Saistītie pārdevēji:</td>
    <td>

      <div id="saistitie_pardeveji_place">

        <?
        $pc_saistitie = check_access('ligumi-labot-saistitie-pardeveji') ? 'canedit' : 'always_disabled';
        $pc_saistitie_sel = check_access('ligumi-labot-saistitie-pardeveji') ? 'canedit chosen' : 'always_disabled';
        if(!empty($saistitie_pardeveji)) {

          ?>

          <? foreach($saistitie_pardeveji as $k => $v) { ?>

            <div style="margin-bottom: 5px;">

              <select class="<?php echo $pc_saistitie_sel;?>" name="saistitie_pardeveji[<?= $v['id'] ;?>][pardeveja_id]">

                <optgroup label="Pārdevēji">
                  <?php foreach(Pardevejs::model()->sales()->findAll(array('order' => 'vards ASC')) as $p){ ?>
                    <option value="<?=$p->id;?>" <?= $v['pardeveja_id'] == $p->id ? "selected" : "" ?>><?=$p->vards;?></option>
                  <?php } ?>
                </optgroup>

                <optgroup label="Partneri">
                  <?php foreach(Pardevejs::model()->partner()->findAll(array('order' => 'vards ASC')) as $p){ ?>
                    <option value="<?=$p->id;?>" <?= $v['pardeveja_id'] == $p->id ? "selected" : "" ?>><?=$p->vards;?></option>
                  <?php } ?>
                </optgroup>

                <optgroup label="Vadītāji">
                  <?php foreach(Pardevejs::model()->manager()->findAll(array('order' => 'vards ASC')) as $p){ ?>
                    <option value="<?=$p->id;?>" <?= $v['pardeveja_id'] == $p->id ? "selected" : "" ?>><?=$p->vards;?></option>
                  <?php } ?>
                </optgroup>

              </select>

              <span style="margin-left: 10px;">Datums:</span> <input class="showcalendar <?php echo $pc_saistitie;?>" type="text" name="saistitie_pardeveji[<?= $v['id'] ;?>][piesaist_datums]" value="<?=!empty($v['piesaist_datums'])?date('d.m.Y', strtotime($v['piesaist_datums'])):'';?>" style="width: 6em;" />
              <?php if(check_access('ligumi-labot-saistitie-pardeveji')) { ?>
                <button class="hidethistoo ui-state-default ui-corner-all <?php echo $pc_saistitie;?>" onclick="$(this).parent().remove();">Dzēst</button>
              <?php } ?>
            </div>

          <? } ?>

        <? } ?>

      </div>

      <?php if(check_access('ligumi-labot-saistitie-pardeveji')) { ?>
        <button class="ui-state-default ui-corner-all hidethistoo <?php echo $pc_saistitie;?>" onclick="$('#saistitie_pardeveji_place').append($('#saistitie_pardeveji_prot').html()).find('.saistpardcalendarnew').datepicker({dateFormat:'dd.mm.yy'}).removeClass('saistpardcalendarnew'); return false;">Pievienot jaunu</button>
      <?php } ?>

    </td>
  </tr>

  <?php if(check_access('ligumi-labot-pilns')) { ?>
    <tr>
      <td class="label">Vadītājs</td>
      <td>
        <?php
        $vaditaji = get_vaditaji();
        ?>
        <select name="vaditajs_id" class="<?php echo $pc_labot_pilns;?>" id="vaditajs_id">
          <option value="">- Pārdevēja vadītājs -</option>
          <?php
          foreach($vaditaji as $vaditajs) {
            ?>
            <option value="<?php echo $vaditajs['id'];?>" <?php echo (isset($d['vaditajs_id']) && $d['vaditajs_id'] == $vaditajs['id']) ? 'selected' : ''?>><?php echo $vaditajs['vards'];?></option>
            <?php
          }
          ?>
        </select>
      </td>
    </tr>
  <?php } ?>

  <tr><td colspan="2" class="sep"><div></div></td></tr>

    <?php if (!empty($_GET['id'])) { ?>



    <tr>
      <td class="label">Piesaistīts klientam:</td>
      <td>
        <input type="text" name="grupas_regnr_display" value="<?= !empty($ligumu_grupa) && !empty($ligumu_grupa['manuala_piesaiste']) ? htmlesc($ligumu_grupa['klienta_nosaukums']) : '' ?>" class="<?php echo $pc_labot_pilns;?>" />
        <input type="hidden" name="grupas_regnr" value="<?= !empty($ligumu_grupa) && !empty($ligumu_grupa['manuala_piesaiste']) ? htmlesc($ligumu_grupa['grupas_regnr']) : '' ?>" class="<?php echo $pc_labot_pilns;?>" />
        <span class="sublabel" style="width: 100%">Jānorāda tikai tad, ja līgums nav automātiski pēc reģ.nr. piesaistīts (vai nepareizi piesaistīts). Lai atsaistītu, izdzēsiet no lauka klienta nosaukumu.</span>
      </td>
    </tr>

  <?php } ?>

    <tr>
    <td class="label">Pārslēgts uz:</td>
    <td>
      <div id="parsl_rinda_place">

        <? if(!empty($parslegsanas_rinda)) { ?>

          <? foreach($parslegsanas_rinda as $k => $v) { ?>

            <div style="margin-bottom: 5px;">
              <input type="text" name="parsl_rinda[<?= $v['id'] ;?>][jaunais_liguma_display]" value="<?= $v['liguma_nosaukums'].', '.$v['liguma_ligumanr'] ?>" class="<?php echo $pc_labot_pilns;?> parsl_rinda_display_inp" style="width: 84%;" />
              <input type="hidden" name="parsl_rinda[<?= $v['id'] ;?>][jaunais_liguma_id]" value="<?= $v['jaunais_liguma_id'] ?>" class="<?php echo $pc_labot_pilns;?> parsl_rinda_id_inp" />
              <?php if(check_access('ligumi-labot-pilns')) { ?>
                <button class="hidethistoo ui-state-default ui-corner-all" onclick="$(this).parent().remove();">Dzēst</button>
              <?php } ?>
            </div>

          <? } ?>

        <? } ?>

      </div>

      <?php if(check_access('ligumi-labot-pilns')) { ?>
        <button class="ui-state-default ui-corner-all hidethistoo" onclick="var row = $('#parsl_rinda_prot div').clone(false); $('#parsl_rinda_place').append(row); initParslegsanasRinda(row.find('input.parsl_rinda_display_inp')); return false;">Pievienot jaunu</button>
      <?php } ?>

      <span class="sublabel" style="width: 100%">Ja atvērtais klienta līgums jau ir beidzies, tad šeit norādītais līgums, uz kuru tas tiek pārslēgts, vairs netiks ņemts vērā un pārslēgšanu var veikt tikai manuāli līgumu kopsavilkumā.</span>
    </td>
  </tr>

  <tr>
    <td class="label">Papildinājums līgumam:</td>
    <td>
      <?php
      $all_contracts = db_get_assoc_all(db_query('
        SELECT id, ligumanr
        FROM ' . DB_PREF . 'ligumi
        WHERE
          saistits_pie IS NULL
        ORDER BY ligumanr
      '));

      if(!empty($all_contracts)) { ?>
        <select name="saistits_pie" class="<?php echo $pc_labot_pilns;?>">
          <option value="">- Nav piesaistits -</option>
          <?php foreach($all_contracts as $contract) {?>
            <option value="<?php echo $contract['id'];?>" <?php echo ($d['saistits_pie'] == $contract['id'] ? 'selected = "selected"' : '')?>><?php echo $contract['ligumanr'];?></option>
          <?php } ?>
        </select>
      <?php } ?>
    </td>
  </tr>

  <tr><td colspan="2" class="sep"><div></div></td></tr>

  <tr>
    <td class="label">Neiekļaut atdotajos līgumos:</td>
    <td>
      <input class="<?php echo check_access('ligumi-labot-statu-info') ? 'canedit' : 'always_disabled';?>" type="checkbox" name="neradit_atd_ligumi" <?=!empty($d['neradit_atd_ligumi'])?"checked":""?> />
    </td>
  </tr>

  <tr>
    <td class="label">Neiekļaut atdotajā apgrozījumā:</td>
    <td>
      <input class="<?php echo check_access('ligumi-labot-statu-info') ? 'canedit' : 'always_disabled';?>" type="checkbox" name="neradit_atd_apgr" <?=!empty($d['neradit_atd_apgr'])?"checked":""?> />
    </td>
  </tr>

  <tr>
    <td class="label">Neiekļaut sasniegtajos līgumos:</td>
    <td>
      <input class="<?php echo check_access('ligumi-labot-statu-info') ? 'canedit' : 'always_disabled';?>" type="checkbox" class="limited_admin_edit" name="neieklaut_sasn_ligumi" <?=!empty($d['neieklaut_sasn_ligumi'])?"checked":""?> />

    </td>
  </tr>

  <tr>
    <td class="label">Neiekļaut sasniegtajā apgrozījumā:</td>
    <td>
      <input class="<?php echo check_access('ligumi-labot-statu-info') ? 'canedit' : 'always_disabled';?>" type="checkbox" class="limited_admin_edit" name="neieklaut_sasn_apgr" <?=!empty($d['neieklaut_sasn_apgr'])?"checked":""?> />
    </td>
  </tr>

  <tr>
    <td class="label">Iekļaut izpildē pilnā apmērā:</td>
    <td>
      <input class="<?php echo check_access('ligumi-labot-statu-info') ? 'canedit' : 'always_disabled';?>" type="checkbox" class="limited_admin_edit" name="ieklaut_pilna_apm" <?=!empty($d['ieklaut_pilna_apm'])?"checked":""?> />
       <span class="sublabel" style="width: 100%;">Tiks iekļauts vadītāja plāna izpildē pilnā apmērā, neatkarīgi no tā vai rēķini ir anulēti.</span>
    </td>
  </tr>

	<?php

		if(isset($_GET['id'])){
			$domains = db_get_assoc_all(db_query('
        SELECT *
        FROM ' . DB_PREF . 'ligumi_domains
        WHERE ligumi_id = '.esc((int)$_GET['id']).'
      '));
		}else{
			$domains = array();
		}

	?>           

		<tr><td colspan="2" class="sep"><div></div></td></tr>
		<tr id="contract_domains">
			<td class="label">Mājaslapas:</td>
			<td>
				<? foreach($domains as $domain){ ?>
				<input type='text' name='websites[]' value='<?=htmlesc($domain['domain'])?>' style='margin-bottom: 5px;' />
				<? } ?>
				<input type='text' name='websites[]' class='websites' style='margin-bottom: 5px;' />
			</td>
		</tr>

</table>

<script>

	// $(document).ready(function(){
$(document).on('keyup','#contract_domains .websites', function(){
			if($(this).val()){
			    var cloned = $(this).clone();
			    $(this).removeClass('websites');
				$(this).after(cloned.val(''));
			}


})

</script>


<? show_comments_block('lig_klients', isset($_GET['id']) ? $_GET['id'] : null) ?>

<? show_files_block('lig_klients', isset($_GET['id']) ? $_GET['id'] : null, false, false, false, false, true) ?>