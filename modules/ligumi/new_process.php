<?php

if (!empty($_POST) || !empty($_FILES)) {

  if(!isset($_GET['id']) && !check_access('ligumi-labot')) {
    die('Jums nav pieejas tiesību jauna līguma pievienošanai.');
  } else {
    if(!check_access('ligumi-labot') && $ligums->pardeveja_id != $_SESSION['user']['id']) {
      die('Jums nav pieejas tiesību šī līguma labošanai.');
    }
  }

  $errors = validate_ligums_form($_POST);

  if($_GET['id']) {
    $ligums = Ligums::model()->findByPk($_GET['id']);
  } else {
    $ligums = new Ligums;
  }

  if (empty($errors)) {

    $mpk = $_POST['mpk'];

    $form_names = array(
      'lig_klients',
      'lig_pakalpojums',
      'lig_rekini',
      'lig_atgadinajumi',
    );

    $_POST = trim_array($_POST);

    $log_data = array();

    if (!empty($_GET['id'])) {
      $liguma_id = (int)$_GET['id'];
    }

    //if(is_limited() && !is_bookkeeper() && !is_minimal()) {
    if(check_access('ligumi-labot-statu-info')) {
        if (!empty($_GET['id'])) {
          $sql = "
              UPDATE `".DB_PREF."ligumi`
              SET
                neieklaut_sasn_ligumi = ".(!empty($_POST['neieklaut_sasn_ligumi']) ? 1 : 0).",
                neieklaut_sasn_apgr = ".(!empty($_POST['neieklaut_sasn_apgr']) ? 1 : 0).",
                ieklaut_pilna_apm = ".(!empty($_POST['ieklaut_pilna_apm']) ? 1 : 0).",
                saistits_pie = " . ($_POST['saistits_pie'] ? $_POST['saistits_pie'] : '(null)') . "
              WHERE id = ".$liguma_id."
              LIMIT 1
            ";

          db_query($sql);
        }
    }

    if (check_access('ligumi-labot-pilns')) {

      $ligumadat = !empty($_POST['ligumadat']) ? "'".esc(date('Y-m-d', strtotime($_POST['ligumadat'])))."'" : 'null';
      $reklama_no = !empty($_POST['reklama_no']) ? "'".esc(date('Y-m-d', strtotime($_POST['reklama_no'])))."'" : 'null';
      $reklama_lidz = !empty($_POST['reklama_lidz']) ? "'".esc(date('Y-m-d', strtotime($_POST['reklama_lidz'])))."'" : 'null';
      $sanemsanas_datums = !empty($_POST['sanemsanas_datums']) ? "'".esc(date('Y-m-d', strtotime($_POST['sanemsanas_datums'])))."'" : 'null';

      //  Vadītājs
      if(isset($_POST['vaditajs_id']) && $_POST['vaditajs_id']) {
        $vaditajs_id =  $_POST['vaditajs_id'];
      } elseif(isset($_POST['pardeveja_id']) && $_POST['pardeveja_id']) {
        //  Ja nav norādīts speciāli vadītājs tad ņemam to kas ir norādītajam pārdevējam
        $res = db_get_assoc(db_query('SELECT vaditajs_id FROM ' . DB_PREF . 'pardeveji WHERE id = ' . esc($_POST['pardeveja_id'])));
        $vaditajs_id = $res['vaditajs_id'] ? $res['vaditajs_id'] : '(null)';
      } else {
        $vaditajs_id = '(null)';
      }

      $campaign_type = isset($_POST['campaign_type']) ? $_POST['campaign_type'] : null;

      if (!empty($_GET['id'])) {

        //$liguma_id = (int)$_GET['id'];

        $old_data = db_get_assoc(db_query("SELECT * FROM `".DB_PREF."ligumi` WHERE `id` = " . $liguma_id));

        $log_data['ligums']['old'] = $old_data;

        $query = db_query("SELECT * FROM `".DB_PREF."rekini` WHERE `liguma_id` = " . $liguma_id . " ORDER BY id ASC");
        $old_rekini_data = array();

        while($row = db_get_assoc($query)) {
          $old_rekini_data[] = $row;
        }

        $auto_atslegts_disable = 'auto_atslegts_disable'; // do not change value
        if ($old_data['google_statuss'] == 4 && $_POST['google_statuss'] != 4 && $old_data['auto_atslegts'] == true) {
          $auto_atslegts_disable = 1;
        }

        $sql = "
          UPDATE `".DB_PREF."ligumi`
          SET
            mpk = '".esc($mpk)."',
            ligumanr = '".esc($_POST['ligumanr'])."',
            ligumadat = ".$ligumadat.",
            nosaukums = '".esc($_POST['nosaukums'])."',
            regnr = '".esc($_POST['regnr'])."',
            pvnnr = '".esc($_POST['pvnnr'])."',
            faktaddr_city = '".esc($_POST['faktaddr_city'])."',
            faktaddr_street = '".esc($_POST['faktaddr_street'])."',
            faktaddr_index = '".esc($_POST['faktaddr_index'])."',
            juridaddr_city = '".esc($_POST['juridaddr_city'])."',
            juridaddr_street = '".esc($_POST['juridaddr_street'])."',
            juridaddr_index = '".esc($_POST['juridaddr_index'])."',
            pardeveja_id = ".(int)$_POST['pardeveja_id'].",
            reklama_no = ".$reklama_no.",
            reklama_lidz = ".($reklama_lidz ? $reklama_lidz : '(null)').",
            pak_nosaukums_db = '".esc($_POST['pak_nosaukums_db'])."',
            sanemsanas_forma = ".(int)$_POST['sanemsanas_forma'].",
            avots = ".(int)$_POST['avots'].",
            sanemsanas_datums = ".$sanemsanas_datums.",
            infomedia_statuss = ".(int)$_POST['infomedia_statuss'].",
            google_statuss = ".(int)$_POST['google_statuss'].",
            infomedia_reklamas_mat = ".(int)$_POST['infomedia_reklamas_mat'].",
            tiessaite_reklamas_mat = ".(int)$_POST['tiessaite_reklamas_mat'].",
            yandex_reklamas_mat = ".(int)$_POST['yandex_reklamas_mat'].",
            auto_atslegts_disable = ".$auto_atslegts_disable.",
            soda_proc = ".(float)str_replace(',', '.', $_POST['soda_proc']).",
            auto_summas = ".(!empty($_POST['auto_summas']) ? 1 : 0).",
            neradit_atdotajos = ".(!empty($_POST['neradit_atdotajos']) ? 1 : 0).",
            neradit_atd_ligumi = ".(!empty($_POST['neradit_atd_ligumi']) ? 1 : 0).",
            neradit_atd_apgr = ".(!empty($_POST['neradit_atd_apgr']) ? 1 : 0).",
            neieklaut_sasn_ligumi = ".(!empty($_POST['neieklaut_sasn_ligumi']) ? 1 : 0).",
            neieklaut_sasn_apgr = ".(!empty($_POST['neieklaut_sasn_apgr']) ? 1 : 0).",
            ieklaut_pilna_apm = ".(!empty($_POST['ieklaut_pilna_apm']) ? 1 : 0).",
            vaditajs_id = " . $vaditajs_id . ",
            saistits_pie = " . ($_POST['saistits_pie'] ? $_POST['saistits_pie'] : '(null)') . ",
            website = '".($mpk ? esc($_POST['website']) : '')."',
            campaign_type = '".($mpk ? esc($campaign_type) : '')."',
            bank_account = '".esc($_POST['bank_account'])."',
            konta_admin = '".esc($_POST['konta_admin'])."'
          WHERE id = ".$liguma_id."
          LIMIT 1
        ";

        db_query($sql);
      } else {

        $sql = "
          INSERT INTO `".DB_PREF."ligumi`
          SET
            mpk = '".esc($mpk)."',
            ligumanr = '".esc($_POST['ligumanr'])."',
            ligumadat = ".$ligumadat.",
            nosaukums = '".esc($_POST['nosaukums'])."',
            regnr = '".esc($_POST['regnr'])."',
            pvnnr = '".esc($_POST['pvnnr'])."',
            faktaddr_city = '".esc($_POST['faktaddr_city'])."',
            faktaddr_street = '".esc($_POST['faktaddr_street'])."',
            faktaddr_index = '".esc($_POST['faktaddr_index'])."',
            juridaddr_city = '".esc($_POST['juridaddr_city'])."',
            juridaddr_street = '".esc($_POST['juridaddr_street'])."',
            juridaddr_index = '".esc($_POST['juridaddr_index'])."',
            pardeveja_id = ".(int)$_POST['pardeveja_id'].",
            sanemsanas_forma = ".(int)$_POST['sanemsanas_forma'].",
            avots = ".(int)$_POST['avots'].",
            sanemsanas_datums = ".$sanemsanas_datums.",
            reklama_no = ".$reklama_no.",
            reklama_lidz = ".($reklama_lidz ? $reklama_lidz : '(null)').",
            pak_nosaukums_db = '".esc($_POST['pak_nosaukums_db'])."',
            infomedia_statuss = ".(int)$_POST['infomedia_statuss'].",
            google_statuss = ".(int)$_POST['google_statuss'].",
            infomedia_reklamas_mat = ".(int)$_POST['infomedia_reklamas_mat'].",
            tiessaite_reklamas_mat = ".(int)$_POST['tiessaite_reklamas_mat'].",
            yandex_reklamas_mat = ".(int)$_POST['yandex_reklamas_mat'].",
            soda_proc = ".(float)str_replace(',', '.', $_POST['soda_proc']).",
            auto_summas = ".(!empty($_POST['auto_summas']) ? 1 : 0).",
            neradit_atdotajos = ".(!empty($_POST['neradit_atdotajos']) ? 1 : 0).",
            neradit_atd_ligumi = ".(!empty($_POST['neradit_atd_ligumi']) ? 1 : 0).",
            neradit_atd_apgr = ".(!empty($_POST['neradit_atd_apgr']) ? 1 : 0).",
            neieklaut_sasn_ligumi = ".(!empty($_POST['neieklaut_sasn_ligumi']) ? 1 : 0).",
            neieklaut_sasn_apgr = ".(!empty($_POST['neieklaut_sasn_apgr']) ? 1 : 0).",
            ieklaut_pilna_apm = ".(!empty($_POST['ieklaut_pilna_apm']) ? 1 : 0).",
            vaditajs_id = " . $vaditajs_id . ",
            saistits_pie = " . ($_POST['saistits_pie'] ? $_POST['saistits_pie'] : '(null)') . ",
            website = '".($mpk ? esc($_POST['website']) : '')."',
            campaign_type = '".($mpk ? esc($campaign_type) : '')."',
            bank_account = '".esc($_POST['bank_account'])."',
            konta_admin = '".esc($_POST['konta_admin'])."'
        ";

        db_query($sql);

        $liguma_id = db_last_id();

      }

      $log_data['ligums']['new'] = db_get_assoc(db_query("SELECT * FROM `".DB_PREF."ligumi` WHERE `id` = " . $liguma_id));

      $log_data['kontakti']['old_m'] = db_get_assoc_all(db_query("SELECT * FROM `".DB_PREF."kontakti` WHERE `liguma_id` = " . $liguma_id));

      if (!empty($_GET['id'])) {

        // deleting contacts

        $sql = "
          DELETE FROM `".DB_PREF."kontakti`
          WHERE
            liguma_id = ".$liguma_id."
            " . (!empty($_POST['kontakti']) ? "AND id NOT IN (".implode(',', array_keys($_POST['kontakti'])).") " : '')."
        ";

        db_query($sql);

        // updating contacts

        if (!empty($_POST['kontakti'])) {

          foreach($_POST['kontakti'] as $kontakta_id => $c) {

            $sql = "
              UPDATE `".DB_PREF."kontakti`
              SET
                vards = '".esc($c['vards'])."',
                epasts = '".esc($c['epasts'])."',
                telefons = '".esc($c['telefons'])."',
                ligumsledzejs = ".(!empty($c['ligumsledzejs']) ? 1 : 0)."
              WHERE id = ".$kontakta_id."
              LIMIT 1
            ";

            db_query($sql);

          }

        }

      }

      // adding contacts

      if (!empty($_POST['kontakti_new']['vards'])) {

        foreach($_POST['kontakti_new']['vards'] as $i => $vards) {

          $telefons = $_POST['kontakti_new']['telefons'][$i];
          $epasts = $_POST['kontakti_new']['epasts'][$i];
          $ligumsledzejs = !empty($_POST['kontakti_new']['ligumsledzejs'][$i]) ? 1 : 0;

          $sql = "
            INSERT INTO `".DB_PREF."kontakti` (
              liguma_id,
              vards,
              epasts,
              telefons,
              ligumsledzejs
            ) VALUES (
              ".$liguma_id.",
              '".esc($vards)."',
              '".esc($epasts)."',
              '".esc($telefons)."',
              ".$ligumsledzejs."
            )
          ";

          db_query($sql);

        }

      }

      $log_data['pakalpojumi']['old_m'] = db_get_assoc_all(db_query("SELECT * FROM `".DB_PREF."pakalpojumi` WHERE `liguma_id` = " . $liguma_id));

      if (!empty($_GET['id'])) {

        // deleting old services

        $sql = "
          DELETE FROM `".DB_PREF."pakalpojumi`
          WHERE
            liguma_id = ".$liguma_id."
            " . (!empty($_POST['pakalpojumi']) ? "AND id NOT IN (".implode(',', array_keys($_POST['pakalpojumi'])).") " : '')."
        ";

        db_query($sql);

        // updating services

        if (!empty($_POST['pakalpojumi'])) {

          foreach($_POST['pakalpojumi'] as $pakalpojuma_id => $p) {

            $sql = "
              UPDATE `".DB_PREF."pakalpojumi`
              SET
                pakalpojuma_veida_id = ".(int)$p['pakalpojuma_veida_id'].",
                pakalpojuma_apr = '".esc($p['pakalpojuma_apr'])."'
              WHERE id = ".$pakalpojuma_id."
              LIMIT 1
            ";

            db_query($sql);

          }

        }

      }

      // adding services

      if (!empty($_POST['pakalpojumi_new']['pakalpojuma_veida_id'])) {

        foreach($_POST['pakalpojumi_new']['pakalpojuma_veida_id'] as $i => $pakalpojuma_veida_id) {

          if (empty($pakalpojuma_veida_id)) {
            continue;
          }

          $pakalpojuma_apr = $_POST['pakalpojumi_new']['pakalpojuma_apr'][$i];

          $sql = "
            INSERT INTO `".DB_PREF."pakalpojumi` (
              liguma_id,
              pakalpojuma_veida_id,
              pakalpojuma_apr
            ) VALUES (
              ".$liguma_id.",
              ".(int)$pakalpojuma_veida_id.",
              '".esc($pakalpojuma_apr)."'
            )
          ";

          db_query($sql);

        }

      }

      // PIELIKUMI INFOMEDIA

      $log_data['pielikumi_infomedia']['old_m'] = db_get_assoc_all(db_query("SELECT * FROM `".DB_PREF."pielikumi_infomedia` WHERE `liguma_id` = " . $liguma_id));

      if (!empty($_GET['id'])) {

        // deleting old pielikumi_infomedia

        $sql = "
          DELETE FROM `".DB_PREF."pielikumi_infomedia`
          WHERE
            liguma_id = ".$liguma_id."
            " . (!empty($_POST['piel_infomedia']) ? "AND id NOT IN (".implode(',', array_keys($_POST['piel_infomedia'])).") " : '')."
        ";

        db_query($sql);

        // updating pielikumi_infomedia

        if (!empty($_POST['piel_infomedia'])) {

          foreach($_POST['piel_infomedia'] as $piel_infomedia_id => $p) {
            $sql = "
              UPDATE `".DB_PREF."pielikumi_infomedia`
              SET
                atslegvards = '".esc($p['atslegvards'])."',
                google_konts = '".esc($p['google_konts'])."',
                atra_saite = '".(isset($p['atra_saite']) ? esc($p['atra_saite']) : 0)."',
                nozare_id = '".esc($p['nozare_id'])."'
              WHERE id = ".$piel_infomedia_id."
              LIMIT 1
            ";
            db_query($sql);
            reset_atra_saite($p['atslegvards']);
          }
        }
      }

      // adding pielikumi_infomedia

      if (!empty($_POST['piel_infomedia_new']['atslegvards'])) {

        foreach($_POST['piel_infomedia_new']['atslegvards'] as $i => $atslegvards) {

          $google_konts = $_POST['piel_infomedia_new']['google_konts'][$i];
          $atra_saite = $_POST['piel_infomedia_new']['atra_saite'][$i];
          $nozare_id = $_POST['piel_infomedia_new']['nozare_id'][$i];

          $sql = "
            INSERT INTO `".DB_PREF."pielikumi_infomedia` (
              liguma_id,
              atslegvards,
              google_konts,
              atra_saite,
              nozare_id
            ) VALUES (
              ".$liguma_id.",
              '".esc($atslegvards)."',
              '".esc($google_konts)."',
              '".esc($atra_saite)."',
              '".esc($nozare_id)."'
            )
          ";

          db_query($sql);
          reset_atra_saite($atslegvards);
        }

      }

      // PIELIKUMI TIESSAITE

      $log_data['pielikumi_tiessaite']['old_m'] = db_get_assoc_all(db_query("SELECT * FROM `".DB_PREF."pielikumi_tiessaite` WHERE `liguma_id` = " . $liguma_id));

      if (!empty($_GET['id'])) {

        // deleting old pielikumi_tiessaite

        $sql = "
          DELETE FROM `".DB_PREF."pielikumi_tiessaite`
          WHERE
            liguma_id = ".$liguma_id."
            " . (!empty($_POST['piel_tiessaite']) ? "AND id NOT IN (".implode(',', array_keys($_POST['piel_tiessaite'])).") " : '')."
        ";

        db_query($sql);

        // updating pielikumi_tiessaite

        if (!empty($_POST['piel_tiessaite'])) {

          foreach($_POST['piel_tiessaite'] as $piel_tiessaite_id => $p) {

            $sql = "
              UPDATE `".DB_PREF."pielikumi_tiessaite`
              SET
                atslegvards = '".esc($p['atslegvards'])."',
                domeni = '".esc($p['domeni'])."',
                google_konts = '".esc($p['google_konts'])."',
                top3 = '".(isset($p['top3']) ? esc($p['top3']) : 0)."',
                nozare_id  = '".esc($p['nozare_id'])."'
              WHERE id = ".$piel_tiessaite_id."
              LIMIT 1
            ";

            db_query($sql);

          }

        }

      }

      // adding pielikumi_tiessaite

      if (!empty($_POST['piel_tiessaite_new']['atslegvards'])) {

        foreach($_POST['piel_tiessaite_new']['atslegvards'] as $i => $atslegvards) {

          $domeni = $_POST['piel_tiessaite_new']['domeni'][$i];
          $google_konts = $_POST['piel_tiessaite_new']['google_konts'][$i];
          $top3 = $_POST['piel_tiessaite_new']['top3'][$i];
          $nozare_id = $_POST['piel_tiessaite_new']['nozare_id'][$i];

          $sql = "
            INSERT INTO `".DB_PREF."pielikumi_tiessaite` (
              liguma_id,
              atslegvards,
              domeni,
              google_konts,
              top3,
              nozare_id
            ) VALUES (
              ".$liguma_id.",
              '".esc($atslegvards)."',
              '".esc($domeni)."',
              '".esc($google_konts)."',
              '".esc($top3)."',
              '".esc($nozare_id)."'
            )
          ";

          db_query($sql);

        }

      }

      // PIELIKUMI YANDEX

      $log_data['pielikumi_yandex']['old_m'] = db_get_assoc_all(db_query("SELECT * FROM `".DB_PREF."pielikumi_yandex` WHERE `liguma_id` = " . $liguma_id));

      if (!empty($_GET['id'])) {

        // deleting old pielikumi_yandex

        $sql = "
          DELETE FROM `".DB_PREF."pielikumi_yandex`
          WHERE
            liguma_id = ".$liguma_id."
            " . (!empty($_POST['piel_yandex']) ? "AND id NOT IN (".implode(',', array_keys($_POST['piel_yandex'])).") " : '')."
        ";

        db_query($sql);

        // updating pielikumi_yandex

        if (!empty($_POST['piel_yandex'])) {

          foreach($_POST['piel_yandex'] as $piel_yandex_id => $p) {

            $sql = "
              UPDATE `".DB_PREF."pielikumi_yandex`
              SET
                atslegvards = '".esc($p['atslegvards'])."',
                domeni = '".esc($p['domeni'])."',
                konts = '".esc($p['konts'])."',
                nozare_id = '".esc($p['nozare_id'])."'
              WHERE id = ".$piel_yandex_id."
              LIMIT 1
            ";

            db_query($sql);

          }

        }

      }

      // adding pielikumi_yandex

      if (!empty($_POST['piel_yandex_new']['atslegvards'])) {

        foreach($_POST['piel_yandex_new']['atslegvards'] as $i => $atslegvards) {

          $domeni = $_POST['piel_yandex_new']['domeni'][$i];
          $konts = $_POST['piel_yandex_new']['konts'][$i];
          $nozare_id = $_POST['piel_yandex_new']['nozare_id'][$i];

          $sql = "
            INSERT INTO `".DB_PREF."pielikumi_yandex` (
              liguma_id,
              atslegvards,
              domeni,
              konts,
              nozare_id
            ) VALUES (
              ".$liguma_id.",
              '".esc($atslegvards)."',
              '".esc($domeni)."',
              '".esc($konts)."',
              '".esc($nozare_id)."'
            )
          ";

          db_query($sql);

        }

      }

      // PIELIKUMI CITI

      $log_data['pielikumi_citi']['old_m'] = db_get_assoc_all(db_query("SELECT * FROM `".DB_PREF."pielikumi_citi` WHERE `liguma_id` = " . $liguma_id));

      if (!empty($_GET['id'])) {

        // deleting old pielikumi_citi

        $sql = "
          DELETE FROM `".DB_PREF."pielikumi_citi`
          WHERE
            liguma_id = ".$liguma_id."
            " . (!empty($_POST['piel_citi']) ? "AND id NOT IN (".implode(',', array_keys($_POST['piel_citi'])).") " : '')."
        ";

        db_query($sql);

        // updating pielikumi_citi

        if (!empty($_POST['piel_citi'])) {

          foreach($_POST['piel_citi'] as $piel_citi_id => $p) {

            $sql = "
              UPDATE `".DB_PREF."pielikumi_citi`
              SET
                atslegvards = '".esc($p['atslegvards'])."',
                konts = '".esc($p['konts'])."',
                nozare_id = '".esc($p['nozare_id'])."'
              WHERE id = ".$piel_citi_id."
              LIMIT 1
            ";

            db_query($sql);

          }

        }

      }

      // adding pielikumi_citi

      if (!empty($_POST['piel_citi_new']['atslegvards'])) {

        foreach($_POST['piel_citi_new']['atslegvards'] as $i => $atslegvards) {

          $konts = $_POST['piel_citi_new']['konts'][$i];
          $konts = $_POST['piel_citi_new']['nozare_id'][$i];

          $sql = "
            INSERT INTO `".DB_PREF."pielikumi_citi` (
              liguma_id,
              atslegvards,
              konts,
              nozare_id,
            ) VALUES (
              ".$liguma_id.",
              '".esc($atslegvards)."',
              '".esc($konts)."',
              '".esc($nozare_id)."'
            )
          ";

          db_query($sql);
        }
      }

      //  MPK
      if($mpk) {

        //  Atslēgvārdi
        $log_data['mpk_keywords']['old_m'] = db_get_assoc_all(db_query("SELECT * FROM `".DB_PREF."mpk_keywords` WHERE `ligums_id` = " . $liguma_id));

        //  Dzēšam vecos a/v
        db_query('DELETE FROM '.DB_PREF.'mpk_keywords WHERE ligums_id = ' . $liguma_id);

        //  Saglabājam labotos
        if(isset($_POST['Keyword'])) {
          foreach((array)$_POST['Keyword'] as $data) {
            $keyword = new mpkKeyword;
            $keyword->setValues($data);
            $keyword->ligums_id = $liguma_id;
            $keyword->save();
          }
        }


        //  Saglabājam jaunos
        if(isset($_POST['NewKeyword'])) {
          foreach($_vars['mpk_sites'] as $site) {
            foreach((array)$_POST['NewKeyword'][$site]['keyword'] as $i => $kw) {
              $keyword = new mpkKeyword;
              $keyword->ligums_id = $liguma_id;
              $keyword->site = $site;
              $keyword->keyword = $kw;
              $keyword->top3 = $_POST['NewKeyword'][$site]['top3'][$i];
              $keyword->nozare_id = $_POST['NewKeyword'][$site]['nozare_id'][$i];
              $keyword->domens = $_POST['NewKeyword'][$site]['domens'][$i];
              $keyword->konts = $_POST['NewKeyword'][$site]['konts'][$i];
              $keyword->status = $_POST['NewKeyword'][$site]['status'][$i];
              $keyword->save();
            }
          }
        }

        $log_data['mpk_keywords']['new_m'] = db_get_assoc_all(db_query("SELECT * FROM `".DB_PREF."mpk_keywords` WHERE `ligums_id` = " . $liguma_id));

        //  Tēmas
        $log_data['mpk_categories']['old_m'] = db_get_assoc_all(db_query("SELECT * FROM `".DB_PREF."mpk_categories` WHERE `ligums_id` = " . $liguma_id));

        //  Dzēšam vecos a/v
        db_query('DELETE FROM '.DB_PREF.'mpk_categories WHERE ligums_id = ' . $liguma_id);

        //  Saglabājam labotos
        if(isset($_POST['Category'])) {
          foreach((array)$_POST['Category'] as $data) {
            $category = new mpkCategory;
            $category->setValues($data);
            $category->ligums_id = $liguma_id;
            $category->save();
          }
        }

        //  Saglabājam jaunos
        foreach($_vars['mpk_sites'] as $site) {
          if(isset($_POST['NewCategory'][$site])) {
            foreach((array)$_POST['NewCategory'][$site]['name'] as $i => $name) {
              $category = new mpkCategory;
              $category->ligums_id = $liguma_id;
              $category->site = $site;
              $category->name = $name;
              $category->domens = $_POST['NewCategory'][$site]['domens'][$i];
              $category->konts = $_POST['NewCategory'][$site]['konts'][$i];
              $category->status = $_POST['NewCategory'][$site]['status'][$i];
              $category->save();
            }
          }
        }

        $log_data['mpk_categories']['new_m'] = db_get_assoc_all(db_query("SELECT * FROM `".DB_PREF."mpk_categories` WHERE `ligums_id` = " . $liguma_id));


        //  Analītika
        if(isset($_POST['Analytics'])) {
          $log_data['mpk_analytics']['old_m'] = db_get_assoc_all(db_query("SELECT * FROM `".DB_PREF."mpk_analytics` WHERE `ligums_id` = " . $liguma_id));

          //  google analytics
          $google_analytics = $ligums->getMpkAnalytics('google');
          $google_analytics->ligums_id = $google_analytics->ligums_id ? $google_analytics->ligums_id : $liguma_id;
          $google_analytics->enabled = isset($_POST['Analytics']['google']['enabled']) ? 1 : 0;
          $google_analytics->comment = $_POST['Analytics']['google']['comment'];
          $google_analytics->save();

          //  yandex metrika
          $yandex_metrika = $ligums->getMpkAnalytics('yandex');
          $yandex_metrika->ligums_id = $yandex_metrika->ligums_id ? $yandex_metrika->ligums_id : $liguma_id;
          $yandex_metrika->enabled = isset($_POST['Analytics']['yandex']['enabled']) ? 1 : 0;
          $yandex_metrika->comment = $_POST['Analytics']['yandex']['comment'];
          $yandex_metrika->save();

          $log_data['mpk_analytics']['new_m'] = db_get_assoc_all(db_query("SELECT * FROM `".DB_PREF."mpk_analytics` WHERE `ligums_id` = " . $liguma_id));
        }

        //  Display Misc
        if(isset($_POST['DisplayMisc'])) {
           $log_data['mpk_display_misc']['old_m'] = db_get_assoc_all(db_query("SELECT * FROM `".DB_PREF."mpk_displays_misc` WHERE `ligums_id` = " . $liguma_id));

           $yandex_kontext = $ligums->getMpkDisplayMisc('yandex');
           $yandex_kontext->description = $_POST['DisplayMisc']['yandex']['description'];
           $yandex_kontext->save();

           $google_display_misc = $ligums->getMpkDisplayMisc('google');
           $google_display_misc->text_ad = isset($_POST['DisplayMisc']['google']['text_ad']) ? 1 : 0;
           $google_display_misc->image_ad = isset($_POST['DisplayMisc']['google']['image_ad']) ? 1 : 0;
           $google_display_misc->save();

           $log_data['mpk_display_misc']['new_m'] = db_get_assoc_all(db_query("SELECT * FROM `".DB_PREF."mpk_displays_misc` WHERE `ligums_id` = " . $liguma_id));
        }

        //  Konversijas
        if(isset($_POST['Conversion'])) {
          $log_data['mpk_conversions']['old_m'] = db_get_assoc_all(db_query("SELECT * FROM `".DB_PREF."mpk_conversions` WHERE `ligums_id` = " . $liguma_id));

          //  google conversions
          $google_conversion = $ligums->getMpkConversion('google');
          $google_conversion->ligums_id = $google_conversion->ligums_id ? $google_conversion->ligums_id : $liguma_id;
          $google_conversion->enabled = isset($_POST['Conversion']['google']['enabled']) ? 1 : 0;
          $google_conversion->comment = $_POST['Conversion']['google']['comment'];
          $google_conversion->konts = $_POST['Conversion']['google']['konts'];
          $google_conversion->save();

          $log_data['mpk_conversions']['new_m'] = db_get_assoc_all(db_query("SELECT * FROM `".DB_PREF."mpk_conversions` WHERE `ligums_id` = " . $liguma_id));
        }

        //  Displeji
        if(isset($_POST['Display'])) {
          $log_data['mpk_displays']['old_m'] = db_get_assoc_all(db_query("SELECT * FROM `".DB_PREF."mpk_displays` WHERE `ligums_id` = " . $liguma_id));

          //  Google displays
          foreach($_vars['mpk_google_displays'] as $code) {
            $google_display = $ligums->getMpkDisplay('google', $code);
            $google_display->ligums_id = $google_display->ligums_id ? $google_display->ligums_id : $liguma_id;
            $google_display->enabled = isset($_POST['Display']['google'][$code]['enabled']) ? 1 : 0;
            $google_display->domens = $_POST['Display']['google'][$code]['domens'];
            $google_display->konts = $_POST['Display']['google'][$code]['konts'];
            $google_display->status = $_POST['Display']['google'][$code]['status'];
            $google_display->save();
          }

          $log_data['mpk_displays']['new_m'] = db_get_assoc_all(db_query("SELECT * FROM `".DB_PREF."mpk_displays` WHERE `ligums_id` = " . $liguma_id));
        }

        //  Citi
        if(isset($_POST['Other'])) {
          $log_data['mpk_others']['old_m'] = db_get_assoc_all(db_query("SELECT * FROM `".DB_PREF."mpk_others` WHERE `ligums_id` = " . $liguma_id));

          $other = $ligums->getMpkOther();
          $other->ligums_id = $other->ligums_id ? $other->ligums_id : $liguma_id;
          $other->description = esc($_POST['Other']['description']);
          $other->save();

          $log_data['mpk_others']['new_m'] = db_get_assoc_all(db_query("SELECT * FROM `".DB_PREF."mpk_others` WHERE `ligums_id` = " . $liguma_id));
        }
      }

			// domains
			$current_domain_data = db_get_assoc_all(db_query("SELECT * FROM `".DB_PREF."ligumi_domains` WHERE `ligumi_id` = " . $liguma_id));
			$domains = array();
			foreach($current_domain_data as $row){
				$domains[] = $row['domain'];
			}

			// add new
			if(isset($_POST['websites']) && is_array($_POST['websites'])){

				foreach($_POST['websites'] as $website){
					if(!$website) continue; // ignore empty
					if(!in_array($website, $domains)){
						$sql = "INSERT INTO `".DB_PREF."ligumi_domains`(ligumi_id, domain) VALUES(".esc($liguma_id).", '".esc($website)."')";
						db_query($sql);
					}
				}

			}

			// remove missing
			foreach($domains as $domain){
				if(!in_array($domain, $_POST['websites'])){
					$sql = "DELETE FROM `".DB_PREF."ligumi_domains` WHERE ligumi_id = ".esc($liguma_id)." AND domain='".esc($domain)."'";
					db_query($sql);
				}
			}

			// end: domains

    } //  end: admin only

    //  admin and bookkeeper.
    //if((is_admin() && !is_limited()) || is_bookkeeper()) {
    if(check_access('ligumi-labot-pilns') || check_access('ligumi-labot-rekini')) {

      //  INVOICES

      $log_data['rekini']['old_m'] = db_get_assoc_all(db_query("SELECT * FROM `".DB_PREF."rekini` WHERE `liguma_id` = " . $liguma_id));

      if (!empty($_GET['id'])) {

        // deleting old invoices

        $sql = "
          DELETE FROM `".DB_PREF."rekini`
          WHERE
            liguma_id = ".$liguma_id."
            " . (!empty($_POST['rekini']) ? "AND id NOT IN (".implode(',', array_keys($_POST['rekini'])).") " : '')."
        ";

        db_query($sql);

        // updating invoices

        if (!empty($_POST['rekini'])) {

          foreach($_POST['rekini'] as $rekina_id => $r) {

            $old_reknr = db_get_val(db_query('SELECT rek_nr FROM ' . DB_PREF . 'rekini WHERE id = ' . esc($rekina_id)));

            $rek_nr = !empty($r['rek_nr']) ? "'" . esc($r['rek_nr']) . "'" : null;
            $izr_datums = !empty($r['izr_datums']) ? "'".date('Y-m-d', strtotime($r['izr_datums']))."'" : 'null';
            $summa = (strlen($r['summa']) != 0) ? (float) str_replace(',', '.', $r['summa']) : 'null';
            $pvn = (strlen($r['pvn']) != 0) ? (float)str_replace(',', '.', $r['pvn']) : 'null';
            $kopsumma = (strlen($r['kopsumma']) != 0) ? (float) str_replace(',', '.', $r['kopsumma']) : 'null';
            $apm_soda_summa = (strlen($r['apm_soda_summa']) != 0) ? (float) str_replace(',', '.', $r['apm_soda_summa']) : 'null';
            $apm_termins = !empty($r['apm_termins']) ? "'".date('Y-m-d', strtotime($r['apm_termins']))."'" : 'null';
            $anul_datums = !empty($r['anul_datums']) ? "'".date('Y-m-d', strtotime($r['anul_datums']))."'" : 'null';
            $nodots_piedz_datums = !empty($r['nodots_piedz_datums']) ? "'".date('Y-m-d', strtotime($r['nodots_piedz_datums']))."'" : 'null';

            $sql = "
              UPDATE `".DB_PREF."rekini`
              SET
                rek_nr = ".$rek_nr.",
                izr_datums = ".$izr_datums.",
                statuss = ".(int)$r['statuss'].",
                summa = ".$summa.",
                pvn = ".$pvn.",
                kopsumma = ".$kopsumma.",
                apm_soda_summa = ".$apm_soda_summa.",
                apm_termins = ".$apm_termins.",
                anulets = ".(!empty($r['anulets']) ? 1 : 0).",
                anul_datums = ".$anul_datums.",
                nodots_piedz = ".(!empty($r['nodots_piedz']) ? 1 : 0).",
                nodots_piedz_datums = ".$nodots_piedz_datums.",
                barteris = ".(!empty($r['barteris']) ? 1 : 0).",
                atkapsanas_rekins = ".(!empty($r['atkapsanas_rekins']) ? 1 : 0)."
              WHERE id = ".$rekina_id."
              LIMIT 1
            ";

            db_query($sql);

            //  ja mainījies rēķina nr tad dzēšam laukā uzģenerētos rēķinus
            if($old_reknr != $r['rek_nr']) {
              $file_ids = array_filter(array_values(db_get_assoc(db_query('
                SELECT fails_avansa_ep, fails_avansa_pp, fails_gala_ep, fails_gala_pp
                FROM '.DB_PREF.'rekini WHERE id = ' . esc($rekina_id)))));

              if(!empty($file_ids)) {
                db_query("DELETE FROM `".DB_PREF."faili` WHERE `id` IN (".implode(',', $file_ids).")");
                db_query("
                  UPDATE `".DB_PREF."rekini`
                  SET
                    fails_avansa_ep = '',
                    fails_avansa_pp = '',
                    fails_gala_ep = '',
                    fails_gala_pp = ''
                  WHERE `id` = ".esc($rekina_id)."
                  LIMIT 1
                ");
              }
            }

          }

        }

      }

      // adding invoices

      if (!empty($_POST['rekini_new']['izr_datums'])) {


        foreach($_POST['rekini_new']['izr_datums'] as $i => $izr_datums) {
          $rek_nr = !empty($_POST['rekini_new']['rek_nr'][$i]) ? "'".$_POST['rekini_new']['rek_nr'][$i]."'" : 'null';
          $statuss = $_POST['rekini_new']['statuss'][$i];
          $summa = (strlen($_POST['rekini_new']['summa'][$i]) != 0) ? (float) str_replace(',', '.', $_POST['rekini_new']['summa'][$i]) : 'null';
          $pvn = (strlen($_POST['rekini_new']['pvn'][$i]) != 0) ? (float)str_replace(',', '.', $_POST['rekini_new']['pvn'][$i]) : 'null';
          $kopsumma = (strlen($_POST['rekini_new']['kopsumma'][$i]) != 0) ? (float) str_replace(',', '.', $_POST['rekini_new']['kopsumma'][$i]) : 'null';
          $anulets = !empty($_POST['rekini_new']['anulets'][$i]) ? 1 : 0;
          $anul_datums = !empty($_POST['rekini_new']['anul_datums'][$i]) ? "'".date('Y-m-d', strtotime($_POST['rekini_new']['anul_datums'][$i]))."'" : 'null';
          $nodots_piedz = !empty($_POST['rekini_new']['nodots_piedz'][$i]) ? 1 : 0;
          $nodots_piedz_datums = !empty($_POST['rekini_new']['nodots_piedz_datums'][$i]) ? "'".date('Y-m-d', strtotime($_POST['rekini_new']['nodots_piedz_datums'][$i]))."'" : 'null';
          $barteris = !empty($_POST['rekini_new']['barteris'][$i]) ? 1 : 0;
          $atkapsanas_rekins = !empty($_POST['rekini_new']['atkapsanas_rekins'][$i]) ? 1 : 0;

          $izr_datums = !empty($izr_datums) ? "'".date('Y-m-d', strtotime($izr_datums))."'" : 'null';
          $apm_termins = !empty($_POST['rekini_new']['apm_termins'][$i]) ? "'".date('Y-m-d', strtotime($_POST['rekini_new']['apm_termins'][$i]))."'" : 'null';

          if ($barteris) {
            if ($izr_datums == 'null') $izr_datums = 'NOW()';
            if ($apm_termins == 'null') $apm_termins = 'NOW()';
          }

          $sql = "
            INSERT INTO `".DB_PREF."rekini`
            SET
              rek_nr = ".$rek_nr.",
              liguma_id = ".$liguma_id.",
              izr_datums = ".$izr_datums.",
              statuss = ".(int)$statuss.",
              summa = ".$summa.",
              pvn = ".$pvn.",
              kopsumma = ".$kopsumma.",
              apm_termins = ".$apm_termins.",
              anulets = ".$anulets.",
              anul_datums = ".$anul_datums.",
              nodots_piedz = ".$nodots_piedz.",
              nodots_piedz_datums = ".$nodots_piedz_datums.",
              barteris = ".$barteris.",
              atkapsanas_rekins = ".$atkapsanas_rekins."
          ";

          if (db_query($sql) && $barteris) {

            $new_rek_id = db_last_id();

            $result = db_query("
              INSERT INTO `".DB_PREF."rekini_maksajumi` (
                rekina_id,
                datums,
                summa,
                created_pardeveja_id
              ) VALUES (
                ".$new_rek_id.",
                NOW(),
                ".(float)$kopsumma.",
                ".$_SESSION['user']['id']."
              )
            ");

            if ($result) {

              db_query("
                UPDATE `".DB_PREF."rekini`
                SET
                  apm_summa = IFNULL(apm_summa, 0) + ".(float)$kopsumma."
                WHERE id = ".$new_rek_id."
                LIMIT 1
              ");

            }

          }

        }

      }

      // updating invoice deliveries

      $piegades_1 = db_get_assoc(db_query("SELECT * FROM `".DB_PREF."ligumi_rekinu_piegades` WHERE `liguma_id` = " . $liguma_id . " AND piegades_veids = 1"));
      if (!empty($piegades_1))
        $log_data['piegades_1']['old'] = $piegades_1;


      $piegades_2 = db_get_assoc(db_query("SELECT * FROM `".DB_PREF."ligumi_rekinu_piegades` WHERE `liguma_id` = " . $liguma_id . " AND piegades_veids = 2"));
      if (!empty($piegades_2))
        $log_data['piegades_2']['old'] = $piegades_2;

      $sql = "
        DELETE FROM `".DB_PREF."ligumi_rekinu_piegades`
        WHERE liguma_id = ".$liguma_id."
      ";

      db_query($sql);

      if (!empty($_POST['piegades'])) {

        foreach($_POST['piegades'] as $k => $p) {

          if (!empty($p['check'])) {

            $sql = "
              INSERT INTO `".DB_PREF."ligumi_rekinu_piegades`
              SET
                liguma_id = ".$liguma_id.",
                piegades_veids = ".(int)$k.",
                adrese = '".esc($p['adrese'])."'
            ";

            db_query($sql);

          }

        }

      }

      $log_data['ligums']['old_m'] = db_get_assoc_all(db_query("SELECT `soda_proc`, `auto_summas`, 'apkalp_maksa_proc' FROM `".DB_PREF."ligumi` WHERE `id` = " . $liguma_id));

      $sql = "
          UPDATE `".DB_PREF."ligumi`
          SET
            soda_proc = ".(float)str_replace(',', '.', $_POST['soda_proc']).",
            auto_summas = ".(!empty($_POST['auto_summas']) ? 1 : 0).",
            apkalp_maksa_proc = ".(isset($_POST['apkalp_maksa_proc']) ? (int)$_POST['apkalp_maksa_proc'] : 0).",
            bank_account = '".esc($_POST['bank_account'])."'
          WHERE id = ".$liguma_id."
          LIMIT 1
        ";

        db_query($sql);

      // end: INVOICES
    }

    //  Admin only
    if (check_access('ligumi-labot-pilns')) {

      $log_data['parsl_rinda']['old_m'] = db_get_assoc_all(db_query("SELECT * FROM `".DB_PREF."ligumi_parslegsanu_rinda` WHERE `liguma_id` = " . $liguma_id));

      if (!empty($_GET['id'])) {

        // dzēšam noņemtos pārslēgšanas rindas ierakstus

        $sql = "
          DELETE FROM `".DB_PREF."ligumi_parslegsanu_rinda`
          WHERE
            liguma_id = ".$liguma_id."
            " . (!empty($_POST['parsl_rinda']) ? "AND id NOT IN (".implode(',', array_keys($_POST['parsl_rinda'])).") " : '')."
        ";

        db_query($sql);

        // labojumam pārslēgšanas rindas ierakstus

        if (!empty($_POST['parsl_rinda'])) {

          foreach($_POST['parsl_rinda'] as $lig_parsl_rinda_id => $p) {

            $sql = "
              UPDATE `".DB_PREF."ligumi_parslegsanu_rinda`
              SET
                jaunais_liguma_id = ".(int)$p['jaunais_liguma_id']."
              WHERE id = ".$lig_parsl_rinda_id."
              LIMIT 1
            ";

            db_query($sql);

          }

        }

      }

      // pievienojam jaunus pārslēgšanas rindas ierakstus

      if (!empty($_POST['parsl_rinda_new']['jaunais_liguma_id'])) {

        foreach($_POST['parsl_rinda_new']['jaunais_liguma_id'] as $i => $jaunais_liguma_id) {

          if (!empty($jaunais_liguma_id)) {

            $sql = "
              INSERT INTO `".DB_PREF."ligumi_parslegsanu_rinda` (
                liguma_id,
                jaunais_liguma_id,
                izveidoja_laiks,
                izveidoja_pardeveja_id
              ) VALUES (
                ".$liguma_id.",
                ".(int)$jaunais_liguma_id.",
                NOW(),
                '".$_SESSION['user']['id']."'
              )
            ";

            db_query($sql);

          }

        }

      }

      // after everything is done, add reminders if needed

      $new_data = db_get_assoc(db_query("SELECT * FROM `".DB_PREF."ligumi` WHERE `id` = " . $liguma_id));

      $beigu_statuss_data = db_get_assoc(db_query("SELECT * FROM `".DB_PREF."ligumi_beigusies` WHERE `liguma_id` = " . $liguma_id));

      $query = db_query("SELECT * FROM `".DB_PREF."rekini` WHERE `liguma_id` = " . $liguma_id . " ORDER BY id ASC");
      $new_rekini_data = array();

      while($row = db_get_assoc($query)) {
        $new_rekini_data[] = $row;
      }

      if (empty($old_data)) {

        generate_auto_reminders($new_data, $beigu_statuss_data, $new_rekini_data);

      } else {

        $old_rekl_statuss = get_reklamas_statuss($old_data);
        $new_rekl_statuss = get_reklamas_statuss($new_data);

        $old_apm_statuss = get_apmaksas_statuss($old_rekini_data);
        $new_apm_statuss = get_apmaksas_statuss($new_rekini_data);

        if ($old_rekl_statuss != $new_rekl_statuss || $old_data['google_statuss'] != $new_data['google_statuss'] || $old_apm_statuss != $new_apm_statuss) {

          generate_auto_reminders($new_data, $beigu_statuss_data, $new_rekini_data);

        }

      }

      if (empty($_POST['grupas_regnr_display']))
        $_POST['grupas_regnr'] = '';

      $current_group = db_get_assoc(db_query("SELECT * FROM `".DB_PREF."ligumi_grupas` WHERE `liguma_id` = " . $liguma_id));

      if (!empty($_POST['grupas_regnr'])) {

        if (!empty($current_group)) {

          db_query("
            UPDATE `".DB_PREF."ligumi_grupas`
            SET
              grupas_regnr = '".esc($_POST['grupas_regnr'])."',
              manuala_piesaiste = 1
            WHERE
             `liguma_id` = ".$liguma_id."
            LIMIT 1
          ");

        } else {

          db_query("
            INSERT INTO `".DB_PREF."ligumi_grupas`
            SET
              grupas_regnr = '".esc($_POST['grupas_regnr'])."',
              liguma_id = ".$liguma_id.",
              manuala_piesaiste = 1
          ");

        }

      } else {

        if (!empty($current_group)) {

          if (empty($current_group['manuala_piesaiste'])) {

            if (!empty($new_data['regnr'])) {

              if ($new_data['regnr'] != $current_group['grupas_regnr'])
                db_query("UPDATE `".DB_PREF."ligumi_grupas` SET grupas_regnr = '".esc($new_data['regnr'])."' WHERE `liguma_id` = ".$liguma_id." LIMIT 1");

            } else {
              db_query("DELETE FROM `".DB_PREF."ligumi_grupas` WHERE `liguma_id` = ".$liguma_id." LIMIT 1");
            }

          } else {

              db_query("DELETE FROM `".DB_PREF."ligumi_grupas` WHERE `liguma_id` = ".$liguma_id." LIMIT 1");

              if (!empty($new_data['regnr'])) {
                db_query("
                  INSERT INTO `".DB_PREF."ligumi_grupas`
                  SET
                    grupas_regnr = '".esc($new_data['regnr'])."',
                    liguma_id = ".$liguma_id."
                ");
              }

          }

        } else {

          if (!empty($new_data['regnr'])) {
            db_query("
              INSERT INTO `".DB_PREF."ligumi_grupas`
              SET
                grupas_regnr = '".esc($new_data['regnr'])."',
                liguma_id = ".$liguma_id."
            ");
          }

        }

      }

      if (empty($log_data['ligums']['old']))
        $log_data['ligums']['title'] = sprintf('Pievienoja jaunu līgumu %s', $log_data['ligums']['new']['ligumanr']);
      else
        $log_data['ligums']['title'] = sprintf('Laboja līgumu %s', $log_data['ligums']['new']['ligumanr']);

      $log_data['kontakti']['new_m'] = db_get_assoc_all(db_query("SELECT * FROM `".DB_PREF."kontakti` WHERE `liguma_id` = " . $liguma_id));
      $log_data['kontakti']['title'] = sprintf('Laboja līguma %s kontaktus', $log_data['ligums']['new']['ligumanr']);

      $log_data['pakalpojumi']['new_m'] = db_get_assoc_all(db_query("SELECT * FROM `".DB_PREF."pakalpojumi` WHERE `liguma_id` = " . $liguma_id));
      $log_data['pakalpojumi']['title'] = sprintf('Laboja līguma %s pakalpojumus', $log_data['ligums']['new']['ligumanr']);

      $log_data['pielikumi_infomedia']['new_m'] = db_get_assoc_all(db_query("SELECT * FROM `".DB_PREF."pielikumi_infomedia` WHERE `liguma_id` = " . $liguma_id));
      $log_data['pielikumi_infomedia']['title'] = sprintf('Laboja līguma %s Infomedia atslēgvārdus', $log_data['ligums']['new']['ligumanr']);

      $log_data['pielikumi_tiessaite']['new_m'] = db_get_assoc_all(db_query("SELECT * FROM `".DB_PREF."pielikumi_tiessaite` WHERE `liguma_id` = " . $liguma_id));
      $log_data['pielikumi_tiessaite']['title'] = sprintf('Laboja līguma %s Google atslēgvārdus', $log_data['ligums']['new']['ligumanr']);

      $log_data['pielikumi_yandex']['new_m'] = db_get_assoc_all(db_query("SELECT * FROM `".DB_PREF."pielikumi_yandex` WHERE `liguma_id` = " . $liguma_id));
      $log_data['pielikumi_yandex']['title'] = sprintf('Laboja līguma %s Yandex atslēgvārdus', $log_data['ligums']['new']['ligumanr']);

      $log_data['pielikumi_citi']['new_m'] = db_get_assoc_all(db_query("SELECT * FROM `".DB_PREF."pielikumi_citi` WHERE `liguma_id` = " . $liguma_id));
      $log_data['pielikumi_citi']['title'] = sprintf('Laboja līguma %s citus atslēgvārdus', $log_data['ligums']['new']['ligumanr']);

      $log_data['rekini']['new_m'] = db_get_assoc_all(db_query("SELECT * FROM `".DB_PREF."rekini` WHERE `liguma_id` = " . $liguma_id));
      $log_data['rekini']['title'] = sprintf('Laboja līguma %s rēķinus', $log_data['ligums']['new']['ligumanr']);

      $piegades_1 = db_get_assoc(db_query("SELECT * FROM `".DB_PREF."ligumi_rekinu_piegades` WHERE `liguma_id` = " . $liguma_id . " AND piegades_veids = 1"));
      if (!empty($piegades_1))
        $log_data['piegades_1']['new'] = $piegades_1;

      if (!empty($log_data['piegades_1']['old']) && !empty($log_data['piegades_1']['new']))
        $log_data['piegades_1']['title'] = sprintf('Laboja līguma %s rēķinu piegādes e-pastu', $log_data['ligums']['new']['ligumanr']);
      else if (!empty($log_data['piegades_1']['old']))
        $log_data['piegades_1']['title'] = sprintf('Dzēsa līguma %s rēķinu piegādes e-pastu', $log_data['ligums']['new']['ligumanr']);
      else if (!empty($log_data['piegades_1']['new']))
        $log_data['piegades_1']['title'] = sprintf('Pievienoja līguma %s rēķinu piegādes e-pastu', $log_data['ligums']['new']['ligumanr']);

      $piegades_2 = db_get_assoc(db_query("SELECT * FROM `".DB_PREF."ligumi_rekinu_piegades` WHERE `liguma_id` = " . $liguma_id . " AND piegades_veids = 2"));
      if (!empty($piegades_2))
        $log_data['piegades_2']['new'] = $piegades_2;

      if (!empty($log_data['piegades_2']['old']) && !empty($log_data['piegades_2']['new']))
        $log_data['piegades_2']['title'] = sprintf('Laboja līguma %s rēķinu piegādes e-pastu', $log_data['ligums']['new']['ligumanr']);
      else if (!empty($log_data['piegades_2']['old']))
        $log_data['piegades_2']['title'] = sprintf('Dzēsa līguma %s rēķinu piegādes e-pastu', $log_data['ligums']['new']['ligumanr']);
      else if (!empty($log_data['piegades_2']['new']))
        $log_data['piegades_2']['title'] = sprintf('Pievienoja līguma %s rēķinu piegādes e-pastu', $log_data['ligums']['new']['ligumanr']);

      $log_data['parsl_rinda']['new_m'] = db_get_assoc_all(db_query("SELECT * FROM `".DB_PREF."ligumi_parslegsanu_rinda` WHERE `liguma_id` = " . $liguma_id));
      $log_data['parsl_rinda']['title'] = sprintf('Laboja līguma %s pārslēgšanas rindu', $log_data['ligums']['new']['ligumanr']);

    }

    // tālākās darbības ir gan darbiniekiem, gan adminiem.

    if (!empty($_GET['id']) || check_access('ligumi-jauns')) {

      if (!empty($_GET['id'])) {
        $liguma_id = (int)$_GET['id'];
      }

      $log_data['atgadinajumi']['old_m'] = db_get_assoc_all(db_query("SELECT * FROM `".DB_PREF."atgadinajumi` WHERE `liguma_id` = " . $liguma_id));

      if (!empty($_GET['id'])) {

        // updating reminders

        if (!empty($_POST['atgadinajumi'])) {

          foreach($_POST['atgadinajumi'] as $atgadinajuma_id => $a) {

            $sql = "
              UPDATE `".DB_PREF."atgadinajumi`
              SET
                datums = '".esc(date('Y-m-d', strtotime($a['datums'])))."',
                redzamiba = '".$a['redzamiba']."',
                saturs = '".esc($a['saturs'])."'
              WHERE id = ".$atgadinajuma_id."
              LIMIT 1
            ";

            db_query($sql);

          }

        }

      }

      // adding reminders

      if (!empty($_POST['atgadinajumi_new']['datums'])) {

        foreach($_POST['atgadinajumi_new']['datums'] as $i => $datums) {

          $redzamiba = $_POST['atgadinajumi_new']['redzamiba'][$i];
          $saturs = $_POST['atgadinajumi_new']['saturs'][$i];

          $sql = "
            INSERT INTO `".DB_PREF."atgadinajumi` (
              liguma_id,
              datums,
              redzamiba,
              saturs,
              created_pardeveja_id
            ) VALUES (
              ".$liguma_id.",
              '".esc(date('Y-m-d', strtotime($datums)))."',
              '".$redzamiba."',
              '".esc($saturs)."',
              ".$_SESSION['user']['id']."
            )
          ";

          db_query($sql);

        }

      }

      $log_data['atgadinajumi']['new_m'] = db_get_assoc_all(db_query("SELECT * FROM `".DB_PREF."atgadinajumi` WHERE `liguma_id` = " . $liguma_id));
      $log_data['atgadinajumi']['title'] = sprintf('Laboja līguma %s atgādinājumus', db_get_val(db_query("SELECT ligumanr FROM `".DB_PREF."ligumi` WHERE `id` = " . $liguma_id)));

      // gan parastajiem adminiem, gan limitētajiem
      if (check_access('ligumi-labot-saistitie-pardeveji')) {

        $log_data['saist_pardeveji']['old_m'] = db_get_assoc_all(db_query("SELECT * FROM `".DB_PREF."ligumi_saistitie_pardeveji` WHERE `liguma_id` = " . $liguma_id));

        if (!empty($_GET['id'])) {

          // dzēšam noņemtos saistītos pārdevējus

          $sql = "
            DELETE FROM `".DB_PREF."ligumi_saistitie_pardeveji`
            WHERE
              liguma_id = ".$liguma_id."
              " . (!empty($_POST['saistitie_pardeveji']) ? "AND id NOT IN (".implode(',', array_keys($_POST['saistitie_pardeveji'])).") " : '')."
          ";

          db_query($sql);

          // labojumam saistītos pārdevējus

          if (!empty($_POST['saistitie_pardeveji'])) {

            foreach($_POST['saistitie_pardeveji'] as $lig_saist_pard_id => $p) {

              $sql = "
                UPDATE `".DB_PREF."ligumi_saistitie_pardeveji`
                SET
                  pardeveja_id = ".(int)$p['pardeveja_id'].",
                  piesaist_datums = '".esc(date('Y-m-d', strtotime($p['piesaist_datums'])))."'
                WHERE id = ".$lig_saist_pard_id."
                LIMIT 1
              ";

              db_query($sql);

            }

          }

        }

        // pievienojam jaunus saistītos pārdevējus

        if (!empty($_POST['saistitie_pardeveji_new']['pardeveja_id'])) {

          if ($liguma_statuss == 6 && (empty($liguma_beigu_statuss) || $liguma_beigu_statuss['beigu_statuss'] == 'termins')) {

            $_SESSION['show_status_change'] = true;
            $_SESSION['on_status_change_add_saist_pard'] = $_POST['saistitie_pardeveji_new'];

          } else {

            foreach($_POST['saistitie_pardeveji_new']['pardeveja_id'] as $i => $pardeveja_id) {

              $piesaist_datums = $_POST['saistitie_pardeveji_new']['piesaist_datums'][$i];

              if (!empty($pardeveja_id) && !empty($piesaist_datums)) {

                $sql = "
                  INSERT INTO `".DB_PREF."ligumi_saistitie_pardeveji` (
                    liguma_id,
                    pardeveja_id,
                    piesaist_datums
                  ) VALUES (
                    ".$liguma_id.",
                    ".(int)$pardeveja_id.",
                    '".esc(date('Y-m-d', strtotime($piesaist_datums)))."'
                  )
                ";

                db_query($sql);

              }

            }

          }

        }

        $log_data['saist_pardeveji']['new_m'] = db_get_assoc_all(db_query("SELECT * FROM `".DB_PREF."ligumi_saistitie_pardeveji` WHERE `liguma_id` = " . $liguma_id));
        $log_data['saist_pardeveji']['title'] = sprintf('Laboja līguma %s saistītos pārdevējus', db_get_val(db_query("SELECT ligumanr FROM `".DB_PREF."ligumi` WHERE `id` = " . $liguma_id)));

      }

      // inserting comments

      process_comments($form_names, $liguma_id);

    }

    log_add("laboja", $log_data);

      // inserting files

      /*
      'lig_klients',
      'lig_pakalpojums',
      'lig_rekini',
      'lig_atgadinajumi',
      */

      /*if (!is_limited()) {
        process_files($form_names, $liguma_id);
      } elseif(is_bookkeeper()) {
        process_files(array('lig_rekini', 'lig_atgadinajumi'), $liguma_id);
      } elseif(is_minimal()) {
        process_files(array('lig_klients', 'lig_rekini', 'lig_pakalpojums'), $liguma_id);
      } else {
        process_files(array('lig_atgadinajumi'), $liguma_id);
      } */

      $process_files = array();

      if(check_access('ligumi-files-klients')) {
        $process_files[] = 'lig_klients';
      }

      if(check_access('ligumi-files-pakalpojums')) {
        $process_files[] = 'lig_pakalpojums';
      }

      if(check_access('ligumi-files-rekini')) {
        $process_files[] = 'lig_rekini';
      }

      if(check_access('ligumi-files-atgadinajumi')) {
        $process_files[] = 'lig_atgadinajumi';
      }

      if(!empty($process_files)) {
        process_files($process_files, $liguma_id);
      }

    if (check_access('ligumi-parslegsana') && empty($_GET['id'])) {
      $_SESSION['show_parslegs_dialog'] = true;
    }

    header('Location: ?c=ligumi&a=labot&id=' . $liguma_id . '&subtab=' . $subtab);
    die();

  }

}

function reset_atra_saite($keyword)
{
  //  Atceļ visas ātrās saites kam tāds pats a/v ja tas nav unikāls
  if(keyword_exists($keyword) > 1) {

    $query = "SELECT p.*
      FROM `".DB_PREF."pielikumi_infomedia` p
      WHERE p.atslegvards = '" . esc($keyword) . "' AND atra_saite = 1";

    $result = db_query($query);

    while($row = db_get_assoc($result)){
      //  Nomaina uz 0
      db_query("UPDATE `".DB_PREF."pielikumi_infomedia`
        SET atra_saite = 0 WHERE id = " . $row['id']);

      //  Atgadinajums tikai par aktīvajiem
      if(get_liguma_status_by_id($row['liguma_id']) == 3) {
        db_query("
          INSERT INTO `".DB_PREF."atgadinajumi` (
            liguma_id,
            atgadinajuma_veida_id,
            datums,
            tema,
            saturs,
            redzamiba
          ) VALUES (
            ".$row['liguma_id'].",
            11,
            NOW(),
            1,
            'Noņemts \"Profils\" no a/v \"" . $keyword . "\", jo tas vairs nav unikāls',
            'admin'
          )
        ");
      }
    }
  }
}
?>