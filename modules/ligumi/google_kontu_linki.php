<?
if (!check_access('ligumi-google-kontu-linki')) {
	die();
}

if (empty($_GET['liguma_id'])) {
  die();
}

$liguma_id = (int) $_GET['liguma_id'];

$lig_data = db_get_assoc(db_query("SELECT * FROM `".DB_PREF."ligumi` WHERE `id` = " . $liguma_id)); // Legacy

$ligums = Ligums::model()->findByPk($liguma_id);

if (empty($lig_data)) {
  die();
}

$kontu_tipi = array(
  'tiessaite' => 'Google',
  'infomedia' => 'Infomedia',
  'yandex' => 'Yandex',
  'citi' => 'Citi'
);

$query = db_query("
  SELECT *
  FROM `".DB_PREF."pielikumi_kontu_linki` t
	WHERE t.liguma_id = ".$liguma_id."
");

$kontu_linki = array();
//$kontu_numuri = array();

foreach(db_get_assoc_all($query) as $row) {
	$kontu_linki[$row['tips']][$row['konts']] = array('links' => $row['links'], 'konta_numurs' => $row['konta_numurs']);
	//$kontu_numuri[$row['tips']][$row['konts']] = $row['konta_numurs'];
}

$kontu_nosaukumi = array();

foreach(array_keys($kontu_tipi) as $key) {
  $kontu_nosaukumi[$key] = array();
}

if($lig_data['mpk']) {

  //  MPK

  //  Categories
  foreach($ligums->getMpkCategories() as $row) {

    if($row->konts) {
      $type = $row->site == 'google' ? 'tiessaite' : $row->site;

      if(in_array($type, array_keys($kontu_tipi))) {

        if (!in_array($row->konts, $kontu_nosaukumi[$type])) {
    			$kontu_nosaukumi[$type][] = $row->konts;
        }
      }
    }
  }

  //  Keywords
  foreach($ligums->getMpkKeywords() as $row) {

    if($row->konts) {
      $type = $row->site == 'google' ? 'tiessaite' : $row->site;

      if(in_array($type, array_keys($kontu_tipi))) {

        if (!in_array($row->konts, $kontu_nosaukumi[$type])) {
    			$kontu_nosaukumi[$type][] = $row->konts;
        }
      }
    }
  }

  //  Conversions
  $conversion = $ligums->getMpkConversion();
  if($conversion->konts) {
    $type = $row->site == 'google' ? 'tiessaite' : $conversion->site;

    if(in_array($type, array_keys($kontu_tipi))) {

      if (!in_array($conversion->konts, $kontu_nosaukumi[$type])) {
  			$kontu_nosaukumi[$type][] = $conversion->konts;
      }
    }
  }

  //  Displays
  $display = $ligums->getMpkDisplay(null, null);
  if($display->konts) {
    $type = $display->site == 'google' ? 'tiessaite' : $display->site;

    if(in_array($type, array_keys($kontu_tipi))) {

      if (!in_array($display->konts, $kontu_nosaukumi[$type])) {
  			$kontu_nosaukumi[$type][] = $display->konts;
      }
    }
  }

} else {

  //  MPT
  $pielikumi_tiessaite = db_get_assoc_all(db_query("
    SELECT google_konts as konts
    FROM `".DB_PREF."pielikumi_tiessaite` t
  	WHERE t.liguma_id = ".$liguma_id."
  "));

  $pielikumi_infomedia = db_get_assoc_all(db_query("
    SELECT google_konts as konts
    FROM `".DB_PREF."pielikumi_infomedia` t
  	WHERE t.liguma_id = ".$liguma_id."
  "));

  $pielikumi_yandex = db_get_assoc_all(db_query("
    SELECT konts
    FROM `".DB_PREF."pielikumi_yandex` t
  	WHERE t.liguma_id = ".$liguma_id."
  "));

  $pielikumi_citi = db_get_assoc_all(db_query("
    SELECT konts
    FROM `".DB_PREF."pielikumi_citi` t
  	WHERE t.liguma_id = ".$liguma_id."
  "));


  foreach(array('tiessaite' => $pielikumi_tiessaite, 'infomedia' => $pielikumi_infomedia, 'yandex' => $pielikumi_yandex, 'citi' => $pielikumi_citi) as $tips => $pielikumi) {

    $kontu_nosaukumi[$tips] = array();

    foreach($pielikumi as $row) {
    	$konti = array_filter(array_map('trim', preg_split("/[,;]/", trim(strtolower($row['konts'])))));
    	foreach($konti as $konts) {
    		if (!in_array($konts, $kontu_nosaukumi[$tips])) {
    			$kontu_nosaukumi[$tips][] = $konts;
        }
    	}
    }
  }

}

if (!empty($_POST)) {

  $_POST = trim_array($_POST);

  if (!empty($_POST['kontu_linki'])) {

    foreach($_POST['kontu_linki'] as $tips => $linki) {

      foreach($linki as $konts => $links) {

  			if (isset($kontu_linki[$tips][$konts])) {

  	      db_query("
  	        UPDATE `".DB_PREF."pielikumi_kontu_linki`
  	        SET
  	          links = '".esc($links)."',
              konta_numurs = '".esc($_POST['kontu_numuri'][$tips][$konts])."'
  					WHERE
  						liguma_id = ".$liguma_id." AND
  	          tips = '".esc($tips)."' AND
  	          konts = '".esc($konts)."'
  					LIMIT 1
  	      ");

  			} else {

  	      db_query("
  	        INSERT INTO `".DB_PREF."pielikumi_kontu_linki`
  	        SET
  	          liguma_id = ".$liguma_id.",
  	          tips = '".esc($tips)."',
  	          konts = '".esc($konts)."',
  	          links = '".esc($links)."',
              konta_numurs = '".esc($_POST['kontu_numuri'][$tips][$konts])."'
  	      ");

  			}

      }

    }

  }

  ?>
  <script type="text/javascript">
  parent.document.location.reload();
  </script>
  <?
  die();

}

?>


<div class="google_kontu_linki_dialog">

  <form action="?c=ligumi&a=google_kontu_linki&liguma_id=<?= $liguma_id ?>&without_nav=1" method="post">

    <table class="data" style="width: 99%;">

      <thead>

        <tr class="last header">
          <th style="width: 8em;">Google konts</th>
          <th>Saite</th>
          <th>Konta numurs</th>
        </tr>

      </thead>

      <tbody class="main">

        <? foreach($kontu_nosaukumi as $tips => $konti) { ?>

          <? if (!empty($konti)) { ?>

            <tr>
              <td colspan="3"><strong><?= $kontu_tipi[$tips] ?></strong></td>
            </tr>

            <? foreach($konti as $konts) { ?>

              <tr>
                <td><?= $konts ?></td>
                <td class="c"><input type="text" name="kontu_linki[<?= $tips ?>][<?= $konts ?>]" value="<?= (isset($kontu_linki[$tips][$konts]['links']) ? $kontu_linki[$tips][$konts]['links'] : '') ?>" /></td>
                <td class="c"><input type="text" name="kontu_numuri[<?= $tips ?>][<?= $konts ?>]" value="<?= (isset($kontu_linki[$tips][$konts]['konta_numurs']) ? $kontu_linki[$tips][$konts]['konta_numurs'] : '') ?>" /></td>
              </tr>

            <? } ?>

          <? } ?>

        <? } ?>

      </tbody>

    </table>

    <div style="text-align: center;">
      <input type="hidden" name="form_sent" value="1" />
      <button class="ui-state-default ui-corner-all">Saglabāt</button>
    </div>

  </form>

</div>