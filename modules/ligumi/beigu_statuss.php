<?
if (!check_access('ligumi-beigu-statuss') || empty($_GET['liguma_id'])) {
  die();
}

$liguma_id = (int) $_GET['liguma_id'];

$lig_data = db_get_assoc(db_query("SELECT * FROM `".DB_PREF."ligumi` WHERE `id` = " . $liguma_id));

if (empty($lig_data)) {
  die();
}

if (!empty($_SESSION['on_status_change_add_saist_pard'])) {
  $add_saist_pard = $_SESSION['on_status_change_add_saist_pard'];
  unset($_SESSION['on_status_change_add_saist_pard']);
}

$saistitais_ligums = db_get_val(db_query("SELECT COUNT(*) FROM `".DB_PREF."ligumi_saistitie_pardeveji` WHERE `pardeveja_id` = " . $_SESSION['user']['id'] . " AND liguma_id = " . $liguma_id));

if (!check_access('ligumi-nenomainitie-statusi-visi') && $lig_data['pardeveja_id'] != $_SESSION['user']['id'] && empty($saistitais_ligums)) {
  die('Nav tiesību apskatīt līgumu!');
}

$stat_data = db_get_assoc(db_query("
	SELECT b.*, p.vards as pardeveja_vards FROM `".DB_PREF."ligumi_beigusies` b
	LEFT JOIN `".DB_PREF."pardeveji` p ON p.id = b.laboja_pardeveja_id
	WHERE `liguma_id` = " . $liguma_id));

if (!empty($stat_data['parslegts_lig_nr'])) {
  $parslegts_lig_id = db_get_val(db_query("SELECT id FROM `".DB_PREF."ligumi` WHERE `ligumanr` = '" . $stat_data['parslegts_lig_nr'] . "'"));
}

$can_pard_edit = true;
if (!empty($stat_data) && $stat_data['beigu_statuss'] == 'parslegts') {
  $can_pard_edit = false;
}

//$can_limited_admin_edit = !empty($add_saist_pard) ? true : false;
$can_limited_admin_edit = true;

if (!empty($_POST)) {

  $_POST = trim_array($_POST);

  $errors = validate_beigu_statuss_form($_POST);

  // neļauja pārdevējam aiztikt pārslēgtos (vai arī pārslēgt vēl neapstr. līgumu)
  //if (!is_admin() && ($stat_data['beigu_statuss'] == 'parslegts' || $_POST['beigu_statuss'] == 'parslegts')) {
  if (!check_access('ligumi-beigu-statuss-parslegtie') && ($stat_data['beigu_statuss'] == 'parslegts' || $_POST['beigu_statuss'] == 'parslegts')) {
    die();
  }

  if (empty($errors)) {

    $beigu_statuss = "'" . $_POST['beigu_statuss'] . "'";
    $parslegts_lig_nr = 'null';
    $atlikt_lidz = 'null';
    $komentars = 'null';

    if ($_POST['beigu_statuss'] == 'parslegts') {
      $parslegts_lig_nr = "'".$_POST['parslegts_lig_nr']."'";
    } elseif ($_POST['beigu_statuss'] == 'termins') {
      $atlikt_lidz = "'" . date('Y-m-d', strtotime($_POST['atlikt_lidz'])) . "'";
      $komentars = "'".$_POST['komentars1']."'";
    } elseif ($_POST['beigu_statuss'] == 'atteikums') {
      $komentars = "'".$_POST['komentars2']."'";
    }

    $log_data = array();

    if (!empty($stat_data)) {

      $log_data['lig_beigusies']['old'] = $stat_data;

      db_query("
        UPDATE `".DB_PREF."ligumi_beigusies`
        SET
          beigu_statuss = ".$beigu_statuss.",
          parslegts_lig_nr = ".$parslegts_lig_nr.",
          atlikt_lidz = ".$atlikt_lidz.",
          komentars = ".$komentars.",
          laboja_pardeveja_id = ".$_SESSION['user']['id'].",
          laboja_laiks = NOW()
        WHERE id = ".$stat_data['id']."
        LIMIT 1
      ");

    } else {

      db_query("
        INSERT INTO `".DB_PREF."ligumi_beigusies`
        SET
          liguma_id = ".$liguma_id.",
          beigu_statuss = ".$beigu_statuss.",
          parslegts_lig_nr = ".$parslegts_lig_nr.",
          atlikt_lidz = ".$atlikt_lidz.",
          komentars = ".$komentars.",
          laboja_pardeveja_id = ".$_SESSION['user']['id'].",
          laboja_laiks = NOW()
      ");

    }

    $new_stat_data = db_get_assoc(db_query("SELECT * FROM `".DB_PREF."ligumi_beigusies` WHERE `liguma_id` = " . $liguma_id));

    $log_data['lig_beigusies']['new'] = $new_stat_data;

    if (!empty($log_data['lig_beigusies']['old']))
      $log_data['lig_beigusies']['title'] = sprintf('Laboja līguma %s beigu statusu', $lig_data['ligumanr']);
    else
      $log_data['lig_beigusies']['title'] = sprintf('Pievienoja līguma %s beigu statusu', $lig_data['ligumanr']);

    log_add("laboja", $log_data);

    if (empty($stat_data) || $stat_data['beigu_statuss'] == 'termins') {

      if ($new_stat_data['beigu_statuss'] != 'termins') {

        db_query("
          UPDATE `".DB_PREF."ligumi_beigusies`
          SET
            pirmoreiz_apstradats = NOW()
          WHERE id = ".$new_stat_data['id']."
          LIMIT 1
        ");

      }

    }

    if (!empty($_POST['add_saist_pard']) && $new_stat_data['beigu_statuss'] == 'atteikums') {

      $log_data = array();

      $log_data['saist_pardeveji']['old_m'] = db_get_assoc_all(db_query("SELECT * FROM `".DB_PREF."ligumi_saistitie_pardeveji` WHERE `liguma_id` = " . $liguma_id));

      $add_saist_pard = unserialize(base64_decode($_POST['add_saist_pard']));

      foreach($add_saist_pard['pardeveja_id'] as $i => $pardeveja_id) {

        $piesaist_datums = $add_saist_pard['piesaist_datums'][$i];

        if (!empty($pardeveja_id) && !empty($piesaist_datums)) {

          $sql = "
            INSERT INTO `".DB_PREF."ligumi_saistitie_pardeveji` (
              liguma_id,
              pardeveja_id,
              piesaist_datums
            ) VALUES (
              ".$liguma_id.",
              ".(int)$pardeveja_id.",
              '".esc(date('Y-m-d', strtotime($piesaist_datums)))."'
            )
          ";

          db_query($sql);

        }

      }

      $log_data['saist_pardeveji']['new_m'] = db_get_assoc_all(db_query("SELECT * FROM `".DB_PREF."ligumi_saistitie_pardeveji` WHERE `liguma_id` = " . $liguma_id));
      $log_data['saist_pardeveji']['title'] = sprintf('Laboja līguma %s saistītos pārdevējus', $lig_data['ligumanr']);

      log_add("laboja", $log_data);

    }

    if ($stat_data['beigu_statuss'] != $new_stat_data['beigu_statuss']) {

      $rekini_data = db_get_assoc_all(db_query("SELECT * FROM `".DB_PREF."rekini` WHERE `liguma_id` = " . $liguma_id . " ORDER BY id ASC"));

      generate_auto_reminders($lig_data, $new_stat_data, $rekini_data);

    }

    ?>
    <script type="text/javascript">
    parent.document.location.reload();
    </script>
    <?
    die();

  }

}

?>

<div class="beigu_statuss_dialog">

  <div style="text-align: right; height: 20px;">

    <div>
      <?php if (check_access('ligumi-beigu-statuss-parslegtie') || (empty($stat_data) || $stat_data['beigu_statuss'] != 'parslegts')) { ?>
        <button onClick="return editoradd(this, <?= is_admin() ? 1 : 0 ?>, <?= is_limited() ? 1 : 0 ?>)">Labot</button>
      <?php } ?>
    </div>

    <div id="loader_placeholder"></div>

  </div>

  <form style="clear: both;" id="fullformplace" action="?c=ligumi&a=beigu_statuss&liguma_id=<?= $liguma_id ?>&without_nav=1" method="post" class="disable_act_panel">

    <?php if (!empty($add_saist_pard)) { ?>
      <input type="hidden" name="add_saist_pard" class="<?= ($can_limited_admin_edit) ? 'limited_admin_edit' : '' ?>" value="<?php echo esc(base64_encode(serialize($add_saist_pard))) ?>"  />
    <?php } ?>

    <div class="row" style="min-height: 3em;">
      <div class="col">Pirmoreiz apstrādāts:</div>
      <div class="col" style="width: 50%;">
        <strong>
          <?php if (!empty($stat_data['pirmoreiz_apstradats'])) { ?>
            <?php echo date('d.m.Y', strtotime($stat_data['pirmoreiz_apstradats'])) ?>
          <?php } else { ?>
            nav
          <?php } ?>
        </strong>

        <?php if (!empty($stat_data['auto_parslegts'])) { ?>
          (pārslēgts automātiski)
        <?php } ?>
      </div>
    </div>
	
	<?php if (!empty($stat_data['pardeveja_vards'])) { ?>
		<div class="row" style="min-height: 3em;">
		  <div class="col">Apstrādāja:</div>
		  <div class="col" style="width: 50%;">
			<strong>
				<?php echo $stat_data['pardeveja_vards']; ?>
			</strong>
		  </div>
		</div>
	<?php } ?>

    <div class="row">
      <div class="col radio">
        <input type="radio" class="<?= ($can_limited_admin_edit) ? 'limited_admin_edit' : '' ?>" name="beigu_statuss" value="parslegts" <?= ($stat_data['beigu_statuss'] == 'parslegts') ? 'checked="checked"' : '' ?> /> Pārslēgts
      </div>
      <div class="col <?= ($stat_data['beigu_statuss'] != 'parslegts') ? 'hidethistoo' : '' ?> <?= ($can_limited_admin_edit) ? 'limited_admin_edit' : '' ?>" style="width: 70%;">
        Jaunā līguma nr.:<br />
        <input class="<?= ($can_limited_admin_edit) ? 'limited_admin_edit' : '' ?>" style="width: 32%; margin-right: 10px;" type="text" name="parslegts_lig_nr" value="<?= $stat_data['parslegts_lig_nr'] ?>" />
        <?php if (!empty($stat_data['parslegts_lig_nr'])) { ?>
          <?php if (!empty($parslegts_lig_id)) { ?>
            <a target="_parent" href="?c=ligumi&a=labot&id=<?php echo $parslegts_lig_id ?>">Atvērt pārslēgto līgumu</a>
          <?php } else { ?>
            <span style="color: red;">Līguma ar šādu nr. nav atrasts</span>
          <?php } ?>
        <?php } ?>
      </div>
    </div>

    <div class="row">
      <div class="col radio">
        <input type="radio" name="beigu_statuss" value="termins" class="<?php echo ($can_pard_edit && $stat_data['beigu_statuss'] != 'parslegts') ? 'non_admin_edit' : '' ?> <?= ($can_limited_admin_edit) ? 'limited_admin_edit' : '' ?>" <?= ($stat_data['beigu_statuss'] == 'termins') ? 'checked="checked"' : '' ?> /> Termiņš
      </div>
      <div class="col <?= ($stat_data['beigu_statuss'] != 'termins') ? 'hidethistoo' : '' ?> <?= ($can_pard_edit) ? 'non_admin_edit' : '' ?> <?= ($can_limited_admin_edit) ? 'limited_admin_edit' : '' ?>" >
        Atlikt līdz:<br />
        <input style="width: 90%;" type="text" name="atlikt_lidz" class="<?= ($can_pard_edit) ? 'non_admin_edit' : '' ?> <?= ($can_limited_admin_edit) ? 'limited_admin_edit' : '' ?>" value="<?= isset($stat_data['atlikt_lidz']) ? date('d.m.Y', strtotime($stat_data['atlikt_lidz'])) : '' ?>" />
      </div>
      <div class="col <?= ($stat_data['beigu_statuss'] != 'termins') ? 'hidethistoo' : '' ?> <?= ($can_pard_edit) ? 'non_admin_edit' : '' ?> <?= ($can_limited_admin_edit) ? 'limited_admin_edit' : '' ?>" style="width: 50%;">
        Kāpēc:<br />
        <textarea name="komentars1" cols="30" rows="2" class="<?= ($can_pard_edit) ? 'non_admin_edit' : '' ?> <?= ($can_limited_admin_edit) ? 'limited_admin_edit' : '' ?>"><?= ($stat_data['beigu_statuss'] == 'termins') ? $stat_data['komentars'] : '' ?></textarea>
      </div>
    </div>

    <div class="row" style="margin-bottom: 2em;">
      <div class="col radio">
        <input type="radio" name="beigu_statuss" value="atteikums" class="<?php echo ($stat_data['beigu_statuss'] != 'parslegts') ? 'non_admin_edit' : '' ?> <?= ($can_limited_admin_edit) ? 'limited_admin_edit' : '' ?>" <?= ($stat_data['beigu_statuss'] == 'atteikums') ? 'checked="checked"' : '' ?> /> Atteikums
      </div>
      <div class="col <?= ($stat_data['beigu_statuss'] != 'atteikums') ? 'hidethistoo' : '' ?> <?= ($can_pard_edit) ? 'non_admin_edit' : '' ?> <?= ($can_limited_admin_edit) ? 'limited_admin_edit' : '' ?>" style="width: 75%;">
        Kāpēc:<br />
        <textarea name="komentars2" cols="60" rows="2" class="<?= ($can_pard_edit) ? 'non_admin_edit' : '' ?> <?= ($can_limited_admin_edit) ? 'limited_admin_edit' : '' ?>"><?= ($stat_data['beigu_statuss'] == 'atteikums') ? $stat_data['komentars'] : '' ?></textarea>
      </div>
    </div>

    <div style="text-align: center; clear: both;" class="hidethistoo <?= ($can_pard_edit) ? 'non_admin_edit' : '' ?> <?= ($can_limited_admin_edit) ? 'limited_admin_edit' : '' ?>">
      <button class="ui-state-default ui-corner-all <?= ($can_pard_edit) ? 'non_admin_edit' : '' ?> <?= ($can_limited_admin_edit) ? 'limited_admin_edit' : '' ?>" style="" onClick="return saveAll(this, 'beigu_statuss')">Saglabāt</button>
    </div>

  </form>

</div>

<script type="text/javascript">
editordisable();
$("input[name=atlikt_lidz]").datepicker({dateFormat:'dd.mm.yy'});
</script>