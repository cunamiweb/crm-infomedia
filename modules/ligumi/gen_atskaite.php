<?php
if(!check_access('ligumi-atskaites')) {
  die('Nav pieejas tiesību.');
}

if(!isset($_GET['id'])) {
  die('Nav izvēlēts līgums.');
}

$ligums = Ligums::model()->findByPk($_GET['id']);

if(!$ligums) {
  die('Neeksistējošs līgums.');
}

//  Language

$lang = 'lv';

require(DOCUMENT_ROOT . '/languages/' . $lang . '/init.php');
$strings = require(DOCUMENT_ROOT . '/languages/' . $lang . '/ligumi/atskaite.php');

/*
  Iegūstam datus no Google Adwords
*/

$data = getAdwordsReportData($_GET['id'], $_GET);

function removeZeroImpressions($elem) {
  return $elem['impressions'] && 1;
}

$data = array_filter($data, 'removeZeroImpressions');

if(empty($data)) {
  die('Nav atrasta neviena reklāmu kopa.');
}

        /*
$data = array(
  array('ad_group' => 'Zobārstniecība','impressions' => '26695','clicks' => '529','avg_pos' => '5,18'),
  array('ad_group' => 'Zobu protezēšana','impressions' => '5052','clicks' => '970','avg_pos' => '2,78'),
  array('ad_group' => 'Zobu higiēnists','impressions' => '553','clicks' => '44','avg_pos' => '1,65'),
  array('ad_group' => 'Стоматология','impressions' => '960','clicks' => '65','avg_pos' => '2,33'),
);      */

$autodownload = array();

require_once("Zend/Pdf.php");

//  Margins
$margin_x = 36;
$margin_y = 36;

$font_size = 10; // Default

$items_per_page = 35;

$pdf = new Zend_Pdf;

$font = Zend_Pdf_Font::fontWithPath('rek_templates/fonts/calibri.ttf');
$font_bold = Zend_Pdf_Font::fontWithPath('rek_templates/fonts/calibrib.ttf');

$grey = new Zend_Pdf_Color_GrayScale(0.5);

$page = new Zend_Pdf_Page(Zend_Pdf_Page::SIZE_A4);

//  Initial page coords
$x = $margin_x;
$y = $page->getHeight() - $margin_y;

//  Logo
$logo = Zend_Pdf_Image::imageWithPath(DOCUMENT_ROOT . '/css/logo_adwords_'.$lang.'.png');
//$logo = Zend_Pdf_Image::imageWithPath(DOCUMENT_ROOT . '/css/logo_google.png');
$page->drawImage($logo, 0, $page->getHeight() - 70.86, 290.55, $page->getHeight());

$y -= 70.86;

$row_height = 18;

//  Nosaukums
$page->setFont($font_bold, $font_size)->drawText($strings['nosaukums'] . ':', $x, $y, 'UTF-8');
$page->setFont($font, $font_size)->drawText($strings['google_adwords_konta_atskaite'], $x + 106, $y, 'UTF-8');

//  Klients
$page->setFont($font_bold, $font_size)->drawText($strings['klients'] . ':', $x, $y - $row_height, 'UTF-8');
$page->setFont($font, $font_size)->drawText($ligums->nosaukums, $x + 106, $y - $row_height, 'UTF-8');

//  Līguma numurs
$page->setFont($font_bold, $font_size)->drawText($strings['liguma_numurs'] . ':', $x, $y - $row_height*2, 'UTF-8');
$page->setFont($font, $font_size)->drawText($ligums->ligumanr, $x + 106, $y - $row_height*2, 'UTF-8');

//  Datumu diapozons
$text = $_GET['atskaite_no'] . ' - ' . $_GET['atskaite_lidz'];
$page->setFont($font_bold, $font_size)->drawText($strings['datumu_diapozons'] . ':', $x, $y - $row_height*3, 'UTF-8');
$page->setFont($font, $font_size)->drawText($text, $x + 106, $y - $row_height*3, 'UTF-8');

$y -= ($row_height * 4 + 20);

//  Table
$table_top_left = array('x' => $x, 'y' => $y);
$table_width = $page->getWidth() - $table_top_left['x'];
$col2 = 120;
$col3 = 120;
$col4 = 120;
$col1 = $table_width - ($col2 + $col3 + $col4) - $margin_x;

//  Draw table

$padding = 5;
$vt_padding = 11;
$table_row_height = 15;

//  Head

$page->drawLine($table_top_left['x'], $table_top_left['y'], $table_width, $table_top_left['y']);

$yrt = $y - $vt_padding;

//  Reklāmu kopa
$page->setFont($font_bold, $font_size)->drawText($strings['reklamu_kopa'],  $x + $padding,                         $yrt, 'UTF-8');

//  Seansi
$page->setFont($font_bold, $font_size)->drawText($strings['seansi'],        $x + $col1 + $padding,                 $yrt, 'UTF-8');

//  Klikšķi
$page->setFont($font_bold, $font_size)->drawText($strings['klikski'],       $x + $col1 + $col2 + $padding,         $yrt, 'UTF-8');

//  Vid.pozīcija
$page->setFont($font_bold, $font_size)->drawText($strings['vid_pozicija'],          $x + $col1 + $col2 + $col3 + $padding, $yrt, 'UTF-8');

$y -= $table_row_height;

$page->drawLine($table_top_left['x'], $y, $table_width, $y);


$totals = array(
  'impressions' => 0,
  'clicks' => 0,
  'avg_pos' => array(),
);

//  Drukājam datus
foreach(array_slice($data, 0, $items_per_page) as $row) {  // pirmajā lapā ietilpst tikai 15

  $totals['impressions'] += $row['impressions'];
  $totals['clicks'] += $row['clicks'];

  $yrt = $y - $vt_padding;

  //  Reklāmu kopa
  $page->setFont($font, $font_size)->drawText($row['ad_group'],  $x + $padding,                         $yrt, 'UTF-8');

  //  Seansi
  $width = stringWidth($row['impressions'], $font, $font_size);
  $page->setFont($font, $font_size)->drawText($row['impressions'],        $x + $col1 + $col2 - ($width + $padding),                 $yrt, 'UTF-8');

  //  Klikšķi
  $width = stringWidth($row['clicks'], $font, $font_size);
  $page->setFont($font, $font_size)->drawText($row['clicks'],       $x + $col1 + $col2 + $col3 - ($width + $padding),         $yrt, 'UTF-8');

  //  Vid.pozīcija
  $avg_pos = round($row['avg_pos'], 2);
  $totals['avg_pos'][] = $avg_pos;
  $width = stringWidth($avg_pos, $font, $font_size);
  $page->setFont($font, $font_size)->drawText($avg_pos,          $x + $col1 + $col2 + $col3 + $col4 - ($width + $padding), $yrt, 'UTF-8');

  $y -= $table_row_height;

  $page->drawLine($table_top_left['x'], $y, $table_width, $y);

}

//  Totals
if(count($data) <= $items_per_page) { // jo jārāda tikai pēdējā lapā
  $yrt = $y - $vt_padding;

  //  Reklāmu kopa
  $page->setFont($font_bold, $font_size)->drawText('--',  $x + $padding,                         $yrt, 'UTF-8');

  //  Seansi
  $width = stringWidth($totals['impressions'], $font_bold, $font_size);
  $page->setFont($font_bold, $font_size)->drawText($totals['impressions'],        $x + $col1 + $col2 - ($width + $padding),                 $yrt, 'UTF-8');

  //  Klikšķi
  $width = stringWidth($totals['clicks'], $font_bold, $font_size);
  $page->setFont($font_bold, $font_size)->drawText($totals['clicks'],       $x + $col1 + $col2 + $col3 - ($width + $padding),         $yrt, 'UTF-8');

  //  Vid.pozīcija
  $avg_pos = round(array_sum(array_filter($totals['avg_pos']))/count(array_filter($totals['avg_pos'])), 2);
  $width = stringWidth($avg_pos, $font_bold, $font_size);
  $page->setFont($font_bold, $font_size)->drawText($avg_pos,          $x + $col1 + $col2 + $col3 + $col4 - ($width + $padding), $yrt, 'UTF-8');

  $y -= $table_row_height;

  $page->drawLine($table_top_left['x'], $y, $table_width, $y);
}

//  Vertikālās līnijas (pirmā lapa)
$page->drawLine($table_top_left['x'], $table_top_left['y'], $table_top_left['x'], $y);
$page->drawLine($table_top_left['x'] + $col1, $table_top_left['y'], $table_top_left['x'] + $col1, $y);
$page->drawLine($table_top_left['x'] + $col1 + $col2, $table_top_left['y'], $table_top_left['x'] + $col1 + $col2, $y);
$page->drawLine($table_top_left['x'] + $col1 + $col2 + $col3, $table_top_left['y'], $table_top_left['x'] + $col1 + $col2 + $col3, $y);
$page->drawLine($table_top_left['x'] + $col1 + $col2 + $col3 + $col4, $table_top_left['y'], $table_top_left['x'] + $col1 + $col2 + $col3 + $col4, $y);


// Footer (pirmā lapa)
printFooter($page, $x);

$pdf->pages[] = $page;

// Pārējās lapas, ja ir
if(count($data) > $items_per_page) {
  $parts = array_chunk(array_slice($data, $items_per_page), $items_per_page);

  $count = count($parts);
  foreach($parts as $i => $rows) {
    $page = new Zend_Pdf_Page(Zend_Pdf_Page::SIZE_A4);

    //  Initial page coords
    $x = $margin_x;
    $y = $page->getHeight() - $margin_y;

    //  Table
    $table_top_left = array('x' => $x, 'y' => $y);
    $table_width = $page->getWidth() - $table_top_left['x'];
    $col2 = 120;
    $col3 = 120;
    $col4 = 120;
    $col1 = $table_width - ($col2 + $col3 + $col4) - $margin_x;

    //  Head

    $page->drawLine($table_top_left['x'], $table_top_left['y'], $table_width, $table_top_left['y']);

    $yrt = $y - $vt_padding;

    //  Reklāmu kopa
    $page->setFont($font_bold, $font_size)->drawText($strings['reklamu_kopa'],  $x + $padding,                         $yrt, 'UTF-8');

    //  Seansi
    $page->setFont($font_bold, $font_size)->drawText($strings['seansi'],        $x + $col1 + $padding,                 $yrt, 'UTF-8');

    //  Klikšķi
    $page->setFont($font_bold, $font_size)->drawText($strings['klikski'],       $x + $col1 + $col2 + $padding,         $yrt, 'UTF-8');

    //  Vid.pozīcija
    $page->setFont($font_bold, $font_size)->drawText($strings['vid_pozicija'],          $x + $col1 + $col2 + $col3 + $padding, $yrt, 'UTF-8');

    $y -= $table_row_height;

    $page->drawLine($table_top_left['x'], $y, $table_width, $y);


    //  Drukājam datus
    foreach($rows as $row) {  // pirmajā lapā ietilpst tikai 15

      $totals['impressions'] += $row['impressions'];
      $totals['clicks'] += $row['clicks'];

      $yrt = $y - $vt_padding;

      //  Reklāmu kopa
      $page->setFont($font, $font_size)->drawText($row['ad_group'],  $x + $padding,                         $yrt, 'UTF-8');

      //  Seansi
      $width = stringWidth($row['impressions'], $font, $font_size);
      $page->setFont($font, $font_size)->drawText($row['impressions'],        $x + $col1 + $col2 - ($width + $padding),                 $yrt, 'UTF-8');

      //  Klikšķi
      $width = stringWidth($row['clicks'], $font, $font_size);
      $page->setFont($font, $font_size)->drawText($row['clicks'],       $x + $col1 + $col2 + $col3 - ($width + $padding),         $yrt, 'UTF-8');

      //  Vid.pozīcija
      $avg_pos = round($row['avg_pos'], 2);
      $totals['avg_pos'][] = $avg_pos;
      $width = stringWidth($avg_pos, $font, $font_size);
      $page->setFont($font, $font_size)->drawText($avg_pos,          $x + $col1 + $col2 + $col3 + $col4 - ($width + $padding), $yrt, 'UTF-8');

      $y -= $table_row_height;

      $page->drawLine($table_top_left['x'], $y, $table_width, $y);

    }

    if($count == ($i + 1)) { // pēdējā lapa
      $yrt = $y - $vt_padding;

      //  Reklāmu kopa
      $page->setFont($font_bold, $font_size)->drawText('--',  $x + $padding,                         $yrt, 'UTF-8');

      //  Seansi
      $width = stringWidth($totals['impressions'], $font_bold, $font_size);
      $page->setFont($font_bold, $font_size)->drawText($totals['impressions'],        $x + $col1 + $col2 - ($width + $padding),                 $yrt, 'UTF-8');

      //  Klikšķi
      $width = stringWidth($totals['clicks'], $font_bold, $font_size);
      $page->setFont($font_bold, $font_size)->drawText($totals['clicks'],       $x + $col1 + $col2 + $col3 - ($width + $padding),         $yrt, 'UTF-8');

      //  Vid.pozīcija
      $avg_pos = round(array_sum(array_filter($totals['avg_pos']))/count(array_filter($totals['avg_pos'])), 2);
      $width = stringWidth($avg_pos, $font_bold, $font_size);
      $page->setFont($font_bold, $font_size)->drawText($avg_pos,          $x + $col1 + $col2 + $col3 + $col4 - ($width + $padding), $yrt, 'UTF-8');

      $y -= $table_row_height;

      $page->drawLine($table_top_left['x'], $y, $table_width, $y);
    }

    //  Vertikālās līnijas (pirmā lapa)
    $page->drawLine($table_top_left['x'], $table_top_left['y'], $table_top_left['x'], $y);
    $page->drawLine($table_top_left['x'] + $col1, $table_top_left['y'], $table_top_left['x'] + $col1, $y);
    $page->drawLine($table_top_left['x'] + $col1 + $col2, $table_top_left['y'], $table_top_left['x'] + $col1 + $col2, $y);
    $page->drawLine($table_top_left['x'] + $col1 + $col2 + $col3, $table_top_left['y'], $table_top_left['x'] + $col1 + $col2 + $col3, $y);
    $page->drawLine($table_top_left['x'] + $col1 + $col2 + $col3 + $col4, $table_top_left['y'], $table_top_left['x'] + $col1 + $col2 + $col3 + $col4, $y);


    // Footer
    printFooter($page, $x);

    $pdf->pages[] = $page;

  }
}

//  Glabājam

$pdf_output = $pdf->render();

$name =  $strings['file_name'] . ' ' .translit($ligums->nosaukums) . ' (' . str_replace('.', '', $_GET['atskaite_no'] . '-' . $_GET['atskaite_lidz']) . ').pdf';
$name = prepare_filename($name);

db_query("
  INSERT INTO `".DB_PREF."faili` SET
    `objekta_id` = ".$ligums->id.",
    `objekta_tips` = 'adwords_atskaite',
    `pardeveja_id` = ".$ligums->pardeveja_id.",
    `fails` = '".esc($name)."',
    `izmers` = ".strlen($pdf_output)."
");


if(!save_file($pdf_output, "faili/" . db_last_id())) {

  db_query("DELETE FROM `".DB_PREF."faili` WHERE `id` = ".db_last_id());

} else {
  $autodownload[] = db_last_id();
}

$_SESSION['autodownload_file_ids'] = $autodownload;

header("Location: ?c=ligumi&a=labot&id=" . getGet('id') . '&subtab=4');

die();

function stringWidth($string, $font, $fontSize)
{
     $drawingString = iconv('UTF-8', 'UTF-16BE//IGNORE', $string);
     $characters = array();
     for ($i = 0; $i < strlen($drawingString); $i++) {
         $characters[] = (ord($drawingString[$i++]) << 8 ) | ord($drawingString[$i]);
     }
     $glyphs = $font->glyphNumbersForCharacters($characters);
     $widths = $font->widthsForGlyphs($glyphs);
     $stringWidth = (array_sum($widths) / $font->getUnitsPerEm()) * $fontSize;
     return $stringWidth;
}

function printFooter(&$page, $x) {

  global $margin_x, $margin_y, $strings, $font, $font_size;

  $page->drawLine($x - 4, $margin_y + 34, $page->getWidth() - $margin_x, $margin_y + 34);

  $page->setFont($font, $font_size)->drawText(REKV_NOSAUK, $x, $margin_y + 18, 'UTF-8');
  $page->setFont($font, $font_size)->drawText(REKV_FIZ_ADR, $x, $margin_y, 'UTF-8');

  $page->setFont($font, $font_size)->drawText($strings['talrunis'].':', $x + 240, $margin_y + 18, 'UTF-8');
  $page->setFont($font, $font_size)->drawText(LIG_REK_TALR, $x + 284, $margin_y + 18, 'UTF-8');

  $page->setFont($font, $font_size)->drawText($strings['fakss'].':', $x + 251, $margin_y, 'UTF-8');
  $page->setFont($font, $font_size)->drawText(LIG_REK_FAX, $x + 284, $margin_y, 'UTF-8');

  $page->setFont($font, $font_size)->drawText($strings['epasts'].':', $x + 246 + 155, $margin_y + 18, 'UTF-8');
  $page->setFont($font, $font_size)->drawText(LIG_REK_EPASTS, $x + 284 + 155, $margin_y + 18, 'UTF-8');

  $page->setFont($font, $font_size)->drawText($strings['web'].':', $x + 255 + 155, $margin_y, 'UTF-8');
  $page->setFont($font, $font_size)->drawText(LIG_REK_WWW, $x + 284 + 155, $margin_y, 'UTF-8');
}

?>