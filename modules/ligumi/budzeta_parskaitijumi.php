<?

if (!check_access('ligumi-labot')) {
  die();
}

if (empty($_GET['rek_id'])) {
  die();
}

$rek_id = (int) $_GET['rek_id'];

$rekins = Rekins::model()->findByPk($rek_id);

if (!$rekins) {
  die();
}

$ligums = $rekins->getLigums();

if(isset($_GET['task']) && $_GET['task'] = 'change_apm_mode') {
  // apmaksātā budžeta rēķināšanas režīma maiņa

  $rekins->apm_budzets = $_GET['sum'] != '-1' ? str_replace(',', '.', $_GET['sum']) : null;
  $rekins->save();

  header('Location: ?c=ligumi&a=labot&a=labot&id='.$ligums->id.'&subtab=2');
  die;
}

function recalculate($rekins) {
  $sum= 0;

  $parskaitijumi = $rekins->getBudzetaParskaitijumi();

  if(!empty($parskaitijumi)) {
    foreach($parskaitijumi as $parskaitijums) {
      $sum += $parskaitijums->summa;
    }

    $rekins->parsk_budzets = $sum;
  } else {
    $rekins->parsk_budzets = null;
  }

  $rekins->save();
  return $rekins;
}

if (!empty($_GET['del_b_parsk_id'])) {

  $old_rek_apm_data = db_get_assoc(db_query("
    SELECT bp.*
    FROM `".DB_PREF."rekini_budzeta_parskaitijumi` bp
    WHERE
      bp.id = ".(int)$_GET['del_b_parsk_id']."
  "));

  if (empty($old_rek_apm_data)) {
    die();
  }

  $log_data = array();

  $result = db_query("
    DELETE FROM `".DB_PREF."rekini_budzeta_parskaitijumi`
    WHERE
      `rekina_id` = ".$rekins->id." AND
      `id` = ".(int)$_GET['del_b_parsk_id']."
    LIMIT 1
  ");

  if ($result) {

    $log_data['budzeta_parskaitijums']['old'] = $old_rek_apm_data;
    $log_data['budzeta_parskaitijums']['title'] = sprintf('Dzēsa līguma %s rēķina %s budžeta pārskaitījumu', $ligums->ligumanr, $rekins->rek_nr);
    log_add("laboja", $log_data);

    $rekins = recalculate($rekins);

  }

  $_SESSION['b_parsk_deleted'] = true;
  header('Location: ?c=ligumi&a=budzeta_parskaitijumi&rek_id='.$rek_id.'&without_nav=1');
  die();

}

if(isset($_POST['Parskaitijums'])) {
  //  Pārskaitījumu labošana


  foreach($_POST['Parskaitijums'] as $id => $data) {

    $parskaitijums = BudzetaParskaitijums::model()->findByPk($id);

    $summa = $data['summa'] ? $data['summa'] : null;

    if(bccomp($summa, $parskaitijums->summa, 2)) {
      $parskaitijums->summa = $summa;
      $parskaitijums->piev_pardeveja_id = $_SESSION['user']['id'];
      $parskaitijums->pievienots = date('Y-m-d H:i:s');
    }

    $parskaitijums->apm_datums = $data['apm_datums'] ? date('Y-m-d H:i:s', strtotime($data['apm_datums'])) : null;

    if($parskaitijums->validate('update')) {
      $parskaitijums->save();

      $rekins = recalculate($rekins);
    }
  }
} else {

  if (!empty($_POST)) {
    //  Jauna pārskaitījuma pievienošana

    $_POST = trim_array($_POST);

    $errors = array();

    $_POST['summa'] = str_replace(',', '.', $_POST['summa']);
    $datums = !empty($_POST['datums']) ? date('Y-m-d', strtotime($_POST['datums'])) : '';

    if (!is_numeric($_POST['summa'])) {
      $errors[] = 'Nav norādīta pareiza summa!';
    }

    if (empty($datums)) {
      $errors[] = 'Nav norādīts datums!';
    } elseif ($datums < $rekins->izr_datums) {
      $errors[] = 'Pārskaitījuma datums nevar būt pirms rēķina izrakstīšanas datuma!';
    }

    /*if(empty($_POST['valuta'])) {
      $errors[] = 'Nav norādīta valūta!';
    } elseif(!in_array($_POST['valuta'], array('LVL', 'EUR'))) {
      $errors[] = 'Norādīta nekorekta valūta!';
    }  */

    if (empty($errors)) {

      $old_rekini_data = db_get_assoc_all(db_query("SELECT * FROM `".DB_PREF."rekini` WHERE `liguma_id` = " . $rekins->liguma_id . " ORDER BY id ASC"));

      $log_data = array();

      $parskaitijums = new BudzetaParskaitijums;

      /*if($_POST['valuta'] == 'LVL') {        */

        $parskaitijums->pievienots = date('Y-m-d H:i:s');
        $parskaitijums->piev_pardeveja_id = $_SESSION['user']['id'];
        $parskaitijums->apm_datums = $datums;
        $parskaitijums->summa = $_POST['summa'];

    /*  } elseif($_POST['valuta'] == 'EUR') {

        $parskaitijums->pievienots_eur = date('Y-m-d H:i:s');
        $parskaitijums->piev_pardeveja_id_eur = $_SESSION['user']['id'];
        $parskaitijums->apm_datums_eur = $datums;
        $parskaitijums->summa_eur = $_POST['summa'];
      }    */

      $parskaitijums->rekina_id = $rek_id;

      if ($parskaitijums->save()) {

        $log_data['budzeta_parskaitijums']['new'] = $parskaitijums->asArray();
        $log_data['budzeta_parskaitijums']['title'] = sprintf('Pievienoja līguma %s rēķinam %s jaunu budžeta pārskaitījumu', $ligums->ligumanr, $rekins->rek_nr);
        log_add("laboja", $log_data);

        $rekins = recalculate($rekins);
      }

      $_SESSION['b_parsk_added'] = true;
      header('Location: ?c=ligumi&a=budzeta_parskaitijumi&rek_id='.$rek_id.'&without_nav=1');
      die();
    }
  }
}
?>

<? if (!empty($errors)) { ?>
  <ul style="margin: 10px 0 5px 0;" class="errors">
    <? foreach($errors as $error) { ?>
      <li><?= $error ?></li>
    <? } ?>
  </ul>
<? } ?>

<? if (!empty($_SESSION['b_parsk_deleted'])) { ?>
  <p style="color: green; margin: 10px 0 5px 0;">Budžeta pārskaitījums izdzēsts</p>
  <? unset($_SESSION['b_parsk_deleted']) ?>
<? } ?>

<? if (!empty($_SESSION['b_parsk_added'])) { ?>
  <p style="color: green; margin: 10px 0 5px 0;">Budžeta pārskaitījums pievienots</p>
  <? unset($_SESSION['b_parsk_added']) ?>
<? } ?>

<button id="labotdzestbutton" onClick="return editoradd(this, 1, 0)">Labot</button>
<form action="" id="fullformplace" method="post">
<table id="rek_apmaksas" class="data" style="width: 99%;">

  <thead>

    <tr class="last">
      <th>Summa</th>
      <th>Pārsk.datums</th>
      <th>Pievienoja</th>
      <th>Pievienots</th>
      <th></th>
    </tr>

  </thead>

  <tbody class="main">

    <?php foreach((array)$rekins->getBudzetaParskaitijumi() as $parsk) { ?>

      <tr>

        <td>
          <span class="inner_row"><input type="text" class="canedit" name="Parskaitijums[<?php echo $parsk->id;?>][summa]" value="<?php echo $parsk->summa ? format_currency($parsk->summa) : ''; ?>"></span>
        </td>

        <td>
          <span class="inner_row"><input type="text"  class="datepick canedit" name="Parskaitijums[<?php echo $parsk->id;?>][apm_datums]" value="<?php echo $parsk->apm_datums ? date('d.m.Y', strtotime($parsk->apm_datums)) : '';?>"></span>
        </td>

        <td>
          <span class="inner_row"><?php echo $parsk->piev_pardeveja_id ? $_vars['sys_pardeveji'][$parsk->piev_pardeveja_id] : '';?></span>
        </td>

        <td>
          <span class="inner_row"><?php echo $parsk->pievienots ? date('d.m.Y', strtotime($parsk->pievienots)) : '';?></span>
        </td>

        <td class="last" style="text-align: center;">
          <a href="?c=ligumi&a=budzeta_parskaitijumi&rek_id=<?= $rek_id ?>&del_b_parsk_id=<?php echo $parsk->id ?>&without_nav=1" onclick="return confirm('Vai tiešām dzēst pārskaitījumu?');" title="Dzēst"><img src="css/del.png" alt="Dzēst" /></a>
        </td>
      </tr>

    <? } ?>

  </tbody>

</table>

 <div class="hidethistoo">
    <input type="submit" onClick="return saveAll(this, 'budzeta_parskaitijums')" class="submit show_in_act_panel" value="Saglabāt">
    <input type="button" onclick="return cancelSaves();" class="show_in_act_panel" value="Atcelt">
  </div>
</form>

<form id="rek_apm_form" action="" method="post">

  Summa: <input type="text" name="summa" value="<?= isset($_POST['summa']) ? $_POST['summa'] : '' ?>" style="width: 80px; margin-right: 10px;" />
  Apmaksas Datums: <input id="apmaksas_datums" type="text" name="datums" value="<?= isset($_POST['datums']) ? $_POST['datums'] : date('d.m.Y') ?>" style="width: 90px; margin-right: 10px;" />

  <button class="ui-state-default ui-corner-all" style="">Pievienot pārskaitījumu</button>

</form>

<script type="text/javascript">
$(function(){
  editordisable();

  //stripeTable($('#rek_apmaksas'));

  $("#apmaksas_datums, .datepick").datepicker({dateFormat:'dd.mm.yy'});
});
</script>