<?php
if ( ! check_access( 'ligumi-saraksts' ) ) {
	die( 'Jums nav pieejas tiesību šai sadaļai.' );
}

require( 'list_process.php' );

log_add( "atvera", "Atvēra līguma kopsavilkumu" );

$kontu_tipi = array(
	'tiessaite' => 'Google',
	'infomedia' => 'Infomedia',
	'yandex'    => 'Yandex',
	'citi'      => 'Citi'
);

$active_page = ! empty( $_GET['page'] ) ? (int) $_GET['page'] : 1;

if ( getGet( 'skatijums' ) ) {

	db_query( "UPDATE `" . DB_PREF . "pardeveji` SET lig_kopsavilkums = '" . esc( getGet( 'skatijums' ) ) . "' WHERE id = " . $_SESSION['user']['id'] . " LIMIT 1" );

	$_SESSION['user']['lig_kopsavilkums'] = getGet( 'skatijums' );

}

$group_view = false;

if ( ! isset( $_GET['fligumu_id'] ) ) {
	$group_view = ( $_SESSION['user']['lig_kopsavilkums'] == 'grupets' ) ? true : false;
}

if ( ! isset( $_GET['flig_statuss'] ) && ! isset( $_GET['fsearch'] ) ) {
	$_GET['flig_statuss'] = array( '1', '3', '4', '5', '6', '6_termins', '6_parslegts', '6_atteikums', '6_atteikums_p' );
}

if ( ! isset( $_GET['show_in_page'] ) ) {
	$show_in_page = 30;
} elseif ( $_GET['show_in_page'] > 0 ) {
	$show_in_page = (int) $_GET['show_in_page'];
} else {
	$show_in_page = '';
}

$order_bys = array(
	'nosaukums'         => 'g.nosaukums',
	'liguma_nr'         => 'g.ligumanr',
	'indekss'           => 'g.faktaddr_index',
	'darbinieks'        => 'p.vards',
	'lig_statuss'       => '',
	'kopapm_statuss'    => '',
	'google_statuss'    => 'g.google_statuss',
	'lig_dat'           => 'g.ligumadat',
	'lig_san_dat'       => 'g.sanemsanas_datums',
	'rekl_dat_no'       => 'g.reklama_no',
	'rekl_dat_lidz'     => 'g.reklama_lidz',
	'sanems_forma'      => 'g.sanemsanas_forma',
	'pakalpojums'       => '',
	'infomedia_statuss' => 'g.infomedia_statuss',
	'google_konts'      => '',
	'google_atslegv'    => '',
	'lig_summa'         => 'liguma_summa_bez_pvn',
	'rek_izr_dat'       => 'last_rek_izr_datums',
	'akt_atgad'         => 'aktivi_atgadinajumi',
	'rentabil'          => 'rentabil',
	'rentabil2'         => 'last_rentabil2',
	#'rentabil2' => 'total_rentabil_db',
	'rekinu_skaits'     => 'rekinu_skaits'
);

$order     = '';
$direction = 'asc';

if ( ! empty( $_GET['order'] ) && isset( $order_bys[ $_GET['order'] ] ) ) {
	$order = $order_bys[ $_GET['order'] ];
}

if ( ! empty( $_GET['direction'] ) && in_array( strtolower( $_GET['direction'] ), array( 'asc', 'desc' ) ) ) {
	$direction = strtolower( $_GET['direction'] );
	if ( $order == 'last_rentabil14' ) {
		if ( $direction == 'asc' ) {
			$direction = 'desc';
		} else {
			$direction = 'asc';
		}
	}
}


$query = db_query( "
  SELECT
    *,
    IF (izr_datums IS NOT NULL,
      IF(anulets = 1,
        6, -- anulets
        IF (nodots_piedz = 1,
          7, -- nodots piedzinai
          IF(apm_termins IS NOT NULL,
            IF(kopsumma <= apm_summa,
              2, -- apmaksats
              IF (apm_summa IS NULL OR apm_summa = 0,
                IF(CURDATE() > apm_termins,
                  3, -- parads
                  4  -- nav termins
                ),
                IF(CURDATE() > apm_termins,
                  3, -- parads
                  5  -- daleji apmaksats
                )
              )
            ),
            0
          )
        )
      ),
      0
    ) AS apm_statuss,
    (
        SELECT datums
        FROM `" . DB_PREF . "rekini_maksajumi`
        WHERE rekina_id = r.id
        ORDER BY datums DESC
        LIMIT 1
    ) as pedejais_apm_datums,
    (
        SELECT apm_datums
        FROM `" . DB_PREF . "rekini_budzeta_parskaitijumi`
        WHERE rekina_id = r.id
        ORDER BY apm_datums DESC
        LIMIT 1
      ) as bp_apm_datums
  FROM `" . DB_PREF . "rekini` r
  ORDER BY izr_datums DESC
" );

$rekini = array();

while ( $row = db_get_assoc( $query ) ) {
	$rekini[ $row['liguma_id'] ][] = $row;
}

$query = db_query( "
  SELECT a.*
  FROM `" . DB_PREF . "atgadinajumi` a
  WHERE
    statuss = 1 AND
    datums <= CURDATE()
  ORDER BY
    a.datums DESC,
    a.id DESC
" );

$atgadinajumi = array();

while ( $row = db_get_assoc( $query ) ) {
	$atgadinajumi[ $row['liguma_id'] ][] = $row;
}

$query = db_query( "
  SELECT
    s.liguma_id,
    p.*
  FROM `" . DB_PREF . "ligumi_saistitie_pardeveji` s
  LEFT JOIN `" . DB_PREF . "pardeveji` p ON (p.id = s.pardeveja_id)
" );

$saist_pard = array();

while ( $row = db_get_assoc( $query ) ) {
	$saist_pard[ $row['liguma_id'] ][] = $row;
}

$query = db_query( "
  SELECT
    p.*,
    v.nosaukums,
    v.parent_id
  FROM `" . DB_PREF . "pakalpojumi` p
  LEFT JOIN `" . DB_PREF . "pakalpojumi_veidi` v ON (p.pakalpojuma_veida_id = v.id)
  ORDER BY v.seciba ASC
" );

$pakalpojumi = array();

while ( $row = db_get_assoc( $query ) ) {
	$pakalpojumi[ $row['liguma_id'] ][ $row['parent_id'] ][] = $row;
}


//  MPK pakalpojumi
$mpk_pakalpojumi = array();

// Search
$result = db_query( '
  SELECT g.id, k.site, COUNT(k.id) as count
  FROM ' . DB_PREF . 'mpk_keywords k
  LEFT JOIN ' . DB_PREF . 'ligumi g ON g.id = k.ligums_id
  WHERE
    g.mpk = 1
  GROUP BY k.ligums_id, k.site
' );
while ( $row = db_get_assoc( $result ) ) {
	$mpk_pakalpojumi[ $row['id'] ][ $row['site'] ]['search'] = $row['count'];
}

// Categories
$result = db_query( '
  SELECT g.id, k.site, COUNT(k.id) as count
  FROM ' . DB_PREF . 'mpk_categories k
  LEFT JOIN ' . DB_PREF . 'ligumi g ON g.id = k.ligums_id
  WHERE
    g.mpk = 1
  GROUP BY k.ligums_id, k.site
' );
while ( $row = db_get_assoc( $result ) ) {
	$mpk_pakalpojumi[ $row['id'] ][ $row['site'] ]['category'] = $row['count'];
}

// Displays
$result = db_query( '
  SELECT g.id, k.site, COUNT(k.id) as count
  FROM ' . DB_PREF . 'mpk_displays k
  LEFT JOIN ' . DB_PREF . 'ligumi g ON g.id = k.ligums_id
  WHERE
    g.mpk = 1 AND k.enabled = 1
  GROUP BY k.ligums_id, k.site
' );
while ( $row = db_get_assoc( $result ) ) {
	$mpk_pakalpojumi[ $row['id'] ][ $row['site'] ]['display'] = $row['count'];
}

// Analytics
$result = db_query( '
  SELECT g.id, k.site, COUNT(k.id) as count
  FROM ' . DB_PREF . 'mpk_analytics k
  LEFT JOIN ' . DB_PREF . 'ligumi g ON g.id = k.ligums_id
  WHERE
    g.mpk = 1 AND k.enabled = 1
  GROUP BY k.ligums_id, k.site
' );
while ( $row = db_get_assoc( $result ) ) {
	$mpk_pakalpojumi[ $row['id'] ][ $row['site'] ]['analytics'] = $row['count'];
}

// Conversions
$result = db_query( '
  SELECT g.id, k.site, COUNT(k.id) as count
  FROM ' . DB_PREF . 'mpk_conversions k
  LEFT JOIN ' . DB_PREF . 'ligumi g ON g.id = k.ligums_id
  WHERE
    g.mpk = 1 AND k.enabled = 1
  GROUP BY k.ligums_id, k.site
' );
while ( $row = db_get_assoc( $result ) ) {
	$mpk_pakalpojumi[ $row['id'] ][ $row['site'] ]['conversions'] = $row['count'];
}

// Conversions
$result = db_query( '
  SELECT g.id as id, k.id as kid
  FROM ' . DB_PREF . 'mpk_others k
  LEFT JOIN ' . DB_PREF . 'ligumi g ON g.id = k.ligums_id
  WHERE
    g.mpk = 1 AND k.description IS NOT NULL && k.description != ""
  GROUP BY k.ligums_id
' );
while ( $row = db_get_assoc( $result ) ) {
	$mpk_pakalpojumi[ $row['id'] ]['other'] = 1;
}

$visi_pielikumi = array();

foreach ( array( 'tiessaite', 'infomedia', 'yandex', 'citi' ) as $pielikuma_tips ) {
	$query = db_query( "
    SELECT p.*
    FROM `" . DB_PREF . "pielikumi_" . $pielikuma_tips . "` p
    LEFT JOIN `" . DB_PREF . "ligumi` g ON g.id = p.liguma_id
    WHERE g.mpk = 0
		ORDER BY atslegvards
  " );

	while ( $row = db_get_assoc( $query ) ) {
		$visi_pielikumi[ $row['liguma_id'] ][ $pielikuma_tips ][] = $row;
	}
}

//  MPK Atslegvardi

$visi_mpk_atslegvardi = array();
$result               = db_query( '
  SELECT k.*
  FROM ' . DB_PREF . 'mpk_keywords k
  LEFT JOIN ' . DB_PREF . 'ligumi g ON g.id = k.ligums_id
  WHERE g.mpk = 1
' );
while ( $row = db_get_assoc( $result ) ) {
	$visi_mpk_atslegvardi[ $row['ligums_id'] ][] = $row;
}

//  MPK Tēmas
$visi_mpk_temas = array();
$result         = db_query( '
  SELECT k.*
  FROM ' . DB_PREF . 'mpk_categories k
  LEFT JOIN ' . DB_PREF . 'ligumi g ON g.id = k.ligums_id
  WHERE g.mpk = 1
' );
while ( $row = db_get_assoc( $result ) ) {
	$visi_mpk_temas[ $row['ligums_id'] ][] = $row;
}

//  MPK Displeji
$visi_mpk_displeji = array();
$result            = db_query( '
  SELECT k.*
  FROM ' . DB_PREF . 'mpk_displays k
  LEFT JOIN ' . DB_PREF . 'ligumi g ON g.id = k.ligums_id
  WHERE g.mpk = 1 AND k.enabled = 1
' );
while ( $row = db_get_assoc( $result ) ) {
	$visi_mpk_displeji[ $row['ligums_id'] ][] = $row;
}

//  MPK konversijas
$visi_mpk_konversijas = array();
$result               = db_query( '
  SELECT k.*
  FROM ' . DB_PREF . 'mpk_conversions k
  LEFT JOIN ' . DB_PREF . 'ligumi g ON g.id = k.ligums_id
  WHERE g.mpk = 1 AND k.enabled = 1
' );
while ( $row = db_get_assoc( $result ) ) {
	$visi_mpk_konversijas[ $row['ligums_id'] ][] = $row;
}

//  MPK analitika
$visi_mpk_analitika = array();
$result             = db_query( '
  SELECT k.*
  FROM ' . DB_PREF . 'mpk_analytics k
  LEFT JOIN ' . DB_PREF . 'ligumi g ON g.id = k.ligums_id
  WHERE g.mpk = 1 AND k.enabled = 1
' );
while ( $row = db_get_assoc( $result ) ) {
	$visi_mpk_analitika[ $row['ligums_id'] ][] = $row;
}

//  MPK other
$visi_mpk_other = array();
$result         = db_query( '
  SELECT k.*
  FROM ' . DB_PREF . 'mpk_others k
  LEFT JOIN ' . DB_PREF . 'ligumi g ON g.id = k.ligums_id
  WHERE g.mpk = 1 AND k.description != "" AND k.description IS NOT NULL
' );
while ( $row = db_get_assoc( $result ) ) {
	$visi_mpk_other[ $row['ligums_id'] ][] = $row;
}

$query = db_query( "
  SELECT *
  FROM `" . DB_PREF . "pielikumi_kontu_linki`
" );

$kontu_linki = array();

while ( $row = db_get_assoc( $query ) ) {
	$kontu_linki[ $row['liguma_id'] ][ $row['tips'] ][ $row['konts'] ] = $row;
}

$where  = array();
$having = array();


if ( isset( $_GET['fligumu_id'] ) ) {
	if ( getGet( 'fligumu_id' ) ) {
		$where[] = 'g.id IN (' . getGet( 'fligumu_id' ) . ')';
	} else {
		$where[] = 'g.id IS NULL';
	}
}


if ( getGet( 'ftikai_ipasie' ) ) {
	$having[] = 'pardevejam_izcelts = 1';
}

if ( getGet( 'fsearch' ) !== '' ) {
	$where[] = '
  (
    g.nosaukums LIKE "%' . getGet( 'fsearch' ) . '%" OR
    g.ligumanr LIKE "%' . getGet( 'fsearch' ) . '%" OR
    g.regnr LIKE "%' . getGet( 'fsearch' ) . '%" OR
    k.vards LIKE "%' . getGet( 'fsearch' ) . '%" OR
    k.epasts LIKE "%' . getGet( 'fsearch' ) . '%" OR
    g.faktaddr_street LIKE "%' . getGet( 'fsearch' ) . '%" OR
    g.juridaddr_street LIKE "%' . getGet( 'fsearch' ) . '%"
  )
';
}

if ( getGet( 'flig_aktivs_datums' ) !== '' ) {
	$where[] = '
    (
      g.reklama_no <= "' . to_db_date( getGet( 'flig_aktivs_datums' ) ) . '" AND
      g.reklama_lidz >= "' . to_db_date( getGet( 'flig_aktivs_datums' ) ) . '"
    )
  ';
}

if ( getGet( 'fnosaukums' ) !== '' ) {
	$where[] = 'g.nosaukums LIKE "%' . getGet( 'fnosaukums' ) . '%"';
}
if ( getGet( 'fliguma_nr' ) !== '' ) {
	$where[] = 'g.ligumanr LIKE "%' . getGet( 'fliguma_nr' ) . '%"';
}
if ( getGet( 'ffaktaddr_index' ) !== '' ) {
	$where[] = 'g.faktaddr_index LIKE "%' . getGet( 'ffaktaddr_index' ) . '%"';
}

//  Rekinu skaits filter
if ( getGet( 'frekinu_skaits' ) !== '' ) {
	$first_char = substr( getGet( 'frekinu_skaits' ), 0, 1 );

	if ( $first_char == '<' || $first_char == '>' ) {
		$operator     = $first_char;
		$filter_value = substr( getGet( 'frekinu_skaits' ), 1 );
	} else {
		$operator     = '=';
		$filter_value = getGet( 'frekinu_skaits' );
	}

	$having[] = 'rekinu_skaits ' . $operator . ' ' . $filter_value;

	unset( $first_char );
	unset( $operator );
	unset( $filter_value );
}

//if (pt_multiselect_is_active('fpardevejs')) $where[] = 'p.id IN (' . implode(',', array_map('intval', getGet('fpardevejs'))) . ')';
if ( pt_multiselect_is_active( 'fpardevejs' ) ) {
	$fpardevejs_ids = array();
	$type_cond      = array();
	foreach ( getGet( 'fpardevejs' ) as $fpardevejs ) {
		if ( is_numeric( $fpardevejs ) ) {
			$fpardevejs_ids[] = $fpardevejs;
		} else {
			if ( $fpardevejs == 'darbinieki' ) {
				$type_cond[] = 'p.tips = "sales"';
			} elseif ( $fpardevejs == 'vaditaji' ) {
				$type_cond[] = '(p.tips = "admin" AND p.vaditajs = 1)';
			} elseif ( $fpardevejs == 'partneri' ) {
				$type_cond[] = 'p.tips = "partner"';
			}
		}
	}

	if ( empty( $fpardevejs_ids ) ) {
		$where[] = implode( ' OR ', $type_cond );
	} else {
		$where[] = 'p.id IN (' . implode( ',', $fpardevejs_ids ) . ')';
	}
}

if ( pt_multiselect_is_active( 'fgoogle_statuss' ) ) {
	$where[] = 'g.google_statuss IN (' . implode( ',', array_map( 'intval', getGet( 'fgoogle_statuss' ) ) ) . ')';
}

if ( getGet( 'flig_no' ) !== '' ) {
	$where[] = 'g.ligumadat >= "' . to_db_date( getGet( 'flig_no' ) ) . '"';
}
if ( getGet( 'flig_lidz' ) !== '' ) {
	$where[] = 'g.ligumadat <= "' . to_db_date( getGet( 'flig_lidz' ) ) . '"';
}

if ( getGet( 'flig_sanemts_no' ) !== '' ) {
	$where[] = 'g.sanemsanas_datums >= "' . to_db_date( getGet( 'flig_sanemts_no' ) ) . '"';
}
if ( getGet( 'flig_sanemts_lidz' ) !== '' ) {
	$where[] = 'g.sanemsanas_datums <= "' . to_db_date( getGet( 'flig_sanemts_lidz' ) ) . '"';
}

if ( getGet( 'flig_sanemts_menesis' ) !== '' ) {
	list( $flig_s_m, $flig_s_y ) = explode( '.', getGet( 'flig_sanemts_menesis' ) );
	$where[] = 'YEAR(g.sanemsanas_datums) = ' . (int) $flig_s_y . ' AND MONTH(g.sanemsanas_datums) = ' . (int) $flig_s_m;
}

if ( getGet( 'frekl_sak_no' ) !== '' ) {
	$where[] = 'g.reklama_no >= "' . to_db_date( getGet( 'frekl_sak_no' ) ) . '"';
}
if ( getGet( 'frekl_sak_lidz' ) !== '' ) {
	$where[] = 'g.reklama_no <= "' . to_db_date( getGet( 'frekl_sak_lidz' ) ) . '"';
}

if ( getGet( 'frekl_sak_menesis' ) !== '' ) {
	list( $flig_sak_m, $flig_sak_y ) = explode( '.', getGet( 'frekl_sak_menesis' ) );
	$where[] = 'YEAR(g.reklama_no) = ' . (int) $flig_sak_y . ' AND MONTH(g.reklama_no) = ' . (int) $flig_sak_m;
}

if ( getGet( 'frekl_beig_no' ) !== '' ) {
	$where[] = 'g.reklama_lidz >= "' . to_db_date( getGet( 'frekl_beig_no' ) ) . '"';
}
if ( getGet( 'frekl_beig_lidz' ) !== '' ) {
	$where[] = 'g.reklama_lidz <= "' . to_db_date( getGet( 'frekl_beig_lidz' ) ) . '"';
}

if ( getGet( 'frekl_beig_menesis' ) !== '' ) {
	list( $flig_beig_m, $flig_beig_y ) = explode( '.', getGet( 'frekl_beig_menesis' ) );
	$where[] = 'YEAR(g.reklama_lidz) = ' . (int) $flig_beig_y . ' AND MONTH(g.reklama_lidz) = ' . (int) $flig_beig_m;
}


if ( pt_multiselect_is_active( 'fsanems_forma' ) ) {
	$where[] = 'g.sanemsanas_forma IN (' . implode( ',', array_map( 'intval', getGet( 'fsanems_forma' ) ) ) . ')';
}

if ( pt_multiselect_is_active( 'favots' ) ) {
	$where[] = 'g.avots IN (' . implode( ',', array_map( 'intval', getGet( 'favots' ) ) ) . ')';
}

if ( pt_multiselect_is_active( 'fkonta_admin' ) ) {
	$where[] = 'g.konta_admin IN ("' . implode( '", "', getGet( 'fkonta_admin' ) ) . '")';
}

if ( ! isset( $_GET['fpakalpojums'] ) ) {
	$_GET['fpakalpojums'] = array();
	foreach ( $_vars['mpt_filter'] as $k => $p ) {
		$_GET['fpakalpojums'][] = $p;
		if ( is_array( $p ) ) {
			foreach ( $p as $j => $l ) {
				$_GET['fpakalpojums'][] = $j;
			}
		}
	}
}
//var_dump($_GET['fpakalpojums']);

if ( pt_multiselect_is_active( 'fpakalpojums' ) ) {

	$where[] = '
    (
      g.id IN (
        SELECT liguma_id
        FROM `' . DB_PREF . 'pakalpojumi`
        WHERE
          pakalpojuma_veida_id IN (' . implode( ',', array_map( 'intval', getGet( 'fpakalpojums' ) ) ) . ') OR
          pakalpojuma_veida_id IN (
            SELECT id FROM `' . DB_PREF . 'pakalpojumi_veidi` WHERE `parent_id` IN (' . implode( ',', array_map( 'intval', getGet( 'fpakalpojums' ) ) ) . ')
          )

       ) AND g.mpk = 0
    )
  ';

}

if ( pt_multiselect_is_active( 'finfomedia_statuss' ) ) {
	$where[] = 'g.infomedia_statuss IN (' . implode( ',', array_map( 'intval', getGet( 'finfomedia_statuss' ) ) ) . ')';
}

//if ( isset( $_GET['fdomens'] ) and $_GET['c'] != '' ) {
//	$where[] = "g.domens like '%" . $_GET['fdomens'] . "'";
//}

if ( getGet( 'fgoogle_konti' ) !== '' ) {
	$where[] = '
  (
    g.id IN (SELECT liguma_id FROM `' . DB_PREF . 'pielikumi_tiessaite` WHERE google_konts LIKE "' . getGet( 'fgoogle_konti' ) . '") OR
    g.id IN (SELECT liguma_id FROM `' . DB_PREF . 'pielikumi_infomedia` WHERE google_konts LIKE "' . getGet( 'fgoogle_konti' ) . '") OR
    g.id IN (SELECT liguma_id FROM `' . DB_PREF . 'pielikumi_yandex` WHERE konts LIKE "' . getGet( 'fgoogle_konti' ) . '") OR
    g.id IN (SELECT liguma_id FROM `' . DB_PREF . 'pielikumi_citi` WHERE konts LIKE "' . getGet( 'fgoogle_konti' ) . '")
  )
';
}
if ( getGet( 'fgoogle_atslegv' ) !== '' ) {
	$where[] = '
  (
    g.id IN (SELECT liguma_id FROM `' . DB_PREF . 'pielikumi_tiessaite` WHERE atslegvards LIKE "' . getGet( 'fgoogle_atslegv' ) . '") OR
    g.id IN (SELECT liguma_id FROM `' . DB_PREF . 'pielikumi_infomedia` WHERE atslegvards LIKE "' . getGet( 'fgoogle_atslegv' ) . '") OR
    g.id IN (SELECT liguma_id FROM `' . DB_PREF . 'pielikumi_yandex` WHERE atslegvards LIKE "' . getGet( 'fgoogle_atslegv' ) . '") OR
    g.id IN (SELECT liguma_id FROM `' . DB_PREF . 'pielikumi_citi` WHERE atslegvards LIKE "' . getGet( 'fgoogle_atslegv' ) . '")
  )
';
}
if ( getGet( 'flig_summa' ) !== '' ) {
	$having[] = 'FLOOR(liguma_summa_bez_pvn) = ' . (int) getGet( 'flig_summa' );
}

//  Provīzijas filtrs
if ( getGet( 'fprov_percent' ) !== '' ) {
	$having[] = 'FLOOR(g.provizija_pardevejam) = ' . (int) getGet( 'fprov_percent' );
}

$atg_having = array();
if ( pt_is_selected( 1, 'fakt_atgad' ) ) {
	$atg_having[] = 'aktivi_atgadinajumi > 0';
}
if ( pt_is_selected( 2, 'fakt_atgad' ) ) {
	$atg_having[] = 'aktivi_atgadinajumi = 0';
}
if ( ! empty( $atg_having ) ) {
	$having[] = '(' . implode( ' OR ', $atg_having ) . ')';
}

//  Tikai vadītāja padoto līgumi
if ( check_access( 'ligumi-vaditajs-tikai-savi' ) && ! is_superadmin() ) {
	$padotie       = Pardevejs::model()->findAll( array( 'conditions' => array( 'vaditajs_id = ' . $_SESSION['user']['id'] ) ) );
	$padotie_ids   = modelsToList( $padotie, 'id' );
	$padotie_ids[] = $_SESSION['user']['id'];
	$where[]       = 'g.pardeveja_id IN (' . implode( ', ', $padotie_ids ) . ')';
}

$sql = "
  SELECT
    g.*,
		g.total_rentabil as total_rentabil_db,
    b.beigu_statuss,
    b.atlikt_lidz as beigu_statuss_atlikt_lidz,
    b.auto_atteikums as beigu_statuss_auto_atteikums,
    (
      SELECT SUM(summa)
      FROM `" . DB_PREF . "rekini`
      WHERE
        liguma_id = g.id AND
        atkapsanas_rekins = 0
    ) as liguma_summa_bez_pvn,
    (
      SELECT COUNT(*)
      FROM `" . DB_PREF . "atgadinajumi`
      WHERE
        liguma_id = g.id AND
        statuss = 1 AND
        datums <= CURDATE() AND
        redzamiba IN ('kopejs', " . ( is_admin() ? "'admin'" : "'pardevejs'" ) . ")
    ) as aktivi_atgadinajumi,
    (
      SELECT izr_datums
      FROM `" . DB_PREF . "rekini`
      WHERE liguma_id = g.id
      ORDER BY izr_datums DESC
      LIMIT 1
    ) as last_rek_izr_datums,
    (
      SELECT COUNT(*)
      FROM `" . DB_PREF . "ligumi_saistitie_pardeveji`
      WHERE liguma_id = g.id
    ) as saistito_pardeveju_skaits,
    IF (i.pardeveja_id IS NOT NULL, 1, 0) as pardevejam_izcelts,
    r.grupas_regnr,
    (
  		SELECT COUNT(*)
  		FROM `new_rekini`
  		WHERE liguma_id = g.id AND atkapsanas_rekins = 0
  	) AS rekinu_skaits,
		( select sum(kopsumma-parsk_budzets) from new_rekini where liguma_id=g.id and kopsumma<=apm_summa ) as rentabil
				
";

if ( $group_view ) {

	$sql .= ",
    (
			SELECT GROUP_CONCAT(liguma_id)
			FROM `" . DB_PREF . "ligumi_grupas`
			WHERE grupas_regnr = r.grupas_regnr
			GROUP BY grupas_regnr
    ) AS ligumu_id_grupaa
  ";

}

$sql .= "
  FROM `" . DB_PREF . "ligumi` g
  LEFT JOIN `" . DB_PREF . "kontakti` k ON (g.id = k.liguma_id)
  LEFT JOIN `" . DB_PREF . "pardeveji` p ON (g.pardeveja_id = p.id)
  LEFT JOIN `" . DB_PREF . "ligumi_beigusies` b ON (g.id = b.liguma_id)
  LEFT JOIN `" . DB_PREF . "ligumi_ipasie` i ON (g.id = i.liguma_id AND i.pardeveja_id = " . ( intval( $_SESSION['user']['id'] ) ) . ")
  LEFT JOIN `" . DB_PREF . "ligumi_grupas` r ON (r.liguma_id = g.id)
";

if ( $group_view ) {
	$where[] = "
    g.id = IFNULL((
  		SELECT g2.id
  		FROM `" . DB_PREF . "ligumi_grupas` gr1
  		LEFT JOIN `" . DB_PREF . "ligumi_grupas` gr2 ON (gr2.grupas_regnr = gr1.grupas_regnr)
  		LEFT JOIN `" . DB_PREF . "ligumi` g2 ON (g2.id = gr2.liguma_id)
  		WHERE gr1.liguma_id = g.id
  		ORDER BY IF(g2.`sanemsanas_datums` IS NULL, 0, 1) ASC, g2.`sanemsanas_datums` DESC, g2.`ligumadat` DESC, g2.`ligumanr` DESC
  		LIMIT 1
    ), g.id) -- compare g.id to itself so the row will be returned even it has no group
  ";
}

if ( ! empty( $where ) ) {
	$sql .= " WHERE " . implode( ' AND ', $where );
}

$sql .= "
GROUP BY g.id";

if ( ! empty( $having ) ) {
	$sql .= "
  HAVING " . implode( ' AND ', $having );
}

$sql .= "
ORDER BY " . ( ! empty( $order ) ? $order . " " . $direction : 'IF(g.`sanemsanas_datums` IS NULL, 0, 1) ASC, g.`sanemsanas_datums` DESC, g.`ligumadat` DESC, g.`ligumanr` DESC' );

if ( $_SERVER["REMOTE_ADDR"] == '213.21.219.21' && true ) {
	#var_dump($order);
	#echo "<br /><br />";
	#var_dump($sql);
	#die();
}


$query = db_query( $sql );


$alldata = array();

$rep     = db_get_assoc_all( db_query( "select * from " . DB_PREF . "ligumi_google_reports order by liguma_id,fullreport desc" ) );
$reports = array();
foreach ( $rep as $r ) {
	if ( ! isset( $reports[ $r['liguma_id'] ] ) ) {
		$reports[ $r['liguma_id'] ] = array();
	}
	$reports[ $r['liguma_id'] ][] = $r;
}

// Totals array
$totals = array(
	'rek_summa'                  => 0,
	'ligumu_summa'               => 0,
	'lig_vid_summa'              => 0,
	'lig_vid_day_summa'          => 0,
	'b_parsk_summa'              => 0,
	'b_apm_summa'                => 0,
	'rentabil_summa'             => 0,
	'pieslegto_rentabil14'       => 0,
	'pieslegto_rentabil14_title' => '',
);

// apply filtering to complex columns
while ( $row = db_get_assoc( $query ) ) {


	$row['liguma_statuss'] = get_liguma_statuss( $row, ! empty( $rekini[ $row['id'] ] ) ? $rekini[ $row['id'] ] : null );


	$row['domains'] = array();
	$domain_table   = db_get_assoc_all( db_query( "SELECT * FROM `" . DB_PREF . "ligumi_domains` WHERE `ligumi_id` = " . $row['id'] ) );
	foreach ( $domain_table as $domain ) {
		$row['domains'][] = $domain['domain'];
	}

	if ( pt_multiselect_is_active( 'flig_statuss' ) ) {

		$show = false;

		if ( pt_is_selected( $row['liguma_statuss'], 'flig_statuss' ) ) {
			$show = true;
		} elseif ( $row['liguma_statuss'] == 6 && ( empty( $row['beigu_statuss'] ) || $row['beigu_statuss'] == 'termins' ) && pt_is_selected( '6_termins', 'flig_statuss' ) ) {
			$show = true;
		} elseif ( $row['liguma_statuss'] == 6 && $row['beigu_statuss'] == 'parslegts' && pt_is_selected( '6_parslegts', 'flig_statuss' ) ) {
			$show = true;
		} elseif ( $row['liguma_statuss'] == 6 && $row['beigu_statuss'] == 'termins' && pt_is_selected( '6_termins', 'flig_statuss' ) ) {
			$show = true;
		} elseif ( $row['liguma_statuss'] == 6 && $row['beigu_statuss'] == 'atteikums' && pt_is_selected( '6_atteikums', 'flig_statuss' ) ) {
			$show = true;
		} elseif ( $row['liguma_statuss'] == 6 && ! empty( $row['beigu_statuss_auto_atteikums'] ) && pt_is_selected( '6_atteikums_p', 'flig_statuss' ) ) {
			$show = true;
		}

		if ( ! $show ) {
			continue;
		}

	}

	if ( pt_multiselect_is_active( 'fpakalpojums_mpk' ) ) {

		$mpk_filters = getGet( 'fpakalpojums_mpk' );

		if ( ! empty( $mpk_filters ) ) {

			if ( ! $row['mpk'] ) {
				continue;
			}

			$continue = true;

			//  Google search
			if ( in_array( 'search', $mpk_filters ) ) {
				if ( ! empty( $visi_mpk_atslegvardi[ $row['id'] ] ) ) {
					foreach ( $visi_mpk_atslegvardi[ $row['id'] ] as $av ) {
						if ( $av['site'] == 'google' ) {
							$continue = false;
							break;
						}
					}
				}
			}


			//  Yandex direct
			if ( in_array( 'direct', $mpk_filters ) ) {
				if ( ! empty( $visi_mpk_atslegvardi[ $row['id'] ] ) ) {
					foreach ( $visi_mpk_atslegvardi[ $row['id'] ] as $av ) {
						if ( $av['site'] == 'yandex' ) {
							$continue = false;
							break;
						}
					}
				}
			}

			//  Google display
			if ( in_array( 'display', $mpk_filters ) ) {

				//  Categories
				if ( ! empty( $visi_mpk_temas[ $row['id'] ] ) ) {
					foreach ( $visi_mpk_temas[ $row['id'] ] as $av ) {
						if ( $av['site'] == 'google' ) {
							$continue = false;
							break;
						}
					}
				}

				//  Display
				if ( ! empty( $visi_mpk_displeji[ $row['id'] ] ) ) {
					foreach ( $visi_mpk_displeji[ $row['id'] ] as $av ) {
						if ( $av['site'] == 'google' ) {
							$continue = false;
							break;
						}
					}
				}
			}

			//  Yandex konteksts
			if ( in_array( 'kontext', $mpk_filters ) ) {
				if ( ! empty( $visi_mpk_temas[ $row['id'] ] ) ) {
					foreach ( $visi_mpk_temas[ $row['id'] ] as $av ) {
						if ( $av['site'] == 'yandex' ) {
							$continue = false;
							break;
						}
					}
				}
			}

			//  Google konversijas
			if ( in_array( 'conversions', $mpk_filters ) ) {
				if ( ! empty( $visi_mpk_konversijas[ $row['id'] ] ) ) {
					foreach ( $visi_mpk_konversijas[ $row['id'] ] as $av ) {
						if ( $av['site'] == 'google' ) {
							$continue = false;
							break;
						}
					}
				}
			}

			//  Google analytics
			if ( in_array( 'analytics', $mpk_filters ) ) {
				if ( ! empty( $visi_mpk_analitika[ $row['id'] ] ) ) {
					foreach ( $visi_mpk_analitika[ $row['id'] ] as $av ) {
						if ( $av['site'] == 'google' ) {
							$continue = false;
							break;
						}
					}
				}
			}

			//  Yandex metrika
			if ( in_array( 'metrika', $mpk_filters ) ) {
				if ( ! empty( $visi_mpk_analitika[ $row['id'] ] ) ) {
					foreach ( $visi_mpk_analitika[ $row['id'] ] as $av ) {
						if ( $av['site'] == 'yandex' ) {
							$continue = false;
							break;
						}
					}
				}
			}

			//  Other
			if ( in_array( 'other', $mpk_filters ) ) {
				if ( isset( $visi_mpk_other[ $row['id'] ] ) ) {
					$continue = false;
				}
			}

			if ( $continue ) {
				continue;
			}
		}
	}


	$row['norekinu_statuss'] = get_apmaksas_statuss( ! empty( $rekini[ $row['id'] ] ) ? $rekini[ $row['id'] ] : null );

	if ( pt_multiselect_is_active( 'fkopapm_statuss' ) ) {

		$show = false;

		if ( pt_is_selected( $row['norekinu_statuss'], 'fkopapm_statuss' ) ) {
			$show = true;
		} else if ( pt_is_selected( '2_barteris', 'fkopapm_statuss' ) ) {
			if ( $row['norekinu_statuss'] == 2 && is_barteris( ! empty( $rekini[ $row['id'] ] ) ? $rekini[ $row['id'] ] : null ) ) {
				$show = true;
			}
		}

		if ( ! $show ) {
			continue;
		}

	}

	$row['max_nokavetas_dienas'] = 0;
	if ( $row['norekinu_statuss'] == 4 ) {
		$row['max_nokavetas_dienas'] = get_max_nokavetas_dienas( ! empty( $rekini[ $row['id'] ] ) ? $rekini[ $row['id'] ] : null );
	}

	$rekini_to_show = array();

	if ( ! empty( $rekini[ $row['id'] ] ) ) {

		foreach ( $rekini[ $row['id'] ] as $rekins ) {

			if ( getGet( 'frek_izr_no' ) !== '' && $rekins['izr_datums'] < to_db_date( getGet( 'frek_izr_no' ) ) ) {
				continue;
			}
			if ( getGet( 'frek_izr_lidz' ) !== '' && $rekins['izr_datums'] > to_db_date( getGet( 'frek_izr_lidz' ) ) ) {
				continue;
			}

			if ( getGet( 'frek_izr_menesis' ) !== '' ) {
				list( $frek_izr_m, $frek_izr_y ) = explode( '.', getGet( 'frek_izr_menesis' ) );
				if ( date( 'Y', strtotime( $rekins['izr_datums'] ) ) != $frek_izr_y || date( 'm', strtotime( $rekins['izr_datums'] ) ) != $frek_izr_m ) {
					continue;
				}
			}


			if ( getGet( 'frek_summa' ) !== '' && floor( $rekins['kopsumma'] ) != (int) getGet( 'frek_summa' ) ) {
				continue;
			}

			if ( pt_multiselect_is_active( 'frek_statuss' ) && ! pt_is_selected( $rekins['statuss'], 'frek_statuss' ) ) {
				continue;
			}

			if ( pt_multiselect_is_active( 'frek_apm_statuss' ) ) {

				$show = false;

				if ( pt_is_selected( $rekins['apm_statuss'], 'frek_apm_statuss' ) ) {
					$show = true;
				} else if ( pt_is_selected( '2_barteris', 'frek_apm_statuss' ) ) {
					if ( $rekins['apm_statuss'] == 2 && ! empty( $rekins['barteris'] ) ) {
						$show = true;
					}
				}

				if ( pt_is_selected( 'atkapsanas_rekins', 'frek_apm_statuss' ) ) {
					if ( ! empty( $rekins['atkapsanas_rekins'] ) ) {
						$show = true;
					}
				}


				if ( ! $show ) {
					continue;
				}

			}

			if ( getGet( 'frek_apm_no' ) !== '' && $rekins['pedejais_apm_datums'] < to_db_date( getGet( 'frek_apm_no' ) ) ) {
				continue;
			}
			if ( getGet( 'frek_apm_lidz' ) !== '' && $rekins['pedejais_apm_datums'] > to_db_date( getGet( 'frek_apm_lidz' ) ) ) {
				continue;
			}

			if ( getGet( 'frek_apm_menesis' ) !== '' ) {
				list( $frek_apm_m, $frek_apm_y ) = explode( '.', getGet( 'frek_apm_menesis' ) );
				if ( date( 'Y', strtotime( $rekins['pedejais_apm_datums'] ) ) != $frek_apm_y || date( 'm', strtotime( $rekins['pedejais_apm_datums'] ) ) != $frek_apm_m ) {
					continue;
				}
			}

			if ( getGet( 'fbp_lvl_apm_no' ) !== '' && ( $rekins['bp_apm_datums'] < to_db_date( getGet( 'fbp_lvl_apm_no' ) ) || ! $rekins['bp_apm_datums'] ) ) {
				continue;
			}
			if ( getGet( 'fbp_lvl_apm_lidz' ) !== '' && ( $rekins['bp_apm_datums'] > to_db_date( getGet( 'fbp_lvl_apm_lidz' ) ) || ! $rekins['bp_apm_datums'] ) ) {
				continue;
			}
			if ( getGet( 'fb_apm_menesis' ) !== '' ) {
				list( $fb_apm_m, $fb_apm_y ) = explode( '.', getGet( 'fb_apm_menesis' ) );
				if ( date( 'Y', strtotime( $rekins['bp_apm_datums'] ) ) != $fb_apm_y || date( 'm', strtotime( $rekins['bp_apm_datums'] ) ) != $fb_apm_m ) {
					continue;
				}
			}

			if ( getGet( 'fbp_lvl_summa' ) !== '' && floor( $rekins['bp_lvl_summa'] ) != (int) getGet( 'fbp_lvl_summa' ) ) {
				continue;
			}

			if ( getGet( 'frentabil' ) !== '' && ( ! $row['mpk'] || $rekins['apm_statuss'] != 2 || bccomp( ( $rekins['kopsumma'] - $rekins['parsk_budzets'] ), getGet( 'frentabil' ), 2 ) != 0 ) ) {
				continue;
			}

			$rekini_to_show[] = $rekins;

		}

	}

	// if contract has no bills to show, but bill filters are in efect, then dont show contract
	if ( empty( $rekini_to_show ) && ( getGet( 'frek_izr_no' ) !== '' || getGet( 'frek_izr_lidz' ) !== '' || getGet( 'frek_summa' ) !== '' || pt_multiselect_is_active( 'frek_statuss' ) || pt_multiselect_is_active( 'frek_apm_statuss' ) ) ) {
		continue;
	}

	$row['rekini'] = $rekini_to_show;

	$row['pakalpojumu_saraksts'] = '';

	if ( ! $row['mpk'] && ! empty( $pakalpojumi[ $row['id'] ] ) ) {

		$visu_pak_saraksts = array();
		foreach ( $pakalpojumi[ $row['id'] ] as $pak_parent_id => $v ) {
			$pak_saraksts = array();
			foreach ( $v as $v2 ) {
				$pak_saraksts[] = $v2['nosaukums'] . ( ! empty( $v2['pakalpojuma_apr'] ) ? "(" . $v2['pakalpojuma_apr'] . ")" : "" );
			}
			$visu_pak_saraksts[] = '<strong>' . $_vars['pakalpojumi'][ $pak_parent_id ]['nosaukums'] . ':</strong><br />' . implode( ', ', $pak_saraksts );
		}
		$row['pakalpojumu_saraksts'] = implode( '<br />', $visu_pak_saraksts );

	} elseif ( ! empty( $mpk_pakalpojumi ) && ! empty( $mpk_pakalpojumi[ $row['id'] ] ) ) {
		$visu_pak_saraksts = array();
		foreach ( $mpk_pakalpojumi[ $row['id'] ] as $site => $pak ) {
			if ( $site && $site != 'other' ) {
				$pak_saraksts = array();
				foreach ( $pak as $pak_type => $count ) {
					$pak_saraksts[] = $_vars[ $site . '_pakalpojumi' ][ $pak_type ];
				}
				$visu_pak_saraksts[] = '<strong>' . $_vars['mekletaji'][ $site ] . ':</strong><br />' . implode( ', ', $pak_saraksts );
			} else {
				$visu_pak_saraksts[] = '<strong>Cits</strong>';
			}
		}
		$row['pakalpojumu_saraksts'] = implode( '<br />', $visu_pak_saraksts );
	}

	//  MPT
	if ( ! $row['mpk'] ) {
//ADPREWIEW
		$continue = false;
		if ( isset( $_GET['fadpreview'] ) and count( $_GET['fadpreview'] ) ) {

			if ( empty( $visi_pielikumi[ $row['id'] ] ) or ! isset( $visi_pielikumi[ $row['id'] ]['tiessaite'] ) or empty( $visi_pielikumi[ $row['id'] ]['tiessaite'] ) ) {
				$continue = true;
			} elseif ( in_array( '1', $_GET['fadpreview'] ) and in_array( '0', $_GET['fadpreview'] ) ) {
				foreach ( $visi_pielikumi[ $row['id'] ]['tiessaite'] as $v ) {
					if ( $v['pedeja_parbaude'] === null ) {
						$continue = true;
					}
				}
			} elseif ( in_array( '1', $_GET['fadpreview'] ) ) {
				$continue = true;
				foreach ( $visi_pielikumi[ $row['id'] ]['tiessaite'] as $v ) {
					$pos = explode( ',', $v['pozicija'] );
					foreach ( $pos as $p ) {
						if ( intval( $p ) > 0 ) {
							$continue = false;
						}
					}
				}
			} elseif ( in_array( '0', $_GET['fadpreview'] ) ) {
				if ( $_SERVER["REMOTE_ADDR"] == '213.21.219.21' ) {
					#var_dump($row);
					#die();
				}
				$continue = true;
				foreach ( $visi_pielikumi[ $row['id'] ]['tiessaite'] as $v ) {
					if ( $v['pedeja_parbaude'] !== null ) {
						$pos = explode( ',', $v['pozicija'] );
						foreach ( $pos as $p ) {
							if ( intval( $p ) == 0 or ( $v['top3'] and ( ! $v['top'] or intval( $p ) > 3 ) ) ) {
								$continue = false;
							}
						}
					}
				}

				if ( $row['liguma_statuss'] != 3 ) {
					$continue = true;
				}

			}
		}
		if ( $continue ) {
			continue;
		}

		$continue = false;
		if ( isset( $_GET['fpozicija'] ) and $_GET['fpozicija'] != '' ) {
			if ( empty( $visi_pielikumi[ $row['id'] ] ) or ! isset( $visi_pielikumi[ $row['id'] ]['tiessaite'] ) or empty( $visi_pielikumi[ $row['id'] ]['tiessaite'] ) ) {
				$continue = true;
			} else {
				$continue = true;
				foreach ( $visi_pielikumi[ $row['id'] ]['tiessaite'] as $v ) {
					if ( $v['pedeja_parbaude'] != null ) {
						$pos = explode( ',', $v['pozicija'] );
						foreach ( $pos as $p ) {
							if ( intval( $p ) == intval( $_GET['fpozicija'] ) ) {
								$continue = false;
							}
						}
					}
				}
			}
		}

		if ( $continue ) {
			continue;
		}

		if ( $_SERVER["REMOTE_ADDR"] == '213.21.219.21' ) {
			#var_dump($visi_pielikumi);
		}
//ADPREWIEW
		$visi_konti            = array();
		$visi_konti_ar_linkiem = array();
		$visi_atslv            = array();

		if ( ! empty( $visi_pielikumi[ $row['id'] ] ) ) {
			$pozicijas = array();
			$adpreview = array();
			foreach ( $visi_pielikumi[ $row['id'] ] as $pielikuma_tips => $pielikumi ) {
				$adpreview[] = '<b>&nbsp;</b>';
				$pozicijas[] = '<b>&nbsp;</b>';
				$konti       = array();
				$atslv       = array();

				foreach ( $pielikumi as $v ) {

					$konts_raw = in_array( $pielikuma_tips, array( 'infomedia', 'tiessaite' ) ) ? $v['google_konts'] : $v['konts'];

					foreach ( array_filter( array_map( 'trim', preg_split( "/[,;]/", trim( strtolower( $konts_raw ) ) ) ) ) as $konts ) {
						if ( ! in_array( $konts, $konti ) ) {
							$konti[] = $konts;
						}
					}

                    $tmpDomain = false;
                    $domeni = array();

                    if ( $pielikuma_tips == 'infomedia' ) {

                        $$tmpDomain = true;
                        $domeni[] = '<a target="_blank" href="' . get_google_url_new( $v['atslegvards'], 'lv' ) . '">lv</a>';

                    } else if ( $pielikuma_tips == 'tiessaite' || $pielikuma_tips == 'yandex' ) {

                        $domeni_parts = array_filter( array_map( 'trim', preg_split( "/[\s,;]/", strtolower( $v['domeni'] ) ) ) );

                        foreach ( $domeni_parts as $domens ) {

                            if ( $pielikuma_tips == 'tiessaite' ) {
                                $url = get_google_url_new( $v['atslegvards'], $domens );
                            } else if ( $pielikuma_tips == 'yandex' ) {
                                $url = get_yandex_url( $v['atslegvards'], $domens );
                            }

                            if($domens == 'lv') {
                                $tmpDomain = true;
                            }

                            if ( $url ) {
                                $domeni[] = '<a target="_blank" href="' . $url . '">' . $domens . '</a>';
                            } else {
                                $domeni[] = $domens;
                            }
                        }

                    }

                    $atslv[] = ( ! empty( $domeni ) ? '(' . implode( ',', $domeni ) . ') ' : '' ) . ($tmpDomain ? ('<a target="_blank" href="https://www.google.lv/search?q='. $v['atslegvards'].'">'.$v['atslegvards']. '</a>') : $v['atslegvards']);

//ADPREWIEW
					if ( $pielikuma_tips == 'tiessaite' and isset( $v['pedeja_parbaude'] ) ) {
						if ( $v['pedeja_parbaude'] === null || $row['liguma_statuss'] == 6 ) {
							$adpreview[] = '&nbsp;';
							$pozicijas[] = '&nbsp;';
						} else {
							if ( $v['pozicija'] === "" ) {
								$pozicijas[] = '0';
								$adpreview[] = '<span style="color:white; background-color:red;">' . _( 'Nav' ) . '</span> ' . substr( $v['pedeja_parbaude'], 8, 2 ) . '.' . substr( $v['pedeja_parbaude'], 5, 2 ) . '.' . substr( $v['pedeja_parbaude'], 0, 4 ) . ' ' . substr( $v['pedeja_parbaude'], 11, 5 );
							} else {
								$pozicijas[] = $v['pozicija'];
								$adpreview[] = ( ( $v['top3'] == 1 and ( ! $v['top'] or $v['pozicija'] > 3 ) ) ? '<span style="color:white; background-color:red;">' . _( 'Nav' ) . '</span>' : _( 'OK' ) ) . ' ' . substr( $v['pedeja_parbaude'], 8, 2 ) . '.' . substr( $v['pedeja_parbaude'], 5, 2 ) . '.' . substr( $v['pedeja_parbaude'], 0, 4 ) . ' ' . substr( $v['pedeja_parbaude'], 11, 5 );
							}
						}
					} else {
						$adpreview[] = '&nbsp;';
						$pozicijas[] = '&nbsp;';
					}
					//ADPREWIEW
				}

				$konti_ar_linkiem = array();

				foreach ( $konti as $v ) {
					if ( isset( $kontu_linki[ $row['id'] ][ $pielikuma_tips ][ $v ] ) && ! empty( $kontu_linki[ $row['id'] ][ $pielikuma_tips ][ $v ]['links'] ) ) {
						$konti_ar_linkiem[] = '<a target="_blank" href="' . $kontu_linki[ $row['id'] ][ $pielikuma_tips ][ $v ]['links'] . '" style="white-space: nowrap;">' . $v . '</a>';
					} else {
						$konti_ar_linkiem[] = '<span style="white-space: nowrap;">' . $v . '</span>';
					}
				}

				$visi_konti[ $pielikuma_tips ]            = '<strong>' . $kontu_tipi[ $pielikuma_tips ] . '</strong>:<br />' . implode( ', ', $konti );
				$visi_konti_ar_linkiem[ $pielikuma_tips ] = '<strong>' . $kontu_tipi[ $pielikuma_tips ] . '</strong>:<br />' . implode( ', ', $konti_ar_linkiem );

				$visi_atslv[ $pielikuma_tips ] = '<strong>' . $kontu_tipi[ $pielikuma_tips ] . '</strong>:<br />' . implode( ',<br />', $atslv );
			}
		}

		$row['google_konti']            = implode( '<br />', $visi_konti );
		$row['google_konti_ar_linkiem'] = implode( '<br />', $visi_konti_ar_linkiem );
		$row['google_atslegv_saraksts'] = implode( '<br />', $visi_atslv );
		$row['pozicijas']               = ( isset( $pozicijas ) && is_array( $pozicijas ) ) ? implode( '<br />', $pozicijas ) : "";
		$row['adpreview']               = ( isset( $adpreview ) && is_array( $adpreview ) ) ? implode( '<br />', $adpreview ) : "";
		if ( $_SERVER["REMOTE_ADDR"] == '213.21.219.21' ) {
			#echo "<pre>";
			#print_r($row);
			#echo "</pre>";
		}

		//  MPK
	} elseif ( $row['mpk'] ) {


		$av_list          = array();
		$konti            = array();
		$konti_ar_linkiem = array();

		//  Atslēgvārdi
		if ( isset( $visi_mpk_atslegvardi[ $row['id'] ] ) && ! empty( $visi_mpk_atslegvardi[ $row['id'] ] ) ) {

			foreach ( $visi_mpk_atslegvardi[ $row['id'] ] as $av ) {

				//  Konti
				list( $k, $kl ) = parseKonts( $av['konts'], $av['site'], isset( $kontu_linki[ $row['id'] ] ) ? $kontu_linki[ $row['id'] ] : array() );

				if ( ! isset( $konti[ $av['site'] ] ) ) {
					$konti[ $av['site'] ] = array();
				}
				$konti[ $av['site'] ] = array_merge( $konti[ $av['site'] ], $k );

				if ( ! isset( $konti_ar_linkiem[ $av['site'] ] ) ) {
					$konti_ar_linkiem[ $av['site'] ] = array();
				}
				$konti_ar_linkiem[ $av['site'] ] = array_merge( $konti_ar_linkiem[ $av['site'] ], $kl );

				$domeni = array();

				$domeni_parts = array_filter( array_map( 'trim', preg_split( "/[\s,;]/", strtolower( $av['domens'] ) ) ) );

				foreach ( $domeni_parts as $domens ) {

					if ( $av['site'] == 'google' ) {
						$url = get_google_url_new( $av['keyword'], $domens );
					} else if ( $av['site'] == 'yandex' ) {
						$url = get_yandex_url( $av['keyword'], $domens );
					}

					if ( $url ) {
						$domeni[] = '<a target="_blank" href="' . $url . '">' . $domens . '</a>';
					} else {
						$domeni[] = $domens;
					}
				}

				if ( ! isset( $av_list[ $av['site'] ] ) ) {
					$av_list[ $av['site'] ] = array();
				}
				$av_list[ $av['site'] ][] = ( ! empty( $domeni ) ? '(' . implode( ',', $domeni ) . ') ' : '' ) . $av['keyword'];
			}
		}

		//  Tēmas
		if ( isset( $visi_mpk_temas[ $row['id'] ] ) && ! empty( $visi_mpk_temas[ $row['id'] ] ) ) {
			foreach ( $visi_mpk_temas[ $row['id'] ] as $av ) {

				//  Konti
				list( $k, $kl ) = parseKonts( $av['konts'], $av['site'], isset( $kontu_linki[ $row['id'] ] ) ? $kontu_linki[ $row['id'] ] : array() );

				if ( ! isset( $konti[ $av['site'] ] ) ) {
					$konti[ $av['site'] ] = array();
				}
				$konti[ $av['site'] ] = array_merge( $konti[ $av['site'] ], $k );

				if ( ! isset( $konti_ar_linkiem[ $av['site'] ] ) ) {
					$konti_ar_linkiem[ $av['site'] ] = array();
				}
				$konti_ar_linkiem[ $av['site'] ] = array_merge( $konti_ar_linkiem[ $av['site'] ], $kl );
			}
		}

		//  Displeji
		if ( isset( $visi_mpk_displeji[ $row['id'] ] ) && ! empty( $visi_mpk_displeji[ $row['id'] ] ) ) {
			foreach ( $visi_mpk_displeji[ $row['id'] ] as $av ) {

				//  Konti
				list( $k, $kl ) = parseKonts( $av['konts'], $av['site'], isset( $kontu_linki[ $row['id'] ] ) ? $kontu_linki[ $row['id'] ] : array() );

				if ( ! isset( $konti[ $av['site'] ] ) ) {
					$konti[ $av['site'] ] = array();
				}
				$konti[ $av['site'] ] = array_merge( $konti[ $av['site'] ], $k );

				if ( ! isset( $konti_ar_linkiem[ $av['site'] ] ) ) {
					$konti_ar_linkiem[ $av['site'] ] = array();
				}
				$konti_ar_linkiem[ $av['site'] ] = array_merge( $konti_ar_linkiem[ $av['site'] ], $kl );
			}
		}

		//  Konversijas
		if ( isset( $visi_mpk_konversijas[ $row['id'] ] ) && ! empty( $visi_mpk_konversijas[ $row['id'] ] ) ) {
			foreach ( $visi_mpk_konversijas[ $row['id'] ] as $av ) {

				//  Konti
				list( $k, $kl ) = parseKonts( $av['konts'], $av['site'], isset( $kontu_linki[ $row['id'] ] ) ? $kontu_linki[ $row['id'] ] : array() );

				if ( ! isset( $konti[ $av['site'] ] ) ) {
					$konti[ $av['site'] ] = array();
				}
				$konti[ $av['site'] ] = array_merge( $konti[ $av['site'] ], $k );

				if ( ! isset( $konti_ar_linkiem[ $av['site'] ] ) ) {
					$konti_ar_linkiem[ $av['site'] ] = array();
				}
				$konti_ar_linkiem[ $av['site'] ] = array_merge( $konti_ar_linkiem[ $av['site'] ], $kl );
			}
		}

		//  Av list
		$av_groups = array();
		foreach ( $av_list as $site => $keywords ) {
			if ( $site ) {
				$av_groups[] = '<strong>' . $_vars['mekletaji'][ $site ] . '</strong><br />' . implode( ',<br />', $keywords );
			}
		}

		//  Konti list
		$visi_konti = array();
		foreach ( $konti as $site => $klist ) {
			if ( $site ) {
				$legacy_site         = $site == 'google' ? 'tiessaite' : $site;
				$visi_konti[ $site ] = '<strong>' . $kontu_tipi[ $legacy_site ] . '</strong>:<br />' . implode( ', ', $klist );
			}
		}

		$visi_konti_ar_linkiem = array();
		foreach ( $konti_ar_linkiem as $site => $klist ) {
			if ( $site ) {
				$legacy_site                    = $site == 'google' ? 'tiessaite' : $site;
				$visi_konti_ar_linkiem[ $site ] = '<strong>' . $kontu_tipi[ $legacy_site ] . '</strong>:<br />' . implode( ', ', $klist );
			}
		}

		$row['google_konti']            = implode( '<br />', $visi_konti );
		$row['google_konti_ar_linkiem'] = implode( '<br />', $visi_konti_ar_linkiem );
		$row['google_atslegv_saraksts'] = implode( '<br />', $av_groups );
	}

	if ( $group_view ) {
		$row['ligumi_skaits_grupaa'] = ! empty( $row['ligumu_id_grupaa'] ) ? count( explode( ",", $row['ligumu_id_grupaa'] ) ) : 0;
	}


	$row['total_rentabil'] = 0;

	if ( $row['mpk'] ) {
		if ( isset( $rekini[ $row['id'] ] ) ) {
			foreach ( (array) $rekini[ $row['id'] ] as $rekins ) {
				if ( $rekins['apm_statuss'] == 2 ) {
					$row['total_rentabil'] += ( $rekins['kopsumma'] - $rekins['parsk_budzets'] );
				}
			}
		}
	}

	$alldata[] = $row;

}

$total_rows = count( $alldata );


// apply sorting to complex columns

if ( getGet( 'order' ) == 'rentabil' ) {

	function sort_by_rentabil_asc( $a, $b ) {
		return $a['total_rentabil'] == $b['total_rentabil'] ? 0 : ( ( $a['total_rentabil'] > $b['total_rentabil'] ) ? - 1 : 1 );
	}

	function sort_by_rentabil_desc( $a, $b ) {
		return $a['total_rentabil'] == $b['total_rentabil'] ? 0 : ( ( $a['total_rentabil'] < $b['total_rentabil'] ) ? - 1 : 1 );
	}

	usort( $alldata, 'sort_by_rentabil_' . $direction );
}


if ( getGet( 'order' ) == 'lig_statuss' ) {

	function sort_by_lig_statuss_asc( $a, $b ) {
		return $a['liguma_statuss'] == $b['liguma_statuss'] ? 0 : ( ( $a['liguma_statuss'] < $b['liguma_statuss'] ) ? - 1 : 1 );
	}

	function sort_by_lig_statuss_desc( $a, $b ) {
		return $a['liguma_statuss'] == $b['liguma_statuss'] ? 0 : ( ( $a['liguma_statuss'] > $b['liguma_statuss'] ) ? - 1 : 1 );
	}

	usort( $alldata, 'sort_by_lig_statuss_' . $direction );

}

if ( getGet( 'order' ) == 'kopapm_statuss' ) {

	function sort_by_kopapm_statuss_asc( $a, $b ) {

		if ( $a['norekinu_statuss'] == $b['norekinu_statuss'] ) {
			return $a['max_nokavetas_dienas'] == $b['max_nokavetas_dienas'] ? 0 : ( $a['max_nokavetas_dienas'] < $b['max_nokavetas_dienas'] ? - 1 : 1 );
		} else {
			return $a['norekinu_statuss'] < $b['norekinu_statuss'] ? - 1 : 1;
		}

	}

	function sort_by_kopapm_statuss_desc( $a, $b ) {

		if ( $a['norekinu_statuss'] == $b['norekinu_statuss'] ) {
			return $a['max_nokavetas_dienas'] == $b['max_nokavetas_dienas'] ? 0 : ( $a['max_nokavetas_dienas'] > $b['max_nokavetas_dienas'] ? - 1 : 1 );
		} else {
			return $a['norekinu_statuss'] > $b['norekinu_statuss'] ? - 1 : 1;
		}

	}

	usort( $alldata, 'sort_by_kopapm_statuss_' . $direction );

}

if ( getGet( 'order' ) == 'pakalpojums' ) {

	function sort_by_pakalpojums_asc( $a, $b ) {
		return strncasecmp( strip_tags( $a['pakalpojumu_saraksts'] ), strip_tags( $b['pakalpojumu_saraksts'] ), 20 );
	}

	function sort_by_pakalpojums_desc( $a, $b ) {
		return strncasecmp( strip_tags( $b['pakalpojumu_saraksts'] ), strip_tags( $a['pakalpojumu_saraksts'] ), 20 );
	}

	usort( $alldata, 'sort_by_pakalpojums_' . $direction );

}

if ( getGet( 'order' ) == 'google_atslegv' ) {

	function sort_by_google_atslegv_asc( $a, $b ) {
		return strncasecmp( strip_tags( $a['google_atslegv_saraksts'] ), strip_tags( $b['google_atslegv_saraksts'] ), 20 );
	}

	function sort_by_google_atslegv_desc( $a, $b ) {
		return strncasecmp( strip_tags( $b['google_atslegv_saraksts'] ), strip_tags( $a['google_atslegv_saraksts'] ), 20 );
	}

	usort( $alldata, 'sort_by_google_atslegv_' . $direction );

}

if ( getGet( 'order' ) == 'google_konti' ) {

	function sort_by_google_konti_asc( $a, $b ) {
		return strncasecmp( strip_tags( $a['google_konti'] ), strip_tags( $b['google_konti'] ), 20 );
	}

	function sort_by_google_konti_desc( $a, $b ) {
		return strncasecmp( strip_tags( $b['google_konti'] ), strip_tags( $a['google_konti'] ), 20 );
	}

	usort( $alldata, 'sort_by_google_konti_' . $direction );

}

// calculate totals

$asd = 0;

foreach ( $alldata as $data ) {

	$totals['ligumu_summa'] += $data['liguma_summa_bez_pvn'];

	$s_ilgums = 0;
	if ( ! empty( $data['reklama_no'] ) && ! empty( $data['reklama_lidz'] ) ) {
		$s_ilgums = round( ( strtotime( $data['reklama_lidz'] ) - strtotime( $data['reklama_no'] ) ) / 86400 ) + 1;
	}

	if ( $s_ilgums > 0 && $data['liguma_summa_bez_pvn'] > 0 ) {

		$totals['lig_vid_summa'] += $data['liguma_summa_bez_pvn'] / $s_ilgums * 365;

		if ( $data['google_statuss'] == 3 && ! $data['mpk'] ) {

			$totals['lig_vid_day_summa'] += $data['liguma_summa_bez_pvn'] / $s_ilgums;

		}

	}

	if ( ! empty( $data['rekini'] ) ) {

		foreach ( $data['rekini'] as $rekins ) {
			$totals['rek_summa'] += $rekins['kopsumma'];

			$apm_summa_bez_pvn = $rekins['apm_summa'] / ( 1 + $rekins['pvn'] / 100 );
			if ( $rekins['apm_budzets'] ) {
				$apm_budzets = $rekins['apm_budzets'];
			} else if ( $data['mpk'] ) {
				$apm_budzets = $apm_summa_bez_pvn - $apm_summa_bez_pvn * ( $data['apkalp_maksa_proc'] / 100 );
			} else {
				$apm_budzets = false;
			}
			$totals['b_apm_summa']   += $apm_budzets;
			$totals['b_parsk_summa'] += $rekins['parsk_budzets'];

			if ( $data['mpk'] && $rekins['apm_statuss'] == 2 ) {
				$totals['rentabil_summa'] += ( $rekins['kopsumma'] - $rekins['parsk_budzets'] );
			}
		}

	}

	$report = false;
	if ( isset( $reports[ $data['id'] ] ) ) {
		$report = $reports[ $data['id'] ];
	}
	if ( $report and $data['reklama_no'] and $data['reklama_lidz'] ) {
		foreach ( $report as $r ) {
			if ( ! $r['fullreport'] and $data['google_statuss'] == '3' ) {
				$from                                 = new DateTime( $r['datums_no'] );
				$to                                   = new DateTime( $r['datums_lidz'] );
				$spent                                = intval( $to->diff( $from )->format( '%a' ) ) + 1;
				$totals['pieslegto_rentabil14']       += intval( $r['Cost'] ) / $spent / 1000000;
				$totals['pieslegto_rentabil14_title'] .= ( intval( $r['Cost'] ) / 1000000 ) . '/' . $spent . ' + ';
			}
		}
	}

}

// limit rows to show

if ( $show_in_page > 0 ) {
	$alldata = array_slice( $alldata, ( $active_page - 1 ) * $show_in_page, $show_in_page );
}

$visible_rows = count( $alldata );
?>

    <form action="" id="fullformplace" method="GET" style="clear: both;">

        <input type="hidden" name="c" value="ligumi"/>

        <input type="hidden" name="order" value="<?= getGet( 'order' ) ?>"/>
        <input type="hidden" name="direction" value="<?= getGet( 'direction' ) ?>"/>

        <input type="hidden" name="page" value="<?= $active_page ?>"/>
        <input type="hidden" name="show_in_page" value="<?= $show_in_page ?>"/>

        <input name="fsearch" type="hidden" style="width: 150px;" value="<?= getGet( 'fsearch' ) ?>">

		<?php if ( isset( $_GET['fligumu_id'] ) ) { ?>
            <input name="fligumu_id" type="hidden" value="<?= getGet( 'fligumu_id' ) ?>"/>
		<?php } ?>

		<?php if ( getGet( 'ftikai_ipasie' ) ) { ?>
            <input name="ftikai_ipasie" type="hidden" value="1"/>
		<?php } ?>

        <div class="glob_filter" style="float: left; width: 300px; text-align: left;">
            Līgums aktīvs uz: <input class="kalendari" id="lig_aktivs_datums" name="flig_aktivs_datums" type="input" style="width:80px;" value="<?= getGet( 'flig_aktivs_datums' ) ?>"> <a
                    class="sfield_clear" href="#">x</a>
            <input class="ui-state-default ui-corner-all" style="width: 5em; visibility: hidden;" type="submit" value="Atlasīt"/>
        </div>

        <div>

  <span style="margin-right: 20px; margin-left: -130px;">
    <?php if ( $group_view ) { ?>
        <a href="?c=ligumi&skatijums=parastais">Līgumu skatījums</a>
    <?php } else { ?>
        <a href="?c=ligumi&skatijums=grupets">Klientu skatījums</a>
    <?php } ?>
  </span>

			<?php print_paginator( $total_rows, $active_page, $show_in_page ) ?>

            <span>
    &nbsp;
				<?php if ( check_access( 'ligumi-jauns' ) ) { ?>
                    <button style="float: right;" onclick="document.location='?c=ligumi&a=jauns'; return false;" class="ui-state-default ui-corner-all">Pievienot jaunu līgumu</button>
				<?php } ?>
  </span>
        </div>


        <table cellpadding="3" cellspacing="0" class="data" id="ligumu_kopsavilkums" style="width: auto;">

            <thead>

            <tr class="groups">
                <th colspan="2"></th>
                <th colspan="3" class="bgc-sta">Statuss</th>
                <th colspan="9" class="bgc-lig">Līgums</th>
				<?= ! check_access( 'ligumi-pakalpojums-col' ) ? '' : '<th colspan="8" class="bgc-pak">Pakalpojums</th>'; // Don't show to bookkeeper   ?>
                <th colspan="12" class="bgc-rek">Norēķini</th>
                <th class="bgc-pro">Provīzija</th>
                <th>Atg.</th>
            </tr>

            <tr class="header">

                <th colname="npk" class="header" width="30">Npk</th>
				<? print_th( 'Nosaukums', 'nosaukums', array( 'width' => 180 ) ) ?>

				<? print_th( 'Līguma statuss', 'lig_statuss' ) ?>
				<? print_th( 'Norēķinu statuss', 'kopapm_statuss' ) ?>
				<? print_th( 'Google statuss', 'google_statuss' ) ?>

				<? print_th( 'Līguma datums', 'lig_dat', array( 'width' => 40 ) ) ?>
				<? print_th( 'Līguma nr.', 'liguma_nr', array( 'width' => 50 ) ) ?>
				<? print_th( 'Līguma slēdzējs', 'darbinieks' ) ?>
				<? print_th( 'Indekss', 'indekss', array( 'width' => 40 ) ) ?>
				<? print_th( 'Līgums saņemts', 'lig_san_dat', array( 'width' => 40 ) ) ?>
				<? print_th( 'Saņemšanas forma', 'sanems_forma', array( 'width' => 30 ) ) ?>
				<? print_th( 'Avots', 'avots', array( 'width' => 30 ) ) ?>
				<? print_th( 'Līgums no', 'rekl_dat_no', array( 'width' => 40 ) ) ?>
				<? print_th( 'Līgums līdz', 'rekl_dat_lidz', array( 'width' => 40 ) ) ?>

				<?php if ( check_access( 'ligumi-pakalpojums-col' ) ) { //  Don't show to bookkeeper ?>
					<? print_th( 'Pakalpojums', 'pakalpojums', array( 'width' => 150 ) ) ?>
					<? print_th( 'Infomedia statuss', 'infomedia_statuss', array( 'width' => 70 ) ) ?>
					<? print_th( 'Konti', 'google_konti', array( 'width' => 60 ) ) ?>
					<? print_th( 'Atslēgvārdi', 'google_atslegv', array( 'width' => 200 ) ) ?>
					<?php //ADPREWIEW ?>
					<? print_th( 'Pozīcijas', 'pozicijas', array() ); ?>
					<? print_th( 'AdPreview', 'adpreview', array() ); ?>
                    <!--					--><? // print_th( 'Domēns', 'domens', array() ); ?>
					<?php //ADPREWIEW end ?>
					<? print_th( 'Konta administrators', 'konta_admin', array( 'width' => 60 ) ) ?>
				<? } ?>

				<? print_th( 'Līguma summa<br />(bez PVN)', 'lig_summa', array( 'width' => 60 ) ) ?>
				<? print_th( 'Rēķinu skaits', 'rekinu_skaits', array( 'width' => 60 ) ) ?>
				<? print_th( 'Rēķina izr.datums', 'rek_izr_dat', array( 'width' => 40 ) ) ?>
				<? print_th( 'Rēķina summa<br />(ar PVN)', 'rek_summa', array( 'ordering' => false, 'width' => 70 ) ) ?>
				<? print_th( 'Rēķina statuss', 'rek_statuss', array( 'ordering' => false, 'width' => 70 ) ) ?>
				<? print_th( 'Apmaksas statuss', 'rek_apm_statuss', array( 'ordering' => false, 'width' => 50 ) ) ?>
				<? print_th( 'Apmaksas datums', 'rek_apm_datums', array( 'ordering' => false, 'width' => 50 ) ) ?>
				<? print_th( 'Apmaksātais<br />budžets (EUR)', 'b_apm_summa', array( 'ordering' => false, 'width' => 50 ) ) ?>
				<? print_th( 'Pārskaitītais<br />budžets (EUR)', 'b_parsk_summa', array( 'ordering' => false, 'width' => 50 ) ) ?>
				<? print_th( 'Pārskaitījuma<br />datums (EUR)', 'bp_lvl_apm_datums', array( 'ordering' => false, 'width' => 50 ) ) ?>
				<? print_th( 'Rentabilitāte MPK', 'rentabil', array( 'ordering' => true, 'width' => 50 ) ) ?>
				<? print_th( 'Rentabilitāte MPT', 'rentabil2', array( 'ordering' => true, 'width' => 50 ) ) ?>

				<? print_th( 'Provīzija&nbsp;%', 'prov_proc', array( 'width' => 50 ) ) ?>

				<? print_th( 'Aktīvie atgādinājumi', 'akt_atgad', array( 'width' => 40 ) ) ?>

            </tr>

            <tr class="filter last">

                <th></th>
                <th><span><input name="fnosaukums" class="sfield" type="input" value="<?= getGet( 'fnosaukums' ) ?>" style="width:80%;"><a class="sfield_clear" href="#">x</a></span></th>

                <th>
                    <select name="flig_statuss[]" class="sfield" multiple="multiple" size="1" style="width: 70px;">
                        <option value=""></option>
						<?php foreach ( $_vars['liguma_statuss'] as $k => $p ) { ?>
                            <option <? pt_print_selected( $k, 'flig_statuss' ) ?> value="<?= $k; ?>"><?= $p; ?></option>
							<? if ( $k == 6 ) { ?>
                                <option <? pt_print_selected( '6_termins', 'flig_statuss' ) ?> value="6_termins">&nbsp;&nbsp;&nbsp;<?= ucfirst( $_vars['liguma_beigu_statuss']['termins'] ) ?></option>
                                <option <? pt_print_selected( '6_parslegts', 'flig_statuss' ) ?> value="6_parslegts">
                                    &nbsp;&nbsp;&nbsp;<?= ucfirst( $_vars['liguma_beigu_statuss']['parslegts'] ) ?></option>
                                <option <? pt_print_selected( '6_atteikums', 'flig_statuss' ) ?> value="6_atteikums">
                                    &nbsp;&nbsp;&nbsp;<?= ucfirst( $_vars['liguma_beigu_statuss']['atteikums'] ) ?></option>
                                <option <? pt_print_selected( '6_atteikums_p', 'flig_statuss' ) ?> value="6_atteikums_p">&nbsp;&nbsp;&nbsp;(Pārtraukts)</option>
							<? } ?>
						<?php } ?>
                    </select>
                    <a class="sfield_clear" href="#">x</a>
                </th>

                <th>
                    <select name="fkopapm_statuss[]" class="sfield" multiple="multiple" size="1" style="width: 70px;">
                        <option value=""></option>
						<?php foreach ( $_vars['kopapmaksas_statuss'] as $k => $p ) { ?>
                            <option <? pt_print_selected( $k, 'fkopapm_statuss' ) ?> value="<?= $k; ?>"><?= $p; ?></option>
							<? if ( $k == 2 ) { ?>
                                <option <? pt_print_selected( '2_barteris', 'fkopapm_statuss' ) ?> value="2_barteris">&nbsp;&nbsp;&nbsp;Barteris</option>
							<? } ?>
						<?php } ?>
                    </select>
                    <a class="sfield_clear" href="#">x</a>
                </th>

                <th>
                    <select name="fgoogle_statuss[]" class="sfield" multiple="multiple" size="1" style="width: 70px;">
                        <option value=""></option>
						<?php foreach ( $_vars['googlestatuss2'] as $k => $p ) { ?>
                            <option <? pt_print_selected( $k, 'fgoogle_statuss' ) ?> value="<?= $k; ?>"><?= $p; ?></option>
						<?php } ?>
                    </select>
                    <a class="sfield_clear" href="#">x</a>
                </th>

                <th>
                    <span><input name="flig_no" class="sfield kalendari" type="input" value="<?= getGet( 'flig_no' ) ?>" style="width:80%;"><a class="sfield_clear" href="#">x</a></span><br>
                    <span><input name="flig_lidz" class="sfield kalendari" type="input" value="<?= getGet( 'flig_lidz' ) ?>" style="width:80%;"><a class="sfield_clear" href="#">x</a></span>
                </th>

                <th><span><input name="fliguma_nr" class="sfield" type="input" value="<?= getGet( 'fliguma_nr' ) ?>" style="width:80%;"><a class="sfield_clear" href="#">x</a></span></th>

                <th id="fpardevejs">
                    <select name="fpardevejs[]" class="sfield" multiple="multiple" size="1" style="width: 100px;">
                        <option value=""></option>
						<?php foreach ( user_filter_list() as $k => $p ) { ?>

                            <option <? pt_print_selected( $k, 'fpardevejs' ) ?> value="<?= $k; ?>"><?= $p; ?></option>
						<?php } ?>
                    </select>
                    <a class="sfield_clear" href="#">x</a>
                </th>

                <th><span><input name="ffaktaddr_index" class="sfield" type="input" value="<?= getGet( 'ffaktaddr_index' ) ?>" style="width:80%;"><a class="sfield_clear" href="#">x</a></span></th>

                <th>
                    <span><input name="flig_sanemts_no" class="sfield kalendari" type="input" value="<?= getGet( 'flig_sanemts_no' ) ?>" style="width:80%;"><a class="sfield_clear"
                                                                                                                                                               href="#">x</a></span><br>
                    <span><input name="flig_sanemts_lidz" class="sfield kalendari" type="input" value="<?= getGet( 'flig_sanemts_lidz' ) ?>" style="width:80%;"><a class="sfield_clear"
                                                                                                                                                                   href="#">x</a></span><br>
                    <span><input name="flig_sanemts_menesis" class="sfield" id="sanemts_month" type="input" value="<?= getGet( 'flig_sanemts_menesis' ) ?>" style="width:80%;"><a class="sfield_clear"
                                                                                                                                                                                  href="#">x</a></span>
                </th>

                <th>
                    <select name="fsanems_forma[]" class="sfield" multiple="multiple" size="1" style="width: 50px;">
                        <option value=""></option>
						<?php foreach ( $_vars['sanemsanas_forma'] as $k => $p ) { ?>
                            <option <? pt_print_selected( $k, 'fsanems_forma' ) ?> value="<?= $k; ?>"><?= $p; ?></option>
						<?php } ?>
                    </select>
                    <a class="sfield_clear" href="#">x</a>
                </th>

                <th>
                    <select name="favots[]" class="sfield" multiple="multiple" size="1" style="width: 50px;">
                        <option value=""></option>
						<?php foreach ( $_vars['avoti'] as $k => $p ) { ?>
                            <option <? pt_print_selected( $k, 'favots' ) ?> value="<?= $k; ?>"><?= $p; ?></option>
						<?php } ?>
                    </select>
                    <a class="sfield_clear" href="#">x</a>
                </th>

                <th>
                    <span><input name="frekl_sak_no" class="sfield kalendari" type="input" value="<?= getGet( 'frekl_sak_no' ) ?>" style="width:80%;"><a class="sfield_clear" href="#">x</a></span><br>
                    <span><input name="frekl_sak_lidz" class="sfield kalendari" type="input" value="<?= getGet( 'frekl_sak_lidz' ) ?>" style="width:80%;"><a class="sfield_clear"
                                                                                                                                                             href="#">x</a></span><br/>
                    <span><input name="frekl_sak_menesis" class="sfield" id="sak_month" type="input" value="<?= getGet( 'frekl_sak_menesis' ) ?>" style="width:80%;"><a class="sfield_clear"
                                                                                                                                                                        href="#">x</a></span>
                </th>

                <th>
                    <span><input name="frekl_beig_no" class="sfield kalendari" type="input" value="<?= getGet( 'frekl_beig_no' ) ?>" style="width:80%;"><a class="sfield_clear"
                                                                                                                                                           href="#">x</a></span><br>
                    <span><input name="frekl_beig_lidz" class="sfield kalendari" type="input" value="<?= getGet( 'frekl_beig_lidz' ) ?>" style="width:80%;"><a class="sfield_clear"
                                                                                                                                                               href="#">x</a></span><br/>
                    <span><input name="frekl_beig_menesis" class="sfield" id="beig_month" type="input" value="<?= getGet( 'frekl_beig_menesis' ) ?>" style="width:80%;"><a class="sfield_clear"
                                                                                                                                                                           href="#">x</a></span>
                </th>

				<?php
				//  Pakalpojums col-set

				if ( check_access( 'ligumi-pakalpojums-col' ) ) {   //   Do not show to bookkeeper ?>
                    <th>
                        <input type="hidden" name="fpakalpojums" value="default">
                        <span>MPT <select name="fpakalpojums[]" class="sfield" multiple="multiple" size="1" style="width: 100px;">
            <option value=""></option>
								<?php foreach ( $_vars['mpt_filter'] as $k => $p ) {
									if ( is_array( $p ) ) { ?>
                                        <optgroup label="<?php echo $k; ?>">
                <?php foreach ( $p as $j => $l ) { ?>
                    <option <? pt_print_selected( $j, 'fpakalpojums' ) ?> value="<?= $j; ?>"><?= $l; ?></option>
                <?php } ?>
                </optgroup>
									<?php } else { ?>
                                        <option <? pt_print_selected( $p, 'fpakalpojums' ) ?> value="<?= $p; ?>"><?= $k; ?></option>
									<?php } ?>
								<?php } ?>
          </select><a class="sfield_clear" href="#">x</a></span><br/>

                        <span>MPK <select name="fpakalpojums_mpk[]" class="sfield" multiple="multiple" size="1" style="width: 100px;">
            <option value=""></option>
								<?php foreach ( $_vars['mpk_filter'] as $k => $p ) {
									if ( is_array( $p ) ) { ?>
                                        <optgroup label="<?php echo $k; ?>">
                <?php foreach ( $p as $j => $l ) { ?>
                    <option <? pt_print_selected( $j, 'fpakalpojums_mpk' ) ?> value="<?= $j; ?>"><?= $l; ?></option>
                <?php } ?>
                </optgroup>
									<?php } else { ?>
                                        <option <? pt_print_selected( $p, 'fpakalpojums_mpk' ) ?> value="<?= $p; ?>"><?= $k; ?></option>
									<?php } ?>
								<?php } ?>
          </select><a class="sfield_clear" href="#">x</a></span>
                    </th>

                    <th>
                        <select name="finfomedia_statuss[]" class="sfield" multiple="multiple" size="1" style="width: 50px;">
                            <option value=""></option>
							<?php foreach ( $_vars['infomediastatuss'] as $k => $p ) { ?>
                                <option <? pt_print_selected( $k, 'finfomedia_statuss' ) ?> value="<?= $k; ?>"><?= $p; ?></option>
							<?php } ?>
                        </select>
                        <a class="sfield_clear" href="#">x</a>
                    </th>

                    <th><span><input name="fgoogle_konti" class="sfield" type="input" value="<?= getGet( 'fgoogle_konti' ) ?>" style="width:80%;"><a class="sfield_clear" href="#">x</a></span></th>

                    <th><span><input name="fgoogle_atslegv" class="sfield" type="input" value="<?= getGet( 'fgoogle_atslegv' ) ?>" style="width:80%;"><a class="sfield_clear" href="#">x</a></span></th>
                    <!--ADPREWIEV-->
                    <th><span><input name="fpozicija" class="sfield" type="input" value="<?= getGet( 'fpozicija' ) ?>" style="width:80%;"><a class="sfield_clear" href="#">x</a></span></th>
                    <th>
                        <select name="fadpreview[]" class="sfield" size="1" multiple="multiple" style="width: 50px;">
                            <option value=""></option>
                            <option value="1"<?= ( ( isset( $_GET['fadpreview'] ) and in_array( '1', $_GET['fadpreview'] ) ) ? ' selected' : '' ); ?>><?= _( 'OK' ); ?></option>
                            <option value="0"<?= ( ( isset( $_GET['fadpreview'] ) and in_array( '0', $_GET['fadpreview'] ) ) ? ' selected' : '' ); ?>><?= _( 'Nav' ); ?></option>
                        </select>
                        <a class="sfield_clear" href="#">x</a>
                    </th>
                    <!--                    <th><span><input name="fdomens" class="sfield" type="input" value="--><?//= getGet( 'fdomens' ) ?><!--" style="width:80%;"><a class="sfield_clear" href="#">x</a></span></th>-->
                    <!--ADPREWIEV-->
                    <th>
                        <select name="fkonta_admin[]" class="sfield" size="1" multiple="multiple" style="width: 50px;">
                            <option value=""></option>
							<?php foreach ( $_vars['konta_admin'] as $k => $p ) { ?>
                                <option <? pt_print_selected( $k, 'fkonta_admin' ) ?> value="<?= $k; ?>"><?= $p; ?></option>
							<?php } ?>
                        </select>
                        <a class="sfield_clear" href="#">x</a>
                    </th>
					<?php
				}
				//  End: pakalpojums col-set
				?>

                <th><span><input name="flig_summa" class="sfield" type="input" value="<?= getGet( 'flig_summa' ) ?>" style="width:80%;"><a class="sfield_clear" href="#">x</a></span></th>

                <th><span><input name="frekinu_skaits" class="sfield" type="input" value="<?= getGet( 'frekinu_skaits' ) ?>" style="width:80%;"><a class="sfield_clear" href="#">x</a></span></th>

                <th>
                    <span><input name="frek_izr_no" class="sfield kalendari" type="input" value="<?= getGet( 'frek_izr_no' ) ?>" style="width:80%;"><a class="sfield_clear" href="#">x</a></span><br>
                    <span><input name="frek_izr_lidz" class="sfield kalendari" type="input" value="<?= getGet( 'frek_izr_lidz' ) ?>" style="width:80%;"><a class="sfield_clear"
                                                                                                                                                           href="#">x</a></span><br/>
                    <span><input name="frek_izr_menesis" class="sfield" id="izr_month" type="input" value="<?= getGet( 'frek_izr_menesis' ) ?>" style="width:80%;"><a class="sfield_clear"
                                                                                                                                                                      href="#">x</a></span>
                </th>

                <th><span><input name="frek_summa" class="sfield" type="input" value="<?= getGet( 'frek_summa' ) ?>" style="width:80%;"><a class="sfield_clear" href="#">x</a></span></th>

                <th>
                    <select name="frek_statuss[]" class="sfield" multiple="multiple" size="1" style="width: 60px;">
                        <option value=""></option>
						<?php foreach ( $_vars['rekina_statuss'] as $k => $p ) { ?>
                            <option <? pt_print_selected( $k, 'frek_statuss' ) ?> value="<?= $k; ?>"><?= $p; ?></option>
						<?php } ?>
                    </select>
                    <a class="sfield_clear" href="#">x</a>
                </th>

                <th>
                    <select name="frek_apm_statuss[]" class="sfield" multiple="multiple" size="1" style="width: 100px;">
                        <option value=""></option>
						<?php foreach ( $_vars['apmaksas_statuss'] as $k => $p ) { ?>
							<?php if ( $k == 1 ) {
								continue;
							} // do not show "Nav sācies" ?>
                            <option <? pt_print_selected( $k, 'frek_apm_statuss' ) ?> value="<?= $k; ?>"><?= $p; ?></option>
							<? if ( $k == 2 ) { ?>
                                <option <? pt_print_selected( '2_barteris', 'frek_apm_statuss' ) ?> value="2_barteris">&nbsp;&nbsp;&nbsp;Barteris</option>
							<? } ?>
						<?php } ?>
                        <option <? pt_print_selected( 'atkapsanas_rekins', 'frek_apm_statuss' ) ?> value="atkapsanas_rekins">Atkāpšanās rēķins</option>
                    </select>
                    <a class="sfield_clear" href="#">x</a>
                </th>

                <th>
                    <span><input name="frek_apm_no" class="sfield kalendari" type="input" value="<?= getGet( 'frek_apm_no' ) ?>" style="width:80%;"><a class="sfield_clear" href="#">x</a></span><br>
                    <span><input name="frek_apm_lidz" class="sfield kalendari" type="input" value="<?= getGet( 'frek_apm_lidz' ) ?>" style="width:80%;"><a class="sfield_clear"
                                                                                                                                                           href="#">x</a></span><br/>
                    <span><input name="frek_apm_menesis" class="sfield" id="apm_month" type="input" value="<?= getGet( 'frek_apm_menesis' ) ?>" style="width:80%;"><a class="sfield_clear"
                                                                                                                                                                      href="#">x</a></span>
                </th>

                <th><span><input name="fb_apm_summa" class="sfield" type="input" value="<?= getGet( 'fb_apm_summa' ) ?>" style="width:80%;"><a class="sfield_clear" href="#">x</a></span></th>
                <th><span><input name="fb_parsk_summa" class="sfield" type="input" value="<?= getGet( 'fb_parsk_summa' ) ?>" style="width:80%;"><a class="sfield_clear" href="#">x</a></span></th>

                <th>
                    <span><input name="fbp_lvl_apm_no" class="sfield kalendari" type="input" value="<?= getGet( 'fbp_lvl_apm_no' ) ?>" style="width:80%;"><a class="sfield_clear"
                                                                                                                                                             href="#">x</a></span><br>
                    <span><input name="fbp_lvl_apm_lidz" class="sfield kalendari" type="input" value="<?= getGet( 'fbp_lvl_apm_lidz' ) ?>" style="width:80%;"><a class="sfield_clear"
                                                                                                                                                                 href="#">x</a></span><br/>
                    <span><input name="fb_apm_menesis" class="sfield" id="b_apm_month" type="input" value="<?= getGet( 'fb_apm_menesis' ) ?>" style="width:80%;"><a class="sfield_clear" href="#">x</a></span>
                </th>

                <th><span><input name="frentabil" class="sfield" type="input" value="<?= getGet( 'frentabil' ) ?>" style="width:80%;"><a class="sfield_clear" href="#">x</a></span></th>

                <th><span><input name="frentabil2" class="sfield" type="input" value="<?= getGet( 'frentabil2' ) ?>" style="width:80%;"><a class="sfield_clear" href="#">x</a></span></th>

                <th><span><input class="sfield" name="fprov_percent" type="input" value="<?= getGet( 'fprov_percent' ) ?>" style="width:80%;"><a class="sfield_clear" href="#">x</a></span></th>

                <th>
                    <select name="fakt_atgad[]" class="sfield" multiple="multiple" size="1" style="width: 40px;">
                        <option value=""></option>
                        <option <? pt_print_selected( 1, 'fakt_atgad' ) ?> value="1">Ir</option>
                        <option <? pt_print_selected( 2, 'fakt_atgad' ) ?> value="2">Nav</option>
                    </select>
                    <a class="sfield_clear" href="#">x</a>
                </th>

            </tr>

            </thead>

            <tbody class="main">

			<?php

			$npk   = $total_rows - ( ( $active_page - 1 ) * $show_in_page );
			$inid  = - 1;
			$today = new DateTime( date( 'Y-m-d', mktime( 0, 0, 0, date( 'n' ), intval( date( 'j' ) ) + 1, date( 'Y' ) ) ) . ' 00:00:00' );
			foreach ( $alldata as $data ) {
				$inid ++;

				?>

                <tr id="tr<?= $inid; ?>" class="lig_row" title="<?= $data['nosaukums']; ?>">

                    <td class="c" class="s_id" id="npk_id<?= $inid; ?>" style="white-space: nowrap;">
						<?= $npk; ?>
                        <img onclick="toggleIpasais(this, <?= $data['id'] ?>); return false;" class="toggle_ipasais <?= $data['pardevejam_izcelts'] ? 'toggle_ipasais_izcelts' : '' ?>"
                             src="css/star_small<?= ! $data['pardevejam_izcelts'] ? '_dis' : '' ?>.png" alt="" title="<?= $data['pardevejam_izcelts'] ? 'Izcelts' : 'Nav izcelts' ?>"/>
                    </td>
                    <td class="s_nosaukums <?= ! empty( $data['aktivi_atgadinajumi'] ) ? 'corner1 has_act_atg' : '' ?>">

                        <a href="?c=ligumi&a=labot&id=<?= $data['id']; ?>"><b><?= $data['nosaukums']; ?></b></a>

						<?php if ( $group_view ) { ?>
							<?php if ( ! empty( $data['ligumi_skaits_grupaa'] ) ) { ?>
                                (<a href="?c=ligumi&fligumu_id=<?php echo $data['ligumu_id_grupaa'] ?>"><?php echo $data['ligumi_skaits_grupaa'] ?></a>)
							<?php } else { ?>
                                (N/A)
							<?php } ?>
						<?php } ?>

						<? if ( ! empty( $data['saistito_pardeveju_skaits'] ) ) { ?>

							<?php
							$lig_saist_pard = array();
							if ( ! empty( $saist_pard[ $data['id'] ] ) ) {
								foreach ( $saist_pard[ $data['id'] ] as $v ) {
									$lig_saist_pard[] = $v['vards'];
								}
							}
							?>

                            <span class="saist_pard_ico" title="Saistītie pārdevēji: <?php echo implode( ', ', $lig_saist_pard ) ?>">s</span>

						<? } ?>

						<? if ( ! empty( $atgadinajumi[ $data['id'] ] ) ) { ?>

                            <a class="act_atg_h" style="" href="?c=ligumi&a=labot&id=<?= $data['id']; ?>&subtab=3"></a>

                            <div style="display: none;" class="atgadinajumi">

                                <table class="data">

                                    <thead>
                                    <tr>
                                        <th>Datums</th>
                                        <th>Apraksts</th>
                                    </tr>
                                    </thead>

                                    <tbody>

									<? foreach ( $atgadinajumi[ $data['id'] ] as $a ) { ?>

                                        <tr>
                                            <td><? echo date( "d.m.Y", strtotime( $a['datums'] ) ) ?></td>
                                            <td><? echo $a['saturs'] ?></td>
                                        </tr>

									<? } ?>

                                    </tbody>

                                </table>

                            </div>

						<? } ?>

                    </td>
					<? if ( $data['liguma_statuss'] == 6 ) { ?>
						<? $active = ( empty( $data['beigu_statuss'] ) || ( $data['beigu_statuss'] == 'termins' && $data['beigu_statuss_atlikt_lidz'] <= date( 'Y-m-d' ) ) ) ? true : false; ?>
                        <td class="c">
                            <div class="lig_statuss_6 <?= ! empty( $data['beigu_statuss'] ) ? 'lig_beigu_statuss_' . $data['beigu_statuss'] : 'lig_beigu_statuss_termins' ?>">
								<?php if ( check_access( 'ligumi-beigu-statuss' ) ) { // Do not show direct edit icon to bookkeeper ?>
                                    <a class="edit_link" href="#" onclick="showLigumaBeiguStatussDialog(<?= $data['id'] ?>); return false;"><img src="css/edit.png" alt="Labot statusu"
                                                                                                                                                 title="Labot statusu"/></a>
								<?php } ?>
                                <a href="?c=ligumi&a=labot&id=<?php echo $data['id'] ?>&subtab=0" class="<?= $active ? 'lig_beigu_statuss_active' : '' ?>">
                                    <span class="stat_name"><?= $_vars['liguma_statuss'][6] ?></span>
                                    (<?= ! empty( $data['beigu_statuss'] ) ? $_vars['liguma_beigu_statuss'][ $data['beigu_statuss'] ] : $_vars['liguma_beigu_statuss']['termins'] ?><?= ! empty( $data['beigu_statuss_auto_atteikums'] ) ? ', <span title="Līgums pirms reklāmas beigām tika pārtraukts">P</span>' : '' ?>
                                    )
                                </a>
                            </div>
                        </td>
					<? } else { ?>
                        <td class="c"><?= ! empty( $_vars['liguma_statuss'][ $data['liguma_statuss'] ] ) ? '<div class="' . ( ! empty( $_vars['liguma_statuss'][ $data['liguma_statuss'] ] ) ? 'lig_statuss_' . $data['liguma_statuss'] : '' ) . '"><a href="?c=ligumi&a=labot&id=' . $data['id'] . '&subtab=0">' . $_vars['liguma_statuss'][ $data['liguma_statuss'] ] . '</a></div>' : '&nbsp;' ?></td>
					<? } ?>

					<? if ( $data['norekinu_statuss'] == 2 && is_barteris( ! empty( $rekini[ $data['id'] ] ) ? $rekini[ $data['id'] ] : null ) ) { ?>
                        <td class="c">
                            <div class="norek_statuss_2">
                                <a href="?c=ligumi&a=labot&id=<?= $data['id']; ?>&subtab=2">Barteris</a>
                            </div>
                        </td>
					<? } else { ?>
                        <td class="c">
                            <div class="<?= ! empty( $_vars['kopapmaksas_statuss'][ $data['norekinu_statuss'] ] ) ? 'norek_statuss_' . $data['norekinu_statuss'] : '' ?>">
								<?= ! empty( $_vars['kopapmaksas_statuss'][ $data['norekinu_statuss'] ] ) ? '<a href="?c=ligumi&a=labot&id=' . $data['id'] . '&subtab=2">' . $_vars['kopapmaksas_statuss'][ $data['norekinu_statuss'] ] . '</a>' : "&nbsp;" ?>

								<? if ( $data['norekinu_statuss'] == 4 ) { ?>
                                    <span style="font-size: 0.75em;">(<?= get_max_nokavetas_dienas( ! empty( $rekini[ $data['id'] ] ) ? $rekini[ $data['id'] ] : null ) ?>)</span>
								<? } ?>
                            </div>
                        </td>
					<? } ?>

                    <td class="c s_googlestatuss"><?= ! empty( $_vars['googlestatuss'][ $data['google_statuss'] ] ) ? '<div class="' . ( ! empty( $_vars['googlestatuss'][ $data['google_statuss'] ] ) ? 'google_statuss_' . $data['google_statuss'] : '' ) . '"><a href="?c=ligumi&a=labot&id=' . $data['id'] . '&subtab=1">' . $_vars['googlestatuss'][ $data['google_statuss'] ] . '</a></div>' : '&nbsp;' ?></td>

                    <td class="c s_ligdat"><strong><?= ! empty( $data['ligumadat'] ) ? date( "d.m.Y", strtotime( $data['ligumadat'] ) ) : "" ?></strong></td>
                    <td class="c"><?= empty( $data['ligumanr'] ) ? "" : $data['ligumanr']; ?></td>
                    <td class="s_pardevejs"><a href="?c=darbinieki&a=labot&id=<?= $data['pardeveja_id'] ?>"><?= $_vars['sys_pardeveji'][ $data['pardeveja_id'] ]; ?></a></td>
                    <td class="c s_index"><?= ! empty( $data['faktaddr_index'] ) ? preg_replace( '/^lv\-/i', '', $data['faktaddr_index'] ) : ""; ?></td>
                    <td class="c s_ligsandat"><em><?= ! empty( $data['sanemsanas_datums'] ) ? date( "d.m.Y", strtotime( $data['sanemsanas_datums'] ) ) : "" ?></em></td>
                    <td class="c s_sanforma"><?= ! empty( $_vars['sanemsanas_forma'][ $data['sanemsanas_forma'] ] ) ? '<div class="' . ( ! empty( $_vars['sanemsanas_forma'][ $data['sanemsanas_forma'] ] ) ? 'sanform_statuss_' . $data['sanemsanas_forma'] : '' ) . '">' . $_vars['sanemsanas_forma'][ $data['sanemsanas_forma'] ] . '</div>' : '&nbsp;' ?></td>
                    <td class="c s_avots"><?= ! empty( $_vars['avoti'][ $data['avots'] ] ) ? '<div class="' . ( ! empty( $_vars['avoti'][ $data['avots'] ] ) ? 'avots_statuss_' . $data['avots'] : '' ) . '">' . $_vars['avoti'][ $data['avots'] ] . '</div>' : '&nbsp;' ?></td>

                    <td class="c s_rekno"><strong><?= ! empty( $data['reklama_no'] ) ? date( "d.m.Y", strtotime( $data['reklama_no'] ) ) : "" ?></strong></td>
                    <td class="c s_reklidz"><strong><?= ! empty( $data['reklama_lidz'] ) ? date( "d.m.Y", strtotime( $data['reklama_lidz'] ) ) : "" ?></strong></td>

                    <!-- Pakalpojums -->
					<?php if ( check_access( 'ligumi-pakalpojums-col' ) ) { //  Do not show Pakalpojums data to bookeeper ?>
                        <td>
							<?php echo $data['mpk'] ? '<strong style="color:green">MPK</strong>' : '<strong style="color: blue;">MPT</strong>'; ?><br/><?= $data['pakalpojumu_saraksts'] ?>
							<?php if ( $data['domains'] ) {
								?>
                                <br/><br/><strong>Mājaslapas:</strong><br/>
								

								<?= implode( ",<br />", $data['domains'] ) ?>
							<? } ?>
                        </td>
                        <td class="c s_infostatuss"><?= ! empty( $_vars['infomediastatuss'][ $data['infomedia_statuss'] ] ) ? '<div class="' . ( ! empty( $_vars['infomediastatuss'][ $data['infomedia_statuss'] ] ) ? 'infomedia_statuss_' . $data['infomedia_statuss'] : '' ) . '">' . $_vars['infomediastatuss'][ $data['infomedia_statuss'] ] . '</div>' : '&nbsp;' ?></td>
                        <td class="s_googlekonti">
							<?= empty( $data['google_konti_ar_linkiem'] ) ? "" : $data['google_konti_ar_linkiem']; ?>
							<? if ( ! empty( $data['google_konti'] ) ) { ?>
                                <br/><a class="edit_link" href="#" onclick="showGoogleKontuLinkiDialog(<?= $data['id'] ?>); return false;"><img src="css/edit.png" alt="Labot saites"
                                                                                                                                                title="Labot saites"/></a>
							<? } ?>
                        </td>
                        <td style="white-space: nowrap;"><?= $data['google_atslegv_saraksts'] ?></td>
                        <td><?= isset( $data['pozicijas'] ) ? $data['pozicijas'] : '---'; ?></td>
                        <td style="white-space:nowrap;"><?= isset( $data['adpreview'] ) ? $data['adpreview'] : '---'; ?></td>
                        <!--                        <td style="white-space:nowrap;">--><? //= ( $data['domens'] ? '<a href="http://' . $data['domens'] . '" target="_blank">' . htmlspecialchars( $data['domens'] ) . '</a>' : '' ); ?><!--</td>-->
                        <td style="white-space: nowrap;"><?= isset( $_vars['konta_admin'][ $data['konta_admin'] ] ) ? $_vars['konta_admin'][ $data['konta_admin'] ] : ''; ?></td>
					<?php } ?>
                    <!-- end: Pakalpojums -->

                    <td class="r fullsummwithoutpvn"><strong><?= ! empty( $data['liguma_summa_bez_pvn'] ) ? number_format( $data['liguma_summa_bez_pvn'], 2, '.', '' ) : "&nbsp;"; ?></strong></td>
                    <td class="r s_rekinu_skaits"><?= ! empty( $data['rekinu_skaits'] ) ? $data['rekinu_skaits'] : "&nbsp;"; ?></td>
					<?
					//  Rēķinu saraksts
					if ( empty( $data['rekini'] ) ) {

						echo "<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>";

					} else {

						$col = array( '', '', '', '', '', '', '', '', '' );

						foreach ( $data['rekini'] as $rekins ) {

							$col[0] .= '<span>' . ( $rekins['izr_datums'] ? date( "d.m.Y", strtotime( $rekins['izr_datums'] ) ) : '' ) . '<br></span>';
							$col[1] .= '<span>' . ( ! empty( $rekins['kopsumma'] ) ? number_format( $rekins['kopsumma'], 2, '.', '' ) : "" ) . '<br></span>';
							$col[2] .= '<span>' . $_vars['rekina_statuss'][ $rekins['statuss'] ] . '<br></span>';


							if ( $rekins['apm_statuss'] == 2 && ! empty( $rekins['barteris'] ) ) {
								$col[3] .= '<span style="display: block;" class="apm_statuss_' . $rekins['apm_statuss'] . '">Barteris<br /></span>';
							} else {

								if ( isset( $_vars['apmaksas_statuss'][ $rekins['apm_statuss'] ] ) ) {

									$col[3] .= '<span style="display: block;" class="apm_statuss_' . $rekins['apm_statuss'] . '">';

									$col[3] .= $_vars['apmaksas_statuss'][ $rekins['apm_statuss'] ];

									if ( ! empty( $rekins['atkapsanas_rekins'] ) ) {
										$col[3] .= ' <strong style="font-size: 12px;" title="Atkāpšanās rēķins">(A)</strong>';
									}

									$col[3] .= '</span>';

								} else {
									$col[3] .= '<span style="display: block;">&nbsp;</span>';
								}

								//  Apmaksas datums
								if ( $rekins['pedejais_apm_datums'] ) {
									$col[4] .= '<span style="display: block;">' . date( 'd.m.Y', strtotime( $rekins['pedejais_apm_datums'] ) ) . '</span>';
								} else {
									$col[4] .= '<span style="display: block;">&nbsp;</span>';
								}

							}

							$apm_summa_bez_pvn = $rekins['apm_summa'] / ( 1 + $rekins['pvn'] / 100 );
							//  MPK rāda gan automātisko gan manuālo apm. budžetu, bet mpt tikai tad ja ir manuālais
							if ( $rekins['apm_budzets'] ) {
								$apm_budzets = $rekins['apm_budzets'];
							} else if ( $data['mpk'] ) {
								$apm_budzets = $apm_summa_bez_pvn - $apm_summa_bez_pvn * ( $data['apkalp_maksa_proc'] / 100 );
							} else {
								$apm_budzets = false;
							}

							if ( $apm_budzets !== false ) {
								$col[5] .= '<span style="display: block;">' . number_format( $apm_budzets, 2 ) . '</span>';

							} else {
								$col[5] .= '<span style="display: block;">&nbsp;</span>';
							}


							if ( $rekins['parsk_budzets'] ) {
								$col[6] .= '<span style="display: block;">' . number_format( $rekins['parsk_budzets'], 2 ) . '</span>';
								$col[7] .= '<span style="display: block;">' . date( 'd.m.Y', strtotime( $rekins['bp_apm_datums'] ) ) . '</span>';

							} else {
								$col[6] .= '<span style="display: block;">&nbsp;</span>';
								$col[7] .= '<span style="display: block;">&nbsp;</span>';
							}

							if ( $data['mpk'] && $rekins['apm_statuss'] == 2 ) {

								$rentabil = $rekins['kopsumma'] - $rekins['parsk_budzets'];

								$col[8] .= '<span style="display: block;' . ( $rentabil < 0 ? ' color: red;' : '' ) . '">' . number_format( $rentabil, 2 ) . '</span>';
							} else {
								$col[8] .= '<span style="display: block;">&nbsp;</span>';
							}

						}

						echo '
              <td class="c rek_datumi">' . $col[0] . '</td>
              <td class="r rek_summas">' . $col[1] . '</td>
              <td class="c rek_statusi">' . $col[2] . '</td>
              <td class="c rek_apm_statusi">' . $col[3] . '</td>
              <td class="c rek_apm_datums">' . $col[4] . '</td>
              <td class="r b_apm_summas">' . $col[5] . '</td>
              <td class="r b_parsk_summas">' . $col[6] . '</td>
              <td class="c bp_lvl_apm_datums">' . $col[7] . '</td>
              <td class="c rentabil">' . $col[8] . '</td>';

					}
					$report = false;
					if ( isset( $reports[ $data['id'] ] ) ) {
						$report = $reports[ $data['id'] ];
					}

					if ( $_SERVER["REMOTE_ADDR"] == '213.21.219.21' && $report ) {
						#echo "<pre>".print_r($report, true)."</pre>";
						#die();
					}

					echo '
					<td class="c rentabil2">';
					if ( $_SERVER["REMOTE_ADDR"] == '213.21.219.21' && true ) {
						#echo $data['last_rentabil2']."<- <br />";
					}
					if ( $report and $data['reklama_no'] and $data['reklama_lidz'] ) {
						foreach ( $report as $r ) {
							if ( $_SERVER["REMOTE_ADDR"] == '213.21.219.21' ) {
								echo "<br />Liguma Id: " . $r['liguma_id'] . "<br />";
								echo "Cost: " . $r['Cost'] . "<br />";
							}
							if ( $r['fullreport'] ) {

								$from  = new DateTime( $data['reklama_no'] );
								$to    = new DateTime( $data['reklama_lidz'] );
								$total = intval( $to->diff( $from )->format( '%a' ) ) + 1;
								if ( $today > $to ) {
									$spent = $total;
								} else {
									$spent = intval( $today->diff( $from )->format( '%a' ) ) + 1;
								}
								echo $from->format( 'd.m.Y' ) . ' ' . $to->format( 'd.m.Y' ) . '<br>';
								echo '<span style="white-space:nowrap;">' . intval( $r['Cost'] ) . ' / <b>' . $spent . '</b> * <b>' . $total . '</b> / ' . floatval( $data['liguma_summa_bez_pvn'] ) . ' / 1000000 = ' . round( intval( $r['Cost'] ) / $spent * $total / floatval( $data['liguma_summa_bez_pvn'] ) / 1000000, 4 ) . '</span><br />';
								$fullrentabil = ( round( ( intval( $r['Cost'] ) / $spent * $total / floatval( $data['liguma_summa_bez_pvn'] ) ) / 1000000, 3 ) * 100 );
								echo '<b>' . $fullrentabil . '% </b> ';
								if ( $spent < 30 ) {
									echo '<span style="background-color:yellow; font-weight:bold;" title="Līgums ir jaunāks par 30 dienām">?</span>';
								}

							} else {
								$from  = new DateTime( $r['datums_no'] );
								$to    = new DateTime( $r['datums_lidz'] );
								$spent = intval( $to->diff( $from )->format( '%a' ) ) + 1;
								echo '<br>' . substr( $r['datums_no'], 8, 2 ) . '.' . substr( $r['datums_no'], 5, 2 ) . '.' . substr( $r['datums_no'], 0, 4 ) . ' ' . substr( $r['datums_lidz'], 8, 2 ) . '.' . substr( $r['datums_lidz'], 5, 2 ) . '.' . substr( $r['datums_lidz'], 0, 4 );
								echo '<br><span style="white-space:nowrap;">' . intval( $r['Cost'] ) . ' / <b>' . $spent . '</b> * <b>' . $total . '</b> / ' . floatval( $data['liguma_summa_bez_pvn'] ) . ' / 1000000 = ' . round( intval( $r['Cost'] ) / $spent * $total / floatval( $data['liguma_summa_bez_pvn'] ) / 1000000, 4 ) . '</span><br />';
								$rentabil14 = ( round( ( intval( $r['Cost'] ) / $spent * $total / floatval( $data['liguma_summa_bez_pvn'] ) ) / 1000000, 3 ) * 100 );
								echo '<b>' . $rentabil14 . '%</b> ';
								if ( $fullrentabil < $rentabil14 ) {
									echo '<span style="color:red; font-weight:bold; font-size:21px;">&#8679;</span>';
								} elseif ( $fullrentabil > $rentabil14 ) {
									echo '<span style="color:green; font-weight:bold; font-size:21px;">&#8681;</span>';
								}
								if ( $data['reklama_lidz'] < date( 'Y-m-d' ) ) {
									echo '<br><b>Līgums beidzies</b>';
								}
								echo '<br><br>Atjaunots: ' . $r['laiks'];
							}
						}
					} else {

						if ( $_SERVER["REMOTE_ADDR"] == '213.21.219.21' ) {
							echo $report ? "1" : "0";
							##echo "<br />";
							##echo $data['reklama_no']."!!<br />";
							#echo $data['reklama_lidz']."!!<br />";

						}


					}
					echo '</td>';
					?>

                    <td class="c prov_percent" style="white-space: nowrap;">
                        <span><?= round( (int) $data['atlidziba_vaditajam'] && $data['mpk'] ? $data['atlidziba_vaditajam'] : $data['provizija_pardevejam'], 2 ) ?></span>%
                    </td>

                    <td class="c">
						<?= ( ! empty( $data['aktivi_atgadinajumi'] ) ) ? 'Ir (' . $data['aktivi_atgadinajumi'] . ')' : 'Nav' ?>
                    </td>
                </tr>

				<? $npk --; ?>

			<?php } ?>

            </tbody>

            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td valign="top" align="right">
                    <div style="position: absolute; width: 300px; text-align: right; margin-left: -310px;">
                        <b>Līgumu summa (bez PVN):</b><br>
                        <b>Līgumu skaits:</b><br>
                        Vidējā līguma summa faktiskā:<br>
                        Vidējā līguma summa gada griezumā:<br>
                        Dienas budžets:<br>
                        Dienas izmaksas:<br>
                        Dienas budžeta procenta likme:
                    </div>
                    <span id="ligumusumma"><strong>EUR&nbsp;<?= number_format( $totals['ligumu_summa'], 2, '.', '' ) ?></strong></span><br>
                    <span id="ligumuskaits"><strong><?= $total_rows; ?></strong></span><br>
                    <span id="ligumavidejais"><strong>EUR&nbsp;<?= number_format( ! empty( $total_rows ) ? $totals['ligumu_summa'] / $total_rows : 0, 2, '.', '' ) ?></strong></span><br>
                    <span id="ligvidgad"><strong>EUR&nbsp;<?= number_format( ! empty( $total_rows ) ? $totals['lig_vid_summa'] / $total_rows : 0, 2, '.', '' ) ?></strong></span><br>
                    <span id="ligvidday" style="font-weight: bold;">EUR&nbsp;<?= number_format( $totals['lig_vid_day_summa'], 2, '.', '' ) ?></span><br>
                    <span id="ligvidday" style="font-weight: bold;"
                          title="<?= htmlspecialchars( substr( $totals['pieslegto_rentabil14_title'], 0, - 3 ) ); ?>">EUR&nbsp;<?= number_format( $totals['pieslegto_rentabil14'], 2, '.', '' ) ?>
                        (<?= ( $totals['lig_vid_day_summa'] != 0 ? number_format( $totals['pieslegto_rentabil14'] / $totals['lig_vid_day_summa'] * 100, 1, '.', '' ) : '0' ); ?>%)</span><br>
                    <input type="input" id="dienbudzproc" value="100">
                </td>
                <td></td>
                <td></td>
                <td valign="top" align="right">
                    <div style="position: absolute; width: 100px; text-align: right; margin-left: -110px;">
                        <b style="background: #F2F2F2;">Rēķinu<br/>summa:</b>
                    </div>
                    <br/>EUR&nbsp;<?= number_format( $totals['rek_summa'], 2, '.', '' ) ?>
                </td>
                <td></td>
                <td></td>
                <td style="vertical-align: top; text-align: right;">
                    <strong>Budžets kopā:</strong>
                </td>
                <td style="vertical-align: top; text-align: right;">
                    EUR <?php echo number_format( $totals['b_apm_summa'], 2, '.', '' ); ?>
                </td>
                <td style="vertical-align: top; text-align: right;">EUR <?php echo number_format( $totals['b_parsk_summa'], 2, '.', '' ); ?></td>
                <td></td>
                <td style="vertical-align: top; text-align: right;">EUR <?php echo number_format( $totals['rentabil_summa'], 2, '.', '' ); ?></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>

        </table>

		<?php print_paginator( $total_rows, $active_page, $show_in_page ) ?>

    </form>

<? // Prototypes // ?>
    <div style="display:none;">

        <div id="komentarsprot__lig_kopsavilkums">
            <table class="comment_form">
                <tr>
                    <td width="70">Komentārs:</td>
                    <td>
                        <textarea name="komentari_new[_lig_kopsavilkums][komentars][]" rows="2" cols="30"></textarea>
                    </td>
                </tr>
            </table>
        </div>

    </div>

    <form action="?c=ligumi" method="post" enctype="multipart/form-data" style="clear: both;">

		<? show_comments_block( '_lig_kopsavilkums', null, true, is_limited() ) ?>

    </form>

    <script type="text/javascript">

        //    Mēnešu filtri

        $('#sanemts_month').monthpicker({
            pattern: 'mm.yyyy', // Default is 'mm/yyyy' and separator char is not mandatory
            selectedYear: <?php echo isset( $flig_s_y ) ? $flig_s_y : date( 'Y' );?>,
            startYear: 2009,
            finalYear: <?php echo date( 'Y' );?>,
            monthNames: ['Jan', 'Feb', 'Mar', 'Apr', 'Mai', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dec']
        }).bind('monthpicker-click-month', function (e, month) {
            $('#fullformplace').submit();
        });

        $('#sak_month').monthpicker({
            pattern: 'mm.yyyy', // Default is 'mm/yyyy' and separator char is not mandatory
            selectedYear: <?php echo isset( $flig_sak_y ) ? $flig_sak_y : date( 'Y' );?>,
            startYear: 2009,
            finalYear: <?php echo date( 'Y' );?>,
            monthNames: ['Jan', 'Feb', 'Mar', 'Apr', 'Mai', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dec']
        }).bind('monthpicker-click-month', function (e, month) {
            $('#fullformplace').submit();
        });

        $('#beig_month').monthpicker({
            pattern: 'mm.yyyy', // Default is 'mm/yyyy' and separator char is not mandatory
            selectedYear: <?php echo isset( $flig_beig_y ) ? $flig_beig_y : date( 'Y' );?>,
            startYear: 2009,
            finalYear: <?php echo date( 'Y' );?>,
            monthNames: ['Jan', 'Feb', 'Mar', 'Apr', 'Mai', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dec']
        }).bind('monthpicker-click-month', function (e, month) {
            $('#fullformplace').submit();
        });

        $('#izr_month').monthpicker({
            pattern: 'mm.yyyy', // Default is 'mm/yyyy' and separator char is not mandatory
            selectedYear: <?php echo isset( $frek_izr_y ) ? $frek_izr_y : date( 'Y' );?>,
            startYear: 2009,
            finalYear: <?php echo date( 'Y' );?>,
            monthNames: ['Jan', 'Feb', 'Mar', 'Apr', 'Mai', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dec']
        }).bind('monthpicker-click-month', function (e, month) {
            $('#fullformplace').submit();
        });

        $('#apm_month').monthpicker({
            pattern: 'mm.yyyy', // Default is 'mm/yyyy' and separator char is not mandatory
            selectedYear: <?php echo isset( $frek_apm_y ) ? $frek_apm_y : date( 'Y' );?>,
            startYear: 2009,
            finalYear: <?php echo date( 'Y' );?>,
            monthNames: ['Jan', 'Feb', 'Mar', 'Apr', 'Mai', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dec']
        }).bind('monthpicker-click-month', function (e, month) {
            $('#fullformplace').submit();
        });

        $('#b_apm_month').monthpicker({
            pattern: 'mm.yyyy', // Default is 'mm/yyyy' and separator char is not mandatory
            selectedYear: <?php echo isset( $fb_apm_y ) ? $fb_apm_y : date( 'Y' );?>,
            startYear: 2009,
            finalYear: <?php echo date( 'Y' );?>,
            monthNames: ['Jan', 'Feb', 'Mar', 'Apr', 'Mai', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dec']
        }).bind('monthpicker-click-month', function (e, month) {
            $('#fullformplace').submit();
        });


        $(document).ready(function () {
            // $(window).on(function () {

            calcDienasBudzets();

            $('#dienbudzproc').keyup(function () {

                if ($('#dienbudzproc').val() > 0) {
                    calcDienasBudzets();
                }

            });

            initPlainSortableTable($('#fullformplace'));

            initActAtgTooltips();

            $('.saist_pard_ico').wTooltip({
                follow: true,
                offsetY: 0,
                offsetX: 10
            });

            initTableCols($('#ligumu_kopsavilkums'), 'lig_kopsavilkums', <?= Zend_Json::encode( get_tab_kolonnas( 'lig_kopsavilkums' ) ) ?>);

            initTableGroupColors($('#ligumu_kopsavilkums'));

            //  Darbinieku filtra "grupām" toggle hack
            $("#fpardevejs input[type=checkbox]").on('change', function (e) {
                if ($(e.target).parent().text()[0] != "-") {
                    $(e.target).parent().nextAll().each(function () {
                        t2 = $(this).text()[0];
                        if ($(this).text()[0] == "-") {
                            var checkbox = $(this).find('input');
                            checkbox.prop("checked", $(e.target).prop('checked'));
                        } else {
                            return false;
                        }
                    });
                }
            });

        });

        function initActAtgTooltips() {

            var positionH = function (td) {

                var position = td.position();

                var h = td.find('.act_atg_h');

                h.css('left', position.left + td.outerWidth() - 15)
                    .css('top', position.top);

            }

            $('td.s_nosaukums.has_act_atg').each(function () {

                var td = $(this);

                var h = td.find('.act_atg_h');

                h.css('width', '15px')
                    .css('height', '12px')
                    .css('position', 'absolute')
                    .css('cursor', 'pointer');

                positionH(td);

                var content = td.find('div.atgadinajumi').html();

                h.wTooltip({
                    content: content,
                    follow: true,
                    offsetY: 0,
                    offsetX: 10
                });

                td.hover(function () {
                    positionH($(this));
                });

            });

        }

        function calcDienasBudzets() {

            // update footer

            var ligviddaysum = <?= $totals['lig_vid_day_summa'] ?>;

            var koif = $('#dienbudzproc').val();

            $('#ligvidday').html('EUR&nbsp;' + parseFloat(ligviddaysum * (koif / 100)).toFixed(2));

        }

        function toggleIpasais(img, liguma_id) {

            var img = $(img);

            if (img.is('.toggle_ipasais_izcelts')) {

                img.attr('src', 'css/star_small_dis.png');
                img.removeClass('toggle_ipasais_izcelts');

            } else {

                img.attr('src', 'css/star_small.png');
                img.addClass('toggle_ipasais_izcelts');

            }

            $.get('ajax.php?action=toggle_ipasais', {liguma_id: liguma_id}, function (result) {
            });

        }

    </script>

<?php
function parseKonts( $konts_string, $site, $kontu_linki ) {
	$konti            = array();
	$konti_ar_linkiem = array();

	foreach ( array_filter( array_map( 'trim', preg_split( "/[,;]/", trim( strtolower( $konts_string ) ) ) ) ) as $konts ) {

		if ( ! in_array( $konts, $konti ) ) {
			$konti[] = $konts;
		}

		$legacy_site = $site == 'google' ? 'tiessaite' : $site;

		if ( isset( $kontu_linki[ $legacy_site ][ $konts ] ) && ! empty( $kontu_linki[ $legacy_site ][ $konts ]['links'] ) ) {
			$konti_ar_linkiem[] = '<a target="_blank" href="' . $kontu_linki[ $legacy_site ][ $konts ]['links'] . '" style="white-space: nowrap;">' . $konts . '</a>';
		} else {
			$konti_ar_linkiem[] = '<span style="white-space: nowrap;">' . $konts . '</span>';
		}
	}

	return array( $konti, $konti_ar_linkiem );
}

?>