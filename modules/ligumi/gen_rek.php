<?
ini_set('display_errors', '1');
error_reporting(E_ALL);
if (!check_access('ligumi-gen-rek')) {
  die();
}

require_once("Zend/Pdf.php");

if (empty($_GET['rek_id'])) {
  die();
}

$rek_id = (int) $_GET['rek_id'];

if (empty($_GET['tips']) || !in_array($_GET['tips'], array('avansa', 'gala'))) {
  die();
}
$tips = $_GET['tips'];

$rek_data = db_get_assoc(db_query("
  SELECT *
  FROM `".DB_PREF."rekini`
  WHERE id = ".$rek_id."
"));

$lig_data = db_get_assoc(db_query("
  SELECT *
  FROM `".DB_PREF."ligumi`
  WHERE id = ".$rek_data['liguma_id']."
"));

if ($tips == 'avansa') {

  if (!empty($rek_data['fails_avansa_ep'])) {

    db_query("DELETE FROM `".DB_PREF."faili` WHERE `id` = ".(int)$rek_data['fails_avansa_ep']);
    db_query("UPDATE `".DB_PREF."rekini` SET `fails_avansa_ep` = NULL WHERE `id` = ".$rek_data['id']." LIMIT 1");

    if (is_file("faili/".(int)$rek_data['fails_avansa_ep'])) {
      unlink("faili/".(int)$rek_data['fails_avansa_ep']);
    }

  }

  if (!empty($rek_data['fails_avansa_pp'])) {

    db_query("DELETE FROM `".DB_PREF."faili` WHERE `id` = ".(int)$rek_data['fails_avansa_pp']);
    db_query("UPDATE `".DB_PREF."rekini` SET `fails_avansa_pp` = NULL WHERE `id` = ".$rek_data['id']." LIMIT 1");

    if (is_file("faili/".(int)$rek_data['fails_avansa_pp'])) {
      unlink("faili/".(int)$rek_data['fails_avansa_pp']);
    }

  }

} else {

  if (!empty($rek_data['fails_gala_ep'])) {

    db_query("DELETE FROM `".DB_PREF."faili` WHERE `id` = ".(int)$rek_data['fails_gala_ep']);
    db_query("UPDATE `".DB_PREF."rekini` SET `fails_gala_ep` = NULL WHERE `id` = ".$rek_data['id']." LIMIT 1");

    if (is_file("faili/".(int)$rek_data['fails_gala_ep'])) {
      unlink("faili/".(int)$rek_data['fails_gala_ep']);
    }

  }

  if (!empty($rek_data['fails_gala_pp'])) {

    db_query("DELETE FROM `".DB_PREF."faili` WHERE `id` = ".(int)$rek_data['fails_gala_pp']);
    db_query("UPDATE `".DB_PREF."rekini` SET `fails_gala_pp` = NULL WHERE `id` = ".$rek_data['id']." LIMIT 1");

    if (is_file("faili/".(int)$rek_data['fails_gala_pp'])) {
      unlink("faili/".(int)$rek_data['fails_gala_pp']);
    }

  }

}

$izr_datums = date('d.m.Y', strtotime($rek_data['izr_datums']));

if ($tips == 'gala' && !empty($_GET['izr_datums'])) {
  $izr_datums = $_GET['izr_datums'];
}

$apm_termins = date('d.m.Y', strtotime($rek_data['apm_termins']));

if ($tips == 'gala' && !empty($_GET['apm_termins'])) {
  $apm_termins = $_GET['apm_termins'];
}

$ligums = Ligums::model()->findByPk($rek_data['liguma_id']);

$query = db_query("
  SELECT *
  FROM `".DB_PREF."ligumi_rekinu_piegades`
  WHERE liguma_id = ".$rek_data['liguma_id']."
");

$piegades = array();

$piegades_adrese = '';

while($row = db_get_assoc($query)) {

  $piegades[$row['piegades_veids']] = $row;

  if ($row['piegades_veids'] == 2) {
    $piegades_adrese = $row['adrese'];
  }

}

if (empty($piegades_adrese)) {

  $piegades_adrese = array_filter(array_map('trim', array($lig_data['faktaddr_street'], $lig_data['faktaddr_city'], !empty($lig_data['faktaddr_index']) ? $lig_data['faktaddr_index'] : '')));
  $piegades_adrese = implode(', ', $piegades_adrese);

}

$autodownload = array();

foreach($piegades as $piegades_veids => $piegade) {

  if ($piegades_veids != 1 && $piegades_veids != 2) {
    continue;
  }

  $default_font_size = 10;

  $pdf = Zend_Pdf::load($tips == 'avansa' ? 'rek_templates/klientu_avansa.pdf' : 'rek_templates/klientu.pdf');

  $font = Zend_Pdf_Font::fontWithPath('rek_templates/fonts/calibri.ttf');
  $font_bold = Zend_Pdf_Font::fontWithPath('rek_templates/fonts/calibrib.ttf');

  $page = $pdf->pages[0];

  $ph = 841.8;

  $page->setFont($font_bold, 14);

  if ($tips == 'avansa') {
    $page->drawText('Avansa rēķins', 262, $ph - 74, 'UTF-8');
  } else {
    $page->drawText('Rēķins', 282, $ph - 74, 'UTF-8');
  }

  $page->setFont($font_bold, 11);

  //

  $page->drawText($lig_data['nosaukums'], 86, $ph - 118, 'UTF-8');

  if (!empty($piegades_adrese)) {

    $start_at = $ph - 132;

    $parts = array_filter(trim_array(explode("\n", wordwrap($piegades_adrese, 30, "\n", true))));

    foreach($parts as $line) {
      $page->drawText($line, 86, $start_at, 'UTF-8');
      $start_at -= 14;
    }

  }
	
	include('modules/ligumi/rekinu_rekviziti.php');
  $from='infomedia';
	if (isset($_REQUEST['from']) and $_REQUEST['from']=='leads'){
		$from='leads';
	}

  $page->setFont($font, $default_font_size);

  $page->drawText($rekinu_rekviziti[$from]['nosauk'], 423, $ph - 118, 'UTF-8');
  $page->drawText($rekinu_rekviziti[$from]['regnr'], 423, $ph - 132.5, 'UTF-8');
  $page->drawText($rekinu_rekviziti[$from]['pvnnr'], 423, $ph - 147, 'UTF-8');
  $page->drawText(REKV_JUR_ADR, 423, $ph - 161.5, 'UTF-8');
  $page->drawText(REKV_BANKA, 423, $ph - 176, 'UTF-8');
  $page->drawText(REKV_SWIFT, 423, $ph - 190.5, 'UTF-8');
	
  //  Konts
  $bank_account = getBankAccount($rek_data, $lig_data,$from);

  $page->drawText($bank_account, 423, $ph - 205, 'UTF-8');

  //

  $page->drawText($lig_data['nosaukums'], 129, $ph - 233.5, 'UTF-8');
  $page->drawText($lig_data['regnr'], 129, $ph - 248.5, 'UTF-8');
  $page->drawText($lig_data['pvnnr'], 129, $ph - 263, 'UTF-8');

  $jur_addr = implode(', ', array_filter(array($lig_data['juridaddr_street'], $lig_data['juridaddr_city'], $lig_data['juridaddr_index'])));
  $page->drawText($jur_addr, 129, $ph - 277.5, 'UTF-8'); //

  //

  $page->setFont($font_bold, $default_font_size);

  $page->drawText($rek_data['rek_nr'], 129, $ph - 306.5, 'UTF-8');

  $page->setFont($font, $default_font_size);

  $page->drawText($izr_datums, 129, $ph - 321, 'UTF-8');

  $page->setFont($font_bold, $default_font_size);

  $page->drawText(date('d.m.Y', strtotime($apm_termins)), 129, $ph - 335.5, 'UTF-8');

  $page->setFont($font, $default_font_size);

  if (preg_match('/.+\/(\d)+$/i', $rek_data['rek_nr'], $m)) {
    $txt = $m[1] . '. maksājums ';
  } else {
    $txt = 'Maksājums ';
  }
  $txt .= sprintf('saskaņā ar līgumu Nr. %s no %s', $lig_data['ligumanr'], date('d.m.Y', strtotime($lig_data['ligumadat'])));
  $page->drawText($txt, 129, $ph - 350.5, 'UTF-8');

  //

  if (!empty($rek_data['atkapsanas_rekins'])) {
    $page->drawText(sprintf(LIG_REK_ATKAPS_PAMATOJUMS, $lig_data['ligumanr']), 35, $ph - 404.5, 'UTF-8');
  } else {
    $page->drawText($ligums->getBillText(), 35, $ph - 404.5, 'UTF-8');
  }

  $page->drawText('gab.', 320, $ph - 404.5, 'UTF-8');
  $page->drawText('1', 385, $ph - 404.5, 'UTF-8');
  $page->drawText(format_currency($rek_data['summa'], true), 440, $ph - 404.5, 'UTF-8');
  $page->drawText(format_currency($rek_data['summa'], true), 515, $ph - 404.5, 'UTF-8');

  //

  $page->drawText(round($rek_data['pvn']) . '%', 468.5, $ph - 434.5, 'UTF-8');

  $page->drawText(format_currency($rek_data['summa'], true), 515, $ph - 419.5, 'UTF-8');
  $page->drawText(format_currency($rek_data['kopsumma'] - $rek_data['summa'], true), 515, $ph - 434, 'UTF-8');

  $page->setFont($font_bold, $default_font_size);

  $page->drawText(format_currency($rek_data['kopsumma'], true), 515, $ph - 448.5, 'UTF-8');
  //$page->drawText(format_currency(round($rek_data['kopsumma']*get_config('lvl_eur'),2), true), 527, $ph - 448.5, 'UTF-8');

  //$page->drawText('EUR', 515, $ph - 463, 'UTF-8');
  //$page->drawText('LVL', 527, $ph - 463, 'UTF-8');

  //

  if($tips == 'avansa') {
    $page->setFont($font, $default_font_size);

    $page->drawText(LIG_REK_INFO1, 35, $ph - 494, 'UTF-8');

    $page->setFont($font_bold, $default_font_size);

    $page->drawText(LIG_REK_INFO2, 35, $ph - 508, 'UTF-8');
  }
  $page->setFont($font, $default_font_size);

  $page->drawText(LIG_REK_INFO3, 35, $ph - 522, 'UTF-8');

  //

  if ($piegades_veids == 1) {

    $page->drawText('Šis rēķins ir sagatavots elektroniski un derīgs bez paraksta un zīmoga.', 35, $ph - 610, 'UTF-8');

  } else {

    $page->drawText('Valdes loceklis', 35, $ph - 624, 'UTF-8');
    $page->drawText('_____________________', 128, $ph - 628, 'UTF-8');
    $page->drawText('/' . LIG_REK_VALDES_LOCEKLIS . '/', 158, $ph - 639, 'UTF-8');

  }

  //

  $page->drawText($rekinu_rekviziti[$from]['nosauk'], 35, $ph - 768.3, 'UTF-8');
  $page->drawText(REKV_FIZ_ADR, 35, $ph - 782.7, 'UTF-8');

  $page->drawText(LIG_REK_TALR, 319, $ph - 768.3, 'UTF-8');
  $page->drawText(LIG_REK_FAX, 319, $ph - 782.7, 'UTF-8');

  $page->drawText(LIG_REK_EPASTS, 474, $ph - 768.3, 'UTF-8');
  $page->drawText(LIG_REK_WWW, 474, $ph - 782.7, 'UTF-8');

  //

  $pdf_output = $pdf->render();

  $name = ($tips == 'avansa' ? 'AVANSA REKINS ' : 'REKINS ') . $rek_data['rek_nr'] . ($piegades_veids == 1 ? ' E' : ' P') . '.pdf';

  $name = prepare_filename($name);

  db_query("
    INSERT INTO `".DB_PREF."faili` SET
      `objekta_id` = ".$rek_data['liguma_id'].",
      `objekta_tips` = 'lig_rekini',
      `pardeveja_id` = ".$_SESSION['user']['id'].",
      `fails` = '".esc($name)."',
      `izmers` = ".strlen($pdf_output)."
  ");

  if(!save_file($pdf_output, "faili/" . db_last_id())) {

    db_query("DELETE FROM `".DB_PREF."faili` WHERE `id` = ".db_last_id());

  } else {

    $autodownload[] = db_last_id();

    if ($tips == 'avansa') {
      $field = ($piegades_veids == 1) ? 'fails_avansa_ep' : 'fails_avansa_pp';
    } else {
      $field = ($piegades_veids == 1) ? 'fails_gala_ep' : 'fails_gala_pp';
    }

    db_query("
      UPDATE `".DB_PREF."rekini`
      SET `".$field."` = ".db_last_id()."
      WHERE `id` = ".$rek_data['id']."
      LIMIT 1
    ");

  }

}

$_SESSION['autodownload_file_ids'] = $autodownload;

header("Location: ?c=ligumi&a=labot&id=" . $rek_data['liguma_id'] . "&subtab=2");
die();
?>