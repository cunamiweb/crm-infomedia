<?php
$subtab = !empty($_GET['subtab']) ? (int)$_GET['subtab'] : 0;

if (empty($_GET['id']) && !check_access('ligumi-jauns')) {   // pievienot jaunu ligumu var tikai pilnais admins
  die();
}

$pc_labot_pak = check_access('ligumi-labot-pilns') ? 'canedit' : 'always_disabled';

if (!empty($_GET['id'])){
  //  Labo esošu līgumu

  $ligums = Ligums::model()->findByPk((int)$_GET['id']);

  //  Legacy
  $query = db_query("SELECT * FROM `".DB_PREF."ligumi` WHERE `id` = ".(int)$_GET['id']);
  $d = db_get_assoc($query);

  $d = htmlesc($d);

  $saistitais_ligums = db_get_val(db_query("SELECT COUNT(*) FROM `".DB_PREF."ligumi_saistitie_pardeveji` WHERE `pardeveja_id` = " . $_SESSION['user']['id'] . " AND liguma_id = " . (int)$_GET['id']));

  if (!check_access('ligumi-saraksts') && $d['pardeveja_id'] != $_SESSION['user']['id'] && empty($saistitais_ligums)) {
    die('Nav tiesību apskatīt līgumu!');
  }

  $query = db_query("
    SELECT
      *,
      (
        SELECT datums
        FROM `".DB_PREF."rekini_maksajumi`
        WHERE rekina_id = r.id
        ORDER BY datums DESC
        LIMIT 1
      ) as pedejais_apm_datums,
      (
        SELECT apm_datums
        FROM `".DB_PREF."rekini_budzeta_parskaitijumi`
        WHERE rekina_id = r.id
        ORDER BY apm_datums DESC
        LIMIT 1
      ) as pedejais_b_apm_datums
    FROM `".DB_PREF."rekini` r
    WHERE r.`liguma_id` = ".(int)$_GET['id'] . "
    ORDER BY r.id ASC");
  $r = array();

  while($row = db_get_assoc($query)) {
    $r[] = htmlesc($row);
  }

  $liguma_statuss = get_liguma_statuss($d, $r);

  $liguma_beigu_statuss = db_get_assoc(db_query("SELECT * FROM `".DB_PREF."ligumi_beigusies` WHERE `liguma_id` = " . (int)$_GET['id']));

  require('new_process.php');

  log_add("atvera", "Atvēra līgumu " . $d['ligumanr']);

  $query = db_query("SELECT * FROM `".DB_PREF."ligumi_rekinu_piegades` WHERE `liguma_id` = ".(int)$_GET['id']);
  $piegades = array();

  while($row = db_get_assoc($query)) {
    $piegades[$row['piegades_veids']] = htmlesc($row);
  }

  $query = db_query("SELECT * FROM `".DB_PREF."kontakti` WHERE `liguma_id` = ".(int)$_GET['id']);
  $kontakti = array();

  while($row = db_get_assoc($query)){
    $kontakti[] = htmlesc($row);
  }

  $query = db_query("
    SELECT
      p.*,
      v.parent_id
    FROM `".DB_PREF."pakalpojumi` p
    LEFT JOIN `".DB_PREF."pakalpojumi_veidi` v ON (v.id = p.pakalpojuma_veida_id)
    WHERE p.`liguma_id` = ".(int)$_GET['id'] . "
    ORDER BY
      v.`parent_id` ASC,
      p.`id` ASC
  ");
  $pakalpojumi = array();

  while($row = db_get_assoc($query)){
    $pakalpojumi[$row['parent_id']][] = htmlesc($row);
  }

  $query = db_query("SELECT * FROM `".DB_PREF."pielikumi_infomedia` WHERE `liguma_id` = ".(int)$_GET['id'] . " ORDER BY id ASC");
  $pielikumi_infomedia = array();

  while($row = db_get_assoc($query)){
    $pielikumi_infomedia[] = htmlesc($row);
  }

  $query = db_query("SELECT * FROM `".DB_PREF."pielikumi_tiessaite` WHERE `liguma_id` = ".(int)$_GET['id'] . " ORDER BY id ASC");
  $pielikumi_tiessaite = array();

  while($row = db_get_assoc($query)){
    $pielikumi_tiessaite[] = htmlesc($row);
  }

  $query = db_query("SELECT * FROM `".DB_PREF."pielikumi_yandex` WHERE `liguma_id` = ".(int)$_GET['id'] . " ORDER BY id ASC");
  $pielikumi_yandex = array();

  while($row = db_get_assoc($query)){
    $pielikumi_yandex[] = htmlesc($row);
  }

  $query = db_query("SELECT * FROM `".DB_PREF."pielikumi_citi` WHERE `liguma_id` = ".(int)$_GET['id'] . " ORDER BY id ASC");
  $pielikumi_citi = array();

  while($row = db_get_assoc($query)){
    $pielikumi_citi[] = htmlesc($row);
  }

  $query = db_query("
    SELECT *
    FROM `".DB_PREF."atgadinajumi`
    WHERE
      `liguma_id` = ".(int)$_GET['id']." AND
      `redzamiba` IN ('kopejs', ".(is_admin() ? "'admin'" : "'pardevejs'").")
  ");

  $atgadinajumi = array();

  while($row = db_get_assoc($query)){
    $atgadinajumi[$row['id']] = htmlesc($row);
  }

  $query = db_query("SELECT * FROM `".DB_PREF."ligumi_saistitie_pardeveji` WHERE `liguma_id` = ".(int)$_GET['id']);
  $saistitie_pardeveji = array();

  while($row = db_get_assoc($query)){
    $saistitie_pardeveji[] = htmlesc($row);
  }

  $query = db_query("
    SELECT
      r.*,
      g.nosaukums as liguma_nosaukums,
      g.ligumanr as liguma_ligumanr
    FROM `".DB_PREF."ligumi_parslegsanu_rinda` r
    LEFT JOIN `".DB_PREF."ligumi` g ON (g.id = r.jaunais_liguma_id)
    WHERE r.`liguma_id` = ".(int)$_GET['id']
  );
  $parslegsanas_rinda = array();

  while($row = db_get_assoc($query)){
    $parslegsanas_rinda[] = htmlesc($row);
  }

  $akt_atg_skaits = db_get_val(db_query("
    SELECT COUNT(*)
    FROM `".DB_PREF."atgadinajumi`
    WHERE
      liguma_id = ".(int)$_GET['id']." AND
      statuss = 1 AND
      datums <= CURDATE() AND
      redzamiba IN ('kopejs', ".(is_admin() ? "'admin'" : "'pardevejs'").")
  "));

  if (!empty($_SESSION['show_parslegs_dialog'])) {

    unset($_SESSION['show_parslegs_dialog']);

    if (!empty($d['regnr'])) {

      $parslegs_dialog_contracts_ids = array();

      $prev_contracts = db_get_assoc_all(db_query("
        SELECT
          g.*,
          b.beigu_statuss
        FROM `".DB_PREF."ligumi` g
        LEFT JOIN `".DB_PREF."ligumi_beigusies` b ON (g.id = b.liguma_id)
        WHERE
          g.`regnr` = '".$d['regnr']."' AND
          g.`id` != ".$d['id']."
      "));

      if (!empty($prev_contracts)) {

        foreach($prev_contracts as $prev_contract) {

          $prev_contract_rekini = db_get_assoc_all(db_query("SELECT * FROM `".DB_PREF."rekini` WHERE `liguma_id` = '".$prev_contract['id']."' ORDER BY izr_datums DESC"));

          $stat = get_liguma_statuss($prev_contract, $prev_contract_rekini);

          // Rādam tikai aktīvos, vai tos, kas beigušies un ar statusu termins vai atteikums

          if ($stat == 3 || ($stat == 6 && (empty($prev_contract['beigu_statuss']) || in_array($prev_contract['beigu_statuss'], array('termins', 'atteikums'))))) {
            $parslegs_dialog_contracts_ids[] = $prev_contract['id'];
          }

        }

      }

    }

  }

  $ligumu_grupa = db_get_assoc(db_query("
    SELECT
      r.*,
      (
        SELECT g.nosaukums
        FROM `".DB_PREF."ligumi` g
        LEFT JOIN `".DB_PREF."ligumi_grupas` r2 ON (r2.liguma_id = g.id)
        WHERE r2.grupas_regnr = r.grupas_regnr
        ORDER BY IF(g.`sanemsanas_datums` IS NULL, 0, 1) ASC, g.`sanemsanas_datums` DESC, g.`ligumanr` DESC
        LIMIT 1
      ) as klienta_nosaukums
    FROM `".DB_PREF."ligumi_grupas` r
    WHERE r.`liguma_id` = " . (int)$_GET['id']
  ));

} else {

  //  Jauns līgums

  $ligums = new Ligums;

  require('new_process.php');

  $d = array();
  $d['soda_proc'] = "0.5";
  $d['auto_summas'] = 1;
  $d['saistits_pie'] = '';

  $r = array();
  $piegades = array();
  $kontakti = array();
  $pakalpojumi = array();
  $pielikumi_infomedia = array();
  $pielikumi_tiessaite = array();
  $pielikumi_yandex = array();
  $pielikumi_citi = array();
  $atgadinajumi = array();
  $saistitie_pardeveji = array();
  $parslegsanu_rinda = array();

  $akt_atg_skaits = 0;

}
?>
<iframe src="" style="display:none;" id="downloadframe" name="downloadframe"></iframe>

<? show_autodownload_iframes() ?>

<?php if (!empty($parslegs_dialog_contracts_ids)) {  ?>
  <script>
    showLigumaAutoParslegsanasDialog(<?php echo $_GET['id'] ?>);
  </script>
<?php } ?>

<?php if (!empty($_SESSION['show_status_change'])) {  ?>
  <script>
    showLigumaBeiguStatussDialog(<?php echo $_GET['id'] ?>);
    alert('Saistītais pārdevējs tiks pievienots, ja nomainīsiet līguma beigu statusu uz Atteikums.');
  </script>
  <?php unset($_SESSION['show_status_change']) ?>
<?php } ?>

<? // Prototypes // ?>
<div style="display:none;">

  <div id="kontprot">
  	<div style="margin-top: 15px;">
	    <div><span class="sublabel">Līguma slēdzējs:</span> <input style="width: auto;" class="ligmasledzejcheck" type="checkbox" name="kontakti_new[ligumsledzejs][]"></div>
	    <div class="clear"><span class="sublabel">Vārds:</span> <input type="input" name="kontakti_new[vards][]" value=""></div>
	    <div><span class="sublabel">Tālrunis:</span> <input type="input" name="kontakti_new[telefons][]" value=""></div>
	    <div><span class="sublabel">E-pasts:</span> <input class="readthismail" type="input" name="kontakti_new[epasts][]" value=""></div>
	    <div class="clear" style="text-align: right;">
	      <button onClick="$(this).parent().parent().remove(); return false;" class="ui-state-default ui-corner-all">Dzēst šo kontaktu</button>
	    </div>
		</div>
  </div>

  <table id="tiessaite_av_prot">
    <tr class="row">
      <td width="200"><input type="input" name="piel_tiessaite_new[atslegvards][]" style="width:170px;"></td>
      <td width="400"><?php echo nozare_select_box('piel_tiessaite_new[nozare_id][]', 0); ?></td>
      <td width="40">
        <input type="hidden" name="piel_tiessaite_new[top3][]" value="0">
        <input type="checkbox" name="piel_tiessaite_new[top3_setter][]" onclick="$(this).prev().val($(this).is(':checked') ? 1 : 0)">
      </td>
      <td width="40"></td>
      <td width="20"><a href="#" onclick="delAtslegv('tiessaite', this); return false;"><img src="css/del.png" width="16" height="16" alt="Dzēst" title="Dzēst" /></a></td>
    </tr>
  </table>

  <table id="tiessaite_av_domeni_prot">
    <tr class="row">
      <td><input type="input" name="piel_tiessaite_new[domeni][]" style="width:93%;" value="lv"></td>
    </tr>
  </table>

  <table id="tiessaite_av_konts_prot">
    <tr class="row">
      <td><input type="input" name="piel_tiessaite_new[google_konts][]" style="width: 93%;"></td>
    </tr>
  </table>

  <table id="infomedia_av_prot">
    <tr class="row">
      <td width="200"><input type="input" name="piel_infomedia_new[atslegvards][]" style="width:170px;"> </td>
      <td width="400"><?php echo nozare_select_box('piel_infomedia_new[nozare_id][]', 0); ?></td>
      <td width="40"></td>
      <td width="40">
        <input type="hidden" name="piel_infomedia_new[atra_saite][]" value="0">
        <input type="checkbox" name="piel_infomedia_new[atra_saite_setter][]" onclick="$(this).prev().val($(this).is(':checked') ? 1 : 0)">
      </td>
      <td width="20"><a href="#" onclick="delAtslegv('infomedia', this); return false;"><img src="css/del.png" width="16" height="16" alt="Dzēst" title="Dzēst" /></a></td>
    </tr>
  </table>

  <table id="infomedia_av_konts_prot">
    <tr class="row">
      <td><input type="input" name="piel_infomedia_new[google_konts][]" style="width: 93%;"></td>
    </tr>
  </table>

  <table id="yandex_av_prot">
    <tr class="row">
      <td width="200"><input type="input" name="piel_yandex_new[atslegvards][]" style="width:170px;"></td>
      <td width="400"><?php echo nozare_select_box('piel_yandex_new[nozare_id][]', 0); ?></td>
      <td width="40"></td>
      <td width="40"></td>
      <td width="20"><a href="#" onclick="delAtslegv('yandex', this); return false;"><img src="css/del.png" width="16" height="16" alt="Dzēst" title="Dzēst" /></a></td>
    </tr>
  </table>

  <table id="yandex_av_domeni_prot">
    <tr class="row">
      <td><input type="input" name="piel_yandex_new[domeni][]" style="width:93%;"></td>
    </tr>
  </table>

  <table id="yandex_av_konts_prot">
    <tr class="row">
      <td><input type="input" name="piel_yandex_new[konts][]" style="width: 93%;"></td>
    </tr>
  </table>

  <table id="citi_av_prot">
    <tr class="row">
      <td width="200"><input type="input" name="piel_citi_new[atslegvards][]" style="width:170px;"></td>
      <td width="400"><?php echo nozare_select_box('piel_citi_new[nozare_id][]', 0); ?></td>
      <td width="40"></td>
      <td width="40"></td>
      <td width="20"><a href="#" onclick="delAtslegv('citi', this); return false;"><img src="css/del.png" width="16" height="16" alt="Dzēst" title="Dzēst" /></a></td>
    </tr>
  </table>

  <table id="citi_av_konts_prot">
    <tr class="row">
      <td><input type="input" name="piel_citi_new[konts][]" style="width: 93%;"></td>
    </tr>
  </table>

  <? foreach(array(25 => 'infomedia', 26 => 'tiessaite', 30 => 'yandex', 27 => 'cits') as $k => $v) { ?>

    <div id="<?= $v ?>_pak_prot">
      <div class="row">
        <select name="pakalpojumi_new[pakalpojuma_veida_id][]" style="width: 92%;">
          <option value=""></option>
          <? foreach($_vars['pakalpojumi'] as $k2 => $v2){ ?>
            <? if ($v2['parent_id'] == $k) { ?>
              <option value="<?=$k2;?>"><?=$v2['nosaukums'];?></option>
            <? } ?>
          <? } ?>
        </select>
        <a href="#" onclick="delPakalp('<?= $v ?>', this); return false;"><img src="css/del.png" width="16" height="16" alt="Dzēst" title="Dzēst" /></a>
      </div>
    </div>

    <div id="<?= $v ?>_pak_skaits_prot">
      <div class="row">
        <input type="input" name="pakalpojumi_new[pakalpojuma_apr][]" style="width:93%; text-align: center;">
      </div>
    </div>

  <? } ?>

  <!-- MPK prototypes -->
  <table id="mpk_keyword_prot">
    <tr class="google">
      <td class="rl"><input name="NewKeyword[google][keyword][]" value="" type="text" class="<?php echo check_access('ligumi-labot-pilns') ? 'canedit' : 'always_disabled';?>"></td>
      <td class="rl"><?php echo nozare_select_box('NewKeyword[google][nozare_id][]', '', array('class' => check_access('ligumi-labot-pilns') ? 'canedit' : 'always_disabled'));?></td>
      <td class="rl">
        <input type="checkbox" onclick="$(this).next('input').val($(this).is(':checked') ? 1 : 0);" class="<?php echo $pc_labot_pak;?>">
        <input type="hidden" name="NewKeyword[google][top3][]" value="0" class="<?php echo $pc_labot_pak;?>">
      </td>
      <td class="rl"><img onclick="mpkDelKw(this)" class="del_kw" src="css/del.png" width="16" height="16" alt="Dzēst" title="Dzēst"></td>
      <td class="domain rl"><input name="NewKeyword[google][domens][]" type="text" value="" class="<?php echo check_access('ligumi-labot-pilns') ? 'canedit' : 'always_disabled';?>"></td>
      <td class="account rl"><input name="NewKeyword[google][konts][]" type="text" value="" class="<?php echo check_access('ligumi-labot-pilns') ? 'canedit' : 'always_disabled';?>"></td>
      <td>
        <select name="NewKeyword[google][status][]" class="<?php echo check_access('ligumi-labot-pilns') ? 'canedit' : 'always_disabled';?>">
          <option value="1">Ir</option>
          <option value="2" selected>Nav</option>
          <option value="3">Daļēji</option>
        </select>
      </td>
    </tr>
    <tr class="yandex">
      <td class="rl"><input name="NewKeyword[yandex][keyword][]" value="" type="text" class="<?php echo check_access('ligumi-labot-pilns') ? 'canedit' : 'always_disabled';?>"></td>
      <td class="rl"><?php echo nozare_select_box('NewKeyword[yandex][nozare_id][]', '', array('class' => check_access('ligumi-labot-pilns') ? 'canedit' : 'always_disabled'));?></td>
      <td class="rl"><input type="checkbox" name="NewKeyword[yandex][top3][]" class="<?php echo check_access('ligumi-labot-pilns') ? 'canedit' : 'always_disabled';?>"></td>
      <td class="rl"><img onclick="mpkDelKw(this)" class="del_kw" src="css/del.png" width="16" height="16" alt="Dzēst" title="Dzēst"></td>
      <td class="domain rl"><input name="NewKeyword[yandex][domens][]" type="text" value="" class="<?php echo check_access('ligumi-labot-pilns') ? 'canedit' : 'always_disabled';?>"></td>
      <td class="account rl"><input name="NewKeyword[yandex][konts][]" type="text" value="" class="<?php echo check_access('ligumi-labot-pilns') ? 'canedit' : 'always_disabled';?>"></td>
      <td>
        <select name="NewKeyword[yandex][status][]" class="<?php echo check_access('ligumi-labot-pilns') ? 'canedit' : 'always_disabled';?>">
          <option value="1">Ir</option>
          <option value="2" selected>Nav</option>
          <option value="3">Daļēji</option>
        </select>
      </td>
    </tr>
  </table>

  <table id="mpk_category_prot">
    <tr class="yandex">
      <td><input name="NewCategory[yandex][name][]" style="width:90%" value="" class="<?php echo $pc_labot_pak;?>"></td>
      <td width="29" class="rl"><img onclick="mpkDelKw(this)" class="del_kw" src="css/del.png" width="16" height="16" alt="Dzēst" title="Dzēst"></td>
      <td width="71" class="domain rl"><input name="NewCategory[yandex][domens][]" type="text" value="" class="<?php echo $pc_labot_pak;?>"></td>
      <td width="71" class="account rl"><input name="NewCategory[yandex][konts][]" type="text" value="" class="<?php echo $pc_labot_pak;?>"></td>
      <td width="71">
        <select name="NewCategory[yandex][status][]" class="<?php echo $pc_labot_pak;?>">
          <option value="1">Ir</option>
          <option value="2" selected>Nav</option>
          <option value="3">Daļēji</option>
        </select>
      </td>
    </tr>

    <tr class="google">
      <td><input name="NewCategory[google][name][]" style="width:90%" value="" class="<?php echo $pc_labot_pak;?>"></td>
      <td width="29" class="rl"><img onclick="mpkDelKw(this)" class="del_kw" src="css/del.png" width="16" height="16" alt="Dzēst" title="Dzēst"></td>
      <td width="71" class="domain rl"><input name="NewCategory[google][domens][]" type="text" value="" class="<?php echo $pc_labot_pak;?>"></td>
      <td width="71" class="account rl"><input name="NewCategory[google][konts][]" type="text" value="" class="<?php echo $pc_labot_pak;?>"></td>
      <td width="71">
        <select name="NewCategory[google][status][]" class="<?php echo $pc_labot_pak;?>">
          <option value="1">Ir</option>
          <option value="2" selected>Nav</option>
          <option value="3">Daļēji</option>
        </select>
      </td>
    </tr>
  </table>

  <table id="atgadinajumsprot">

    <tbody>

      <tr>
        <td><input type="input" class="showcalendarnew atgadinajumi_termimi" value="<?=date('d.m.Y');?>" name="atgadinajumi_new[datums][]"></td>
        <td class="status">Jauns</td>

        <? /* ?>
        <td>
          <select name="atgadinajumi_new[tema][]">
            <? foreach($_vars['atgadinajumu_temas'] as $temas_id => $temas_nosauk) { ?>
              <option value="<?= $temas_id ?>"><?= $temas_nosauk ?></option>
            <? } ?>
          </select>
        </td>
        <? */ ?>

        <td>
          <select name="atgadinajumi_new[redzamiba][]">
            <? if (is_admin()) { ?>
              <option value="admin">Adminiem</option>
            <? } else { ?>
              <option value="pardevejs">Pārdevējam</option>
            <? } ?>
            <option value="kopejs">Visiem</option>
          </select>
        </td>

        <td><textarea name="atgadinajumi_new[saturs][]" rows="2" cols="30"></textarea></td>
        <td><?=date('d.m.Y');?></td>
        <td><?=$_vars['sys_pardeveji'][$_SESSION['user']['id']];?></td>
        <td></td>
        <td></td>
      </tr>

    </tbody>

  </table>

  <? $form_names = array('lig_klients', 'lig_pakalpojums', 'lig_rekini', 'lig_atgadinajumi') ?>

  <? foreach($form_names as $form_name) { ?>

    <?
    $fields_query = db_query("SELECT * FROM `".DB_PREF."formu_lauki` WHERE forma = '".esc($form_name)."' ORDER BY id ASC");
    $fields = array();

    while($row = db_get_assoc($fields_query)) {
      $fields[] = $row;
    }
    ?>

    <div id="komentarsprot_<?= $form_name ?>">
      <table class="comment_form">
        <tr>
          <td width="70">Komentārs:</td>
          <td>
            <textarea name="komentari_new[<?= $form_name ?>][komentars][]" rows="2" cols="30"></textarea>
          </td>
        </tr>
        <? if (!empty($fields)) { ?>
        <tr>
          <td>Lauks:</td>
          <td>
            <select name="komentari_new[<?= $form_name ?>][formas_lauka_id][]">
              <option value="">Nepiesaistīts laukam</option>
              <? foreach($fields as $row) { ?>
                <option value="<?= $row['id'] ?>"><?= $row['nosaukums'] ?></option>
              <? } ?>
            </select>
          </td>
        </tr>
        <? } ?>
      </table>
    </div>

  <? } ?>

  <div id="saistitie_pardeveji_prot">
    <div style="margin-bottom: 5px;">
      <select name="saistitie_pardeveji_new[pardeveja_id][]">
        <option value="">&nbsp;</option>
        <?php foreach($_vars['sys_pardeveji'] as $k=>$p){ ?>
          <option value="<?=$k;?>"><?=$p;?></option>
        <?php } ?>
      </select>
      <span style="margin-left: 10px;">Datums:</span> <input class="saistpardcalendarnew" type="text" name="saistitie_pardeveji_new[piesaist_datums][]" value="<?= date('d.m.Y') ?>" style="width: 6em;" />
      <button onclick="$(this).parent().remove();" class="ui-state-default ui-corner-all">Dzēst</button>
    </div>
  </div>

  <div id="parsl_rinda_prot">
    <div style="margin-bottom: 5px;">
      <input type="text" name="parsl_rinda_new[jaunais_liguma_display][]" value="" class="parsl_rinda_display_inp" style="width: 84%;" />
      <input type="hidden" name="parsl_rinda_new[jaunais_liguma_id][]" value="" class="parsl_rinda_id_inp" />
      <button onclick="$(this).parent().remove();" class="ui-state-default ui-corner-all">Dzēst</button>
    </div>
  </div>

</div>

<h1 style="font-size:20px; height: 22px;" id="uznemuma_nosaukums"><?=!empty($d['nosaukums'])?$d['nosaukums']:""?></h1>

<div class="sub-menu">
  <span>
    <a href="javascript:;" class="tabulation <?= ($subtab == 0) ? 'active' : '' ?>" active="0" opens="form_klients">Līgums</a> |
    <a href="javascript:;" class="tabulation <?= ($subtab == 1) ? 'active' : '' ?>" active="1" opens="form_pakalpojums">Pakalpojums</a> |
    <a href="javascript:;" class="tabulation <?= ($subtab == 2) ? 'active' : '' ?>" active="2" opens="form_rekini">Rēķini</a> |
    <a href="javascript:;" class="tabulation <?= ($subtab == 3) ? 'active' : '' ?>" active="3" opens="form_atgadinajumi">Atgādinājumi <?= !empty($akt_atg_skaits) ? '(' . $akt_atg_skaits . ')' : '' ?></a>
    <?php
    if(check_access('ligumi-atskaites') && $ligums->hasAdwordsAccounts()) { ?>
        | <a href="javascript:;" class="tabulation <?= ($subtab == 4) ? 'active' : '' ?>" active="4" opens="form_atskaites">Atskaites</a>
    <?php } ?>
  </span>
</div>

<button id="labotdzestbutton" onClick="return editoradd(this, <?= is_admin() ? 1 : 0 ?>, <?= is_limited() ? $_SESSION['user']['ierobezots'] : 0 ?>)">Labot</button>

<form action="?c=ligumi&a=labot&id=<?= !empty($_GET['id']) ? '&a=labot&id=' . (int) $_GET['id'] : '&a=jauns' ;?>" id="fullformplace" method="post" enctype="multipart/form-data">

  <div id="form_rekini" class="<?=($subtab==2?"tab_visible":"tab_hidden");?>">
    <? include("forms/rekini.php"); ?>
  </div>

  <div id="form_pakalpojums" class="<?=($subtab==1?"tab_visible":"tab_hidden");?>">
    <? include("forms/pakalpojums.php"); ?>
  </div>

  <div id="form_klients" class="<?=($subtab==0?"tab_visible":"tab_hidden");?>">  
    <? include("forms/klients.php"); ?>
  </div>

  <div id="form_atgadinajumi" class="<?=($subtab==3?"tab_visible":"tab_hidden");?>">
    <? include("forms/atgadinajumi.php"); ?>
  </div>

  <?php if(check_access('ligumi-atskaites')) { ?>

      <div id="form_atskaites" class="<?=($subtab==4?"tab_visible":"tab_hidden");?>">
        <? include("forms/atskaites.php"); ?>
      </div>

  <?php } ?>

  <?php if (!empty($_GET['id'])) { ?>
    <input type="hidden" name="liguma_id" class="<?php echo $pc_labot_pak;?>" value="<?php echo $_GET['id'] ?>" />
  <?php } ?>

  <div class="hidethistoo">
    <input type="submit" onClick="return saveAll(this, 'ligums')" class="submit show_in_act_panel" value="Saglabāt">
    <input type="button" onclick="return cancelSaves();" class="show_in_act_panel" value="Atcelt">
    <? if(check_access('ligumi-dzest') && !empty($_GET['id'])) { ?>
      <input type="button" onclick="return deleteClient()" class="show_in_act_panel align_left" value="Dzēst līgumu">
    <? } ?>
  </div>

</form>

<script>

var activeone = <?= $subtab ?>;

$(function() {

  $('.tabulation').click(function(){

    $('#form_klients').removeClass('tab_visible').addClass('tab_hidden');
    $('#form_pakalpojums').removeClass('tab_visible').addClass('tab_hidden');
    $('#form_rekini').removeClass('tab_visible').addClass('tab_hidden');
    $('#form_atgadinajumi').removeClass('tab_visible').addClass('tab_hidden');
    $('#form_atskaites').removeClass('tab_visible').addClass('tab_hidden');

    $('#'+$(this).attr('opens')).removeClass('tab_hidden').addClass('tab_visible');

    $('.tabulation').removeClass('active');

    $(this).addClass('active');

    recreateRekiniNr();

    activeone = $(this).attr('active');

  });

  $('#nosaukumsplace').keyup(function(){
    $('#uznemuma_nosaukums').html($(this).val());
  });

});

function copyFaktToJur() {

  $('#juradd1').val($('#add1').val());
  $('#juradd2').val($('#add2').val());
  $('#juradd3').val($('#add3').val());

  return false;

}

function copyJurToFakt() {

  $('#add1').val($('#juradd1').val());
  $('#add2').val($('#juradd2').val());
  $('#add3').val($('#juradd3').val());

  return false;

}

function deleteClient(){

  if (confirm('Vai Jūs tiešām vēlaties dzēst šo klientu?')) {
    document.location = './?deleteClient=<?=!empty($_GET['id'])?(int)$_GET['id']:0;?>';
  }

  return false;

}

function activateTab(tab_name) {

  $('#form_klients').removeClass('tab_visible').addClass('tab_hidden');
  $('#form_pakalpojums').removeClass('tab_visible').addClass('tab_hidden');
  $('#form_rekini').removeClass('tab_visible').addClass('tab_hidden');
  $('#form_atgadinajumi').removeClass('tab_visible').addClass('tab_hidden');
  $('#form_atskaites').removeClass('tab_visible').addClass('tab_hidden');

  $('#form_' + tab_name).removeClass('tab_hidden').addClass('tab_visible');

  $('.tabulation').removeClass('active');

  $('a[open=form_' + tab_name + ']').addClass('active');

  activeone = $('a[open=form_' + tab_name + ']').attr('active');

}

$("#ligumadat").datepicker({dateFormat:'dd.mm.yy'});
$("#rekno").datepicker({dateFormat:'dd.mm.yy'});
$("#reklidz").datepicker({dateFormat:'dd.mm.yy'});
$("#sanemsanas_datums").datepicker({dateFormat:'dd.mm.yy'});

editordisable();

initSelectsWithStatusColors();

<? if(empty($_GET['id'])) { ?>
  editoradd($('#labotdzestbutton'), <?= is_admin() ? 1 : 0 ?>, <?= is_limited() ? 1 : 0 ?>);
<? } ?>

<? if(empty($_GET['id'])) { ?>

$('input[name=regnr]').keyup(function() {
  $('input[name=pvnnr]').val('LV' + $(this).val());
});

<? } ?>

$("input[name=grupas_regnr_display]").autocomplete({
  source: "ajax.php?action=ligumu_grupas",
  minLength: 2,
  select: function(event, ui) {
    $("input[name=grupas_regnr_display]").val(ui.item.label);
    $('input[name=grupas_regnr]').val(ui.item.id);
  }
});

function initParslegsanasRinda(display_inp) {

  var id_inp = display_inp.closest('div').find('.parsl_rinda_id_inp');

  display_inp.autocomplete({
    source: "ajax.php?action=ligumi_parslegsanas_rindai",
    minLength: 2,
    select: function(event, ui) {
      display_inp.val(ui.item.label);
      id_inp.val(ui.item.id);
    }
  });

}

function setManualBApm(rek_id, summa)
  {
    $($('<div></div>')
      .append(
        $('<input type="text" name="summa" />').val(parseFloat(summa).toFixed(2)).css('width', '100px')
      ).append(' EUR')
    ).dialog({
      resizable: false,
      height:140,
      width: 400,
      modal: true,
      title: 'Apmaksātais budžets',
      buttons: {
        "Rēķināt automātiski": function() {
            $( this ).dialog( "close" );
            window.location = '?c=ligumi&a=budzeta_parskaitijumi&rek_id='+rek_id+'&task=change_apm_mode&sum=-1';
        },
        "Iestatīt manuāli": function() {
            $( this ).dialog( "close" );
            window.location = '?c=ligumi&a=budzeta_parskaitijumi&rek_id='+rek_id+'&task=change_apm_mode&sum=' + $(this).find('input[name=summa]').val();
        }
      }
    });
  }

$("input.parsl_rinda_display_inp").each(function() {
  initParslegsanasRinda($(this));
});
</script>