<?
if (!check_access('ligumi-parslegsana')) {
  die();
}

if (empty($_GET['liguma_id'])) {
  die();
}

$liguma_id = (int) $_GET['liguma_id'];

$lig_data = db_get_assoc(db_query("SELECT * FROM `".DB_PREF."ligumi` WHERE `id` = " . $liguma_id));

if (empty($lig_data)) {
  die();
}

$prev_contracts = db_get_assoc_all(db_query("
  SELECT
    g.*,
    b.beigu_statuss
  FROM `".DB_PREF."ligumi` g
  LEFT JOIN `".DB_PREF."ligumi_beigusies` b ON (g.id = b.liguma_id)
  WHERE
    g.`regnr` = '".$lig_data['regnr']."' AND
    g.`id` != ".$lig_data['id']."
"));

$prev_contracts_to_show = array();

if (!empty($prev_contracts)) {

  foreach($prev_contracts as $prev_contract) {

    $prev_contract_rekini = db_get_assoc_all(db_query("SELECT * FROM `".DB_PREF."rekini` WHERE `liguma_id` = '".$prev_contract['id']."' ORDER BY izr_datums DESC"));

    $stat = get_liguma_statuss($prev_contract, $prev_contract_rekini);

    // Rādam tikai aktīvos, vai tos, kas beigušies un ar statusu termins vai atteikums

    if ($stat == 3 || ($stat == 6 && (empty($prev_contract['beigu_statuss']) || in_array($prev_contract['beigu_statuss'], array('termins', 'atteikums'))))) {
      $prev_contract['liguma_statuss'] = $stat;
      $prev_contracts_to_show[$prev_contract['id']] = $prev_contract;
    }

  }

}

if (!empty($_POST)) {

  $_POST = trim_array($_POST);

  if (!empty($_POST['parslegt_ligumu'])) {

    foreach($_POST['parslegt_ligumu'] as $parslegt_ligumu_id) {

      if (!empty($prev_contracts_to_show[$parslegt_ligumu_id])) {

        $prev_contract = $prev_contracts_to_show[$parslegt_ligumu_id];

        $vecais_ligums = db_get_assoc(db_query("SELECT * FROM `".DB_PREF."ligumi` WHERE id = ".$prev_contract['id']." LIMIT 1"));;

        if ($prev_contract['liguma_statuss'] == 3) {

          // ja nu ir tomēr
          db_query("DELETE FROM `".DB_PREF."ligumi_parslegsanu_rinda` WHERE `liguma_id` = ".((int) $prev_contract['id'])." LIMIT 1");

          db_query("
            INSERT INTO `".DB_PREF."ligumi_parslegsanu_rinda`
            SET
              liguma_id = ".$prev_contract['id'].",
              jaunais_liguma_id = ".$lig_data['id'].",
              izveidoja_laiks = NOW(),
              izveidoja_pardeveja_id = ".$_SESSION['user']['id']."
          ");

          $rindas_id = db_last_id();

          $log_data = array();

          $log_data['parslegsanas_rinda']['new'] = db_get_assoc(db_query("SELECT * FROM `".DB_PREF."ligumi_parslegsanu_rinda` WHERE id = ".$rindas_id." LIMIT 1"));
          $log_data['parslegsanas_rinda']['title'] = sprintf('Izveidoja jaunu līguma pārslēgšanas rindu no %s uz %s', $vecais_ligums['ligumanr'], $lig_data['ligumanr']);

          log_add("laboja", $log_data);


        } else if ($prev_contract['liguma_statuss'] == 6) {

          db_query("DELETE FROM `".DB_PREF."ligumi_beigusies` WHERE `liguma_id` = ".((int) $prev_contract['id'])." LIMIT 1");

          db_query("
            INSERT INTO `".DB_PREF."ligumi_beigusies`
            SET
              liguma_id = ".$prev_contract['id'].",
              beigu_statuss = 'parslegts',
              parslegts_lig_nr = '".$lig_data['ligumanr']."',
              auto_parslegts = 1,
              laboja_laiks = NOW(),
              pirmoreiz_apstradats = NOW()
          ");

          $parslegsanas_id = db_last_id();

          $log_data = array();

          $log_data['parslegsanas_rinda']['new'] = db_get_assoc(db_query("SELECT * FROM `".DB_PREF."ligumi_beigusies` WHERE id = ".$parslegsanas_id." LIMIT 1"));
          $log_data['parslegsanas_rinda']['title'] = sprintf('Pārslēdza līgumu %s uz %s', $vecais_ligums['ligumanr'], $lig_data['ligumanr']);

          log_add("laboja", $log_data);


        }

      }

    }

  }

  ?>
  <script type="text/javascript">
  parent.document.location.reload();
  </script>
  <?
  die();

}

?>

<div class="liguma_parslegsana_dialog">

  <form action="?c=ligumi&a=parslegsana&liguma_id=<?= $liguma_id ?>&without_nav=1" method="post">

    <p>Tika atrasti viens vai vairāki iepriekš noslēgti līgumi ar šādu pašu reģistrācijas numuru.
    Atzīmējiet kuriem līgumiem vēlaties norādīt, ka tie ir pārslēgti uz šo jauno līgumu.
    Ja līgums vēl aizvien ir aktīvs, tad pārslēgšana notiks automātiski, kad pienāks
    līguma beigu datums.</p>

    <table class="data" style="width: 99%;">

      <thead>

        <tr class="last header">
          <th>Līguma Nr</th>
          <th>Nosaukums</th>
          <th>Līguma datums</th>
          <th>Reklāma no</th>
          <th>Reklāma līdz</th>
          <th>Statuss</th>
          <th>Pārslēgt</th>
        </tr>

      </thead>

      <tbody class="main">

        <?php foreach($prev_contracts_to_show as $prev_contract) { ?>

          <tr>
            <td class="c"><?php echo $prev_contract['ligumanr'] ?></td>
            <td><?php echo $prev_contract['nosaukums'] ?></td>
            <td class="c"><?php echo date('d.m.Y', strtotime($prev_contract['ligumadat'])) ?></td>
            <td class="c"><?php echo date('d.m.Y', strtotime($prev_contract['reklama_no'])) ?></td>
            <td class="c"><?php echo date('d.m.Y', strtotime($prev_contract['reklama_lidz'])) ?></td>
            <td class="c">
              <?php echo $_vars['liguma_statuss'][$prev_contract['liguma_statuss']] ?>
              <?php if ($prev_contract['liguma_statuss'] == 6) { ?>
                (<?= !empty($prev_contract['beigu_statuss']) ? $_vars['liguma_beigu_statuss'][$prev_contract['beigu_statuss']] : $_vars['liguma_beigu_statuss']['termins'] ?>)
              <?php } ?>
            </td>
            <td class="c">
              <input type="checkbox" name="parslegt_ligumu[]" value="<?php echo $prev_contract['id'] ?>" />
            </td>
          </tr>

        <?php } ?>

      </tbody>

    </table>

    <div style="text-align: center;">
      <input type="hidden" name="form_sent" value="1" />
      <button class="ui-state-default ui-corner-all">Saglabāt</button>
    </div>

  </form>

</div>