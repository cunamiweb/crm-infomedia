<?php

if (!check_access('epastu-izraksts')) {
  die();
}

if(isset($_GET['create']) && $_GET['create']) {

    #ini_set('display_errors', 1);
    #error_reporting(E_ALL);

  require(dirname($_SERVER['SCRIPT_FILENAME']) . '/libs/phpexcel/Classes/PHPExcel.php');

  $type = isset($_GET['type']) ? $_GET['type'] : null;

  $data = array();

  if($type == 'ligumi') {

    $headings = array('Statuss', 'Vārds Uzvārds', 'E-pasts', 'Nosaukums', 'Reģistrācijas numurs');
    $name = 'E-pasti no ligumiem (' . date('d.m.Y.') . ').xls';
    $objid = 0;

    $sql = '
      SELECT g.id, g.nosaukums, g.regnr, c.epasts, c.vards, b.beigu_statuss
      FROM '.DB_PREF.'ligumi g
      LEFT JOIN '.DB_PREF.'ligumi_beigusies b ON b.liguma_id = g.id
      LEFT JOIN '.DB_PREF.'kontakti c ON c.liguma_id = g.id AND ligumsledzejs = 1
      WHERE epasts IS NOT NULL AND epasts != ""
    ';

    $result = db_get_assoc_all(db_query($sql));

    foreach($result as $row) {

      $status = get_liguma_status_by_id($row['id']);

      $status_text = $_vars['liguma_statuss'][$status];


      if($status == 6) {
        if (!$row['beigu_statuss'] || $row['beigu_statuss'] == 'termins') {
          $status_text .= ' (termiņš)';
        } elseif ($row['beigu_statuss'] == 'parslegts') {
          $status_text .= ' (pārslēgts)';
        } elseif ($row['beigu_statuss'] == 'atteikums') {
          $status_text .= ' (atteikums)';
        } elseif (!empty($row['beigu_statuss_auto_atteikums'])) {
          $status_text .= ' (pārtraukts)';
        }
      }


      $data[] = array(
        $status_text,
        $row['vards'],
        $row['epasts'],
        $row['nosaukums'],
        $row['regnr'],
      );
    }

  } else if($type == 'piedavajumi') {

    $name = 'E-pasti no piedavajumiem (' . date('d.m.Y.') . ').xls';
    $objid = 1;
    $headings = array('Vārds Uzvārds', 'E-pasts', 'Nosaukums', 'Datums');

    $sql = '
      SELECT p.kontaktpersona, p.epasts, p.klients, p.datums
      FROM '.DB_PREF.'piedavajumi p
      LEFT JOIN '.DB_PREF.'kontakti c ON c.epasts = p.epasts AND ligumsledzejs = 1
      WHERE
        p.epasts IS NOT NULL AND
        p.epasts != "" AND
        c.id IS NULL
    ';

    $result = db_get_assoc_all(db_query($sql));

    foreach($result as $row) {
      $data[] = array(
        $row['kontaktpersona'],
        $row['epasts'],
        $row['klients'],
        date('d.m.Y.', strtotime($row['datums'])),
      );
    }

  } else {
    throw new exception('Neatpazīts tips.');
  }

  //  Generate the xls
  $objPHPExcel = new PHPExcel();

  // Set document properties
  // by agris changed server name
  $objPHPExcel->getProperties()->setCreator('crm.infomedia.lv')
    ->setLastModifiedBy('crm.infomedia.lv')
    ->setTitle('E-pastu izraksts');

  $objPHPExcel->getActiveSheet()->fromArray($headings, NULL, 'A1');
  $objPHPExcel->getActiveSheet()->fromArray($data, NULL, 'A2');

  foreach(range('A','E') as $columnID) {
    $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
        ->setAutoSize(true);
  }

  $name = prepare_filename($name);

  db_query("
    INSERT INTO `".DB_PREF."faili` SET
      `objekta_id` = ".$objid.",
      `objekta_tips` = 'email_report',
      `pardeveja_id` = ".$_SESSION['user']['id'].",
      `fails` = '".esc($name)."',
      `izmers` = 1
  ");

  $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

  $objWriter->save('faili/' . db_last_id());

  $autodownload[] = db_last_id();

  $_SESSION['autodownload_file_ids'] = $autodownload;

  #header("location: ?c=atskaites&a=emails");
  ?>
  <script>
    document.location = '?c=atskaites&a=emails';
  </script>
  <?

  die();
}

  log_add("atvera", "Atvēra epastu izrakstu");
?>

<iframe src="" style="display:none;" id="downloadframe" name="downloadframe"></iframe>

<? show_autodownload_iframes() ?>

<div class="container">
  <div style="width:49%; float:left;">
    <h2>E-pasti no līgumiem</h2>
    <a href="?c=atskaites&a=emails&create=1&type=ligumi" type="button" id="ligumi_button" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" aria-disabled="false">
      <span class="ui-button-text">Izveidot jaunu</span>
    </a><br />
    <?php
    show_files_block('email_report', 1, false, false, false, false, false);
      ?>
  </div>
  <div style="width:49%; float:right;">
    <h2>E-pasti no piedāvājumiem</h2>
    <a href="?c=atskaites&a=emails&create=1&type=piedavajumi" type="button" id="piedavajumi_button" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" aria-disabled="false">
      <span class="ui-button-text">Izveidot jaunu</span>
    </a><br />
    <?php
      show_files_block('email_report', 1, false, false, false, false, false);
      ?>
  </div>
  <div class="clr"></div>
</div>