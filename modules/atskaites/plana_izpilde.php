<?php
if (!check_access('atskaites-plana-izpilde')) {
  die();
}

//ini_set('display_errors', '1');

log_add("atvera", "Atvēra plāna izpildes statistiku");

$data = array();

$sel_year = (!empty($_GET['year']) && $_GET['year'] <= date('Y')) ? $_GET['year'] : date('Y');

$cache_key = 'getKopPlanaIzpildesStats-' . $sel_year;

$stats = cache_get($cache_key);

if(!$stats) {
  $stats = get_plana_izpildes_stats($sel_year);
  cache_set($cache_key, $stats);
}

$quarter_data = $stats['quarter_data'];
$month_data = $stats['month_data'];
$pardeveju_data = $stats['pardeveju_data'];
$total_data = $stats['total_data'];

//echo '<pre style="text-align: left;">';
//print_r($month_data);
//print_r($quarter_data);
?>

<form action="" method="get" id="izpilde_filter">
  <input type="hidden" name="c" value="atskaites" />
  <input type="hidden" name="a" value="plana_izpilde" />
  <div>
    Gads:
    <span>
      <select id="year" name="year" type="input">
        <? for($year = 2011; $year <= date('Y'); $year ++) { ?>
          <option <?= ($year == $sel_year) ? 'selected="selected"' : '' ?> value="<?= $year ?>"><?= $year ?></option>
        <? } ?>
      </select>
    </span>
	</div>
</form>

<table cellpadding="3" cellspacing="0" class="data" id="plana_izpilde" style="width: auto;">

  <?php print_plana_izpilde_head() ?>

  <tbody>

    <?php
    foreach ($quarter_data as $quarter_period => $quarter_row) {

      $quarter_row['real'] = $quarter_row['kopa'];

      print_plana_izpilde_row($quarter_period, $quarter_row);

      foreach ($month_data[$quarter_period] as $month_period => $month_row) {

        $month_row['real'] = $month_row['kopa'];

        print_plana_izpilde_row($month_period, $month_row, array(
          'month_rows' => true,
          'allow_open_month' => true
        ));

        foreach ($pardeveju_data[$month_period] as $pardeveja_id => $pardeveja_row) {

          $pardeveja_row['real'] = $pardeveja_row['kopa'];
          print_plana_izpilde_row($month_period, $pardeveja_row, array(
            'pardeveja_rows' => true,
            'pardevejs' => '<a target="_blank" href="?c=darbinieki&a=labot&id='.$pardeveja_id.'&subtab=izpilde">'.$_vars['sys_pardeveji'][$pardeveja_id].'</a>'
          ));

        }

      }

    }

    print_plana_izpilde_row('total', $total_data);
    ?>

  </tbody>

</table>
<div><a target="_blank" href="/premiums_gen.php">Pārģenerēt prēmijas procentus</a></div>
<div><a target="_blank" href="/premiums_virtual_gen.php">Pārģenerēt virtuālās prēmijas procentus</a></div>

<script>
initPlanaIzpildeTable();

$("form#izpilde_filter").each(function() {

  var form = $(this);

  $('select#year', form).change(function() {
    form.submit();
  });

});

$(function(){

  //  Atver tekošo
  var cur_ceturksnis = <?php echo ceil(date('m')/3);?>;
  var cur_menesis = <?php echo date('m');?>; //  pēc kārtas ceturksnī. tb 1-3
  $(".open_months").eq(cur_ceturksnis-1).trigger('click');
  $('.open_pardeveji').eq(cur_menesis - 1).trigger('click');

});
</script>