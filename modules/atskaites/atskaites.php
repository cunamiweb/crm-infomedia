<?php

$a = empty($_GET['a']) ? "" : $_GET['a'];

if(!$a) {
  if(check_access('atskaites-plana-izpilde')) {
    $a = 'plana_izpilde';

  } else if(check_access('atskaites-ligumu-stati')) {
    $a = 'ceturksnu_statistika';

  } else if(check_access('atskaites-unikalie-av')) {
    $a = 'unikalie_av';
  }
}

if(!getGet('without_nav')) {
?>
<div class="sub-menu">
  <span>

    <?php if(check_access('atskaites-ligumu-stati')) { ?>
      <a href="?c=atskaites&a=ceturksnu_statistika" <?php echo $a == 'ceturksnu_statistika' ? 'class="active"': '';?>>Līgumu statistika</a>
    <?php }?>

    <?php if(check_access('atskaites-plana-izpilde')) { ?>
      <a href="?c=atskaites&a=plana_izpilde"  <?php echo $a == 'plana_izpilde' ? 'class="active"': '';?>>Plāna izpilde</a>
    <?php }?>

    <?php if(check_access('atskaites-av-stati')) { ?>
      <a href="?c=atskaites&a=ligumu_parskats" <?php echo $a == 'ligumu_parskats' ? 'class="active"': '';?>>A/V statistika</a>
    <?php }?>

    <?php if(check_access('atskaites-unikalie-av')) { ?>
      <a href="?c=atskaites&a=unikalie_av" <?php echo $a == 'unikalie_av' ? 'class="active"': '';?>>Unikālie A/V</a>
    <?php }?>

    <?php if(check_access('atskaites-zurnals')) { ?>
      <a href="?c=atskaites&a=log" <?php echo $a == 'log' ? 'class="active"': '';?>>Žurnāls</a>
    <?php } ?>

    <?php if(check_access('epastu-izraksts')) { ?>
      <a href="?c=atskaites&a=emails" <?php echo $a == 'emails' ? 'class="active"': '';?>>E-pastu izraksts</a>
    <?php } ?>

  </span>
</div>
<?
}

switch($a){

  case "ligumu_parskats": $include = "ligumu_parskats.php";  break;
  case "ceturksnu_statistika": $include = "ceturksnu_statistika.php";  break;
  case "log": $include = "log.php";  break;
  case "plana_izpilde": $include = "plana_izpilde.php";  break;
  case "unikalie_av": $include = "unikalie_av.php"; break;
  case "emails": $include = "emails.php"; break;

  default:
    if(check_access('atskaites-plana-izpilde')) {
      $include = "plana_izpilde.php";

    } else if(check_access('atskaites-ligumu-stati')) {
      $include = "ceturksnu_statistika.php";

    } else if(check_access('atskaites-unikalie-av')) {
      $include = "unikalie_av.php";
    }

}

include($include);
?>