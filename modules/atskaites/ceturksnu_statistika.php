<?php
if (!check_access('atskaites-ligumu-stati')) {
  die();
}
log_add("atvera", "Atvēra ceturkšņu atskaiti");

$data = array();

$sel_year = (!empty($_GET['year']) && $_GET['year'] <= date('Y')) ? $_GET['year'] : date('Y');

$quarter_from = $sel_year . '-1';
$quarter_to = $sel_year . '-4';

if ($sel_year == date('Y')) {

  if (date('m') <= 3) {
    $show_till = 1;
  } elseif (date('m') <= 6) {
    $show_till = 2;
  } elseif (date('m') <= 9) {
    $show_till = 3;
  } elseif (date('m') <= 12) {
    $show_till = 4;
  }

} else {
  $show_till = 4;
}

$data = get_statistika('quarter', $quarter_from, $quarter_to, null, $sel_year . '-' . $show_till);
?>

<form action="" method="get" id="stats_filter">
  <input type="hidden" name="c" value="atskaites" />
  <input type="hidden" name="a" value="ceturksnu_statistika" />
  <div>
    Gads:
    <span>
      <select id="year" name="year" type="input">
        <? for($year = 2011; $year <= date('Y'); $year ++) { ?>
          <option <?= ($year == $sel_year) ? 'selected="selected"' : '' ?> value="<?= $year ?>"><?= $year ?></option>
        <? } ?>
      </select>
    </span>
	</div>
</form>

<table cellpadding="3" cellspacing="0" class="data" id="ceturksnu_statistika" style="width: auto;">

  <?php print_statistika_head() ?>

  <tbody>

    <?php
    foreach ($data as $i => $d) {
      print_statistika_rows($i, $d);
    }
    ?>

  </tbody>

</table>

<script>
initStatistikaTable();

$("form#stats_filter").each(function() {

  var form = $(this);

  $('select#year', form).change(function() {
    form.submit();
  });

});
</script>