<?php
if (!check_access('atskaites-zurnals')) {
  die();
}

$where = array();

if (!isset($_GET['fdatums_no'])) {
  $_GET['fdatums_no'] = date('d.m.Y');
}

if (!isset($_GET['fdatums_lidz'])) {
  $_GET['fdatums_lidz'] = date('d.m.Y');
}

if (getGet('fdatums_no')) {
  $where[] = "z.datums >= '".date("Y-m-d", strtotime(getGet('fdatums_no')))."'";
}

if (getGet('fdatums_lidz')) {
  $where[] = "z.datums <= '".date("Y-m-d", strtotime(getGet('fdatums_lidz')))."'";
}

if (getGet('fpardeveja_id')) {
  $where[] = "z.pardeveja_id = " . (int) $_GET['fpardeveja_id'];
}

$sql = "
  SELECT
    z.*,
    p.vards as pardeveja_vards
  FROM `".DB_PREF."log` z
  LEFT JOIN `".DB_PREF."pardeveji` p ON (z.pardeveja_id = p.id)
";

if (!empty($where)) {
  $sql .= " WHERE " . implode(' AND ', $where);
}

$sql .= "
  ORDER BY z.laiks DESC
";

$query = db_query($sql);
?>

<form method="get" action="">

  <input type="hidden" name="c" value="atskaites" />
  <input type="hidden" name="a" value="log" />

  Datums no: <input class="kalendari" id="datums_no" name="fdatums_no" value="<?= getGet('fdatums_no') ?>" type="input" style="width:80px;">
  Datums līdz: <input class="kalendari" id="datums_lidz" name="fdatums_lidz" value="<?= getGet('fdatums_lidz') ?>" type="input" style="width:80px;">
  Pārdevējs:
    <select name="fpardeveja_id">
      <option value=""></option>
      <?php foreach($_vars['sys_pardeveji'] as $k=>$p){ ?>
        <option value="<?=$k;?>" <?= getGet('fpardeveja_id') == $k ? 'selected' : '' ?>><?=$p;?></option>
      <?php } ?>
    </select>
  <input class="ui-state-default ui-corner-all" type="submit" value="Atlasīt" />

</form>

<table id="log_table" class="data" style="width: 99%;">

  <thead>

    <tr class="last">
      <th>Datums</th>
      <th>Laiks</th>
      <th>Apraksts</th>
      <th>Lietotājs</th>
      <th>IP</th>
    </tr>

  </thead>

  <tbody class="main">

    <? while($row = db_get_assoc($query)) { ?>

      <tr>
        <td><?= date('d.m.Y', strtotime($row['laiks'])) ?></td>
        <td><?= date('H:i', strtotime($row['laiks'])) ?></td>
        <td><?= nl2br($row['apraksts']) ?></td>
        <td><?= $row['pardeveja_vards'] ?></td>
        <td><?= $row['ip'] ?></td>
      </tr>

    <? } ?>

  </tbody>

</table>

<script type="text/javascript">
$(document).ready(function() {
  $(".kalendari").datepicker({dateFormat:'dd.mm.yy'});
  stripeTable($("table#log_table"));
});
</script>