<?php
/**
 * Unikālie atslēgvārdi Infomedia pakalpojumam aktīvajiem līgumiem
 * "Unikālie" nozīmē, ka konkrētais atslēgvārds ir tikai vienam klientam
 *
 * @author Dāvis Frīdenvalds davis@datateks.lv
 * @date 16.02.2012.
 */

if (!check_access('atskaites-unikalie-av')) {
  die();
}

log_add("atvera", "Atvēra unikālo atslegvardu pārskatu");

$order = 'p.atslegvards';
$direction = 'asc';

$order_bys = array(
  'atslegvards' => 'p.atslegvards',
  'nosaukums' => 'g.nosaukums',
  'reklama_lidz' => 'g.reklama_lidz'
);
if (!empty($_GET['order']) && isset($order_bys[$_GET['order']])) {
  $order = $order_bys[$_GET['order']];
} else {
  $order = 'g.reklama_lidz';
}

if (!empty($_GET['direction']) && in_array(strtolower($_GET['direction']), array('asc', 'desc'))) {
  $direction = strtolower($_GET['direction']);
} else {
  $direction = 'desc';
}

$filter = array(
  'nosaukums' => getGet('fnosaukums'),
  'atslegvards' => getGet('fatslegv'),
  'reklama_lidz' => getGet('flig_lidz'),
  'nozare_id' => getGet('fnozare'),
);

$unikalie_av = get_unique_keywords($order, $direction, $filter);

?>
<p>Aktīvie a/v caur Infomedia.lv, kuri nedublējas.</p>

  <form action="" id="fullformplace" method="GET" style="clear: both;">
    <input type="hidden" name="c" value="atskaites" />
    <input type="hidden" name="a" value="unikalie_av" />
    <input type="hidden" name="order" value="<?= getGet('order') ?>" />
    <input type="hidden" name="direction" value="<?= getGet('direction') ?>" />

    <table  cellpadding="3" cellspacing="0" class="data" id="unikalie_av" style="width: auto;">
      <thead>
        <tr class="header">
          <th width="40">Npk</th>
          <?php print_th('Līgums', 'nosaukums', array('width' => 300));?>
          <?php print_th('Atslēgvārds', 'atslegvards', array('width' => 200));?>
          <?php print_th('Cena', 'price', array('width' => 70));?>
          <?php print_th('Nozare', 'nozare_id', array('width' => 300));?>
          <?php print_th('Reklāma vismaz līdz', 'reklama_lidz', array('width' => 120));?>
        </tr>
        <tr class="filter last">
          <th></th>
          <th><span><input name="fnosaukums" class="sfield" type="input" value="<?= getGet('fnosaukums') ?>" size="270" style="max-width: 270px;"><a class="sfield_clear" href="#">x</a></span></th>
          <th><span><input name="fatslegv" class="sfield" type="input" value="<?= getGet('fatslegv') ?>" size="170" style="max-width: 170px;"><a class="sfield_clear" href="#">x</a></span></th>
          <th></th>
          <th><span>
            <?php
              $parents = db_get_assoc_all(db_query('SELECT * FROM '.DB_PREF.'nozares WHERE parent_id = 0'));

              $children = db_get_assoc_all(db_query('SELECT * FROM '.DB_PREF.'nozares WHERE parent_id != 0'));

              $p_children = array();

              foreach($children as $child) {
                if(!isset($p_children[$child['parent_id']])) {
                  $p_children[$child['parent_id']] = array($child);
                } else {
                  $p_children[$child['parent_id']][]= $child;
                }
              }
            ?>
            <select name="fnozare[]" class="sfield" multiple="multiple" size="1" style="width: 120px;">
              <option value=""></option>
              <?php
              foreach($parents as $parent) {
                echo '<optgroup label="'.$parent['title_lv'].'">';

                if(isset($p_children[$parent['id']])) {
                  foreach((array)$p_children[$parent['id']] as $child) {
                    echo '<option value="'.$child['id'].'" '. (isset($_GET['fnozare']) && is_array($_GET['fnozare']) && in_array($child['id'], $_GET['fnozare']) ? 'selected' : '') . '>' . $child['title_lv'] . '</option>';
                  }
                }

                 echo '</optgroup>';
              } ?>
            </select>
            <a href="#" onclick="showNozareSelection();return false;">Izvēlēties</a>
            <a class="sfield_clear" href="#">x</a>
          </span></th>
          <th>
            <span><input name="flig_lidz" class="sfield kalendari" type="input" value="<?= getGet('flig_lidz') ?>" style="width:80%;"><a class="sfield_clear" href="#">x</a></span>
          </th>
        </tr>
      </thead>
      <tbody>
        <?php
        //  Attēlojam tabulā
        if(!empty($unikalie_av)) {
          $count = count($unikalie_av);
          foreach($unikalie_av as $row) {
            //  Cena
            $datediff = strtotime($row['reklama_lidz']) - time();
            $days = floor($datediff/(60*60*24));


            $price = (365 - $days) * (55/365);
            $price = ceil($price);
            if($price < 10) {
              $price = 10;
            }
            ?>
            <tr>
              <td><?=$count;?></td>
              <td><a href="?c=ligumi&a=labot&id=<?=$row['liguma_id'];?>&subtab=1"><b><?=$row['nosaukums'];?></b></a></td>
              <td><?=$row['atslegvards'];?></td>
              <td><?=format_currency($price);?> EUR</td>
              <td><?=$row['nozare'];?></td>
              <td><?=date('d.m.Y', strtotime($row['reklama_lidz']));?></td>
            </tr>
          <?php
            $count--;
          }
        } else { ?>
          <td><td colspan="4" class='error'>Nav unikālu atslēgvārdu.</td></td>
        <?php }
        ?>
      </tbody>
    </table>
  </form>
  <div id = 'nmi' class="nozares_multi_izvele" style="display:none">
      <?php
      $nozare_ids = (array)getGet('fnozare');
      $parents = db_get_assoc_all(db_query('SELECT * FROM '.DB_PREF.'nozares WHERE parent_id = 0 ORDER BY title_lv'));
      $children = db_get_assoc_all(db_query('
        SELECT *
        FROM '.DB_PREF.'nozares
        WHERE parent_id != 0
        ORDER BY title_lv
      '));

      $active_nozares = array();
      foreach((array)$unikalie_av as $av) {
        if($av['nozare_id']) {
          $active_nozares[] = $av['nozare_id'];
        }
      }

      $active_parents = array();

      $p_children = array();

      foreach($children as $child) {
        /*$avs = db_get_assoc_all(db_query('
          (SELECT id FROM ' . DB_PREF .'pielikumi_infomedia WHERE nozare_id = ' . esc($child['id']) . ')
          UNION
          (SELECT id FROM ' . DB_PREF .'pielikumi_yandex WHERE nozare_id = ' . esc($child['id']) . ')
          UNION
          (SELECT id FROM ' . DB_PREF .'pielikumi_tiessaite WHERE nozare_id = ' . esc($child['id']) . ')
          UNION
          (SELECT id FROM ' . DB_PREF .'pielikumi_citi WHERE nozare_id = ' . esc($child['id']) . ')
          '));

        if(!empty($avs)) {
          $child['has_av'] = true;
        } else {
          $child['has_av'] = false;
        }  */

        if(in_array($child['id'], $active_nozares)) {
          $child['has_av'] = true;
          $active_parents[] = $child['parent_id'];
        } else {
          $child['has_av'] = false;
        }

        if(!isset($p_children[$child['parent_id']])) {
          $p_children[$child['parent_id']] = array($child);
        } else {
          $p_children[$child['parent_id']][]= $child;
        }
      }

      foreach($parents as $parent) { ?>
        <h2 style="cursor:pointer;" class="<?php echo in_array($parent['id'], $active_parents) ? 'has_subcats' : '';?>" onclick="$(this).next().toggle();"><?php echo $parent['title_lv'];?></h2>

          <div style="display:none" class="maincat">

          <?php if(isset($p_children[$parent['id']])) { ?>
            <ul>
            <?php foreach((array)$p_children[$parent['id']] as $child) { ?>
              <li class="<?php echo $child['has_av'] ? 'has_av' : '';?>">
                <input type="checkbox" class="fnozare_check" name="fnozare[]" value="<?php echo $child['id'];?>" <?php echo in_array($child['id'], $nozare_ids) ? 'checked="checked"' : '';?> />
                <a href="#" class="fnozare_title" onclick="$(this).prev('.fnozare_check').prop('checked', !$(this).prev('.fnozare_check').prop('checked'));return false;"><?php echo $child['title_lv'];?></a>
              </li>
            <?php } ?>
            </ul>
          <?php } ?>

          </div>
      <?php } ?>


    <input type="submit" class="subm" value="Filtrēt" />
  </div>
  <script type="text/javascript">
    $('.fnozare_check').change(function(){check_statuses()});
    $('.fnozare_title').click(function(){check_statuses()});

    $('.nozares_multi_izvele .subm').click(function(){
      $('.nozares_multi_izvele .fnozare_check:checked').each(function(){
        var chb = $(this);
        $('#fullformplace').append(
          $('<input>').attr('type', 'hidden').attr('name', 'fnozare[]').attr('value', chb.val())
        );
      });
      $('#nmi').dialog('close');
      $('#fullformplace').submit();

      return false;
    });

    function check_statuses() {
      $('.nozares_multi_izvele h2').removeClass('has_checked');
      $('.nozares_multi_izvele h2').each(function(){
        if($(this).next('div').find('.fnozare_check:checked').length) {
          $(this).addClass('has_checked');
        }
      });
    }

     $('.sfield').change(function(){
      $('#fullformplace').submit();
     })
     initPlainSortableTable($('#fullformplace'));

     function showNozareSelection(){
         $('#nmi').dialog({
          title: 'Nozaru filtrs',
          autoOpen: true,
          width: 500,
          height: $(window).height() - 50,
          modal: true,
          resizable: false,
          draggable: false,
          closeOnEscape: false
      }).width(470);
     }
  </script>