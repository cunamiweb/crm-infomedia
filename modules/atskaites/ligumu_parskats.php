<?php
if (!check_access('atskaites-av-stati')) {
  die();
}

log_add("atvera", "Atvēra līgumu pārskata atskaiti");
?>

<form method="get" action="">

  <input type="hidden" name="getLigumiParskatsXls" value="1" />

  Datums no: <input class="kalendari" id="datums_no" name="fdatums_no" type="input" style="width:80px;">
  Datums līdz: <input class="kalendari" id="datums_lidz" name="fdatums_lidz" type="input" style="width:80px;">
  <input class="ui-state-default ui-corner-all" type="submit" value="Atlasīt" />

</form>

<script type="text/javascript">
$(".kalendari").datepicker({dateFormat:'dd.mm.yy'});
</script>