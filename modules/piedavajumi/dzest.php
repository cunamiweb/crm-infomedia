<?php
$id = getGet('id');

if($id) {
  $piedavajums = Piedavajums::model()->findByPk($id);

  //  Var dzēst tikai tos kas nekad nav bijuši apstiprināti (savus)  vai ja ir speciālas tiesības
  if(($piedavajums->statuss == 0 && $piedavajums->pardevejs_id == $_SESSION['user']['id']) || check_access('piedavajumi-dzest')) {
    $log_data['piedavajums']['old'] = $piedavajums->asArray();
    $log_data['piedavajums']['title'] = 'Izdzēsa piedāvājumu';

    if($piedavajums->delete()) {
      log_add('laboja', $log_data);
      header('Location: ?c=piedavajumi');
    } else {
      die('Piedāvājumu neizdevās izdzēst');
    }
  } else {
    die('Nedrīkst dzēst piedāvājumu, kas jau ticis apstiprināts.');
  }
}
?>