<?php
$id = getGet('id');

if($id) {
  $atslegvards = Atslegvards::model()->findByPk($id);
  $piedavajums = $atslegvards->getPiedavajums();

  if(check_access('piedavajumi-admin') || $piedavajums->pardevejs_id == $_SESSION['user']['id']) {
    //  Pietiek tiesibu
    if($atslegvards->delete()) {

      //  Ja labo ne-admins un bija iepriekš apstiprināts tad statusu resetojam uz "neapstiprinats pēc apstiprinājuma"
      if(!check_access('piedavajumi-admin')) {
        $piedavajums->statuss = $piedavajums->statuss ? -1 : 0;
        $piedavajums->requested = 0;
        $piedavajums->save();
      }

      //  Ja standarta cena tad uzreiz apstiprināts
      if($piedavajums->isStandard()) {
        $piedavajums->confirm();
      }

      //  Nosūta atgadinajumu
      //piedavajums_notify($piedavajums);

      //  Pievieno komentāru, ja piedāvājums jau reiz ticis apstiprināts un nav standarta
      if($piedavajums->statuss != 0 && !$piedavajums->isStandard()) {
        $komentars = new Komentars;
        $komentars->objekta_id = $piedavajums->id;
        $komentars->objekta_tips = 'piedavajums';
        $komentars->pardeveja_id = $_SESSION['user']['id'];
        $komentars->komentars = 'Izdzēsa a/v "' . $atslegvards->atslegvards . '"';
        $komentars->save();
      }

      header('Location: ?c=piedavajumi&a=labot&id=' . $piedavajums->id);
    } else {
      die('Atslēgvārdu neizdevās izdzēst.');
    }
  }
}
?>