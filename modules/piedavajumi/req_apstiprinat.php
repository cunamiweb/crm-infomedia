<?php
$id = getGet('id');
if($id) {
  $piedavajums = Piedavajums::model()->findByPk($id);

  $atslegvardi = $piedavajums->getAtslegvardi();
  if(empty($atslegvardi)) {
    die('Nevar pieprasīt apstiprinājumu piedāvājumam, kuram nav atslēgvārdu.');
  }

  if($piedavajums->requested) {
    die('Piedāvājumam jau ticis nosūtīts apstiprināšanas pieprasījums');
  }

  $piedavajums->requested = 1;

  if($piedavajums->save()) {
    //  Nosūta paziņojumu adminam
    piedavajums_notify($piedavajums);
    header('Location: ?c=piedavajumi');
  } else {
    die('Neizdevās nosūtīt pieprasījumu');
  }
}
?>