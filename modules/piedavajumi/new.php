<?php

$id = getGet('id');

if($a == 'labot' && $id) {
  $piedavajums = Piedavajums::model()->findByPk(getGet('id'));
  log_add("atvera", "Atvēra piedāvājumu " . $piedavajums->klients . ' (' . $piedavajums->datums . ')');
} else {
  $piedavajums = new Piedavajums;
}

require("new_process.php");

if($id) {
  if(!check_access('piedavajumi-admin') && $piedavajums->pardevejs_id != $_SESSION['user']['id']) {
    die('Jums nav piekļuves tiesību šim piedāvājumam.');
  }
}

$atslegvardi = $piedavajums->getAtslegvardi();

//  Visi aktīvie pārdevēji
$pardeveji = Pardevejs::model()->sales()->active()->findAll();

$visi_lietotaji = Pardevejs::model()->findAll();

$mekletaji = Mekletajs::model()->findAll();

if (!check_access('piedavajumi-krievu-valoda') and $_SESSION['user']['tips']!='admin'){
	unset($mekletaji[4],$mekletaji[5]);
}
?>

<iframe src="" style="display:none;" id="downloadframe" name="downloadframe"></iframe>

<?php show_autodownload_iframes() ?>

<!-- Prototypes -->
<div style="display:none;">
  <div id="komentarsprot_piedavajums">
    <table class="comment_form">
      <tr>
        <td width="70">Komentārs:</td>
        <td>
          <textarea name="komentari_new[piedavajums][komentars][]" rows="2" cols="30"></textarea>
        </td>
      </tr>
      <? if (!empty($fields)) { ?>
      <tr>
        <td>Lauks:</td>
        <td>
          <select name="komentari_new[piedavajums][formas_lauka_id][]">
            <option value="">Nepiesaistīts laukam</option>
            <? foreach($fields as $row) { ?>
              <option value="<?= $row['id'] ?>"><?= $row['nosaukums'] ?></option>
            <? } ?>
          </select>
        </td>
      </tr>
      <? } ?>
    </table>
  </div>

  <div id="atslegvards">
    <form class="av_form" name="add-atslegvards" method="POST" action="/?c=piedavajumi&a=labot&id=<?=$_REQUEST['id']; ?>">
      <div class="new_av_list">
        <div class="atslv_cont" style="margin-bottom: 2px;">
          <input style="width: 250px;" type="text" name="Atslegvards[][atslegvards]" class="atslegvards_atslegvards" value=""> <a href="#" onclick="$(this).parent().remove();">x</a>
          </div>
      </div>
      <button onclick="$('.atslv_cont:first').clone().appendTo('.new_av_list'); return false;" class="ui-state-default ui-corner-all">Vēlviens a/v</button>
      <input type="submit" value="Pievienot" />
      <input type="hidden" name="owner_id" value="<?php echo $piedavajums->id;?>" />
    </form>
  </div>

  <div id="av_no_nozares">
    <form class="av_form" name="av-no-nozares" method="GET" action="">
      <input type="hidden" name="c" value="piedavajumi" />
      <input type="hidden" name="a" value="add_from_std" />
      <input type="hidden" name="id" value="<?php echo $piedavajums->id;?>" />

      <table class="data_form layout1">
        <tr><td class="label" width="100">Atslēgvārds</td><td><?php echo nozare_select_box('nozare_id', 0, array('id' => 'nozare_id')); ?></td></tr>
        <tr>
          <td colspan="2">
            <table cellpadding="3" cellspacing="0" class="std_varianti data" style="display:none;">
              <thead>
                <th></th>
                <th class="header">Atslēgas vārds</th>
                <th class="header">Meklētājs</th>
                <th class="header">Valsts</th>
              </thead>
              <tbody class="main">
                <tr class="prototype" style="display:none;">
                  <td class="chb"></td>
                  <td class="atslegvards"></td>
                  <td class="mekletajs"></td>
                  <td class="valsts"></td>
                </tr>
              </tbody>
            </table>
            <input class="submit" style="display:none;" type="submit" value="Pievienot piedāvājumam" />
          </td>
        </tr>
      </table>
    </form>
  </div>

  <div id="av_no_nozares2">
    <?php
    $mekletajs = $piedavajums->getMekletajs();
    $country_id = $piedavajums->getMekletajs() ? $mekletajs->getValsts()->id : 1;

    $parents = Nozare::model()->findAll(array(
      'conditions' => array(
        'parent_id = 0',
        'country = ' . $country_id,
      ),
      'order' => 'title_lv',
    ));

    $unordered_children = Nozare::model()->findAll(array(
      'conditions' => array(
        'parent_id != 0',
        'country = ' . $country_id,
      ),
      'order' => 'title_lv'
    ));

    $children = array();

    foreach($unordered_children as $child) {
      if(!isset($children[$child->parent_id])) {
        $children[$child->parent_id] = array($child);
      } else {
        $children[$child->parent_id][]= $child;
      }
    }

    $rows = db_get_assoc_all(db_query('SELECT id, atslegvards, meklejumi, nozare_id FROM ' . DB_PREF . 'piedavajumi_av WHERE tips = "std"'));  //  ātrdarbības dēļ

    $nozares_in_bold = array();
    $standarta_piedavajumi = array();

    foreach($rows as $sp) {
      if(isset($unordered_children[$sp['nozare_id']])) {
        $nozares_in_bold[] = $unordered_children[$sp['nozare_id']]->id;
        $nozares_in_bold[] = $unordered_children[$sp['nozare_id']]->parent_id;
      }

      if(!isset($standarta_piedavajumi[$sp['nozare_id']])) {
        $standarta_piedavajumi[$sp['nozare_id']] = array($sp);
      } else {
        $standarta_piedavajumi[$sp['nozare_id']][]= $sp;
      }
    }
    ?>
    <form class="av_form" name="av-no-nozares" method="GET" action="">
      <input type="hidden" name="c" value="piedavajumi" />
      <input type="hidden" name="a" value="add_from_std" />
      <input type="hidden" name="id" value="<?php echo $piedavajums->id;?>" />

      <div id="nozares_list">
        <?php foreach($parents as $parent) { ?>
        <h2 style="cursor:pointer; font-weight: <?php echo in_array($parent->id, $nozares_in_bold) ? 'bold' : 'normal';?>" onclick="$(this).next().toggle();"><?php echo $parent->title_lv;?></h2>
        <div style="display:none" class="maincat">
          <?php if(isset($children[$parent->id])) { ?>
            <ul>
            <?php foreach((array)$children[$parent->id] as $child) { ?>
              <li id="subcat_<?php echo $child->id;?>">
                <h3 onclick="$(this).next().toggle()" style="cursor:pointer; font-weight: <?php echo in_array($child->id, $nozares_in_bold) ? 'bold' : 'normal';?>"><?php echo $child->title_lv;?></h3>
                <div style="display:none" class="avs">
                  <?php
                  $avs = $standarta_piedavajumi[$child->id];
                  if(!empty($avs)) { ?>
                    <table cellpadding="3" cellspacing="0" class="std_varianti data">
                      <thead>
                        <th width="40"></th>
                        <th class="header">Atslēgas vārds</th>
                        <th class="header">Meklējumi</th>
                      </thead>
                      <tbody class="main">
                        <?php foreach($avs as $av) { ?>
                        <tr>
                          <td class="c chb"><input type="checkbox" name="std[]" value="<?php echo $av['id'];?>"></td>
                          <td class="atslegvards"><?php echo $av['atslegvards'];?></td>
                          <td class="r"><?php echo $av['meklejumi'];?></td>
                        </tr>
                        <?php } ?>
                      </tbody>
                    </table>
                  <?php } ?>
                </div>
              </li>
            <?php } ?>
            </ul>
          <?php } ?>
        </div>
      <?php } ?>
      </div>
      <input type="submit" value="Pievienot" />
    </form>
  </div>

  <div id="change_totals">
    <form method="POST" action="" onsubmit="return validateTotals(this);">
      <table class="data_form layout1">
        <tr><td class="label" width="150">Standarta cena</td><td><input type="text" name="Totals[cena]" id="totals_cena" value=""></td></tr>
        <tr><td class="label" width="150">Spec cena</td><td><input type="text" name="Totals[cena_s]" id="totals_cena_s" value=""></td></tr>
        <tr><td class="label" width="150">Top cena</td><td><input type="text" name="Totals[cena_t]" id="totals_cena_t" value=""></td></tr>
        <tr><td class="label" width="150">Paskaidrojums</td><td><textarea cols="30" name="Totals[comment]" id="totals_comment"></textarea></td></tr>
      </table>
      <input type="submit" value="Mainīt" />
    </form>
  </div>
</div>

<button id="labotdzestbutton" onClick="return editoradd(this, <?php echo is_admin() ? 1 : 0 ?>, <?php echo is_limited() ? $_SESSION['user']['ierobezots'] : 0 ?>)">Labot</button>

<form action="?c=piedavajumi<?= !empty($id) ? '&a=labot&id=' . $id : '&a=jauns' ;?>" id="fullformplace" method="post" enctype="multipart/form-data">
  <?php if($id) { ?>
    <input type="hidden" name="Piedavajums[id]" id="piedavajums_id" value="<?php echo $id;?>">
  <?php } ?>
  <table width="750" class="data_form layout1">

      <tr><td class="label" width="170">Klients:</td><td><input class="non_admin_edit limited_admin_edit" type="input" id="piedavajums_klients" name="Piedavajums[klients]" value="<?php echo $piedavajums->klients;?>"></td></tr>
      <tr><td class="label" width="170">Kontaktpersona:</td><td><input class="non_admin_edit limited_admin_edit" type="input" id="piedavajums_kontaktpersona" name="Piedavajums[kontaktpersona]" value="<?php echo $piedavajums->kontaktpersona;?>"></td></tr>
      <tr><td class="label" width="170">E-pasts:</td><td><input class="non_admin_edit limited_admin_edit" type="input" id="piedavajums_epasts" name="Piedavajums[epasts]" value="<?php echo $piedavajums->epasts;?>"></td></tr>
      <tr><td class="label" width="170">Telefons:</td><td><input class="non_admin_edit limited_admin_edit" type="input" id="piedavajums_telefons" name="Piedavajums[telefons]" value="<?php echo $piedavajums->telefons;?>"></td></tr>
      <tr><td class="label" width="170">Meklētājs:</td>
        <td>
          <select class="non_admin_edit limited_admin_edit" id="piedavajums_mekletajs_id" name="Piedavajums[mekletajs_id]">
            <option value="">- Izvēlēties -</option>
            <?php foreach($mekletaji as $mekletajs) {?>
              <option value="<?php echo $mekletajs->id;?>" <?php echo $mekletajs->id == $piedavajums->mekletajs_id ? 'selected="SELECTED"' : '';?>><?php echo $mekletajs->nosaukums;?></option>
            <?php } ?>
          </select>
        </td>
      </tr>

      <?php if(check_access('piedavajumi-admin')) { ?>
        <tr><td class="label" width="170">Menedžeris:</td>
        <td>
          <select class="limited_admin_edit" id="piedavajums_pardevejs_id" name="Piedavajums[pardevejs_id]">
            <option value="">- Izvēlēties -</option>
            <?php foreach($pardeveji as $pardevejs) {  ?>
              <option value="<?php echo $pardevejs->id;?>" <?php echo $pardevejs->id == $piedavajums->pardevejs_id ? 'selected' : '';?>><?php echo $pardevejs->vards;?></option>
            <?php } ?>
          </select>
        </td></tr>
      <?php } else { ?>
        <input type="hidden" name="Piedavajums[pardevejs_id]" value="<?php echo $_SESSION['user']['id'];?>">
      <?php } ?>


   </table>

   <div class="hidethistoo">
    <input type="submit" onClick="return saveAll(this, 'piedavajums')" class="submit show_in_act_panel" value="Saglabāt">
    <input type="button" onclick="return cancelSaves();" class="show_in_act_panel" value="Atcelt">
    <?php if($id) { ?>
      <input type="button" onclick="return newAv();" class="show_in_act_panel" value="Pievienot jaunu a.v.">
      <input type="button" onclick="return newAvFromNozare();" class="show_in_act_panel" value="Pievienot jaunu a.v. no nozares">

      <?php if($piedavajums->statuss != 1 && check_access('piedavajumi-apstiprinat')) { ?>
        <input type="button" onclick="doConfirm();return false;" class="confirm show_in_act_panel" value="Apstiprināt">
      <?php } ?>

      <?php if(!$piedavajums->requested && !empty($atslegvardi) && $piedavajums->statuss != 1) {?>
        <input type="button" onclick="return sendRequest(); return false;" class="show_in_act_panel" style="font-weight: bold;" value="Sūtīt apstiprināšanai">
      <?php } ?>

      <?php if($piedavajums->statuss == 0 || check_access('piedavajumi-dzest')) { ?>
        <input type="button" onclick="return deletePiedavajums()" class="show_in_act_panel align_left" value="Dzēst piedāvājumu">
      <?php } ?>

    <? } ?>
  </div>

<?php
if($id) { ?>
  <table cellpadding="3" cellspacing="0" id="atslegvardi_list" class="data ckey_pieteikumi_list" style="width: auto;">
    <thead>

      <tr class="header">
          <th width="20" class="header" colname="id">Nr.</th>
          <th class="header">Atslēgas vārds</th>
          <th class="header">Meklēšanas skaits</th>
          <th class="header">Cena par klikšķi</th>
          <th class="header">Standarta cena</th>

          <?php if(check_access('piedavajumi-admin') || ($piedavajums->statuss == 1 && $piedavajums->s_variants)) { ?>
            <th class="header">Spec cena</th>
          <?php } ?>

          <?php if(check_access('piedavajumi-admin') || ($piedavajums->statuss == 1 && $piedavajums->t_variants)) { ?>
            <th class="header">Top cena</th>
          <?php } ?>

          <th  class="hidethistoo limited_admin_edit non_admin_edit" width="165" class="header">Darbības</th>
      </tr>
    </thead>
    <tbody class="main">
      <?php
      $i = 1;
      $overrides = $piedavajums->getOverridenCena();

      $has_pos6 = false;
      $has_top3 = false;

      $valsts = $piedavajums->getMekletajs()->getValsts();

      foreach((array)$atslegvardi as $av) {
        $poz3 = is_3poz($av->atslegvards, $valsts->country_code);
        $top3 = is_top3($av->atslegvards, $valsts->country_code);

        if(!empty($poz3)) {
          $has_pos6 = true;
        }

        if($piedavajums->t_variants && $top3) {
          $has_top3 = true;
        }

      ?>
        <tr <?php echo $i%2 ? 'class="odd"' : '';?>>
          <td><?php echo $i++;?>.</td>
          <td class="av">
            <a href="?c=nozares&a=cenu_izmainas&id=<?php echo $av->id;?>"><?php echo $av->atslegvards;?></a>
            <?php if(!empty($poz3)) { ?>
              <?php if(check_access('nozares-aiznemtie-av')) { ?>
                <span class="warn_ico_dynwidth" style="margin-top: 2px;"><a href="?c=ligumi&fligumu_id=<?php echo implode(',', $poz3);?>" target="_blank">3 poz</a></span>
              <?php } else { ?>
                <span class="warn_ico_dynwidth" style="margin-top: 2px;">3 poz</span>
              <?php } ?>
            <?php } ?>

            <?php if(!empty($top3)) { ?>
              <?php if(check_access('nozares-aiznemtie-av')) { ?>
                <span class="warn_ico_dynwidth" style="margin-top: 2px;"><a href="?c=ligumi&fligumu_id=<?php echo implode(',', $top3);?>" target="_blank">top 3</a></span>
              <?php } else { ?>
                <span class="warn_ico_dynwidth" style="margin-top: 2px;">top 3</span>
              <?php } ?>
            <?php } ?>
          </td>
          <td class="r"><?php echo $av->meklejumi;?></td>
          <td class="r"><?php echo $av->google_cpc;?></td>
          <td class="r">
            <?php if($av->override) { ?>
              <span class="bez_pvn_rek_ico" style="margin-top: 2px;">!</span>
            <?php } ?>
            <?php echo check_access('piedavajumi-cenas-detail') ? format_currency($av->cena()) : '&nbsp;';?>
          </td>

          <?php if(check_access('piedavajumi-admin') || ($piedavajums->statuss == 1 && $piedavajums->s_variants)) { ?>
            <td class="r">
              <?php if($av->override_s) { ?>
                <span class="bez_pvn_rek_ico" style="margin-top: 2px;">!</span>
              <?php } ?>
              <?php echo check_access('piedavajumi-cenas-detail') ? format_currency($av->cena_S()) : '&nbsp;';?>
            </td>
          <?php } ?>

          <?php if(check_access('piedavajumi-admin') || ($piedavajums->statuss == 1 && $piedavajums->t_variants)) { ?>
            <td class="r">
              <?php if($av->override_t) { ?>
                <span class="bez_pvn_rek_ico" style="margin-top: 2px;">!</span>
              <?php } ?>
              <?php echo check_access('piedavajumi-cenas-detail') ? format_currency($av->cena_T()) : '&nbsp;';?>
            </td>
          <?php } ?>

          <td class="hidethistoo limited_admin_edit non_admin_edit">
            <?php if($piedavajums->isStandard() || $piedavajums->statuss != 1) { // tikai neapstiprinātajiem un standarta ?>
              <button onclick="window.location = '?c=piedavajumi&a=update_av&id=<?php echo $av->id;?>'; return false;" class="ui-state-default ui-corner-all non_admin_edit">Atjaunināt</button>
            <?php } ?>
            <button onclick="deleteAv(<?php echo $av->id;?>); return false;" class="ui-state-default ui-corner-all non_admin_edit">Dzēst</button>
          </td>
        </tr>
      <?php } ?>
        <tr class="totals">
          <td class="r" colspan="2"><strong>Kopā:</strong></td>
          <td class="r"><?php echo $piedavajums->getMeklejumi();?></td>
          <td></td>
          <td class="r" id="total">
            <?php if($overrides && $overrides['cena'] != -1) { ?>
              <span class="bez_pvn_rek_ico" style="margin-top: 2px;">!</span>
              <div class="tooltip" style="display:none;">
                <?php echo $visi_lietotaji[$overrides['pardevejs_id']]->vards;?> <?php echo date('d.m.Y', strtotime($overrides['laiks'])); ?><br />
                <em><?php echo $overrides['komentars'];?></em>
              </div>
            <?php } ?>
            <strong><?php echo format_currency($piedavajums->cena());?></strong>
          </td>

          <?php if(check_access('piedavajumi-admin') || ($piedavajums->statuss == 1 && $piedavajums->s_variants)) { ?>
            <td class="r" id="total_s">
              <?php if($overrides && $overrides['cena_s'] != -1) { ?>
                <span class="bez_pvn_rek_ico" style="margin-top: 2px;">!</span>
                <div class="tooltip" style="display:none;">
                  <?php echo $visi_lietotaji[$overrides['pardevejs_id']]->vards;?> <?php echo date('d.m.Y', strtotime($overrides['laiks'])); ?><br />
                  <em><?php echo $overrides['komentars'];?></em>
                </div>
              <?php } ?>
              <strong><?php echo format_currency($piedavajums->cena_S());?></strong>
            </td>
          <?php } ?>

          <?php if(check_access('piedavajumi-admin') || ($piedavajums->statuss == 1 && $piedavajums->t_variants)) { ?>
            <td class="r" id="total_t">
              <?php if($overrides && $overrides['cena_t'] != -1) { ?>
                <span class="bez_pvn_rek_ico" style="margin-top: 2px;">!</span>
                <div class="tooltip" style="display:none;">
                  <?php echo $visi_lietotaji[$overrides['pardevejs_id']]->vards;?> <?php echo date('d.m.Y', strtotime($overrides['laiks'])); ?><br />
                  <em><?php echo $overrides['komentars'];?></em>
                </div>
              <?php } ?>
              <strong><?php echo format_currency($piedavajums->cena_T());?></strong>
            </td>
          <?php } ?>

          <td>
            <div class="hidethistoo limited_admin_edit">
            <?php if(check_access('piedavajumi-mainit-kopsummas')) { ?>
              <button onclick="changeTotals(); return false;" class="limited_admin_edit ui-state-default ui-corner-all">Mainīt kopsummas</button>
              <?php if($overrides && ($overrides['cena'] + $overrides['cena_s'] + $overrides['cena_t']) > -3) { ?>
                <button onclick="window.location='?c=piedavajumi&a=remove_override&id=<?php echo $piedavajums->id;?>'; return false;" class="limited_admin_edit ui-state-default ui-corner-all">Noņemt izmainīto</button>
              <?php } ?>
            <?php } ?>
            </div>
            <?php if($piedavajums->isStandard() || $piedavajums->statuss != 1) { // tikai neapstiprinātajiem un standarta ?>
              <button onclick="window.location = '?c=piedavajumi&a=update_av&piedavajums_all=1&piedavajums_id=<?php echo $piedavajums->id;?>'; return false;" class="ui-state-default ui-corner-all hidethistoo non_admin_edit">Atjaunināt visu</button>
            <?php } ?>
          </td>
        </tr>
      </tbody>
  </table>

  <table width="750" class="data_form layout1">
  <tr>
    <td class="label" width="170">Pieprasīt SPEC pied.:</td>
    <td style="vertical-align:top">
      <input class="non_admin_edit limited_admin_edit" type="checkbox" name="Piedavajums[s_variants]" id="piedavajums_s_variants" <?php echo $piedavajums->s_variants ? 'checked="checked"' : '';?> />
      , jo:
    </td>
    <td>
      <textarea class="non_admin_edit limited_admin_edit" id="piedavajums_s_pamatojums" style="width:400px;" rows="2" name="Piedavajums[s_pamatojums]"><?php echo $piedavajums->s_pamatojums;?></textarea>
    </td>
  </tr>
  <tr>
    <td class="label" width="170">Pieprasīt TOP pied.:</td>
    <td style="vertical-align:top">
      <input class="non_admin_edit limited_admin_edit" type="checkbox" name="Piedavajums[t_variants]" id="piedavajums_t_variants" <?php echo $piedavajums->t_variants ? 'checked="checked"' : '';?> />
      , jo:
    </td><td>
      <textarea class="non_admin_edit limited_admin_edit" id="piedavajums_t_pamatojums" name="Piedavajums[t_pamatojums]" style="width:400px;" rows="2"><?php echo $piedavajums->t_pamatojums;?></textarea>
    </td>
  </tr>
  </table>
  <?php if(!empty($atslegvardi) && $piedavajums->statuss == 1) { ?>
    <div>
      Drukāt:
      <a href="?c=piedavajumi&a=gen_pdf&id=<?php echo $id;?>&lang=lv">LV</a>
      <a href="?c=piedavajumi&a=gen_pdf&id=<?php echo $id;?>&lang=en">EN</a>
      <a href="?c=piedavajumi&a=gen_pdf&id=<?php echo $id;?>&lang=ru">RU</a>
    </div>
    <div>
      Drukāt bez cenas:
      <a href="?c=piedavajumi&a=gen_pdf&id=<?php echo $id;?>&lang=lv&partial=1">LV</a>
      <a href="?c=piedavajumi&a=gen_pdf&id=<?php echo $id;?>&lang=en&partial=1">EN</a>
      <a href="?c=piedavajumi&a=gen_pdf&id=<?php echo $id;?>&lang=ru&partial=1">RU</a>
    </div>
  <?php } ?>
  <div class="clr"></div>

  <?php
  show_comments_block('piedavajums', $id ? $id : null);
  show_files_block('piedavajums', $id ? $id : null, false, false, true, false, true);
}
?>
</form>


<script type="text/javascript">
editordisable();

$(document).ready(function() {

  $('#total .bez_pvn_rek_ico').wTooltip({
    content: $('#total .tooltip').html(),
    follow: true,
    offsetY: 0,
    offsetX: 10
  });

  $('#total_s .bez_pvn_rek_ico').wTooltip({
    content: $('#total_s .tooltip').html(),
    follow: true,
    offsetY: 0,
    offsetX: 10
  });

  $('#total_t .bez_pvn_rek_ico').wTooltip({
    content: $('#total_t .tooltip').html(),
    follow: true,
    offsetY: 0,
    offsetX: 10
  });

  <?php if(getGet('is_removed')) { ?>
    alert('Viens vai vairāki a/v netika pievienoti nepietiekama meklējumu skaita dēļ!');
  <?php } ?>
})

function doConfirm()
{
    var question = '';

  <?php if((isset($has_pos3) && $has_pos3) || (isset($has_top3) && $has_top3)) { ?>
    var question = '';

    <?php if(isset($has_pos3) && $has_pos3) { ?>
      question += 'Šajā piedāvājumā ir a/v, kuri jau ir vismaz 3 aktīvos līgumos'
    <?php } ?>

    <?php if(isset($has_top3) && $has_top3) { ?>
      question += '\nŠajā piedāvājumā ir a/v, kuri jau ir vismaz 1 aktīvā līgumā ar Top 3 pozīciju'
    <?php } ?>

    question += '\nVai apstiprināt piedāvājumu?'

    var conf = confirm(question);
  <?php } else { ?>
    var conf = 1;
  <?php } ?>

  if(conf) {
    window.location='?c=piedavajumi&a=apstiprinat&id=<?php echo $id;?>';
  }
}

function changeTotals()
{
  var c = $("#total strong").html();
  var cs = $("#total_s strong").html();
  var ct = $("#total_t strong").html();

  var form = $('#change_totals');

  form.find("#totals_cena").val(c);
  form.find("#totals_cena_s").val(cs);
  form.find("#totals_cena_t").val(ct);

  form.dialog({title:"Kopsummu maiņa", width:500});
}

function sendRequest()
{
  var conf = confirm('Vai sūtīt piedāvājumu apstiprināšanai?');
  if(conf) {
    window.location = '?c=piedavajumi&a=sutit_apstiprinasanai&id=<?php echo $id;?>';
  }
}

function newAv()
{
  var forma = $("#atslegvards").clone();

  var form = forma.find("form").first();

	form.submit(function(e){

		if(!$(this).hasClass("valid")){
    	validateAV(this);
			e.preventDefault();
		}

	});

  forma.removeAttr('id');

  forma.dialog({title:'Jauns atslēgvārds'});
}

$('.atslegvards_atslegvards').on('keypress', function(e){
    if(e.which == 13) {
      var field = $(e.target).parent().clone()
      field.find('input').val('')

      var parent = $(e.target).parents('form')

      parent.find('.new_av_list').append(field)
      field.find('input').focus()
      return false;
    }
  })

function newAvFromNozare()
{
  var forma = $("#av_no_nozares2").clone();
  forma.removeAttr('id');
  forma.dialog({title:'Standarta piedāvājumi', width: 550});
}

function newAvFromNozare_old()
{
  var forma = $("#av_no_nozares").clone();
  forma.removeAttr('id');
  forma.dialog({title:'Standarta piedāvājumi', width: 550});

  //  Nozare izvēlēta. iegūstam un attēlojam standarta piedāvājumus
  forma.find('#nozare_id').change(function(e){
    $.getJSON('ajax.php?action=get_std_piedavajumi', {id : $(e.target).val()}, function(data){

      var table = forma.find(".std_varianti");
      table.find('tbody tr:not(.prototype)').remove();
      table.hide();
      forma.find('.submit').hide();

      for (var i = 0; i < data.length; i++) {
        var row = table.find('.prototype').clone().removeClass('prototype');

        row.find('.chb').append($('<input type="checkbox" name="std[]">').attr('value', data[i]['id']));
        row.find('.atslegvards').append(data[i]['atslegvards'])

        //alert(row);

        table.append(row.show()).show();
        forma.find('.submit').show();
      }
    })
  })
}

function deletePiedavajums()
{
  var conf = confirm('Vai tiešām dzēst šo piedāvājumu?');

  if(conf) {
    window.location = '?c=piedavajumi&a=dzest&id=<?php echo $id;?>';
  }
}

function validateTotals(form) {
  var error_msgs = '';

  if(!$("#totals_cena").val() || !$("#totals_cena_s").val() || !$("#totals_cena_t").val()) {
    error_msgs += '- Cenu jānorāda obligāti\n';
  } else if(!isNumber($("#totals_cena").val()) || !isNumber($("#totals_cena_s").val()) || !isNumber($("#totals_cena_t").val())) {
    error_msgs += '- Cenai jābūt decimālskaitlim\n';
  }

  /*if(!$("#totals_comment").val()) {
    error_msgs += '- Paskaidrojumu jānorāda obligāti';
  } */

  if(error_msgs) {
    alert(error_msgs);
    return false;
  } else {
    return true;
  }
}

function validateAV(form)
{
  var error_msgs = '';

  $(form).find("input[type='submit']").after("<img style='vertical-align:middle;margin-left: 5px;width: 20px;height: 20px;' class='loading' src='/loading.gif' />");

  $.ajax({
	  type: "POST",
	  url: 'validate.php?action=atslegvards',
	  data: $(form).serialize(),
	  success: function(data) {
	    data = jQuery.parseJSON(data);

      if (data['success']) {
	      had_errors = false;
	    } else {

	      had_errors = true;

	      $.each(data['errors'], function(tab_name, errors) {

	        $.each(errors, function(field_name, error_msg) {
	          $('#' + field_name).addClass('input_has_error');
	          if (error_msg != '') {
	            error_msgs += "- " + error_msg + "\n";
	          }
	        });
	      });
	    }

			if (had_errors) {
				$(form).find(".loading").remove();
	      if (error_msgs != '') alert(error_msgs);

	    } else {
	      $(form).addClass("valid").submit();
	    }

	  }
	});

  /*$.post('validate.php?action=atslegvards', $(form).serialize(), function(data){
    data = $.parseJSON(data);
    if (data['success']) {
      had_errors = false;
    } else {

      had_errors = true;

      $.each(data['errors'], function(tab_name, errors) {

        $.each(errors, function(field_name, error_msg) {
          $('#' + field_name).addClass('input_has_error');
          if (error_msg != '') {
            error_msgs += "- " + error_msg + "\n";
          }
        });
      });
    }

    if (had_errors) {
      if (error_msgs != '') alert(error_msgs);
      return false;
    } else {
      return true;
    }
  });*/

  //return false;
}

function deleteAv(id)
{
  var conf = confirm('Dzēst atslēgvārdu?');
  if(conf) {
    window.location = '?c=piedavajumi&a=dzest_av&id=' + id;
  }
}

<?php if(empty($id)) { ?>
  editoradd($('#labotdzestbutton'), <?= is_admin() ? 1 : 0 ?>, <?= is_limited() ? 1 : 0 ?>);
<?php } ?>

</script>