<?php
if(!check_access('piedavajumi-apstiprinat')) {
  die('Jums nav pieejas tiesību šai darbībai.');
}

$id = getGet('id');
if($id) {
  $piedavajums = Piedavajums::model()->findByPk($id);

  $piedavajums->statuss = 1;
  $piedavajums->statuss_datums = date('Y-m-d');
  $piedavajums->notification_sent = 0;

  if($piedavajums->save()) {
    header('Location: ?c=piedavajumi');
  } else {
    die('Neizdevās apstiprināt');
  }
}
?>