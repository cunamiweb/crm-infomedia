<?php

$id = getGet('id');

if(isset($_POST['Piedavajums'])) {

  if(empty($errors)) {

    $log_data = array();


    //  Menedžeris
    if(check_access('piedavajumi-admin')) {
      $pardevejs_id = $_POST['Piedavajums']['pardevejs_id'];
    } else {
      $pardevejs_id = $_SESSION['user']['id'];
    }

    //  Labo vai pievieno jaunu
    if($id) {
      // Labo

      $piedavajums = Piedavajums::model()->findByPk($id);

      $log_data['piedavajums']['old'] = $piedavajums->asArray();

      // Pārbauda vai drīkst labot
      if(check_access('piedavajumi-admin') || $piedavajums->pardevejs_id == $_SESSION['user']['id']) {

        $piedavajums->setValues($_POST['Piedavajums']);
        $piedavajums->pardevejs_id = $pardevejs_id;

        if(isset($_POST['Piedavajums']['s_variants'])) {
          $piedavajums->s_variants = 1;
        } else {
          $piedavajums->s_variants = 0;
        }

        if(isset($_POST['Piedavajums']['t_variants'])) {
          $piedavajums->t_variants = 1;
        } else {
          $piedavajums->t_variants = 0;
        }

        //  Ja labo ne-admins un bija iepriekš apstiprināts tad statusu resetojam uz "neapstiprinats pēc apstiprinājuma"
        //if(!is_admin() ) {
        $piedavajums->statuss = $piedavajums->statuss ? -1 : 0;
        $piedavajums->requested = 0;
        //}

        //  Ja standarta cena tad uzreiz apstiprināts
        if($piedavajums->isStandard()) {
          $piedavajums->confirm(false, true);
        }

        if($piedavajums->save()) {

          $log_data['piedavajums']['new'] = $piedavajums->asArray();
          $log_data['piedavajums']['title'] = sprintf('Laboja piedāvājumu klientam "%s"', $piedavajums->klients);
          log_add("laboja", $log_data);

          //  Nosūta paziņojumu adminam
          //piedavajums_notify($piedavajums);

          //  ja nomainījies meklētājs tad updeito arī av
          if($log_data['piedavajums']['old']['mekletajs_id'] != $piedavajums->mekletajs_id) {
            $ids = array();
            foreach($piedavajums->getAtslegvardi() as $atslegvards) {
              $ids[] = $atslegvards->id;
              $atslegvards->mekletajs_id = $piedavajums->mekletajs_id;
              $atslegvards->save();
            }

            header('Location: ?c=piedavajumi&a=update_av&piedavajums_id='.$piedavajums->id.'&ids[]='.implode('&ids[]=', $ids));
          }

        } else {
          die('Piedāvājumu neizdevās saglabāt');
        }

      } else {
        die('Jūs neesiet autorizēti labot šo piedāvājumu.');
      }
    } else {
      //  Jauns

      $log_data['piedavajums']['old'] = array();

      $piedavajums->setValues($_POST['Piedavajums']);
      $piedavajums->pardevejs_id = $pardevejs_id;
      $piedavajums->datums = date('Y-m-d');

      if($piedavajums->save()) {
        $log_data['piedavajums']['new'] = $piedavajums->asArray();
        $log_data['piedavajums']['title'] = 'Pievienoja jaunu piedāvājumu';
        log_add("laboja", $log_data);

        header('Location: ?c=piedavajumi&a=labot&id='.$piedavajums->id);
      } else {
        die('Piedāvājumu neizdevās saglabāt');
      }
    }
  }

  process_comments(array('piedavajums'), $id);
  process_files(array('piedavajums'), $id);
}

if(isset($_POST['Atslegvards']) && $id) {

  $piedavajums = Piedavajums::model()->findByPk($id);

  $ids = array();

  foreach($_POST['Atslegvards'] as $av) {
    $atslegvards = new Atslegvards;
    $atslegvards->setValues($av);
    $atslegvards->owner_id = $piedavajums->id;
    $atslegvards->tips = 'av';
    $atslegvards->mekletajs_id = $piedavajums->mekletajs_id;

    if($atslegvards->save()) {
      $ids[] = $atslegvards->id;
    }

    //  Ja labo ne-admins un bija iepriekš apstiprināts tad statusu resetojam uz "neapstiprinats pēc apstiprinājuma"
    //if(!is_admin()) {
    $piedavajums->statuss = $piedavajums->statuss ? -1 : 0;
    $piedavajums->requested = 0;
    $piedavajums->save();
    //}

    //  Ja standarta cena tad uzreiz apstiprināts
    if($piedavajums->isStandard()) {
      $piedavajums->confirm();
    }

    //  Nosūta atgadinajumu
    //piedavajums_notify($piedavajums);

    log_add("laboja", 'Pievienoja atslēgvārdu ' . $atslegvards->atslegvards . ' piedāvājumam ' . $piedavajums->klients . ' (' . $piedavajums->datums . ')');

    //  Pievieno komentāru ka ir jauns a/v ja šitas piedāvājums jau reiz ticis apstiprināts un nav standarta
    if($piedavajums->statuss != 0 && !$piedavajums->isStandard()) {
      $komentars = new Komentars;
      $komentars->objekta_id = $piedavajums->id;
      $komentars->objekta_tips = 'piedavajums';
      $komentars->pardeveja_id = $_SESSION['user']['id'];
      $komentars->komentars = 'Pievienoja a/v "' . $atslegvards->atslegvards . '"';
      $komentars->save();
    }
  }

  header('Location: ?c=piedavajumi&a=update_av&piedavajums_id='.$piedavajums->id.'&check_remove=1&ids[]='.implode('&ids[]=', $ids));

}

if(isset($_POST['Totals']) && $id && check_access('piedavajumi-mainit-kopsummas')) {
  if($_POST['Totals']['cena'] && $_POST['Totals']['cena_s'] && $_POST['Totals']['cena_t']) {

    $model = Piedavajums::model()->findByPk($id);

    //  Standarta cena
    $cena = str_replace(',', '.', $_POST['Totals']['cena']);
    $cena = bccomp($cena, $model->cena(true), 2) == 0 ? -1 : $cena;

    //  Standarta cena
    $cena_s = str_replace(',', '.', $_POST['Totals']['cena_s']);
    $cena_s = bccomp($cena_s, $model->cena_s(true), 2) == 0 ? -1 : $cena_s;

    //  Standarta cena
    $cena_t = str_replace(',', '.', $_POST['Totals']['cena_t']);
    $cena_t = bccomp($cena_t, $model->cena_t(true), 2) == 0 ? -1 : $cena_t;

    $sql = 'INSERT INTO ' . DB_PREF .'piedavajumi_kops_izm
      (piedavajums_id, cena, cena_s, cena_t, komentars, pardevejs_id, laiks)
      VALUES (
      ' . esc($id) . ',
      ' . $cena . ',
      ' . $cena_s . ',
      ' . $cena_t . ',
      "' . esc($_POST['Totals']['comment']) . '",
      ' . $_SESSION['user']['id'] . ',
      "' . date('Y-m-d H:i:s') . '"
      )';

    if(db_query($sql)) {
      $log_data = array('piedavajumi' => array(
        'title' => 'Mainīja piedāvājuma ' . $piedavajums->klients . ' (' . $piedavajums->datums . ') kopsummas. ' . esc($_POST['Totals']['comment']),
        'new' => array($cena, $cena_s, $cena_t),
      ));
      log_add('laboja', $log_data);
    }

  } else {
    die('Kopsummas izmaiņas neizdevās saglabāt');
  }
}



?>