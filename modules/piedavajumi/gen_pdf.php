<?php
$lang = getGet('lang') ? getGet('lang') : 'lv';

require(DOCUMENT_ROOT . '/languages/' . $lang . '/init.php');
$strings = require(DOCUMENT_ROOT . '/languages/' . $lang . '/piedavajumi/pdf.php');

$piedavajums = Piedavajums::model()->findByPk(getGet('id'));

$partial = getGet('partial') ? (int)getGet('partial') : 0;

if(!$piedavajums) {
  die('Piedāvājums nav atrasts');
} elseif(!check_access('piedavajumi-admin') && $piedavajums->pardevejs_id != $_SESSION['user']['id'])  {
  die('Jums nav pieejas tiesību šim piedāvājumam.');
}

$atslegvardi = $piedavajums->getAtslegvardi();

/*if ($piedavajums->fails) {

  db_query("DELETE FROM `".DB_PREF."faili` WHERE `id` = ".(int)$piedavajums->fails);
  $piedavajums->fails = null;
  $piedavajums->save();

  if (is_file("faili/".(int)$piedavajums->fails)) {
    unlink("faili/".(int)$piedavajums->fails);
  }

}*/

$datestring = strftime(stristr(PHP_OS,"win") ? $strings['datums_win'] : $strings['datums_unix'], strtotime($piedavajums->datums));
$datestring = sprintf($datestring, getLocalMonth(date('m', strtotime($piedavajums->datums)), $lang));     // jo setlocale nezkapēc nestrādā uz šī servera

$autodownload = array();

require_once("Zend/Pdf.php");

//  Margins
$margin_x = 36;
$margin_y = 36;

//$template = DOCUMENT_ROOT . '/rek_templates/piedavajums.pdf';

$font_size = 10; // Default

//$pdf = Zend_Pdf::load($template);
$pdf = new Zend_Pdf;

$font = Zend_Pdf_Font::fontWithPath('rek_templates/fonts/calibri.ttf');
$font_bold = Zend_Pdf_Font::fontWithPath('rek_templates/fonts/calibrib.ttf');

$grey = new Zend_Pdf_Color_GrayScale(0.5);

$page = new Zend_Pdf_Page(Zend_Pdf_Page::SIZE_A4);

//  Initial page coords
$x = $margin_x;
$y = $page->getHeight() - $margin_y;

//  Logo
//$logo = Zend_Pdf_Image::imageWithPath(DOCUMENT_ROOT . '/css/logo_adwords_'.$lang.'.png');
$logo = Zend_Pdf_Image::imageWithPath(DOCUMENT_ROOT . '/css/logo_adwords_partner.png');
$page->drawImage($logo, 0, $page->getHeight() - 70.86, 350, $page->getHeight());

$y -= 70.86;

$row_height = 18;

$pardevejs = $piedavajums->getPardevejs();

//  No
$page->setFont($font_bold, $font_size)->drawText($strings['no'] . ':', $x, $y, 'UTF-8');
$page->setFont($font, $font_size)->drawText(PIED_PDF_COMPANY, $x + 106, $y, 'UTF-8');

//  Kontaktpersona
$page->setFont($font_bold, $font_size)->drawText($strings['kontaktpersona'] . ':', $x, $y - $row_height, 'UTF-8');
$page->setFont($font, $font_size)->drawText(mb_convert_case($pardevejs->vards, MB_CASE_TITLE, "UTF-8"), $x + 106, $y - $row_height, 'UTF-8');

//  Telefons
$page->setFont($font_bold, $font_size)->drawText($strings['talrunis'] . ':', $x, $y - $row_height*2, 'UTF-8');

$phones = $pardevejs->getPhones();
if(!empty($phones)) {
  $phone = $phones[0];
  $phone = $phones[0];
} else {
  $phone = PIED_PDF_PHONE;
}

if(in_array($lang, array('ru', 'en'))) {
  if(strncmp($phone, '371', strlen('371')) && strncmp($phone, '+371', strlen('+371'))) {
    $phone = '+371'.$phone;
  }
}

$page->setFont($font, $font_size)->drawText($phone, $x + 106, $y - $row_height*2, 'UTF-8');

//  Epasts
$page->setFont($font_bold, $font_size)->drawText($strings['epasts'] . ':', $x, $y - $row_height*3, 'UTF-8');

$emails = $pardevejs->getEmails();
if(!empty($emails)) {
  $email = $emails[0];
} else {
  $email = PIED_PDF_EMAIL;
}
$page->setFont($font, $font_size)->drawText($email, $x + 106, $y - $row_height*3, 'UTF-8');

$second_col = $x + 280;

//  Kam
$page->setFont($font_bold, $font_size)->drawText($strings['kam'] . ':', $second_col , $y, 'UTF-8');
$page->setFont($font, $font_size)->drawText($piedavajums->klients, $second_col + 106, $y, 'UTF-8');

//  Kontaktpersona
$page->setFont($font_bold, $font_size)->drawText($strings['kontaktpersona'] . ':', $second_col, $y - $row_height, 'UTF-8');
$page->setFont($font, $font_size)->drawText($piedavajums->kontaktpersona, $second_col + 106, $y - $row_height, 'UTF-8');

//  Telefons
$page->setFont($font_bold, $font_size)->drawText($strings['talrunis'] . ':', $second_col, $y - $row_height*2, 'UTF-8');
$page->setFont($font, $font_size)->drawText($piedavajums->telefons, $second_col + 106, $y - $row_height*2, 'UTF-8');

//  Epasts
$page->setFont($font_bold, $font_size)->drawText($strings['epasts'] . ':', $second_col, $y - $row_height*3, 'UTF-8');
$page->setFont($font, $font_size)->drawText($piedavajums->epasts, $second_col + 106, $y - $row_height*3, 'UTF-8');

$y -= $row_height*3 + 42.5;

//  Title
$page->setFont($font_bold, 16)->drawText($strings['komercpiedavajums'], $x + 190, $y, 'UTF-8');

$y -= 26;

if(!$partial) {

  // Subtitle
  $page->setFont($font_bold, $font_size)->drawText($strings['par'] . ':', $x, $y, 'UTF-8');
  $page->setFont($font, $font_size)->drawText($strings['google_adwords_cenu_piedavajums'], $x + 31, $y, 'UTF-8');

  $y -= 26;

  //  Summary
  $page->setFont($font, $font_size)->drawText(sprintf($strings['reklamas_kampana_ieklaujot_x_atslegvardus'], count($atslegvardi)) . ':', $x, $y, 'UTF-8');

  $y -= 3;

//  Table
$table_top_left = array('x' => $x - 7, 'y' => $y);
$table_width = $page->getWidth() - $table_top_left['x'];
$col1 = 42.5;
$col3 = 75;
$col4 = 90;
$col5 = 90;
$col2 = $table_width - ($col1 + $col3 + $col4 + $col5) - 29;

$page->drawLine($table_top_left['x'], $table_top_left['y'], $table_width, $table_top_left['y']);

$y -= 15;

//  Head
//  Npk
$page->setFont($font_bold, $font_size)->drawText($strings['npk'], $x, $y, 'UTF-8');
//  Priekšmets
$page->setFont($font_bold, $font_size)->drawText($strings['prieksmets'], $x + $col1 + 70, $y, 'UTF-8');
//  Periods
$page->setFont($font_bold, $font_size)->drawText($strings['periods'], $x + $col1 + $col2 + 15, $y, 'UTF-8');
//  Summa w/o pvn
$page->setFont($font_bold, $font_size)->drawText($strings['summa_eur'], $x + $col1 + $col2 + $col3 + 10, $y + 6, 'UTF-8');
$page->setFont($font_bold, $font_size)->drawText(sprintf($strings['bez_x_pvn'], VAT), $x + $col1 + $col2 + $col3 + 3, $y - 3, 'UTF-8');
//  Summa w/ pvn
$page->setFont($font_bold, $font_size)->drawText($strings['summa_eur'], $x + $col1 + $col2 + $col3 + $col4 + 10, $y + 6, 'UTF-8');
$page->setFont($font_bold, $font_size)->drawText(sprintf($strings['ar_x_pvn'], VAT), $x + $col1 + $col2 + $col3 + $col4 + 3, $y - 3, 'UTF-8');

$y -= 6;

$page->drawLine($table_top_left['x'], $y, $table_width, $y);

//  Table body
$y -= 12;

// Priekšmets 1. rinda
$page->setFont($font, $font_size)->drawText($strings['pec_zemak_minetajiem_atslegvardiem_reklama'], $x + $col1, $y, 'UTF-8');

$y -= 15;

//  Npk
$page->setFont($font, $font_size)->drawText('1.', $x + 10, $y, 'UTF-8');
//  Priekšmets 2. rinda
$page->setFont($font, $font_size)->drawText($strings['tiks_radita_reklamas_pozicijas_bez_laika'], $x + $col1, $y, 'UTF-8');
//  Periods
$page->setFont($font, $font_size)->drawText(sprintf($strings['menesi'], 12), $x + $col1 + $col2 + 10, $y, 'UTF-8');
//  Summa w/o pvn
$page->setFont($font, $font_size)->drawText(format_currency($piedavajums->gala_cena(true)) . '*', $x + $col1 + $col2 + $col3 + 20, $y, 'UTF-8');
$page->setFont($font, $font_size)->drawText(format_currency($piedavajums->gala_cena()), $x + $col1 + $col2 + $col3 + $col4 + 20, $y, 'UTF-8');

$y -= 15;

//  Priekšmets 3.rinda
$page->setFont($font, $font_size)->drawText($strings['ierobezojuma_neatkarigi_no_kliksku_skaita'], $x + $col1, $y, 'UTF-8');

//  Summa w/o pvn
$page->setFont($font, $font_size)->drawText('EUR', $x + $col1 + $col2 + $col3 + 20, $y, 'UTF-8');

//  Summa w PVN
$page->setFont($font, $font_size)->drawText('EUR', $x + $col1 + $col2 + $col3 + $col4 + 20, $y, 'UTF-8');

$y -= 15;

//  Priekšmets 4.rinda
$page->setFont($font, $font_size)->drawText($strings['extra_line'], $x + $col1, $y, 'UTF-8');

$y -= 3;

$page->drawLine($table_top_left['x'], $y, $table_width, $y);

//  Vertikālās līnijas
$page->drawLine($table_top_left['x'], $table_top_left['y'], $table_top_left['x'], $y);
$page->drawLine($table_top_left['x'] + $col1, $table_top_left['y'], $table_top_left['x'] + $col1, $y);
$page->drawLine($table_top_left['x'] + $col1 + $col2, $table_top_left['y'], $table_top_left['x'] + $col1 + $col2, $y);
$page->drawLine($table_top_left['x'] + $col1 + $col2 + $col3, $table_top_left['y'], $table_top_left['x'] + $col1 + $col2 + $col3, $y);
$page->drawLine($table_top_left['x'] + $col1 + $col2 + $col3 + $col4, $table_top_left['y'], $table_top_left['x'] + $col1 + $col2 + $col3 + $col4, $y);
$page->drawLine($table_top_left['x'] + $col1 + $col2 + $col3 + $col4 + $col5, $table_top_left['y'], $table_top_left['x'] + $col1 + $col2 + $col3 + $col4 + $col5, $y);

$y -= 18;

$page->setFont($font, $font_size)->drawText('*' . $strings['visas_google_adwords_un_apkalposanas_izmakas_ieklautas_reklamas_kampanas_cena'], $x, $y, 'UTF-8');

$y -= 22;

}

// Subtitle
$page->setFont($font_bold, $font_size)->drawText($strings['par'] . ':', $x, $y, 'UTF-8');
$page->setFont($font, $font_size)->drawText(sprintf($strings['atslegvardu_meklesanas_statistika_google_lv'], $piedavajums->getMekletajs()->nosaukums), $x + 31, $y, 'UTF-8');

$y -= 3;

$table_top_left = array('x' => $x - 7, 'y' => $y);
$table_width = $page->getWidth() - $table_top_left['x'];
$col1 = 311.81;
$col2 = $table_width - $col1;

$page->drawLine($table_top_left['x'], $table_top_left['y'], $table_width, $table_top_left['y']);

$y -= 15;

//  Head
//  Atslēgvārds
$page->setFont($font_bold, $font_size)->drawText($strings['atslegvards'], $x + 127.5, $y, 'UTF-8');
//  Meklējumi
$page->setFont($font_bold, $font_size)->drawText($strings['vieteja_meklesana_menesi'], $x + $col1 + 42.5, $y, 'UTF-8');

$y -= 6;

$page->drawLine($table_top_left['x'], $y, $table_width, $y);

//  Body
foreach(array_slice($atslegvardi, 0, 15) as $atslegvards) {  // pirmajā lapā ietilpst tikai 15
  $y -= 15;
  //  Atslegvards
  $page->setFont($font, $font_size)->drawText($atslegvards->atslegvards, $x, $y, 'UTF-8');
  //  Meklējumi
  $page->setFont($font, $font_size)->drawText($atslegvards->meklejumi, $x + $col1 + 85, $y, 'UTF-8');

  $y -= 6;

  $page->drawLine($table_top_left['x'], $y, $table_width, $y);
}

//  Vertikālās līnijas
$page->drawLine($table_top_left['x'], $table_top_left['y'], $table_top_left['x'], $y);
$page->drawLine($table_top_left['x'] + $col1, $table_top_left['y'], $table_top_left['x'] + $col1, $y);
$page->drawLine($col1 + $col2, $table_top_left['y'], $col1 + $col2, $y);
          
if(count($atslegvardi) <= 15) { // jo jārāda tikai pēdējā lapā
  $y -= 18;

  $page->setFont($font_bold, $font_size)->drawText($strings['piedavajums_sagatavots'] . ':', $x, $y, 'UTF-8');
  //$datestring = strftime(stristr(PHP_OS,"win") ? $strings['datums_win'] : $strings['datums_unix'], strtotime($piedavajums->datums)) . ' ' . getLocalMonth(date('m', strtotime($piedavajums->datums)), $lang);
  $page->setFont($font, $font_size)->drawText($datestring, $x + 128, $y, 'UTF-8');

  $y -= 18;

  $page->setFont($font_bold, $font_size)->drawText($strings['piedavajums_derigs'] . ':', $x, $y, 'UTF-8');
  $page->setFont($font, $font_size)->drawText($strings['1_menesis'], $x + 135, $y, 'UTF-8');
}

// Footer
$page->setFont($font, $font_size)->setFillColor($grey)->drawText($strings['footer_company'], $x, 0 + $margin_y + 18, 'UTF-8');
$page->setFont($font, $font_size)->setFillColor($grey)->drawText($strings['footer_address'], $x, 0 + $margin_y, 'UTF-8');

$page->setFont($font, $font_size)->setFillColor($grey)->drawText($strings['talr_footer'], $page->getWidth() - 228, 0 + $margin_y + 18, 'UTF-8');
$page->setFont($font, $font_size)->setFillColor($grey)->drawText($strings['epasts_footer'], $page->getWidth() - 228, 0 + $margin_y, 'UTF-8');

$pdf->pages[] = $page;

//  Additional pages
if(count($atslegvardi) > 15) {
  $parts = array_chunk(array_slice($atslegvardi, 15), 29);

  $count = count($parts);
  foreach($parts as $i => $atslegvardi) {
    $page = new Zend_Pdf_Page(Zend_Pdf_Page::SIZE_A4);

    //  Initial page coords
    $x = $margin_x;
    $y = $page->getHeight() - $margin_y;

    $table_top_left = array('x' => $x - 7, 'y' => $y);
    $table_width = $page->getWidth() - $table_top_left['x'];
    $col1 = 311.81;
    $col2 = $table_width - $col1;

    $page->drawLine($table_top_left['x'], $table_top_left['y'], $table_width, $table_top_left['y']);

    $y -= 15;

    //  Head
    //  Atslēgvārds
    $page->setFont($font_bold, $font_size)->drawText($strings['atslegvards'], $x + 127.5, $y, 'UTF-8');
    //  Meklējumi
    $page->setFont($font_bold, $font_size)->drawText($strings['vieteja_meklesana_menesi'], $x + $col1 + 42.5, $y, 'UTF-8');

    $y -= 6;

    $page->drawLine($table_top_left['x'], $y, $table_width, $y);

    //  Body
    foreach($atslegvardi as $atslegvards) {  // pirmajā lapā ietilpst tikai 15
      $y -= 15;
      //  Atslegvards
      $page->setFont($font, $font_size)->drawText($atslegvards->atslegvards, $x, $y, 'UTF-8');
      //  Meklējumi
      $page->setFont($font, $font_size)->drawText($atslegvards->meklejumi, $x + $col1 + 85, $y, 'UTF-8');

      $y -= 6;

      $page->drawLine($table_top_left['x'], $y, $table_width, $y);
    }

    //  Vertikālās līnijas
    $page->drawLine($table_top_left['x'], $table_top_left['y'], $table_top_left['x'], $y);
    $page->drawLine($table_top_left['x'] + $col1, $table_top_left['y'], $table_top_left['x'] + $col1, $y);
    $page->drawLine($col1 + $col2, $table_top_left['y'], $col1 + $col2, $y);

    if($count == ($i + 1)) { // pēdējā lapa
      $y -= 18;

      $page->setFont($font_bold, $font_size)->drawText($strings['piedavajums_sagatavots'] . ':', $x, $y, 'UTF-8');
      $page->setFont($font, $font_size)->drawText($datestring, $x + 128, $y, 'UTF-8');

      $y -= 18;

      $page->setFont($font_bold, $font_size)->drawText($strings['piedavajums_derigs'] . ':', $x, $y, 'UTF-8');
      $page->setFont($font, $font_size)->drawText($strings['1_menesis'], $x + 135, $y, 'UTF-8');
    }

    // Footer
    $page->setFont($font, $font_size)->setFillColor($grey)->drawText($strings['footer_company'], $x, 0 + $margin_y + 18, 'UTF-8');
    $page->setFont($font, $font_size)->setFillColor($grey)->drawText($strings['footer_address'], $x, 0 + $margin_y, 'UTF-8');

    $page->setFont($font, $font_size)->setFillColor($grey)->drawText($strings['talr_footer'], $page->getWidth() - 228, 0 + $margin_y + 18, 'UTF-8');
    $page->setFont($font, $font_size)->setFillColor($grey)->drawText($strings['epasts_footer'], $page->getWidth() - 228, 0 + $margin_y, 'UTF-8');

    $pdf->pages[] = $page;
  }
}

/* Save */

$pdf_output = $pdf->render();

$name =  ($partial ? $strings['piedavajums_file_name_stat'] : $strings['piedavajums_file_name']) . ' ' .translit($piedavajums->klients) . ' (' . date('d.m.Y', strtotime($piedavajums->datums)) . ').pdf';
$name = prepare_filename($name);

db_query("
  INSERT INTO `".DB_PREF."faili` SET
    `objekta_id` = ".$piedavajums->id.",
    `objekta_tips` = 'piedavajums',
    `pardeveja_id` = ".$_SESSION['user']['id'].",
    `fails` = '".esc($name)."',
    `izmers` = ".strlen($pdf_output)."
");


if(!save_file($pdf_output, "faili/" . db_last_id())) {

  db_query("DELETE FROM `".DB_PREF."faili` WHERE `id` = ".db_last_id());

} else {
  $autodownload[] = db_last_id();

  $piedavajums->fails = db_last_id();
  $piedavajums->save();
}

$_SESSION['autodownload_file_ids'] = $autodownload;

header("Location: ?c=piedavajumi&a=labot&id=" . getGet('id'));

die();
?>