<?php
if(!check_access('piedavajumi')) {
  die('Jums nav piekļuves tiesību šai sadaļai.');
}

$a = empty($_GET['a']) ? "" : $_GET['a'];

switch($a){

  case "jauns": $include = "new.php";  break;
  case "labot": $include = "new.php";  break;
  case "arhivs": $include = "arhivs.php";  break;
  case "update_av": $include = "update_av.php";  break;
  case "mainit_menedzeri": $include = "change_manager.php";  break;
  case "dzest": $include = "dzest.php";  break;
  case "dzest_av": $include = "dzest_av.php";  break;
  case "remove_override": $include = "remove_override.php";  break;
  case "apstiprinat": $include = "apstiprinat.php";  break;
  case "sutit_apstiprinasanai": $include = "req_apstiprinat.php";  break;
  case "add_from_std": $include = "add_from_std.php";  break;
  case "gen_pdf": $include = "gen_pdf.php";  break;
  default: $include = "list.php";

}

include($include);

?>