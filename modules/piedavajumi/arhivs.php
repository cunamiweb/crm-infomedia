<?php
if(!check_access('piedavajumi-admin')) {
  die('Jums nav pieejas tiesību šai sadaļai.');
}

//  Nosaka apakšadaļu
$subtab = getGet('subtab') ? getGet('subtab') : 'top';
if(!in_array($subtab, array('top', 'spec', 'std'))) {
  throw new Exception('Illegal subtab');
}

$conditions = array();

//  Veids
if($subtab == 'top') {
  $conditions[] = 't_variants = 1';
} elseif($subtab == 'spec') {
  $conditions[] = 's_variants = 1';
} elseif($subtab == 'std') {
  $conditions[] = 's_variants = 0 AND t_variants = 0';
}

//  Arhīvā tikai apstiprinātie
$conditions[] = 'statuss = 1';

//  FILTRI

//  Datumi
if(isset($_GET['fstatuss_datums_no']) && $_GET['fstatuss_datums_no']) {
  $conditions[] = 'statuss_datums >= "'. date('Y-m-d', strtotime($_GET['fstatuss_datums_no'])) . '"';
}

if(isset($_GET['fstatuss_datums_lidz']) && $_GET['fstatuss_datums_lidz']) {
  $conditions[] = 'statuss_datums <= "'. date('Y-m-d', strtotime($_GET['fstatuss_datums_lidz'])) . '"';
}

if(isset($_GET['fdatums_no']) && $_GET['fdatums_no']) {
  $conditions[] = 'datums >= "'. date('Y-m-d', strtotime($_GET['fdatums_no'])) . '"';
}

if(isset($_GET['fdatums_lidz']) && $_GET['fdatums_lidz']) {
  $conditions[] = 'datums <= "'. date('Y-m-d', strtotime($_GET['fdatums_lidz'])) . '"';
}

//  Menedžeris
if(isset($_GET['fpardevejs']) && !empty($_GET['fpardevejs'])) {
  $conditions[] = 'pardevejs_id IN (' . implode(', ', $_GET['fpardevejs']) . ')';
}

//  Klients
if(isset($_GET['fklients']) && $_GET['fklients']) {
  $conditions[] = 'klients LIKE "%'.$_GET['fklients'].'%"';
}

$piedavajumi = Piedavajums::model()->findAll(array('conditions' => $conditions));

$total_rows = count($piedavajumi);
$active_page = getGet('page') ? getGet('page') : 1;
$show_in_page = isset($_GET['show_in_page']) ? getGet('show_in_page') : 30;

$piedavajumi = Piedavajums::model()->findAll(array(
  'conditions' => $conditions,
  'order' => 'statuss_datums DESC, id DESC',
  'offset' => $show_in_page ? ($active_page - 1) * $show_in_page : 0,
  'limit' => $show_in_page ? $show_in_page : 9999999,
));
?>

<div class="sub-menu">
  <span>
    <a href="?c=piedavajumi">Tekošie</a> |
    <a href="?c=piedavajumi&a=arhivs" class="active">Arhīvs</a>
  </span>
</div>

<div class="sub-menu">
  <span>
    <a href="?c=piedavajumi&a=arhivs&subtab=top" <?php echo $subtab == 'top' ? 'class="active"' : '';?>>Top</a> |
    <a href="?c=piedavajumi&a=arhivs&subtab=spec" <?php echo $subtab == 'spec' ? 'class="active"' : '';?>>Speciālie</a> |
    <a href="?c=piedavajumi&a=arhivs&subtab=std" <?php echo $subtab == 'std' ? 'class="active"' : '';?>>Standarta</a>
  </span>
</div>

<form action="" id="fullformplace" method="GET" style="clear: both;">
  <input type="hidden" name="c" value="piedavajumi" />
  <input type="hidden" name="a" value="arhivs" />
  <input type="hidden" name="subtab" value="<?php echo $subtab;?>" />
  <input type="hidden" name="order" value="<?= getGet('order') ?>" />
  <input type="hidden" name="direction" value="<?= getGet('direction') ?>" />

  <input type="hidden" name="page" value="<?= $active_page ?>" />
  <input type="hidden" name="show_in_page" value="<?= $show_in_page ?>" />

  <div>
    <?php print_paginator($total_rows, $active_page, $show_in_page); ?>

    <span>

      &nbsp;
      <?php if(check_access('piedavajumi-admin')) { ?>
        &nbsp;
        <button style="float: right; margin-right: 5px;" id="btn_ch_manag" class="ui-state-default ui-corner-all">Mainīt menedžeri</button>
        <script type="text/javascript">
          $("#btn_ch_manag").click(function(){
            $(".chb_piedavajums:checked").each(function(){
              $('#change_manager').find('form').append(
                $('<input>').attr('type', 'hidden').attr('name', 'piedavajums_id[]').val($(this).val())
              );
            })

            $('#change_manager').dialog({title:'Mainīt menedžeri'});

            return false;
          });

        </script>
      <?php } ?>
    </span>
  </div>

  <script type="text/javascript">
  $(function(){
    $("#bulk-checkbox").click(function(e){
      var state = $(e.target).prop('checked')
      $('.chb_piedavajums').each(function(){
        if(state) {
          $(this).prop('checked', true);
        } else {
          $(this).removeAttr('checked');
        }
      });
    });
  })
  </script>

  <table cellpadding="3" cellspacing="0" id="pieteikumi_kopsavilkums" class="data ckey_pieteikumi_list" style="width: auto;">

    <thead>

      <tr class="header">
          <th width="20" class="header" colname="id">Nr.</th>
          <th width="20" <?php echo !check_access('piedavajumi-admin') ? 'style="display:none;"' : '';?> class="header"></th>
          <th width="80" class="header" colname="statuss_datums">Apstiprinājuma datums</th>
          <th width="80" class="header" colname="datums">Datums</th>
          <th width="350" class="header" colname="klients">Klients</th>
          <th width="200" class="header"  colname="pardevejs_id">Menedžeris</th>
      </tr>

      <tr class="filter last">

        <th></th>

        <th <?php echo !check_access('piedavajumi-admin') ? 'style="display:none;"' : '';?>><input type="checkbox" id="bulk-checkbox"></th>

        <th>
          <span><input name="fstatuss_datums_no" class="sfield kalendari" type="input" value="<?php echo getGet('fstatuss_datums_no') ?>" style="width:80%;"><a class="sfield_clear" href="#">x</a></span><br>
          <span><input name="fstatuss_datums_lidz" class="sfield kalendari" type="input" value="<?php echo getGet('fstatuss_datums_lidz') ?>" style="width:80%;"><a class="sfield_clear" href="#">x</a></span>
        </th>

        <th>
          <span><input name="fdatums_no" class="sfield kalendari" type="input" value="<?php echo getGet('fdatums_no') ?>" style="width:80%;"><a class="sfield_clear" href="#">x</a></span><br>
          <span><input name="fdatums_lidz" class="sfield kalendari" type="input" value="<?php echo getGet('fdatums_lidz') ?>" style="width:80%;"><a class="sfield_clear" href="#">x</a></span>
        </th>


        <th><span><input name="fklients" class="sfield" type="input" onchange="$('#fullformplace').submit()" value="<?php echo getGet('fklients') ?>"><a class="sfield_clear" href="#">x</a></span></th>

        <th>
          <select name="fpardevejs[]" class="sfield" multiple="multiple" size="1" style="width: 100px;">
            <option value=""></option>
            <?php foreach($_vars['sys_pardeveji_sales_only'] as $k=>$p){ ?>
              <option <?php pt_print_selected($k, 'fpardevejs') ?> value="<?php echo $k;?>"><?php echo $p;?></option>
            <?php } ?>
          </select>
          <a class="sfield_clear" href="#">x</a>
        </th>
      </tr>

    </thead>

    <tbody class="main">
      <?php
      //$i = count($piedavajumi) - 1;
        $i = $total_rows - 1;
        foreach((array)$piedavajumi as $piedavajums) { ?>
          <tr id="tr<?php echo $i;?>" title="<?php echo $piedavajums->klients;?>">
            <td class="c"><?php echo ($i+1);?></td>
            <td class="c" <?php echo !check_access('piedavajumi-admin') ? 'style="display:none;"' : '';?>><input type="checkbox" class="chb_piedavajums" name="chb_piedavajums[]" value="<?php echo $piedavajums->id;?>"></td>
            <td class="c"><?php echo date('d.m.Y', strtotime($piedavajums->statuss_datums));?></td>
            <td class="c"><?php echo date('d.m.Y', strtotime($piedavajums->datums));?></td>
            <td><a href="?c=piedavajumi&a=labot&id=<?php echo $piedavajums->id;?>"><?php echo $piedavajums->klients;?></a></td>
            <td class="c"><?php echo $piedavajums->getPardevejs()->vards;?></td>
          </tr>
        <?php
        $i--;
      } ?>
    </tbody>
    </table>


<script type="text/javascript">
  $(document).ready(function() {

    initPlainSortableTable($('#fullformplace'));
    initTableCols($('#pieteikumi_kopsavilkums'), 'pieteikumi_kopsavilkums', <?php echo Zend_Json::encode(get_tab_kolonnas('pieteikumi_kopsavilkums')) ?>);
    initTableGroupColors($('#pieteikumi_kopsavilkums'));

 });
</script>
</form>

<!-- Prototypes -->
<div style="display:none;">
  <div id="change_manager">
    <form name="manager_ch" method="POST" action="?c=piedavajumi&a=mainit_menedzeri&archive=<?php echo $subtab;?>">
    <?php
    $pardeveji_s = Pardevejs::model()->active()->findAll(array(
      'conditions' => array(
        'tips IN ("sales", "admin")',
        '(tips != "admin" OR vaditajs = 1)'
      ),
      'order' => 'tips DESC, vards',
    ));
    ?>
    <select name="new_pardevejs_id">
      <option value="">- Izvēlēties</option>
      <?php foreach($pardeveji_s as $pardevejs) {?>
        <option value="<?php echo $pardevejs->id;?>"><?php echo $pardevejs->vards;?></option>
      <?php } ?>
    </select>
    <input type="submit" value="Mainīt" />
    </form>
  </div>
</div>