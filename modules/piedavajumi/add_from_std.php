<?php
$piedavajums = Piedavajums::model()->findByPk(getGet('id'));

if($piedavajums) {
  $stds = getGet('std');
  $avs = array();
  $is_removed = false;

  if(!empty($stds)) {
    foreach($stds as $std) {
      $std = StandartaPiedavajums::model()->findByPk($std);var_dump($std->asArray());
      if($piedavajums->mekletajs_id == $std->mekletajs_id) {

        if($std) {
          // vai ir min meklējumu skaits
          if($std->meklejumi >= get_config('min_meklejumi_av')) {
            $av = new Atslegvards;
            $av->setValues($std->asArray(), true);
            $av->id = null;
            $av->owner_id = $piedavajums->id;
            $av->tips = 'av';

            if($av->save()) {
              $avs[] = $av;
            }
          } else {
            $is_removed = true;
          }
        }
      }
    }
    //  Ja labo ne-admins un bija iepriekš apstiprināts tad statusu resetojam uz "neapstiprinats pēc apstiprinājuma"
    if(!empty($avs)) {
    //if(!is_admin() && !empty($avs)) {
      $piedavajums->statuss = $piedavajums->statuss ? -1 : 0;
      $piedavajums->requested = 0;
      $piedavajums->save();
    }

    //  Ja standarta cena tad uzreiz apstiprināts
    if($piedavajums->isStandard()) {
      $piedavajums->confirm();
    }

    //  Nosūta atgadinajumu
    //piedavajums_notify($piedavajums);

    foreach((array)$avs as $atslegvards) {
      //  Pievieno komentāru par izmaiņām, ja piedāvājums iepriekš jau bijis apstiprināts  un nav standarta
      if($piedavajums->statuss != 0 && !$piedavajums->isStandard()) {
        $komentars = new Komentars;
        $komentars->objekta_id = $piedavajums->id;
        $komentars->objekta_tips = 'piedavajums';
        $komentars->pardeveja_id = $_SESSION['user']['id'];
        $komentars->komentars = 'Pievienoja standarta a/v "' . $atslegvards->atslegvards . '"';
        $komentars->save();
      }
    }
  }
  header('Location: ?c=piedavajumi&a=labot&id='.$piedavajums->id.($is_removed ? '&is_removed=1' : ''));
}
?>