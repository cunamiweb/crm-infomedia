<?php

//  Single
$id = getGet('id');
if($id) {
  $model = Atslegvards::model()->findByPk($id);
  $piedavajums = $model->getPiedavajums();

  if($piedavajums->isStandard() || $piedavajums->statuss != 1) { // Atjauno tikai tad ja vēl nav apstiprināts vai ir standarta piedāvājums

    //  Mēneša meklējumi
    $model->updateSearchCount();

    //  CPC
    $model->updateCPC();

    //  Cenas overraidu update no std a/v, ja tādu var atrast
    $std = $model->findMatchingStd();
    if($std) {
      $model->override = $std->override;
      $model->override_s = $std->override_s;
      $model->override_t = $std->override_t;
      $model->save();
    }
  }
  header('Location: ?c=piedavajumi&a=labot&id=' . $piedavajums->id);
}

// Multi
if(getGet('piedavajums_all')) {
  $ids = modelsToList(Piedavajums::model()->findByPk(getGet('piedavajums_id'))->getAtslegvardi(), 'id');
} else {
  $ids = getGet('ids');
}

if(is_array($ids)){
	$ids = array_filter($ids);
}

if($ids && count($ids) > 0) {

  $atslegvardi = array();
  $mekletajs_ids = array();
  foreach($ids as $id) {
    $av = Atslegvards::model()->findByPk($id);
    $piedavajums = $av->getPiedavajums();

    if($piedavajums->isStandard() || $piedavajums->statuss != 1) { // Tikai ja vēl nav apstiprināts vai standarta

      $atslegvardi[] = $av;
      if(!in_array($av->mekletajs_id, $mekletajs_ids)) {
        $mekletajs_ids[] = $av->mekletajs_id;
      }
    }
  }

  if(count($mekletajs_ids) != 1) {
    die('Kļūda - dažādi meklētāji.');
  }

  $mekletajs = Mekletajs::model()->findByPk($mekletajs_ids[0]);

  $adwords = new Adwords;
  //  Search count
	
  try {
  	$lang=$mekletajs->getValsts()->language_code;
		if ($lang=='by'){
			$lang='ru';
		}
    $result = $adwords->getMonthlySearches(modelsToList($atslegvardi, 'atslegvards'), array($mekletajs->getValsts()->country_code),array($lang));

    $is_removed = false;
    foreach($atslegvardi as $atslegvards) {
      $status = 0;

      if($result && isset($result[$atslegvards->atslegvards])) {
        $atslegvards->meklejumi = $result[$atslegvards->atslegvards];

        //  Ja ir atbilstošais flag un meklējumi ir mazāk kā minimālais tad izvāc šo av
        if(getGet('check_remove') == 1 && $atslegvards->meklejumi < get_config('min_meklejumi_av')) {
          if($atslegvards->delete()) {
            $log_data['piedavajums']['old'] = $atslegvards->asArray();
            $log_data['piedavajums']['title'] = 'Atslēgvārds izņemts, jo nepietiekams meklējumu skaits';
            log_add("laboja", $log_data);

            $is_removed = true;
          }
        }

        if($atslegvards->save()) {
          $status = 1;
        }

        $adwords->addSearchesLog($atslegvards->atslegvards, $mekletajs->id, $atslegvards->meklejumi, $status);
      } else {

        $adwords->addSearchesLog($atslegvards->atslegvards, $mekletajs->id, null, -1);
      }
    }
  } catch (Exception $e) {
    foreach($atslegvardi as $atslegvards) {
      $adwords->addSearchesLog($atslegvards->atslegvards, $mekletajs->id, null, -2, $e->getMessage());
    }
    die('Kļūda sazinoties ar AdWords API: '.$e->getMessage());
  }

  //  CPC
  try {
  	$lang=$mekletajs->getValsts()->language_code;
		if ($lang=='by'){
			$lang='ru';
    }

    $result = $adwords->getCostsEstimate(modelsToList($atslegvardi, 'atslegvards'), array($mekletajs->getValsts()->country_code), array($lang));

    $kwt_result = $adwords->getIdeaCPC(modelsToList($atslegvardi, 'atslegvards'), array($mekletajs->getValsts()->country_code), array($lang));

    foreach($atslegvardi as $atslegvards) {
      $status = 0;

      if($result && isset($result[$atslegvards->atslegvards])) {

        //$cpc = $result[$atslegvards->atslegvards]['average_cpc']; // In adwords account currency
        $cpc = $kwt_result[$atslegvards->atslegvards]; // In adwords account currency
        //$cpc_converted = ADWORDS_CURRENCY != 'LVL' ? convert_currency($cpc, ADWORDS_CURRENCY, 'LVL') : $cpc;

        //$atslegvards->google_cpc = $cpc_converted;
        $atslegvards->google_cpc = $cpc;
        //$atslegvards->google_cpc = ADWORDS_CURRENCY != 'LVL' ? convert_currency($cpc, ADWORDS_CURRENCY, 'LVL') : $cpc;
        $atslegvards->google_clicks = $result[$atslegvards->atslegvards]['clicks'];

        if($atslegvards->save()) {
          $status = 1;
        }

        $adwords->addCpcLog($atslegvards->atslegvards, $mekletajs->id, $atslegvards->google_cpc, $atslegvards->google_clicks, $status);
      } else {
        $adwords->addCpcLog($atslegvards->atslegvards, $mekletajs->id, null, null, $status);
      }
    }
  } catch (Exception $e) {
    foreach($atslegvardi as $atslegvards) {
      $adwords->addCpcLog($atslegvards->atslegvards, $mekletajs->id, null, null, -2, $e->getMessage());
      die('Kļūda sazinoties ar AdWords API: '.$e->getMessage());
    }
  }

  //  Cenas overraidi no standarta pied.
  foreach($atslegvardi as $atslegvards) {
    $std = $atslegvards->findMatchingStd();
    if($std) {
      $atslegvards->override = $std->override;
      $atslegvards->override_s = $std->override_s;
      $atslegvards->override_t = $std->override_t;
      $atslegvards->save();
    }
  }

  if(getGet('piedavajums_id')) {
    header('Location: ?c=piedavajumi&a=labot&id=' . getGet('piedavajums_id') . '&is_removed=' . (isset($is_removed) ? $is_removed : false));
    die();
  }
}

?>