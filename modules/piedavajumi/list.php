<?php
$active_page = getGet('page') ? getGet('page') : 1;
$show_in_page = isset($_GET['show_in_page']) ? getGet('show_in_page') : (is_admin() ? 30 : 15);

//  Conditions
$conditions = array();

if(check_access('piedavajumi-admin')) {
  if(!isset($_GET['fstatuss'])) {
    $_GET['fstatuss'][]  = 0;
    $_GET['fstatuss'][]  = -1;   // "Neapstiprināts" ir gan 0 gan -1.
  }
}

if(!check_access('piedavajumi-admin')){
  $conditions[] = 'pardevejs_id = ' . esc($_SESSION['user']['id']);
}

//  Statuss
if(isset($_GET['fstatuss']) && !empty($_GET['fstatuss'])) {
  if(in_array('0', $_GET['fstatuss'])) {
    $_GET['fstatuss'][] = -1;   // "Neapstiprināts" ir gan 0 gan -1.
  }

  $conditions[] = 'statuss IN (' .  implode(', ', $_GET['fstatuss']) . ')';
}

//  Datumi
if(isset($_GET['fdatums_no']) && $_GET['fdatums_no']) {
  $conditions[] = 'datums >= "'. date('Y-m-d', strtotime($_GET['fdatums_no'])) . '"';
}

if(isset($_GET['fdatums_lidz']) && $_GET['fdatums_lidz']) {
  $conditions[] = 'datums <= "'. date('Y-m-d', strtotime($_GET['fdatums_lidz'])) . '"';
}

//  Menedžeris
if(isset($_GET['fpardevejs']) && !empty($_GET['fpardevejs'])) {
  $conditions[] = 'pardevejs_id IN (' . implode(', ', $_GET['fpardevejs']) . ')';
}

//  Klients
if(isset($_GET['fklients']) && $_GET['fklients']) {
  $conditions[] = 'klients LIKE "%'.$_GET['fklients'].'%"';
}

//  Tikai speciālie un top
if(check_access('piedavajumi-admin')) {
  //$conditions[] = '(t_variants = 1 OR s_variants = 1)';
}

//  Tikai pieprasītie
if(check_access('piedavajumi-admin')) {
  $conditions[] = 'requested = 1';
}

if(check_access('piedavajumi-admin')) {
  $order = 't_variants DESC, s_variants ASC, datums DESC, id DESC';
} else {
  $order = 'datums DESC, id DESC';
}

$order = 'datums DESC'; // always by date 12.04.2017


$piedavajumi = Piedavajums::model()->findAll(array(
  'conditions' => $conditions,
));

$total_rows = count($piedavajumi);

$piedavajumi = Piedavajums::model()->findAll(array(
  'conditions' => $conditions,
  'order' => $order,
  'offset' => $show_in_page ? ($active_page - 1) * $show_in_page : 0,
  'limit' => $show_in_page ? $show_in_page : 999999,
));

$pardeveji = Pardevejs::model()->findAll();
?>

<?php
//  Submenu
if(check_access('piedavajumi-admin')) { ?>
<div class="sub-menu">

      <span>

          <a href="?c=piedavajumi" class="active">Tekošie</a> |
          <a href="?c=piedavajumi&a=arhivs">Arhīvs</a>

      </span>

    </div>
<?php } ?>

<form action="" id="fullformplace" method="GET" style="clear: both;">

  <input type="hidden" name="c" value="piedavajumi" />

  <input type="hidden" name="order" value="<?= getGet('order') ?>" />
  <input type="hidden" name="direction" value="<?= getGet('direction') ?>" />

  <input type="hidden" name="page" value="<?= $active_page ?>" />
  <input type="hidden" name="show_in_page" value="<?= $show_in_page ?>" />

  <div>
    <?php print_paginator($total_rows, $active_page, $show_in_page); ?>
    <span>

      &nbsp;
      <?php if(check_access('piedavajumi-jauns')) { ?>
        <button style="float: right;" onclick="document.location='?c=piedavajumi&a=jauns'; return false;" class="ui-state-default ui-corner-all">Izveidot jaunu piedāvājumu</button>
      <?php } ?>
      <?php if(check_access('piedavajumi-admin')) { ?>
        &nbsp;
        <button style="float: right; margin-right: 5px;" id="btn_ch_manag" class="ui-state-default ui-corner-all">Mainīt menedžeri</button>
        <script type="text/javascript">
          $("#btn_ch_manag").click(function(){
            $(".chb_piedavajums:checked").each(function(){
              $('#change_manager').find('form').append(
                $('<input>').attr('type', 'hidden').attr('name', 'piedavajums_id[]').val($(this).val())
              );
            })

            $('#change_manager').dialog({title:'Mainīt menedžeri'});

            return false;
          });

        </script>
      <?php } ?>
    </span>
  </div>

  <table cellpadding="3" cellspacing="0" id="pieteikumi_kopsavilkums" class="data ckey_pieteikumi_list" style="width: auto;">

    <thead>

      <tr class="header">
          <th width="100" <?php echo !check_access('piedavajumi-admin') ? 'style="display:none;"' : '';?>></th>
          <th width="20" class="header" colname="id">Nr.</th>
          <th width="20" <?php echo !check_access('piedavajumi-admin') ? 'style="display:none;"' : '';?> class="header"></th>
          <th width="80" class="header" colname="datums">Datums</th>
          <th width="350" class="header" colname="klients">Klients</th>
          <th width="200" class="header" <?php echo !check_access('piedavajumi-admin') ? 'style="display:none;"' : '';?> colname="pardevejs_id">Menedžeris</th>
          <th width="50" class="header" colname="statuss">Statuss</th>
      </tr>

      <tr class="filter last">

        <th <?php echo !check_access('piedavajumi-admin') ? 'style="display:none;"' : '';?>></th>
        <th></th>
        <th <?php echo !check_access('piedavajumi-admin') ? 'style="display:none;"' : '';?>></th>

        <th>
          <span><input name="fdatums_no" class="sfield kalendari" type="input" value="<?php echo getGet('fdatums_no') ?>" style="width:80%;"><a class="sfield_clear" href="#">x</a></span><br>
          <span><input name="fdatums_lidz" class="sfield kalendari" type="input" value="<?php echo getGet('fdatums_lidz') ?>" style="width:80%;"><a class="sfield_clear" href="#">x</a></span>
        </th>


        <th><span><input name="fklients" class="sfield" type="input" onchange="$('#fullformplace').submit()" value="<?php echo getGet('fklients') ?>"><a class="sfield_clear" href="#">x</a></span></th>

        <th <?php echo !check_access('piedavajumi-admin') ? 'style="display:none;"' : '';?>>
          <select name="fpardevejs[]" class="sfield" multiple="multiple" size="1" style="width: 100px;">
            <option value=""></option>
            <?php foreach($_vars['sys_pardeveji_sales_only'] as $k=>$p){ ?>
              <option <?php pt_print_selected($k, 'fpardevejs') ?> value="<?php echo $k;?>"><?php echo $p;?></option>
            <?php } ?>
          </select>
          <a class="sfield_clear" href="#">x</a>
        </th>

        <th>
          <select name="fstatuss[]" class="sfield" multiple="multiple" size="1" style="width: 70px;">
            <option value=""></option>
            <option <?php pt_print_selected(1, 'fstatuss') ?> value="1">Apstiprināts</option>
            <option <?php pt_print_selected(0, 'fstatuss') ?> value="0">Neapstiprināts</option>
          </select>
          <a class="sfield_clear" href="#">x</a>
        </th>
      </tr>

    </thead>

    <tbody class="main">
      <?php
      $i = $total_rows - 1;
        foreach((array)$piedavajumi as $piedavajums) {
          $statuss = $piedavajums->statuss == 1 ? '<div class="norek_statuss_2">Apstiprināts</div>' : ($piedavajums->requested ? '<div class="norek_statuss_3">Nosūtīts</div>' : '<div class="norek_statuss_4">Neapstiprināts</div>');
          ?>
          <tr id="tr<?php echo $i;?>" title="<?php echo $piedavajums->klients;?>">
            <td class="c" <?php echo !check_access('piedavajumi-admin') ? 'style="display:none;"' : '';?>>
              <?php
              if($piedavajums->s_variants && $piedavajums->t_variants) {
                $stars = 2;
              } elseif($piedavajums->s_variants) {
                $stars = 1;
              } elseif($piedavajums->t_variants) {
                $stars = 3;
              } else {
                $stars = 0;
              }

              for($stars; $stars > 0; $stars--) { ?>
                <img src="css/star.png" />
              <?php } ?>
            </td>
            <td class="c"><?php echo ($i+1);?></td>
            <td class="c" <?php echo !check_access('piedavajumi-admin') ? 'style="display:none;"' : '';?>><input type="checkbox" class="chb_piedavajums" name="chb_piedavajums[]" value="<?php echo $piedavajums->id;?>"></td>
            <td class="c"><?php echo date('d.m.Y', strtotime($piedavajums->datums));?></td>
            <td><a href="?c=piedavajumi&a=labot&id=<?php echo $piedavajums->id;?>"><?php echo $piedavajums->klients;?></a></td>
            <td class="c" <?php echo !check_access('piedavajumi-admin') ? 'style="display:none;"' : '';?>><?php echo $pardeveji[$piedavajums->pardevejs_id]->vards;?></td>
            <td class="c"><?php echo $statuss . ($piedavajums->statuss == 1 && $piedavajums->statuss_datums ? date('d.m.Y', strtotime($piedavajums->statuss_datums)) : '');?></td>
          </tr>
        <?php
        $i--;
      } ?>
    </tbody>
    </table>

<script type="text/javascript">
$(document).ready(function() {


  initPlainSortableTable($('#fullformplace'));

  initTableCols($('#pieteikumi_kopsavilkums'), 'pieteikumi_kopsavilkums', <?php echo Zend_Json::encode(get_tab_kolonnas('pieteikumi_kopsavilkums')) ?>);

  initTableGroupColors($('#pieteikumi_kopsavilkums'));

});

</script>

<?php if(check_access('piedavajumi-admin')) {   ?>
  <h2>Pēdējie standarta cenu piedāvājumi</h2>
  <?php
  $active_page2 = getGet('page2') ? getGet('page2') : 1;
  $show_in_page2 = isset($_GET['show_in_page2']) ? getGet('show_in_page2') : 10;

  //  Conditions
  $conditions2 = array();

  $conditions2[] = 't_variants = 0 AND s_variants = 0';

  //  Datumi
  if(isset($_GET['fdatums_no2']) && $_GET['fdatums_no2']) {
    $conditions2[] = 'datums >= "'. date('Y-m-d', strtotime($_GET['fdatums_no2'])) . '"';
  }

  if(isset($_GET['fdatums_lidz2']) && $_GET['fdatums_lidz2']) {
    $conditions2[] = 'datums <= "'. date('Y-m-d', strtotime($_GET['fdatums_lidz2'])) . '"';
  }

  //  Menedžeris
  if(isset($_GET['fpardevejs2']) && !empty($_GET['fpardevejs2'])) {
    $conditions2[] = 'pardevejs_id IN (' . implode(', ', $_GET['fpardevejs2']) . ')';
  }

  //  Klients
  if(isset($_GET['fklients2']) && $_GET['fklients2']) {
    $conditions2[] = 'klients LIKE "%'.$_GET['fklients2'].'%"';
  }

  $piedavajumi2 = Piedavajums::model()->findAll(array(
    'conditions' => $conditions2,
  ));

  $total_rows2 = count($piedavajumi2);

  $piedavajumi2 = Piedavajums::model()->findAll(array(
    'conditions' => $conditions2,
    'order' => 'datums DESC, id DESC',
    'offset' => $show_in_page2 ? ($active_page2 - 1) * $show_in_page2 : 0,
    'limit' => $show_in_page2 ? $show_in_page2 : 9999999,
  ));
  ?>

  <input type="hidden" name="order2" value="<?= getGet('order2') ?>" />
  <input type="hidden" name="direction2" value="<?= getGet('direction2') ?>" />

  <input type="hidden" name="page2" value="<?= $active_page2 ?>" />
  <input type="hidden" name="show_in_page2" value="<?= $show_in_page2 ?>" />

  <div>
    <?php print_paginator($total_rows2, $active_page2, $show_in_page2, array('page2', 'show_in_page2')); ?>
  </div>

  <table cellpadding="3" cellspacing="0" id="pieteikumi_kopsavilkums2" class="data ckey_pieteikumi_list2" style="width: auto;">

    <thead>

      <tr class="header">
          <th width="20" class="header" colname="id">Nr.</th>
          <th width="80" class="header" colname="datums">Datums</th>
          <th width="350" class="header" colname="klients">Klients</th>
          <th width="200" class="header"  colname="pardevejs_id">Menedžeris</th>
      </tr>

      <tr class="filter last">

        <th></th>

        <th>
          <span><input name="fdatums_no2" class="sfield kalendari" type="input" value="<?php echo getGet('fdatums_no2') ?>" style="width:80%;"><a class="sfield_clear" href="#">x</a></span><br>
          <span><input name="fdatums_lidz2" class="sfield kalendari" type="input" value="<?php echo getGet('fdatums_lidz2') ?>" style="width:80%;"><a class="sfield_clear" href="#">x</a></span>
        </th>


        <th><span><input name="fklients2" class="sfield" type="input" onchange="$('#fullformplace').submit()" value="<?php echo getGet('fklients2') ?>"><a class="sfield_clear" href="#">x</a></span></th>

        <th>
          <select name="fpardevejs2[]" class="sfield" multiple="multiple" size="1" style="width: 100px;">
            <option value=""></option>
            <?php foreach($_vars['sys_pardeveji_sales_only'] as $k=>$p){ ?>
              <option <?php pt_print_selected($k, 'fpardevejs2') ?> value="<?php echo $k;?>"><?php echo $p;?></option>
            <?php } ?>
          </select>
          <a class="sfield_clear" href="#">x</a>
        </th>
      </tr>

    </thead>

    <tbody class="main">
      <?php
      $i = $total_rows2 - 1;
        foreach((array)$piedavajumi2 as $piedavajums) { ?>
          <tr id="tr<?php echo $i;?>" title="<?php echo $piedavajums->klients;?>">
            <td class="c"><?php echo ($i+1);?></td>
            <td class="c"><?php echo date('d.m.Y', strtotime($piedavajums->datums));?></td>
            <td><a href="?c=piedavajumi&a=labot&id=<?php echo $piedavajums->id;?>"><?php echo $piedavajums->klients;?></a></td>
            <td class="c"><?php echo $pardeveji[$piedavajums->pardevejs_id]->vards;?></td>
          </tr>
        <?php
        $i--;
      } ?>
    </tbody>
    </table>


  <script type="text/javascript">
  $(document).ready(function() {
    initTableCols($('#pieteikumi_kopsavilkums2'), 'pieteikumi_kopsavilkums2', <?php echo Zend_Json::encode(get_tab_kolonnas('pieteikumi_kopsavilkums2')) ?>);
  });

  </script>
<?php } ?>
</form>

<!-- Prototypes -->
<div style="display:none;">
  <div id="change_manager">
    <form name="manager_ch" method="POST" action="?c=piedavajumi&a=mainit_menedzeri">
    <?php
    $pardeveji_s = Pardevejs::model()->active()->findAll(array(
      'conditions' => array(
        'tips IN ("sales", "admin")',
        '(tips != "admin" OR vaditajs = 1)'
      ),
      'order' => 'tips DESC, vards',
    ));
    ?>
    <select name="new_pardevejs_id">
      <option value="">- Izvēlēties</option>
      <?php foreach($pardeveji_s as $pardevejs) {?>
        <option value="<?php echo $pardevejs->id;?>"><?php echo $pardevejs->vards;?></option>
      <?php } ?>
    </select>
    <input type="submit" value="Mainīt" />
    </form>
  </div>
</div>