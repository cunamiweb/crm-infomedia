<?php
if(!check_access('piedavajumi-admin')) {
  die('Jums nav pieejas tiesību šai darbībai.');
}

if(isset($_POST)) {

  if(!empty($_POST['piedavajums_id']) && $_POST['new_pardevejs_id']) {

    foreach($_POST['piedavajums_id'] as $piedavajums_id) {
      $piedavajums = Piedavajums::model()->findByPk($piedavajums_id);

      if($piedavajums) {

        $piedavajums->pardevejs_id = esc($_POST['new_pardevejs_id']);
        $piedavajums->save();
      }
    }
  }
}

header('Location: ?c=piedavajumi' . (isset($_GET['archive']) ? '&a=arhivs&subtab=' . $_GET['archive'] : ''));
?>