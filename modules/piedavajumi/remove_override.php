<?php
if(!check_access('piedavajumi-mainit-kopsummas')) {
  die('Jums nav pieejas tiesību šai sadaļai.');
}

$id = getGet('id');

if($id) {
  $piedavajums = Piedavajums::model()->findByPk($id);

  if(db_query('
    INSERT INTO ' . DB_PREF . 'piedavajumi_kops_izm
      (piedavajums_id, cena, cena_s, cena_t, pardevejs_id, laiks)
    VALUES
      ('.$id.', -1, -1, -1, ' . $_SESSION['user']['id'] . ', "' . date('Y-m-d H:i:s') . '")')) {

    log_add('laboja', 'Atgrieza piedāvājuma ' . $piedavajums->klients . ' (' . $piedavajums->datums . ') kopsummas uz oriģinālo.');
  }
}

header('Location: ?c=piedavajumi&a=labot&id=' . $id);
?>