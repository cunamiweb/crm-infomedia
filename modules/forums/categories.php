<?php
if(!check_access('forums-labot-kategorijas')) {
  die();
}

$categories = ForumCategory::model()->findAll();
?>
<button style="float: right; margin-top: 5px;" onclick="window.location = '?c=forums&a=jauna_kategorija'" class="ui-state-default ui-corner-all">Jauna kategorija</button>

<table cellpadding="3" cellspacing="0" id="categories">

  <thead>

    <tr class="header">

      <th width="200">Nosaukums</th>
      <th width="40">Tēmas</th>
    </tr>

  </thead>

  <?php
  $i = 1;
  ?>
  <tbody class="main">

    <? foreach($categories as $category) { ?>

      <tr <?php echo $i%2 ? 'class="odd"' : '';?>>
        <td><a href="?c=forums&a=labot_kategoriju&id=<?php echo $category->id;?>"><?php echo $category->nosaukums;?></a></td>
        <td><?php echo count($category->getTopics());?></td>
      </tr>

    <? } ?>

  </tbody>

</table>

<script>
stripeTable($('#categories'));
</script>