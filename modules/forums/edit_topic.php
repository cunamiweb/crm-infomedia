<?
if (empty($_GET['id'])) {
  die();
}

$topic_id = (int)$_GET['id'];

$main_data = db_get_assoc(db_query("
  SELECT
    f.*,
    p.vards as pardeveja_vards
  FROM `".DB_PREF."forums` f
  LEFT JOIN `".DB_PREF."pardeveji` p ON (p.id = f.pardeveja_id)
  WHERE f.id = ".$topic_id."
  LIMIT 1
"));

if (empty($main_data) || !empty($main_data['parent_id'])) {
  die();
}

if (!check_access('forums-labot-visas-temas') && $main_data['pardeveja_id'] != $_SESSION['user']['id']) {
  die();
}

require('edit_topic_process.php');

$main_data = htmlesc($main_data);

$categories = ForumCategory::model()->findAll();
?>
<h1 style="font-size:20px; margin-bottom: 0; text-align: left; margin-left: 5px;" >
  <? if (!empty($main_data['sticky'])) { ?>
    <img src="css/star.png" alt="Svarīga tēma:" />
  <? } ?>
  <?= $main_data['tema'] ?>
</h1>

<form action="" method="post" id="fullformplace" style="clear: both; margin: 20px 0;">

  <table width="700">

    <tr>
      <td>Kategorija:</td>
      <td>
        <select type="text" name="category_id" id="category_id">
          <option value="0">- Bez kategorijas -</option>
          <?php foreach((array)$categories as $category) {?>
            <option value="<?php echo $category->id;?>" <?php echo $category->id == $main_data['category_id'] ? 'selected="SELECTED"' : '';?>><?php echo $category->nosaukums;?></option>
          <?php } ?>
        </select>
      </td>
    </tr>

    <tr>
      <td>Tēma:</td>
      <td><input type="text" name="tema" id="tema" value="<?= $main_data['tema'] ?>" /></td>
    </tr>

    <tr>
      <td>Teksts:</td>
      <td><textarea rows="8" cols="40" name="saturs" id="saturs"><?= $main_data['saturs'] ?></textarea></td>
    </tr>

    <tr>
      <td colspan="2" style="text-align: right;">
        <div style="text-align: center;" id="loader_placeholder"></div>
        <input style="width:150px;" type="button" onclick="document.location='?c=forums&a=skatit&id=<?= $main_data['id'] ?>'" class="ui-state-default ui-corner-all" value="Atcelt">
        <input style="width:150px;" class="ui-state-default ui-corner-all" type="submit" onClick="return saveAll(this, 'forums_tema')" value="Saglabāt">
      </td>
    </tr>

  </table>

</form>