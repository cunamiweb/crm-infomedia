<?php
if(!check_access('forums-jauna-kategorija') && !check_access('forums-labot-kategorijas')) {
  die();
}

if(isset($_POST['ForumCategory'])) {
  $id = getGet('id');

  $log_data = array();

  if($id && check_access('forums-labot-kategorijas')) {
    //  Labo
    $model = ForumCategory::model()->findByPk($id);

    $log_data['foruma_kategorija']['old'] = $model->asArray();
    $log_data['foruma_kategorija']['title'] = sprintf('Laboja foruma kategoriju %s', $model->nosaukums);

  } elseif(check_access('forums-jauna-kategorija')) {
    //  Jauns
    $model = new ForumCategory;
    $model->izveidots = date('Y-m-d H:i:s');

    $log_data['foruma_kategorija']['title'] = sprintf('Pievienoja jaunu foruma kategoriju %s', $model->nosaukums);
  }

  if(isset($model)) {
      $model->setValues($_POST['ForumCategory']);

      $log_data['foruma_kategorija']['new'] = $model->asArray();

      if($model->save()) {

        log_add('laboja', $log_data);

        header('Location: ?c=forums&category_id=' . $model->id);
      } else {
        die('Foruma kategoriju neizdevās saglabāt');
      }
  }
}

?>