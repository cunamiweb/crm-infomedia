<?
require('list_process.php');

log_add("atvera", "Atvēra forumu");

$category_id = getGet('category_id');

$sql = "
  SELECT
    f.*,
    p.vards as pardeveja_vards,
    (
      SELECT COUNT(*)
      FROM `".DB_PREF."forums`
      WHERE parent_id = f.id
    ) as atbilzu_skaits,
    p2.vards as pedejais_atbildetajs,
    (
      SELECT COUNT(*)
      FROM `".DB_PREF."forums_atverts`
      WHERE
        temas_id = f.id AND
        pardeveja_id = ".$_SESSION['user']['id']."
    ) as tema_atverta,
    (
      SELECT COUNT(*)
      FROM `".DB_PREF."forums` sf
      LEFT JOIN `".DB_PREF."forums_atverts` sa ON (
        sa.temas_id = sf.id AND
        sa.pardeveja_id = ".$_SESSION['user']['id']."
      )
      WHERE
        sf.parent_id = f.id AND
        sa.pardeveja_id IS NULL
    ) as jaunas_atbildes
  FROM `".DB_PREF."forums` f
  LEFT JOIN `".DB_PREF."pardeveji` p ON (p.id = f.pardeveja_id)
  LEFT JOIN `".DB_PREF."pardeveji` p2 ON (p2.id = f.last_update_pardeveja_id)
  WHERE (f.parent_id IS NULL OR f.parent_id = 0)";

if($category_id) {
  $sql .= ' AND f.category_id = ' . esc(getGet('category_id'));
} else {
  $sql .= ' AND f.category_id = 0';
}

$sql .= "
  ORDER BY
    f.sticky DESC,
    f.created DESC
";

$query = db_query($sql);

$has_sticky = false;

$temas = array();

while($row = db_get_assoc($query)) {

  $temas[] = $row;

  if (!empty($row['sticky'])) {
    $has_sticky = true;
  }

}

$categories = ForumCategory::model()->findAll(array('order' => 'nosaukums'));

if($category_id) {
  $active_category = ForumCategory::model()->findByPk($category_id);

  if(!$active_category) {
    die('Neeksistējoša foruma kategorija');
  }
}

?>


<?php if(check_access('forums-jauna-tema')) { ?>
    <button style="float: right; margin-top: 5px;" onclick="$('#fullformplace').show(); window.location.hash='#fullformplace';" class="ui-state-default ui-corner-all">Jauna tēma</button>
<?php } ?>

<?php if(check_access('forums-jauna-kategorija') && !$category_id) { ?>
  <button style="float: right; margin-top: 5px; margin-right: 5px;" onclick="window.location = '?c=forums&a=jauna_kategorija'" class="ui-state-default ui-corner-all">Jauna kategorija</button>
<?php } ?>

<?php if(check_access('forums-labot-kategorijas') && $category_id) { ?>
  <button style="float: right; margin-top: 5px; margin-right: 5px;" onclick="window.location = '?c=forums&a=labot_kategoriju&id=<?php echo $category_id;?>'" class="ui-state-default ui-corner-all">Labot kategoriju</button>
<?php } ?>

<h1 style="font-size:20px; margin-bottom: 0; text-align: left; margin-left: 5px;">
  <?php if($category_id) { ?>

    <a style="font-size:20px; margin-bottom: 0; text-align: left; margin-left: 5px;" href="?c=forums">Forums</a> > <?php echo $active_category->nosaukums;?>

  <?php } else { ?>
    Forums
  <?php } ?>
</h1>

<?php if(!$category_id) { ?>
<table width="100%" cellpadding="3" cellspacing="0" class="data">

  <thead>

    <tr class="header">
      <th width="125" class="c">Izveidots</th>
      <th>Kategorija</th>
      <th width="60" class="c">Tēmas</th>
    </tr>

  </thead>
  <tbody class="main">
    <?php foreach((array)$categories as $category) { ?>
      <tr class="<?php echo $category->hasUnread() ? 'new' : '' ?>">
        <td style="white-space: nowrap; font-size: 14px;"><?php echo date('d.m.Y', strtotime($category->izveidots));?>&nbsp;&nbsp;&nbsp;<?php echo date('H:i', strtotime($category->izveidots));?></td>
        <td><a style="font-size: 16px; text-transform: uppercase; font-weight: bold;" href="?c=forums&category_id=<?php echo $category->id;?>"><?php echo $category->nosaukums;?></a></td>
        <td class="c"><?php echo count($category->getTopics());?></td>
      </tr>
    <?php } ?>
  </tbody>
</table>
<?php } ?>

<table width="100%" cellpadding="3" cellspacing="0" id="forums" class="data">

  <thead>

    <tr class="header">

      <th width="125">Pievienots</th>

      <? if ($has_sticky) { ?>
        <th width="18"></th>
      <? } ?>

      <th>Tēma</th>
      <th width="60" class="c">Atbildes</th>
      <th width="50">Autors</th>
      <th width="50">Pēdējais atbildētājs</th>
    </tr>

  </thead>

  <tbody class="main">

    <? foreach($temas as $row) { ?>

      <tr class="<?= empty($row['tema_atverta']) ? 'new' : '' ?>">

        <td style="white-space: nowrap;"><?= date('d.m.Y', strtotime($row['created'])) ?>&nbsp;&nbsp;&nbsp;<?= date('H:i', strtotime($row['created'])) ?></td>

        <? if ($has_sticky) { ?>
          <td>
            <? if (!empty($row['sticky'])) { ?>
              <img src="css/star.png" alt="Svarīga tēma" title="Svarīga tēma" />
            <? } ?>
          </td>
        <? } ?>

        <td>
          <a style="font-weight: bold;" href="?c=forums&a=skatit&id=<?= $row['id'] ?>"><?= $row['tema'] ?></a>
          <? if (empty($row['tema_atverta'])) { ?>
            <span style="color: #888888; margin-left: 10px;">jauna tēma!</span>
          <? } ?>
        </td>

        <td class="c">
          <?= $row['atbilzu_skaits'] ?>
          <? if (!empty($row['jaunas_atbildes'])) { ?>
            <span style="color: red;">(<?= $row['jaunas_atbildes'] ?>)</span>
          <? } ?>
        </td>
        <td style="white-space: nowrap;"><?= $row['pardeveja_vards'] ?></td>
        <td style="white-space: nowrap;">
          <? if (!empty($row['pedejais_atbildetajs'])) { ?>
            <?= $row['pedejais_atbildetajs'] ?>, <?= date('d.m.Y H:i', strtotime($row['last_update'])) ?>
          <? } ?>
        </td>
      </tr>

    <? } ?>

  </tbody>

</table>

<script>
stripeTable($('#forums'));
</script>

<form action="?c=forums" method="post" id="fullformplace" style="display: none; clear: both;">

  <table width="700">

    <tr>
      <td>Kategorija:</td>
      <td>
        <select type="text" name="category_id" id="category_id">
          <option value="0">- Bez kategorijas -</option>
          <?php foreach((array)$categories as $category) {?>
            <option value="<?php echo $category->id;?>" <?php echo isset($active_category) && $category->id == $active_category->id ? 'selected="SELECTED"' : '';?>><?php echo $category->nosaukums;?></option>
          <?php } ?>
        </select>
      </td>
    </tr>

    <tr>
      <td>Tēma:</td>
      <td><input type="text" name="tema" id="tema" /></td>
    </tr>

    <tr>
      <td>Teksts:</td>
      <td><textarea rows="8" cols="40" name="saturs" id="saturs"></textarea></td>
    </tr>

    <tr>
      <td colspan="2" style="text-align: right;">
        <div style="text-align: center;" id="loader_placeholder"></div>
        <input style="width:150px;" type="button" onclick="return cancelSaves();" class="ui-state-default ui-corner-all" value="Atcelt">
        <input style="width:150px;" class="ui-state-default ui-corner-all" type="submit" onClick="return saveAll(this, 'forums_tema')" value="Pievienot">
      </td>
    </tr>

  </table>

</form>