<?
if (empty($_GET['id'])) {
  die();
}

$reply_id = (int)$_GET['id'];

$reply_data = db_get_assoc(db_query("
  SELECT
    f.*,
    p.vards as pardeveja_vards
  FROM `".DB_PREF."forums` f
  LEFT JOIN `".DB_PREF."pardeveji` p ON (p.id = f.pardeveja_id)
  WHERE f.id = ".$reply_id."
  LIMIT 1
"));

if (empty($reply_data) || empty($reply_data['parent_id'])) {
  die();
}

$topic_id = $reply_data['parent_id'];

if ($reply_data['pardeveja_id'] != $_SESSION['user']['id']) {
  die();
}

require('edit_reply_process.php');

$topic_data = db_get_assoc(db_query("
  SELECT
    f.*,
    p.vards as pardeveja_vards
  FROM `".DB_PREF."forums` f
  LEFT JOIN `".DB_PREF."pardeveji` p ON (p.id = f.pardeveja_id)
  WHERE f.id = ".$topic_id."
  LIMIT 1
"));

$reply_data = htmlesc($reply_data);

$topic_data = htmlesc($topic_data);
?>
<h1 style="font-size:20px; margin-bottom: 0; text-align: left; margin-left: 5px;" >
  <? if (!empty($topic_data['sticky'])) { ?>
    <img src="css/star.png" alt="Svarīga tēma:" />
  <? } ?>
  <?= $topic_data['tema'] ?>
</h1>

<form action="" method="post" id="fullformplace" style="clear: both; margin: 20px 0;">

  <table width="700">

    <tr>
      <td>Atbilde:</td>
      <td><textarea rows="8" cols="40" name="saturs" id="saturs"><?= $reply_data['saturs'] ?></textarea></td>
    </tr>

    <tr>
      <td colspan="2" style="text-align: right;">
        <div style="text-align: center;" id="loader_placeholder"></div>
        <input style="width:150px;" type="button" onclick="document.location='?c=forums&a=skatit&id=<?= $topic_data['id'] ?>'" class="ui-state-default ui-corner-all" value="Atcelt">
        <input style="width:150px;" class="ui-state-default ui-corner-all" type="submit" onClick="return saveAll(this, 'forums_atbilde')" value="Saglabāt">
      </td>
    </tr>

  </table>

</form>