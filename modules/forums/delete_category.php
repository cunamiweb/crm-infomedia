<?php
if(!check_access('forums-labot-kategorijas')) {
  die();
}

$id = getGet('id');

if($id) {
  $category = ForumCategory::model()->findByPk($id);

  if($category) {

    $log_data = array();
    $log_data['foruma_kategorija']['old'] = $category->asArray();

    if($category->delete()) {

      $log_data['foruma_kategorija']['title'] = sprintf('Dzēsa foruma kategoriju %s', $category->nosaukums);
      log_add("laboja", $log_data);

      //  Reseto visiem šīs kategorijas topikiem kategoriju
      foreach($category->getTopics() as $topic) {
        $topic->category_id = 0;
        $topic->save();
      }

      header('Location: ?c=forums&a=kategorijas');
    }
  } else {
    die('Kategorija nav atrasta');
  }
}
?>