<?php
if(!check_access('forums-jauna-kategorija') && !check_access('forums-labot-kategorijas')) {
  die();
}

$id = getGet('id');

if($id && $a == 'labot_kategoriju') {
  $category = ForumCategory::model()->findByPk($id);
  log_add("atvera", "Atvēra foruma kategoriju " . $category->nosaukums);
} else {
  $category = new ForumCategory;
}

require("category_process.php");
?>

<button id="labotdzestbutton" onClick="return editoradd(this, <?php echo is_admin() ? 1 : 0 ?>, <?php echo is_limited() ? $_SESSION['user']['ierobezots'] : 0 ?>)">Labot</button>

<form action="?c=forums<?= !empty($id) ? '&a=labot_kategoriju&id=' . $id : '&a=jauna_kategorija' ;?>" id="fullformplace" method="post" enctype="multipart/form-data">
  <?php if($id) { ?>
    <input type="hidden" name="id" value="<?php echo $id;?>" />
  <?php } ?>
  <table width="750" class="data_form layout1">
    <tr>
      <td class="label" width="170">Nosaukums:</td>
      <td>
        <input type="text" name="ForumCategory[nosaukums]" id="forumcategory_id" value="<?php echo $category->nosaukums;?>" />
      </td>
    </tr>
  </table>
  <div class="hidethistoo">
    <input type="submit" onClick="return saveAll(this, 'forum_category')" class="submit show_in_act_panel" value="Saglabāt">
    <input type="button" onclick="return cancelSaves();" class="show_in_act_panel" value="Atcelt">
    <?php if($id) { ?>
      <input type="button" onclick="return deleteCategory();" class="show_in_act_panel align_left" value="Dzēst">
    <? } ?>
  </div>
</form>

<script type="text/javascript">

  editordisable();

  function deleteCategory()
  {
    var conf = confirm('Dzēst šo kategoriju?');
    if(conf) {
      window.location = '?c=forums&a=dzest_kategoriju&id=<?php echo $id;?>'
    }

    return false;
  }

  <?php if(empty($id)) { ?>
    editoradd($('#labotdzestbutton'), 1);
  <?php } ?>
</script>
