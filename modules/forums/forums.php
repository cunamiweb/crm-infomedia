<?php
if(!check_access('forums')) {
  die('Jums nav pieejas tiesību forumam.');
}

$a = empty($_GET['a']) ? "" : $_GET['a'];

switch($a){

  case "skatit": $include = "view.php";  break;
  case "jauna_kategorija": case "labot_kategoriju": $include = "new_category.php";  break;
  case "dzest_kategoriju": $include = "delete_category.php";  break;
  case "labot_temu": $include = "edit_topic.php";  break;
  case "labot_atbildi": $include = "edit_reply.php";  break;

  default: $include = "list.php";

}

include($include);

?>