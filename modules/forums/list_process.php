<?php
if (!empty($_POST) && check_access('forums-jauna-tema')) {

  $errors = validate_forums_tema_form($_POST);

  if (empty($errors)) {

    $_POST = trim_array($_POST);


    $sql = "
      INSERT INTO `".DB_PREF."forums`
      SET
        parent_id = NULL,
        tema = '".esc($_POST['tema'])."',
        saturs = '".esc($_POST['saturs'])."',
        pardeveja_id = ".(int)$_SESSION['user']['id'].",
        last_update = NOW(),
        category_id = ".esc($_POST['category_id'])."
    ";

    db_query($sql);

    $new_id = db_last_id();

    header('Location: ?c=forums&a=skatit&id=' . $new_id);
    die();

  }

}
?>