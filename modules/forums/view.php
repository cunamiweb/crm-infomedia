<?
if (empty($_GET['id'])) {
  die();
}

$topic_id = (int)$_GET['id'];

$sql = "
  INSERT IGNORE INTO `".DB_PREF."forums_atverts` (
    temas_id, pardeveja_id
  ) VALUES (
    ".$topic_id.",
    ".(int)$_SESSION['user']['id']."
  )
";

db_query($sql);

$sql = "
  INSERT IGNORE INTO `".DB_PREF."forums_atverts` (
    temas_id, pardeveja_id
  )
  SELECT
    id,
    '".(int)$_SESSION['user']['id']."'
  FROM `".DB_PREF."forums`
  WHERE parent_id = ".$topic_id."
";

db_query($sql);

require('view_process.php');

$main_data = db_get_assoc(db_query("
  SELECT
    f.*,
    p.vards as pardeveja_vards
  FROM `".DB_PREF."forums` f
  LEFT JOIN `".DB_PREF."pardeveji` p ON (p.id = f.pardeveja_id)
  WHERE f.id = ".$topic_id."
  LIMIT 1
"));

if (empty($main_data) || !empty($main_data['parent_id'])) {
  die();
}

log_add("atvera", "Atvēra forumu tēmu " . $main_data['tema']);

$query = db_query("
  SELECT
    f.*,
    p.vards as pardeveja_vards
  FROM `".DB_PREF."forums` f
  LEFT JOIN `".DB_PREF."pardeveji` p ON (p.id = f.pardeveja_id)
  WHERE f.parent_id = ".$topic_id."
  ORDER BY f.created ASC
");

if($main_data['category_id']) {
  $category = ForumCategory::model()->findByPk($main_data['category_id']);
}

?>

<iframe src="" style="display:none;" id="downloadframe" name="downloadframe"></iframe>

<button style="float: right; margin-top: 5px;" onclick="$('#fullformplace').show(); window.location.hash='#fullformplace';" class="ui-state-default ui-corner-all">Pievienot atbildi</button>
<button style="float: right; margin-top: 5px; margin-right: 10px;" onclick="document.location='?c=forums'" class="ui-state-default ui-corner-all">Uz tēmu sarakstu</button>

<? if (check_access('forums-sticky-jebkuru') || $main_data['pardeveja_id'] == $_SESSION['user']['id']) { ?>
  <button style="float: right; margin-top: 5px; margin-right: 10px;" onclick="document.location='?c=forums&a=skatit&id=<?= $topic_id ?>&toggle_sticky=1'" class="ui-state-default ui-corner-all"><?= empty($main_data['sticky']) ? 'Atzīmēt kā svarīgs' : 'Noņemt atzīmi svarīgs' ?> <img width="11" height="11" src="css/star.png" alt="" /></button>
<? } ?>

<h1 style="font-size:20px; margin-bottom: 0; text-align: left; margin-left: 5px;" >
  <a style="font-size:20px; margin-bottom: 0; text-align: left; margin-left: 5px;" href="?c=forums">Forums</a> >

  <?php if($main_data['category_id']) { ?>
    <a style="font-size:20px; margin-bottom: 0; text-align: left; margin-left: 5px;" href="?c=forums&category_id=<?php echo $category->id;?>"><?php echo $category->nosaukums;?></a> >
  <?php } ?>

  <? if (!empty($main_data['sticky'])) { ?>
    <img src="css/star.png" alt="Svarīga tēma:" />
  <? } ?>
  <?= $main_data['tema'] ?>
</h1>

<table width="100%" cellpadding="3" cellspacing="0" id="forums_skatit" class="data">

  <thead>

    <tr class="header">
      <th width="50" class="c">Autors</th>
      <th>Teksts</th>
      <th width="130">Pievienots</th>
      <th width="50"></th>
    </tr>

  </thead>

  <tbody>

    <tr class="start">
      <td style="white-space: nowrap;" class="c"><strong><?= $main_data['pardeveja_vards'] ?></strong></td>
      <td><?= nl2br($main_data['saturs']) ?></td>
      <td><?= date('d.m.Y H:i', strtotime($main_data['created'])) ?></td>
      <td style="text-align: right;">
        <? if (check_access('forums-labot-visas-temas') || $main_data['pardeveja_id'] == $_SESSION['user']['id']) { ?>
          <a href="?c=forums&a=labot_temu&id=<?= $main_data['id'] ?>" title="Labot tēmu"><img src="css/edit.png" alt="Labot tēmu" /></a>
        <? } ?>
        <? if (check_access('forums-labot-visas-temas') || $main_data['pardeveja_id'] == $_SESSION['user']['id']) { ?>
          <a href="?deleteForumTopic=<?= $main_data['id'] ?>" onclick="return confirm('Vai tiešām dzēst tēmu ar visām tās atbildēm?');" title="Dzēst tēmu"><img src="css/del.png" alt="Dzēst" /></a>
        <? } ?>
      </td>
    </tr>

  </tbody>

  <tbody class="main">

    <? while($row = db_get_assoc($query)) { ?>

      <tr id="reply-<?= $row['id'] ?>">
        <td style="white-space: nowrap;" class="c"><strong><?= $row['pardeveja_vards'] ?></strong></td>
        <td><?= nl2br($row['saturs']) ?></td>
        <td><?= date('d.m.Y H:i', strtotime($row['created'])) ?></td>
        <td style="text-align: right;">

          <? if ($row['pardeveja_id'] == $_SESSION['user']['id']) { ?>
            <a href="?c=forums&a=labot_atbildi&id=<?= $row['id'] ?>" title="Labot atbildi"><img src="css/edit.png" alt="Labot atbildi" /></a>
          <? } ?>

          <? if (check_access('forums-dzest-visas-atbildes') || $row['pardeveja_id'] == $_SESSION['user']['id']) { ?>
            <a href="?deleteForumPost=<?= $row['id'] ?>" onclick="return confirm('Vai tiešām dzēst atbildi?');" title="Dzēst atbildi"><img src="css/del.png" alt="Dzēst" /></a>
          <? } ?>

        </td>
      </tr>

    <? } ?>

  </tbody>

</table>

<script>
stripeTable($('#forums_skatit'));
</script>

<button style="float: right;" onclick="$('#fullformplace').show(); window.location.hash='#fullformplace';" class="ui-state-default ui-corner-all">Pievienot atbildi</button>

<form action="?c=forums&a=skatit&id=<?= $topic_id ?>" method="post" id="fullformplace" style="display: none; clear: both;">

  <table width="700">

    <tr>
      <td>Atbilde:</td>
      <td><textarea rows="8" cols="40" name="saturs" id="saturs"></textarea></td>
    </tr>

    <tr>
      <td colspan="2" style="text-align: right;">
        <div style="text-align: center;" id="loader_placeholder"></div>
        <input type="hidden" name="add_reply" value="1" />
        <input style="width:150px;" type="button" onclick="return cancelSaves();" class="ui-state-default ui-corner-all" value="Atcelt">
        <input style="width:150px;" class="ui-state-default ui-corner-all" type="submit" onClick="return saveAll(this, 'forums_atbilde')" value="Pievienot">
      </td>
    </tr>

  </table>

</form>

<form action="?c=forums&a=skatit&id=<?= $topic_id ?>" method="post" style="clear: both;" enctype="multipart/form-data">

  <input type="hidden" name="add_file" value="1" />

  <? show_files_block('forums_tema', $topic_id, true, (check_access('forums-labot-visas-temas') || $main_data['pardeveja_id'] == $_SESSION['user']['id']) ? false : true) ?>

</form>