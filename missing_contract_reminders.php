<?php
/**
 * Skripts, kas noteiktos intervālos sūta atgādinājumus klientiem, par nesaņemtu
 * līguma oriģinālu.
 * Nav saņemts oriģināls, ja klientam pie saņemšanas forma ir norādīts 'e-pasts'
 * vai 'fakss'.
 * Sūta tikai aktīvajiem līgumiem.
 * Sūta mēnesi pēc līguma saņemšanas datuma un pēc tam ik pēc divām nedēļām.
 * Pēc nosūtīšanas to piefiksē datubāzē un ieliek komentāru pie līguma.
 *
 * @since 2012.04.16.
 * @author Dāvis Frīdenvalds davis@datateks.lv
*/

ini_set('max_execution_time', 3600);
include('inc/init.php');

// Līgumi, kam nav oriģināls un saņemšanas datums vismaz mēnesis
$month_ago = mktime(23,59,59,date('m') - 1, date('d'), date('Y'));
$result = db_query('
  SELECT g.id, g.ligumanr, g.ligumadat, k.epasts, MAX(a.izsutits) as pedejais
  FROM '.DB_PREF.'ligumi g
  LEFT JOIN '.DB_PREF.'kontakti k ON g.id = k.liguma_id
  LEFT JOIN '.DB_PREF.'ligumi_atgadinajumi_log as a ON a.ligums_id = g.id AND a.statuss = 1
  WHERE
    DATE(g.sanemsanas_datums) <= "' . date('Y-m-d', $month_ago) . '" AND
    g.sanemsanas_forma != 1
  GROUP BY g.id
  ORDER BY g.sanemsanas_datums DESC
');

$limit = TEST_EMAILS ? 1 : 999999;
$i = 0;
// Skatamies katru līgumu
while($i < $limit && $ligums = db_get_assoc($result)) {
  //  Ja ir aktīvs
  if(get_liguma_status_by_id($ligums['id']) == 3) {

    $must_be_sent = false;

    //  Skatās pēdējo reizi kad sūtīts atgādinājums

    //var_dump($ligums);

    if(!$ligums['pedejais']) {
      //  Nav sūtīts, tas nozīmē, ka pirmā reize - sūtam
      $must_be_sent = true;
    } else {
      //  Ir sūtīts - skatāmies vai ir pagājušas 14 dienas
      $two_weeks_ago = mktime(23,59,59,date('m'),date('d') - 14, date('Y'));
      if(strtotime($ligums['pedejais']) <= $two_weeks_ago) {
        //  Ir pagājušas - sūtam
        $must_be_sent = true;
      }
    }

    if($must_be_sent) {

      //  Sūtam
      if(TEST_EMAILS) {
        //  Testa režīms
        $recipient = 'davis@datateks.lv';

        $transport = Swift_SmtpTransport::newInstance('smtp.datateks.lv', 25)
          ->setUsername('davis@datateks.lv')
          //->setPassword('your password')
        ;
        //$transport = Swift_MailTransport::newInstance();

      } else {
        //  Reālā sūtīšana
        $recipient = $ligums['epasts'];

        $transport = Swift_MailTransport::newInstance();
      }

      try {
        $mailer = Swift_Mailer::newInstance($transport);

        $subject = ORIG_ATG_SUBJ;

        $body = sprintf(ORIG_ATG_BODY, $ligums['ligumanr'], $ligums['ligumadat']);
        //var_dump($body);
        $body_html = nl2br($body);
        $body_plain = strip_tags($body);

        $mail = Swift_Message::newInstance();

        $mail->setFrom(array(ORIG_ATG_FROM_EMAIL => ORIG_ATG_FROM_NAME));

        $mail->setTo($recipient);

        $logo_cid = $mail->embed(Swift_Image::fromPath('css/logo.gif', 'logo.gif', 'image/gif'));

        $sending_status = 0;

        $body_html = str_replace('{logo}', $logo_cid, $body_html);

        $mail->setSubject($subject);
        $mail->setBody($body_html, 'text/html');
        $mail->addPart($body_plain, 'text/plain');


        if ($mailer->send($mail)) {
          $status = 1;
          //echo 'Sent.';
        } else {
          $status = 0;
          //echo 'Could not send.';
        }
      } catch(Exception $e) {
        $status = 0;
        //echo 'Could not send (Error).';
      }

      //  Saglabājam datubāzē
      db_query('
        INSERT INTO '.DB_PREF.'ligumi_atgadinajumi_log
          (ligums_id, izsutits, sanemejs, statuss)
        VALUES
          (' . $ligums['id'] . ', "' . date('Y-m-d H:i:s') . '", "' . $recipient . '", ' . $status . ')
      ');

      if($status) {
        //  Pievienojam komentāru ja veiksmīgi nosūtījās
        db_query("
          INSERT INTO `".DB_PREF."komentari` SET
            `objekta_id` = " . $ligums['id'] . ",
            `objekta_tips` = 'lig_klients',
            `pardeveja_id` = 0,
            `komentars` = 'Nosūtīts atgādinājums par nesaņemto līguma oriģinālu.'
        ");
      }
    } else {
      //echo 'No need to send.';
    }

    $i++;
  }
}
?>
