<?php
define("DB_PREF", "new_");
define("DB_HOST", "127.0.0.1");
define("DB_NAME", "infomedia_system");
define("DB_USER", "infomedia");
define("DB_PASS", "d@tenb@s3n");

// only used when STOP_ON_ERRORS = false
define("ERROR_MAIL", 'ap-junk@datateks.lv');

// Adwords
define('ADWORDS_VERSION', 'v201302');
define('ADWORDS_CURRENCY', 'EUR'); // Currency the adwords account operates with
// We now define auth params at ...../AdWords/auth.ini
//define('ADWORDS_API_DEVELOPER_TOKEN', '4dDwTjflo3JH3paB0GwvOg');
/*define('ADWORDS_API_EMAIL', 'davis@datateks.lv');
define('ADWORDS_API_PASSWORD', 'dDat@123'); */
//define('ADWORDS_API_EMAIL', 'info@infomedia.lv');
//define('ADWORDS_API_PASSWORD', 'imedia01');
//define('ADWORDS_API_CLIENT_ID', '');   */
define('ADOWRDS_API_USER_AGENT', 'SIA Datateks');
//define('ADWORDS_API_DEFAULT_SERVER', 'https://adwords-sandbox.google.com');
define('ADWORDS_API_DEFAULT_SERVER', 'https://adwords.google.com');
define('ADWORDS_REFRESH_RATE', 1); // Days

// this flag should be true ONLY on development site
// when error happens, detailed info is shown
// will stop execution even on notices
// when true, error is not sent to ERROR_MAIL
define("STOP_ON_ERRORS", true);

define('VAT', 21);

define('REK_FROM_NAME', 'Ieva Jurgeviča');
define('REK_FROM_EMAIL', 'ieva.jurgevica@infomedia.lv');

//  Do not send emails on certain scripts (for example, reminders about unreceived contract original)
// Do not depend on this setting as not all emailing scripts take this in account
define("TEST_EMAILS", true);

define("DEBUG_QUERIES", false);
define("ENABLE_RAW_LOG", false);

define("SUPERUSER_FULL_ACCESS", true);

define("KORP_VADITAJA_ID", 55);

define("PARD_VADITAJA_ID", 36);
define("PARD_VADITAJA_BONUSI_START_YEAR", 2013);

define("REKV_NOSAUK", "Infomedia, SIA");
define("REKV_JUR_ADR", "Rītupes iela 7/9-38, Rīga, LV-1019");
define("REKV_FIZ_ADR", "Kr. Barona iela 108-7, Rīga, LV-1001");
define("REKV_BANKA", "Swedbank, AS");
define("REKV_SWIFT", "HABALV22");
define("REKV_KONTS", "LV38HABA0551020711161");
define("REKV_REG_NR", "40103166641");
define("REKV_PVN_NR", "LV40103166641");

define("BONUSS_START_YEAR", 2011); // Produkcijā: 2011

define('PREMIJAS_NEW_START_YEAR', 2012); // Sāk darboties otrā prēmiju sistēma. Produkcijā: 2012
define('PREMIJAS_NEW_START_QUEARTER', 2); // Produkcijā: 2

define('PREMIJAS_NEW2_START_YEAR', 2013); // Sāk darboties trešā prēmiju sistēma. Produkcijā: 2013
define('PREMIJAS_NEW2_START_QUEARTER', 1); // Produkcijā: 1

define("DARB_REK_PAK_NOSAUK", "Atlīdzība par pakalpojumiem");
define("DARB_REK_PREM_NOSAUK", "Papildus atlīdzība par apgrozījuma apjomu");
define("DARB_REK_KOREKC_NOSAUK", "Tekošā korekcija par iepriekšējiem periodiem");

define("LIG_REK_PAK_NOSAUK", "Reklāmas pakalpojumi");
define("LIG_REK_ATKAPS_PAMATOJUMS", "Līgumsods par līgumu Nr. %s (līguma punkts 11.5.)");  

define("LIG_REK_INFO1", "Būsim pateicīgi par savlaicīgu rēķina apmaksu.");
define("LIG_REK_INFO2", "Ja rēķins netiks apmaksāts līdz norādītajam datumam, reklāmas darbība tiks apturēta saskaņā ar līguma punktu Nr. 8.7.");
define("LIG_REK_INFO3", "Informācijai par rēķiniem un līgumiem lūdzam zvanīt 29970762.");
define("LIG_REK_INFO3_MPK", "Informācijai par rēķiniem un līgumiem lūdzam zvanīt 20370996.");  

define("LIG_REK_VALDES_LOCEKLIS", "V. Juzāns");
define("LIG_REK_TALR", "67290200");
define("LIG_REK_FAX", "67291033");
define("LIG_REK_EPASTS", "info@infomedia.lv");
define("LIG_REK_WWW", "www.infomedia.lv");

define("AVANSA_REK_SUBJ", "Avansa rēķins par reklāmas pakalpojumiem");
define("AVANSA_REK_BODY", "Labdien!

Nosūtu Jums avansa rēķinu par mūsu pakalpojumiem.

Lūdzu, apstipriniet avansa rēķina saņemšanu. Paldies!

--
Jauku dienu vēlot,

<img src=\"{logo}\" alt=\"\" />
<div style=\"color: #0070C0; padding-left: 40px; font-size: 14px;\">
<strong>Ieva Jurgeviča</strong>
grāmatvede
SIA Infomedia
Cēsu iela 31, k-1, Rīga, LV-1012
Tālr.: +371 67290200
Fakss: +371 67290201
Mob.t.: +371 29970762
ieva@infomedia.lv
www.infomedia.lv</div>");

define("GALA_REK_SUBJ", "Gala rēķins par reklāmas pakalpojumiem");
define("GALA_REK_BODY", "Labdien!

Nosūtu Jums gala rēķinu par mūsu pakalpojumiem.

<strong><u>Samaksa jau ir veikta. Iepriekšējais bija avansa rēķins, kurš netiek iekļauts grāmatvedībā.
Šis ir jāiegrāmato.</u></strong>

Lūdzu, apstipriniet rēķina saņemšanu. Paldies!

--
Jauku dienu vēlot,

<img src=\"{logo}\" alt=\"\" />
<div style=\"color: #0070C0; padding-left: 40px; font-size: 14px;\">
<strong>Ieva Jurgeviča</strong>
grāmatvede
SIA Infomedia
Cēsu iela 31, k-1, Rīga, LV-1012
Tālr.: +371 67290200
Fakss: +371 67290201
Mob.t.: +371 29970762
ieva@infomedia.lv
www.infomedia.lv</div>");

define('ORIG_ATG_FROM_NAME', 'Test Testing');
define('ORIG_ATG_FROM_EMAIL', 'davis@datateks.lv');
define("ORIG_ATG_SUBJ", "Atgādinājums par nesaņemtu līguma oriģinālu");
define("ORIG_ATG_BODY", "Labdien!

Atgādinām, ka neesam saņēmuši no Jums mūsu savstarpējā līguma Nr. %s no %s oriģinālu!

Lūdzu, pēc iespējas ātrāk nosūtīt to pa pastu uz adresi Cēsu 31, k-1, Rīga, LV-1012!

--
<img src=\"{logo}\" alt=\"\" />
<div style=\"color: #0070C0; padding-left: 40px; font-size: 14px;\">
<strong>Juris Roķis</strong>
klientu administrators
SIA Infomedia
Cēsu iela 31, k-1, Rīga, LV-1012
Tālr.: +371 67796615
Fakss: +371 67290201
Mob.t.: +371 29970762
juris@infomedia.lv
www.infomedia.lv</div>");

// Piedavajuma pdf
define("PIED_PDF_COMPANY", "Infomedia SIA");
define("PIED_PDF_CONTACT", "Vārds Uzvārds");
define("PIED_PDF_PHONE", "20000000");
define("PIED_PDF_EMAIL", "vards@infomedia.lv");
define("PIED_PDF_ADDRESS", "Cēsu iela 31, k-1, Rīga, LV-1012");

//  Caching
define('CACHING_ENABLED', true);
define('MEMCACHE_HOST', '127.0.0.1');
define('MEMCACHE_PORT', '11211');
define('MEMCACHE_DEFAULT_EXP', 60*60*24); //24h
define('MEMCACHE_KEY_PREFIX', 'infomedia_');
?>