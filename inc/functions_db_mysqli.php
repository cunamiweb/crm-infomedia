<?php
function db_connect($host, $user, $pass) {

  return mysqli_connect($host, $user, $pass);

}

function db_select($name) {

  global $db;

  return mysqli_select_db($db,$name);

}

function esc($obj) {

  global $db;

  if (is_array($obj)) {

    foreach($obj as $key => $value) {
      $obj[$key] = esc($value);
    }

  } elseif (is_string($obj)) {

    if (get_magic_quotes_gpc()) {
      $obj = stripslashes($obj);
    }

    $obj = mysqli_real_escape_string($db,$obj);

  }

  return $obj;

}


function db_query($sql) {

  global $db;

  $time_start = microtime(true);
  $result = mysqli_query($db,$sql);
  $time_end = microtime(true);

  $time = microtime_diff($time_start, $time_end);

  if (DEBUG_QUERIES || ENABLE_RAW_LOG) {

    $GLOBALS['executed_queries'][] = array(
      'sql' => $sql,
      'time' => $time
    );

    $GLOBALS['query_total_time'] += $time;

  }

  if (!$result) {
    trigger_error(mysqli_error($db), E_USER_WARNING);
  }

  return $result;

}

function db_get_assoc($result) {

  return mysqli_fetch_assoc($result);

}

function db_get_assoc_all($result) {

  $rows = array();

  while($row = db_get_assoc($result)) {
    $rows[] = $row;
  }

  return $rows;

}

function db_get_val($result) {

  $data = mysqli_fetch_row($result);

  if (isset($data[0])) {
    return $data[0];
  }

}

function db_last_id() {

  global $db;

  return mysqli_insert_id($db);

}

function db_num_rows($result) {

  return mysqli_num_rows($result);

}

function db_found_rows() {

  return db_get_val(db_query("SELECT FOUND_ROWS()"));

}
function db_close(){
	global  $db;
	mysqli_close($db);
}
?>