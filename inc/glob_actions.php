<?php
if ($_SESSION['login']) {

  if(!empty($_GET['logout'])){

    log_add("atvera", "Beidza darbu");

    $_SESSION = array();

    if (!empty($_GET['auto_logout'])) {
      $_SESSION['auto_logout'] = true;
    }

    header("location: ./");
    die();

  }

  if(!empty($_GET['getfile'])){

    $sql = "
      SELECT f.*
      FROM `".DB_PREF."faili` f
      WHERE f.`id` = ".(int)$_GET['getfile']."
    ";

    if (!is_admin()) {

      $sql .= "
        AND
          (
            (
              objekta_tips IN ('darb_norekini', 'darb_rekviziti', 'darb_atgadinajumi', 'darb_ligumi', 'piedavajums', 'adwords_atskaite') AND
              objekta_id = ".$_SESSION['user']['id']."
            ) OR (
              objekta_tips IN ('lig_rekini', 'lig_klients', 'lig_pakalpojums', 'lig_atgadinajumi', 'piedavajums', 'adwords_atskaite') AND
              (
                objekta_id IN (
                  SELECT id
                  FROM `".DB_PREF."ligumi`
                  WHERE pardeveja_id = ".$_SESSION['user']['id']."
                ) OR
                objekta_id IN (
                  SELECT liguma_id
                  FROM `".DB_PREF."ligumi_saistitie_pardeveji`
                  WHERE pardeveja_id = ".$_SESSION['user']['id']."
                )  OR
                objekta_id IN (
                  SELECT id
                  FROM `".DB_PREF."piedavajumi`
                  WHERE pardevejs_id = ".$_SESSION['user']['id']."
                )
              )
            ) OR (
              objekta_tips = 'forums_tema'
            )
          )
      ";

    }

    $r = db_query($sql);
    $a = db_get_assoc($r);

    if (empty($a)) {
      die();
    }

    $name = 'faili/'.$a['id'];

    if(file_exists($name)){

      $outname = $a['fails'];

      $fp = fopen($name, 'rb');

      header("Content-type: application/octet-stream");
      header("Content-Disposition:attachment; filename=\"".trim(htmlentities($outname))."\"");
      header("Content-Length: ".filesize($name));

      fpassthru($fp);
      die();

    }

    die();

  }

  if(!empty($_GET['deletefile'])){

    if (!is_admin()) {

      $pardeveja_id = db_get_val(db_query("
        SELECT pardeveja_id
        FROM `".DB_PREF."faili`
        WHERE `id` = ".(int)$_GET['deletefile']."
      "));

      if ($pardeveja_id != $_SESSION['user']['id']) {
        die();
      }

    }

    $old_data = db_get_assoc(db_query("SELECT * FROM `".DB_PREF."faili` WHERE id = " . (int)$_GET['deletefile']));

    db_query("DELETE FROM `".DB_PREF."faili` WHERE `id` = ".(int)$_GET['deletefile']);
    unlink("faili/".(int)$_GET['deletefile']);

    $log_data = array();

    $log_data['komentars']['old'] = $old_data;
    $log_data['komentars']['title'] = sprintf('Dzēsa failu ar ID %s', (int)$_GET['deletefile']);

    log_add("laboja", $log_data);

    die();

  }

  if(!empty($_GET['deleteClient'])){

    if (!check_access('ligumi-dzest')) {
      die();
    }

    $id = (int)$_GET['deleteClient'];

    $old_data = db_get_assoc(db_query("SELECT * FROM `".DB_PREF."ligumi` WHERE id = " . $id));

    db_query("DELETE FROM `".DB_PREF."ligumi` WHERE `id` = ".(int)$id);

    db_query("DELETE FROM `".DB_PREF."atgadinajumi` WHERE `liguma_id` = ".(int)$id);

    db_query("DELETE FROM `".DB_PREF."kontakti` WHERE `liguma_id` = ".(int)$id);

    db_query("DELETE FROM `".DB_PREF."ligumi_rekinu_piegades` WHERE `liguma_id` = ".(int)$id);

    db_query("DELETE FROM `".DB_PREF."pakalpojumi` WHERE `liguma_id` = ".(int)$id);

    db_query("DELETE FROM `".DB_PREF."pielikumi_infomedia` WHERE `liguma_id` = ".(int)$id);

    db_query("DELETE FROM `".DB_PREF."pielikumi_tiessaite` WHERE `liguma_id` = ".(int)$id);

    db_query("DELETE FROM `".DB_PREF."pielikumi_citi` WHERE `liguma_id` = ".(int)$id);

    db_query("DELETE FROM `".DB_PREF."rekini` WHERE `liguma_id` = ".(int)$id);

    // Izdzešam visus klienta failus
    $query = db_query("SELECT * FROM `".DB_PREF."faili` WHERE `objekta_id` = ".(int)$id." AND objekta_tips IN ('lig_klients', 'lig_pakalpojums', 'lig_rekini', 'lig_atgadinajumi')");

    while($row = db_get_assoc($query)) unlink("faili/".$row['id']);

    db_query("DELETE FROM `".DB_PREF."faili` WHERE `objekta_id` = ".(int)$id." AND objekta_tips IN ('lig_klients', 'lig_pakalpojums', 'lig_rekini', 'lig_atgadinajumi')");

    $log_data = array();

    $log_data['ligums']['old'] = $old_data;
    $log_data['ligums']['title'] = sprintf('Dzēsa līgumu %s klientam %s', $old_data['ligumanr'], $old_data['nosaukums']);

    log_add("laboja", $log_data);

    header('Location: ?c=ligumi');
    die();

  }

  if(!empty($_GET['updateAtgStatus']) && !empty($_GET['status'])){

    // called from ajax

    $old_data = db_get_assoc(db_query("SELECT * FROM `".DB_PREF."atgadinajumi` WHERE id = " . (int)$_GET['updateAtgStatus']));

    if (!is_admin() && empty($old_data['pardevejs_var_labot'])) {
      die();
    }

    $apstr_laiks = 'apstr_laiks';
    $apstr_pardeveja_id = 'apstr_pardeveja_id';

    if ($old_data['statuss'] == 1 && $_GET['status'] != 1) {
      $apstr_laiks = "NOW()";
      $apstr_pardeveja_id = (int)$_SESSION['user']['id'];
    } elseif ($old_data['statuss'] != 1 && $_GET['status'] == 1) {
      $apstr_laiks = 'NULL';
      $apstr_pardeveja_id = 'NULL';
    }

    $result = db_query("
      UPDATE `".DB_PREF."atgadinajumi`
      SET
        statuss = ".(int)$_GET['status'].",
        apstr_laiks = ".$apstr_laiks.",
        apstr_pardeveja_id = ".$apstr_pardeveja_id."
      WHERE `id` = ".(int)$_GET['updateAtgStatus']
    );

    echo $result ? 1 : 0;

    $log_data = array();

    $log_data['atgadinajums']['old'] = $old_data;
    $log_data['atgadinajums']['new'] = db_get_assoc(db_query("SELECT * FROM `".DB_PREF."atgadinajumi` WHERE id = " . (int)$_GET['updateAtgStatus']));
    $log_data['atgadinajums']['title'] = sprintf('Laboja atgādinājuma ar ID %s statusu', (int)$_GET['updateAtgStatus']);

    log_add("laboja", $log_data);

    die();

  }

  if(!empty($_GET['deletecomment'])){

    if (!is_admin() || is_limited()) {

      $pardeveja_id = db_get_val(db_query("
        SELECT pardeveja_id
        FROM `".DB_PREF."komentari`
        WHERE `id` = ".(int)$_GET['deletecomment']."
      "));

      if ($pardeveja_id != $_SESSION['user']['id']) {
        die();
      }

    }

    $old_data = db_get_assoc(db_query("SELECT * FROM `".DB_PREF."komentari` WHERE id = " . (int)$_GET['deletecomment']));

    db_query("
      DELETE FROM `".DB_PREF."komentari`
      WHERE `id` = ".(int)$_GET['deletecomment']."
    ");

    $log_data = array();

    $log_data['komentars']['old'] = $old_data;
    $log_data['komentars']['title'] = sprintf('Dzēsa komentāru ar ID %s', (int)$_GET['deletecomment']);

    log_add("laboja", $log_data);

    die();

  }

  if(!empty($_GET['setTableColVisible']) && !empty($_GET['tableName'])){

    // called from ajax

    db_query("
      INSERT INTO `".DB_PREF."tabulu_kolonnas`
      SET
        `pardeveja_id` = ".$_SESSION['user']['id'].",
        `tabula` = '".esc($_GET['tableName'])."',
        `lauks` = '".esc($_GET['setTableColVisible'])."',
        `redzams` = 1
      ON DUPLICATE KEY UPDATE
        `redzams` = 1
    ");

    die();

  }

  if(!empty($_GET['setTableColHidden']) && !empty($_GET['tableName'])){

    // called from ajax

    db_query("
      INSERT INTO `".DB_PREF."tabulu_kolonnas`
      SET
        `pardeveja_id` = ".$_SESSION['user']['id'].",
        `tabula` = '".esc($_GET['tableName'])."',
        `lauks` = '".esc($_GET['setTableColHidden'])."',
        `redzams` = 0
      ON DUPLICATE KEY UPDATE
        `redzams` = 0
    ");

    die();

  }

  if(!empty($_GET['deleteForumTopic']) && is_numeric($_GET['deleteForumTopic'])) {

    if (!is_admin() || is_limited()) {

      $post_data = db_get_assoc(db_query("
        SELECT *
        FROM `".DB_PREF."forums`
        WHERE `id` = ".(int)$_GET['deleteForumTopic']."
      "));

      if ($post_data['pardeveja_id'] != $_SESSION['user']['id']) {
        die();
      }

    }

    db_query("DELETE FROM `".DB_PREF."forums` WHERE `id` = ".(int)$_GET['deleteForumTopic']);

    db_query("DELETE FROM `".DB_PREF."forums` WHERE `parent_id` = ".(int)$_GET['deleteForumTopic']);

    header("location: ./?c=forums");
    die();

  }

  if(!empty($_GET['deleteForumPost']) && is_numeric($_GET['deleteForumPost'])) {

    $post_data = db_get_assoc(db_query("
      SELECT *
      FROM `".DB_PREF."forums`
      WHERE `id` = ".(int)$_GET['deleteForumPost']."
    "));

    if (!is_admin()) {

      if ($post_data['pardeveja_id'] != $_SESSION['user']['id']) {
        die();
      }

    }

    db_query("DELETE FROM `".DB_PREF."forums` WHERE `id` = ".(int)$_GET['deleteForumPost']);

    header("location: ./?c=forums&a=skatit&id=" . $post_data['parent_id']);
    die();

  }

  if (!empty($_GET['getLigumiParskatsXls'])) {

    if (!is_admin()) {
      die();
    }

    require_once ('Spreadsheet/Excel/Writer.php');

    $where = array();

    if (getGet('fdatums_no') !== '') $where[] = 'g.datums >= "'.to_db_date(getGet('fdatums_no')).'"';
    if (getGet('fdatums_lidz') !== '') $where[] = 'g.datums <= "'.to_db_date(getGet('fdatums_lidz')).'"';

    $sql = "
      SELECT
        g.*,
        g.piesl_tiessaites_av_skaits + g.piesl_infomedia_av_skaits + g.piesl_yandex_av_skaits as av_skaits_kopaa
      FROM `".DB_PREF."ligumi_parskats_log` g
    ";

    if (!empty($where)) {
      $sql .= " WHERE " . implode(' AND ', $where);
    }

    $sql .= "
      ORDER BY g.izveidots ASC
    ";

    $query = db_query($sql);

    $data = array();

    while($row = db_get_assoc($query)) {
      // ja ir vairāki ieraksti vienā dienā, ņemam pēdējo
      $data[$row['datums']] = $row;
    }

    krsort($data);


    $filename = 'Ligumu_parskats';
    $filename .= !empty($_GET['fdatums_no']) ? '_no_' . $_GET['fdatums_no'] : '';
    $filename .= !empty($_GET['fdatums_lidz']) ? '_lidz_' . $_GET['fdatums_lidz'] : '';

    $workbook = new Spreadsheet_Excel_Writer();
    $workbook->setVersion(8);

    $workbook->send($filename . '.xls');

    $worksheet = &$workbook->addWorksheet('Līgumi');

    $worksheet->setInputEncoding('UTF-8');

    $workbook->setCustomColor(12, 10, 200, 10);

    $format_title = &$workbook->addFormat(array(
      'Bold' => true
    ));

    $format_number = &$workbook->addFormat(array(
      'NumFormat' => '0'
    ));

    $format_decimal = &$workbook->addFormat(array(
      'NumFormat' => '0.00'
    ));

    $format_date = &$workbook->addFormat(array(
      'NumFormat' => 'DD.MM.YYYY'
    ));

    $cols = array(
      'Datums' => array('f' => 'datums', 'w' => 10, 'format' => 'date'),
      'Pieslēgto līgumu sk.' => array('f' => 'piesl_lig_skaits', 'w' => 20, 'format' => 'number'),
      'Pieslēgto Google AV sk.' => array('f' => 'piesl_tiessaites_av_skaits', 'w' => 27, 'format' => 'number'),
      "Pieslēgto Infomedia AV sk." => array('f' => 'piesl_infomedia_av_skaits', 'w' => 27, 'format' => 'number'),
      "Pieslēgto Yandex AV sk." => array('f' => 'piesl_yandex_av_skaits', 'w' => 27, 'format' => 'number'),
      'AV sk. kopā' => array('f' => 'av_skaits_kopaa', 'w' => 17, 'format' => 'number'),
      'Dienas budžets' => array('f' => 'dienas_budzets', 'w' => 17, 'format' => 'decimal')
    );

    $c = 0;

    foreach($cols as $title => $field) {
      $worksheet->setColumn($c, $c, $field['w']);
      $worksheet->writeString(0, $c, $title, $format_title);
      $c++;
    }

    $r = 1;

    foreach($data as $row) {

      $c = 0;

      foreach($cols as $title => $field) {

        if (!isset($field['format'])) $field['format'] = false;

        if ($field['format'] == 'number') {

          $worksheet->writeNumber(
            $r, $c++,
            (int)isset($row[$field['f']]) ? $row[$field['f']] : 0,
            $format_number
          );


        } elseif ($field['format'] == 'decimal') {

          $worksheet->writeNumber(
            $r, $c++,
            (float)isset($row[$field['f']]) ? $row[$field['f']] : '',
            $format_decimal
          );


        } elseif ($field['format'] == 'date') {

          if (!empty($row[$field['f']])) {

            $val = db_get_val(db_query("SELECT DATEDIFF('".$row[$field['f']]."', '1899-12-30')"));

            $worksheet->write(
              $r, $c++,
              $val,
              $format_date
            );

          } else {
            $worksheet->writeString($r, $c++, '');
          }

        } else {

          $worksheet->writeString(
            $r, $c++,
            isset($row[$field['f']]) ? $row[$field['f']] : ''
          );

        }

      }

      $r ++;

    }

    $workbook->close();
    die();

  }

  if(isset($_GET['maillisteAktiviNavSacies'])) {
    require('mailliste.php');
    die();
  }
}
?>