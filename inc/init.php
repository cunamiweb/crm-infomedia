<?php



include('config.php');
include('functions.php');
include('functions.getstatistika.php');
include('functions_db_mysqli.php');
include('functions_validate.php');
include('functions_ajax.php');
include('functions_caching.php');

require_once DIR_ROOT . '/vendor/autoload.php';

define('DOCUMENT_ROOT', $_SERVER['DOCUMENT_ROOT']);

set_include_path(get_include_path() . PATH_SEPARATOR . dirname($_SERVER['SCRIPT_FILENAME']) . '/libs/zend/library');

set_include_path(get_include_path() . PATH_SEPARATOR . dirname($_SERVER['SCRIPT_FILENAME']) . '/libs/PEAR');

require_once('Zend/Json.php');

require_once dirname($_SERVER['SCRIPT_FILENAME']) . '/libs/swiftmailer/swift_required.php';

$GLOBALS['executed_queries'] = array();
$GLOBALS['query_total_time'] = 0;
$GLOBALS['schemas'] = array();

mb_internal_encoding('UTF-8');

//set_error_handler('standard_error_handler', E_ALL ^ E_DEPRECATED);

// server should keep session data for AT LEAST 4 hours
ini_set('session.gc_maxlifetime', 3600 * 4);

// each client should remember their session id for EXACTLY 4 hours
session_set_cookie_params(3600 * 4);

session_start();

if (!isset($_SESSION['login'])) {
  $_SESSION['login'] = false;
}

if(!empty($_GET['do_auto_logout'])) { // called through ajax
  if ($_SESSION['login']) {
    if (!empty($_SESSION['last_use_time']) && !empty($_SESSION['user']['auto_logout_after']) && $_SESSION['last_use_time'] < time() - ($_SESSION['user']['auto_logout_after'] * 60)) {
      echo 1;
    }
  } else {
    echo 1;
  }
  echo 0;
  die();
}

if ($_SESSION['login'] && empty($_GET['logout'])) {

  if (!empty($_SESSION['last_use_time'])  && !empty($_SESSION['user']['auto_logout_after']) && $_SESSION['last_use_time'] < time() - ($_SESSION['user']['auto_logout_after'] * 60)) {

    header('Location: ?logout=true&auto_logout=1');
    die();

  }

  $_SESSION['last_use_time'] = time();

}

//  Caching
if(CACHING_ENABLED) {

  if(class_exists('Memcache')) {
    $memcache = new Memcache;
    $cacheAvailable = $memcache->connect(MEMCACHE_HOST, MEMCACHE_PORT);
  } else {
    $memcache = new Memcached;
    $cacheAvailable = $memcache->addServer(MEMCACHE_HOST, MEMCACHE_PORT);
  }


}

date_default_timezone_set('Europe/Riga');



// DB

//$db = db_connect(DB_HOST, DB_USER, DB_PASS) or die("Neizdevas pieslegties datubazei!");
$db = db_connect(DB_HOST.(defined('DB_PORT')?':'.DB_PORT:''), DB_USER, DB_PASS) or die("Neizdevas pieslegties datubazei!");

db_select(DB_NAME) or die("Neatradu datubaazi!");

db_query('SET NAMES utf8');

include('models.php');

//  User config
update_config();

//  Permissions
if(isset($_SESSION['user'])) {
    $GLOBALS['permissions'] = get_tiesibu_dati($_SESSION['user']['id'], $_SESSION['user']['tips']);
}

include('variables.php');
