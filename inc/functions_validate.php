<?php

function validate_ligums_form($data) {

  global $_vars;

  $data = trim_array($data);

  $errors = array();

  if (check_access('ligumi-labot-pilns')) {

    if (empty($data['ligumanr'])) {
      $errors['klients']['orgligumanr'] = '';
    } else {

      $c = db_get_val(db_query("
        SELECT COUNT(*)
        FROM `".DB_PREF."ligumi`
        WHERE
          ligumanr = '".esc($data['ligumanr'])."'
          ".(!empty($data['liguma_id']) ? " AND id != " . (int)$data['liguma_id'] : '')."
      "));

      if (!empty($c)) {
        $errors['klients']['orgligumanr'] = 'Šāds līguma numurs jau ir aizņemts!';
      }


    }

    if (!empty($data['reklama_no']) && !empty($data['reklama_lidz']) && to_db_date($data['reklama_no']) > to_db_date($data['reklama_lidz'])) {
      $errors['pakalpojums']['reklidz'] = 'Reklāmas beigu datums nevar būt jaunāks par sākuma datumu!';
    }

    if (!empty($data['rekini'])) {

      foreach($data['rekini'] as $rekina_id => $r) {

        if (!empty($r['anulets']) && empty($r['anul_datums'])) {
          $errors['rekini'][] = sprintf('Rēķinam %s nav norādīts anulēšanas datums!', $r['rek_nr']);
        }

        if (!empty($r['nodots_piedz']) && empty($r['nodots_piedz_datums'])) {
          $errors['rekini'][] = sprintf('Rēķinam %s nav norādīts nodots piedziņai datums!', $r['rek_nr']);
        }

        if (!empty($data['ligumadat']) && !empty($r['izr_datums']) && to_db_date($r['izr_datums']) < to_db_date($data['ligumadat'])) {
          $errors['rekini'][] = sprintf('Rēķinam %s izrakstīšanas datums ir norādīts vecāks par līguma datumu!', $r['rek_nr']);
        }

        if (!empty($r['izr_datums']) && !empty($r['apm_termins']) && to_db_date($r['apm_termins']) < to_db_date($r['izr_datums'])) {
          $errors['rekini'][] = sprintf('Rēķinam %s apmaksas termiņš ir norādīts vecāks par izrakstīšanas datumu!', $r['rek_nr']);
        }

      }

    }

    if (!empty($data['rekini_new']['izr_datums'])) {

      foreach($data['rekini_new']['izr_datums'] as $i => $izr_datums) {

        if (!empty($data['rekini_new']['anulets'][$i]) && empty($data['rekini_new']['anul_datums'][$i])) {
          $errors['rekini'][] = sprintf('Rēķinam %s nav norādīts anulēšanas datums!', $data['rekini_new']['rek_nr'][$i]);
        }

        if (!empty($data['rekini_new']['nodots_piedz'][$i]) && empty($data['rekini_new']['nodots_piedz_datums'][$i])) {
          $errors['rekini'][] = sprintf('Rēķinam %s nav norādīts nodots piedziņai datums!', $data['rekini_new']['rek_nr'][$i]);
        }

        if (!empty($data['ligumadat']) && !empty($data['rekini_new']['izr_datums'][$i]) && to_db_date($data['rekini_new']['izr_datums'][$i]) < to_db_date($data['ligumadat'])) {
          $errors['rekini'][] = sprintf('Rēķinam %s izrakstīšanas datums ir norādīts vecāks par līguma datumu!', $data['rekini_new']['rek_nr'][$i]);
        }

      }

    }

    if (empty($data['nosaukums'])) {
      $errors['klients']['nosaukumsplace'] = '';
    }

  }

  if (check_access('ligumi-labot-pilns')) {

    if (!empty($data['saistitie_pardeveji'])) {

      foreach($data['saistitie_pardeveji'] as $lig_saist_pard_id => $p) {

        if ($data['pardeveja_id'] == $p['pardeveja_id']) {
          $errors['klients'][] = 'Saistītais pārdevējs nevar būt arī galvenais pārdevējs!';
        }

      }

    }

    if (!empty($_POST['saistitie_pardeveji_new']['pardeveja_id'])) {

      foreach($_POST['saistitie_pardeveji_new']['pardeveja_id'] as $i => $pardeveja_id) {

        if ($data['pardeveja_id'] == $pardeveja_id) {
          $errors['klients'][] = 'Saistītais pārdevējs nevar būt arī galvenais pārdevējs!';
        }

      }

    }

    //  Ātrā saite var būt tikai unikāliem a/v
    //  esošie
    if(isset($data['piel_infomedia'])) {
      foreach($data['piel_infomedia'] as $av_data) {
        if($av_data['atslegvards'] && isset($av_data['atra_saite']) && $av_data['atra_saite']) {
          if(keyword_exists($av_data['atslegvards']) > 1) {
            $errors['pakalpojums']['piel_infomedia'][] = $av_data['atslegvards'] . ' nav unikāls, tāpēc tam nevar aktivizēt Profils!';
          }
        }
      }
    }

    //  jaunie
    if(isset($data['piel_infomedia_new'])){
      $count = count($data['piel_infomedia_new']['atslegvards']);
      for($i = 0; $i < $count; $i++) {
        $keyword = $data['piel_infomedia_new']['atslegvards'][$i];
        if($keyword) {
          if(isset($data['piel_infomedia_new']['atra_saite'][$i]) && $data['piel_infomedia_new']['atra_saite'][$i]) {
            if(keyword_exists($keyword) > 0) {
              $errors['pakalpojums']['piel_infomedia'][] = $keyword . ' nav unikāls, tāpēc tam nevar aktivizēt Profils!';
            }
          }
        }
      }
    }
  }

  return $errors;

}

function validate_darbinieks_ligumi_form($data) {

  $data = trim_array($data);

  $errors = array();

  return $errors;

}

function validate_darbinieks_saistitie_ligumi_form($data) {

  $data = trim_array($data);

  $errors = array();

  return $errors;

}


function validate_darbinieks_norekini_form($data) {

  $data = trim_array($data);

  $errors = array();

  /*
  if (!empty($data['rekini'])) {

    foreach($data['rekini'] as $rekina_id => $r) {

    }

  }
  */

  return $errors;

}

function validate_darbinieks_rekviziti_form($data) {

  $data = trim_array($data);

  $errors = array();

  if (empty($data['vards']))
    $errors['rekviziti']['vardsplace'] = 'Nav norādīts vārds, uzvārds!';

  if (empty($data['pardeveja_id'])) {

    if (empty($data['lietotajvards'])) {
      $errors['rekviziti']['lietotajvards'] = 'Nav norādīts lietotājvārds!';
    } else {

      $c = db_get_val(db_query("
        SELECT COUNT(*)
        FROM `".DB_PREF."pardeveji`
        WHERE
          lietotajvards = '".esc($data['lietotajvards'])."'
      "));

      if (!empty($c)) {
        $errors['rekviziti']['lietotajvards'] = 'Šāds lietotājvārds jau ir aizņemts!';
      }

    }

  }

  if (!empty($data['password1']) || !empty($data['password2'])) {

    if ($data['password1'] != $data['password2']) {
      $errors['rekviziti']['password1'] = 'Paroles nesakrīt!';
      $errors['rekviziti']['password2'] = '';
    } elseif (mb_strlen($data['password1']) < 6) {
      $errors['rekviziti']['password1'] = 'Parole ir par īsu!';
      $errors['rekviziti']['password2'] = '';
    }

  }

  //  Amati

  $amati = array();
  $amati_error = false;

  if(isset($data['amats'])) {
    foreach((array)$data['amats'] as $amats) {
      $k = $amats['sakot_no_y'] . $amats['sakot_no_q'];
      if(isset($amati[$k])) {
        $amati_error = true;
        break;
      } else {
        $amati[$k] = true;
      }
    }
  }

  if(isset($data['amats_new'])) {
    foreach((array)$data['amats_new']['sakot_no_y'] as $i => $sakot_no_y) {
      $k = $sakot_no_y . $data['amats_new']['sakot_no_q'][$i];
      if(isset($amati[$k])) {
        $amati_error = true;
        break;
      } else {
        $amati[$k] = true;
      }
    }
  }

  if($amati_error) {
    $errors['amats'][] = 'Amatu tabulā dublējas rindas!';
  }

  return $errors;

}

function validate_darbinieks_atgadinajumi_form($data) {

  $data = trim_array($data);

  $errors = array();

  return $errors;

}

function validate_atgadinajumi_form($data) {

  $data = trim_array($data);

  $errors = array();

  return $errors;

}

function validate_forums_tema_form($data) {

  $data = trim_array($data);

  $errors = array();

  if (empty($data['tema'])) {
    $errors['form']['tema'] = 'Nav norādīta tēma!';
  }

  if (empty($data['saturs'])) {
    $errors['form']['saturs'] = 'Nav norādīts teksts!';
  }

  return $errors;

}

function validate_forums_atbilde_form($data) {

  $data = trim_array($data);

  $errors = array();

  if (empty($data['saturs'])) {
    $errors['form']['saturs'] = 'Nav norādīts teksts!';
  }

  return $errors;

}

function validate_darb_kopsavilkums_form($data) {

  $data = trim_array($data);

  $errors = array();


  return $errors;

}

function validate_lig_kopsavilkums_form($data) {

  $data = trim_array($data);

  $errors = array();


  return $errors;

}

function validate_beigu_statuss_form($data) {

  $data = trim_array($data);

  $errors = array();

  if (empty($data['beigu_statuss'])) {
    $errors['form']['beigu_statuss'] = 'Nav norādīts veids!';
  } else {

    if ($data['beigu_statuss'] == 'parslegts') {

      if (empty($data['parslegts_lig_nr'])) {
        $errors['form']['parslegts_lig_nr'] = 'Nav norādīts jaunā līguma numurs!';
      }


    } else if ($data['beigu_statuss'] == 'termins') {

      if (empty($data['atlikt_lidz'])) {
        $errors['form']['atlikt_lidz'] = 'Nav norādīts datums līdz guram atlikt statusa nomaiņu!';
      }

      if (empty($data['komentars1'])) {
        $errors['form']['komentars1'] = 'Nav norādīts komentārs pie atlikšanas uz laiku!';
      }

    } elseif ($data['beigu_statuss'] == 'atteikums') {

      if (empty($data['komentars2'])) {
        $errors['form']['komentars2'] = 'Nav norādīts komentārs pie atteikuma!';
      }

    }

  }

  return $errors;

}

function validate_darbinieks_statistika_form($data) {

  $data = trim_array($data);

  $errors = array();

  return $errors;

}

function validate_darbinieks_izpilde_form($data) {

  $data = trim_array($data);

  $errors = array();

  $min_izpildes_error = false;
  $min_izpildes = array();

  if (!empty($data['min_izpilde'])) {

    foreach($data['min_izpilde'] as $p) {

      $k = $p['sakot_no_y'] . '-' . $p['sakot_no_m'];

      if (isset($min_izpildes[$k])) {
        $min_izpildes_error = true;
        break;
      } else {
        $min_izpildes[$k] = true;
      }

    }

  }

  if (!empty($data['min_izpilde_new']['sakot_no_y'])) {

    foreach($data['min_izpilde_new']['sakot_no_y'] as $i => $sakot_no_y) {

      $sakot_no_m = $data['min_izpilde_new']['sakot_no_m'][$i];

      $k = $sakot_no_y . '-' . $sakot_no_m;

      if (isset($min_izpildes[$k])) {
        $min_izpildes_error = true;
        break;
      } else {
        $min_izpildes[$k] = true;
      }

    }

  }

  if ($min_izpildes_error)
    $errors['izpilde'][] = 'Min. izpildes tabulā dublējās rindas!';

  return $errors;

}

function validate_darbinieks_bonuss_form($data)
{
  $errors = array();

  return $errors;
}

function validate_piedavajums_form($data) {
  $errors = array();

  $model = new Piedavajums;
  $model->setValues($data['Piedavajums']);
  $model->validate();

  if($model->hasErrors()) {
    foreach($model->errors as $key => $errs) {
      $errors['form']['piedavajums_'.$key] = $errs[0];
    }
  }

  return $errors;
}

function validate_atslegvards_form($data)
{
  $errors = array();

  foreach($data['Atslegvards'] as $av) {
    $model = new Atslegvards;

    $model->setValues($av);
    $model->owner_id = $data['owner_id'] ? $data['owner_id'] : null;

    $model->validate();

    if($model->hasErrors()) {
      foreach($model->errors as $key => $errs) {
        $errors['form']['atslegvards_'.$key] = $errs[0];
      }
    }
  }

  return $errors;
}

function validate_std_piedavajums_form($data)
{
  $errors = array();

  $model = new StandartaPiedavajums;
  $model->setValues($data['StandartaPiedavajums']);
  $model->nozare_id = $data['nozare_id'] ? $data['nozare_id'] : null;   
  $model->validate();

  if($model->hasErrors()) {
    foreach($model->errors as $key => $errs) {
      $errors['form']['standartapiedavajums_'.$key] = $errs[0];
    }
  }



  return $errors;
}

function validate_nozare_form($data)
{
  $errors = array();

  $model = new Nozare;
  $model->setValues($data['Nozare']);

  $model->validate();

  if($model->hasErrors()) {
    foreach($model->errors as $key => $errs) {
      $errors['form']['nozare_'.$key] = $errs[0];
    }
  }

  return $errors;
}

function validate_uzstadijumi_form($data)
{
  $errors = array();

  foreach($data['Uzstadijums'] as $id => $values) {
    $model = Uzstadijums::model()->findByPk($id);
    $model->setValues($values);
    $model->validate();

    if($model->hasErrors()) {
      foreach($model->errors as $key => $errs) {
        $errors['form']['uzstadijums_'.$id . '_' . $key] = $errs[0];
      }
    }
  }

  return $errors;
}

function validate_forum_category_form($data)
{
  $errors = array();

  $model = new ForumCategory;
  $model->setValues($data['ForumCategory']);

  $model->validate();

  if($model->hasErrors()) {
    foreach($model->errors as $key => $errs) {
      $errors['form']['forumcategory_'.$key] = $errs[0];
    }
  }

  return $errors;
}

function validate_grupu_tiesibas_form($data) {
  $errors = array();

  return $errors;
}

function validate_budzeta_parskaitijums_form($data) {
  $errors = array();

  foreach($_POST['Parskaitijums'] as $id => $data) {
    $model = BudzetaParskaitijums::model()->findByPk($id);
    $model->setValues($data);

    $model->validate('update');

    if($model->hasErrors()) {
      foreach($model->errors as $key => $errs) {
        $errors['form']['parskaitijums_'.$key] = $errs[0];
      }
    }
  }

  return $errors;
}
?>