<?php



// 23.03.2012. Dāvis Frīdenvalds - visi komenti kuriem ir ? priekšā ir rakstīti tikai tagad,
// minot un mēģinot iebraukt šajā funkcijā - tie var būt nepareizi vai bezjēdzīgi - nepaļauties.
function get_statistika($period_type, $from, $to, $pardeveja_id, $show_till = false, $show_empty = true, $exclude_piedzina = false) {
	global $dbligumi,$dbligumi_beigusies,$dbligumi_parslegsanu_rinda,$dbligumi_saistitie_pardeveji,$dbrekini,$dbligumi_beigusies2;
	
	if (!isset($dbligumi)){
		$tmp=db_get_assoc_all(db_query("select * from ".DB_PREF."ligumi"));
		$dbligumi=array();
		foreach ($tmp as $r){
			$dbligumi[$r['id']]=$r;
		}
		unset($tmp);
	}
	if (!isset($dbligumi_beigusies)){
		$dbligumi_beigusies=db_get_assoc_all(db_query("select * from ".DB_PREF."ligumi_beigusies"));
		$lig=array();
		$lig2=array();
		foreach ($dbligumi_beigusies as $r){
			if (!isset($lig[$r['liguma_id']])){
				$lig[$r['liguma_id']]=array();
			}
			if (!isset($lig2[$r['parslegts_lig_nr']])){
				$lig[$r['parslegts_lig_nr']]=array();
			}
			$lig[$r['liguma_id']][]=$r;
			$lig2[$r['parslegts_lig_nr']][]=$r;
		}
		$dbligumi_beigusies=$lig;
		$dbligumi_beigusies2=$lig2;
		unset($lig,$lig2);
	}
	if (!isset($dbligumi_parslegsanu_rinda)){
		$dbligumi_parslegsanu_rinda=db_get_assoc_all(db_query("select * from ".DB_PREF."ligumi_parslegsanu_rinda"));
		$lig=array();
		foreach ($dbligumi_parslegsanu_rinda as $r){
			if (!isset($lig[$r['jaunais_liguma_id']])){
				$lig[$r['jaunais_liguma_id']]=array();
			}
			$lig[$r['jaunais_liguma_id']][]=$r;
		}
		$dbligumi_parslegsanu_rinda=$lig;
		unset($lig);
	}
	if (!isset($dbligumi_saistitie_pardeveji)){
		$dbligumi_saistitie_pardeveji=db_get_assoc_all(db_query("select * from ".DB_PREF."ligumi_saistitie_pardeveji"));
		$lig=array();
		foreach ($dbligumi_saistitie_pardeveji as $r){
			if (!isset($lig[$r['liguma_id']])){
				$lig[$r['liguma_id']]=array();
			}
			$lig[$r['liguma_id']][]=$r;
		}
		$dbligumi_saistitie_pardeveji=$lig;
		unset($lig);
	}
	if (!isset($dbrekini)){
		$dbrekini=db_get_assoc_all(db_query("select * from ".DB_PREF."rekini"));
		$rek=array();
		foreach ($dbrekini as $r){
			if (!isset($rek[$r['liguma_id']])){
				$rek[$r['liguma_id']]=array();
			}
			$rek[$r['liguma_id']][]=$r;
		}
		$dbrekini=$rek;
		unset($rek);
	}
	
  $cur_period = date('Y') . '-';

  if ($period_type == 'month') {
    //? Skatās tikai mēneša statistiku
    $cur_period .= date('m');
  } else {
    //? Skatās ceturksni

    if (date('m') <= 3) {
      $cur_period .= 1;
    } elseif (date('m') <= 6) {
      $cur_period .= 2;
    } elseif (date('m') <= 9) {
      $cur_period .= 3;
    } elseif (date('m') <= 12) {
      $cur_period .= 4;
    }
  }

  if ($period_type == 'month') {
    //? Ja skatās mēneša statistiku

    list($to_y, $to_m) = explode('-', $to);
    $to = $to_y . '-' . str_pad($to_m, 2, '0', STR_PAD_LEFT);  //? gads-divciparu mēnesis

    list($from_y, $from_m) = explode('-', $from);
    $from = $from_y . '-' . str_pad($from_m, 2, '0', STR_PAD_LEFT); //? gads-divciparu mēnesis

    if ($show_till) {
      list($show_till_y, $show_till_m) = explode('-', $show_till);
      $show_till = $show_till_y . '-' . str_pad($show_till_m, 2, '0', STR_PAD_LEFT); //? gads-divciparu mēnesis
    }
  }

  //? Ceturkšņu mēneši no līdz
  $quarters = array(
    1 => array('01', '03'),
    2 => array('04', '06'),
    3 => array('07', '09'),
    4 => array('10', '12'),
  );

  $vars = array(
    'atdoti'          => array('skaits' => 0, 'apgr' => 0, 'vid_summa' => 0, 'ligumu_id' => array()),
    'apstradati'      => array('skaits' => 0, 'apgr' => 0, 'proc_atdot_sk' => 0, 'proc_atdot_apgr' => 0, 'vid_summa' => 0, 'ligumu_id' => array()),
    'atkartoti'       => array('skaits' => 0, 'apgr' => 0, 'proc_apstr_sk' => 0, 'proc_apstr_apgr' => 0, 'vid_summa' => 0, 'ligumu_id' => array()),
    'atkartoti_iepr'  => array('skaits' => 0, 'apgr' => 0, 'ligumu_id' => array()),
    'zaudeti'         => array('skaits' => 0, 'apgr' => 0, 'proc_atdot_sk' => 0, 'proc_atdot_apgr' => 0, 'vid_summa' => 0, 'ligumu_id' => array()),
    'jauni'           => array('skaits' => 0, 'apgr' => 0, 'proc_kopa_sk' => 0,  'proc_kopa_apgr' => 0, 'vid_summa' => 0, 'ligumu_id' => array()),
    'atjaunoti'       => array('skaits' => 0, 'apgr' => 0, 'proc_kopa_sk' => 0,  'proc_kopa_apgr' => 0, 'vid_summa' => 0, 'ligumu_id' => array()),
    'kopa'            => array('skaits' => 0, 'apgr' => 0, 'proc_atdot_sk' => 0, 'proc_atdot_apgr' => 0, 'vid_summa' => 0, 'ligumu_id' => array()),
    'neaiztikti'      => array('skaits' => 0, 'apgr' => 0, 'proc_atdot_sk' => 0, 'proc_atdot_apgr' => 0, 'vid_summa' => 0, 'ligumu_id' => array())
  );

  if (!empty($pardeveja_id)) {
    //? Konkrēta pārdevēja stati

    // Oriģinālie līgumi (arī tie, kam jau saistītie pārdevēji)
    $orig_ligumu_id = array();
    $sql = "
      SELECT g.id
      FROM ".DB_PREF."ligumi g
      LEFT JOIN ".DB_PREF."ligumi_saistitie_pardeveji s ON (s.liguma_id = g.id)
      WHERE
        g.pardeveja_id = ".$pardeveja_id."
    ";
    foreach(db_get_assoc_all(db_query($sql)) as $row) {
      if (!in_array($row['id'], $orig_ligumu_id)) {
        $orig_ligumu_id[] = $row['id'];
      }
    }

		$orig_ligumu_id = array();
		
    // Zaudēti līgumu ID

    $zaudeti_ligumu_id = array();

    //?  Saistītie zaudētie
    $sql = "
      SELECT s.liguma_id
      FROM ".DB_PREF."ligumi_saistitie_pardeveji s
      LEFT JOIN ".DB_PREF."ligumi_beigusies b ON (b.liguma_id = s.liguma_id)
      WHERE
        s.id = (
          SELECT s2.id
          FROM ".DB_PREF."ligumi_saistitie_pardeveji s2
          WHERE s2.liguma_id = s.liguma_id
          ORDER BY s2.piesaist_datums ASC, s2.id ASC
          LIMIT 1
        ) AND
        s.pardeveja_id = ".$pardeveja_id." AND
        b.beigu_statuss = 'atteikums' AND
        b.pirmoreiz_apstradats > s.piesaist_datums -- līgums, kas piesaistīts tajā dienā, kad nomainīts beigu statuss, iet pie oriģinālā
    ";
		
    foreach(db_get_assoc_all(db_query($sql)) as $row) {
      if (!in_array($row['liguma_id'], $zaudeti_ligumu_id)) {
        $zaudeti_ligumu_id[] = $row['liguma_id'];
      }
    }

    //? Oriģinālie zaudētie
    $sql = "
      SELECT g.id
      FROM new_ligumi g
      LEFT JOIN new_ligumi_beigusies b ON (b.liguma_id = g.id)
      LEFT JOIN new_ligumi_saistitie_pardeveji s ON (s.liguma_id = g.id)
      WHERE
        g.pardeveja_id = ".$pardeveja_id." AND
        b.beigu_statuss = 'atteikums' AND
        (
          (
            s.id IS NOT NULL AND
            s.id = (
              SELECT s2.id
              FROM new_ligumi_saistitie_pardeveji s2
              WHERE s2.liguma_id = s.liguma_id
              ORDER BY s2.piesaist_datums ASC, s2.id ASC
              LIMIT 1
            ) AND
            s.piesaist_datums >= b.pirmoreiz_apstradats -- līgums, kas piesaistīts tajā dienā, kad nomainīts beigu statuss, iet pie oriģinālā
          )
          OR
          (
            s.id IS NULL
          )
        )
    ";
		
    foreach(db_get_assoc_all(db_query($sql)) as $row) {
      if (!in_array($row['id'], $zaudeti_ligumu_id)) {
        $zaudeti_ligumu_id[] = $row['id'];
      }
    }


    // Atdoti līgumu ID

    $atdoti_ligumu_id = array();

    //? Atdoti saistītie
    $sql = "
      SELECT s.liguma_id
      FROM ".DB_PREF."ligumi_saistitie_pardeveji s
      LEFT JOIN ".DB_PREF."ligumi g ON (g.id = s.liguma_id)
      WHERE
        s.id = (
          SELECT s2.id
          FROM ".DB_PREF."ligumi_saistitie_pardeveji s2
          WHERE s2.liguma_id = s.liguma_id
          ORDER BY s2.piesaist_datums ASC, s2.id ASC
          LIMIT 1
        ) AND
        s.pardeveja_id = ".$pardeveja_id." AND
        g.reklama_lidz >= s.piesaist_datums
    ";
		
    foreach(db_get_assoc_all(db_query($sql)) as $row) {
      if (!in_array($row['liguma_id'], $atdoti_ligumu_id)) {
        $atdoti_ligumu_id[] = $row['liguma_id'];
      }
    }

    //? Atdoti oriģinālie
    $sql = "
      SELECT g.id
      FROM new_ligumi g
      -- LEFT JOIN new_ligumi_beigusies b ON (b.liguma_id = g.id)
      LEFT JOIN new_ligumi_saistitie_pardeveji s ON (s.liguma_id = g.id)
      WHERE
        g.pardeveja_id = ".$pardeveja_id." AND
        -- b.beigu_statuss IN ('atteikums', 'parslegts') AND
        (
          (
            s.id IS NOT NULL AND
            s.id = (
              SELECT s2.id
              FROM new_ligumi_saistitie_pardeveji s2
              WHERE s2.liguma_id = s.liguma_id
              ORDER BY s2.piesaist_datums ASC, s2.id ASC
              LIMIT 1
            ) AND
            s.piesaist_datums > g.reklama_lidz
          )
          OR
          (
            s.id IS NULL
          )
        )
    ";

    foreach(db_get_assoc_all(db_query($sql)) as $row) {
      if (!in_array($row['id'], $atdoti_ligumu_id)) {
        $atdoti_ligumu_id[] = $row['id'];
      }
    }

  } else {
    //?  Nav norādīts konkrēts pārdevējs

    $ligumu_id = array();

    //? Visi līgumi, kuri pieder pārdevējiem - oriģināliem vai saistītiem.
    $sql = "
      SELECT g.id
      FROM ".DB_PREF."ligumi g
      LEFT JOIN ".DB_PREF."pardeveji p ON (p.id = g.pardeveja_id)
      LEFT JOIN ".DB_PREF."ligumi_saistitie_pardeveji s ON (s.liguma_id = g.id)
      LEFT JOIN ".DB_PREF."pardeveji p2 ON (p2.id = s.pardeveja_id)
      WHERE
        (
          p.id IS NOT NULL AND
          p.tips = 'sales'
        ) OR (
          p2.id IS NOT NULL AND
          p.tips = 'sales'
        )
    ";

    $ligumu_id_raw = db_get_assoc_all(db_query($sql));

    foreach($ligumu_id_raw as $row) {
      if (!in_array($row['id'], $ligumu_id)) {
        $ligumu_id[] = $row['id'];
      }
    }

  }

  $anuleti_ligumu_id = array();

  //? Līgumi kuriem visi rēķini ir anulēti
  $sql = "
    SELECT
      g.id,
      (
        SELECT COUNT(*)
        FROM ".DB_PREF."rekini r2
        WHERE r2.liguma_id = g.id
      ) AS rekinu_skaits,
      (
        SELECT COUNT(*)
        FROM ".DB_PREF."rekini r3
        WHERE r3.liguma_id = g.id AND r3.anulets = 1
      ) AS anuleto_skaits
    FROM ".DB_PREF."ligumi g
    HAVING
      rekinu_skaits > 0 AND
      rekinu_skaits = anuleto_skaits
  ";

  foreach(db_get_assoc_all(db_query($sql)) as $row) {
    if (!in_array($row['id'], $anuleti_ligumu_id)) {
      $anuleti_ligumu_id[] = $row['id'];
    }
  }
	
  // Uz piedziņu
  if($exclude_piedzina) {
    $piedzina_ligumu_id = array();

    //? Līgumi, kuriem visi rēķini ir aizvesti uz piedziņu
    $sql = "
      SELECT
        g.id,
        (
          SELECT COUNT(*)
          FROM ".DB_PREF."rekini r2
          WHERE r2.liguma_id = g.id
        ) AS rekinu_skaits,
        (
          SELECT COUNT(*)
          FROM ".DB_PREF."rekini r3
          WHERE r3.liguma_id = g.id AND r3.nodots_piedz = 1
        ) AS piedzina_skaits
      FROM ".DB_PREF."ligumi g
      HAVING
        rekinu_skaits > 0 AND
        rekinu_skaits = piedzina_skaits
    ";


    foreach(db_get_assoc_all(db_query($sql)) as $row) {
      if (!in_array($row['id'], $piedzina_ligumu_id)) {
        $piedzina_ligumu_id[] = $row['id'];
      }
    }
  }

  $result = array();

  $total = $vars; //?  Grupu skelets

  list($y, $period) = explode('-', $from); //? $period šobrīd var būt gan mēneša gan ceturkšņa cipars
	
  while($y . '-' . $period <= $to) {
    //? Tagad katram "periodam" taisīs visas atlases

    $r = false;

    if ((empty($show_till) || $y . '-' . $period <= $show_till)) { //? Show till laikam ir kkāds papildus ierobežojums apskatāmajam intervālam

      $r = $vars; //?  Grupu skelets

      if ($period_type == 'month') {
        //?  Ja apskatīts tiek mēnesis

        //? Nosaka ceturksni
        if ($period <= 3)
          $cur_q = 1;
        else if ($period <= 6)
          $cur_q = 2;
        else if ($period <= 9)
          $cur_q = 3;
        else if ($period <= 12)
          $cur_q = 4;

      } else {
        //? Ja apskatīts tiek ceturksnis

        $cur_q = (int)$period;
      }
			
			
      //? Iegūst ceturkšņa sākuma mēnesi un beigu mēnesi. Šitas ir neatkarīgi vai skatāmais periods ir ceturksnis vai mēnesis
      $q_from = $y . $quarters[$cur_q][0]; //? gadsmenesis 201201
      $q_to = $y . $quarters[$cur_q][1]; //? gadsmenesis 201201

      if ($period_type == 'month') {
        $month = $y . $period; //? gadsmenesis 201201
      }
			
			$qfrom=$y.'-'.$quarters[$cur_q][0]; //? gadsmenesis 201201
      $qto=$y.'-'.$quarters[$cur_q][1]; //? gadsmenesis 201201
      if ($period_type=='month') {
        $month2=$y.'-'.$period; //? gadsmenesis 201201
      }
			
			/*
      // Atdotie
			
      $sql = "
        SELECT
          g.id,
          g.neradit_atd_ligumi,
          g.neradit_atd_apgr,
          (
            SELECT SUM(r.summa)
            FROM ".DB_PREF."rekini r
            WHERE r.liguma_id = g.id AND r.atkapsanas_rekins = 0
          ) AS rekinu_summa,
          (
            SELECT COUNT(*)
            FROM ".DB_PREF."rekini r2
            WHERE r2.liguma_id = g.id
          ) AS rekinu_skaits,
          (
            SELECT COUNT(*)
            FROM ".DB_PREF."rekini r3
            WHERE r3.liguma_id = g.id AND r3.anulets = 1
          ) AS anuleto_skaits";

        //? Ja prasīta statistika kam jāatfiltrē uz piedziņu aizvestos
        if($exclude_piedzina) {
          $sql .= ", (
            SELECT COUNT(*)
            FROM ".DB_PREF."rekini r4
            WHERE r4.liguma_id = g.id AND r4.nodots_piedz = 1
          ) AS piedzina_skaits";
        };

        $sql .= " FROM ".DB_PREF."ligumi g
        WHERE
          -- g.neradit_atdotajos = 0 AND
          EXTRACT(YEAR_MONTH FROM g.reklama_lidz) " . ($period_type == 'quarter' ? "BETWEEN '".$q_from."' AND '".$q_to."'" : "= '".$month."'"). "
      "; //? $month īstenībā ir gadsmenesis

      //? Te tiek realizēta atlase pēc izvēlētā pārdevēja vai ja tas nav norādīts tad tos kuriem vispār ir pārdevējs
      if (!empty($pardeveja_id)) {
        $sql .= " AND g.id IN (".(!empty($atdoti_ligumu_id) ? implode(',', $atdoti_ligumu_id) : 'NULL').")";
      } else {
        $sql .= " AND g.id IN (".(!empty($ligumu_id) ? implode(',', $ligumu_id) : 'NULL').")";
      }

      //? Neiekļaut tos kas anulēti
      if (!empty($anuleti_ligumu_id))
        $sql .= " AND g.id NOT IN (".implode(',', $anuleti_ligumu_id).")";

      //  Neiekļaut tos, kas uz piedziņu
      if ($exclude_piedzina && !empty($piedzina_ligumu_id))
        $sql .= " AND g.id NOT IN (".implode(',', $piedzina_ligumu_id).")";

      $atdoti = db_get_assoc_all(db_query($sql));
			*/

			
			//? Te tiek realizēta atlase pēc izvēlētā pārdevēja vai ja tas nav norādīts tad tos kuriem vispār ir pārdevējs
			$tmpligumi=array();
			if (!empty($pardeveja_id) and !empty($atdoti_ligumu_id)){
				foreach ($atdoti_ligumu_id as $aid){
					$tmpligumi[$aid]=$dbligumi[$aid];
				}
      }
			elseif (!empty($ligumu_id)){
				foreach ($ligumu_id as $lid){
					$tmpligumi[$lid]=$dbligumi[$lid];
				}
      }
			//? Neiekļaut tos kas anulēti
			if (!empty($anuleti_ligumu_id)){
				foreach ($anuleti_ligumu_id as $aid){
					unset($tmpligumi[$aid]);
				}
			}
			//  Neiekļaut tos, kas uz piedziņu
			if ($exclude_piedzina && !empty($piedzina_ligumu_id)){
				foreach ($piedzina_ligumu_id as $pid){
					unset($tmpligumi[$pid]);
				}
			}
			
			if ($period_type=='quarter'){
				foreach ($tmpligumi as $lid=>$g){
					if ($qfrom>substr($g['reklama_lidz'],0,7) or substr($g['reklama_lidz'],0,7)>$qto){
						unset($tmpligumi[$lid]);
					}
				}
			}
			else{
			 foreach ($tmpligumi as $lid=>$g){
					if (substr($g['reklama_lidz'],0,7)!=$month2){
						unset($tmpligumi[$lid]);
					}
				}
			}
			
			$atdoti=array();
			foreach ($tmpligumi as $gid=>$g){
				$rekinu_summa=0;
				$rekinu_skaits=0;
				$anuleto_skaits=0;
				$piedzina_skaits=0;
				if (isset($dbrekini[$gid])){
					foreach ($dbrekini[$gid] as $re){
						if ($re['atkapsanas_rekins']=='0'){
							$rekinu_summa+=$re['summa'];
						}
						$rekinu_skaits++;
						if ($re['anulets']=='1'){
							$anuleto_skaits++;
						}
						if ($re['nodots_piedz']=='1'){
							$piedzina_skaits++;
						}
					}
				}
				$atdoti[]=array('id'=>$gid,'neradit_atd_ligumi'=>$g['neradit_atd_ligumi'],'neradit_atd_apgr'=>$g['neradit_atd_apgr'],'rekinu_summa'=>$rekinu_summa,'rekinu_skaits'=>$rekinu_skaits,'anuleto_skaits'=>$anuleto_skaits,'piedzina_skaits'=>$piedzina_skaits);
			}


      //? Iet cauri visiem savāktajiem un updeito perioda status. arī total.
      foreach($atdoti as $row) {

        if(!$row['neradit_atd_apgr']) {
          $r['atdoti']['apgr'] += $row['rekinu_summa'];
        }

        if(!$row['neradit_atd_ligumi']) {
          $r['atdoti']['skaits'] ++;

          $r['atdoti']['ligumu_id'][] = $row['id'];
          $total['atdoti']['ligumu_id'][] = $row['id'];
        }
      }

      $total['atdoti']['skaits'] += $r['atdoti']['skaits'];
      $total['atdoti']['apgr'] += $r['atdoti']['apgr'];




      // Atkārtotie

      $sql = "
        SELECT
          g2.id as jaunais_id,
          (
            SELECT SUM(r.summa)
            FROM ".DB_PREF."rekini r
            WHERE r.liguma_id = g2.id AND r.atkapsanas_rekins = 0
          ) AS jauna_rekinu_summa,
          SUM((
            SELECT SUM(r.summa)
            FROM ".DB_PREF."rekini r
            WHERE r.liguma_id = g.id AND r.atkapsanas_rekins = 0
          )) AS iepriekseja_rekinu_summa,
          COUNT(*) as ieprieksejo_ligumu_skaits,
          GROUP_CONCAT(g.id SEPARATOR ',') as ieprieksejo_ligumu_id
        FROM ".DB_PREF."ligumi g
        LEFT JOIN ".DB_PREF."ligumi_beigusies b ON (b.liguma_id = g.id )
        LEFT JOIN ".DB_PREF."ligumi_parslegsanu_rinda p ON (p.liguma_id = g.id)
        LEFT JOIN ".DB_PREF."ligumi_saistitie_pardeveji s ON (s.liguma_id = g.id)
        LEFT JOIN ".DB_PREF."ligumi g2 ON (
          (
            b.id IS NOT NULL AND
            g2.ligumanr = b.parslegts_lig_nr
          ) OR
          (
            p.id IS NOT NULL AND
            g2.id = p.jaunais_liguma_id
          )
        )
        WHERE
          EXTRACT(YEAR_MONTH FROM g.reklama_lidz) " . ($period_type == 'quarter' ? "BETWEEN '".$q_from."' AND '".$q_to."'" : "= '".$month."'") . " AND
          (
            (
              b.id IS NOT NULL AND
              b.beigu_statuss IN ('parslegts') AND
              -- EXTRACT(YEAR_MONTH FROM b.pirmoreiz_apstradats) BETWEEN '".$q_from."' AND '".$q_to."'
              -- 23.03.2012 - Pie atkārtotajiem neparādījās līgums kuram beidzās termiņš ceturkšņa pēdējā dienā.
              -- Automātiskā apstrāde notiek nākamās dienas plkst. 00:00 un pirmoreiz_apstrādāts tiek ielikts skripta izpildes datums,
              -- un tāpēc iekrīt nākamajā ceturksnī.
              EXTRACT(YEAR_MONTH FROM DATE_SUB(b.pirmoreiz_apstradats, INTERVAL 1 DAY)) BETWEEN '".$q_from."' AND '".$q_to."'
            ) OR
            (
              p.id IS NOT NULL AND
              -- EXTRACT(YEAR_MONTH FROM DATE_ADD(g.reklama_lidz, INTERVAL 1 DAY)) BETWEEN '".$q_from."' AND '".$q_to."'
              -- 23.03.2012. Pie atkārtotajiem neparādās līgumi kuri beigsies konkrētā ceturkšņa pēdējā dienā,
              -- jo pieskaitot beigu termiņam vienu dienu, tas uzreiz nonāk nākamajā ceturksnī.
              -- Man nav skaidrs kāpēc to vienu dienu vajadzēja skaitīt klāt.
              EXTRACT(YEAR_MONTH FROM g.reklama_lidz) BETWEEN '".$q_from."' AND '".$q_to."'
            )
          ) AND
          EXTRACT(YEAR_MONTH FROM g2.sanemsanas_datums) <= '".$q_to."' AND
          -- 23.03.2012 - vajadzēja, lai mēneša ietvaros parezi skatās atkārtotos
          -- EXTRACT(YEAR_MONTH FROM g2.sanemsanas_datums) " . ($period_type == 'quarter' ? "<= '".$q_to."'" : "<= '".$month."'") . " AND
          (
            (
              s.id IS NOT NULL AND
              s.id = (
                SELECT s2.id
                FROM new_ligumi_saistitie_pardeveji s2
                WHERE s2.liguma_id = s.liguma_id
                ORDER BY s2.piesaist_datums ASC, s2.id ASC
                LIMIT 1
              ) AND
              s.pardeveja_id = g2.pardeveja_id AND
              (
                (
                  b.id IS NOT NULL AND
                  s.piesaist_datums < b.pirmoreiz_apstradats
                ) OR
                (
                  p.id IS NOT NULL AND
                  s.piesaist_datums < DATE_ADD(g.reklama_lidz, INTERVAL 1 DAY)
                )
              )
            )
            OR
            (
              s.id IS NULL AND
              g.pardeveja_id = g2.pardeveja_id
            )
          )
      ";
			
			$empty=false;
      if (!empty($pardeveja_id)) {
        $sql .= " AND g2.id IN (".(!empty($orig_ligumu_id) ? implode(',', $orig_ligumu_id) : 'NULL').")";
				if (empty($orig_ligumu_id)){
					$empty=true;
				}
      } else {
        $sql .= " AND g2.id IN (".(!empty($ligumu_id) ? implode(',', $ligumu_id) : 'NULL').")";
				if (empty($ligumu_id)){
					$empty=true;
				}
      }

      if (!empty($anuleti_ligumu_id))
        $sql .= " AND g2.id NOT IN (".implode(',', $anuleti_ligumu_id).")";

      if ($exclude_piedzina && !empty($piedzina_ligumu_id))
        $sql .= " AND g2.id NOT IN (".implode(',', $piedzina_ligumu_id).")";


      $sql .= "
        GROUP BY g2.id
      ";
			
			if ($empty){
				$atkartotie=array();
			}
			else{
      	$atkartotie = db_get_assoc_all(db_query($sql));
			}
			
			
			/*
			$tmpligumi2=$tmpligumi;
			$tmpligumi3=array();
			$tmpligumi_beigusies=array();
			$tmpligumi_parslegsanu_rinda=array();
			foreach ($tmpligumi2 as $gid=>$g){
				$is=false;
				if (isset($dbligumi_beigusies[$gid])){   //b.id IS NOT NULL
					$tmpligumi_beigusies[$gid]=$dbligumi_beigusies[$gid];
					foreach ($dbligumi_beigusies[$gid] as $b){   //b.beigu_statuss IN ('parslegts') AND EXTRACT(YEAR_MONTH FROM DATE_SUB(b.pirmoreiz_apstradats, INTERVAL 1 DAY)) BETWEEN '".$q_from."' AND '".$q_to."'
						$date=date('Y-m',mktime(0,0,0,substr($b['pirmoreiz_apstradats'],5,2),substr($b['pirmoreiz_apstradats'],8,2),substr($b['pirmoreiz_apstradats'],0,4)));
						if ($b['beigu_statuss']=='parslegts' and $date>=$qfrom and $date<=$qto){
							$is=true;
							break;
						}
					}
				}
				if (isset($dbligumi_parslegsanu_rinda[$gid])){
					$tmpligumi_parslegsanu_rinda[$gid]=$dbligumi_parslegsanu_rinda[$gid];
					if ($is or (substr($g['reklama_lidz'],0,7)>=$qfrom and substr($g['reklama_lidz'],0,7)<=$qto)){   //p.id IS NOT NULL AND EXTRACT(YEAR_MONTH FROM g.reklama_lidz) BETWEEN '".$q_from."' AND '".$q_to."'
						$tmpligumi3[$gid]=$g;
					}
				}
			}
			$tmpligumi2=$tmpligumi3;
			
			if (count($tmpligumi2)){
				$g2=array();
				foreach ($dbligumi as $gid=>$g){
					$is=false;
					foreach ($tmpligumi_beigusies as $gid2=>$lb){
						foreach ($lb as $b){
							if ($g['ligumanr']==$b['parslegts_lig_nr']){   //g2.ligumanr = b.parslegts_lig_nr
								$is=$gid2;
								break;
							}
						}
					}
					if (!$is){
						foreach ($tmpligumi_parslegsanu_rinda as $gid2=>$pr){
							foreach ($pr as $p){
								if ($g['id']==$p['jaunais_liguma_id']){   //g2.id = p.jaunais_liguma_id
									$is=$gid2;
									break;
								}
							}
						}
					}
					if ($is){
						$g2[$gid2]=$g;
					}
				}
				
				
				
				$tmpligumi3=array();
				foreach ($tmpligumi2 as $gid=>$g){
					if (isset($g2[$gid])){
						foreach ($g2[$gid] as $gg){
							var_dump($gg['sanemsanas_datums']);
							if (substr($gg['sanemsanas_datums'],0,7)<=$qto){
								$tmpligumi3[$gid]=$g;
							}
						}
					}
				}
			}
			*/
			


      foreach($atkartotie as $row) {

        $r['atkartoti']['skaits'] += $row['ieprieksejo_ligumu_skaits'];
        $r['atkartoti']['apgr'] += $row['jauna_rekinu_summa'];

        $r['atkartoti']['ligumu_id'][] = $row['jaunais_id'];
        $total['atkartoti']['ligumu_id'][] = $row['jaunais_id'];

        $r['atkartoti_iepr']['skaits'] += $row['ieprieksejo_ligumu_skaits'];
        $r['atkartoti_iepr']['apgr'] += $row['iepriekseja_rekinu_summa'];

        foreach(explode(',', $row['ieprieksejo_ligumu_id']) as $v) {
          $r['atkartoti_iepr']['ligumu_id'][] = $v;
        }

      }

      $total['atkartoti']['skaits'] += $r['atkartoti']['skaits'];
      $total['atkartoti']['apgr'] += $r['atkartoti']['apgr'];
			
			
			
			/*
      // Zaudēti

      $sql = "
        SELECT
          g.id,
          (
            SELECT SUM(r.summa)
            FROM ".DB_PREF."rekini r
            WHERE r.liguma_id = g.id AND r.atkapsanas_rekins = 0
          ) AS rekinu_summa
        FROM ".DB_PREF."ligumi g
        LEFT JOIN ".DB_PREF."ligumi_beigusies b ON (b.liguma_id = g.id )
        WHERE
          EXTRACT(YEAR_MONTH FROM g.reklama_lidz) " . ($period_type == 'quarter' ? "BETWEEN '".$q_from."' AND '".$q_to."'" : "= '".$month."'"). " AND
          b.beigu_statuss IN ('atteikums') AND
          EXTRACT(YEAR_MONTH FROM b.pirmoreiz_apstradats) BETWEEN '".$q_from."' AND '".$q_to."' AND
      ";
			
			$empty=false;
      if (!empty($pardeveja_id)) {
        $sql .= " g.id IN (".(!empty($zaudeti_ligumu_id) ? implode(',', $zaudeti_ligumu_id) : 'NULL').")";
				if (empty($zaudeti_ligumu_id)){
					$empty=true;
				}
      }
			else {
        $sql .= " g.id IN (".(!empty($ligumu_id) ? implode(',', $ligumu_id) : 'NULL').")";
				if (empty($ligumu_id)){
					$empty=true;
				}
      }
			
      $sql .= " GROUP BY g.id";
			
			if ($empty){
				$zaudeti=array();
				$totalempty++;
			}
			else{
      	$zaudeti = db_get_assoc_all(db_query($sql));
				$totalselect++;
			}
			*/
			
			// Zaudēti
			$zaudeti=array();
			$tmpligumi=array();
			if (!empty($pardeveja_id)){
				$inarray=$zaudeti_ligumu_id;
			}
			else{
				$inarray=$ligumu_id;
			}
			foreach ($inarray as $in){
				if (isset($dbligumi[$in])){
					if (($period_type=='quarter' and substr($dbligumi[$in]['reklama_lidz'],0,7)>=$qfrom and substr($dbligumi[$in]['reklama_lidz'],0,7)<=$qto) or ($period_type!='quarter' and substr($dbligumi[$in]['reklama_lidz'],0,7)==$month2)){   //EXTRACT(YEAR_MONTH FROM g.reklama_lidz) " . ($period_type == 'quarter' ? "BETWEEN '".$q_from."' AND '".$q_to."'" : "= '".$month."'"). " AND
						$tmpligumi[$in]=$dbligumi[$in];
					}
				}
			}
			foreach ($tmpligumi as $gid=>$g){
				if (isset($dbligumi_beigusies[$gid])){
					$rekinu_summa=0;
					foreach ($dbligumi_beigusies[$gid] as $b){
						if ($b['beigu_statuss']=='atteikums' and substr($b['pirmoreiz_apstradats'],0,7)>=$qfrom and substr($b['pirmoreiz_apstradats'],0,7)<=$qto){
							if (isset($dbrekini[$gid])){
								foreach ($dbrekini[$gid] as $r2){
									if ($r2['atkapsanas_rekins']=='0'){
										$rekinu_summa+=$r2['summa'];
									}
								}
							}
						}
					}
					$zaudeti[]=array('id'=>$gid,'rekinu_summa'=>$rekinu_summa);
				}
			}
			
			
      foreach($zaudeti as $row) {

        $r['zaudeti']['skaits'] ++;
        $r['zaudeti']['apgr'] += $row['rekinu_summa'];

        $r['zaudeti']['ligumu_id'][] = $row['id'];
        $total['zaudeti']['ligumu_id'][] = $row['id'];

      }

      $total['zaudeti']['skaits'] += $r['zaudeti']['skaits'];
      $total['zaudeti']['apgr'] += $r['zaudeti']['apgr'];


      // Apstrādāti
      //? Apstrādātos iegūst vienkārši saliekot kopā zaudētos ar atkārtotajiem

      $r['apstradati']['skaits'] = $r['atkartoti_iepr']['skaits'] + $r['zaudeti']['skaits'];
      $r['apstradati']['apgr'] = $r['atkartoti_iepr']['apgr'] + $r['zaudeti']['apgr'];

      $r['apstradati']['ligumu_id'] = array_merge(
        $r['atkartoti_iepr']['ligumu_id'],
        $r['zaudeti']['ligumu_id']
      );

      $total['apstradati']['skaits'] += $r['apstradati']['skaits'];
      $total['apstradati']['apgr'] += $r['apstradati']['apgr'];

      $total['apstradati']['ligumu_id'] = array_merge(
        $total['apstradati']['ligumu_id'],
        $r['apstradati']['ligumu_id']
      );

/*
      // Jauni

      $sql = "
        SELECT
          g.id,
          (
            SELECT SUM(r.summa)
            FROM ".DB_PREF."rekini r
            WHERE r.liguma_id = g.id AND r.atkapsanas_rekins = 0
          ) AS rekinu_summa
        FROM ".DB_PREF."ligumi g
        LEFT JOIN ".DB_PREF."ligumi_beigusies b ON (b.beigu_statuss = 'parslegts' AND b.parslegts_lig_nr = g.ligumanr)
				LEFT JOIN ".DB_PREF."ligumi_parslegsanu_rinda p ON (p.jaunais_liguma_id = g.id)
        WHERE
          EXTRACT(YEAR_MONTH FROM g.sanemsanas_datums) " . ($period_type == 'quarter' ? "BETWEEN '".$q_from."' AND '".$q_to."'" : "= '".$month."'"). " AND
          b.id IS NULL AND
          p.id IS NULL
      ";
			
			$empty=false;
      if (!empty($pardeveja_id)) {
        $sql .= " AND g.id IN (".(!empty($orig_ligumu_id) ? implode(',', $orig_ligumu_id) : 'NULL').")";
				if (empty($orig_ligumu_id)){
					$empty=true;
				}
      }
			else {
        $sql .= " AND g.id IN (".(!empty($ligumu_id) ? implode(',', $ligumu_id) : 'NULL').")";
				if (empty($ligumu_id)){
					$empty=true;
				}
      }

      if (!empty($anuleti_ligumu_id))
        $sql .= " AND g.id NOT IN (".implode(',', $anuleti_ligumu_id).")";

      $sql .= "
        GROUP BY g.id
      ";
			
			//if ($empty){
			//	$jauni=array();
			//	$totalempty++;
			//}
			//else{
      	$jauni = db_get_assoc_all(db_query($sql));
			//	$totalselect++;
			//}
			*/
			
			$jauni=array();
			$tmpligumi=array();
			if (!empty($pardeveja_id)){
				$inarray=$orig_ligumu_id;
			}
			else{
				$inarray=$ligumu_id;
			}
			foreach ($inarray as $in){
				if (!in_array($in,$anuleti_ligumu_id) and isset($dbligumi[$in])){
					if (($period_type=='quarter' and substr($dbligumi[$in]['sanemsanas_datums'],0,7)>=$qfrom and substr($dbligumi[$in]['sanemsanas_datums'],0,7)<=$qto) or ($period_type!='quarter' and substr($dbligumi[$in]['sanemsanas_datums'],0,7)==$month2)){
						$tmpligumi[$in]=$dbligumi[$in];
					}
				}
			}
			
			foreach ($tmpligumi as $gid=>$g){
				$is=true;
				if (isset($dbligumi_beigusies2[$g['ligumanr']])){
					foreach ($dbligumi_beigusies2[$g['ligumanr']] as $b){
						if ($b['beigu_statuss']!='parslegts'){
							$is=false;
						}
					}
				}
				else{
					$is=false;
				}
				if (!$is and isset($dbligumi_parslegsanu_rinda[$gid])){
					$is=true;
				}
				if (!$is){
					$rekinu_summa=0;
					if (isset($dbrekini[$gid])){
						foreach ($dbrekini[$gid] as $r2){
							if ($r2['atkapsanas_rekins']=='0'){
								$rekinu_summa+=$r2['summa'];
							}
						}
					}
					$jauni[]=array('id'=>$gid,'rekinu_summa'=>$rekinu_summa);
				}
			}
			
      foreach($jauni as $row) {

        $r['jauni']['skaits'] ++;
        $r['jauni']['apgr'] += $row['rekinu_summa'];

        $r['jauni']['ligumu_id'][] = $row['id'];
        $total['jauni']['ligumu_id'][] = $row['id'];

      }

      $total['jauni']['skaits'] += $r['jauni']['skaits'];
      $total['jauni']['apgr'] += $r['jauni']['apgr'];


      // Atjaunoti

      $sql = "
        SELECT
          g.id,
          (
            SELECT SUM(r.summa)
            FROM ".DB_PREF."rekini r
            WHERE r.liguma_id = g.id AND r.atkapsanas_rekins = 0
          ) AS rekinu_summa
        FROM ".DB_PREF."ligumi g
        LEFT JOIN ".DB_PREF."ligumi_beigusies b ON (b.beigu_statuss = 'parslegts' AND b.parslegts_lig_nr = g.ligumanr)
        LEFT JOIN ".DB_PREF."ligumi_parslegsanu_rinda p ON (p.jaunais_liguma_id = g.id)
        LEFT JOIN ".DB_PREF."ligumi g2 ON (
          (
            b.id IS NOT NULL AND
            g2.id = b.liguma_id
          ) OR
          (
            p.id IS NOT NULL AND
            g2.id = p.liguma_id
          )
        )
        WHERE
          EXTRACT(YEAR_MONTH FROM g.sanemsanas_datums) " . ($period_type == 'quarter' ? "BETWEEN '".$q_from."' AND '".$q_to."'" : "= '".$month."'"). " AND
          (
            b.id IS NOT NULL OR
            p.id IS NOT NULL
          ) AND
          EXTRACT(YEAR_MONTH FROM g2.reklama_lidz) < '".$q_from."'
          -- 23.03.2012. vajag lai pa mēnešiem arī skatās
          -- EXTRACT(YEAR_MONTH FROM g2.reklama_lidz) " . ($period_type == 'quarter' ? "< '".$q_from."'" : "< '".$month."'") . "
      ";
			
			$empty=false;
      if (!empty($pardeveja_id)) {
        $sql .= " AND g.id IN (".(!empty($orig_ligumu_id) ? implode(',', $orig_ligumu_id) : 'NULL').")";
				if (empty($orig_ligumu_id)){
					$empty=true;
				}
      }
			else {
        $sql .= " AND g.id IN (".(!empty($ligumu_id) ? implode(',', $ligumu_id) : 'NULL').")";
				if (empty($ligumu_id)){
					$empty=true;
				}
      }

      if (!empty($anuleti_ligumu_id))
        $sql .= " AND g.id NOT IN (".implode(',', $anuleti_ligumu_id).")";

      $sql .= "
        GROUP BY g.id
      ";
			
			if ($empty){
				$atjaunoti=array();
			}
			else{
      	$atjaunoti = db_get_assoc_all(db_query($sql));
			}
			
      foreach($atjaunoti as $row) {

        $r['atjaunoti']['skaits'] ++;
        $r['atjaunoti']['apgr'] += $row['rekinu_summa'];

        $r['atjaunoti']['ligumu_id'][] = $row['id'];
        $total['atjaunoti']['ligumu_id'][] = $row['id'];

      }

      $total['atjaunoti']['skaits'] += $r['atjaunoti']['skaits'];
      $total['atjaunoti']['apgr'] += $r['atjaunoti']['apgr'];


      // Atjaunoti - 2

      $sql = "
        SELECT
          g2.id,
          (
            SELECT SUM(r.summa)
            FROM ".DB_PREF."rekini r
            WHERE r.liguma_id = g2.id AND r.atkapsanas_rekins = 0
          ) AS rekinu_summa
        FROM ".DB_PREF."ligumi g
        LEFT JOIN ".DB_PREF."ligumi_beigusies b ON (b.liguma_id = g.id )
        LEFT JOIN ".DB_PREF."ligumi_parslegsanu_rinda p ON (p.liguma_id = g.id)
        LEFT JOIN ".DB_PREF."ligumi g2 ON (
          (
            b.id IS NOT NULL AND
            g2.ligumanr = b.parslegts_lig_nr
          ) OR
          (
            p.id IS NOT NULL AND
            g2.id = p.jaunais_liguma_id
          )
        )
        LEFT JOIN ".DB_PREF."ligumi_saistitie_pardeveji s ON (s.liguma_id = g.id)
        WHERE
          EXTRACT(YEAR_MONTH FROM g.reklama_lidz) " . ($period_type == 'quarter' ? "BETWEEN '".$q_from."' AND '".$q_to."'" : "= '".$month."'") . " AND
          (
            (
              b.id IS NOT NULL AND
              b.beigu_statuss IN ('parslegts') AND
              EXTRACT(YEAR_MONTH FROM b.pirmoreiz_apstradats) BETWEEN '".$q_from."' AND '".$q_to."'
            ) OR
            (
              p.id IS NOT NULL AND
              EXTRACT(YEAR_MONTH FROM DATE_ADD(g.reklama_lidz, INTERVAL 1 DAY)) BETWEEN '".$q_from."' AND '".$q_to."'
            )
          ) AND
          EXTRACT(YEAR_MONTH FROM g2.sanemsanas_datums) <= '".$q_to."' AND
          s.id IS NOT NULL AND
          s.id = (
            SELECT s2.id
            FROM new_ligumi_saistitie_pardeveji s2
            WHERE s2.liguma_id = s.liguma_id
            ORDER BY s2.piesaist_datums ASC, s2.id ASC
            LIMIT 1
          ) AND
          s.pardeveja_id = g2.pardeveja_id AND
          (
            (
              b.id IS NOT NULL AND
              s.piesaist_datums > b.pirmoreiz_apstradats
            ) OR
            (
              p.id IS NOT NULL AND
              s.piesaist_datums > DATE_ADD(g.reklama_lidz, INTERVAL 1 DAY)
            )
          ) AND
          s.piesaist_datums <= g2.sanemsanas_datums
      ";
			
			$empty=false;
      if (!empty($pardeveja_id)) {
        $sql .= " AND g2.id IN (".(!empty($orig_ligumu_id) ? implode(',', $orig_ligumu_id) : 'NULL').")";
				if (empty($orig_ligumu_id)){
					$empty=true;
				}
      }
			else {
        $sql .= " AND g2.id IN (".(!empty($ligumu_id) ? implode(',', $ligumu_id) : 'NULL').")";
				if (empty($ligumu_id)){
					$empty=true;
				}
      }

      if (!empty($anuleti_ligumu_id))
        $sql .= " AND g2.id NOT IN (".implode(',', $anuleti_ligumu_id).")";

      $sql .= "
        GROUP BY g2.id
      ";
			
			if ($empty){
				$atjaunoti=array();
			}
			else{
      	$atjaunoti = db_get_assoc_all(db_query($sql));
			}
			
      foreach($atjaunoti as $row) {

        $r['atjaunoti']['skaits'] ++;
        $r['atjaunoti']['apgr'] += $row['rekinu_summa'];

        $r['atjaunoti']['ligumu_id'][] = $row['id'];
        $total['atjaunoti']['ligumu_id'][] = $row['id'];

      }

      $total['atjaunoti']['skaits'] += $r['atjaunoti']['skaits'];
      $total['atjaunoti']['apgr'] += $r['atjaunoti']['apgr'];


      // Kopā

      $r['kopa']['skaits'] = $r['atkartoti']['skaits'] + $r['jauni']['skaits'] + $r['atjaunoti']['skaits'];
      $r['kopa']['apgr'] = $r['atkartoti']['apgr'] + $r['jauni']['apgr'] + $r['atjaunoti']['apgr'];

      $r['kopa']['ligumu_id'] = array_merge(
        $r['atkartoti']['ligumu_id'],
        $r['jauni']['ligumu_id'],
        $r['atjaunoti']['ligumu_id']
      );

      $total['kopa']['skaits'] += $r['kopa']['skaits'];
      $total['kopa']['apgr'] += $r['kopa']['apgr'];

      $total['kopa']['ligumu_id'] = array_merge(
        $total['kopa']['ligumu_id'],
        $r['kopa']['ligumu_id']
      );


      // Neapstrādāti

      $neaiztikti_skaits = $r['atdoti']['skaits'] - $r['apstradati']['skaits'];
      $neaiztikti_apgr = $r['atdoti']['apgr'] - $r['apstradati']['apgr'];

      $neaiztikti_ligumu_id = array();

      foreach($r['atdoti']['ligumu_id'] as $v) {
        if (!in_array($v, $r['apstradati']['ligumu_id'])) {
          $neaiztikti_ligumu_id[] = $v;
        }
      }

      if ($y . '-' . $period >= $cur_period) {

        // Ceturksnis vēl nav beidzies

        $r['neaiztikti']['skaits'] = $neaiztikti_skaits;
        $r['neaiztikti']['apgr'] = $neaiztikti_apgr;

        $r['neaiztikti']['ligumu_id'] = $neaiztikti_ligumu_id;

        $total['neaiztikti']['skaits'] += $r['neaiztikti']['skaits'];
        $total['neaiztikti']['apgr'] += $r['neaiztikti']['apgr'];

        $total['neaiztikti']['ligumu_id'] = array_merge(
          $total['neaiztikti']['ligumu_id'],
          $r['neaiztikti']['ligumu_id']
        );

      } else {

        // Ceturksnis ir beidzies

        $r['neaiztikti']['skaits'] = 0;
        $r['neaiztikti']['apgr'] = 0;

        // Pieskaitam neaiztiktos pie zaudētajiem

        $r['zaudeti']['skaits'] += $neaiztikti_skaits;
        $r['zaudeti']['apgr'] += $neaiztikti_apgr;

        $r['zaudeti']['ligumu_id'] = array_merge(
          $r['zaudeti']['ligumu_id'],
          $neaiztikti_ligumu_id
        );

        $total['zaudeti']['skaits'] += $neaiztikti_skaits;
        $total['zaudeti']['apgr'] += $neaiztikti_apgr;

        $total['zaudeti']['ligumu_id'] = array_merge(
          $total['zaudeti']['ligumu_id'],
          $r['zaudeti']['ligumu_id']
        );

        // Pieskaitam neaiztiktos pie apstrādātajiem

        $r['apstradati']['skaits'] += $neaiztikti_skaits;
        $r['apstradati']['apgr'] += $neaiztikti_apgr;

        $r['apstradati']['ligumu_id'] = array_merge(
          $r['apstradati']['ligumu_id'],
          $neaiztikti_ligumu_id
        );

        $total['apstradati']['skaits'] += $neaiztikti_skaits;
        $total['apstradati']['apgr'] += $neaiztikti_apgr;

        $total['apstradati']['ligumu_id'] = array_merge(
          $total['apstradati']['ligumu_id'],
          $r['apstradati']['ligumu_id']
        );

      }


      // Tālāk aprēķini

      // Atdoti

      if (!empty($r['atdoti']['skaits'])) $r['atdoti']['vid_summa'] = $r['atdoti']['apgr'] /  $r['atdoti']['skaits'];

      // Apstrādāti

      if (!empty($r['apstradati']['skaits'])) $r['apstradati']['vid_summa'] = $r['apstradati']['apgr'] /  $r['apstradati']['skaits'];
      if (!empty($r['atdoti']['skaits'])) $r['apstradati']['proc_atdot_sk'] =  ($r['apstradati']['skaits'] / $r['atdoti']['skaits']) * 100;
      if (!empty($r['atdoti']['apgr'])) $r['apstradati']['proc_atdot_apgr'] =  ($r['apstradati']['apgr'] / $r['atdoti']['apgr']) * 100;

      // Atkārtoti

      if (!empty($r['atkartoti']['skaits'])) $r['atkartoti']['vid_summa'] = $r['atkartoti']['apgr'] /  $r['atkartoti']['skaits'];
      if (!empty($r['atdoti']['skaits'])) $r['atkartoti']['proc_atdot_sk'] =  ($r['atkartoti']['skaits'] / $r['atdoti']['skaits']) * 100;
      if (!empty($r['atdoti']['apgr'])) $r['atkartoti']['proc_atdot_apgr'] =  ($r['atkartoti']['apgr'] / $r['atdoti']['apgr']) * 100;


      // Zaudēti

      if (!empty($r['zaudeti']['skaits'])) $r['zaudeti']['vid_summa'] = $r['zaudeti']['apgr'] /  $r['zaudeti']['skaits'];
      if (!empty($r['atdoti']['skaits'])) $r['zaudeti']['proc_atdot_sk'] =  ($r['zaudeti']['skaits'] / $r['atdoti']['skaits']) * 100;
      if (!empty($r['atdoti']['apgr'])) $r['zaudeti']['proc_atdot_apgr'] =  ($r['zaudeti']['apgr'] / $r['atdoti']['apgr']) * 100;

      // Jauni

      if (!empty($r['jauni']['skaits'])) $r['jauni']['vid_summa'] = $r['jauni']['apgr'] /  $r['jauni']['skaits'];
      if (!empty($r['kopa']['skaits'])) $r['jauni']['proc_kopa_sk'] =  ($r['jauni']['skaits'] / $r['kopa']['skaits']) * 100;
      if (!empty($r['kopa']['apgr'])) $r['jauni']['proc_kopa_apgr'] =  ($r['jauni']['apgr'] / $r['kopa']['apgr']) * 100;

      // Atjaunoti

      if (!empty($r['atjaunoti']['skaits'])) $r['atjaunoti']['vid_summa'] = $r['atjaunoti']['apgr'] /  $r['atjaunoti']['skaits'];
      if (!empty($r['kopa']['skaits'])) $r['atjaunoti']['proc_kopa_sk'] =  ($r['atjaunoti']['skaits'] / $r['kopa']['skaits']) * 100;
      if (!empty($r['kopa']['apgr'])) $r['atjaunoti']['proc_kopa_apgr'] =  ($r['atjaunoti']['apgr'] / $r['kopa']['apgr']) * 100;

      // Kopā

      if (!empty($r['kopa']['skaits'])) $r['kopa']['vid_summa'] = $r['kopa']['apgr'] /  $r['kopa']['skaits'];
      if (!empty($r['atdoti']['skaits'])) $r['kopa']['proc_atdot_sk'] =  ($r['kopa']['skaits'] / $r['atdoti']['skaits']) * 100;
      if (!empty($r['atdoti']['apgr'])) $r['kopa']['proc_atdot_apgr'] =  ($r['kopa']['apgr'] / $r['atdoti']['apgr']) * 100;

      // Neapstrādāti

      if (!empty($r['neaiztikti']['skaits'])) $r['neaiztikti']['vid_summa'] = $r['neaiztikti']['apgr'] /  $r['neaiztikti']['skaits'];
      if (!empty($r['atdoti']['skaits'])) $r['neaiztikti']['proc_atdot_sk'] =  ($r['neaiztikti']['skaits'] / $r['atdoti']['skaits']) * 100;
      if (!empty($r['atdoti']['apgr'])) $r['neaiztikti']['proc_atdot_apgr'] =  ($r['neaiztikti']['apgr'] / $r['atdoti']['apgr']) * 100;

    } //? end: iekļaujas intervālā (show_till)

    $has_stats = false;

    if (!empty($r)) {
      foreach($r as $type => $data) {
        if (!empty($data['ligumu_id'])) {
          $has_stats = true;
        }
      }
    }

    if ($has_stats || $show_empty) {
      $result[$y . '-' . $period] = $r;
    }

    if (($period_type == 'quarter' && $period < 4) || ($period_type == 'month' && $period < 12)) {
      $period ++;
    } else {
      $y ++;
      $period = 1;
    }

    if ($period_type == 'month')
      $period = str_pad($period, 2, '0', STR_PAD_LEFT);

  } //?  end: ciklēšanās pa periodiem
 
  // Tālāk aprēķini
  if ($period_type == 'quarter') {

    // Atdoti

    if (!empty($total['atdoti']['skaits'])) $total['atdoti']['vid_summa'] = $total['atdoti']['apgr'] /  $total['atdoti']['skaits'];

    // Apstrādāti

    if (!empty($total['apstradati']['skaits'])) $total['apstradati']['vid_summa'] = $total['apstradati']['apgr'] /  $total['apstradati']['skaits'];
    if (!empty($total['atdoti']['skaits'])) $total['apstradati']['proc_atdot_sk'] =  ($total['apstradati']['skaits'] / $total['atdoti']['skaits']) * 100;
    if (!empty($total['atdoti']['apgr'])) $total['apstradati']['proc_atdot_apgr'] =  ($total['apstradati']['apgr'] / $total['atdoti']['apgr']) * 100;

    // Atkārtoti

    if (!empty($total['atkartoti']['skaits'])) $total['atkartoti']['vid_summa'] = $total['atkartoti']['apgr'] /  $total['atkartoti']['skaits'];
    if (!empty($total['atdoti']['skaits'])) $total['atkartoti']['proc_atdot_sk'] =  ($total['atkartoti']['skaits'] / $total['atdoti']['skaits']) * 100;
    if (!empty($total['atdoti']['apgr'])) $total['atkartoti']['proc_atdot_apgr'] =  ($total['atkartoti']['apgr'] / $total['atdoti']['apgr']) * 100;

    // Zaudēti

    if (!empty($total['zaudeti']['skaits'])) $total['zaudeti']['vid_summa'] = $total['zaudeti']['apgr'] /  $total['zaudeti']['skaits'];
    if (!empty($total['atdoti']['skaits'])) $total['zaudeti']['proc_atdot_sk'] =  ($total['zaudeti']['skaits'] / $total['atdoti']['skaits']) * 100;
    if (!empty($total['atdoti']['apgr'])) $total['zaudeti']['proc_atdot_apgr'] =  ($total['zaudeti']['apgr'] / $total['atdoti']['apgr']) * 100;

    // Jauni

    if (!empty($total['jauni']['skaits'])) $total['jauni']['vid_summa'] = $total['jauni']['apgr'] /  $total['jauni']['skaits'];
    if (!empty($total['kopa']['skaits'])) $total['jauni']['proc_kopa_sk'] =  ($total['jauni']['skaits'] / $total['kopa']['skaits']) * 100;
    if (!empty($total['kopa']['apgr'])) $total['jauni']['proc_kopa_apgr'] =  ($total['jauni']['apgr'] / $total['kopa']['apgr']) * 100;

    // Atjaunoti

    if (!empty($total['atjaunoti']['skaits'])) $total['atjaunoti']['vid_summa'] = $total['atjaunoti']['apgr'] /  $total['atjaunoti']['skaits'];
    if (!empty($total['kopa']['skaits'])) $total['atjaunoti']['proc_kopa_sk'] =  ($total['atjaunoti']['skaits'] / $total['kopa']['skaits']) * 100;
    if (!empty($total['kopa']['apgr'])) $total['atjaunoti']['proc_kopa_apgr'] =  ($total['atjaunoti']['apgr'] / $total['kopa']['apgr']) * 100;

    // Kopā

    if (!empty($total['kopa']['skaits'])) $total['kopa']['vid_summa'] = $total['kopa']['apgr'] /  $total['kopa']['skaits'];
    if (!empty($total['atdoti']['skaits'])) $total['kopa']['proc_atdot_sk'] =  ($total['kopa']['skaits'] / $total['atdoti']['skaits']) * 100;
    if (!empty($total['atdoti']['apgr'])) $total['kopa']['proc_atdot_apgr'] =  ($total['kopa']['apgr'] / $total['atdoti']['apgr']) * 100;

    // Neaiztikti

    if (!empty($total['neaiztikti']['skaits'])) $total['neaiztikti']['vid_summa'] = $total['neaiztikti']['apgr'] /  $total['neaiztikti']['skaits'];
    if (!empty($total['atdoti']['skaits'])) $total['neaiztikti']['proc_atdot_sk'] =  ($total['neaiztikti']['skaits'] / $total['atdoti']['skaits']) * 100;
    if (!empty($total['atdoti']['apgr'])) $total['neaiztikti']['proc_atdot_apgr'] =  ($total['neaiztikti']['apgr'] / $total['atdoti']['apgr']) * 100;

    $result['total'] = $total;

  }
	
  return $result;

}

?>