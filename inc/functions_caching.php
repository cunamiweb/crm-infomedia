<?php

function cache_get($key, $default = false)
{
  if(CACHING_ENABLED) {

    global $memcache, $cacheAvailable;

    if(!$cacheAvailable) {

      return false;

    } else {

      $value = $memcache->get(MEMCACHE_KEY_PREFIX . $key);

      if($value === false) {

        return $default;

      } else {

        return $value;
      }
    }

  } else {
    return false;
  }
}

function cache_set($key, $value, $expiration = false)
{
  if(CACHING_ENABLED) {

    global $memcache, $cacheAvailable;

    if(!$cacheAvailable) {

      return false;

    } else {

      if($expiration === false) {

        $expiration = MEMCACHE_DEFAULT_EXP;
      }

      if(class_exists('Memcache')) {
        return $memcache->set(MEMCACHE_KEY_PREFIX . $key, $value, false, $expiration);
      } else {
        return $memcache->set(MEMCACHE_KEY_PREFIX . $key, $value, $expiration);
      }
    }
  }
}

function cache_flush()
{
  if(CACHING_ENABLED) {

    global $memcache, $cacheAvailable;

    $memcache->flush();
  }
}
?>