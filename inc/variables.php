<?php
// visadi mainigie
$_vars = array();

$_vars['sanemsanas_forma'] = array(
  1=>"Orģināls",
  2=>"Fakss",
  3=>"E-pasts",
);

$_vars['infomediastatuss'] = array(
  1=>"Nav ievadīts",
  3=>"Daļēji ievadīts",  
  2=>"Ievadīts",
  4=>"Googlē",
);

$_vars['googlestatuss'] = array(
  1=>"Nav ievadīts",
  2=>"Ievadīts",
  3=>"Pieslēgts",
  4=>"Atslēgts",
);

$_vars['googlestatuss2'] = array(
  1=>"Nav ievadīts",
  3=>"Pieslēgts",
  4=>"Atslēgts",
);

$_vars['rekina_piegade'] = array(
  1=>"Uz e-pastu",
  2=>"Pa&nbsp;pastu",
);

$_vars['pakalpojumi'] = array();

$query = db_query("
  SELECT *
  FROM `".DB_PREF."pakalpojumi_veidi`
  ORDER BY
    `parent_id` ASC,
    `seciba` ASC,    
    `id` ASC
");

$pakalpojumi = db_get_assoc_all($query);

foreach($pakalpojumi as $row){

  if (empty($row['parent_id'])) {

    $row['nosaukums_select'] = $row['nosaukums'];

    $_vars['pakalpojumi'][$row['id']] = $row;

    $pakalpojumi2 = $pakalpojumi;

    foreach($pakalpojumi2 as $row2) {

      if (!empty($row2['parent_id']) && $row2['parent_id'] == $row['id']) {

        $row2['nosaukums_select'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' . $row2['nosaukums'];

        $_vars['pakalpojumi'][$row2['id']] = $row2;

      }

    }

  }

}

$_vars['mpt_filter'] = array();
foreach($pakalpojumi as $row){

  if (empty($row['parent_id'])) {

    // $row['nosaukums'];

    //$_vars['pakalpojumi'][$row['id']] = $row;

    $pakalpojumi2 = $pakalpojumi;

    $children = array();

    foreach($pakalpojumi2 as $row2) {

      if (!empty($row2['parent_id']) && $row2['parent_id'] == $row['id']) {

        $children[$row2['id']] = $row2['nosaukums'];

      }

    }

    $_vars['mpt_filter'][$row['nosaukums']] = $children;
  }

}


$_vars['rekina_statuss'] = array(
  1=>"Nav",
  2=>"Izsūtīts",
  3=>"Apstiprināts",
);

$_vars['apmaksas_statuss'] = array(
  4=>"Nav apm. termiņš",
  2=>"Apmaksāts",
  5=>"Daļēji apmaksāts",
  3=>"Parāds!",
  7=>"Nodots piedziņai",
  6=>"Anulēts",
  1=>"Nav sācies",
);

$_vars['kopapmaksas_statuss'] = array(
  1=>"Nav apm. termiņš",
  2=>"Apmaksāts",
  3=>"Daļēji apmaksāts",
  4=>"Parāds",
  6=>"Nodots piedziņai",
  5=>"Anulēts",
);

$_vars['liguma_statuss'] = array(
  1=>"Nav sācies",
  //2=>"Aktīvs, bet ir parāds"
  3=>"Aktīvs",
  4=>"Apturēts",
  5=>"Pārtraukts",
  6=>"Beidzies",
  7=>"Anulēts",
);

$_vars['liguma_beigu_statuss'] = array(
  'termins' => 'termiņš',
  'parslegts' => 'pārslēgts',
  'atteikums' => 'atteikums',
);

$_vars['sys_pardeveji'] = array();

$query = db_query("SELECT * FROM `".DB_PREF."pardeveji` ORDER BY `tips` DESC, `vards` ASC");

while($row = db_get_assoc($query)){
  $_vars['sys_pardeveji'][$row['id']] = $row['vards'];
}

$_vars['sys_pardeveji_sales_only'] = array();

$query = db_query("SELECT * FROM `".DB_PREF."pardeveji` WHERE tips = 'sales' ORDER BY `vards` ASC");

while($row = db_get_assoc($query)){
  $_vars['sys_pardeveji_sales_only'][$row['id']] = $row['vards'];
}            

$_vars['sys_pardeveji_admin_only'] = array();

$query = db_query("SELECT * FROM `".DB_PREF."pardeveji` WHERE tips = 'admin' ORDER BY `vards` ASC");

while($row = db_get_assoc($query)){
  $_vars['sys_pardeveji_admin_only'][$row['id']] = $row['vards'];
}

$_vars['atgadinajumu_temas'] = array(
  1=>"Klienti",
  2=>"Klientu norēķini",
  3=>"Darbinieki",
  4=>"Privāts",
  5=>"Darbinieku algas"
);

$_vars['pard_rekina_statuss'] = array(
  1=>"Neapmaksāts",
  2=>"Apmaksāts"
);

$_vars['menesi'] = array(
  1=>"Janvāris",
  2=>"Februāris",
  3=>"Marts",
  4=>"Aprīlis",
  5=>"Maijs",
  6=>"Jūnijs",
  7=>"Jūlijs",
  8=>"Augusts",
  9=>"Septembris",
  10=>"Oktobris",
  11=>"Novembris",
  12=>"Decembris"
);

/**
 * Komentāru objektu tipu sasaiste ar atgādinājumu veidiem
 * atgadinajuma veida id => komentāru objekta tips
 */
$_vars['atg_obj_tipi'] = array(
  1 => 'lig_rekini',
  8 => 'lig_rekini',
  9 => 'lig_rekini',
  10 => 'lig_rekini',
  12 => 'lig_pakalpojums',
  13 => 'lig_pakalpojums',
  14 => 'lig_klients',
  15 => 'lig_pakalpojums',
  16 => 'lig_pakalpojums',
);

/**
 * Atgādinājumu objektu id - kāda objekta id ņemt katram atgādinājumu veidam.
 * Vajadzīgs komentāru piesaistei.
 */
$_vars['atg_obj_id'] = array(
  1 => 'liguma_id',
  8 => 'liguma_id',
  9 => 'liguma_id',
  10 => 'liguma_id',
  12 => 'liguma_id',
  13 => 'liguma_id',
  14 => 'liguma_id',
  15 => 'liguma_id',
  16 => 'liguma_id',
);

/**
 * Romiešu simboli ceturkšņu numuriem
 */
$_vars['quarters_roman'] = array(
  1 => 'I',
  2 => 'II',
  3 => 'III',
  4 => 'IV',
);

$_vars['mekletaji'] = array(
  'google' => 'Google',
  'yandex' => 'Yandex',
);

$_vars['mpk_sites'] = array(
  'google',
  'yandex'
);

$_vars['mpk_google_displays'] = array(
  'youtube',
  'mobile',
  'retargeting',
);

$_vars['google_pakalpojumi'] = array(
  'search' => 'Search',
  'display' => 'Display',
  'category' => 'Display tēmas',
  'analytics' => 'Analytics',
  'conversions' => 'Konversijas',
);

$_vars['yandex_pakalpojumi'] = array(
  'search' => 'Direct',
  'category' => 'Konteksts',
  'analytics' => 'Metrika',
);

$_vars['mpk_filter'] = array(
  'Google' => array(
    'search' => 'Search',
    'display' => 'Display',
    'conversions' => 'Konversijas',
    'analytics' => 'Analytics',
  ),
  'Yandex' => array(
    'direct' => 'Direct',
    'kontext' => 'Konteksts',
    'metrika' => 'Metrika',
  ),
  'Cits' => 'other',
);

$_vars['bank_account_numbers'] = array(
  'mpt' => 'LV38HABA0551020711161',
  'mpk' => 'LV61HABA0551034668077'
);
$_vars['bank_account_numbers_leads'] = array(
  'mpt' => 'LV35HABA0551038848275',
  'mpk' => 'LV12HABA0551038848301'
);

$_vars['avoti'] = array(
  '1' => 'Konsultants',
  '2' => 'Web',
  '3' => 'Zvanītājs'
);

$_vars['konta_admin'] = array(
  'juris' => 'Juris',
  'marianna' => 'Marianna',
  'jekaterina' => 'Jekaterina',
);

?>