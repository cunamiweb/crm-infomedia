<?php
define("DB_PREF", "new_");
define("DB_HOST", "localhost");
define("DB_NAME", "crm");
define("DB_USER", "crm");
define("DB_PASS", "qf5pEc17U3zk1xz");


setlocale(LC_NUMERIC, 'en_US'); //corect numeric values on other pc locale
error_reporting(E_ALL & ~E_NOTICE & ~E_WARNING & ~E_DEPRECATED);

//DEVS

set_time_limit(360);
ini_set('memory_limit', '384M');


// only used when STOP_ON_ERRORS = false
define("ERROR_MAIL", 'ap-junk@datatekss.lv');

define("CFG_ROOT", __FILE__);
define("DIR_ROOT", dirname(__DIR__.'..'));

// Adwords
define('ADWORDS_VERSION', 'v201809');

define('ADWORDS_CURRENCY', 'USD'); // Currency the adwords account operates with
define('ADOWRDS_USER_AGENT', 'Datateks');
# define('ADWORDS_API_DEFAULT_SERVER', 'https://adwords.google.com'); // this is defined as endpoint in inc/adwords_default.ini 
define('ADWORDS_REFRESH_RATE', 1); // Days
define('ADWORDS_DEFAULT_CLIENT_CUSTOMER_ID', '349-826-6465');
#define('ADWORDS_DEFAULT_CLIENT_CUSTOMER_ID', '671-287-1572');

# old
define('ADWORDS_CLIENT_ID', '64309583925-anak6vo60p0cj42knoe3coupjksqkl4s.apps.googleusercontent.com');
define('ADWORDS_CLIENT_SECRET', 'gjPfAWFAJhLG8Yb0eo82OMUl');

# new
#define('ADWORDS_CLIENT_ID', '653361751478-mhemiuepoc5o598imqdc6ef21lrqsvu6.apps.googleusercontent.com');
#define('ADWORDS_CLIENT_SECRET', 'ZTZLy-X1I-1kKrtCNrqN2Jr-');


/*
define('ADWORDS_REFRESH_TOKEN', '1/VuHZRt3rgvTkAhO67X974L4mN06iQ0ZVIeapFtCf6Rk');
define('ADWORDS_DEVELOPER_TOKEN', '4dDwTjflo3JH3paB0GwvOg');*/

define('ADWORDS_C_CLIENT_ID', '');
define('ADWORDS_C_CLIENT_SECRET', '');
define('ADWORDS_C_REFRESH_TOKEN', '');
define('ADWORDS_C_DEVELOPER_TOKEN', '');
//define('ADWORDS_API_DEVELOPER_TOKEN', 'N3AOSMEu47LSrsav5Inlbg');
/*define('ADWORDS_API_EMAIL', 'davis@datateks.lv');
define('ADWORDS_API_PASSWORD', 'dDat@123'); */
/*define('ADWORDS_API_EMAIL', 'info@infomedia.lv');
define('ADWORDS_API_PASSWORD', 'imedia01');
define('ADWORDS_API_CLIENT_ID', '');    */
//define('ADWORDS_API_DEFAULT_SERVER', 'https://adwords-sandbox.google.com');

// this flag shoul be true ONLY on development site
// when error happens, detailed info is shown
// will stop execution even on notices
// when true, error is not sent to ERROR_MAIL
define("STOP_ON_ERRORS", false);

define('VAT', 21);

define('REK_FROM_NAME', 'SIA Infomedia grāmatvedība');
define('REK_FROM_EMAIL', 'gramatvediba@infomedia.lv');

//  Do not send emails on certain scripts (for example, reminders about unreceived contract original)
// Do not depend on this setting as not all emailing scripts take this in account
define("TEST_EMAILS", false);

define("DEBUG_QUERIES", false);
define("ENABLE_RAW_LOG", false);

define("SUPERUSER_FULL_ACCESS", true);

define("KORP_VADITAJA_ID", 55);

define("PARD_VADITAJA_ID", 36);
define("PARD_VADITAJA_BONUSI_START_YEAR", 2013);

define("REKV_NOSAUK", "Infomedia Leads, SIA"); //changed by agris
define("REKV_JUR_ADR", "Brīvības iela 155, k-7, Rīga, LV-1012");
define("REKV_FIZ_ADR", "Gaujas iela 11, Rīga, LV-1026");
define("REKV_BANKA", "Swedbank, AS");
define("REKV_SWIFT", "HABALV22");
define("REKV_KONTS", "LV38HABA0551020711161");
define("REKV_KONTS2", "LV35HABA0551038848275");
define("REKV_REG_NR", "40103166641");
define("REKV_PVN_NR", "LV40103166641");

define("BONUSS_START_YEAR", 2011);

define('PREMIJAS_NEW_START_YEAR', 2012);
define('PREMIJAS_NEW_START_QUEARTER', 3);

define('PREMIJAS_NEW2_START_YEAR', 2013); // Sāk darboties trešā prēmiju sistēma. Produkcijā: 2013
define('PREMIJAS_NEW2_START_QUEARTER', 1); // Produkcijā: 1

define("DARB_REK_PAK_NOSAUK", "Atlīdzība par pakalpojumiem");
define("DARB_REK_PREM_NOSAUK", "Papildus atlīdzība par apgrozījuma apjomu");
define("DARB_REK_KOREKC_NOSAUK", "Tekošā korekcija par iepriekšējiem periodiem");

define("LIG_REK_PAK_NOSAUK", "Reklāmas pakalpojumi");
define("LIG_REK_ATKAPS_PAMATOJUMS", "Līgumsods par līgumu Nr. %s (līguma punkts 11.5.)");

define("LIG_REK_INFO1", "Būsim pateicīgi par savlaicīgu rēķina apmaksu.");
define("LIG_REK_INFO2", "Ja rēķins netiks apmaksāts līdz norādītajam datumam, reklāmas darbība tiks apturēta saskaņā ar līguma punktu Nr. 8.7.");
define("LIG_REK_INFO3", "");
define("LIG_REK_INFO3_MPK", "Informācijai par rēķiniem un līgumiem lūdzam zvanīt 20370996.");

define("LIG_REK_VALDES_LOCEKLIS", "V. Juzāns");
define("LIG_REK_TALR", "67290200");
define("LIG_REK_FAX", "67290201");
define("LIG_REK_EPASTS", "info@infomedia.lv");
define("LIG_REK_WWW", "www.infomedia.lv");

define("AVANSA_REK_SUBJ", "Avansa rēķins par reklāmas pakalpojumiem");
define("AVANSA_REK_BODY", "Labdien!

Nosūtu Jums avansa rēķinu par mūsu pakalpojumiem.

Lūdzu, apstipriniet avansa rēķina saņemšanu. Paldies!

--
Jauku dienu vēlot,

<img src=\"{logo}\" alt=\"\" />
<div style=\"color: #0070C0; padding-left: 40px; font-size: 14px;\">
<strong>Infomedia Leads, SIA</strong>
Gaujas iela 11, Rīga, LV-1026
Tālr.: +371 67290200
Fakss: +371 67290201
Mob.t.: +371 26552875
gramatvediba@infomedia.lv
www.infomedia.lv</div>");

define("GALA_REK_SUBJ", "Gala rēķins par reklāmas pakalpojumiem");
define("GALA_REK_BODY", "Labdien!

Nosūtu Jums gala rēķinu par mūsu pakalpojumiem.

<strong><u>Samaksa jau ir veikta. Iepriekšējais bija avansa rēķins, kurš netiek iekļauts grāmatvedībā.
Šis ir jāiegrāmato.</u></strong>

Lūdzu, apstipriniet rēķina saņemšanu. Paldies!

--
Jauku dienu vēlot,

<img src=\"{logo}\" alt=\"\" />
<div style=\"color: #0070C0; padding-left: 40px; font-size: 14px;\">
<strong>Infomedia Leads, SIA</strong>
Gaujas iela 11, Rīga, LV-1026
Tālr.: +371 67290200
Fakss: +371 67290201
Mob.t.: +371 26552875
gramatvediba@infomedia.lv
www.infomedia.lv</div>");

define('ORIG_ATG_FROM_NAME', 'Gatis Jānis Dišlers');
define('ORIG_ATG_FROM_EMAIL', 'gatis@infomedia.lv');
define("ORIG_ATG_SUBJ", "Atgādinājums par nesaņemtu līguma oriģinālu");
define("ORIG_ATG_BODY", "Labdien!

Atgādinām, ka neesam saņēmuši no Jums mūsu savstarpējā līguma Nr. %s no %s oriģinālu!

Lūdzu, pēc iespējas ātrāk nosūtīt to pa pastu uz adresi Gaujas iela 11, Rīga, LV-1026!

--
<img src=\"{logo}\" alt=\"\" />
<div style=\"color: #0070C0; padding-left: 40px; font-size: 14px;\">
<strong>Gatis Jānis Dišlers</strong>
klientu administrators
SIA Infomedia
Gaujas iela 11, Rīga, LV-1026
Tālr.: +371 67796615
Fakss: +371 67290201
Mob.t.: +371 26552875
gatis@infomedia.lv
www.infomedia.lv</div>");

// Piedavajuma pdf
define("PIED_PDF_COMPANY", "Infomedia Leads SIA");
define("PIED_PDF_CONTACT", "Vārds Uzvārds");
define("PIED_PDF_PHONE", "20000000");
define("PIED_PDF_EMAIL", "vards@infomedia.lv");
define("PIED_PDF_ADDRESS", "Gaujas iela 11, Rīga, LV-1026");

//  Caching
define('CACHING_ENABLED', false);
define('MEMCACHE_HOST', '127.0.0.1');
define('MEMCACHE_PORT', '11211');
define('MEMCACHE_DEFAULT_EXP', 60*60*24); //24h
define('MEMCACHE_KEY_PREFIX', 'infomedia_');
?>
