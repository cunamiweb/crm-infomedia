<?php

// Load models
$models_path = (isset($_SERVER['PWD'])?$_SERVER['PWD']:$_SERVER['DOCUMENT_ROOT']).'/models';

require($models_path . '/base.class.php');
require($models_path . '/piedavajums.class.php');
require($models_path . '/pardevejs.class.php');
require($models_path . '/valsts.class.php');
require($models_path . '/atslegvards.class.php');
require($models_path . '/nozare.class.php');
require($models_path . '/pielikumsInfomedia.class.php');
require($models_path . '/pielikumsTiessaite.class.php');
require($models_path . '/pielikumsYandex.class.php');
require($models_path . '/pielikumsCits.class.php');
require($models_path . '/standartaPiedavajums.class.php');
require($models_path . '/adwords.class.php');
require($models_path . '/uzstadijums.class.php');
require($models_path . '/ligums.class.php');
require($models_path . '/pakalpojums.class.php');
require($models_path . '/mekletajs.class.php');
require($models_path . '/komentars.class.php');
require($models_path . '/manualCenaLog.class.php');
require($models_path . '/forumCategory.class.php');
require($models_path . '/forumTopic.class.php');
require($models_path . '/tiesiba.class.php');
require($models_path . '/rawLog.class.php');
require($models_path . '/rekins.class.php');
require($models_path . '/budzetaParskaitijums.class.php');
require($models_path . '/mpkAnalytics.class.php');
require($models_path . '/mpkCategory.class.php');
require($models_path . '/mpkConversion.class.php');
require($models_path . '/mpkDisplay.class.php');
require($models_path . '/mpkDisplayMisc.class.php');
require($models_path . '/mpkKeyword.class.php');
require($models_path . '/mpkOther.class.php');
require($models_path . '/pardevejaRekins.class.php');
?>
