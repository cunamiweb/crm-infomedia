<?php
function db_connect($host, $user, $pass) {

  return mysql_connect($host, $user, $pass);

}

function db_select($name) {

  global $db;

  return mysql_select_db($name, $db);

}

function esc($obj) {

  global $db;

  if (is_array($obj)) {

    foreach($obj as $key => $value) {
      $obj[$key] = esc($value);
    }

  } elseif (is_string($obj)) {

    if (get_magic_quotes_gpc()) {
      $obj = stripslashes($obj);
    }

    $obj = mysql_real_escape_string($obj, $db);

  }

  return $obj;

}


function db_query($sql) {

  global $db;

  $time_start = microtime(true);
  $result = mysql_query($sql, $db);
  $time_end = microtime(true);

  $time = microtime_diff($time_start, $time_end);

  if (DEBUG_QUERIES || ENABLE_RAW_LOG) {

    $GLOBALS['executed_queries'][] = array(
      'sql' => $sql,
      'time' => $time
    );

    $GLOBALS['query_total_time'] += $time;

  }

  if (!$result) {
    trigger_error(mysql_error($db), E_USER_WARNING);
  }

  return $result;

}

function db_get_assoc($result) {

  return mysql_fetch_assoc($result);

}

function db_get_assoc_all($result) {

  $rows = array();

  while($row = db_get_assoc($result)) {
    $rows[] = $row;
  }

  return $rows;

}

function db_get_val($result) {

  $data = mysql_fetch_row($result);

  if (isset($data[0])) {
    return $data[0];
  }

}

function db_last_id() {

  global $db;

  return mysql_insert_id($db);

}

function db_num_rows($result) {

  return mysql_num_rows($result);

}

function db_found_rows() {

  return db_get_val(db_query("SELECT FOUND_ROWS()"));

}
?>