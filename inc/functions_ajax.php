<?php
function ajax_check_client() {

  $str = !empty($_GET['nosauk']) ? trim($_GET['nosauk']) : '';

  $str = trim(str_ireplace(array('sia', '%'), '', $str));

  if (mb_strlen($str) < 3) {
    echo '<p style="margin: 15px 0 0 0; text-align: center;"><strong>Nosaukumā jānorāda vismaz 3 simboli!</strong></p>';
    return;
  }

  $query = db_query("
    SELECT *
    FROM `".DB_PREF."ligumi`
    WHERE
      `nosaukums` LIKE '%".esc($str)."%'
      " . (mb_strlen($str) >= 8 ? "OR `regnr` LIKE '%".esc($str)."%'" : '') . "
  ");

  echo '
    <table id="ligums_check_table">
      <tr>
        <th>Klients</th>
        <th>Līgums no</th>
        <th>Līgums līdz</th>
      </tr>
  ';

  if (db_num_rows($query) > 0) {

    while($row = db_get_assoc($query)) {

      echo '
        <tr>
          <td>'.$row['nosaukums'].'</td>
          <td>'.(!empty($row['reklama_no']) ? date('d.m.Y', strtotime($row['reklama_no'])) : '').'</td>
          <td>'.(!empty($row['reklama_lidz']) ? date('d.m.Y', strtotime($row['reklama_lidz'])) : '').'</td>
        </tr>
      ';

    }

  } else {

    echo '
      <tr>
        <td colspan="3">Netika atrasts neviens klients</td>
      </tr>
    ';

  }

  echo '</table>';


}

function ajax_send_rek_to_email() {

  if (!is_admin()) {
    return;
  }

  if (empty($_GET['rek_id'])) {
    return;
  }

  $rek_id = (int) $_GET['rek_id'];

  $tipi = array('fails_avansa_ep', 'fails_gala_ep');

  if (empty($_GET['tips']) || !in_array($_GET['tips'], $tipi)) {
    return;
  }

  $tips = $_GET['tips'];

  $rek_data = db_get_assoc(db_query("
    SELECT *
    FROM `".DB_PREF."rekini`
    WHERE id = ".$rek_id."
  "));

  if (empty($rek_data[$tips])) {
    $error = 'Rēķinam nav izveidots PDF fails!';
  } else {

    $faila_data = db_get_assoc(db_query("
      SELECT *
      FROM `".DB_PREF."faili`
      WHERE id = ".$rek_data[$tips]."
    "));

    if (empty($faila_data) || !is_file('faili/'.$faila_data['id'])) {
      $error = 'Rēķina fails netika atrasts!';
    } else {

      $piegade = db_get_assoc(db_query("
        SELECT *
        FROM `".DB_PREF."ligumi_rekinu_piegades`
        WHERE
          liguma_id = ".$rek_data['liguma_id']." AND
          piegades_veids = 1
        LIMIT 1
      "));

      if (empty($piegade['adrese'])) {
        $error = 'Klientam nav norādīts piegādes e-pasts!';
      } else {

        $name = 'faili/'.$faila_data['id'];

        $mailer = Swift_Mailer::newInstance(Swift_MailTransport::newInstance());
        $mail = Swift_Message::newInstance();

        $mail->setFrom(array(REK_FROM_EMAIL => REK_FROM_NAME));
        $mail->setTo($piegade['adrese']);

        $mail->attach(Swift_Attachment::fromPath($name)->setFilename($faila_data['fails']));

        $logo_cid = $mail->embed(Swift_Image::fromPath('css/logo.gif', 'logo.gif', 'image/gif'));

        if ($tips == 'fails_avansa_ep') {

          $subject = AVANSA_REK_SUBJ;
          $body_html = nl2br(AVANSA_REK_BODY);
          $body_plain = strip_tags(AVANSA_REK_BODY);

        } else {

          $subject = GALA_REK_SUBJ;
          $body_html = nl2br(GALA_REK_BODY);
          $body_plain = strip_tags(GALA_REK_BODY);

        }

        $body_html = str_replace('{logo}', $logo_cid, $body_html);

        $mail->setSubject($subject);
        $mail->setBody($body_html, 'text/html');
        $mail->addPart($body_plain, 'text/plain');

        if (!$mailer->send($mail)) {
          $error = 'Neizdevās nosūtīt rēķinu pa e-pastu! Tehniska kļūda';
        } else {

          //  Veiksmīgi nosūtīts

          //  Rēķinu atzīmējam kā izsūtīts ja nav jau atzīmēts kā apstiprināts
          if($rek_data['statuss'] != 3) {
            db_query('
            UPDATE '.DB_PREF.'rekini
            SET statuss = 2
            WHERE id = '.$rek_id.'
            ');
          }
        }

      }

    }

  }


  if (!empty($error)) {
    echo Zend_Json::encode(array('error' => $error));
  } else {
    echo Zend_Json::encode(array('success' => 1));
  }

}

function ajax_toggle_ipasais() {

  if (empty($_GET['liguma_id'])) {
    return;
  }

  $liguma_id = (int) $_GET['liguma_id'];

  $data = db_get_assoc(db_query("
    SELECT *
    FROM `".DB_PREF."ligumi_ipasie`
    WHERE
      pardeveja_id = ".$_SESSION['user']['id']." AND
      liguma_id = ".$liguma_id."
  "));

  if (!empty($data)) {

    db_query("
      DELETE FROM `".DB_PREF."ligumi_ipasie`
      WHERE
        pardeveja_id = ".$_SESSION['user']['id']." AND
        liguma_id = ".$liguma_id."
      LIMIT 1
    ");

  } else {

    db_query("
      INSERT INTO `".DB_PREF."ligumi_ipasie`
      SET
        pardeveja_id = ".$_SESSION['user']['id'].",
        liguma_id = ".$liguma_id."
    ");

  }

}

function ajax_ligumu_grupas() {

  if (empty($_GET['term']))
    die();

  $term = $_GET['term'];

  $data = db_get_assoc_all(db_query("
    SELECT
      g.nosaukums,
      r.grupas_regnr
    FROM `".DB_PREF."ligumi` g
    LEFT JOIN `".DB_PREF."ligumi_grupas` r ON (r.liguma_id = g.id)
    WHERE
      g.id = (
    		SELECT g2.id
    		FROM `".DB_PREF."ligumi_grupas` gr1
    		LEFT JOIN `".DB_PREF."ligumi_grupas` gr2 ON (gr2.grupas_regnr = gr1.grupas_regnr)
    		LEFT JOIN `".DB_PREF."ligumi` g2 ON (g2.id = gr2.liguma_id)
    		WHERE gr1.liguma_id = g.id
    		ORDER BY IF(g2.`sanemsanas_datums` IS NULL, 0, 1) ASC, g2.`sanemsanas_datums` DESC, g2.`ligumanr` DESC
    		LIMIT 1
      ) AND
      g.nosaukums LIKE '%".esc($term)."%'
  "));

  $result = array();

  foreach($data as $row) {
    $result[] = array(
      'id' => $row['grupas_regnr'],
      'label' => $row['nosaukums']
    );
  }

  echo Zend_Json::encode($result);
}

function ajax_statistika_months_rows() {

  if (empty($_GET['quarter']))
    die();

  $quarter = $_GET['quarter'];

  $pardeveja_id = null;

  if (!empty($_GET['pardeveja_id'])) {
    $pardeveja_id = (int) $_GET['pardeveja_id'];
  }

  if (!is_admin()) {

    if (empty($pardeveja_id) || $pardeveja_id != $_SESSION['user']['id']) {
      die();
    }

  }

  list($year, $q) = explode("-", $quarter);

  $from = $year;
  $to = $year;

  if ($q == 1) {
    $from .= '-1';
    $to .= '-3';
  } elseif ($q == 2) {
    $from .= '-4';
    $to .= '-6';
  } elseif ($q == 3) {
    $from .= '-7';
    $to .= '-9';
  } elseif ($q == 4) {
    $from .= '-10';
    $to .= '-12';
  }

  $data = get_statistika('month', $from, $to, $pardeveja_id, date('Y-m'));

  echo '<table>';
  foreach ($data as $i => $d) {
    print_statistika_rows($i, $d, array(
      'month_rows' => true,
      'allow_open_month' => empty($pardeveja_id) ? true : false
    ));
  }
  echo '</table>';

}

function ajax_statistika_month_pardeveji_rows() {

  global $_vars;

  if (!is_admin()) {
    die();
  }

  if (empty($_GET['month']))
    die();

  $month = $_GET['month'];

  $pardeveji = array();

  foreach($_vars['sys_pardeveji'] as $pardeveja_id => $pardevejs)
    $pardeveji[$pardeveja_id] = get_statistika('month', $month, $month, $pardeveja_id, date('Y-m'), false);

  echo '<table>';

  foreach($pardeveji as $pardeveja_id => $data) {

    foreach($data as $i => $d) {
      print_statistika_rows($i, $d, array(
        'pardeveja_rows' => true,
        'pardevejs' => $_vars['sys_pardeveji'][$pardeveja_id]
      ));
    }

  }

  echo '</table>';

}

function ajax_ligumi_parslegsanas_rindai() {

  if (empty($_GET['term']))
    die();

  $term = $_GET['term'];

  $data = db_get_assoc_all(db_query("
    SELECT
      g.id,
      g.nosaukums,
      g.ligumanr,
      g.ligumadat
    FROM `".DB_PREF."ligumi` g
    WHERE
      g.nosaukums LIKE '%".esc($term)."%' OR
      g.ligumanr LIKE '%".esc($term)."%'
  "));

  $result = array();

  foreach($data as $row) {
    $result[] = array(
      'id' => $row['id'],
      'label' => $row['nosaukums'] . ', ' . $row['ligumanr']
    );
  }

  echo Zend_Json::encode($result);
}

function ajax_atg_delete()
{
  if(isset($_GET['id'])) {
    $id = $_GET['id'];
    $user_id = $_SESSION['user']['id'];

    //  Pārbauda vai lietotājam pieder šis atgādinājums
    $result = db_query('
      SELECT *
      FROM ' . DB_PREF . 'atgadinajumi a
      WHERE
        id = ' . esc($id) . '
    ');

    $reminder = db_get_assoc($result);

    if($reminder['created_pardeveja_id'] == $user_id) {
      if(db_query('
        DELETE FROM ' . DB_PREF . 'atgadinajumi WHERE id = ' . esc($id) . '
      ')) {
        echo Zend_Json::encode(true);
      }
    }
  }
}

// Not used since 2012-08-16
function ajax_get_std_piedavajumi()
{
  if(isset($_GET['id'])) {
    $nozare = Nozare::model()->findByPk($_GET['id']);
    if($nozare) {
      $stds = $nozare->getStandartaPiedavajums();

      $result = array();
      if(!empty($stds)) {
        foreach($stds as $std) {
          $result[] = array(
            'id' => $std->id,
            'atslegvards' => $std->atslegvards,
            'izmaina' => $std->izmaina,
            'mekletajs' => $std->mekletajs,
            'valstis' => modelsToList($std->getValstis(), 'nosaukums'),
          );
        }
      }

      echo Zend_Json::encode($result);
    }
  }
  die();
}

function ajax_change_std_prices()
{
  if(!is_admin()) {
    die();
  }

  if(getGet('id')) {
    $model = StandartaPiedavajums::model()->findByPk(getGet('id'));
    $last_prices = $model->priceArray();

    //  Standarta cena
    $log = new ManualCenaLog;
    $log->atslegvards = $model->atslegvards;
    $log->mekletajs_id = $model->mekletajs_id;
    $log->no = $model->cena();
    if(isset($_GET['StandartaPiedavajums']['override'])) {
      $model->override = $_GET['StandartaPiedavajums']['override'];
    } else {
      $model->override = null;
    }
    $log->uz = $model->cena();
    $log->save();

    //  Speciālā cena
    $log = new ManualCenaLog;
    $log->atslegvards = $model->atslegvards;
    $log->mekletajs_id = $model->mekletajs_id;
    $log->no = $model->cena_s();
    $log->cenas_tips = '_s';
    if(isset($_GET['StandartaPiedavajums']['override_s'])) {
      $model->override_s = $_GET['StandartaPiedavajums']['override_s'];
    } else {
      $model->override_s = null;
    }
    $log->uz = $model->cena_s();
    $log->save();

    //  Top cena
    $log = new ManualCenaLog;
    $log->atslegvards = $model->atslegvards;
    $log->mekletajs_id = $model->mekletajs_id;
    $log->no = $model->cena_t();
    $log->cenas_tips = '_t';
    if(isset($_GET['StandartaPiedavajums']['override_t'])) {
      $model->override_t = $_GET['StandartaPiedavajums']['override_t'];
    } else {
      $model->override_t = null;
    }
    $log->uz = $model->cena_t();
    $log->save();


    if($model->save()) {

      //  Sinhronizē ar pārējiem
      $model->synchronize(array('override', 'override_s', 'override_t'));

      //  Ielogo cenu izmaiņu
      $model->logPriceChange('manual', $last_prices);

      echo Zend_Json::encode(true);
    } else {
      echo Zend_Json::encode(false);
    }
  }
}

//  Not used. 2012-08-23
function ajax_search_std_av()
{
  $ids = array();
  if(isset($_GET['avstring'])) {
    $models = StandartaPiedavajums::model()->findAll(array('conditions' => array('atslegvards LIKE "%'.esc($_GET['avstring']).'%"')));

    //  html output
    $html = '<table cellpadding="3" cellspacing="0" id="atslegvardi_list" class="data ckey_pieteikumi_list" style="width: auto;">
      <thead>
        <tr class="header">
            ' . print_th('Atslēgas vārds', 'atslegvards', array(), true) . '
            ' . print_th('Meklēšanas skaits', 'meklejumi', array(), true) . '
            <th class="header">Google klikšķa cena</th>
            <th class="header">Standarta cena</th>
            <th class="header">Speciālā cena</th>
            <th class="header">Top cena</th>
            <th class="hidethistoo" width="120" class="header">Darbības</th>
        </tr>
      </thead>
      <tbody class="main">';

      $currency = 'LVL';
      $i = 1;
      foreach((array)$models as $model) {
        $ids[] = $model->nozare_id;

        $html .= '<tr '. ($i % 2 ? 'class="odd"' : ''). '>
          <td class="av"><a href="?c=nozares&a=cenu_izmainas&id='. $model->id . '">'.$model->atslegvards.'</a></td>
          <td class="r">'. $model->meklejumi . '</td>
          <td class="r">';

        if($currency != 'LVL') {
          $html .= convert_currency($model->google_cpc, 'LVL', $currency);
        } else {
          $html .= $model->google_cpc;
        }

        $html .= '</td>
          <td class="r cena">';

        if($model->override) {
          $html .= '<span class="bez_pvn_rek_ico" style="margin-top: 2px;">!</span>';
        }

        $html .= '<span class="value">'. format_currency($model->cena($currency)).'</span>
          </td>
          <td class="r cena_s">';

        if($model->override_s) {
          $html .= '<span class="bez_pvn_rek_ico" style="margin-top: 2px;">!</span>';
        }

        $html .= '<span class="value">'. format_currency($model->cena_S($currency)).'</span>
          </td>
          <td class="r cena_t">';
        if($model->override_t) {
          $html .= '<span class="bez_pvn_rek_ico" style="margin-top: 2px;">!</span>';
        }

        $html .= '<span class="value">'. format_currency($model->cena_T($currency)).'</span>
          </td>
          <td class="hidethistoo">
            <button onclick="changePrices(this, '.$model->id.', '.($model->override !== null ? 1 : 0).',  '.($model->override_s !== null ? 1 : 0).', '.($model->override_t !== null ? 1 : 0).'); return false;" class="ui-state-default ui-corner-all">Mainit cenas</button>';

       $html .= '
          </td>
        </tr>';
      $i++;
    }
    $html .= '</tbody>
      </table>';
  }

  echo Zend_Json::encode(array($ids, $html));
}

function ajax_get_std_prices()
{
  $id = getRequest('id');
  if($id) {
    $model = StandartaPiedavajums::model()->findByPk($id);
    $data = array(
      'cena' => $model->cena(),
      'cena_s' => $model->cena_s(),
      'cena_t' => $model->cena_t(),
      'atslegvards' => $model->atslegvards,
    );

    echo Zend_Json::encode($data);
    die();
  }
}
?>