<?php
function resize_bytes($size) {

   if($size==0){ $size = 1; }

   $filesizename = array(" Bytes", " KB", " MB", " GB", " TB", " PB", " EB", " ZB", " YB");

   return round($size/pow(1024, ($i = floor(log($size, 1024)))), 2) . $filesizename[$i];

}

function upload($files, $who, $dest, $overwrite=true) { // False - OK, else return error

  if (!is_uploaded_file($files['tmp_name'][$who]) || $files['error'][$who] != 0) return false;

  if ($files['size'][$who] <= 0) return false;

  if(!$overwrite) if (file_exists($dest)) return false;

  if (function_exists("move_uploaded_file")) {
    if (!move_uploaded_file($files['tmp_name'][$who], $dest))  return false;
  } else {
    if (!copy($fails["tmp_name"][$who], $dest))  return false;
  }

  if (!chmod($dest, 0644)) return false;

  return true;

}

function trim_array($array) {

  if (!is_array($array)) {
    return false;
  }

  foreach($array as $k => $v) {

    if (is_array($v)) {
      $array[$k] = trim_array($v);
    } elseif (is_string($v)) {
      $array[$k] = trim($v);
    } else {
      $array[$k] = $v;
    }

  }

  return $array;

}

function debug_var($var) {

  echo '<pre>';
  print_r($var);
  echo '</pre>';

}

function standard_error_handler($errno, $errstr, $errfile, $errline, $errcontext) {

  $msg = '';

  $levels = array(
    1 => 'E_ERROR',
    2 => 'E_WARNING',
    4 => 'E_PARSE',
    8 => 'E_NOTICE',
    16 => 'E_CORE_ERROR',
    32 => 'E_CORE_WARNING',
    64 => 'E_COMPILE_ERROR',
    128 => 'E_COMPILE_WARNING',
    256 => 'E_USER_ERROR',
    512 => 'E_USER_WARNING',
    1024 => 'E_USER_NOTICE',
    2048 => 'E_STRICT',
    4096 => 'E_RECOVERABLE_ERROR',
    8192 => 'E_DEPRECATED',
    16384 => 'E_USER_DEPRECATED'
  );

  $errtype = isset($levels[$errno]) ? $levels[$errno] : $errno;

  $msg .= sprintf("%s: %s in %s on line %s)\n\n", $errtype, $errstr, $errfile, $errline);

  if (php_sapi_name() != 'cli') {

    $msg .= sprintf("Time: %s\n", date('Y-m-d H:i:s'));
    $msg .= sprintf("IP: %s\n\n", $_SERVER['REMOTE_ADDR']);

    $msg .= sprintf("CONTEXT: \n\n%s\n\n", print_r($errcontext, true));

    //$msg .= sprintf("BACKTRACE: \n\n%s\n\n", print_r(debug_backtrace(), true));

    $msg .= sprintf("_SERVER variable: \n\n%s\n\n", print_r($_SERVER, true));

  }

  if (STOP_ON_ERRORS) {

    die('<pre style="text-align: left;">' . $msg . '</pre>');

  } else {

    if (ERROR_MAIL) {
      mail(ERROR_MAIL, 'Infomedia System error', $msg);
    }

    return false; // "give" error back to standard PHP error handler

  }

}

function show_files_block($objekta_tips, $objekta_id = null, $show_submit = false, $only_view = false, $limited_admin_edit = false, $bookkeeper_admin_edit = false, $minimal_admin_edit = false) {

  ?>

  <div class="file_block">

    <h2>Saistītie faili</h2>

    <? if(!empty($objekta_id) || $objekta_id === 0) { ?>

      <ul>

      <? $query = db_query("
        SELECT *
        FROM `".DB_PREF."faili`
        WHERE
          `objekta_id` = ".(int)$objekta_id." AND
          `objekta_tips` = '".esc($objekta_tips)."'
        ORDER BY `id` DESC
      "); ?>

      <? while($row = db_get_assoc($query)) { ?>
        <li style="margin-bottom:10px;"><a class="download_link" href="?getfile=<?=$row['id'];?>" target="downloadframe"><?=$row['fails'];?></a> <span class="filesize">(<?=resize_bytes($row['izmers']);?>)</span>
        <? if (!$only_view && ((is_admin() && !is_limited()) || $row['pardeveja_id'] == $_SESSION['user']['id'])) { ?>
          <a class="hidethistoo <?= !empty($limited_admin_edit) ? 'limited_admin_edit' : '' ?> <?= !empty($bookkeeper_admin_edit) ? 'bookkeeper_admin_edit' : '' ?> <?= !empty($minimal_admin_edit) ? 'minimal_admin_edit' : '' ?>" href="javascript:;" onClick="deleteFile(this,<?=$row['id'];?>)">Dzēst šo failu!</a></li>
        <? } ?>
      <? } ?>

      </ul>

    <? } ?>

    <div style="margin:5px;"></div>

    <?php if (!$only_view) { ?>

      <button class="hidethistoo <?= !empty($limited_admin_edit) ? 'limited_admin_edit' : '' ?> <?= !empty($bookkeeper_admin_edit) ? 'bookkeeper_admin_edit' : '' ?> <?= !empty($minimal_admin_edit) ? 'minimal_admin_edit' : '' ?>" onclick="addmorefiles(this, 'faili_<?=$objekta_tips;?>'); $(this).parents('div.file_block').find('div.submit').show(); return false;">Pievienot jaunu failu</button>

      <? if ($show_submit) { ?>
        <div style="display: none; text-align: center; margin-top: 10px;" class="submit">
          <button type="submit">Saglabāt</button>
        </div>
      <? } ?>

    <?php } ?>

  </div>

  <?php

}

function show_files_block_wperms($objekta_tips, $objekta_id = null, $show_submit = false, $add = false, $delete = false) {

  ?>

  <div class="file_block">

    <h2>Saistītie faili</h2>

    <? if(!empty($objekta_id)) { ?>

      <ul>

      <? $query = db_query("
        SELECT *
        FROM `".DB_PREF."faili`
        WHERE
          `objekta_id` = ".(int)$objekta_id." AND
          `objekta_tips` = '".esc($objekta_tips)."'
        ORDER BY `id` DESC
      "); ?>

      <? while($row = db_get_assoc($query)) { ?>
        <li style="margin-bottom:10px;"><a class="download_link" href="?getfile=<?=$row['id'];?>" target="downloadframe"><?=$row['fails'];?></a> <span class="filesize">(<?=resize_bytes($row['izmers']);?>)</span>
        <? if ($delete) { ?>
          <a class="hidethistoo canedit" href="javascript:;" onClick="deleteFile(this,<?=$row['id'];?>)">Dzēst šo failu!</a></li>
        <? } ?>
      <? } ?>

      </ul>

    <? } ?>

    <div style="margin:5px;"></div>

    <?php if ($add) { ?>

      <button class="hidethistoo canedit" onclick="addmorefiles(this, 'faili_<?=$objekta_tips;?>'); $(this).parents('div.file_block').find('div.submit').show(); return false;">Pievienot jaunu failu</button>

      <? if ($show_submit) { ?>
        <div style="display: none; text-align: center; margin-top: 10px;" class="submit">
          <button type="submit">Saglabāt</button>
        </div>
      <? } ?>

    <?php } ?>

  </div>

  <?php

}

function microtime_diff($start, $end) {

  return round($end - $start, 6);

}


function process_files($file_types, $object_id) {

  foreach($file_types as $file_type) {

    $key = 'faili_' . $file_type;

    if (!empty($_FILES[$key])) {

      foreach($_FILES[$key]['name'] as $k => $name){

        db_query("
          INSERT INTO `".DB_PREF."faili` SET
            `objekta_id` = ".$object_id.",
            `objekta_tips` = '".$file_type."',
            `pardeveja_id` = ".$_SESSION['user']['id'].",
            `fails` = '".esc($name)."',
            `izmers` = ".$_FILES[$key]['size'][$k]."
        ");

        $faila_id = db_last_id();

        if(!upload($_FILES[$key], $k, "faili/" . db_last_id(), false)) {

          db_query("DELETE FROM `".DB_PREF."faili` WHERE `id` = ".db_last_id());

        } else {

          $log_data = array();

          $log_data['fails']['new'] = db_get_assoc(db_query("SELECT * FROM `".DB_PREF."faili` WHERE id = " . $faila_id));
          $log_data['fails']['title'] = sprintf('Pievienoja jaunu failu ar ID %s', $faila_id);

          log_add("laboja", $log_data);

        }

      }

    }

  }

}

function is_superadmin() {

  if (is_admin() && $_SESSION['user']['superadmin'] == 1) {
    return true;
  } else {
    return false;
  }
}

function is_admin() {

  if (!empty($_SESSION['user']['tips']) && $_SESSION['user']['tips'] == 'admin') {
    return true;
  } else {
    return false;
  }
}

function is_limited() {

  if (!empty($_SESSION['user']['ierobezots'])) {
    return true;
  } else {
    return false;
  }
}

function is_bookkeeper()
{
  if (!empty($_SESSION['user']['ierobezots']) && $_SESSION['user']['ierobezots'] == 2) {
    return true;
  } else {
    return false;
  }
}

function is_minimal()
{
  if (!empty($_SESSION['user']['ierobezots']) && $_SESSION['user']['ierobezots'] == 3) {
    return true;
  } else {
    return false;
  }
}

function format_currency($amount, $coma = false) {

  if ($coma) {
    return number_format($amount, 2, ',', '');
  } else {
    return number_format($amount, 2, '.', '');
  }

}


function show_comments_block($objekta_tips, $objekta_id = null, $show_submit = false, $only_view = false) {

  ?>

  <div class="comments_block">

    <h2>Komentāri</h2>

    <?php if (!$only_view) { ?>

      <div style="padding-bottom:5px; text-align: center;">
        <button class="hidethistoo non_admin_edit limited_admin_edit bookkeeper_admin_edit minimal_admin_edit" onclick="addmorecomments(this, '<?=$objekta_tips;?>'); $(this).parents('div.comments_block').find('div.submit').show(); return false;">Pievienot jaunu komentāru</button>
      </div>

      <? if ($show_submit) { ?>
        <div style="display: none; text-align: center;" class="submit">
          <button type="submit">Saglabāt</button>
        </div>
      <? } ?>

    <?php } ?>

    <ul>

    <? if(!empty($objekta_id) || substr($objekta_tips, 0, 1) == '_') {

      $query = db_query("
        SELECT
          k.*,
          f.nosaukums as lauka_nosaukums,
          p.vards as pardeveja_vards
        FROM `".DB_PREF."komentari` k
        LEFT JOIN `".DB_PREF."formu_lauki` f ON (f.id = k.formas_lauka_id)
        LEFT JOIN `".DB_PREF."pardeveji` p ON (p.id = k.pardeveja_id)
        WHERE
          k.`objekta_tips` = '".esc($objekta_tips)."'
          " . (substr($objekta_tips, 0, 1) != '_' ? " AND k.`objekta_id` = ".(int)$objekta_id : '') . "
        ORDER BY k.created DESC
      ");

      $i = db_num_rows($query);

      while($row = db_get_assoc($query)) {
      ?>

        <li>
          <div class="meta">
            <span class="n"><?= $i ?>.</span>
            <strong><?= $row['pardeveja_vards'] ?></strong>
            <? if (!empty($row['lauka_nosaukums'])) { ?>
              par lauku <strong><?= $row['lauka_nosaukums'] ?></strong>
            <? } ?>
            <span class="time"><?= date('d.m.Y H:i', strtotime($row['created'])) ?></span>
            <? if (!$only_view && ((is_admin() && !is_limited()) || $row['pardeveja_id'] == $_SESSION['user']['id'])) { ?>
              <a class="del_link" href="#" onclick="deleteComment(this, <?= $row['id'] ?>); return false;" title="Dzēst"><img src="css/del.png" alt="Dzēst" /></a>
            <? } ?>
          </div>
          <div class="text">
            <?= nl2br($row['komentars']) ?>
          </div>
          <div class="clear"></div>
        </li>

        <? $i -- ?>

      <?  } ?>

    <? } ?>

    </ul>

  </div>

  <?php

}


function process_comments($object_types, $object_id = null) {

  foreach($object_types as $object_type) {

    $key = $object_type;

    if (!empty($_POST['komentari_new'][$key])) {

      foreach($_POST['komentari_new'][$key]['komentars'] as $k => $komentars){


        if (empty($komentars)) {
          continue;
        }

        $formas_lauka_id = 'null';
        if (!empty($_POST['komentari_new'][$key]['formas_lauka_id'][$k])) {
          $formas_lauka_id = (int) $_POST['komentari_new'][$key]['formas_lauka_id'][$k];
        }

        db_query("
          INSERT INTO `".DB_PREF."komentari` SET
            `objekta_id` = ".(!empty($object_id) ? (int)$object_id : 'null').",
            `objekta_tips` = '".$object_type."',
            `formas_lauka_id` = ".$formas_lauka_id.",
            `pardeveja_id` = ".$_SESSION['user']['id'].",
            `komentars` = '".esc($komentars)."'
        ");

        $komentara_id = db_last_id();

        $log_data = array();

        $log_data['komentars']['new'] = db_get_assoc(db_query("SELECT * FROM `".DB_PREF."komentari` WHERE id = " . $komentara_id));
        $log_data['komentars']['title'] = sprintf('Pievienoja jaunu komentāru ar ID %s', $komentara_id);

        log_add("laboja", $log_data);

      }

    }

  }

}

function get_apmaksas_statuss($rekini, $checkdate = null) {

  if (empty($checkdate)) {
    $checkdate = date("Y-m-d", strtotime('-3 days'));
  }

  $statuss = 0;

  $kopa = 0;

  $nav_sacies = 0;
  $apmaksats = 0;
  $parads = 0;
  $nav_apmaksats = 0;
  $dal_apmaksats = 0;
  $anulets = 0;
  $nodots_piedz = 0;

  if(!empty($rekini)) {

    foreach($rekini as $v) {

      $kopa ++;

      if(!empty($v['izr_datums'])) {

        if(!empty($v['anulets'])){

          $anulets ++;

        } else if(!empty($v['nodots_piedz'])){

          $nodots_piedz ++;

        } else {

          if(!empty($v['apm_termins'])) {

            $apmSumSant = !empty($v['apm_summa']) ? (int) ($v['apm_summa'] * 100) : 0;
            $needSumSant = !empty($v['kopsumma']) ? (int) ($v['kopsumma'] * 100) : 0;

            if ($needSumSant <= $apmSumSant){

              $apmaksats ++;

            } elseif (empty($apmSumSant)) {

              if($checkdate > $v['apm_termins']){

                $parads ++;

              } else {

                $nav_apmaksats ++;

              }

            } else {

              if($checkdate > $v['apm_termins']) {

                $parads ++;

              } else {

                $dal_apmaksats ++;

              }

            }

          }

        }

      }

    }

  }

  if ($kopa == 0 || ($dal_apmaksats == 0 && $apmaksats == 0 && $parads == 0 && $nodots_piedz == 0 && $anulets != $kopa)) {

    // nav apm. termiņs (visi rēķini ir neapmaksāti, bet nav parādu un piedziņas)

    $statuss = 1;

  } else if ($apmaksats > 0 && $dal_apmaksats == 0 && $parads == 0 && $nodots_piedz == 0) {

    // apmaksāts (nav parādu un piedziņas)

    $statuss = 2;

  } else if ($dal_apmaksats > 0 && $parads == 0 && $nodots_piedz == 0) {

    // daļēji apmaksāts (kāds rēķins ir daļēji apmaksāts, bet nav parādu un piedziņas)

    $statuss = 3;

  } else if ($parads > 0) {

    // parāds (kādam no rēķiniem ir parāds ilgāk par 3 dienām)

    $statuss = 4;

  } else if ($anulets == $kopa) {

    // anulēts (visi rēķini anulēti)

    $statuss = 5;

  } else if ($nodots_piedz > 0) {

    // nodots piedziņai (kāds no rēķiniem ir nodots piedziņai)

    $statuss = 6;

  }

  return $statuss;

}

function get_max_nokavetas_dienas($rekini) {

  $curdate = date("Y-m-d");

  $dienas = 0;

  if(!empty($rekini)) {

    foreach($rekini as $v) {

      if(!empty($v['apm_termins']) && empty($v['anulets'])) {

        $apmSumSant = !empty($v['apm_summa']) ? (int) ($v['apm_summa'] * 100) : 0;
        $needSumSant = !empty($v['kopsumma']) ? (int) ($v['kopsumma'] * 100) : 0;

        if ($needSumSant > $apmSumSant && $v['apm_termins'] < $curdate) {

          $diff = days_between($v['apm_termins'], $curdate);

          if ($diff > $dienas) {
            $dienas = $diff;
          }

        }

      }

    }

  }

  return $dienas;

}

function is_barteris($rekini) {

  $bartera_rekini = 0;

  foreach($rekini as $r) {

    if (!empty($r['barteris'])) {

      $bartera_rekini ++;

    }

  }

  if (count($rekini) > 0 && count($rekini) == $bartera_rekini) {
    return true;
  }

  return false;

}

function get_liguma_statuss($ligums, $rekini) {
  $rekinu_statuss = get_apmaksas_statuss($rekini);

  if (empty($rekinu_statuss) || empty($ligums['reklama_no']) || (empty($ligums['reklama_lidz']) && (!$ligums['mpk'] || $ligums['campaign_type'] != 'test')) || empty($ligums['google_statuss'])) {
    return 0;
  }

  $c = array();

  $c[1] = array(); // vēl nav
  $c[2] = array(); // aktīvs
  $c[3] = array(); // beidzies

  $c[1][1] = array(); // nav ievadīts
  $c[1][2] = array(); // ievadīts
  $c[1][3] = array(); // aktīvs
  $c[1][4] = array(); // atslēgts

  $c[2][1] = array(); // nav ievadīts
  $c[2][2] = array(); // ievadīts
  $c[2][3] = array(); // aktīvs
  $c[2][4] = array(); // atslēgts

  $c[3][1] = array(); // nav ievadīts
  $c[3][2] = array(); // ievadīts
  $c[3][3] = array(); // aktīvs
  $c[3][4] = array(); // atslēgts


  // Reklāmas termiņš: vēl nav
  // -------------------------

  // Google statuss: nav ievadīts
  $c[1][1][1] = 1;
  $c[1][1][2] = 1;
  $c[1][1][3] = 1;
  $c[1][1][4] = 1;
  $c[1][1][5] = 7;
  $c[1][1][6] = 1;

  // Google statuss: ievadīts
  $c[1][2][1] = 1;
  $c[1][2][2] = 1;
  $c[1][2][3] = 1;
  $c[1][2][4] = 1;
  $c[1][2][5] = 7;
  $c[1][2][6] = 1;

  // Google statuss: aktīvs
  $c[1][3][1] = 1;
  $c[1][3][2] = 1;
  $c[1][3][3] = 1;
  $c[1][3][4] = 1;
  $c[1][3][5] = 7;
  $c[1][3][6] = 1;

  // Google statuss: atslēgts
  $c[1][4][1] = 1;
  $c[1][4][2] = 1;
  $c[1][4][3] = 1;
  $c[1][4][4] = 1;
  $c[1][4][5] = 7;
  $c[1][4][6] = 1;


  // Reklāmas termiņš: aktīvs
  // ------------------------

  // Google statuss: nav ievadīts
  $c[2][1][1] = 3;
  $c[2][1][2] = 3;
  $c[2][1][3] = 3;
  $c[2][1][4] = 3;
  $c[2][1][5] = 7;
  $c[2][1][6] = 3;

  // Google statuss: ievadīts
  $c[2][2][1] = 3;
  $c[2][2][2] = 3;
  $c[2][2][3] = 3;
  $c[2][2][4] = 3;
  $c[2][2][5] = 7;
  $c[2][2][6] = 3;

  // Google statuss: aktīvs
  $c[2][3][1] = 3;
  $c[2][3][2] = 3;
  $c[2][3][3] = 3;
  $c[2][3][4] = 3;
  $c[2][3][5] = 7;
  $c[2][3][6] = 3;

  // Google statuss: atslēgts
  $c[2][4][1] = 4; // vai 5 (sk. tālāk kodā)
  $c[2][4][2] = 4; // vai 5
  $c[2][4][3] = 4; // vai 5
  $c[2][4][4] = 4; // vai 5
  $c[2][4][5] = 7;
  $c[2][4][6] = 4; // vai 5


  // Reklāmas termiņš: beidzies
  // --------------------------

  // Google statuss: nav ievadīts
  $c[3][1][1] = 6;
  $c[3][1][2] = 6;
  $c[3][1][3] = 6;
  $c[3][1][4] = 6;
  $c[3][1][5] = 7;
  $c[3][1][6] = 6;

  // Google statuss: ievadīts
  $c[3][2][1] = 6;
  $c[3][2][2] = 6;
  $c[3][2][3] = 6;
  $c[3][2][4] = 6;
  $c[3][2][5] = 7;
  $c[3][2][6] = 6;

  // Google statuss: aktīvs
  $c[3][3][1] = 6;
  $c[3][3][2] = 6;
  $c[3][3][3] = 6;
  $c[3][3][4] = 6;
  $c[3][3][5] = 7;
  $c[3][3][6] = 6;

  // Google statuss: atslēgts
  $c[3][4][1] = 6;
  $c[3][4][2] = 6;
  $c[3][4][3] = 6;
  $c[3][4][4] = 6;
  $c[3][4][5] = 7;
  $c[3][4][6] = 6;


  $lig_statuss = 0;

  $rekl_statuss = get_reklamas_statuss($ligums);

  foreach($c as $k1 => $v1) {

    if ($rekl_statuss == $k1) {

      foreach($v1 as $k2 => $v2) {

        if ($ligums['google_statuss'] == $k2) {

          foreach($v2 as $k3 => $v3) {

            if ($k3 == $rekinu_statuss) {

              if ($k1 == 2 && $k2 == 4 && in_array($k3, array(1, 2, 3, 4, 6))) {

                // ja reklāma aktīva, bet gūgles statuss pātraukts, tad līguma
                // statuss "apturēts"

                // ja reklāma aktīva, bet gūgles statuss pātraukts, un visi
                // rēķini ir vai nu apmaksāti vai anulēti (bet ne vienādi),
                // tad statuss ir "pārtraukts"

                if (is_rekini_visi_apm_un_annul($rekini)) {

                  $lig_statuss = 5; // paatraukts
                  break 3;

                } else {

                  $lig_statuss = $v3;
                  break 3;

                }

              } else {

                $lig_statuss = $v3;
                break 3;

              }

            }

          }

        }

      }

    }

  }

  return $lig_statuss;

}

function get_liguma_status_by_id($ligums_id) {
  $result = db_query("
    SELECT g.*
    FROM `".DB_PREF."ligumi` as g
    WHERE id = " . esc($ligums_id)
  );

  $row = db_get_assoc($result);

  //  Pārbaudam vai līgums aktīvs
  $query_rekini = db_query("
    SELECT r.*
    FROM `".DB_PREF."rekini` as r
    WHERE liguma_id = " . esc($ligums_id)
  );
  $rekini = array();
  while($rekini_row = db_get_assoc($query_rekini)) {
    $rekini[] = $rekini_row;
  }

  return get_liguma_statuss($row, $rekini);
}

function is_rekini_visi_apm_un_annul($rekini) {

  $kopa = 0;
  $anulets = 0;
  $apmaksats = 0;

  foreach((array)$rekini as $r) {

    $kopa ++;

    if (!empty($r['izr_datums'])) {

      if (!empty($r['anulets'])) {

        $anulets ++;

      } else {

        if(!empty($r['apm_termins'])) {

          $apmSumSant = !empty($r['apm_summa']) ? (int) ($r['apm_summa'] * 100) : 0;
          $needSumSant = !empty($r['kopsumma']) ? (int) ($r['kopsumma'] * 100) : 0;

          if ($needSumSant <= $apmSumSant){
            $apmaksats ++;
          }

        }

      }

    }

  }

  if ($anulets > 0 && $apmaksats > 0 && $anulets + $apmaksats == $kopa) {
    return true;
  }

  return false;

}

function get_reklamas_statuss($ligums) {

  $rekl_statuss = 0;

  $curdate = date('Y-m-d');

  if(!$ligums['mpk'] || $ligums['campaign_type'] != 'test' || $ligums['reklama_lidz']) {
    if ($curdate < $ligums['reklama_no']) {
      $rekl_statuss = 1;
    } else if ($curdate >= $ligums['reklama_no'] && $curdate <= $ligums['reklama_lidz']) {
      $rekl_statuss = 2;
    } else if ($curdate > $ligums['reklama_lidz']) {
        $rekl_statuss = 3;
    }
  } else {
    //  MPK testa kampaņa
    if ($curdate < $ligums['reklama_no']) {
      $rekl_statuss = 1;
    } else if ($curdate >= $ligums['reklama_no']) {
      $rekl_statuss = 2;
    }
  }

  return $rekl_statuss;

}

function get_rekina_statuss($rekins) {

  $apm_statuss_id = null;

  if(!empty($rekins['anulets'])){

    $apm_statuss_id = 6;

  } else if(!empty($rekins['nodots_piedz'])){

    $apm_statuss_id = 7;

  } else {

    if(!empty($rekins['apm_termins'])){

      $now = date("Y")*10000+date("m")*100+date("d");

      $thisnow = explode(".", date("d.m.Y", strtotime($rekins['apm_termins'])));
      $thisnow = $thisnow[2]*10000+$thisnow[1]*100+$thisnow[0];

      $apmSumSant = !empty($rekins['apm_summa']) ? (int) ($rekins['apm_summa'] * 100) : 0;
      $needSumSant = !empty($rekins['kopsumma']) ? (int) ($rekins['kopsumma'] * 100) : 0;

      if ($needSumSant == $apmSumSant){

        $apm_statuss_id = 2;

      } elseif (empty($apmSumSant)) {

        if($now > $thisnow){

          $apm_statuss_id = 3;

        } else {

          $apm_statuss_id = 4;

        }

      } else {

        if($now > $thisnow) {

          $apm_statuss_id = 3;

        } else {

          $apm_statuss_id = 5;

        }

      }

    }

  }

  return $apm_statuss_id;

}

function generate_auto_reminders($ligums, $beigu_statuss, $rekini) {

  $rekinu_statuss = get_apmaksas_statuss($rekini);

  if (empty($rekinu_statuss) || empty($ligums['reklama_no']) || empty($ligums['reklama_lidz']) || empty($ligums['google_statuss'])) {
    return 0;
  }

  $c = array();

  $c[1] = array(); // vēl nav
  $c[2] = array(); // aktīvs
  $c[3] = array(); // beidzies

  $c[1][1] = array(); // nav ievadīts
  $c[1][2] = array(); // ievadīts
  $c[1][3] = array(); // aktīvs
  $c[1][4] = array(); // atslēgts

  $c[2][1] = array(); // nav ievadīts
  $c[2][2] = array(); // ievadīts
  $c[2][3] = array(); // aktīvs
  $c[2][4] = array(); // atslēgts

  $c[3][1] = array(); // nav ievadīts
  $c[3][2] = array(); // ievadīts
  $c[3][3] = array(); // aktīvs
  $c[3][4] = array(); // atslēgts


  // Reklāmas termiņš: vēl nav
  // -------------------------

  // Google statuss: nav ievadīts
  $c[1][1][1] = false;
  $c[1][1][2] = false;
  $c[1][1][3] = false;
  $c[1][1][4] = 1;
  $c[1][1][5] = 2;
  $c[1][1][6] = false;

  // Google statuss: ievadīts
  $c[1][2][1] = false;
  $c[1][2][2] = false;
  $c[1][2][3] = false;
  $c[1][2][4] = 1;
  $c[1][2][5] = 2;
  $c[1][2][6] = false;

  // Google statuss: aktīvs
  $c[1][3][1] = 3;
  $c[1][3][2] = 3;
  $c[1][3][3] = 3;
  $c[1][3][4] = array(1);
  $c[1][3][5] = array(4, 2);
  $c[1][3][6] = 4;

  // Google statuss: atslēgts
  $c[1][4][1] = false;
  $c[1][4][2] = false;
  $c[1][4][3] = false;
  $c[1][4][4] = 1;
  $c[1][4][5] = 2;
  $c[1][4][6] = false;


  // Reklāmas termiņš: aktīvs
  // ------------------------

  // Google statuss: nav ievadīts
  $c[2][1][1] = array(5, 6);
  $c[2][1][2] = array(5, 6);
  $c[2][1][3] = array(5, 6);
  $c[2][1][4] = 1;
  $c[2][1][5] = 2;
  $c[2][1][6] = false;

  // Google statuss: ievadīts
  $c[2][2][1] = 6;
  $c[2][2][2] = 6;
  $c[2][2][3] = 6;
  $c[2][2][4] = 1;
  $c[2][2][5] = 2;
  $c[2][2][6] = false;

  // Google statuss: aktīvs
  $c[2][3][1] = false;
  $c[2][3][2] = false;
  $c[2][3][3] = false;
  $c[2][3][4] = array(1);
  $c[2][3][5] = array(4, 2);
  $c[2][3][6] = 4;

  // Google statuss: atslēgts
  $c[2][4][1] = false; // ja līg. statuss "apturēts", tad 6
  $c[2][4][2] = false; // // ja līg. statuss "apturēts", tad 6
  $c[2][4][3] = false; // // ja līg. statuss "apturēts", tad 6
  $c[2][4][4] = 1; //
  $c[2][4][5] = 2;
  $c[2][4][6] = false;


  // Reklāmas termiņš: beidzies
  // --------------------------

  // Google statuss: nav ievadīts
  $c[3][1][1] = false; // 2 vai 7 (sk. tālāk)
  $c[3][1][2] = false; // 2 vai 7 (sk. tālāk)
  $c[3][1][3] = false; // 2 vai 7 (sk. tālāk)
  $c[3][1][4] = 1;  // 2 vai 7 (sk. tālāk)
  $c[3][1][5] = 2;
  $c[3][1][6] = false; // 2 vai 7 (sk. tālāk)

  // Google statuss: ievadīts
  $c[3][2][1] = false; // 2 vai 7 (sk. tālāk)
  $c[3][2][2] = false; // 2 vai 7 (sk. tālāk)
  $c[3][2][3] = false; // 2 vai 7 (sk. tālāk)
  $c[3][2][4] = 1; // 2 vai 7 (sk. tālāk)
  $c[3][2][5] = 2;
  $c[3][2][6] = false; // 2 vai 7 (sk. tālāk)

  // Google statuss: aktīvs
  $c[3][3][1] = 4;
  $c[3][3][2] = 4;
  $c[3][3][3] = 4;
  $c[3][3][4] = array(1); // 2 vai 7 (sk. tālāk)
  $c[3][3][5] = array(4, 2);
  $c[3][3][6] = 4;  // 2 vai 7 (sk. tālāk)

  // Google statuss: atslēgts
  $c[3][4][1] = false; // 2 vai 7 (sk. tālāk)
  $c[3][4][2] = false; // 2 vai 7 (sk. tālāk)
  $c[3][4][3] = false;  // 2 vai 7 (sk. tālāk)
  $c[3][4][4] = 1; // 2 vai 7 (sk. tālāk)
  $c[3][4][5] = 2;
  $c[3][4][6] = false;  // 2 vai 7 (sk. tālāk)

  $rekl_statuss = 0;

  $curdate = date('Y-m-d');

  if ($curdate < $ligums['reklama_no']) {
    $rekl_statuss = 1;
  } else if ($curdate > $ligums['reklama_no'] && $curdate < $ligums['reklama_lidz']) {
    $rekl_statuss = 2;
  } else if ($curdate > $ligums['reklama_lidz']) {
    $rekl_statuss = 3;
  }

  foreach($c as $k1 => $v1) {

    if ($rekl_statuss == $k1) {

      foreach($v1 as $k2 => $v2) {

        if ($ligums['google_statuss'] == $k2) {

          foreach($v2 as $k3 => $atg_veidu_ids) {

            if ($k3 == $rekinu_statuss) {

              if (empty($atg_veidu_ids)) {
                $atg_veidu_ids = array();
              } if (!is_array($atg_veidu_ids)) {
                $atg_veidu_ids = array($atg_veidu_ids);
              }

              if ($k1 == 2 && $k2 == 4 && in_array($k3, array(1, 2, 3))) { // reklāmas termiņs: aktīvs; google statuss: atslēgts

                $kopa = 0;
                $anulets = 0;
                $apmaksats = 0;

                foreach($rekini as $r) {

                  $kopa ++;

                  if (!empty($r['izr_datums'])) {

                    if (!empty($r['anulets'])) {

                      $anulets ++;

                    } else {

                      if(!empty($r['apm_termins'])) {

                        $apmSumSant = !empty($r['apm_summa']) ? (int) ($r['apm_summa'] * 100) : 0;
                        $needSumSant = !empty($r['kopsumma']) ? (int) ($r['kopsumma'] * 100) : 0;

                        if ($needSumSant <= $apmSumSant){

                          $apmaksats ++;

                        }

                      }

                    }

                  }

                }

                if ($anulets > 0 && $apmaksats > 0 && $anulets + $apmaksats == $kopa) {
                  // do nothing
                } else {
                  $atg_veidu_ids[] = 6; // "Pieslēgt pie google"
                }

              }

              if ($k1 == 3 && (($k2 == 1 && in_array($k3, array(1, 2, 3, 4, 6))) || ($k2 == 2 && in_array($k3, array(1, 2, 3, 4, 6))) || ($k2 == 3 && in_array($k3, array(4, 6))) || ($k2 == 4 && in_array($k3, array(1, 2, 3, 4, 6))))) {

                if (empty($beigu_statuss) || ($beigu_statuss['beigu_statuss'] == 'termins')) {
                  $atg_veidu_ids[] = 7;
                } elseif (!empty($beigu_statuss) && $beigu_statuss['beigu_statuss'] == 'atteikums') {
                  $atg_veidu_ids[] = 2;
                }

              }

              if (!empty($atg_veidu_ids)) {

                foreach($atg_veidu_ids as $atgadinajuma_veida_id) {

                  $existing = db_get_assoc(db_query("
                    SELECT id
                    FROM `".DB_PREF."atgadinajumi`
                    WHERE
                      `liguma_id` = ".(int)$ligums['id']." AND
                      `atgadinajuma_veida_id` = ".(int)$atgadinajuma_veida_id." AND
                      `statuss` = 1
                  "));

                  if (empty($existing['id'])) {

                    $veids = db_get_assoc(db_query("
                      SELECT *
                      FROM `".DB_PREF."atgadinajumi_veidi`
                      WHERE id = ".(int)$atgadinajuma_veida_id."
                    "));

                    db_query("
                      INSERT INTO `".DB_PREF."atgadinajumi` SET
                        `liguma_id` = ".(int)$ligums['id'].",
                        `atgadinajuma_veida_id` = ".(int)$atgadinajuma_veida_id.",
                        `datums` = NOW(),
                        `tema` = ".(int)$veids['nokl_tema'].",
                        `saturs` = '".esc($veids['nokl_teksts'])."',
                        `redzamiba` = '".esc($veids['nokl_redzamiba'])."',
                        `pardevejs_var_labot` = ".$veids['nokl_pardevejs_var_labot']."
                    ");

                  }

                }

                break 3;

              }

            }

          }

        }

      }

    }

  }


}

function save_file($content, $dest) {

  $h = fopen($dest, 'w');

  if (!$h) return false;

  if (!fwrite($h, $content)) return false;

  fclose($h);

  if (!chmod($dest, 0644)) return false;

  return true;

}

function prepare_filename($str) {

  $str = str_replace(array('/', '\\', '?', '%', '*', ':', '|', '"', '<', '>'), '-', $str);
  $str = preg_replace('/\s{2,}/', ' ', $str);
  $str = preg_replace('/\-{2,}/', '-', $str);

  return $str;

}

function show_autodownload_iframes() {

  if (!empty($_SESSION['autodownload_file_ids'])) {

    foreach($_SESSION['autodownload_file_ids'] as $id) {

      echo '<iframe src="?getfile='.$id.'" style="display:none;"></iframe>';

    }

    unset($_SESSION['autodownload_file_ids']);

  }

}

function get_darb_rek_next_no($prefix, $pardeveja_id) {

  $len = strlen($prefix) + 1;

  $sql = "
    SELECT IFNULL(MAX(CONVERT(SUBSTR(`rek_nr`, ".$len."), SIGNED)), 0) + 1
    FROM `".DB_PREF."pardeveji_rekini`
    WHERE
      SUBSTR(`rek_nr`, 1, ".($len - 1).") = '".esc($prefix)."' AND
      pardeveja_id = ".$pardeveja_id."
  ";

  $next_no = db_get_val(db_query($sql));

  return $prefix . str_pad((string)$next_no, 3, '0', STR_PAD_LEFT);

}

function print_paginator($total_rows, $active_page, $show_in_page, $classes = array()) {

  if(!empty($classes)) {
    $page_class = $classes[0];
    $show_in_page_class = $classes[1];
  } else {
    $page_class = 'page';
    $show_in_page_class = 'show_in_page';
  }

  $total_pages = 1;

  if ($show_in_page > 0) {
    $total_pages = ceil($total_rows / $show_in_page);
  }

  ?>

  <div class="paging">

    <button onclick="$(this).parents('form').find('input[name=<?php echo $page_class;?>]').val(<?= $active_page - 1 ?>); $(this).parents('form').submit();" class="ui-state-default <?= ($active_page == 1) ? 'ui-state-disabled' : 'active_everywhere' ?> ui-corner-all" <?= ($active_page == 1) ? 'disabled' : '' ?>>&lt;&lt;</button>

    <select class="sfield" onchange="$(this).parents('form').find('input[name=<?php echo $page_class;?>]').val($(this).val()); $(this).parents('form').submit();">
      <? for($i = 1; $i <= $total_pages; $i ++) { ?>
        <option value="<?= $i ?>" <?= ($i == $active_page) ? 'selected="selected"' : '' ?>><?= ($i . '/' . $total_pages) ?></option>
      <? } ?>
    </select>

    <button onclick="$(this).parents('form').find('input[name=<?php echo $page_class;?>]').val(<?= $active_page + 1 ?>); $(this).parents('form').submit();" class="ui-state-default <?= ($active_page == $total_pages) ? 'ui-state-disabled' : 'active_everywhere' ?> ui-corner-all" <?= ($active_page == $total_pages) ? 'disabled' : '' ?>>&gt;&gt;</button>

    &nbsp;&nbsp;Rezultāti lapā:
    <input onchange="$(this).parents('form').find('input[name=<?php echo $show_in_page_class;?>]').val($(this).val()); $(this).parents('form').find('input[name=<?php echo $page_class;?>]').val(1); $(this).parents('form').submit();" type="text" style="width: 2em; text-align: center;" value="<?php echo $show_in_page ?>" />

    &nbsp;&nbsp;<a onclick="$(this).parents('form').find('input[name=<?php echo $page_class;?>]').val(1); $(this).parents('form').find('input[name=<?php echo $show_in_page_class;?>]').val(''); $(this).parents('form').submit(); return false;" href="#">Visi</a>

  </div>

  <?php

}

function getGet($name) {
  if (isset($_GET[$name])) {
    return $_GET[$name];
  }
  return '';
}

function getRequest($name) {
  if (isset($_GET[$name])) {
    return $_GET[$name];
  } elseif(isset($_POST[$name])) {
    return $_POST[$name];
  }
}

function print_th($title, $col_name = null, $params = array(), $return = false) {

  $col_name_attr = '';
  $onclick_attr = '';
  $width_attr = '';
  $style = '';

  if (!empty($col_name)) {

    $col_name_attr = 'colname="'.$col_name.'"';

    if (!isset($params['ordering']) || $params['ordering'] == true) {
      $onclick_attr = 'onclick="return changeTableOrder(this, \''.$col_name.'\');"';
    } else {
      $style = 'style="cursor: default;"';
    }

  }

  if (!empty($params['width'])) {
    $width_attr = !empty($params['width']) ? 'width="'.$params['width'].'"' : '';
  }

  if (!empty($params['rowspan'])) {
    $width_attr = !empty($params['rowspan']) ? 'rowspan="'.$params['rowspan'].'"' : '';
  }

  $html = '<th ' . $style . ' class="header" ' . $col_name_attr . ' ' . $width_attr . ' ' . $onclick_attr .'>' . $title .'</th>';

  if($return) {
    return $html;
  } else {
    echo $html;
  }
}

function to_db_date($date) {
  return date('Y-m-d', strtotime($date));
}

function get_nenomainitie_statusi($filter = array()) {

  static $cache = array();

  $key = serialize($filter);

  if (!empty($cache[$key])) {
    return $cache[$key];
  }

  if (!empty($filter['pardeveja_id'])) {
    $filter['pardeveja_id'] = (int) $filter['pardeveja_id'];
  }

  $cur_date = date('Y-m-d');

  $query = db_query("
    SELECT r.*
    FROM `".DB_PREF."rekini` r
    LEFT JOIN `".DB_PREF."ligumi` g ON (g.id = r.liguma_id)
    WHERE g.reklama_lidz <= '".$cur_date."'
  ");

  $rekini = array();
  while($row = db_get_assoc($query)) {
    $rekini[$row['liguma_id']][] = $row;
  }

  $sql = "
    SELECT
      g.*,
      b.beigu_statuss
  ";

  if (!empty($filter['pardeveja_id'])) {

    $sql .= "
        ,(
          SELECT COUNT(*)
          FROM `".DB_PREF."ligumi_saistitie_pardeveji`
          WHERE
            liguma_id = g.id AND
            pardeveja_id = ".$filter['pardeveja_id']."
        ) as saistitais_ligums
    ";

  } elseif(!empty($filter['pardeveju_ids'])) {
    $sql .= "
        ,(
          SELECT COUNT(*)
          FROM `".DB_PREF."ligumi_saistitie_pardeveji`
          WHERE
            liguma_id = g.id AND
            pardeveja_id IN  (".implode(', ', $filter['pardeveju_ids']).")
        ) as saistitais_ligums
    ";
  }

  $sql .= "
    FROM `".DB_PREF."ligumi` g
    LEFT JOIN `".DB_PREF."ligumi_beigusies` b ON (g.id = b.liguma_id)
    LEFT JOIN `".DB_PREF."pardeveji` p ON (p.id = g.pardeveja_id)
    WHERE
  ";

  if (!empty($filter['pardeveja_id'])) {

    $sql .= "
      (
        g.pardeveja_id = ".$filter['pardeveja_id']." OR
        g.id IN (
          SELECT liguma_id
          FROM `".DB_PREF."ligumi_saistitie_pardeveji`
          WHERE pardeveja_id = ".$filter['pardeveja_id']."
        )
      ) AND
    ";

  } elseif(!empty($filter['pardeveju_ids'])) {
    $sql .= "
      (
        g.pardeveja_id IN (".implode(',', $filter['pardeveju_ids']).") OR
        g.id IN (
          SELECT liguma_id
          FROM `".DB_PREF."ligumi_saistitie_pardeveji`
          WHERE pardeveja_id IN (".implode(',',$filter['pardeveju_ids']).")
        )
      ) AND
    ";
  }

  $sql .= "
      g.reklama_lidz <= '".$cur_date."' AND
      (
        b.id IS NULL OR
        (
          b.beigu_statuss = 'termins' AND
          b.atlikt_lidz <= '".$cur_date."'
        )
      )
    ORDER BY
  ";

  if (empty($filter['pardeveja_id'])) {
    $sql .= "p.vards ASC, ";
  }

  $sql .= " IF (b.beigu_statuss IS NOT NULL, b.atlikt_lidz, g.reklama_lidz) ASC";

  $query = db_query($sql);

  $cache[$key] = array();

  while($row = db_get_assoc($query)) {

    $lig_statuss = get_liguma_statuss($row, isset($rekini[$row['id']]) ? $rekini[$row['id']] : null);

    if ($lig_statuss == 6) { // beidzies
      $cache[$key][] = $row;
    }

  }

  return $cache[$key];

}

function htmlesc($mixed) {

  if (is_string($mixed)) {
    return htmlspecialchars($mixed);
  }

  if (!is_array($mixed)) {
    return $mixed;
  }

  foreach($mixed as $k => $v) {

    if (is_array($v)) {
      $mixed[$k] = htmlesc($v);
    } elseif (is_string($v)) {
      $mixed[$k] = htmlspecialchars($v);
    } else {
      $mixed[$k] = $v;
    }

  }

  return $mixed;

}

function translit($str) {

  //$str = mb_strtolower($str, 'utf-8');

  $pairs = array(

    'ā' => 'a',
    'č' => 'c',
    'ē' => 'e',
    'ģ' => 'g',
    'ī' => 'i',
    'ķ' => 'k',
    'ļ' => 'l',
    'ņ' => 'n',
    'š' => 's',
    'ū' => 'u',
    'ž' => 'z',
    'ō' => 'o',
    'ŗ' => 'r',

    'а' => 'a',
    'б' => 'b',
    'в' => 'v',
    'г' => 'g',
    'д' => 'd',
    'е' => 'e',
    'ё' => 'jo',
    'ж' => 'zh',
    'з' => 'z',
    'и' => 'i',
    'й' => 'ij',
    'к' => 'k',
    'л' => 'l',
    'м' => 'm',
    'н' => 'n',
    'о' => 'o',
    'п' => 'p',
    'р' => 'r',
    'с' => 's',
    'т' => 't',
    'у' => 'u',
    'ф' => 'f',
    'х' => 'h',
    'ц' => 'c',
    'ч' => 'ch',
    'ш' => 'sh',
    'щ' => 'shch',
    'ь' => '',
    'ы' => 'i',
    'ъ' => '',
    'э' => 'e',
    'ю' => 'ju',
    'я' => 'ja'
  );

  $upairs = array();

  foreach($pairs as $k => $v) {
    $upairs[mb_strtoupper($k, 'utf-8')] = ucfirst($v);
  }

  $str = strtr($str, $pairs);
  $str = strtr($str, $upairs);

  return $str;

}

function pt_print_selected($val, $field_name) {

  // this is for plain table multiselects

  $val = (string) $val;

  $values = getGet($field_name);

  if (is_array($values) && in_array($val, $values)) {
    echo 'selected="selected"';
  }

}

function pt_is_selected($val, $field_name) {

  // this is for plain table multiselects

  $val = (string) $val;

  $values = getGet($field_name);

  if (is_array($values) && in_array($val, $values)) {
    return true;
  }

  return false;

}

function pt_multiselect_is_active($field_name) {

  if (is_array(getGet($field_name)) && count(getGet($field_name)) > 0) {
    return true;
  }

  return false;

}


function get_tab_kolonnas($table_name) {

  $sql = "
    SELECT *
    FROM `".DB_PREF."tabulu_kolonnas`
    WHERE
      `pardeveja_id` = ".$_SESSION['user']['id']." AND
      `tabula` = '".esc($table_name)."'
  ";

  return db_get_assoc_all(db_query($sql));

}

function get_forums_jaunu_temu_skaits() {

  $sql = "
    SELECT COUNT(*)
    FROM `".DB_PREF."forums` f
    LEFT JOIN `".DB_PREF."forums_atverts` a ON (
      f.id = a.temas_id AND
      a.pardeveja_id = ".$_SESSION['user']['id']."
    )
    WHERE
      (
        f.parent_id IS NULL OR
        f.parent_id = 0
      ) AND
      a.pardeveja_id IS NULL
  ";

  return (int)db_get_val(db_query($sql));

}

function get_saist_lig_skaits($pardeveja_id) {

  $sql = "
    SELECT COUNT(*)
    FROM `".DB_PREF."ligumi_saistitie_pardeveji`
    WHERE pardeveja_id = ".$pardeveja_id."
  ";

  return (int)db_get_val(db_query($sql));

}

function days_between($date1, $date2) {

  $time1 = strtotime($date1);
  $time2 = strtotime($date2);

  $one_day = 60 * 60 * 24;

  return round(($time2 - $time1) / $one_day);

}

function convert($size){
	$unit=array('b','kb','mb','gb','tb','pb');
	return @round($size/pow(1024,($i=floor(log($size,1024)))),2).' '.$unit[$i];
}



function print_statistika_head() {

  ?>
  <thead>

    <tr class="header groups">
      <th rowspan="2" style="font-weight: normal; width: 60px;" class="b-r">Cet.</th>
      <th colspan="2" class="b-r color1">Atdoti</th>
      <th colspan="2" class="b-r color2">Apstrādāti</th>
      <th colspan="2" class="b-r color2">Neapstrādāti</th>
      <th colspan="2" class="b-r color4">Zaudēti</th>
      <th colspan="2" class="b-r color3">Atkārtoti</th>
      <th colspan="2" class="b-r color3">Jaunie</th>
      <th colspan="2" class="b-r color3">Atjaunoti</th>
      <th colspan="2" class="color5">Kopā</th>
    </tr>

    <tr class="header subgroups">
      <th>Skaits</th>
      <th class="b-r">Apgr.</th>
      <th>Skaits</th>
      <th class="b-r">Apgr.</th>
      <th>Skaits</th>
      <th class="b-r">Apgr.</th>
      <th>Skaits</th>
      <th class="b-r">Apgr.</th>
      <th>Skaits</th>
      <th class="b-r">Apgr.</th>
      <th>Skaits</th>
      <th class="b-r">Apgr.</th>
      <th>Skaits</th>
      <th class="b-r">Apgr.</th>
      <th class="color5">Skaits</th>
      <th class="color5">Apgr.</th>
    </tr>

  </thead>
  <?php

}

function print_statistika_rows($i, $d, $options = array()) {

  global $_vars;

  $roman_numbers = array(
    1 => 'I',
    2 => 'II',
    3 => 'III',
    4 => 'IV',
  );

  $months = $_vars['menesi'];

  if ($i != 'total') {
    list($year, $period) = explode('-', $i);
  }


  $tr_class = '';
  if (!empty($options['month_rows']))
    $tr_class = 'month_row';
  else if (!empty($options['pardeveja_rows']))
    $tr_class = 'pardeveja_row';

  ?>
  <tr class="top <?php echo $i == 'total' ? 'top_total' : '' ?> <?php echo $tr_class ?>">

    <td rowspan="3" class="b-r label">
      <?php if ($i == 'total') { ?>

        Pavisam gadā

      <?php } else { ?>

        <?php if (!empty($options['pardeveja_rows'])) { ?>

          <?php echo $options['pardevejs'] ?>

        <?php } else if (!empty($options['month_rows'])) { ?>

          <?php if (!empty($options['allow_open_month'])) { ?>
            <a href="#" class="open_pardeveji month-<?php echo $i ?>"><?php echo $months[(int)$period] ?></a>
          <?php } else { ?>
            <?php echo $months[(int)$period] ?>
          <?php } ?>

        <?php } else { ?>

          <a href="#" class="open_months quarter-<?php echo $i ?> <?php echo !empty($options['pardeveja_id']) ? 'pardeveja_id-' . $options['pardeveja_id'] : '' ?>"><?php echo $roman_numbers[(int)$period] ?></a>

        <?php } ?>

      <?php } ?>
    </td>

    <td class="c color1"><?php echo isset($d['atdoti']['skaits']) ? (is_admin() ? '<a href="?c=ligumi&fligumu_id=' . implode(',', $d['atdoti']['ligumu_id']) . '">' . $d['atdoti']['skaits'] . '</a>' : $d['atdoti']['skaits']) : '&nbsp;' ?></td>
    <td class="b-r color1"><?php echo isset($d['atdoti']['skaits']) ? format_currency($d['atdoti']['apgr']) : '' ?></td>

    <td class="c color2"><?php echo isset($d['apstradati']['skaits']) ? (is_admin() ? '<a href="?c=ligumi&fligumu_id=' . implode(',', $d['apstradati']['ligumu_id']) . '">' . $d['apstradati']['skaits'] . '</a>' : $d['apstradati']['skaits']) : '' ?></td>
    <td class="b-r color2"><?php echo isset($d['apstradati']['apgr']) ? format_currency($d['apstradati']['apgr']) : '' ?></td>

    <td title="atdotie - apstrādātie" class="c color2" ><?php echo isset($d['neaiztikti']['skaits']) ? (is_admin() ? '<a href="?c=ligumi&fligumu_id=' . implode(',', $d['neaiztikti']['ligumu_id']) . '">' . $d['neaiztikti']['skaits'] . '</a>' : $d['neaiztikti']['skaits']) : '' ?></td>
    <td class="b-r color2" title="atdotie - apstrādātie"><?php echo isset($d['neaiztikti']['apgr']) ? format_currency($d['neaiztikti']['apgr']) : '' ?></td>

    <td class="c color4"><?php echo isset($d['zaudeti']['skaits']) ? (is_admin() ? '<a href="?c=ligumi&fligumu_id=' . implode(',', $d['zaudeti']['ligumu_id']) . '">' . $d['zaudeti']['skaits'] . '</a>' : $d['zaudeti']['skaits']) : '' ?></td>
    <td class="b-r color4"><?php echo isset($d['zaudeti']['apgr']) ? format_currency($d['zaudeti']['apgr']) : '' ?></td>

    <td class="c color3"><?php echo isset($d['atkartoti']['skaits']) ? (is_admin() ? '<a href="?c=ligumi&fligumu_id=' . implode(',', $d['atkartoti']['ligumu_id']) . '">' . $d['atkartoti']['skaits'] . '</a>' : $d['atkartoti']['skaits']) : '' ?></td>
    <td class="b-r color3"><?php echo isset($d['atkartoti']['apgr']) ? format_currency($d['atkartoti']['apgr']) : '' ?></td>

    <td class="c color3"><?php echo isset($d['jauni']['skaits']) ? (is_admin() ? '<a href="?c=ligumi&fligumu_id=' . implode(',', $d['jauni']['ligumu_id']) . '">' . $d['jauni']['skaits'] . '</a>' : $d['jauni']['skaits']) : '' ?></td>
    <td class="b-r color3"><?php echo isset($d['jauni']['apgr']) ? format_currency($d['jauni']['apgr']) : '' ?></td>

    <td class="c color3"><?php echo isset($d['atjaunoti']['skaits']) ? (is_admin() ? '<a href="?c=ligumi&fligumu_id=' . implode(',', $d['atjaunoti']['ligumu_id']) . '">' . $d['atjaunoti']['skaits'] . '</a>' : $d['atjaunoti']['skaits']) : '' ?></td>
    <td class="b-r color3"><?php echo isset($d['atjaunoti']['apgr']) ? format_currency($d['atjaunoti']['apgr']) : '' ?></td>

    <td title="atkārtotie + jaunie + atjaunotie" class="c color5"><?php echo isset($d['kopa']['skaits']) ? (is_admin() ? '<a href="?c=ligumi&fligumu_id=' . implode(',', $d['kopa']['ligumu_id']) . '">' . $d['kopa']['skaits'] . '</a>' : $d['kopa']['skaits']) : '' ?></td>
    <td class="color5"><?php echo isset($d['kopa']['apgr']) ? format_currency($d['kopa']['apgr']) : '' ?></td>

  </tr>

  <tr class="<?php echo $tr_class ?>">

    <td class="<?php echo $i == 'total' ? 'color1' : '' ?>">&nbsp;</td>
    <td class="b-r <?php echo $i == 'total' ? 'color1' : '' ?> "></td>

    <td title="% no atdoto līgumu skaita" class="<?php echo $i == 'total' ? 'color2' : '' ?> "><?php echo isset($d['apstradati']['proc_atdot_sk']) ? round($d['apstradati']['proc_atdot_sk'], 1) . '%' : '' ?></td>
    <td title="% no atdoto līgumu apgrozījuma" class="b-r <?php echo $i == 'total' ? 'color2' : '' ?> "><?php echo isset($d['apstradati']['proc_atdot_apgr']) ? round($d['apstradati']['proc_atdot_apgr'], 1) . '%' : '' ?></td>

    <td title="% no atdoto līgumu skaita" class="<?php echo $i == 'total' ? 'color2' : '' ?> "><?php echo isset($d['neaiztikti']['proc_atdot_sk']) ? round($d['neaiztikti']['proc_atdot_sk'], 1) . '%' : '' ?></td>
    <td title="% no atdoto līgumu apgrozījuma" class="b-r <?php echo $i == 'total' ? 'color2' : '' ?> "><?php echo isset($d['neaiztikti']['proc_atdot_apgr']) ? round($d['neaiztikti']['proc_atdot_apgr'], 1) . '%' : '' ?></td>

    <td title="% no atdoto līgumu skaita" class="<?php echo $i == 'total' ? 'color4' : '' ?> "><?php echo isset($d['zaudeti']['proc_atdot_sk']) ? round($d['zaudeti']['proc_atdot_sk'], 1) . '%' : '' ?></td>
    <td title="% no atdoto līgumu apgrozījuma" class="b-r <?php echo $i == 'total' ? 'color4' : '' ?> "><?php echo isset($d['zaudeti']['proc_atdot_apgr']) ? round($d['zaudeti']['proc_atdot_apgr'], 1) . '%' : '' ?></td>

    <td title="% no atdoto līgumu skaita" class="<?php echo $i == 'total' ? 'color3' : '' ?> "><?php echo isset($d['atkartoti']['proc_atdot_sk']) ? round($d['atkartoti']['proc_atdot_sk'], 1) . '%' : '' ?></td>
    <td title="% no atdoto līgumu apgrozījuma" class="b-r <?php echo $i == 'total' ? 'color3' : '' ?> "><?php echo isset($d['atkartoti']['proc_atdot_apgr']) ? round($d['atkartoti']['proc_atdot_apgr'], 1) . '%' : '' ?></td>

    <td title="% no kopējo skaita" class="<?php echo $i == 'total' ? 'color3' : '' ?> "><?php echo isset($d['jauni']['proc_kopa_sk']) ? round($d['jauni']['proc_kopa_sk'], 1) . '%' : '' ?></td>
    <td title="% no kopējā apgrozījuma" class="b-r <?php echo $i == 'total' ? 'color3' : '' ?> "><?php echo isset($d['jauni']['proc_kopa_apgr']) ? round($d['jauni']['proc_kopa_apgr'], 1) . '%' : '' ?></td>

    <td title="% no kopējo skaita" class="<?php echo $i == 'total' ? 'color3' : '' ?> "><?php echo isset($d['atjaunoti']['proc_kopa_sk']) ? round($d['atjaunoti']['proc_kopa_sk'], 1) . '%' : '' ?></td>
    <td title="% no kopējā apgrozījuma" class="b-r <?php echo $i == 'total' ? 'color3' : '' ?> "><?php echo isset($d['atjaunoti']['proc_kopa_apgr']) ? round($d['atjaunoti']['proc_kopa_apgr'], 1) . '%' : '' ?></td>

    <td title="% no atdoto līgumu skaita" class="color5 <?echo isset($d['kopa']['proc_atdot_sk']) ? (intval($d['kopa']['proc_atdot_sk']) < 100 ? 'not_achieved' : 'achieved') : '' ?>"><?php echo isset($d['kopa']['proc_atdot_sk']) ? round($d['kopa']['proc_atdot_sk'], 1) . '%' : '' ?></td>
    <td title="% no atdoto līgumu apgrozījuma" class="color5 <?echo isset($d['kopa']['proc_atdot_apgr']) ? (intval($d['kopa']['proc_atdot_apgr']) < 100 ? 'not_achieved' : 'achieved') : '' ?>"><?php echo isset($d['kopa']['proc_atdot_apgr']) ? round($d['kopa']['proc_atdot_apgr'], 1) . '%' : '' ?></td>

  </tr>

  <tr class="bottom <?php echo !empty($options['month_rows']) ? 'bottom_month-' . $i : 'bottom_quarter-' . $i ?> <?php echo $tr_class ?>">

    <td class="<?php echo $i == 'total' ? 'color1' : '' ?> ">&nbsp;</td>
    <td title="vidējā līguma summa" class="b-r <?php echo $i == 'total' ? 'color1' : '' ?> "><?php echo isset($d['atdoti']['vid_summa']) ? format_currency($d['atdoti']['vid_summa']) : '' ?></td>

    <td class="<?php echo $i == 'total' ? 'color2' : '' ?> "></td>
    <td title="vidējā apstrādātā līguma summa" class="b-r <?php echo $i == 'total' ? 'color2' : '' ?> "><?php echo isset($d['apstradati']['vid_summa']) ? format_currency($d['apstradati']['vid_summa']) : '' ?></td>

    <td class="<?php echo $i == 'total' ? 'color2' : '' ?> "></td>
    <td title="vidējā neaiztiktā līguma summa" class="b-r <?php echo $i == 'total' ? 'color2' : '' ?> "><?php echo isset($d['neaiztikti']['vid_summa']) ? format_currency($d['neaiztikti']['vid_summa']) : '' ?></td>

    <td class="<?php echo $i == 'total' ? 'color4' : '' ?> "></td>
    <td title="vidējā zaudētā līguma summa" class="b-r <?php echo $i == 'total' ? 'color4' : '' ?> "><?php echo isset($d['zaudeti']['vid_summa']) ? format_currency($d['zaudeti']['vid_summa']) : '' ?></td>

    <td class="<?php echo $i == 'total' ? 'color3' : '' ?> "></td>
    <td title="vidējā atkārtotā līguma summa" class="b-r <?php echo $i == 'total' ? 'color3' : '' ?> "><?php echo isset($d['atkartoti']['vid_summa']) ? format_currency($d['atkartoti']['vid_summa']) : '' ?></td>

    <td class="<?php echo $i == 'total' ? 'color3' : '' ?> "></td>
    <td title="vidējā jaunā līguma summa" class="b-r <?php echo $i == 'total' ? 'color3' : '' ?> "><?php echo isset($d['jauni']['vid_summa']) ? format_currency($d['jauni']['vid_summa']) : '' ?></td>

    <td class="<?php echo $i == 'total' ? 'color3' : '' ?> "></td>
    <td title="vidējā atjaunotā līguma summa" class="b-r <?php echo $i == 'total' ? 'color3' : '' ?> "><?php echo isset($d['atjaunoti']['vid_summa']) ? format_currency($d['atjaunoti']['vid_summa']) : '' ?></td>

    <td class="color5"></td>
    <td title="vidējā kopejā līguma summa" class="color5"><?php echo isset($d['kopa']['vid_summa']) ? format_currency($d['kopa']['vid_summa']) : '' ?></td>

  </tr>
  <?php

}

function get_google_url($kw, $tld) {
	global $GOOGLEURL;
	if (!isset($GOOGLEURL)){
		$GOOGLEURL=array();
	}
	if (!isset($GOOGLEURL[$tld])){
  	if ($tld == 'uk') {
	    $tld = 'co.uk';
	  }

	  $sql = "
  	  SELECT *
    	FROM `".DB_PREF."google_saites`
    	WHERE tld = '".$tld."'
    	LIMIT 1";

  	$google = db_get_assoc(db_query($sql));

  	if (empty($google['url'])) {
  		$GOOGLEURL[$tld]=false;
	  }
		else{
  		$GOOGLEURL[$tld] = $google['url'];
		}
	}
	return str_replace('{kw}', urlencode($kw), $GOOGLEURL[$tld]);

}

function get_google_url_new($kw, $tld) {
	global $GOOGLEURL_NEW;
	if (!isset($GOOGLEURL_NEW)){
		$GOOGLEURL_NEW=array();
	}
	if (!isset($GOOGLEURL_NEW[$tld])){
  	if ($tld == 'uk') {
	    $tld = 'co.uk';
	  }

	  $sql = "
  	  SELECT *
    	FROM `".DB_PREF."google_saites`
    	WHERE tld = '".$tld."'
    	LIMIT 1";

  	$google = db_get_assoc(db_query($sql));
  	if (empty($google['url_new'])) {
  		$GOOGLEURL_NEW[$tld]=false;
  		return false;
	  }
		else{
  		$GOOGLEURL_NEW[$tld]=$google['url_new'];
		}
	}
	return str_replace('{{kw}}', urlencode($kw), $GOOGLEURL_NEW[$tld]);

}


function get_yandex_url($kw, $tld) {
	global $YANDEXURL;
	if (!isset($YANDEXURL)){
		$YANDEXURL=array();
	}
	if (!isset($YANDEXURL[$tld])){
  	$sql = "
	    SELECT *
    	FROM `".DB_PREF."yandex_saites`
    	WHERE tld = '".$tld."'
    	LIMIT 1";

  	$google = db_get_assoc(db_query($sql));

  	if (empty($google['url'])) {
	    $YANDEXURL[$tld]=false;
	  }
		else{
  		$YANDEXURL[$tld]=$google['url'];
		}
	}
  return str_replace('{kw}', urlencode($kw), $YANDEXURL[$tld]);

}

function log_add($darbiba, $apraksts) {

  if (is_array($apraksts)) {

    $content = array();

    foreach($apraksts as $k => $v) {

      $diffs = '';

      if (isset($v['old_m']) && isset($v['new_m'])) {

        $old = array();
        foreach($v['old_m'] as $item) {
          $old[$item['id']] = $item;
        }

        $new = array();
        foreach($v['new_m'] as $item) {
          $new[$item['id']] = $item;
        }

        foreach($new as $item) {
          if (!isset($old[$item['id']])) {
            $diffs .= "Pievienots:\n" . log_data_single($item) . "\n\n";
          } else {
            $diff_tmp = log_data_diff($old[$item['id']], $item, false);

            if ($diff_tmp)
              $diffs .= "Labots:\n" . $diff_tmp . "\n\n";
          }
        }

        foreach($old as $item) {
          if (!isset($new[$item['id']])) {
            $diffs .= "Dzēsts:\n" . log_data_single($item) . "\n\n";
          }
        }

      } else if (isset($v['old_m'])) {

        foreach($v['old_m'] as $item) {
          $diffs .= "Dzēsts:\n" . log_data_single($item) . "\n\n";
        }

      } else if (isset($v['new_m'])) {

        foreach($v['new_m'] as $item) {
          $diffs .= "Pievienots:\n" . log_data_single($item) . "\n\n";
        }

      } else if (isset($v['old']) && isset($v['new'])) {

        $diffs = log_data_diff($v['old'], $v['new'], true) . "\n\n";

      } else if (isset($v['old'])) {

        $diffs = log_data_single($v['old']) . "\n\n";

      } else if (isset($v['new'])) {

        $diffs = log_data_single($v['new']) . "\n\n";

      }

      if (trim($diffs) != '') {
        $content[] = (isset($v['title']) ? $v['title'] : $k) . "\n---\n" . $diffs;
      }

    }

    $content = implode("", $content);

  } else {

    $content = $apraksts;

  }

  if (!empty($content)) {

    db_query("
      INSERT INTO `".DB_PREF."log` SET
        `datums` = NOW(),
        `laiks` = NOW(),
        `ip` = '".$_SERVER['REMOTE_ADDR']."',
        `pardeveja_id` = ".$_SESSION['user']['id'].",
        `darbiba` = '".$darbiba."',
        `saite` = '".esc($_SERVER['REQUEST_URI'])."',
        `apraksts` = '".esc($content)."'
    ");

  }

}

function log_data_diff($old, $new, $only_diff) {

  $items = array();

  $changed = false;

  foreach($old as $k => $v) {

    if ($new[$k] != $v) {
      $changed = true;
      $items[] = sprintf('- %s - NO "%s" UZ "%s"', $k, $v, $new[$k]);
    } else if (!$only_diff) {
      $items[] = sprintf('- %s - "%s"', $k, $v);
    }

  }

  if ($changed)
    return implode("\n", $items);

  return '';

}

function log_data_single($data) {

  $items = array();

  foreach($data as $k => $v) {
      $items[] = sprintf('- %s - "%s"', $k, $v);
  }

  return implode("\n", $items);

}

function print_plana_izpilde_head($new_cols = false) {

  if($new_cols) { ?>

  <thead>

    <tr class="header groups">
      <th rowspan="2" style="font-weight: normal; width: 60px;" class="b-r">Cet.</th>
      <th colspan="2" class="b-r color1">Plānotais</th>
      <th colspan="2" class="b-r color2">Sasniegtais</th>

      <?php if(is_admin()) {?>
        <th colspan="2" class="b-r color3">Rezultāts</th>
      <?php } ?>

      <th colspan="2" class="b-r color3">Līdz prēmijai</th>

      <?php if(isset($_GET['id'])) {?>
        <th colspan="<?=(is_admin() ? '5' : '2');?>" class="color4">Prēmija</th>
      <?php } ?>
    </tr>

    <tr class="header subgroups">
      <th>Līg. skaits</th>
      <th class="b-r">Apgrozījums</th>

      <th>Līg. skaits</th>
      <th class="b-r">Apgrozījums</th>

      <?php if(is_admin()) {?>
        <th class="color3">Līg. skaits</th>
        <th class="b-r color3">Apgrozījums</th>
      <?php } ?>

      <th class="color3">Līg. skaits</th>
      <th class="b-r color3">Apgrozījums</th>

      <?php if(isset($_GET['id'])) { ?>

        <?php if(is_admin()) { ?>
          <th>Virtuālais apgrozījums</th>
          <th>Virs/zem plāna</th>
        <?php } ?>

        <th>Prēmija %</th>
        <th>Prēmija EUR</th>

        <?php if(is_admin()) { ?>
          <th>No līguma</th>
        <?php } ?>

      <?php };?>
    </tr>

  </thead>

  <?php } else {
  ?>
  <thead>

    <tr class="header groups">
      <th rowspan="2" style="font-weight: normal; width: 60px;" class="b-r">Cet.</th>
      <th colspan="2" class="b-r color1">Plānotais</th>
      <th colspan="2" class="b-r color2">Sasniegtais</th>
      <th colspan="2" class="b-r color3">Rezultāts</th>
      <?php if(isset($_GET['id'])) {?>
        <th colspan="<?=(is_admin() ? '5' : '2');?>" class="color4">Prēmija</th>
      <?php } ?>
    </tr>

    <tr class="header subgroups">
      <th>Līg. skaits</th>
      <th class="b-r">Apgrozījums</th>
      <th>Līg. skaits</th>
      <th class="b-r">Apgrozījums</th>
      <th class="color3">Līg. skaits</th>
      <th class="b-r color3">Apgrozījums</th>

      <?php if(isset($_GET['id'])) { ?>
        <th>Apmērs</th>

        <?php if(is_admin()) { ?>
          <th>Plānotais</th>
          <th>Sasniegtais</th>
        <?php } ?>
        <th>Pr. summa</th>
        <?php if(is_admin()) { ?>
          <th>No līg.</th>
        <?php } ?>
      <?php };?>
    </tr>

  </thead>
  <?php
  }

}

function print_plana_izpilde_row($i, $d, $options = array(), $pd = null, $virtual = false, $new_cols = false) {

  global $_vars;

  $roman_numbers = array(
    1 => 'I',
    2 => 'II',
    3 => 'III',
    4 => 'IV',
  );

  $months = $_vars['menesi'];

  $is_quarter_row = empty($options['month_rows']) && $i != 'total' ? true : false;

  if ($i != 'total') {

    list($year, $period) = explode('-', $i);

    if (!empty($options['month_rows']) || !empty($options['pardeveja_rows'])) {

      if ($period <= 3) {
        $q = 1;
      } elseif ($period <= 6) {
        $q = 2;
      } elseif ($period <= 9) {
        $q = 3;
      } elseif ($period <= 12) {
        $q = 4;
      }
      $quarter = $year . '-' . $q;

    }

    if(empty($options['month_rows'])) {
      $q = $period;
     /* var_dump($d['kopa']['skaits']);
      var_dump(count($d['kopa']['ligumu_id_all']));
      echo "<br /><br />"; */
    }

  }

  $tr_class = '';
  if (!empty($options['month_rows']))
    $tr_class = 'month_row';
  else if (!empty($options['pardeveja_rows']))
    $tr_class = 'pardeveja_row';

  if (isset($d['atdoti']['skaits']) && isset($d['real']['skaits'])) {
    if (!empty($d['atdoti']['skaits']))
      $proc_skaits = ($d['real']['skaits'] / $d['atdoti']['skaits']) * 100;
    else if (!empty($d['real']['skaits']))
      $proc_skaits = 100;
    else
      $proc_skaits = 0;
  }

  if (isset($d['atdoti']['apgr']) && isset($d['real']['apgr'])) {
    if (!empty($d['atdoti']['apgr']) && bccomp($d['atdoti']['apgr'], '0', 2) != 0)
      $proc_apgr = ($d['real']['apgr'] / $d['atdoti']['apgr']) * 100;
    else if (!empty($d['real']['apgr']))
      $proc_apgr = 100;
    else
      $proc_apgr = 0;
  }

  ?>
  <tr class="top <?php echo !empty($options['month_rows']) ? 'month_row-' . $quarter : (!empty($options['pardeveja_rows']) ? 'pardeveja_row-' . $i . ' pardeveja_row_quarter-' . $quarter: '') ?>  <?php echo $tr_class ?> <?php echo $i == 'total' ? 'total_row' : '';?>">

    <td class="b-r label">
      <?php if ($i == 'total') { ?>

        Pavisam&nbsp;gadā

      <?php } else { ?>

        <?php if (!empty($options['pardeveja_rows'])) { ?>

          <?php echo $options['pardevejs'] ?>

        <?php } else if (!empty($options['month_rows'])) { ?>

          <?php if (!empty($options['allow_open_month'])) { ?>
            <a href="#" class="open_pardeveji month-<?php echo $i ?>"><?php echo $months[(int)$period] ?></a>
          <?php } else { ?>
            <?php echo $months[(int)$period] ?>
          <?php } ?>

        <?php } else { ?>

          <a href="#" class="open_months quarter-<?php echo $i ?> <?php echo !empty($options['pardeveja_id']) ? 'pardeveja_id-' . $options['pardeveja_id'] : '' ?>"><?php echo $roman_numbers[(int)$period] ?></a>

        <?php } ?>

      <?php } ?>
    </td>

    <?php
    //  Līgumu saraksta route atkarībā no lietotāja tipa
    if(is_admin()) {
      $route = '?c=ligumi';
    } else {
      $route = '?c=darbinieki&a=labot&id=' . $_SESSION['user']['id'] . '&subtab=ligumi';
    }
    ?>

    <td class="c color1"><?php echo isset($d['atdoti']['skaits']) ? (!empty($d['atdoti']['ligumu_id']) ? '<a href="' . $route . '&fligumu_id=' . implode(',', $d['atdoti']['ligumu_id']) . '">' . $d['atdoti']['skaits'] . '</a>' : $d['atdoti']['skaits']) : '&nbsp;' ?></td>
    <td class="b-r color1"><?php echo isset($d['atdoti']['skaits']) ? format_currency($d['atdoti']['apgr'] * ($new_cols ? 1.03 : 1)) : '' ?></td>

    <?php
    $real_key = $virtual || $i == 'total' ? 'real' : 'kopa'; //  Atkarībā no tā vai attēlojam virtuālo vai īsto, mainās tas kurus datus uzskatam par īsto sastniegto.

    //var_dump(implode('","', $d[$real_key]['ligumu_id_all']));die;
    ?>
    <td class="c color2"><?php echo isset($d[$real_key]['skaits']) ? (!empty($d[$real_key]['ligumu_id']) || !empty($d[$real_key]['ligumu_id_all']) ? '<a href="' . $route . '&fligumu_id=' . implode(',', (isset($d[$real_key]['ligumu_id_all']) ? $d[$real_key]['ligumu_id_all'] : $d[$real_key]['ligumu_id'])) . '">' . $d[$real_key]['skaits'] . '</a>' : $d[$real_key]['skaits']) : '' ?></td>
    <td class="b-r color2"><?php echo isset($d[$real_key]['apgr']) ? format_currency($d[$real_key]['apgr']) : '' ?></td>

    <?php if(!$new_cols || is_admin()) { ?>
      <td class="c color3 <?echo isset($proc_skaits) ? (intval($proc_skaits) < 100 ? 'not_achieved' : 'achieved') : '' ?>"><?php echo isset($proc_skaits) ? round($proc_skaits) . '%' : '' ?></td>
      <td class="b-r c color3 <?echo isset($proc_apgr) ? (intval($proc_apgr) < 100 ? 'not_achieved' : 'achieved') : '' ?>"><?php echo isset($proc_apgr) ? round($proc_apgr) . '%' : '' ?></td>
    <?php } ?>

    <?php
    if($new_cols) {
      //  "Līdz prēmijai"
      $lidz_skaitam = $d['atdoti']['skaits'] - $d['real']['skaits'];
      $lidz_apmeram = $d['atdoti']['apgr'] * 1.03 - $d['real']['apgr'];

      if($is_quarter_row) { ?>

        <td class="lidzpremijai c color3 <?php echo $lidz_skaitam > 0 ? 'not_achieved' : ''?>"><?php echo $lidz_skaitam > 0 ? $lidz_skaitam : 0 ?></td>
        <td class="lidzpremijai b-r r color3 <?php echo $lidz_apmeram > 0 ? 'not_achieved' : ''?>" style="padding-left: 10px;"><?php echo $lidz_apmeram > 0 ? format_currency($lidz_apmeram) : '0.00' ?></td>

      <?php } else { ?>

        <td class="c color3"></td>
        <td class="b-r c color3"></td>

      <?php }
    }
    ?>

    <?php
    if(isset($_GET['id'])) {
      if ($i != 'total') {


        //$q_premium = get_quarter_premium($year, isset($q) ? $q : $period, $_GET['id']);
        if($virtual) {
          list($premium, $sum,  $contract_premium) = calculate_premium_virtual_data($year, isset($q) ? $q : $period, $pd, $_GET['id']);
        } else {
          list($premium, $sum,  $contract_premium) = calculate_premium_data($year, isset($q) ? $q : $period, $pd, $_GET['id']);
        }

        //var_dump($year, isset($q) ? $q : $period, $premium );
        if($premium || ($virtual && $premium !== false)) {
          //  Ir prēmija

          /*$overhead = bcsub($pd['kopa']['apgr'], $pd['atdoti']['apgr'], 2);
          $q_sum = bcmul($overhead, bcdiv($q_premium, 100, 2), 2);
          $contract_premium = bcmul(bcdiv($q_sum, $pd['kopa']['apgr'], 4), 100, 2);*/

          if(!is_new_premium_sys(array('year' => $year, 'quarter' => $q))) {
            if(isset($q)) {
              if(is_premium_condition_met($d)) {
                $mprem = 5;
              } else {
                $mprem = 0;
              }
            }
          }

          $prem_col = isset($mprem) ? $mprem : $premium;

          if(isset($q) && is_new_premium_sys(array('year' => $year, 'quarter' => $q))) {

           //  Jaunā  sistēma

            //if($is_quarter_row) {var_dump($pd['kopa']['ligumu_id']);echo'<br /><br /><br />';}
            print_plana_izpilde_premium_cols($prem_col, $pd['atdoti']['apgr'], $pd['kopa']['apgr'], $sum, $contract_premium, $new_cols, $is_quarter_row);

          } else {

            // Vecā sistēma
            $kj = !isset($q) ? $sum : null;
            $kp = !isset($q) ? $contract_premium : null;
            print_plana_izpilde_premium_cols($prem_col, $pd['atdoti']['apgr'], $pd['kopa']['apgr'], $kj, $kp);

          }
        } else {
          print_plana_izpilde_premium_cols(null);
        }
      } else {
        //  Total
        $year = isset($_GET['izpilde_year']) ? $_GET['izpilde_year'] : date('Y');

        if(!$new_cols) {
          $premiums = array();
          $contract_premiums = array();
          $total_planned = 0;
          $total_comleted = 0;
          $total_sum = 0;
          for($i = 1; $i <= 4; $i++) {
             $pdi = $pd[$year.'-'.$i];  //  pd ir quarter data masīvs nevis total
             list($premium, $sum,  $contract_premium) = calculate_premium_data($year, $i, $pdi, $_GET['id']);
             if($premium) {
               $premiums[] = $premium;
               $contract_premiums[] = $contract_premium;
               $total_planned += $pdi['atdoti']['apgr'];
               $total_comleted += $pdi['kopa']['apgr'];
               $total_sum += $sum;

             }
          }

          if(!empty($premiums)) {
            $avg_premium = array_sum($premiums)/count($premiums);
            $avg_cotract_premium = array_sum($contract_premiums)/count($contract_premiums);
            //print_plana_izpilde_premium_cols($avg_premium, $total_planned, $total_comleted, $total_sum, $avg_cotract_premium);
            print_plana_izpilde_premium_cols(null, $total_planned, $total_comleted, $total_sum, null, null, $new_cols);
          } else {
            print_plana_izpilde_premium_cols(null);
          }
        } else {
          //  Hakotie totali
          print_plana_izpilde_premium_cols(null, $pd['atdoti']['apgr'], $pd['kopa']['apgr'], null, null, $new_cols);
        }
      }
    }
  ?>
  </tr>
  <?php
}

function calculate_premium_virtual_data($year, $quarter, $d, $merchant_id)
{
  return calculate_premium_data($year, $quarter, $d, $merchant_id, true);
}

function calculate_premium_data($year, $quarter, $d, $merchant_id, $virtual = false) {

  if($virtual) {
    if(is_admin()) {
      $q_premium = get_quarter_premium_virtual($year, $quarter, $merchant_id);
    } else {
      $q_premium = 5;
    }
  } else {
    $q_premium = get_quarter_premium($year, $quarter, $merchant_id);
  }

  if($q_premium) {
    //  Ir prēmija

    $is_new_sys = is_new_premium_sys(array('year' => $year, 'quarter' => $quarter));

    if($is_new_sys) {

      $virt_apgr_from_atdoti = $d['atdoti']['apgr'] * 1.03;

      if($virtual && !is_admin() && $virt_apgr_from_atdoti > $d['kopa']['apgr']) {
      //if($virtual && !is_admin()) {
        $sum = $virt_apgr_from_atdoti;
      } else {
        $sum = $d['kopa']['apgr'];
      }

      //die($sum);
    } else {
      $sum = $d['kopa']['apgr'] - $d['atdoti']['apgr']; // kopa - atdoti = parpalikums
    }

    $q_sum = $sum * ($q_premium/100); // parpalikums * (premija % / 100) = ideālā premija

    if($is_new_sys) {
      $contract_premium = $q_premium;
    } else {
      $contract_premium = ($q_sum / $d['kopa']['apgr']) * 100; // ideālā premija / kopā * 100
    }

    return array($q_premium, round($q_sum, 2), round($contract_premium, 6));
  } else {
    if($virtual && $q_premium !== false) {
      return array(0,0,0);
    } else {
      return array(false,0,0);
    }
  }

}

function print_plana_izpilde_premium_cols($premium, $planned = null, $completed = null, $sum = null, $contract_premium = null, $new_cols = false, $is_quarter_row = false) {
  if(!$new_cols) {

    ?>
    <td class="color4 c"><?php echo $premium !== null ? number_format($premium, 2) . '%' : '&nbsp;';?></td>

    <?php if(is_admin()) { ?>
      <td class="color4"><?php echo $planned !== null ? format_currency($planned) : '&nbsp;';?></td>
      <td class="color4"><?php echo $completed !== null ? format_currency($completed) : '&nbsp;';?></td>
    <?php } ?>

      <td class="color4"><?php echo $sum !== null ? format_currency($sum) : '&nbsp;';?></td>

    <?php if(is_admin()) { ?>
      <td class="color4 c"><?php echo $contract_premium !== null ? number_format($contract_premium, 2) . '%' : '&nbsp;';?></td>
    <?php }
  } else {

    //  Jaunās kolonnas
    ?>

    <?php if(is_admin()) { ?>
      <td class="color4"><?php echo $completed !== null ? format_currency($completed) : '&nbsp;';?></td>
      <?php
      $delta = round($completed, 2) - round($planned * 1.03, 2);
      ?>
      <td class="color4 <?php echo round($delta, 2) >= 0 ? 'positive' : 'negative';?>"><?php echo format_currency($delta);?></td>
    <?php } ?>

    <td class="color4 c"><?php echo $is_quarter_row && $premium !== null ? number_format($premium, 2) . '%' : '&nbsp;';?></td>
    <td class="premiumsum color4 <?php echo round($sum, 2) > 0 ? 'positive' : 'negative';?>"><?php echo $is_quarter_row && $sum !== null ? format_currency($sum) : '&nbsp;';?></td>

    <?php if(is_admin()) { ?>
      <td class="color4 c"><?php echo $is_quarter_row && $contract_premium !== null ? number_format($contract_premium, 2) . '%' : '&nbsp;';?></td>
    <?php }
  }
}

function get_plana_izpildes_stats($sel_year, $pardevejam = null, $without_piedzina = false) {

  global $_vars;

  $data_raw = array();

  if (empty($pardevejam)) {

    $sql = "
      SELECT p.*
      FROM `".DB_PREF."pardeveji` p
      WHERE p.vaditajs = 0 AND tips != 'partner'
    ";

    $pardeveji = array();

    foreach(db_get_assoc_all(db_query($sql)) as $row) {
      $pardeveji[$row['id']] = $row;
    }

    foreach($_vars['sys_pardeveji_sales_only'] as $pardeveja_id => $pardevejs) {

      $from = '01';
      $to = '12';

      $aktivs_no_y = !empty($pardeveji[$pardeveja_id]['aktivs_no']) ? date('Y', strtotime($pardeveji[$pardeveja_id]['aktivs_no'])) : false;
      $aktivs_lidz_y = !empty($pardeveji[$pardeveja_id]['aktivs_lidz']) ? date('Y', strtotime($pardeveji[$pardeveja_id]['aktivs_lidz'])) : false;

      if (!$aktivs_no_y || $aktivs_no_y <= $sel_year) {

        if (!$aktivs_lidz_y || $aktivs_lidz_y >= $sel_year) {

          if ($aktivs_no_y && $aktivs_no_y == $sel_year) {
            $from = date('m', strtotime($pardeveji[$pardeveja_id]['aktivs_no']));
          }

          if ($aktivs_lidz_y && $aktivs_lidz_y == $sel_year) {
            $to = date('m', strtotime($pardeveji[$pardeveja_id]['aktivs_lidz']));
          }

          $data_raw[$pardeveja_id] = get_statistika('month', $sel_year.'-'.$from, $sel_year.'-'.$to, $pardeveja_id, false, true, $without_piedzina);

        }

      }

    }

  } else {
    $data_raw[$pardevejam] = get_statistika('month', $sel_year.'-1', $sel_year.'-12', $pardevejam, false, true, $without_piedzina);
  }


  $sql = "
    SELECT i.*
    FROM `".DB_PREF."pardeveji_min_izpilde` i
    ORDER BY pardeveja_id ASC, sakot_no DESC
  ";

  $min_izpildes = array();

  foreach(db_get_assoc_all(db_query($sql)) as $row) {
    $min_izpildes[$row['pardeveja_id']][] = $row;
  }


  $quarter_data = array();
  $month_data = array();
  $pardeveju_data = array();
  $total_data = array();

  $fields = array(
    'atdoti' => array('skaits' => 0, 'apgr' => 0, 'ligumu_id' => array()),
    'kopa' => array('skaits' => 0, 'apgr' => 0, 'ligumu_id' => array()),
  );

  for($i = 1; $i <= 4; $i ++) {
    $quarter_data[$sel_year.'-'.$i] = $fields;
  }

  for($i = 1; $i <= 12; $i ++) {

    if ($i <= 3) {
      $quarter = $sel_year.'-1';
    } elseif ($i <= 6) {
      $quarter = $sel_year.'-2';
    } elseif ($i <= 9) {
      $quarter = $sel_year.'-3';
    } elseif ($i <= 12) {
      $quarter = $sel_year.'-4';
    }

    $month_data[$quarter][$sel_year.'-'.str_pad($i, 2, '0', STR_PAD_LEFT)] = $fields;
  }

  $total_data = $fields;

  foreach($data_raw as $pardeveja_id => $pardeveja_data) {

    foreach($pardeveja_data as $period => $period_data) {

      list($year, $month) = explode('-', $period);
      $correct_period = $year.'-'.str_pad($month, 2, '0', STR_PAD_LEFT);

      if (!empty($min_izpildes[$pardeveja_id])) {
        foreach($min_izpildes[$pardeveja_id] as $min) {

          if ($correct_period >= substr($min['sakot_no'], 0, 7)) {

            if ($min['lig_skaits'] > $period_data['atdoti']['skaits']) {
              $period_data['atdoti']['skaits'] = $min['lig_skaits'];
            }

            if ($min['lig_apgroz'] > $period_data['atdoti']['apgr']) {
              $period_data['atdoti']['apgr'] = $min['lig_apgroz'];
            }

            break;

          }

        }
      }

      if ($month <= 3) {
        $quarter = $year.'-1';
      } elseif ($month <= 6) {
        $quarter = $year.'-2';
      } elseif ($month <= 9) {
        $quarter = $year.'-3';
      } elseif ($month <= 12) {
        $quarter = $year.'-4';
      }

      if (is_array($period_data)) {
        foreach($period_data as $col => $col_val) {

          if (in_array($col, array('atdoti', 'kopa'))) {

            $quarter_data[$quarter][$col]['skaits'] += $col_val['skaits'];
            $quarter_data[$quarter][$col]['apgr'] += $col_val['apgr'];
            $quarter_data[$quarter][$col]['ligumu_id'] = array_unique(array_merge($quarter_data[$quarter][$col]['ligumu_id'], $col_val['ligumu_id']));

            $month_data[$quarter][$period][$col]['skaits'] += $col_val['skaits'];
            $month_data[$quarter][$period][$col]['apgr'] += $col_val['apgr'];
            $month_data[$quarter][$period][$col]['ligumu_id'] = array_unique(array_merge($month_data[$quarter][$period][$col]['ligumu_id'], $col_val['ligumu_id']));

            if (!isset($pardeveju_data[$period][$pardeveja_id][$col]['skaits'])) $pardeveju_data[$period][$pardeveja_id][$col]['skaits'] = 0;
            if (!isset($pardeveju_data[$period][$pardeveja_id][$col]['apgr'])) $pardeveju_data[$period][$pardeveja_id][$col]['apgr'] = 0;
            if (!isset($pardeveju_data[$period][$pardeveja_id][$col]['ligumu_id'])) $pardeveju_data[$period][$pardeveja_id][$col]['ligumu_id'] = array();

            $pardeveju_data[$period][$pardeveja_id][$col]['skaits'] += $col_val['skaits'];
            $pardeveju_data[$period][$pardeveja_id][$col]['apgr'] += $col_val['apgr'];
            $pardeveju_data[$period][$pardeveja_id][$col]['ligumu_id'] = array_unique(array_merge($pardeveju_data[$period][$pardeveja_id][$col]['ligumu_id'], $col_val['ligumu_id']));

            $total_data[$col]['skaits'] += $col_val['skaits'];
            $total_data[$col]['apgr'] += $col_val['apgr'];

          }

        }


      }

      if (empty($pardeveju_data[$period][$pardeveja_id]['atdoti']['skaits']) && empty($pardeveju_data[$period][$pardeveja_id]['kopa']['skaits'])) {
        unset($pardeveju_data[$period][$pardeveja_id]);
      }
      if(isset($pardeveju_data[$period][$pardeveja_id]['atdoti']['apgr'])) {
        $pardeveju_data[$period][$pardeveja_id]['atdoti']['apgr'] *= 1.03;
      }
    }
  }

  $quarters = array_keys($quarter_data);
  foreach((array)$quarters as $quarter) {
    $quarter_data[$quarter]['atdoti']['apgr'] *= 1.03;

    $months = array_keys($month_data[$quarter]);
    foreach((array)$months as $month) {
      $month_data[$quarter][$month]['atdoti']['apgr'] *= 1.03;
    }
  }

  $total_data['atdoti']['apgr'] *= 1.03;

  $month_data[$quarter][$period]['atdoti']['apgr'] *= 1.03;

  return array(
    'quarter_data' => $quarter_data,
    'month_data' => $month_data,
    'pardeveju_data' => $pardeveju_data,
    'total_data' => $total_data
  );

}



/**
 * Aktīvo atgādinājumu skaits userim
 * @id int Lietotāja id
 */
function get_active_reminder_count($id)
{
  $akt_atg_skaits = db_get_val(db_query("
    SELECT COUNT(*)
    FROM `".DB_PREF."atgadinajumi` a
    LEFT JOIN `".DB_PREF."ligumi` g ON (a.liguma_id = g.id)
    WHERE
      a.statuss = 1 AND
      a.datums <= CURDATE() AND
      (
        a.pardeveja_id = ".$id." OR
        g.pardeveja_id = ".$id." OR
        g.id IN (
          SELECT liguma_id
          FROM `".DB_PREF."ligumi_saistitie_pardeveji`
          WHERE pardeveja_id = ".$id."
        )
      ) AND
      a.`redzamiba` IN ('kopejs', 'pardevejs')
  "));
  return $akt_atg_skaits;
}

/**
 * Atlasa visus unikālos infomedia pakalpojuma atslēgvārdus.
 * Skatās tikai aktīvajiem līgumiem
 *
 * @return array atslēgvārdu tabulas rindas
 */
function get_unique_keywords($order = null, $direction = null, $filter = array())
{
  /*$query = "SELECT p.atslegvards, p.liguma_id, COUNT(p.id) AS skaits, g.*
          FROM `".DB_PREF."pielikumi_infomedia` p
          LEFT JOIN `".DB_PREF."ligumi` g ON g.id = p.liguma_id
          GROUP BY p.atslegvards
          HAVING skaits = 1
          ORDER BY p.atslegvards ASC"; */

  /*  Filtri  */
  $where = array();

  //  Līguma nosaukums
  if(isset($filter['nosaukums']) && $filter['nosaukums']) {
    $where[] = "g.nosaukums LIKE '%".esc($filter['nosaukums'])."%'";
  }

  //  Atslēgvārds
  if(isset($filter['atslegvards']) && $filter['atslegvards']) {
    $where[] = "p.atslegvards LIKE '%".esc($filter['atslegvards'])."%'";
  }

  //  Līguma beigu datums
  if(isset($filter['reklama_lidz']) && $filter['reklama_lidz']) {
    $timestamp = strtotime($filter['reklama_lidz']);
    $where[] = "DATE(g.reklama_lidz) >= '".date('Y-m-d', $timestamp)."'";
  }

  //  Nozare id
  if(isset($filter['nozare_id']) && !empty($filter['nozare_id'])) {
    $where[] = 'p.nozare_id IN ('.implode(', ', $filter['nozare_id']).')';
  }

  if(!empty($where)) {
    $where = 'WHERE ' . implode(' AND ', $where);
  } else {
    $where = '';
  }


  $query = "SELECT p.id as pid, p.atslegvards, p.liguma_id, p.nozare_id, g.*, n.title_lv as nozare
          FROM `".DB_PREF."pielikumi_infomedia` p
          LEFT JOIN `".DB_PREF."ligumi` g ON g.id = p.liguma_id
          LEFT JOIN `".DB_PREF."nozares` n ON n.id = p.nozare_id
          " . $where . "
          ORDER BY " . $order . " " . $direction;
  $result = db_query($query);

  $unikalie_av = array();
  $existing = array();

  while($row = db_get_assoc($result)){
    //  Pārbaudam vai līgums aktīvs
    $query_rekini = db_query("
      SELECT r.*
      FROM `".DB_PREF."rekini` as r
      WHERE liguma_id = " . $row['liguma_id']
    );
    $rekini = array();
    while($rekini_row = db_get_assoc($query_rekini)) {
      $rekini[] = $rekini_row;
    }

    $statuss = get_liguma_statuss($row, $rekini);

    if($statuss == 3) { // 3: aktīvs
      //  Ir aktīvs a/v

      if(!in_array($row['atslegvards'], $existing)) {
        // Skatāmies vai šāds jau eksistē pie aktīvajiem. Ja eksistē tad ņemam laukā un liekam atslēgvārdu blacklistē; ja neeksistē - liekam iekšā.
        $count = count($unikalie_av);
        $exists = false;
        for($i = 0; $i < $count; $i++) {
          if($unikalie_av[$i]['atslegvards'] == $row['atslegvards']) {
            unset($unikalie_av[$i]);
            $exists = true;
            break;
          }
        }
        if($exists) {
          $existing[] = $row['atslegvards'];
          $unikalie_av = array_values($unikalie_av);
        } else {
          $unikalie_av[] = $row;
        }
      }
    }
  }
  return $unikalie_av;
}

/**
 * Pārbauda vai konkrētais infomedia atslēgvārds ir unikāls.
 * Skatās tikai aktīvajiem līgumiem.
 *
 * @param string $keyword - atslegvārds
 * @return int 0: neeksistē, 1: ir unikāls, 2: nav unikāls
 */
 function keyword_exists($keyword)
 {
    $exists = 0;

    $query = "SELECT p.atslegvards, p.liguma_id, g.*
      FROM `".DB_PREF."pielikumi_infomedia` p
      LEFT JOIN `".DB_PREF."ligumi` g ON g.id = p.liguma_id
      WHERE p.atslegvards = '" . esc(trim($keyword)) . "'";

    $result = db_query($query);

    $unikalie_av = array();

    while($row = db_get_assoc($result)){
      //  Pārbaudam vai līgums aktīvs
      $query_rekini = db_query("
        SELECT r.*
        FROM `".DB_PREF."rekini` as r
        WHERE liguma_id = " . $row['liguma_id']
      );
      $rekini = array();
      while($rekini_row = db_get_assoc($query_rekini)) {
        $rekini[] = $rekini_row;
      }

      $statuss = get_liguma_statuss($row, $rekini);

      if($statuss == 3) { // 3: aktīvs
        $exists += 1;
      }

      if($exists == 2) {
        //  Jau zinām, ka nav unikāls - nav vajadzības turpināt
        break;
      }
    }

    return $exists;
}

/**
 * Atrod pēdējo komentāru konkrētam objektam
 * @param int $id - objekta id
 * @param string $type - objekta tips
 * @return mixed false ja nav komentāru, string komentārs ja ir
*/
function get_last_comment($id, $type)
{
  $query = "SELECT k.* FROM `".DB_PREF."komentari` k
    WHERE k.objekta_id = " . esc($id) . " AND k.objekta_tips = '" . esc($type) . "'
    ORDER BY k.created DESC
    LIMIT 0,1";

  $result = db_query($query);

  if(mysql_num_rows($result) > 0) {
    return mysql_fetch_assoc($result);
  }
}

function get_quarter_premium($year, $quarter, $merchant_id)
{
  $premium = db_get_assoc(db_query('SELECT * FROM `' . DB_PREF . 'premijas` WHERE gads = ' . $year . ' AND ceturksnis = ' . $quarter . ' AND pardevejs_id = ' . $merchant_id));
  if(!empty($premium) && $premium['ir_premija']) {
    return $premium['premijas_apmers'];
  }
}

function get_quarter_premium_virtual($year, $quarter, $merchant_id)
{
  $premium = db_get_assoc(db_query('SELECT * FROM `' . DB_PREF . 'premijas_virtual` WHERE gads = ' . $year . ' AND ceturksnis = ' . $quarter . ' AND pardevejs_id = ' . $merchant_id));
  if(!empty($premium)) {
    if($premium['ir_premija']) {
      return $premium['premijas_apmers'];
    } else {
      return 0;
    }
  } else {
    return false;
  }
}

function get_contract_premium($year, $quarter, $merchant_id)
{
  /*
  old
  $quarter_k = $year . '-' . $quarter;
  $stats = get_plana_izpildes_stats($year, $merchant_id, true);  */
  $data = get_izpildits($merchant_id, $year, $quarter, null, true);
  //$stats = array();
  //$stats['quarter_data'] = get_statistika('quarter', $quarter_k, $quarter_k, $merchant_id, false, true, true);
  list($q_premium, $q_sum, $contract_premium) = calculate_premium_data($year, $quarter, $data, $merchant_id);

  return $contract_premium;
}

/**
 * Pārbauda vai izpildās nosacījums
 * Līgumiem jābūt vismaz tikpat cik plānā un apgrozījumam jābūt 110% no plāna
 * Jaunā sistēmā (sākot ar 2012 gada 3 cet) 105%
 *
 * @param array $period_data - statistikas dati
 * @return boolean true, ja izpildās; false, ja neizpildās
 */
function is_premium_condition_met($period_data, $amount_condition = '1.1')
{
  //  Līgumiem jābūt vismaz tikpat cik plānā
  if($period_data['atdoti']['skaits'] <= $period_data['kopa']['skaits']) {

    //var_dump('Ligumi pietiek');
    //  Summai jābūt 110 no tās kas plānā
    if(bccomp($period_data['atdoti']['apgr'], '0', 2) == 1) {  //  plāns > 0
        $completed = round($period_data['kopa']['apgr'] / $period_data['atdoti']['apgr'], 1);
    } elseif(bccomp($period_data['kopa']['apgr'], '0', 2) == 1) { //  plāns == 0 && izpildīts > 0
      $completed = 1;
    } else { // plāns == 0 && izpildīts == 0
      $completed = 0;
    }
    if(bccomp($completed, $amount_condition, 2) != -1) { //-1: sasniegtais/planotais < condition
      //var_dump('izpildās');
      //  Nosacījumi izpildās
      return true;
    } else {
      //var_dump('neizpildās.');
      //var_dump('compl = ' . $completed);
      //  Nosacījumi neizpildās, jo nav sasniegts apgrozījums
      return false;
    }
  } else {
    //var_dump('nepietiek līgumi');
    //  Nosacījumi neizpildās, jo nav sasniegts līgumu skaits
    return false;
  }
}

/**
 * Pārbauda vai datumā jābūt jaunajai prēmiju sistēmai
 * true ja jaunā, false ja vecā
 */
function is_new_premium_sys($args)
{
  $starts = mktime(0,0,0,PREMIJAS_NEW_START_QUEARTER*3-2, 1, PREMIJAS_NEW_START_YEAR);

  if(isset($args['date']) && $args['date']) {

    //  Pilns datums
    $check = strtotime($args['date']);

  } elseif(isset($args['year'], $args['month']) && $args['year'] && $args['month']) {

    //  Gads un mēnesis
    $check = mktime(0,0,0,$args['month'], 1, $args['year']);

  } elseif(isset($args['year'], $args['quarter']) && $args['year'] && $args['quarter']) {

    //  Gads un ceturksnis
    $check = mktime(0,0,0,$args['quarter']*3-2, 1, $args['year']);

  } else {
    throw new Exception('Nepietiek parametru prēmiju sistēmas versijas noteikšanai.');
  }

  if($check >= $starts) {
    return true;
  } else {
    return false;
  }
}

function is_new2_premium_sys($args)
{
  $starts = mktime(0,0,0,PREMIJAS_NEW2_START_QUEARTER*3-2, 1, PREMIJAS_NEW2_START_YEAR);

  if(isset($args['date']) && $args['date']) {

    //  Pilns datums
    $check = strtotime($args['date']);

  } elseif(isset($args['year'], $args['month']) && $args['year'] && $args['month']) {

    //  Gads un mēnesis
    $check = mktime(0,0,0,$args['month'], 1, $args['year']);

  } elseif(isset($args['year'], $args['quarter']) && $args['year'] && $args['quarter']) {

    //  Gads un ceturksnis
    $check = mktime(0,0,0,$args['quarter']*3-2, 1, $args['year']);

  } else {
    throw new Exception('Nepietiek parametru prēmiju sistēmas versijas noteikšanai.');
  }

  if($check >= $starts) {
    return true;
  } else {
    return false;
  }
}

function determine_premium_new_sys($period_data, $premiums_map)
{
  //  Līgumiem jābūt vismaz tikpat cik plānā
  if($period_data['atdoti']['skaits'] <= $period_data['kopa']['skaits']) {

    //  Summai jābūt 110 no tās kas plānā

    $completed = get_apgrozijuma_attiecibu($period_data);

    $premium = 0;
    foreach($premiums_map as $condition => $amount) {
      if(bccomp($condition, $completed, 2) == 1) {
        break;
      } else {
        $premium = $amount;
      }
    }
    return $premium;

  } else {
    //  Nosacījumi neizpildās, jo nav sasniegts līgumu skaits
    return 0;
  }
}

function get_apgrozijuma_attiecibu($period_data)
{
  if(bccomp($period_data['atdoti']['apgr'], '0', 2) == 1) {  //  plāns > 0

    $completed = round($period_data['kopa']['apgr'] / $period_data['atdoti']['apgr'], 2);

  } elseif(bccomp($period_data['kopa']['apgr'], '0', 2) == 1) { //  plāns == 0 && izpildīts > 0

    $completed = 1;

  } else { // plāns == 0 && izpildīts == 0

    $completed = 0;
  }

  return $completed;
}

function get_izpildits_virtual($merchant_id, $year, $quarter = null, $month = null, $without_piedzina = false) {
  return get_izpildits($merchant_id, $year, $quarter, $month, $without_piedzina, true);
}

/**
 * Aprēķina izpildīto noteiktā mēnesī vai ceturksnī vai gadā. Ja nav norādīts mēnesis/ceturksnis
 * @param int $merchant_id - pārdevēja id
 * @param int $year - apskatāmais gads
 * @param int $quarter - ceturksnis. 1-4
 * @param int $month - mēnesis. 1-12
 * @param bool $without_piedzina - vai izfiltrēt laukā tos kas uz piedziņu
 * @return array [summa, līgumi array()]
 */
function get_izpildits($merchant_id, $year, $quarter = null, $month = null, $without_piedzina = false, $virtual = false)
{
  $data = array(
    'kopa' => array(
      'skaits' => 0,
      'apgr' => 0,
      'ligumu_id' => array(),
      'ligumu_id_all' => array()
    ),
    'atdoti' => array(
      'skaits' => 0,
      'apgr' => 0,
      'ligumu_id' => array()
    ),
    'real' => array(
      'skaits' => 0,
      'apgr' => 0,
      'ligumu_id' => array(),
      'ligumu_id_all' => array(),
    )
  );

  //  Līgumi kuri saņemti noteiktā periodā vai arī iepriekšējais līgums beidzies
  // šajā periodā (gadījumā ja iepriekšējais beidzas vēlāk nekā saņemts jaunais)
  // un tam nav visi rēķini anulēti vai uz piedziņu, ja $without_piedzina = true
  $sql = "
    SELECT
      g.id,
      g.neieklaut_sasn_ligumi,
      g.neieklaut_sasn_apgr,
      GREATEST(g.sanemsanas_datums, IFNULL(MAX(gp.reklama_lidz),0), IFNULL(MAX(gb.reklama_lidz),0))  as pdatums,
      (
        SELECT COUNT(*)
        FROM `".DB_PREF."rekini` r
        WHERE
          r.liguma_id = g.id AND
          r.atkapsanas_rekins = 0 AND
          r.anulets = 0 " . ($without_piedzina ? " AND r.nodots_piedz = 0" : "") . "
      ) as rekinu_skaits,
      (
        SELECT SUM(r1.summa)
        FROM `".DB_PREF."rekini` r1
        WHERE
          r1.liguma_id = g.id AND
          r1.atkapsanas_rekins = 0 AND
          r1.anulets = 0 " . ($without_piedzina ? " AND r1.nodots_piedz = 0" : "") . "
      ) as rekinu_summa,
      g.saistits_pie
    FROM `".DB_PREF."ligumi` g
    LEFT JOIN `".DB_PREF."ligumi_beigusies` b ON b.parslegts_lig_nr = g.ligumanr
    LEFT JOIN `".DB_PREF."ligumi` gb ON gb.id = b.liguma_id
    LEFT JOIN `".DB_PREF."ligumi_parslegsanu_rinda` p ON p.jaunais_liguma_id = g.id AND p.jaunais_liguma_id != p.liguma_id
    LEFT JOIN `".DB_PREF."ligumi` gp ON gp.id = p.liguma_id
    WHERE
      g.pardeveja_id = " . esc($merchant_id) . "
      --  AND g.saistits_pie IS NULL
    GROUP BY g.id";

  //  Sagatavojam apskatāmā perioda termiņus
  $sql .= " HAVING (YEAR(pdatums) = " . esc($year);

  if($month) {
    //  Mēnesis
    $sql .= " AND MONTH(pdatums) = " . esc($month);

  } elseif($quarter) {
    //  Ceturksnis
    $sql .= " AND (
      MONTH(pdatums) = " . ((int)$quarter * 3) . " OR
      MONTH(pdatums) = " . ((int)$quarter * 3 - 1) . " OR
      MONTH(pdatums) = " . ((int)$quarter * 3 - 2) . ")";
  }
  $sql .= ") AND rekinu_skaits > 0
  ";

  /*if($quarter == 1) {
    echo $sql;
  }  */

  $result = db_query($sql);

  while($row = db_get_assoc($result)) {

   if(!$row['neieklaut_sasn_ligumi'] || !$row['neieklaut_sasn_apgr']) {

    $data['kopa']['ligumu_id_all'][] = $row['id'];
   }

   // Vai iekļaut skaitā
   if(!$row['neieklaut_sasn_ligumi'] && !$row['saistits_pie']) {

    $data['kopa']['skaits']++;
   }

   // Vai iekļaut apgrozijumā
   if(!$row['neieklaut_sasn_apgr']) {

    $data['kopa']['apgr'] += $row['rekinu_summa'];
    $data['kopa']['ligumu_id'][] = $row['id'];
   }
  }

  /*  Atdotie līgumi. Skat get_statistika funkcijā - šis ir ņemts no tās. */

  //  Iegūstam visu atdoto līgumu id (gan oriģinālos gan piesaistītos)
  $atdoti_ligumu_id = array();

  $sql = "
    SELECT s.liguma_id
    FROM " . DB_PREF."ligumi_saistitie_pardeveji s
    LEFT JOIN " . DB_PREF."ligumi g ON (g.id = s.liguma_id)
    WHERE
      s.id = (
        SELECT s2.id
        FROM " . DB_PREF."ligumi_saistitie_pardeveji s2
        WHERE s2.liguma_id = s.liguma_id
        ORDER BY s2.piesaist_datums ASC, s2.id ASC
        LIMIT 1
      ) AND
      s.pardeveja_id = " . esc($merchant_id) . " AND
      g.reklama_lidz >= s.piesaist_datums
  ";
  $result = db_query($sql);

  while($row = db_get_assoc($result)) {
    if (!in_array($row['liguma_id'], $atdoti_ligumu_id)) {
      $atdoti_ligumu_id[] = $row['liguma_id'];
    }
  }

  $sql = "
    SELECT g.id
    FROM ".DB_PREF."ligumi g
    LEFT JOIN ".DB_PREF."ligumi_saistitie_pardeveji s ON (s.liguma_id = g.id)
    WHERE
      g.pardeveja_id = " . esc($merchant_id) . " AND
      (
        (
          s.id IS NOT NULL AND
          s.id = (
            SELECT s2.id
            FROM ".DB_PREF."ligumi_saistitie_pardeveji s2
            WHERE s2.liguma_id = s.liguma_id
            ORDER BY s2.piesaist_datums ASC, s2.id ASC
            LIMIT 1
          ) AND
          s.piesaist_datums > g.reklama_lidz
        )
        OR
        (
          s.id IS NULL
        )
      )
  ";
  $result = db_query($sql);

  while($row = db_get_assoc($result)) {
    if (!in_array($row['id'], $atdoti_ligumu_id)) {
      $atdoti_ligumu_id[] = $row['id'];
    }
  }

  //  Anulētie
  $anuleti_ligumu_id = array();
  $sql = "
    SELECT
      g.id,
      (
        SELECT COUNT(*)
        FROM ".DB_PREF."rekini r2
        WHERE r2.liguma_id = g.id
      ) AS rekinu_skaits,
      (
        SELECT COUNT(*)
        FROM ".DB_PREF."rekini r3
        WHERE r3.liguma_id = g.id AND r3.anulets = 1
      ) AS anuleto_skaits
    FROM ".DB_PREF."ligumi g
    HAVING
      rekinu_skaits > 0 AND
      rekinu_skaits = anuleto_skaits
  ";

  foreach(db_get_assoc_all(db_query($sql)) as $row) {
    if (!in_array($row['id'], $anuleti_ligumu_id)) {
      $anuleti_ligumu_id[] = $row['id'];
    }
  }

  // Uz piedziņu
  if($without_piedzina) {
    $piedzina_ligumu_id = array();
    $sql = "
      SELECT
        g.id,
        (
          SELECT COUNT(*)
          FROM ".DB_PREF."rekini r2
          WHERE r2.liguma_id = g.id
        ) AS rekinu_skaits,
        (
          SELECT COUNT(*)
          FROM ".DB_PREF."rekini r3
          WHERE r3.liguma_id = g.id AND r3.nodots_piedz = 1
        ) AS piedzina_skaits
      FROM ".DB_PREF."ligumi g
      HAVING
        rekinu_skaits > 0 AND
        rekinu_skaits = piedzina_skaits
    ";

    foreach(db_get_assoc_all(db_query($sql)) as $row) {
      if (!in_array($row['id'], $piedzina_ligumu_id)) {
        $piedzina_ligumu_id[] = $row['id'];
      }
    }
  }

  if(!empty($atdoti_ligumu_id)) {

    $data['real'] = $data['kopa'];

    //  Perioda kritērijs
    if($month) {
      //  Mēnesis
      $data['atdoti'] = get_izpilde_atdoti_period($merchant_id, $year, $month, $atdoti_ligumu_id, $anuleti_ligumu_id, $without_piedzina ? $piedzina_ligumu_id : array());

      //  Virtuālie
      if($virtual) {

        $virtualie = get_izpilde_atdoti_virtual_period($merchant_id, $year, $month, $atdoti_ligumu_id, $anuleti_ligumu_id, $without_piedzina ? $piedzina_ligumu_id : array());

        $data['kopa']['ligumu_id'] = array_merge($data['kopa']['ligumu_id'], $virtualie['ligumu_id']);
        $data['kopa']['apgr'] += $virtualie['apgr'] * 1.03;
        $data['kopa']['skaits'] += $virtualie['skaits'];
      }

    } else {
      if($quarter) {
        //  Ceturksnis
        $months = array($quarter*3 - 2, $quarter*3 - 1, $quarter*3);
      } else {
        //  Gads
        $months = range(1, 12);
      }

      foreach($months as $m) {
        $mdata = get_izpilde_atdoti_period($merchant_id, $year, $m, $atdoti_ligumu_id, $anuleti_ligumu_id, $without_piedzina ? $piedzina_ligumu_id : array());
        $data['atdoti']['ligumu_id']  = array_merge($data['atdoti']['ligumu_id'], $mdata['ligumu_id']);
        $data['atdoti']['apgr'] += $mdata['apgr'];
        $data['atdoti']['skaits'] += $mdata['skaits']; // potenciāla iespēja rasties kļūdām ja gadījumā kāds līgums kaut kad iekrīt vairākos mēnešos.

        //  Virtuālie
        if($virtual) {

          $virtualie = get_izpilde_atdoti_virtual_period($merchant_id, $year, $m, $atdoti_ligumu_id, $anuleti_ligumu_id, $without_piedzina ? $piedzina_ligumu_id : array());

          $data['kopa']['ligumu_id'] = array_merge($data['kopa']['ligumu_id'], $virtualie['ligumu_id']);
          $data['kopa']['apgr'] += $virtualie['apgr'] * 1.03;
          $data['kopa']['skaits'] += $virtualie['skaits'];
        }
      }
    }
  }

  //  2013-11-06 Tagad, ja ir sasniegts plānotais tad atkal slēdzamies atpakaļ uz reālo nevis virtuālo
  if($data['real']['apgr'] >= $data['atdoti']['apgr']) {
    $data['kopa'] = $data['real'];
  }

  return $data;
}

function get_izpilde_atdoti_virtual_period($merchant_id, $year, $month, $atdoti_ids, $anuleti_ids, $piedzina_ids = array())
{
  return get_izpilde_atdoti_period($merchant_id, $year, $month, $atdoti_ids, $anuleti_ids, $piedzina_ids, true);
}

function get_izpilde_atdoti_period($merchant_id, $year, $month, $atdoti_ids, $anuleti_ids, $piedzina_ids = array(), $virtual = false)
{
  $queryPeriod = " = '" . $year . str_pad($month, 2, '0', STR_PAD_LEFT) . "'";

  $sql = "
      SELECT
        g.id,
        g.reklama_lidz,
        g.neradit_atd_ligumi,
        g.neradit_atd_apgr,
        (
          SELECT IFNULL(SUM(r.summa),0)
          FROM ".DB_PREF."rekini r
          WHERE r.liguma_id = g.id AND r.atkapsanas_rekins = 0
        ) AS rekinu_summa,
        (
          SELECT COUNT(*)
          FROM ".DB_PREF."rekini r2
          WHERE r2.liguma_id = g.id
        ) AS rekinu_skaits,
        (
          SELECT COUNT(*)
          FROM ".DB_PREF."rekini r3
          WHERE r3.liguma_id = g.id AND r3.anulets = 1
        ) AS anuleto_skaits,
        (
          SELECT COUNT(*)
          FROM ".DB_PREF."rekini r4
          WHERE r4.liguma_id = g.id AND r4.nodots_piedz = 1
        ) AS piedzina_skaits,
        jaunais_liguma_id
      FROM ".DB_PREF."ligumi g
      LEFT JOIN ".DB_PREF."ligumi_parslegsanu_rinda pr ON pr.liguma_id = g.id
      WHERE
        -- g.neradit_atdotajos = 0 AND
        g.saistits_pie IS NULL AND
        EXTRACT(YEAR_MONTH FROM g.reklama_lidz) " . $queryPeriod . " AND
        g.id IN (" . implode(',', $atdoti_ids) . ")
    ";

    //  Neiekļaut tos kas anulēti
    if (!empty($anuleti_ids)) {
      $sql .= " AND g.id NOT IN (" . implode(',', $anuleti_ids) . ")";
    }

    //  Neiekļaut tos, kas uz piedziņu
    if (!empty($piedzina_ids)) {
      $sql .= " AND g.id NOT IN (". implode(',', $piedzina_ids). ")";
    }

    //  Virtuālie - tikai tos kas ir sākot ar šodienu nākotnē
    /* if($virtual) {
      $sql .= ' AND g.reklama_lidz >= "' . date('Y-m-d') . '"';
      $sql .= ' AND jaunais_liguma_id IS NULL';
    } */

    //echo $sql;

    $result = db_query($sql);

    $data = array(
      'ligumu_id' => array(),
      'apgr' => 0,
      'skaits' => 0,
    );

    $virtual_minus_skaits = 0;
    $virtual_minus_apgr = 0;
    $virtual_minus_ids = array();

    while($row = db_get_assoc($result)) {

     // Pamatlīguma summa
     if(!$row['neradit_atd_apgr'] && isset($row['rekinu_summa'])) {
      $data['apgr'] += $row['rekinu_summa'];
     }

     if(!$row['neradit_atd_ligumi']) {
       $data['ligumu_id'][] = $row['id'];

       // Skaitā liekam iekšā tikai tos kas ir galvenie (nevis saistītie)
       $data['skaits']++;
     }


     // Virtuālie. atskaita nost nokavētos
     if(strtotime($row['reklama_lidz']) < strtotime(date('Y-m-d'))) {

        if(!$row['neradit_atd_ligumi']) {
          $virtual_minus_skaits++;

          $virtual_minus_ids[] = $row['id'];
        }

        if(!$row['neradit_atd_apgr'] && isset($row['rekinu_summa'])) {
          $virtual_minus_apgr += $row['rekinu_summa'];
        }

     }

     // Sasitīto līgumu summa

     $rsql = "SELECT
        g.id,
        g.reklama_lidz,
        g.neradit_atd_ligumi,
        g.neradit_atd_apgr,
        (
          SELECT IFNULL(SUM(r.summa),0)
          FROM ".DB_PREF."rekini r
          WHERE r.liguma_id = g.id AND r.atkapsanas_rekins = 0
        ) AS rekinu_summa
      FROM ".DB_PREF."ligumi g
      WHERE
        -- g.neradit_atdotajos = 0 AND
        g.saistits_pie = " . $row['id'] . "
      ";

      //  Neiekļaut tos saistītos, kas anulēti
      if (!empty($anuleti_ids)) {
        $rsql .= " AND g.id NOT IN (" . implode(',', $anuleti_ids) . ")";
      }

      //  Neiekļaut tos saistītos, kas uz piedziņu
      if (!empty($piedzina_ids)) {
        $rsql .= " AND g.id NOT IN (". implode(',', $piedzina_ids). ")";
      }

     foreach(db_get_assoc_all(db_query($rsql)) as $rrow) {

       if(isset($rrow['rekinu_summa'])) {

         if(!$rrow['neradit_atd_apgr']) {
          $data['apgr'] += $rrow['rekinu_summa'];
         }

         if(!$rrow['neradit_atd_ligumi']) {
           //$data['skaits']++;
           $data['ligumu_id'][] = $rrow['id'];
         }

         // Virtuālie. atskaita nost nokavētos
         if(strtotime($rrow['reklama_lidz']) < strtotime(date('Y-m-d'))) {

          if(!$rrow['neradit_atd_ligumi']) {
            //$virtual_minus_skaits++;
            $virtual_minus_ids[] = $rrow['id'];
          }

          if(!$rrow['neradit_atd_apgr'] && isset($rrow['rekinu_summa'])) {
            $virtual_minus_apgr += $rrow['rekinu_summa'];
          }



         }
       }
     }
    }

    //  Min izpilde
    list($min_skaits, $min_apgr) = get_min_izpilde($merchant_id, $year, $month);

    if($min_skaits > $data['skaits']) {
      $data['skaits'] = $min_skaits;
    }

    if($min_apgr > $data['apgr']) {
       $data['apgr'] = $min_apgr;
    }

    if($virtual) {
      $data['apgr'] -= $virtual_minus_apgr;
      $data['skaits'] -= $virtual_minus_skaits;
      $data['ligumu_id'] = array_diff($data['ligumu_id'], $virtual_minus_ids);
    }

    return $data;
}

/**
 * Atrod minimālo plāna izpildi konkrētajam mēnesim

 @param id $merchant_id - tirgotāja id
 @param string $year - inrteresējošais gads
 @param string $month - interesējošais mēnesis
 @return array [lig skaits, apgr]
 */
function get_min_izpilde($merchant_id, $year, $month) {
  $sql = "
    SELECT lig_skaits, lig_apgroz
    FROM `".DB_PREF."pardeveji_min_izpilde`
    WHERE
      pardeveja_id = " . esc($merchant_id) . " AND
      ((YEAR(sakot_no) = '".esc($year)."' AND MONTH(sakot_no) <= '".esc($month)."') OR YEAR(sakot_no) < '".esc($year)."')
    ORDER BY sakot_no DESC
  ";

  //echo $sql;

  $data = db_get_assoc_all(db_query($sql));

  return isset($data[0]) ? array($data[0]['lig_skaits'], $data[0]['lig_apgroz']) : array(0, 0);
}

function get_izpilde_virtual_historical($merchant_id, $year, $quarter = null, $month = null)
{
  return get_izpilde_historical($merchant_id, $year, $quarter, $month, true);
}

function get_izpilde_historical($merchant_id, $year, $quarter = null, $month = null, $virtual = false)
{
  if($month) {
    $sql = "
      SELECT *
      FROM ".DB_PREF."izpilde_history
      WHERE
        pardevejs_id = " . esc($merchant_id) . " AND
        gads = " . $year . " AND
        menesis = " . $month;
    $history_data = db_get_assoc(db_query($sql));

    if($history_data) {
      return array(
        'kopa' => array(
          'ligumu_id_all' => explode(',',$history_data['kopa_ligumu_ids_all']),
          'ligumu_id' => explode(',',$history_data['kopa_ligumu_ids']),
            'apgr' => $history_data['kopa_apgr'],
            'skaits' => $history_data['kopa_skaits']
        ),
        'atdoti' => array(
          'ligumu_id' => explode(',',$history_data['atdoti_ligumu_ids']),
          'apgr' => $history_data['atdoti_apgr'],
          'skaits' => $history_data['atdoti_skaits']
          ),
      );
    } else {
      return $virtual ? get_izpildits_virtual($merchant_id, $year, $quarter, $month) : get_izpildits($merchant_id, $year, $quarter, $month);
    }
  } else {

    $data = array(
      'kopa' => array(
        'ligumu_id' => array(),
        'ligumu_id_all' => array(),
        'apgr' => 0,
        'skaits' => 0
      ),
      'atdoti' => array(
        'ligumu_id' => array(),
        'apgr' => 0,
        'skaits' => 0
        ),
      'real' => array(
        'ligumu_id' => array(),
        'ligumu_id_all' => array(),
        'apgr' => 0,
        'skaits' => 0
      ),
    );

    if($quarter) {
      $months = array($quarter * 3 - 2, $quarter * 3 - 1, $quarter * 3);
    } else {
      $months = range(1, 12);
    }

    foreach($months as $mo) {
      $sql = "
        SELECT *
        FROM ".DB_PREF."izpilde_history
        WHERE
          pardevejs_id = " . esc($merchant_id) . " AND
          gads = " . $year . " AND
          menesis = " . $mo;
      $history_data = db_get_assoc(db_query($sql));

      if($history_data) {
        $data['kopa']['ligumu_id_all'] = array_merge($data['kopa']['ligumu_id_all'], explode(',', $history_data['kopa_ligumu_ids_all']));
        $data['kopa']['ligumu_id'] = array_merge($data['kopa']['ligumu_id'], explode(',', $history_data['kopa_ligumu_ids']));
        $data['kopa']['apgr'] +=  $history_data['kopa_apgr'];
        $data['kopa']['skaits'] += $history_data['kopa_skaits'];

        $data['atdoti']['ligumu_id'] = array_merge($data['atdoti']['ligumu_id'], explode(',', $history_data['atdoti_ligumu_ids']));
        $data['atdoti']['apgr'] +=  $history_data['atdoti_apgr'];
        $data['atdoti']['skaits'] += $history_data['atdoti_skaits'];
      } else {
        $real = $virtual ? get_izpildits_virtual($merchant_id, $year, null, $mo) : get_izpildits($merchant_id, $year, null, $mo);

        $data['kopa']['ligumu_id_all'] = array_merge($data['kopa']['ligumu_id_all'],$real['kopa']['ligumu_id_all']);
        $data['kopa']['ligumu_id'] = array_merge($data['kopa']['ligumu_id'],$real['kopa']['ligumu_id']);
        $data['kopa']['apgr'] +=  $real['kopa']['apgr'];
        $data['kopa']['skaits'] += $real['kopa']['skaits'];

        $data['real']['ligumu_id_all'] = array_merge($data['real']['ligumu_id_all'],$real['real']['ligumu_id_all']);
        $data['real']['ligumu_id'] = array_merge($data['real']['ligumu_id'],$real['real']['ligumu_id']);
        $data['real']['apgr'] +=  $real['real']['apgr'];
        $data['real']['skaits'] += $real['real']['skaits'];

        $data['atdoti']['ligumu_id'] = array_merge($data['atdoti']['ligumu_id'], $real['atdoti']['ligumu_id']);
        $data['atdoti']['apgr'] +=  $real['atdoti']['apgr'];
        $data['atdoti']['skaits'] += $real['atdoti']['skaits'];
      }
    }

    return $data;
  }
}

function is_quarter_fixed($year, $quarter)
{
  $dynamic_till =  mktime(0, 0, 0, $quarter * 3 + 2, 1, $year);  // + 2
  //$dynamic_till =  mktime(0, 0, 0, $quarter * 3 + 6, 1, $year);  // testešanai
  if(time() < $dynamic_till) {
    return false;
  } else {
    return true;
  }
}

function get_amats($merchant_id, $year, $quarter)
{
  $month = ($quarter * 3) - 2; //  Mēneša numurs
  //$sakot_no = date('Y-m-d', strtotime(mktime(0,0,0,$month, 1, $year)));

  $sql = '
    SELECT a.* FROM '.DB_PREF.'pardeveji_amati as pa
    LEFT JOIN '.DB_PREF.'amati a ON a.id = pa.amats_id
    WHERE
      pa.pardevejs_id = ' . esc($merchant_id) . ' AND
      (
        YEAR(pa.sakot_no) < "' . $year . '" OR
        (YEAR(pa.sakot_no) = "' . $year . '" AND MONTH(pa.sakot_no) <= "' . $month . '")
      )
    ORDER BY pa.sakot_no DESC
    LIMIT 1
  ';

  $row = db_get_assoc(db_query($sql));
  if(!empty($row)) {
    return $row;
  } else {
    return false;
  }
}

function get_vaditaji()
{
  $sql = '
    SELECT * FROM ' . DB_PREF . 'pardeveji v
    WHERE
      v.vaditajs = 1 AND
      v.aktivs = 1
    ORDER BY v.ierobezots ASC
  ';

  return db_get_assoc_all(db_query($sql));
}

function get_bonuss_plan($merchant_id, $year, $quarter = null, $month = null)
{
  /*$sql = '
    SELECT minimalais as min, optimalais as opt
    FROM ' . DB_PREF . 'bonusi_plans
    WHERE
      vaditajs_id = ' . $merchant_id . ' AND
      gads = ' . (int)$year;

  if($quarter) {
    $sql .= ' AND ceturksnis = ' . $quarter;
  } else {
    $sql .= ' AND ceturksnis IS NULL';
  }

  if($month) {
    $sql .= ' AND menesis = ' . (int)$month;
  } else {
    $sql .= ' AND menesis IS NULL';
  }

  $sql .= ' GROUP BY vaditajs_id';  */

  $sql = '
    SELECT IFNULL(SUM(minimalais), 0) as min, IFNULL(SUM(optimalais), 0) as opt
    FROM '.DB_PREF.'bonusi_plans
    WHERE
      vaditajs_id = ' . $merchant_id . ' AND
      gads = ' . (int)$year . '
  ';

  if($quarter) {
    $sql .= ' AND menesis IN (' . ($quarter * 3 - 2) . ', ' . ($quarter * 3 - 1) . ', ' . ($quarter * 3) . ')';
  } /* else {
    $sql .= ' AND ceturksnis IS NULL';
  }  */

  if($month) {
    $sql .= ' AND menesis = ' . (int)$month;
  } /*else {
    $sql .= ' AND menesis IS NULL';
  }      */

  $sql .= ' GROUP BY vaditajs_id';

  return db_get_assoc(db_query($sql));
}

function get_bonuss_premium($merchant_id, $year, $quarter = null, $month = null)
{
  $sql = '
    SELECT min, opt
    FROM ' . DB_PREF . 'bonusi_premijas
    WHERE
      vaditajs_id = ' . $merchant_id . ' AND
      gads = ' . (int)$year;

    if($quarter) {
      $sql .= ' AND ceturksnis = ' . (int) $quarter;
    } else {
      $sql .= ' AND ceturksnis IS NULL';
    }

    if($month) {
      $sql .= ' AND menesis = ' . (int)$month;
    } else {
      $sql .= ' AND menesis IS NULL';
    }

  $data = db_get_assoc(db_query($sql));

  if(!isset($data['min'], $data['opt'])) {
    $data['min'] = 0;
    $data['opt'] = 0;
  }

  return $data;
}

function calculate_bonuss($data)
{
  $result = array(
      'virs_min' => 0,
      'virs_opt' => 0,
      'summa_min' => 0,
      'summa_opt' => 0,
    );

  if(isset($data['summa'], $data['plans_min'], $data['plans_opt'], $data['min_proc'], $data['opt_proc'])) {
    if(bccomp($data['plans_min'], 0, 2) == 1 && bccomp($data['summa'], $data['plans_min'], 2) == 1) {
      //  Minimums ir pārsniegts
      if(bccomp($data['plans_opt'], 0, 2) == 1 && bccomp($data['summa'], $data['plans_opt'], 2) == 1) {
        //  Optimālais ir pārsniegts
        $result['virs_opt'] = $data['summa'] - $data['plans_opt'];
        $result['summa_opt'] = round($result['virs_opt'] * ($data['opt_proc'] / 100), 2);

        $data['summa'] = $data['plans_opt'];
      }
      $result['virs_min'] = $data['summa'] - $data['plans_min'];
      $result['summa_min'] = round($result['virs_min'] * ($data['min_proc'] / 100), 2);
    }
  }

  return $result;
}

function get_bonuss_stats($merchant_id, $year, $nostatic = false, $assoc = false, $force_dynamic = false)
{
  // Saskaitam sasniegto pa mēnešiem
  $months = range(1, 12);
  $sql = '
    SELECT
      MONTH(g.sanemsanas_datums) as month,
      SUM(r.summa) as summa,
      GROUP_CONCAT(g.id SEPARATOR ",") as ligumi,
      GROUP_CONCAT(r.id SEPARATOR ",") AS rekini
    FROM '.DB_PREF.'ligumi g
    LEFT JOIN '.DB_PREF.'rekini r ON r.liguma_id = g.id AND r.atkapsanas_rekins = 0 AND (r.anulets = 0 OR g.ieklaut_pilna_apm = 1)
    WHERE
      (
        g.vaditajs_id = ' . $merchant_id . '
        OR g.pardeveja_id = ' . $merchant_id . '
      ) AND
      YEAR(g.sanemsanas_datums) = ' . $year . '
    GROUP BY month
  ';
  $result = db_query($sql);

  //$mo_live_data = db_get_assoc_all(db_query($sql));

  $mo_live_data = array();
  while($row = db_get_assoc($result)) {
    $mo_live_data[$row['month']] = $row;
  }

  //var_dump($mo_live_data);die;

  //  Gads
  $sql = '
    SELECT summa, ligumi, min_proc, opt_proc, virs_min, virs_opt, summa_min, summa_opt, plans_min, plans_opt, rekini
    FROM '.DB_PREF.'bonusi
    WHERE
      vaditajs_id = ' . $merchant_id . ' AND
      gads = ' . $year . ' AND
      ceturksnis IS NULL AND
      menesis IS NULL
    LIMIT 1
  ';
  $year_stats = db_get_assoc(db_query($sql));

  if(!$year_stats || $force_dynamic) {
    $year_stats['summa'] = 0;
    $year_stats['ligumi'] = '';
    $year_stats['rekini'] = '';

    foreach($mo_live_data as $mo) {
      $year_stats['summa'] += $mo['summa'];
      if($mo['ligumi']) {
        $year_stats['ligumi'][]= $mo['ligumi'];
      }
      $year_stats['rekini'] = array();
      if($mo['rekini']) {
        $year_stats['rekini'][]= $mo['rekini'];
      }

      //  Plāns
      $yplan = get_bonuss_plan($merchant_id, $year);
      $year_stats['plans_min'] = $yplan['min'];
      $year_stats['plans_opt'] = $yplan['opt'];

      //  Procenti
      $yproc = get_bonuss_premium($merchant_id, $year);
      $year_stats['min_proc'] = $yproc['min'];
      $year_stats['opt_proc'] = $yproc['opt'];

      $year_results = calculate_bonuss($year_stats);
      $year_stats['virs_min'] = $year_results['virs_min'];
      $year_stats['virs_opt'] = $year_results['virs_opt'];
      $year_stats['summa_min'] = $year_results['summa_min'];
      $year_stats['summa_opt'] = $year_results['summa_opt'];
    }
    $year_stats['ligumi'] = !empty($year_stats['ligumi']) ? implode(',', $year_stats['ligumi']) : '';
    $year_stats['rekini'] = !empty($year_stats['rekini']) ? implode(',', $year_stats['rekini']) : '';
  } elseif($nostatic) {
    $year_stats = null;
  } else {
    $year_stats['static'] = true;
  }

  // Ceturkšņi
  $quarters_stats = array();
  $quarters = range(1, 4);
  foreach($quarters as $quarter) {

    $sql = '
      SELECT summa, ligumi, min_proc, opt_proc, virs_min, virs_opt, summa_min, summa_opt, plans_min, plans_opt, rekini
      FROM '.DB_PREF.'bonusi
      WHERE
        vaditajs_id = ' . $merchant_id . ' AND
        gads = ' . $year . ' AND
        ceturksnis = ' . $quarter . '
      LIMIT 1
    ';
    $quarter_stats = db_get_assoc(db_query($sql));

    if(!$quarter_stats || $force_dynamic) {
      $quarter_stats['summa'] = 0;
      $quarter_stats['ligumi'] = array();
      $quarter_stats['rekini'] = array();

      $q_months = array($quarter * 3 - 2, $quarter * 3 - 1, $quarter * 3);
      foreach($q_months as $qm) {
        if(isset($mo_live_data[$qm])) {
          $quarter_stats['summa'] += $mo_live_data[$qm]['summa'];
          if($mo_live_data[$qm]['ligumi']) {
            $quarter_stats['ligumi'][] = $mo_live_data[$qm]['ligumi'];
          }
          if($mo_live_data[$qm]['rekini']) {
            $quarter_stats['rekini'][] = $mo_live_data[$qm]['rekini'];
          }
        }
      }

      $quarter_stats['ligumi'] = implode(',',$quarter_stats['ligumi']);
      $quarter_stats['rekini'] = implode(',',$quarter_stats['rekini']);

      //  Plāns
      $qplan = get_bonuss_plan($merchant_id, $year, $quarter);
      $quarter_stats['plans_min'] = $qplan['min'];
      $quarter_stats['plans_opt'] = $qplan['opt'];

      //  Procenti
      $qproc = get_bonuss_premium($merchant_id, $year, $quarter);
      $quarter_stats['min_proc'] = $qproc['min'];
      $quarter_stats['opt_proc'] = $qproc['opt'];

      $quarter_results = calculate_bonuss($quarter_stats);
      $quarter_stats['virs_min'] = $quarter_results['virs_min'];
      $quarter_stats['virs_opt'] = $quarter_results['virs_opt'];
      $quarter_stats['summa_min'] = $quarter_results['summa_min'];
      $quarter_stats['summa_opt'] = $quarter_results['summa_opt'];
    } elseif($nostatic) {
      $quarter_stats = null;
    } else {
      $quarter_stats['static'] = true;
    }
    $quarters_stats[$quarter] = $quarter_stats;
  }

  //  Mēneši
  $months_stats = array();
  foreach($months as $month) {
  //foreach($mo_live_data as $month => $data) {
    $sql = '
      SELECT summa, ligumi, min_proc, opt_proc, virs_min, virs_opt, summa_min, summa_opt, plans_min, plans_opt, rekini
      FROM '.DB_PREF.'bonusi
      WHERE
        vaditajs_id = ' . $merchant_id . ' AND
        gads = ' . $year . ' AND
        menesis = ' . $month . '
      LIMIT 1
    ';
    $month_stats = db_get_assoc(db_query($sql));

    if(!$month_stats || $force_dynamic) {
      if(isset($mo_live_data[$month])) {
        //var_dump($mo_live_data[$month]);
        $month_stats = $mo_live_data[$month];
      } else {
        $month_stats = array(
          'month' => $month,
          'summa' => 0,
          'ligumi' => '',
        );
      }

      //  Plāns
      $mplan = get_bonuss_plan($merchant_id, $year, null, $month);
      $month_stats['plans_min'] = $mplan['min'];
      $month_stats['plans_opt'] = $mplan['opt'];

      //  Procenti
      $mproc = get_bonuss_premium($merchant_id, $year, null, $month);
      $month_stats['min_proc'] = $mproc['min'];
      $month_stats['opt_proc'] = $mproc['opt'];

      $month_results = calculate_bonuss($month_stats);
      $month_stats['virs_min'] = $month_results['virs_min'];
      $month_stats['virs_opt'] = $month_results['virs_opt'];
      $month_stats['summa_min'] = $month_results['summa_min'];
      $month_stats['summa_opt'] = $month_results['summa_opt'];
    } elseif($nostatic) {
      $month_stats = null;
    } else {
      $month_stats['static'] = true;
    }

    $months_stats[$month] = $month_stats;
  }

  if(!$assoc) {
    return array($year_stats, $quarters_stats, $months_stats);
  } else {
    return array(
      'year' => $year_stats,
      'quarters' => $quarters_stats,
      'months' => $months_stats,
    );
  }
}

function get_bonuss_stats_full($merchant_id, $year, $assoc = false)
{
  list($year_stats, $quarters_stats, $months_stats) = get_bonuss_stats($merchant_id, $year);  //  W Static
  list($dyn_year_stats, $dyn_quarters_stats, $dyn_months_stats) = get_bonuss_stats($merchant_id, $year, false, false, true); // Dynamic

  //var_dump($dyn_months_stats);

  //  Year
  list($year_stats['paid'], $year_stats['correction'], $year_stats['balance'], $year_stats['corr_ligumi']) = calculate_bonuss_balance($year_stats, $dyn_year_stats);


  //  Quarters
  foreach($quarters_stats as $quarter => $q_stats) {
    list($quarters_stats[$quarter]['paid'], $quarters_stats[$quarter]['correction'], $quarters_stats[$quarter]['balance'], $quarters_stats[$quarter]['corr_ligumi']) = calculate_bonuss_balance($q_stats, $dyn_quarters_stats[$quarter]);
  }

  //  Months
  foreach($months_stats as $month => $m_stats) {
    list($months_stats[$month]['paid'], $months_stats[$month]['correction'], $months_stats[$month]['balance'], $months_stats[$month]['corr_ligumi']) = calculate_bonuss_balance($m_stats, $dyn_months_stats[$month]);
  }

  if(!$assoc) {
    return array($year_stats, $quarters_stats, $months_stats);
  } else {
    return array(
      'year' => $year_stats,
      'quarters' => $quarters_stats,
      'months' => $months_stats,
    );
  }
}

function calculate_bonuss_balance($static, $dynamic) {
  if(isset($static['static']) && $static['static']) {
    $paid = $static['summa_min'] + $static['summa_opt'];
    $correction = ($dynamic['summa_min'] + $dynamic['summa_opt']) - $paid;

    $ligums_corr = array();
    $static_rekini = explode(',', $static['rekini']);
    $dyn_rekini = explode(',', $dynamic['rekini']);
    $rekini = array_filter(array_diff(array_merge($static_rekini, $dyn_rekini), array_intersect($static_rekini, $dyn_rekini)));

    if(!empty($rekini)) {
      $rows = db_get_assoc_all(db_query('SELECT DISTINCT liguma_id FROM '.DB_PREF.'rekini WHERE id IN ('.implode(',', $rekini).')'));
      foreach((array)$rows as $row) {
        $ligums_corr[] = $row['liguma_id'];
      }
    }
  } else {
    $paid = 0;
    $correction = 0;
    $balance = 0;
    $ligums_corr = array();
  }
  if(isset($dynamic['summa_min'], $dynamic['summa_opt'])) {
    $balance = round(($dynamic['summa_min'] + $dynamic['summa_opt']) - $paid, 2);
  } else {
    $balance = round(0 - $paid, 2);
  }

  return array($paid, $correction, $balance, implode(',', $ligums_corr));
}

function print_bonuss_head() {

  ?>
  <thead>

    <tr class="header groups">
      <th rowspan="3" style="font-weight: normal; width: 60px; vertical-align: middle;" class="b-r">Cet.</th>
      <th colspan="2" class="b-r color1">Plānotais</th>
      <th colspan="2" class="b-r color2">Sasniegtais</th>
      <th colspan="4" class="b-r color4">Bonuss</th>
      <th colspan="4" class="">Bilance</th>
    </tr>

    <tr class="header subgroups">
      <th rowspan="2" style="vertical-align: middle;">MIN</th>
      <th rowspan="2" class="b-r" style="vertical-align: middle;">OPT</th>
      <th rowspan="2" style="vertical-align: middle;">EUR</th>
      <th rowspan="2" style="vertical-align: middle;" class="b-r">%</th>
      <th colspan="2" class="b-r">Virs MIN</th>
      <th colspan="2" class="b-r">Virs OPT</th>
      <th rowspan="2" style="vertical-align: middle;">Kopā<br />aprēķināts</th>
      <th rowspan="2" style="vertical-align: middle;" class="b-r">Kopā<br />izmaksāts</th>
      <!--<th rowspan="2" style="vertical-align: middle;">Izmaksāts</th>-->
      <th rowspan="2" style="vertical-align: middle;">Korekcija</th>
      <th rowspan="2" style="vertical-align: middle;">Tekošā uz izmaksu</th>
    </tr>

    <tr class="header subgroups">
      <th>Summa</th>
      <th class="b-r">Bonuss</th>
      <th>Summa</th>
      <th class="b-r">Bonuss</th>
    </tr>

  </thead>
  <?php

}

function print_bonuss_row($type, $period, $data, $quarter = null, $total = false)
{
  global $_vars;

  $tr_class = '';
  if ($type == 'month') {
    $tr_class = 'month_row month_row-' . $quarter;
  } elseif($type == 'year') {
    $tr_class = 'total_row';
  }

  ?>
  <tr class="top <?php echo $tr_class ?>">

    <td class="b-r label">
      <?php
      if($type == 'year') {
        //  Gads
        echo 'Gadā';
        $input_class = 'always_disabled';
      } elseif($type == 'month') {
        //  Mēnesis
        echo $_vars['menesi'][$period];
        $input_class = check_access('darbinieki-labot-bonuss') ? 'canedit' : 'always_disabled';
      } elseif($type == 'quarter') {
        //  Ceturksnis ?>
        <a href="#" class="open_months quarter-<?php echo $period ?>"><?php echo $_vars['quarters_roman'][(int)$period] ?></a>
        <?php
        $input_class = 'always_disabled';
      }

      $input_class2 = check_access('darbinieki-labot-bonuss') ? 'canedit' : 'always_disabled';
      ?>
    </td>

    <?php
    //  Līgumu saraksta route atkarībā no lietotāja tipa
    if(is_admin()) {
      $route = '?c=ligumi';
    } else {
      $route = '?c=darbinieki&a=labot&id=' . $_SESSION['user']['id'] . '&subtab=ligumi';
    }
    ?>

    <td class="r color1" style="white-space: nowrap; min-width: 135px">
      <input type="text" class="<?php echo $input_class;?>" name="plans[<?php echo $type . '-' . $period;?>][min]" style="width: 70px;" value="<?php echo isset($data['plans_min']) && $data['plans_min'] ? $data['plans_min'] : '';?>" />EUR &nbsp;
      <input type="text" class="<?php echo $input_class2;?>" name="proc[<?php echo $type . '-' . $period;?>][min]" style="width: 30px;" value="<?php echo isset($data['min_proc']) && $data['min_proc'] ? $data['min_proc'] : '';?>" />%
    </td>
    <td class="b-r color1" style="white-space: nowrap;">
      <input type="text" class="<?php echo $input_class;?>" name="plans[<?php echo $type . '-' . $period;?>][opt]" style="width: 70px;" value="<?php echo isset($data['plans_opt']) && $data['plans_opt'] ? $data['plans_opt'] : '';?>" />EUR &nbsp;
      <input type="text" class="<?php echo $input_class2;?>" name="proc[<?php echo $type . '-' . $period;?>][opt]" style="width: 30px;" value="<?php echo isset($data['opt_proc']) && $data['opt_proc'] ? $data['opt_proc'] : '';?>" />%
    </td>

    <td class="color2 r">
      <?php
      if($data['summa']) {
        if($data['ligumi']) {

          $ligumi = array_unique(explode(',', $data['ligumi']));
          ?>
          <a style="padding: 0;" href="<?php echo $route . '&fligumu_id=' . implode(',', $ligumi);?>"><?php echo format_currency($data['summa']);?></a>
          <?php
        } else {
          echo number_format($data['summa'], 2);
        }
      }
      ?>
    </td>

    <?php
      if(isset($data['plans_opt'], $data['summa']) && $data['plans_opt'] && $data['summa']) {
        $rez = (bccomp($data['plans_opt'], 0, 0) != 0 ? round($data['summa'] / $data['plans_opt'] * 100). '%': '');
        $rez_class = $data['summa'] > $data['plans_opt'] ? 'positive' : ($data['summa'] > $data['plans_min'] ? 'average' : 'negative');
      } else {
        $rez_class = '';
        $rez = '';
      }
    ?>
    <td class="b-r c color2 <?php echo $rez_class;?>">
      <?php echo $rez;?>
    </td>

    <td class="color4"><?php echo isset($data['virs_min']) && $data['virs_min'] ? format_currency($data['virs_min']) : '';?></td>
    <td class="color4 b-r"><?php echo isset($data['summa_min']) && $data['summa_min'] ? format_currency($data['summa_min']) : '';?></td>
    <td class="color4"><?php echo isset($data['virs_opt']) && $data['virs_opt'] ? format_currency($data['virs_opt']) : '';?></td>
    <td class="color4 b-r"><?php echo isset($data['summa_opt']) && $data['summa_opt'] ? format_currency($data['summa_opt']) : '';?></td>
    <td class="" style="font-weight: bold;"><?php echo (isset($data['summa_min']) && $data['summa_min']) || (isset($data['summa_opt']) && $data['summa_opt']) ? format_currency(($data['summa_min'] + $data['summa_opt'])) : '';?></td>
    <td class=" b-r" style="font-weight: normal;"><?php echo format_currency($data['paid']);?></td>

    <!--<td><?php echo isset($data['static']) && $data['static'] ? format_currency($data['paid']) : '';?></td>-->
    <td style="font-weight: normal;">
      <?php
        if(isset($data['static']) && $data['static']) {
          if($data['corr_ligumi']) {
            ?>
            <a style="padding: 0;" href="<?php echo $route . '&fligumu_id=' . $data['corr_ligumi'];?>"><?php echo format_currency($data['correction']);?></a>
            <?php
          } else {
            echo bccomp($data['correction'], 0, 2) == 1 ? format_currency($data['correction']) : '';
          }
        } ?>
    </td>
    <td style="font-weight: bold;"><?php echo $total !== false ? format_currency($total) : '';?></td>
  </tr>
  <?php
}

function cleanup_bonuss($bstats, $gads){
  $rows = array();
  //var_dump($gads);
  foreach($bstats as $type => $bdata) {
    //var_dump($type);

    if($type == 'year') {
      //  Gadam jābūt pagājušam
      if($gads < (int)date('Y')) {
        $bdata['kopsumma_wo_pvn'] = (isset($bdata['summa_opt']) ? $bdata['summa_opt'] : 0) + (isset($bdata['summa_min']) ? $bdata['summa_min'] : 0);

        if(bccomp($bdata['kopsumma_wo_pvn'], 0, 2) == 1) {
          $bdata['bid'] = $gads . '-' . 'year' . '-' . $gads;
          $bdata['sort'] = $gads . '0' . '0';
          $bdata['year'] = $gads;
          $bdata['quarter'] = null;
          $bdata['month'] = null;
          $rows[] = $bdata;
        }
      }
    } elseif($type == 'quarters') {
      //var_dump('inkārt');
      foreach($bdata as $bquart => $bqdata) {
        //  Ceturksnim jābūt pagājušam
        if($gads < (int)date('Y') || ($gads == date('Y') && $bquart * 3 < (int)date('m'))) {
          $bqdata['kopsumma_wo_pvn'] = $bqdata['summa_opt'] + $bqdata['summa_min'];
          if(bccomp($bqdata['kopsumma_wo_pvn'], 0, 2) == 1) {
            $bqdata['bid'] = $gads . '-' . 'quarter' . '-' . $bquart;
            $bqdata['sort'] = $gads . $bquart . '13';
            $bqdata['year'] = $gads;
            $bqdata['quarter'] = $bquart;
            $bqdata['month'] = null;
            $rows[] = $bqdata;
          }
        }
      }
    } elseif($type == 'months') {
      foreach($bdata as $bmonth => $bmdata) {
        //  Ceturksnim jābūt pagājušam
        //var_dump($bmonth);
        if($gads < (int)date('Y') || ($gads == date('Y') && $bmonth < (int)date('m'))) {
          //var_dump('ir');
          $bmdata['kopsumma_wo_pvn'] = $bmdata['summa_opt'] + $bmdata['summa_min'];
          //var_dump($bmdata['kopsumma_wo_pvn']);
          if(bccomp($bmdata['kopsumma_wo_pvn'], 0, 2) == 1) {
            $bmdata['bid'] = $gads . '-' . 'month' . '-' . $bmonth;
            $bmdata['sort'] = $gads . ceil($bmonth/3) . str_pad($bmonth, 2, "0", STR_PAD_LEFT);
            $bmdata['year'] = $gads;
            $bmdata['quarter'] = null;
            $bmdata['month'] = $bmonth;
            $rows[] = $bmdata;
          }
        }
      }
    }
  }
  usort($rows, 'bysortparam');
  return $rows;
}

function bysortparam($a, $b){
  if ($a['sort'] == $b['sort']) {
        return 0;
    }
    return ($a['sort'] > $b['sort']) ? -1 : 1;
}

function corrections_total($merchant_id)
{
  $wo_pvn = 0;
  $w_pvn = 0;
  $w_pvn_plain = 0; // Summa par kādu ir izmaksāts ar pvn bet šajā ciparā vēl pvn nav iekšā

  $start_year = $merchant_id == PARD_VADITAJA_ID ? PARD_VADITAJA_BONUSI_START_YEAR : BONUSS_START_YEAR;

  //  Nofiksētie bonusi
  $rows = db_get_assoc_all(db_query('
    SELECT b.gads, b.menesis, b.ceturksnis, p.pvn
    FROM ' . DB_PREF . 'bonusi b
    INNER JOIN '.DB_PREF.'pardeveji_rekini p ON p.id = b.pardeveja_rekina_id
    WHERE
      b.vaditajs_id = ' . $merchant_id . ' AND
      b.gads >= ' . $start_year . '
  '));

  $data = array();

  for($year = $start_year; $year <= date('Y'); $year++) {
    $data[$year] = get_bonuss_stats_full($merchant_id, $year, true);
  }

  foreach($rows as $row) {
    if($row['menesis']) {
      $pdata = $data[$row['gads']]['months'][$row['menesis']];
    } elseif($row['ceturksnis']) {
      $pdata = $data[$row['gads']]['quarters'][$row['ceturksnis']];
    } else {
      $pdata = $data[$row['gads']]['year'];
    }

    if(bccomp($pdata['correction'], '0', 2)) {
      //  Ir korekcija
      if(bccomp($row['pvn'], '0', 2)) {
        //  Ir pvn
        $w_pvn_plain += $pdata['correction'];
        $w_pvn += $pdata['correction'] * (1 + $row['pvn'] / 100);
      } else {
        //  Nav pvn
        $wo_pvn += $pdata['correction'];
      }
    }
  }

  //  Atņem jau izmaksātās
  $paid = db_get_assoc_all(db_query('
    SELECT
      par_pvn, SUM(summa) as summa
    FROM ' . DB_PREF . 'bonusi_korekcijas
    WHERE
      vaditajs_id = ' . $merchant_id . ' AND
      (
        beigu_gads IS NULL OR beigu_gads >= ' . $start_year . '
      )
    GROUP BY par_pvn
  '));

  foreach($paid as $paid_correction) {
    if(bccomp($paid_correction['par_pvn'], '0', 2)) {
      $w_pvn_plain -= $paid_correction['summa'];
    } else {
      $wo_pvn -= $paid_correction['summa'];
    }
  }


  $result = array();

  if(bccomp($wo_pvn, '0', 2)) {
    $result[] = array('kopsumma_wo_pvn' => $wo_pvn, 'pvn' => 0);
  }

  if(bccomp($w_pvn_plain, '0', 2)) {
    $result[] = array('kopsumma_wo_pvn' => $w_pvn_plain, 'pvn' => 1);
  }

  return $result;
}

function get_all_nozares()
{
  $result = db_get_assoc_all(db_query('SELECT * FROM ' . DB_PREF . 'nozares ORDER BY title_lv'));
  $nozares = array();

  foreach($result as $nozare) {
    $nozares[$nozare['id']] = $nozare;
  }

  return $nozares;
}

function nozare_select_box($name, $selected, $htmloptions = array()) {
  $html = '<select name="'.$name.'" '.(isset($htmloptions['class'])).' '.(isset($htmloptions['id']) ? 'id="'.$htmloptions['id'].'"' : '').' class="'.(isset($htmloptions['class']) ? $htmloptions['class'] : 'nozare_select_box') . '">';

  $html .= '<option value="0"></option>';

  $parents = db_get_assoc_all(db_query('SELECT * FROM '.DB_PREF.'nozares WHERE parent_id = 0 ORDER BY title_lv'));

  $children = db_get_assoc_all(db_query('SELECT * FROM '.DB_PREF.'nozares WHERE parent_id != 0 ORDER BY title_lv'));

  $p_children = array();

  foreach($children as $child) {
    if(!isset($p_children[$child['parent_id']])) {
      $p_children[$child['parent_id']] = array($child);
    } else {
      $p_children[$child['parent_id']][]= $child;
    }
  }

  foreach($parents as $parent) {
    $html .= '<optgroup label="'.$parent['title_lv'].'">';

    if(isset($p_children[$parent['id']])) {
      foreach((array)$p_children[$parent['id']] as $child) {
        $html .= '<option value="'.$child['id'].'" '.($selected == $child['id'] ? 'selected' : '').'>' . $child['title_lv'] . '</option>';
      }
    }

    $html .= '</optgroup>';
  }

  $html .= '</select>';

  return $html;
}

function nozare_popup_content($id)
{
  $html = '<div class="nozares_izvele" id="'.$id.'_dialog">';

  $parents = db_get_assoc_all(db_query('SELECT * FROM '.DB_PREF.'nozares WHERE parent_id = 0 ORDER BY title_lv'));

  $children = db_get_assoc_all(db_query('SELECT * FROM '.DB_PREF.'nozares WHERE parent_id != 0 ORDER BY title_lv'));

  $p_children = array();

  foreach($children as $child) {
    if(!isset($p_children[$child['parent_id']])) {
      $p_children[$child['parent_id']] = array($child);
    } else {
      $p_children[$child['parent_id']][]= $child;
    }
  }

  foreach($parents as $parent) {
    $html .= '<h2 style="cursor:pointer;" onclick="$(this).next().toggle();">'.$parent['title_lv'].'</h2><div style="display:none" class="maincat">';

    if(isset($p_children[$parent['id']])) {
      $html .= '<ul>';
      foreach((array)$p_children[$parent['id']] as $child) {
        $html .= '<li><a href="#" onclick="$(\'#'.$id.'\').val('.$child['id'].'); $(\'#'.$id.'_dialog\').dialog(\'close\');return false;">'. $child['title_lv'] . '</a></li>';
      }
      $html .= '</ul>';
    }

    $html .= '</div>';
  }

  $html .= '</div>';

  return $html;
}

/**
 * Google currency converter
 * Since 2012-08-21 config based conversion
 */
function convert_currency($amount, $from, $to, $precision = 2)
{
    /*$url = 'http://www.google.com/ig/calculator?hl=en&q='. $amount.$from.'=?'.$to;

    $response = file_get_contents($url);
    $response = preg_replace('/\xa0/', '', $response);
    $response = json_decode(str_replace(array('lhs','rhs','error','icc'),array('"lhs"','"rhs"','"error"','"icc"'), $response));

    if($response) {
        $result = explode(' ', $response->rhs);
        $amount = round($result[0], $precision);
        $formated = number_format($result[0], $precision, '.', '');

        return $formated;
    }  */

    if($from == 'EUR' && $to == 'LVL') {
      $converted  = $amount * get_config('lvl_eur');
    } elseif($from == 'LVL' && $to == 'EUR') {
      $converted = $amount / get_config('lvl_eur');
    }

    return number_format($converted, $precision, '.', '');
}

function modelsToList($models, $values, $keys = null)
{
  $result = array();

  if(!empty($models)) {
    foreach($models as $model) {
      if($keys) {
        $result[$model->{$keys}] = $model->{$values};
      } else {
        $result[] = $model->{$values};
      }
    }
  }

  return $result;
}

function get_config($id)
{
  if(isset($GLOBALS['config'][$id])) {
    return $GLOBALS['config'][$id];
  }
}

function update_config()
{
  $GLOBALS['config'] = array();
  $models = Uzstadijums::model()->findAll();
  foreach($models as $model) {
    $GLOBALS['config'][$model->id] = $model->value;
  }
}

function date_check_valid( $datestring ) {
   if ( preg_match('/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/', $datestring) ) {
      list($year , $month , $day) = explode('-',$datestring);
      return checkdate($month , $day , $year);
   } else {
      return false;
   }
}

function getLocalMonth($month_no, $language = 'lv') // month no with leading zeros!
{
  $months = include(DOCUMENT_ROOT . '/languages/' . $language . '/months.php');
  return $months[$month_no];
}

function piedavajums_notify($piedavajums)
{
  if($piedavajums->statuss != 1 && $piedavajums->notification_sent == 0) {
    if(TEST_EMAILS) {
      $recipients = array('davis@datateks.lv');
      $transport = Swift_SmtpTransport::newInstance('smtp.datateks.lv', 25)
          ->setUsername('davis@datateks.lv');
    } else {
      $recipients = explode(',',get_config('email_piedavajumi_admin'));
      $transport = Swift_MailTransport::newInstance();
    }

    try {
      $mailer = Swift_Mailer::newInstance($transport);

      $subject = 'Piedāvājums gaida apstiprinājumu';

      $pardevejs = $piedavajums->getPardevejs();

      $body = sprintf("Klients: %s\nMenedžeris: %s\nLaiks: %s", $piedavajums->klients, $pardevejs->vards, date('Y-m-d H:i'));

      $body_html = nl2br($body);

      $mail = Swift_Message::newInstance();

      //  From mail
      $emails = $pardevejs->getEmails();
      if(!empty($emails)) {
        $email = $emails[0];
      } else {
        $email = ORIG_ATG_FROM_EMAIL;
      }

      //var_dump($email, $pardevejs->vards);die('dsadsa');
      $mail->setFrom(array($email => $pardevejs->vards));

      $mail->setTo($recipients);

      $mail->setSubject($subject);
      $mail->setBody($body_html, 'text/html');
      $mail->addPart($body, 'text/plain');


      if ($mailer->send($mail)) {
        $status = 1;

        $piedavajums->notification_sent = 1;
        $piedavajums->save();
        //echo 'Sent.';
      } else {
        $status = 0;
        //echo 'Could not send.';
      }
    } catch(Exception $e) {
      $status = 0;
      //echo 'Could not send (Error).';
    }

    return $status;
  }
}

//  Pārbauda vai atslēgvārds jau nav aizņemts vismaz trijās pozīcijās (aktīviem līgumiem)
function is_3poz($atslegvards, $valsts_kods)
{
  $date = date('Y-m-d');
  $sql = '
    SELECT l.id
    FROM new_ligumi l
    LEFT JOIN new_pielikumi_tiessaite g ON g.liguma_id = l.id
    WHERE
      g.atslegvards = "'.esc($atslegvards).'"
      AND DATE(l.reklama_no) <= "'.$date.'"
      AND DATE(l.reklama_lidz) >= "'.$date.'"
      AND g.domeni = "'.esc($valsts_kods).'"
  ';
  $rows = db_get_assoc_all(db_query($sql));

  $active = array();

  foreach($rows as $row) {
    if(get_liguma_status_by_id($row['id']) == 3) {
      $active[] = $row['id'];
    }
  }

  $active_i = array();
  if($valsts_kods == 'lv') {
    $infomedia = db_get_assoc_all(db_query('
      SELECT g.atslegvards, l.id as lid
      FROM '.DB_PREF.'pielikumi_infomedia g
      LEFT JOIN '.DB_PREF.'ligumi l ON g.liguma_id = l.id
      WHERE
        DATE(l.reklama_no) <= "'.$date.'"
        AND DATE(l.reklama_lidz) >= "'.$date.'"
        AND g.atslegvards = "'.$atslegvards.'"
    '));

    foreach((array)$infomedia as $irow) {
      if(get_liguma_status_by_id($irow['lid']) == 3) {
        $active_i[] = $irow['lid'];
      }
    }
  }

  if(count($active) >= 3 || (count($active) == 2 && !empty($active_i))) {
    return array_merge($active, $active_i);
  } else {
    return false;
  }
}

//  Pārbauda vai atslēgvārds jau nav aizņemts vismaz sešās pozīcijās (aktīviem līgumiem)
function is_6poz($atslegvards, $valsts_kods)
{
  $date = date('Y-m-d');
  $sql = '
    SELECT l.id
    FROM new_ligumi l
    LEFT JOIN new_pielikumi_tiessaite g ON g.liguma_id = l.id
    WHERE
      g.atslegvards = "'.esc($atslegvards).'"
      AND DATE(l.reklama_no) <= "'.$date.'"
      AND DATE(l.reklama_lidz) >= "'.$date.'"
      AND g.domeni = "'.esc($valsts_kods).'"
  ';
  $rows = db_get_assoc_all(db_query($sql));

  $active = array();

  foreach($rows as $row) {
    if(get_liguma_status_by_id($row['id']) == 3) {
      $active[] = $row['id'];
    }
  }

  $active_i = array();
  if($valsts_kods == 'lv') {
    $infomedia = db_get_assoc_all(db_query('
      SELECT g.atslegvards, l.id as lid
      FROM '.DB_PREF.'pielikumi_infomedia g
      LEFT JOIN '.DB_PREF.'ligumi l ON g.liguma_id = l.id
      WHERE
        DATE(l.reklama_no) <= "'.$date.'"
        AND DATE(l.reklama_lidz) >= "'.$date.'"
        AND g.atslegvards = "'.$atslegvards.'"
    '));

    foreach((array)$infomedia as $irow) {
      if(get_liguma_status_by_id($irow['lid']) == 3) {
        $active_i[] = $irow['lid'];
      }
    }
  }

  if(count($active) >= 3 || (count($active) == 5 && !empty($active_i))) {
    return array_merge($active, $active_i);
  } else {
    return false;
  }
}

//  Pārbauda vai atslēgvārds jau nav aizņemts vienā top3 pozīcijā (aktīvam līgumam)
function is_top3($atslegvards, $valsts_kods) {
  $date = date('Y-m-d');
  $sql = '
    SELECT l.id
    FROM new_ligumi l
    LEFT JOIN new_pielikumi_tiessaite g ON g.liguma_id = l.id
    WHERE
      g.atslegvards = "'.esc($atslegvards).'"
      AND g.top3 = 1
      AND DATE(l.reklama_no) <= "'.$date.'"
      AND DATE(l.reklama_lidz) >= "'.$date.'"
      AND g.domeni = "'.esc($valsts_kods).'"
  ';
  $rows = db_get_assoc_all(db_query($sql));

  $active = array();

  foreach($rows as $row) {
    if(get_liguma_status_by_id($row['id']) == 3) {
      $active[] = $row['id'];
    }
  }

  if(!empty($active)) {
    return $active;
  } else {
    return false;
  }
}

function get_tiesibu_dati($user_id, $type)
{
  $result = db_get_assoc_all(db_query('
    SELECT t.alias, l.piekluve as lietotajs, g.piekluve as grupa
    FROM ' . DB_PREF . 'tiesibas t
    LEFT JOIN '.DB_PREF.'tiesibas_lietotaji l ON l.tiesibas_id = t.id AND l.lietotajs_id = ' . esc($user_id) . '
    LEFT JOIN '.DB_PREF.'tiesibas_grupas g ON g.tiesibas_id = t.id AND g.grupa = "' . esc($type) . '"
  '));

  $data = array();

  if(!empty($result)) {
    foreach($result as $row) {
      if($row['alias']) {
        $data[$row['alias']]['user'] = $row['lietotajs'];
        $data[$row['alias']]['group'] = $row['grupa'];
      }
    }
  }

  return $data;
}

/**
 * Pārbauda lietotāja tiesības uz konkrētu uzdevumu
 * Tiesību tabulu dati jau pie init ir ielādēti un saglabāti iekš globals, ātrdarbības dēļ
 */
function check_access($task, $user_id = null, $allow_superadmin = SUPERUSER_FULL_ACCESS) {

  if(!$user_id) {
    $perms = $GLOBALS['permissions'];
  } else {
    $user = Pardevejs::model()->findByPk($user_id);
    $perms = get_tiesibu_dati($user->id, $user->tips);
  }

  if(isset($perms[$task])) {

    //  Produkcijas vidē ja tas ir super admins tad tālāk nemaz neskatās un uzreiz atļauj visu
    if($allow_superadmin) {
      if($user_id) {
        if($user->superadmin) {
          return true;
        }
      } elseif(is_superadmin()) {
        return true;
      }
    }


    //  Pārbauda LIETOTĀJA individuālās tiesības
    if($perms[$task]['user']) { // <> 0
      return $perms[$task]['user'] == 1 ? true : false;

    } else {
      //  Lietotāja tiesībās bija 0, skatāmies GRUPAS tiesības
      return $perms[$task]['group'] == 1 ? true : false;

    }
  } else {
    //  Nav tādas tiesības definētas
    throw new Exception('Undefined permissions task: ' . $task);
  }
}

//  Saraksts ar sagrupētiem darbiniekiem - Darbinieki, Vadītāji, Partneri - ko pēc tam izmantot filtru ģenerēšanai
function user_filter_list()
{
  $list = array();

  //  Darbinieki
  $list['darbinieki'] = 'Darbinieki';
  foreach((array)Pardevejs::model()->sales()->findAll(array('order' => 'vards')) as $pardevejs) {
    $list[$pardevejs->id] = '-&nbsp;&nbsp;&nbsp;&nbsp;' . $pardevejs->vards;
  }

  //  Vaditaji
  $list['vaditaji'] = 'Vadītāji';
  foreach((array)Pardevejs::model()->manager()->findAll(array('order' => 'vards')) as $pardevejs) {
    $list[$pardevejs->id] = '-&nbsp;&nbsp;&nbsp;&nbsp;' . $pardevejs->vards;
  }

  //  Partneri
  $list['partneri'] = 'Partneri';
  foreach((array)Pardevejs::model()->partner()->findAll(array('order' => 'vards')) as $pardevejs) {
    $list[$pardevejs->id] = '-&nbsp;&nbsp;&nbsp;&nbsp;' . $pardevejs->vards;
  }

  return $list;
}

function getBankAccount($rek_data, $lig_data,$from='infomedia') {
  global $_vars;

  if($rek_data['bank_account'] && false) { // currently disabled
    //  Ir manuāli norādīts konts rēķina līmenī
    $bank_account = $rek_data['bank_account'];
  } else if($lig_data['bank_account'] && false) { // currently disabled
    //  Ir manuāli norādīts konts līguma līmenī
    $bank_account = $lig_data['bank_account'];

  } else {

    if($lig_data['mpk']) {
      // MPK pakalpojums. Ņemam mpk kontu
			if ($from=='infomedia'){
      	$bank_account = $_vars['bank_account_numbers']['mpk'];
			}else{
      	$bank_account = $_vars['bank_account_numbers_leads']['mpk'];
			}

    } else {
      //  MPT pakalpojums. Skatāmies līguma slēdzēju
      $pardevejs = Pardevejs::model()->findByPk($lig_data['pardeveja_id']);

      if($pardevejs) {

        if($pardevejs->isKorp() || $pardevejs->isPartner()) {
        	if ($from=='infomedia'){
          	$bank_account = $_vars['bank_account_numbers']['mpk'];
					}
					else{
						$bank_account = $_vars['bank_account_numbers_leads']['mpk'];
					}
        }
				else {
					if ($from=='infomedia'){	
          	$bank_account = $_vars['bank_account_numbers']['mpt'];
					}
					else{
						$bank_account = $_vars['bank_account_numbers_leads']['mpt'];
					}
        }
      } else {
        // Neeksistējošs pārdevējs. ņemam defaulto kontu
				if ($from=='infomedia'){
        	$bank_account = REKV_KONTS;
				}
				else{
					$bank_account = REKV_KONTS2;
				}
      }
    }
  }

  return $bank_account;
}

function getAdwordsReportData($ligums_id, $params = array(), $filter_campaigns = true)
{
  $args = array();

  if(isset($params['atskaite_no'], $params['atskaite_lidz'])) {
    //  Vai beigu datums nav lielāks par sākumu
    if(strtotime($params['atskaite_no']) > strtotime($params['atskaite_lidz'])) {
      die('Beigu datums nevar būt mazāks par sākuma datumu.');
    }

    $args['date'] = array();
    if(isset($params['atskaite_no'])) {
      $args['date']['min'] = $params['atskaite_no'];
    }
    if(isset($params['atskaite_lidz'])) {
      $args['date']['max'] = $params['atskaite_lidz'];
    }
  }

  $ligums = Ligums::model()->findByPk($ligums_id);

  $filter_name = mb_strtolower($ligums->nosaukums);

  $filter_name = preg_replace('/([.]*)( sia)$/', '$1', trim($filter_name));
  $filter_name = preg_replace('/([.]*)( ik)$/', '$1', trim($filter_name));

  //  Client id
  $account_numbers = $ligums->getAdwordsAccounts();

  //var_dump($account_numbers);

  if(empty($account_numbers)) {
    die('Šim klientam nav adwords kontu');
  }

  //  testing
  //$filter_campaigns = false; // JA RĀDA KA NAV ATRASTA NEVIENA REKLĀMAS KOPA TAD ŠO PAMĒĢINĀT ATKOMENTĒT
  //$account_numbers = array('7569685857', '9468521030');

  //$adwords = new Adwords(ADWORDS_VERSION);var_dump($adwords->getAccountHierarchy());die;
  // end testing

  $data = array();

  foreach($account_numbers as $customerId) {
    $adwords = new Adwords(ADWORDS_VERSION, $customerId, 'customers');

    $filePath = 'tmp/adwords_reports/'.uniqid().'.csv';
    $adwords->downloadReport($filePath, $args);

    $campaigns = $adwords->getCampaigns();

    if($filter_campaigns) {
      foreach($campaigns as $id => $campaign) {
        if(strstr(mb_strtolower($campaign), $filter_name) === false) {
          unset($campaigns[$id]);
        }
      }
    }

    if(!empty($campaigns)) {
      $stats = loadReportData($filePath, array_keys($campaigns));

      $data = array_merge($data, $stats);
    }
  }

  if($filter_campaigns) {
    //  Concat similar ad groups
    $tmp_data = array();

    foreach($data as $row) {
      $tmp_data[strtolower($row['ad_group'])][] = $row;
    }

    $new_data = array();

    foreach($tmp_data as $rows) {
      $impressions = 0;
      $clicks = 0;
      $positions = array();

      foreach($rows as $row) {
        $impressions += $row['impressions'];
        $clicks += $row['clicks'];
        $positions[] = $row['avg_pos'];
      }

      $pos_count = count(array_filter($positions));
      $pos_sum = array_sum(array_filter($positions));

      $new_data[] = array(
        'ad_group' => $row['ad_group'],
        'impressions' => $impressions,
        'clicks' => $clicks,
        'avg_pos' => $pos_count ? $pos_sum/$pos_count : 0,
      );
    }

    return $new_data;
  }

  return $data;
}

function loadReportData($filePath, $campaigns)
{
  require_once('libs/php-parsecsv/parsecsv.lib.php');

  $csv = new parseCSV();
  //$csv->encoding('UTF-16', 'UTF-8');
  //$csv->delimiter = ',';
  $csv->offset = 2;
  $csv->heading = false;
  $csv->parse($filePath);

  $data = array();
  foreach($csv->data as $row) {
    if(in_array($row[5], $campaigns)) {
      $data[] = array(
        'campaign_name' => $row[0],
        'ad_group' => $row[1],
        'impressions' => $row[2],
        'clicks' => $row[3],
        'avg_pos' => $row[4],
      );
    }
  }

  return $data;
}

/**
 * allow cli and localhost only
 */
function allowCliOnly(){

    if (isset($_SERVER['HTTP_CLIENT_IP'])
    || isset($_SERVER['HTTP_X_FORWARDED_FOR'])
    || !(in_array(@$_SERVER['REMOTE_ADDR'], ['127.0.0.1', '::1'], true) || PHP_SAPI === 'cli-server' || PHP_SAPI === 'cli')
) {
    header('HTTP/1.0 403 Forbidden');
    exit('You are not allowed to access this file.');
}
}

?>