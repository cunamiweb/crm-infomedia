<?php
//set_time_limit(0);
include( "inc/init.php" );
include( "inc/glob_actions.php" );
?>
<!DOCTYPE HTML>
<html>
<head>
    <title>Infomedia</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>


    <!--<link type="text/css" href="css/ui-lightness/jquery-ui-1.8.15.custom.css" rel="stylesheet" />-->
    <link rel="stylesheet" href="assets/jquery-ui-1.12.1/jquery-ui.min.css" />
    <link rel="stylesheet" href="js/jquery_multiselect/jquery.multiSelect.css" />
    <link rel="stylesheet" href="css/style.css?15" >


    <!--<script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>-->
    <!--    <script type="text/javascript" src="https://code.jquery.com/jquery-1.8.0.min.js"></script>-->
    <!--    <script type="text/javascript" src="https://code.jquery.com/jquery-migrate-1.4.1.js"></script>-->
<!--    <script type="text/javascript" src="https://code.jquery.com/jquery-1.8.0.min.js"></script>-->
    <script type="text/javascript" src="assets/jquery-3.3.1.min.js"></script>
    <!--<script type="text/javascript" src="js/jquery-ui-1.8.15.custom.min.js"></script>-->
    <script type="text/javascript" src="assets/jquery-ui-1.12.1/jquery-ui.min.js"></script>
    <script type="text/javascript" src="js/jquery.tablesorter.min.js"></script>
    <script type="text/javascript" src="js/jstorage.min.js"></script>
    <script type="text/javascript" src="js/wtooltip.js"></script>
    <!--<script type="text/javascript" src="js/jquery_multiselect/jquery.multiSelect.js"></script>-->
    <script type="text/javascript" src="js/jquery_multiselect/jquery.multiSelect_modified2019.js"></script>
    <script type="text/javascript" src="js/jquery.toggleDisable.js"></script>

    <!--<script type="text/javascript" src="js/chosen/chosen.jquery.min.js"></script>-->
    <!--    <script type="text/javascript" src="assets/chosen_v1.8.7/chosen.jquery.min.js"></script>-->
    <!--<link type="text/css" href="js/chosen/chosen.css" rel="stylesheet" />-->
    <!--    <link type="text/css" href="assets/chosen_v1.8.7/chosen.min.css" rel="stylesheet" />-->

    <!--<script type="text/javascript" src="js/jquery.mtz.monthpicker.js"></script>-->
    <script type="text/javascript" src="js/jquery.mtz.monthpicker_mod_2019.js"></script>

    <script type="text/javascript" src="js/scripts.js?9"></script>



	<?php if ( $_SESSION['login'] ) { ?>
        <script type="text/javascript">init_auto_logout()</script>
	<?php } ?>
</head>
<body>

<?php
if ( ! $_SESSION['login'] ) {
	include( "modules/login.php" );
} else {

	if ( ENABLE_RAW_LOG ) {
		$rawlog = new RawLog;
		$rawlog->save();
	}

	include( "modules/main.php" );

	/*if(ENABLE_RAW_LOG) {
	  $rawlog->sqls = array();
	  foreach($GLOBALS['executed_queries'] as $v) {
		$rawlog->sqls[] = $v['sql'];
	  }
	  $rawlog->save();
	} */
}
db_close();
if ( DEBUG_QUERIES ) { ?>
    <div>
		<?php foreach ( $GLOBALS['executed_queries'] as $v ) { ?>
            <p>
                <strong>SQL:</strong>:
            <pre><?php echo $v['sql']; ?></pre>
            <strong>Exec. time:</strong> <?php echo $v['time']; ?><br/>
            </p>
		<?php } ?>
    </div>
    <p><strong>Total time:</strong> <?php echo $GLOBALS['query_total_time']; ?></p>
<?php } ?>

</body>
</html>
